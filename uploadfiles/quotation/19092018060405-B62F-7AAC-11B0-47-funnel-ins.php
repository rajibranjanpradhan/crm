<?php
include "session_handler.php";
?>
<!DOCTYPE html>
<html class="no-js css-menubar" lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="description" content="bootstrap material admin template">
    <meta name="author" content="">
    
    <title>Update | Bizapp CRM</title>
    
    <link rel="apple-touch-icon" href="../../assets/images/apple-touch-icon.png">
    <link rel="shortcut icon" href="../../assets/images/favicon.ico">
    
    <!-- Stylesheets -->
    <link rel="stylesheet" href="../../assets/global/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../assets/global/css/bootstrap-extend.min.css">
    <link rel="stylesheet" href="../../assets/css/site.min.css">
    
    <!-- Plugins -->
    <link rel="stylesheet" href="../../assets/global/vendor/animsition/animsition.css">
    <link rel="stylesheet" href="../../assets/global/vendor/asscrollable/asScrollable.css">
    <link rel="stylesheet" href="../../assets/global/vendor/switchery/switchery.css">
    <link rel="stylesheet" href="../../assets/global/vendor/intro-js/introjs.css">
    <link rel="stylesheet" href="../../assets/global/vendor/slidepanel/slidePanel.css">
    <link rel="stylesheet" href="../../assets/global/vendor/jquery-mmenu/jquery-mmenu.css">
    <link rel="stylesheet" href="../../assets/global/vendor/flag-icon-css/flag-icon.css">
    <link rel="stylesheet" href="../../assets/global/vendor/waves/waves.css">
        <link rel="stylesheet" href="../../assets/global/vendor/chartist/chartist.css">
        <link rel="stylesheet" href="../../assets/global/vendor/jvectormap/jquery-jvectormap.css">
        <link rel="stylesheet" href="../../assets/global/vendor/chartist-plugin-tooltip/chartist-plugin-tooltip.css">
        <link rel="stylesheet" href="../../assets/examples/css/dashboard/v1.css">
    <!--  date picker  -->
    <link rel="stylesheet" href="../../assets/global/vendor/bootstrap-datepicker/bootstrap-datepicker.css">
    
    <!-- Fonts -->
    <link rel="stylesheet" href="../../assets/global/fonts/material-design/material-design.min.css">
    <link rel="stylesheet" href="../../assets/global/fonts/brand-icons/brand-icons.min.css">
    <link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Roboto:300,400,500,300italic'>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

     <!-- table set -->
    <link rel='stylesheet' href='../../assets/css/datatableset.css'>
    <?php include "includes/css/select.php"; ?>
    <link rel='stylesheet' href='https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css'>
    
    <!--[if lt IE 9]>
    <script src="../../assets/global/vendor/html5shiv/html5shiv.min.js"></script>
    <![endif]-->
    
    <!--[if lt IE 10]>
    <script src="../../assets/global/vendor/media-match/media.match.min.js"></script>
    <script src="../../assets/global/vendor/respond/respond.min.js"></script>
    <![endif]-->
    
    <!-- Scripts -->
    <script src="../../assets/global/vendor/breakpoints/breakpoints.js"></script>
    <script>
      Breakpoints();
    </script>
  
  </head>
  <body class="animsition site-navbar-small dashboard">
    <!--[if lt IE 8]>
        <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->

    <?php include "navbar-header.php"; ?>     
    <?php include "side-navigation.php"; ?>   
    <?php
                                            //$q=$_GET['v'];
                                        

                                            $c=$_GET['c'];
                                            $ctype=$_GET['ctype'];
                                            $md=$_GET['md'];
                                            $proj=$_GET['proj'];
                                            $fid=$_GET['fid'];
                                            $query=mysqli_query($dbc,"select * from `funnel` where `id`='$fid' and `Company`='$c' and `MeetingDate`='$md' and `ProjectName`='$proj'  order by id ");
                                            while($row=mysqli_fetch_assoc($query))
                                            {
                                                $company=$row['Company'];
                                                $sector=$row['Sector'];
                                                $campaigntype=$row['CampaignType'];
                                                $projectname=$row['ProjectName'];
                                                $meetingdate=$row['MeetingDate'];
                                                $products=$row['Products'];
                                                $service=$row['Services'];
                                                $detail=$row['Detail'];
                                                $revenue=$row['Revenue'];
                                                $margin=$row['Margin'];
                                                $stage=$row['Stage'];
                                                $probability=$row['Probability'];
                                                $demo=$row['Demo'];
                                                $poc=$row['POC'];
                                                $action=$row['Action'];
                                                $dateofclosure=$row['DateOfClosure'];
                                                $expectedclosure=$row['ExpectedClosure'];
                                               
                                            }    

                                            $marginpercent=($margin*100)/$revenue;

                                        ?>


    <!-- Page -->
    <div class="page">
      <!-- write body content here -->

      <div class="page-content">

        <div class="panel">
               <div class="panel-body container-fluid">
                  <div class="row row-lg">
                     <div class="col-md-12">
                        <div class="example-wrap">
                           <h4 class="example-title">Update Funnel Data</h4>
                            <form action="../auth/funnel/ins.php" method="post" autocomplete="off"  >

                           <input type="hidden" name="campaigntype" value="<?php echo $campaigntype;?>" />
                           <input type="hidden" name="projectname" value="<?php echo $projectname; ?>" />
                           <input type="hidden" name="meetingdate" value="<?php echo $meetingdate; ?>" />

                           <input type="hidden" name="fid" value="<?php echo $fid; ?>" />

                             <div class="row">
                                <div class="form-group  col-md-12">
                                <label class="form-control-label" >Company</label>
                  
                             
                             <input type="text" name="company" class=" form-control" data-plugin="maxlength"
                               data-placement="bottom-right-inside" value="<?php echo $company; ?>" >
                    
                           </div>

                              <div class="form-group  col-md-6">
                                <label class="form-control-label" >Sector</label>
                  
                             <select name="sector" class="form-control maxlength" data-plugin="select2">
                                <?php

                                                                $comp=$_GET['c'];
                                                                    $project=mysqli_query($dbc,"select distinct(SectorName) from `sector`  ");
                                                                    echo '<option  value='.$sector.'>'.$sector.'</option>';
                                                                    while($row=mysqli_fetch_assoc($project))
                                                                    {
                                                                        //$pro=$row['ProjectName'];


                                                        
                                                                         echo '<option  value="'.$row['SectorName'].'">'.$row['SectorName'].'</option>';
                                                                        
                                                                    }
                                                                ?>
                    </select>
                                      
                         </div>
                
                <!-- End Example Basic -->
                    <div class="form-group  col-md-6">
                                <label class="form-control-label" >Products</label>
                        <?php $fet=mysqli_query($dbc,"select * from products"); ?>
                             <select name="products" class="form-control maxlength" data-plugin="select2">
                                <option value="<?php echo $products; ?>"><?php echo $products; ?></option>
                                    <?php
                                        while($fetr=mysqli_fetch_assoc($fet))
                                        {
                                  echo "<option value={$fetr['ProductName']}>{$fetr['ProductName']}</option>";
                                         }

                                    ?>
                            </select>
                                      
                    </div>
                    <div class="form-group  col-md-6">
                                <label class="form-control-label" >Service</label>
                        <?php $fet=mysqli_query($dbc,"select distinct(Services) from products"); ?>
                             <select name="products" class="form-control maxlength" data-plugin="select2">
                                <option value="<?php echo $service; ?>"><?php echo $service; ?></option>
                                <?php
                                 while($fetr=mysqli_fetch_assoc($fet))
                                {
                                 echo "<option value={$fetr['Services']}>{$fetr['Services']}</option>";
                                 }

                                ?>
                            </select>
                                      
                    </div>

                            <div class="form-group  col-md-6">
                                <label class="form-control-label" >Details</label>
                  
                             
                             <input type="text" name="detail" class=" form-control" data-plugin="maxlength"
                               data-placement="bottom-right-inside" value="<?php echo $detail; ?>"
                              placeholder="Enter Details of Requirement">
                    
                           </div>
                           <div class="form-group  col-md-6">
                                <label class="form-control-label" >Revenue</label>
                  
                             
                             <input type="number" name="revenue" class=" form-control" data-plugin="maxlength"
                               data-placement="bottom-right-inside" value="<?php echo $revenue; ?>">
                    
                           </div>
                           <div class="form-group  col-md-6">
                                <label class="form-control-label" >Margin(%)</label>
                  
                             
                             <input type="text" name="margin" class=" form-control" data-plugin="maxlength"
                               data-placement="bottom-right-inside" value="<?php echo $marginpercent; ?>" >
                    
                           </div>
                           <div class="form-group  col-md-6">
                                <label class="form-control-label" >Stage</label>
                      
                             <select name="stage" class="form-control maxlength" data-plugin="select2">
                                <option value="<?php echo $stage; ?>"><?php echo $stage; ?></option>
                                                                <option value="Pitching">Pitching</option>
                                                                <option value="Demo">Demo</option>
                                                                <option value="POC">POC</option>
                                                                <option value="Negotiation">Negotiation</option>
                                                                <option value="Won">Won</option>
                                                                <option value="Quotation">Quotation</option>
                                                                <option value="Lost">Lost</option>
                                                                <option value="Hold">Hold</option>
                            </select>
                                      
                    </div>
                    <div class="form-group  col-md-6">
                                <label class="form-control-label" >Probability</label>
                      
                             <select name="probability" class="form-control maxlength" data-plugin="select2">
                    <option value="<?php echo $probability; ?>"><?php echo $probability; ?> %</option>
                                                                <option value="0">0 %</option>
                                                                <option value="25">25 %</option>
                                                                <option value="50">50 %</option>
                                                                <option value="75">75 %</option>
                                                                <option value="100">100 %</option>
                            </select>
                                      
                    </div>
                    <div class="form-group  col-md-6">
                                <label class="form-control-label" >Demo</label>
                      
                             <select name="demo" class="form-control maxlength" data-plugin="select2">
                                <option value="<?php echo $demo; ?>"><?php echo $demo; ?></option>
                                                                <option value="Yes">Yes</option>
                                                                <option value="No">No</option>
                            </select>
                                      
                    </div>
                    <div class="form-group  col-md-6">
                                <label class="form-control-label" >POC</label>
                      
                             <select name="poc" class="form-control maxlength" data-plugin="select2">
                                <option value="<?php echo $probability; ?>"><?php echo $poc; ?></option>
                                                                <option value="Yes">Yes</option>
                                                                <option value="No">No</option>
                            </select>
                                      
                    </div>
                    <div class="form-group  col-md-12">
                                <label class="form-control-label" >Action</label>
                                <textarea name="action" value="<?php echo $action; ?>" rows="3" title="Input Action..." class="form-control" placeholder="Enter customer Action" ><?php echo $action; ?></textarea>
                <!-- End Example Basic -->
                    </div>
                    <div class="form-group  col-md-6">
                                <label class="form-control-label" >Expected Date of Closure</label>

                                   <div class="input-group">
                                      <div class="input-group-prepend">
                                           <span class="input-group-text">
                                            <i class="icon md-calendar" aria-hidden="true"></i>
                                           </span>
                                      </div>
                                         <input type="text" name="dateofclosure" value="<?php echo $dateofclosure; ?>" class="form-control datepair-date datepair-end" data-plugin="datepicker">
                                   </div>

                    </div>
                    <div class="form-group  col-md-6">
                                <label class="form-control-label" >Expected Closure</label>
                      <select name="expectedclosure" class="form-control maxlength" data-plugin="select2">
                    <option value="<?php echo $expectedclosure; ?>"><?php echo $expectedclosure; ?></option>
                                                            <option value="January">January</option>
                                                                <option value="February">February</option>
                                                                <option value="March">March</option>
                                                                <option value="April">April</option>
                                                                <option value="May">May</option>
                                                                <option value="June">June</option>
                                                                <option value="July">July</option>
                                                                <option value="August">August</option>
                                                                <option value="September">September</option>
                                                                <option value="October">October</option>
                                                                <option value="November">November</option>
                                                                <option value="December">December</option>
                                                            </select>
                    </div>
                </div>
             <button type="submit" name="submit" class="btn-primary btn">Submit</button>
              <button type="submit" class="btn-default btn-pure btn">Cancel</button>
        </form>
        </div> 
      </div>
    </div></div></div></div></div>
    <!-- End Page -->


    <!-- Footer -->
    <?php include "includes/footer.php"; ?>

    
    <!-- Core  -->
    <script src="../../assets/global/vendor/babel-external-helpers/babel-external-helpers.js"></script>
    <script src="../../assets/global/vendor/jquery/jquery.js"></script>
    <script src="../../assets/global/vendor/popper-js/umd/popper.min.js"></script>
    <script src="../../assets/global/vendor/bootstrap/bootstrap.js"></script>
    <script src="../../assets/global/vendor/animsition/animsition.js"></script>
    <script src="../../assets/global/vendor/mousewheel/jquery.mousewheel.js"></script>
    <script src="../../assets/global/vendor/asscrollbar/jquery-asScrollbar.js"></script>
    <script src="../../assets/global/vendor/asscrollable/jquery-asScrollable.js"></script>
    <script src="../../assets/global/vendor/waves/waves.js"></script>
    
    <!-- Plugins -->
    <script src="../../assets/global/vendor/jquery-mmenu/jquery.mmenu.min.all.js"></script>
    <script src="../../assets/global/vendor/switchery/switchery.js"></script>
    <script src="../../assets/global/vendor/intro-js/intro.js"></script>
    <script src="../../assets/global/vendor/screenfull/screenfull.js"></script>
    <script src="../../assets/global/vendor/slidepanel/jquery-slidePanel.js"></script>
        <script src="../../assets/global/vendor/chartist/chartist.min.js"></script>
        <script src="../../assets/global/vendor/chartist-plugin-tooltip/chartist-plugin-tooltip.js"></script>
        <script src="../../assets/global/vendor/jvectormap/jquery-jvectormap.min.js"></script>
        <script src="../../assets/global/vendor/jvectormap/maps/jquery-jvectormap-world-mill-en.js"></script>
        <script src="../../assets/global/vendor/matchheight/jquery.matchHeight-min.js"></script>
        <script src="../../assets/global/vendor/peity/jquery.peity.min.js"></script>
    
    <!-- Scripts -->
    <script src="../../assets/global/js/Component.js"></script>
    <script src="../../assets/global/js/Plugin.js"></script>
    <script src="../../assets/global/js/Base.js"></script>
    <script src="../../assets/global/js/Config.js"></script>
    
    <script src="section/Menubar.js"></script>
    <script src="section/Sidebar.js"></script>
    <script src="section/PageAside.js"></script>
    <script src="section/GridMenu.js"></script>
    
    <!-- Config -->
    <script src="../../assets/global/js/config/colors.js"></script>
    <script src="../../assets/js/config/tour.js"></script>
    <script>Config.set('assets', '../assets');</script>
    
    <!-- Page -->
    <script src="../../assets/js/Site.js"></script>
    <script src="../../assets/global/js/Plugin/asscrollable.js"></script>
    <script src="../../assets/global/js/Plugin/slidepanel.js"></script>
    <script src="../../assets/global/js/Plugin/switchery.js"></script>
        <script src="../../assets/global/js/Plugin/matchheight.js"></script>
        <script src="../../assets/global/js/Plugin/jvectormap.js"></script>
        <script src="../../assets/global/js/Plugin/peity.js"></script>
    
        <script src="../../assets/examples/js/dashboard/v1.js"></script>

<?php include "includes/css/select.php"; ?>
      <?php include "includes/js/select.php"; ?>

        <!-- date picker -->
        <script src="../../assets/global/vendor/bootstrap-datepicker/bootstrap-datepicker.js"></script>
        <script src="../../assets/global/js/Plugin/bootstrap-datepicker.js"></script>

    
  </body>
</html>

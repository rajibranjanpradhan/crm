-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: May 17, 2018 at 11:09 AM
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `navikra`
--

-- --------------------------------------------------------

--
-- Table structure for table `hr_holiday`
--

CREATE TABLE IF NOT EXISTS `hr_holiday` (
`id` int(255) NOT NULL,
  `Representive` varchar(100) DEFAULT NULL,
  `RMail` varchar(100) DEFAULT NULL,
  `Day` varchar(20) DEFAULT NULL,
  `HoliDayDate` varchar(20) DEFAULT NULL,
  `HoliDay` varchar(100) DEFAULT NULL,
  `ModificationDetail` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `hr_holiday`
--

INSERT INTO `hr_holiday` (`id`, `Representive`, `RMail`, `Day`, `HoliDayDate`, `HoliDay`, `ModificationDetail`) VALUES
(1, 'Korai Purusottam', 'purusottam@navikra.com', 'Monday', '2018-01-01', 'New Year Day', '2018-01-06 11:44:05'),
(3, 'Korai Purusottam', 'purusottam@navikra.com', 'Friday', '2018-01-26', 'Republic Day', '2018-01-06 11:45:41');

-- --------------------------------------------------------

--
-- Table structure for table `hr_leave`
--

CREATE TABLE IF NOT EXISTS `hr_leave` (
`id` int(255) NOT NULL,
  `Representive` varchar(100) DEFAULT NULL,
  `RMail` varchar(100) DEFAULT NULL,
  `FromDate` varchar(100) DEFAULT NULL,
  `ToDate` varchar(100) DEFAULT NULL,
  `NoOfDay` int(255) NOT NULL DEFAULT '0',
  `NoOfDayRemaining` int(255) NOT NULL DEFAULT '0',
  `Reason` text,
  `Approval` int(3) NOT NULL DEFAULT '0',
  `DecisionBy` varchar(100) DEFAULT NULL,
  `ModificationDetail` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `hr_leave`
--

INSERT INTO `hr_leave` (`id`, `Representive`, `RMail`, `FromDate`, `ToDate`, `NoOfDay`, `NoOfDayRemaining`, `Reason`, `Approval`, `DecisionBy`, `ModificationDetail`) VALUES
(4, 'Korai Purusottam', 'purusottam@navikra.com', '2018-01-06', '2018-01-11', 0, 0, 'dsfsdf', 1, 'purusottam@navikra.com', '2018-01-06 08:03:42'),
(5, 'Korai Purusottam', 'purusottam@navikra.com', '2018-01-31', '2018-02-15', 0, 0, 'test2', 2, 'purusottam@navikra.com', '2018-01-06 15:47:24'),
(6, 'Korai Purusottam', 'purusottam@navikra.com', '2018-01-07', '2018-01-10', 0, 0, 'demo', 0, NULL, '2018-01-07 13:50:36'),
(7, 'Korai Purusottam', 'purusottam@navikra.com', '2018-02-01', '2018-02-07', 0, 0, '', 0, NULL, '2018-02-05 21:26:08'),
(8, 'Korai Purusottam', 'purusottam@navikra.com', '2018-02-05', '2018-02-06', 0, 0, 'sdf', 0, NULL, '2018-02-05 21:31:58'),
(9, 'Korai Purusottam', 'purusottam@navikra.com', '2018-02-06', '2018-02-15', 0, 0, 'as', 0, NULL, '2018-02-05 21:44:31'),
(10, 'Korai Purusottam', 'purusottam@navikra.com', '2018-03-11', '2018-03-15', 5, 0, 'test1', 2, 'purusottam@navikra.com', '2018-03-11 17:42:06'),
(11, 'Korai Purusottam', 'purusottam@navikra.com', '2018-03-11', '2018-03-20', 10, 0, 'test2', 1, 'purusottam@navikra.com', '2018-03-11 17:47:26');

-- --------------------------------------------------------

--
-- Table structure for table `incentive`
--

CREATE TABLE IF NOT EXISTS `incentive` (
`id` int(255) NOT NULL,
  `Representive` varchar(100) DEFAULT NULL,
  `RMail` varchar(100) DEFAULT NULL,
  `IncentiveName` varchar(100) DEFAULT NULL,
  `IncentivePercentage` varchar(100) DEFAULT NULL,
  `Criteria` varchar(100) DEFAULT NULL,
  `ModificationDetail` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `incentive`
--

INSERT INTO `incentive` (`id`, `Representive`, `RMail`, `IncentiveName`, `IncentivePercentage`, `Criteria`, `ModificationDetail`) VALUES
(4, 'Korai Purusottam', 'purusottam@navikra.com', 'Basic', '10', '100', '2018-02-25 20:56:16');

-- --------------------------------------------------------

--
-- Table structure for table `invoice`
--

CREATE TABLE IF NOT EXISTS `invoice` (
`id` int(255) NOT NULL,
  `Representive` varchar(100) DEFAULT NULL,
  `RMail` varchar(100) DEFAULT NULL,
  `InvNo` varchar(100) DEFAULT NULL,
  `Company` varchar(100) DEFAULT NULL,
  `Product` text,
  `Service` varchar(100) NOT NULL,
  `PartDescription1` text,
  `Quantity1` varchar(100) DEFAULT NULL,
  `UnitPrice1` varchar(100) DEFAULT NULL,
  `PartDescription2` text,
  `Quantity2` varchar(100) DEFAULT NULL,
  `UnitPrice2` varchar(100) DEFAULT NULL,
  `PartDescription3` text,
  `Quantity3` varchar(100) DEFAULT NULL,
  `UnitPrice3` varchar(100) DEFAULT NULL,
  `PartDescription4` text,
  `Quantity4` varchar(100) DEFAULT NULL,
  `UnitPrice4` varchar(100) DEFAULT NULL,
  `PartDescription5` text NOT NULL,
  `Quantity5` varchar(100) NOT NULL,
  `UnitPrice5` varchar(100) NOT NULL,
  `Validity` varchar(100) NOT NULL DEFAULT '0',
  `Delivery` varchar(100) NOT NULL DEFAULT '0',
  `Payment` varchar(100) NOT NULL DEFAULT '0',
  `SubTotal` varchar(100) NOT NULL DEFAULT '0',
  `SGST` varchar(100) NOT NULL DEFAULT '0',
  `CGST` varchar(100) NOT NULL DEFAULT '0',
  `IGST` varchar(100) NOT NULL DEFAULT '0',
  `GrandTotal` varchar(100) NOT NULL DEFAULT '0',
  `ModificationDetail` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `invoice`
--

INSERT INTO `invoice` (`id`, `Representive`, `RMail`, `InvNo`, `Company`, `Product`, `Service`, `PartDescription1`, `Quantity1`, `UnitPrice1`, `PartDescription2`, `Quantity2`, `UnitPrice2`, `PartDescription3`, `Quantity3`, `UnitPrice3`, `PartDescription4`, `Quantity4`, `UnitPrice4`, `PartDescription5`, `Quantity5`, `UnitPrice5`, `Validity`, `Delivery`, `Payment`, `SubTotal`, `SGST`, `CGST`, `IGST`, `GrandTotal`, `ModificationDetail`) VALUES
(1, 'Korai Purusottam', 'purusottam@navikra.com', 'NTSPL-B001', 'AKAMAI TECHNOLOGIES', 'AWS', 'Installation', 'g', '1', '100000', '', '', '', '', '', '', '', '', '', '', '', '', '', '2', 'Advance', '100000', '9000', '9000', '0', '118000', '2018-02-21 23:34:06');

-- --------------------------------------------------------

--
-- Table structure for table `invoice_upload`
--

CREATE TABLE IF NOT EXISTS `invoice_upload` (
`id` int(255) NOT NULL,
  `Representive` varchar(100) DEFAULT NULL,
  `RMail` varchar(100) DEFAULT NULL,
  `OPFNo` varchar(100) DEFAULT NULL,
  `InvoiceNo` varchar(100) DEFAULT NULL,
  `LicenceDetail` varchar(100) DEFAULT NULL,
  `OtherDetail` text,
  `Product` varchar(100) DEFAULT NULL,
  `FilePath` text,
  `ModificationDetail` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `invoice_upload`
--

INSERT INTO `invoice_upload` (`id`, `Representive`, `RMail`, `OPFNo`, `InvoiceNo`, `LicenceDetail`, `OtherDetail`, `Product`, `FilePath`, `ModificationDetail`) VALUES
(1, 'Korai Purusottam', 'purusottam@navikra.com', 'OPF10', 'INV123', 'sk', 'jksn', 'AWS,ABC', 'INV123.OPF10.test1.csv', '2018-05-17 08:20:37');

-- --------------------------------------------------------

--
-- Table structure for table `leads`
--

CREATE TABLE IF NOT EXISTS `leads` (
`id` int(255) NOT NULL,
  `Representive` varchar(100) NOT NULL,
  `RMail` varchar(100) NOT NULL,
  `CampaignType` varchar(100) NOT NULL,
  `ProjectName` varchar(100) NOT NULL,
  `Date` date NOT NULL,
  `Time` time NOT NULL,
  `Company` varchar(100) NOT NULL,
  `Sector` varchar(100) DEFAULT NULL,
  `ContactPerson` varchar(100) NOT NULL,
  `Designation` varchar(100) NOT NULL,
  `Mobile` varchar(20) NOT NULL,
  `Mail` varchar(100) NOT NULL,
  `Response` varchar(100) NOT NULL,
  `Remarks` text NOT NULL,
  `FollowUp` text NOT NULL,
  `FollowUpDate` date DEFAULT NULL,
  `FollowUpTime` time DEFAULT NULL,
  `Requirement` text NOT NULL,
  `MeetingStatus` varchar(100) DEFAULT NULL,
  `LeadFrom` varchar(100) DEFAULT NULL,
  `AssignedTo` text,
  `LeadMessage` text,
  `ModificationDetail` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `leads`
--

INSERT INTO `leads` (`id`, `Representive`, `RMail`, `CampaignType`, `ProjectName`, `Date`, `Time`, `Company`, `Sector`, `ContactPerson`, `Designation`, `Mobile`, `Mail`, `Response`, `Remarks`, `FollowUp`, `FollowUpDate`, `FollowUpTime`, `Requirement`, `MeetingStatus`, `LeadFrom`, `AssignedTo`, `LeadMessage`, `ModificationDetail`) VALUES
(1, 'Korai Purusottam', 'purusottam@navikra.com', 'Reference', 'aws', '2018-04-16', '16:53:00', 'AKAMAI TECHNOLOGIES', 'IT', 'Guru / Prashant D', 'Manager', '9880599530', 'ggudla@akamai.com', 'Positive', 'sdfsf', 'Yes', '2018-04-17', '14:34:00', 'sfsdfsfsd', 'Yes', 'purusottam@navikra.com', 'All,Technical,Sales,purusottam@navikra.com', 'Hello test1', '2018-04-15 22:05:34'),
(2, '', 'bhargava@rabitasoft.com', 'Direct', 'aws', '2018-04-18', '15:43:00', 'Aditya Birla Minacs IT Services Ltd.', 'IT', 'Suresh ', 'Manager', '9845015367', 'suresh.p@minacs.adityabirla.com', 'Positive', 'EC2', 'Yes', '2018-04-19', '14:13:00', 'AWS', 'Yes', 'puru@gmail.com', 'puru@gmail.com', '', '2018-04-18 11:12:55'),
(3, '', 'bhargava@rabitasoft.com', 'Digital', 'aws', '2018-04-18', '14:43:00', '24/7 Customer Pvt. Ltd.', 'IT', 'Vinayak Relekar', 'Director', '9845972305', 'Vinayak.relekar@247-inc.com', 'Positive', 'fdfsff', 'Yes', '2018-04-18', '15:42:00', 'wdsfs', 'Yes', NULL, NULL, NULL, '2018-04-18 11:27:46'),
(4, 'Korai Purusottam', 'purusottam@navikra.com', 'Reference', 'aws', '2018-04-19', '14:43:00', 'Acer India Pvt. Ltd.', 'IT', 'Senthil Kumar Selvarajan', 'Manager', '9886606745', 'senthil.kumar@acer.com', 'Positive', 'sdfdsf', 'Yes', '2018-04-20', '15:24:00', 'adadas', 'No', NULL, NULL, NULL, '2018-04-18 22:23:22'),
(5, 'Korai Purusottam', 'purusottam@navikra.com', 'Direct', 'aws', '2018-04-19', '15:42:00', 'BROADCOM INDIA PRIVATE LTD', 'IT', 'Mohammed ', 'Manager', '6565596510', 'mrizwan@broadcom.com', 'Positive', 'asdfg', 'Yes', '2018-04-20', '18:56:00', 'asdasf', 'No', NULL, NULL, NULL, '2018-04-18 22:55:22'),
(6, 'Korai Purusottam', 'purusottam@navikra.com', 'Direct', 'aws', '2018-04-19', '15:13:00', 'BROADCOM INDIA PRIVATE LTD', 'IT', 'Mohammed ', 'Manager', '6565596510', 'mrizwan@broadcom.com', 'Positive', 'qwer', 'Yes', '2018-04-20', '17:06:00', 'sadasd', 'No', NULL, NULL, NULL, '2018-04-18 22:58:25'),
(7, 'Puru', 'puru@gmail.com', 'Reference', '', '2018-05-01', '838:59:59', 'A A F India Pvt Ltd', 'Manufacturing', '', 'Manager', '9743377735', 'itsupport@aafindia.net ', 'Positive', '', '', NULL, NULL, '', NULL, NULL, NULL, NULL, '2018-05-01 09:00:05'),
(8, 'Puru', 'puru@gmail.com', 'Reference', '', '2018-05-01', '00:00:00', 'ADA', '', '', '', '*****', 'siddesha@jetmail.ada.gov.in', 'Positive', '', '', NULL, NULL, '', NULL, NULL, NULL, NULL, '2018-05-01 09:03:07'),
(9, 'Raghu Vamsi', 'raghu@navikra.com', 'Direct', 'testemp', '2018-05-07', '17:06:00', 'A K Aerotek Software Centre Pvt Ltd', 'IT', 'C P Parmesh ', 'Associate Manager  ', '9845447244', 'parmesh.cp@akaerotek.com', 'Positive', 'yjgjfj', 'Yes', '2018-05-07', '19:08:00', 'gdgdg', 'Yes', NULL, NULL, NULL, '2018-05-07 06:51:58'),
(10, 'Korai Purusottam', 'purusottam@navikra.com', 'Reference', '', '2018-05-08', '00:00:00', '1point1', 'BPO', '', 'IT Manager', '080-67087209', 'mohammed.fazal@1point1.in', 'Positive', '', '', NULL, NULL, '', NULL, NULL, NULL, NULL, '2018-05-08 10:46:40');

-- --------------------------------------------------------

--
-- Table structure for table `location`
--

CREATE TABLE IF NOT EXISTS `location` (
`id` int(255) NOT NULL,
  `Representive` varchar(100) NOT NULL,
  `RMail` varchar(100) NOT NULL,
  `Company` varchar(255) NOT NULL,
  `Sector` varchar(100) NOT NULL,
  `SubSector` varchar(100) NOT NULL,
  `FirstName` varchar(100) NOT NULL,
  `LastName` varchar(100) NOT NULL,
  `Level` varchar(100) NOT NULL,
  `Dept` varchar(100) NOT NULL,
  `Designation` varchar(100) NOT NULL,
  `Mobile` varchar(20) NOT NULL,
  `Mail` varchar(100) NOT NULL,
  `ContactPerson2` varchar(100) NOT NULL,
  `ContactNumber` varchar(20) NOT NULL,
  `EmailID` varchar(100) NOT NULL,
  `Url` text NOT NULL,
  `Address` text NOT NULL,
  `Location` text NOT NULL,
  `SubLocation` text NOT NULL,
  `City` varchar(100) NOT NULL,
  `Pin` int(6) NOT NULL,
  `State` varchar(100) NOT NULL,
  `StdCode` int(20) DEFAULT NULL,
  `LandlineNo` varchar(20) NOT NULL,
  `FaxNo` text NOT NULL,
  `NoOfEmployees` varchar(100) DEFAULT NULL,
  `CompanyType` varchar(100) DEFAULT NULL,
  `GSTNo` varchar(100) DEFAULT NULL,
  `ModificationDetail` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=1115 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `location`
--

INSERT INTO `location` (`id`, `Representive`, `RMail`, `Company`, `Sector`, `SubSector`, `FirstName`, `LastName`, `Level`, `Dept`, `Designation`, `Mobile`, `Mail`, `ContactPerson2`, `ContactNumber`, `EmailID`, `Url`, `Address`, `Location`, `SubLocation`, `City`, `Pin`, `State`, `StdCode`, `LandlineNo`, `FaxNo`, `NoOfEmployees`, `CompanyType`, `GSTNo`, `ModificationDetail`) VALUES
(2, '', '', '1point1', 'BPO', '', 'Mohammed Fazal', '', '', 'Information Technology', 'IT Manager', '080-67087209', 'mohammed.fazal@1point1.in', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(3, '', '', '24/7 Customer Pvt. Ltd.', 'IT', '', 'Vishal', 'singh', '', 'Information Technology', 'Manager', '9901877911', 'vishal.singh@247-inc.com', '', '', '', 'www.247-inc.com', 'Embassy Golf Link', '', 'Domlur', 'Bangalore', 560071, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(4, '', '', '24/7 Customer Pvt. Ltd.', 'IT', '', 'Vinayak', 'Relekar', '', 'Information Technology', 'Director', '9845972305', 'Vinayak.relekar@247-inc.com', '', '', '', 'www.247-inc.com', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(5, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(6, '', '', '3i Infotech', 'ITES', '', 'Logaraj', 'M', '', 'Information Technology', 'Sr Manager', '9845972305', 'logaraj.m@3i-infotech.com', '', '', '', '3i-infotech.com', 'Embassy Golf Link', '', 'Domlur', 'Bangalore', 560071, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(7, '', '', '3M India', 'Manufacturing', '', 'Murali', 'puttaraj', '', 'Information Technology', 'Manager', '9008712167', 'pmurali@mmm.com', '', '', '', 'www.3mindia.in', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(8, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(9, '', '', 'A A F India Pvt Ltd', 'Manufacturing', '', 'Anand', '', '', 'Information Technology', 'Manager', '9743377735', 'itsupport@aafindia.net ', '', '', '', 'www.aafasia.com', 'Plot No 117 & 118, Bommasandra Jigini Link Road  KIADB Industrial Area, Bande Nallasandra Village Hadapsar ', '', 'Bommasandra', 'Bangalore ', 560105, 'Karnataka', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(10, '', '', 'A K Aerotek Software Centre Pvt Ltd', 'IT', '', 'C P', 'Parmesh ', '', 'Information Technology', 'Associate Manager  ', '9845447244', 'parmesh.cp@akaerotek.com', '', '', '', 'www.ak-aerotek.com', 'No. 8/1, Dr. M. H. Marigowda Road\n(Hosur Road - near Brand Factory)\nWilson Garden', '', 'Wilson Garden', 'Bangalore ', 560027, 'Karnataka', 80, '67148200', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(11, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(12, '', '', 'Aarbee Structures', '', '', 'Muralidharan', '', '', '', '', '9986673000', 'murali@aarbeesteeldetail.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(13, '', '', 'Aaron Lights Private Limited ', '', '', 'Sathya Narayan Roa', '', '', '', 'IT Manager', '9845022954', 'rao@aaronlights.com\n\n', '', '', '', 'www.aaronlights.com\n\n', '1/1 1st floor Middle School Cross Road, VVPU Ram,Bangalore,Karnataka-560004', 'Bangalore', '', 'Bangalore', 0, '', 0, '80-65300193/2', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(14, '', '', 'AB Mauri India Pvt Ltd', 'Manufacturing', '', 'Ramchandran', '-', '', 'Head - IT', '', '-', 'ramachandran.parasuraman@abmauri.com', '', '', '', 'www.abmauri.in', 'Plot No 117 & 118, Bommasandra Jigini Link Road  KIADB Industrial Area, Bande Nallasandra Village Hadapsar ', '', 'Bommasandra', 'Bangalore ', 560105, 'Karnataka', 80, '30798080', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(15, '', '', 'ABB Global Industries & Services Ltd', 'Manufacturing', '', 'Ramakrishna TS', '', '', '', '', '9972583917', 'ramakrishna.ts@in.abb.com', '', '', '', '', 'Plot No. 5 & 6, 2nd Phase', '', 'Peenya Industrial Area', 'Bangalore', 560058, 'Karnataka', 80, '22949486', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(16, '', '', 'ABB Global Industries & Services Ltd.', 'Manufacturing', '', 'Venkatachalam Prasad', '', '', 'Project Manager', '', '9972748459', 'prasad.venkatachalam@in.abb.com', '', '', '', '', 'Plot No. 4A, 5 & 6, 2nd Phase', '', 'Peenya Industrial Area', 'Bangalore', 560058, 'Karnataka', 80, '22949645', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(17, '', '', 'ABB Global Industries & Services Ltd.', 'Manufacturing', '', 'Balamurugan Veluchamy', '', '', 'Manager - Project Management & Networking Management', '', '9972583918', 'balamurugan.v@in.abb.com', '', '', '', '', 'Bhoruka Tech Park, Block 1', 'Whitefield Road', 'Mahadevapura', 'Bangalore', 560048, 'Karnataka', 80, '42069950', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(18, '', '', 'ABB Global Industries & Services Ltd.', 'Manufacturing', '', 'Ganesh', '', '', 'Manager - Product Delivery', '', '9845325823', 'ganesh.n.rapelli@in.abb.com', '', '', '', '', 'Plot No. 4A, 5 & 6, 2nd Phase', '', 'Peenya Industrial Area', 'Bangalore', 560048, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(19, '', '', 'ABB Global Industries & Services Ltd.', 'Manufacturing', '', 'Balamurugan', '', '', 'Manager - Project Management & Networking Management', '', '9972583918', 'balamurugan.v@in.abb.com', '', '', '', '', 'No.19/5 & 19/6, 4th Floor, Kareem Towers', 'Cunningham Road', '', 'Bangalore', 560052, 'Karnataka', 80, '22343434 / 44444445', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(20, '', '', 'ABB Global Industries & Services Ltd.', 'Manufacturing', '', 'Sethu Murugan', '', '', 'Manager - Test & Delivery ', '', '9880493208', 'sethu.s.murugan@in.abb.com', '', '', '', '', 'Plot No. 4A, 5 & 6, 2nd Phase', '', 'Peenya Industrial Area', 'Bangalore', 560058, 'Karnataka', 80, '22949645', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(21, '', '', 'ABB Global Industries & Services Ltd.', 'Manufacturing', '', 'Nandkumar', 'Basarikatti', 'Manager Level', 'Technical', 'Manager', '9845443684', 'nandkumar.lb@in.abb.com', '', '', '', '', 'Bhoruka Tech Park, Block 1', '', 'Mahadevapura', 'Bangalore', 560066, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(22, '', '', 'ABB Global Industries & Services Ltd.', 'Process Automation', '', 'Sethu', 'Murugan', '', '', 'Manager - Test & Delivery ', '9880493208', 'sethu.s.murugan@in.abb.com', '', '', '', '', 'Plot No. 5 & 6, 2nd Phase', '', 'Peenya Industrial Area', 'Bangalore', 560071, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(23, '', '', 'ABB INDIA LIMITED', 'Manufacturing', '', 'Arun B E', '', '', '', '', '9686843163', 'arun.be@in.abb.com', '', '', '', 'www.altisource.com', 'No. 43, Block B3 & B4, Velankani Tech Park', '', '1st Phase, Electronic City ', 'Bangalore', 560100, 'Karnataka', 80, '66991700', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(24, '', '', 'ABB India Ltd.', 'Manufacturing', '', 'Lakshman', 'K N', 'GM Above', 'Technical', 'Assistant Vice President', '9901491172', 'kn.lakshman@in.abb.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(25, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(26, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(27, '', '', 'Accenture Services PVT Ltd', '', '', 'Naveenkumar CC', '', '', '', '', '9844084166', 'naveenkumar.cc@in.abb.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(28, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(29, '', '', 'Acer India Pvt. Ltd.', 'IT', '', 'Lakshmikanth', 'Bhat', 'Manager Level', 'Technical Support', 'Manager', '9741988150', 'lakshmikanth.bhat@acer.com / kkanth24@hotmail.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(30, '', '', 'Acer India Pvt. Ltd.', 'IT', '', 'Senthil Kumar', 'Selvarajan', 'Manager Level', 'MIS', 'Manager', '9886606745', 'senthil.kumar@acer.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(31, '', '', 'Actavis Pharma Development Centre Pvt Ltd (Allergan)', '', '', ' Chandra Shekhar', '', '', '', 'IT Manager', '91 80 56096171', 'chandra.shekhar@actavis.com\n\n', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(32, '', '', 'Actiance India Pvt. Ltd', 'IT', '', 'Mimit', 'Mehta', '', 'Information Technology', 'Manager', '9485380690', 'MMehta@actiance.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(33, '', '', 'ADA', '', '', 'Shri H Siddesha, Outstanding Scientist - PD(LCA-Mk2,TD(P))', '', '', '', '', '*****', 'siddesha@jetmail.ada.gov.in', '', '', '', 'www.actavis.com\n\n', 'No.15, 80 Feet Road, Koramangala, Bangalore 560 095, India.', 'Bangalore', '', 'Bangalore', 0, '', 0, '91 80 25706658', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(34, '', '', 'ADA', '', '', 'Shri G Ramakrishnan, Sc/Engr G - TD(ICT)', '', '', '', '', '9844634629', 'ramki@jetmail.ada.gov.in', '', '', '', 'www.actavis.com\n\n', 'No.15, 80 Feet Road, Koramangala, Bangalore 560 095, India.', 'Bangalore', '', 'Bangalore', 0, '', 0, '91 80 25706658', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(35, '', '', 'ADA', '', '', 'Dr P Selvaraj - TD (PS & P)', '', '', '', '', '*****', 'selva@jetmail.ada.gov.in', '', '', '', '', 'Plot #1-4, Survey 5/2, 15KM Stone Sigasandara Post, Bartena Agrahar Hosur Road Bangalore 560 100 India ', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(36, '', '', 'Adadyn Technology Private Limited', '', '', 'Anbu', '', '', '', 'IT Head', '91 8867541137', 'it@addyan.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(37, '', '', 'ADE', '', '', 'R.Chandrashekar – Sc G Flight simulators', '', '', '', '', '25057671', 'rcsekar@ade.drdo.in', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(38, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(39, '', '', 'Aditya Birla Group', 'Manufacturing', '', 'Arkalgud', '', '', 'Assistant Manager - Delivery', '', '9483161341', 'sheshadri.a@adityabirla.com', '', '', '', 'www.adadyn.com', '3rd Floor, KMJ Arcadia, Industrial Main Road 5th Block, Koramangala, Bengaluru 560095 ', 'Bangalore', '', 'Bangalore', 0, '', 0, '91 8041558080 415580', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(40, '', '', 'Aditya Birla Minacs IT Services Ltd.', 'IT', '', 'Suresh', '', '', 'Manager - Technical', '', '9845015367', 'suresh.p@minacs.adityabirla.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(41, '', '', 'Aditya Birla Minacs IT Services Ltd.', 'IT', '', 'Suresh', '', '', 'Manager - Technical', '', '9845015367', 'suresh.p@minacs.adityabirla.com', '', '', '', '', 'Cheyyar Taluk, Melmaruvathur,Kanchipuram Dist., ', '', '', 'Tamilnadu - 603319', 0, '', 0, 'Phone No: 04115 – 22', 'Fax No: 04115 – 229 247', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(42, '', '', 'Aegis Ltd.', 'BPO', '', 'Prasanna', 'Aithal', 'Manager Level', 'Software Development', 'Assistant Manager', '9972333229', 'aithalprasanna@yahoo.com / prasanna.aithal@aegisglobal.com', '', '', '', '', 'Plot No. 5 & 6, 2nd Phase', '', 'Peenya Industrial Area', 'Bangalore', 560071, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(43, '', '', 'Aeronautical Development Agency', 'Defence Labs', '', 'S L ', 'Aneesh', '', 'Dy System Administrator', '', '9916077838', 'aneeshsl@gmail.com', '', '', '', '', 'Gopalan Tower, Millenium, Near CMR', '', 'Brookefield', 'Bangalore', 560100, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(44, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(45, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(46, '', '', 'Airvana Networks India Pvt. Ltd.', 'Telecom', '', 'Girish', '', '', 'Manager - Software Configurations', '', '9886762896', 'gkallanagoudar@airvana.com', '', '', '', '-', 'Systems Block Kodihalli Airport Road', 'Dodda Nekkundi', '', '', 560017, 'Karnataka', 0, '-', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(47, '', '', 'Airvana Networks India Pvt. Ltd.', 'Telecom', '', 'Chandra Shekar', '', '', 'Senior Manager - Program Management', '', '9686452624', 'cloganathan@airvana.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(48, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(49, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(50, '', '', 'AKAMAI TECHNOLOGIES', 'IT', '', 'Guru / Prashant', 'D', '', 'Information Technology', 'Manager', '9880599530', 'ggudla@akamai.com', '', '', '', '', '1st Floor, B Wing, Block A, Salarpuria Softzone, Varthur Hobli', '', 'Bellandur', 'Bangalore', 560100, 'Karnataka', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(51, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(52, '', '', 'Allegis Group India', '', '', ' J.Samson', '', '', '', '', '9916292268', 'jsamson@allegisgroup.com', '', '', '', '', 'No. 253/1, 11th Main, 3rd Phase,\nPeenya Industrial Area,', 'Peenya Industrial Area', '', 'Bangalore', 560058, 'Karnataka', 80, '67200082 ', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(53, '', '', 'Allegis Group India', '', '', 'R.Prasad', '', '', '', '', '9880095047', 'rprasad@allegisgroup.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(54, '', '', 'Allergan India Pvt Ltd', 'Pharma', '', 'Karthi', 'V', '', 'Information Technology', 'Manager', '9341806000', 'v_karthi@allergan.com', '', '', '', '', 'Centre For Womens Studies,Allagappa University,Karaikudi,', '', '', 'Tamil nadu', 0, '', 0, '04565-225205', '04565-225202', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(55, '', '', 'Alliance Business Academy', 'Education', 'Higher', 'santosh ', 'K', '', 'Systems and Networking', 'Manager', '9880765253', 'santosh.k@alliance.edu.in', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(56, '', '', 'ALLIANZ MANAGED OPERATIONS &', '', '', 'Rarish Rajan', '', '', '', '', '9881739980', 'RARISH.RAJAN@ALLIANZ.COM', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(57, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(58, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(59, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(60, '', '', 'Altimetrik India Pvt Ltd', '', '', 'Rajshkar', '', '', '', '', '9845288362', 'rthimmaiah@altimetrik.com', '', '', '', '', '16, III Cross Street, West CIT Nagar', '', '', 'Chennai – 600 035.', 0, '', 0, 'Phone: 044 – 2432 34', 'Fax.No: 044 – 2436 1204', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(61, '', '', 'Altimetrik India Pvt Ltd', '', '', 'Rajshkar', '', '', '', '', '9845288362', 'rthimmaiah@altimetrik.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(62, '', '', 'Altiostar Networks', 'Manufacturing', 'Telecom', 'Amit A Atharkar', '', '', '', '', '9900013819', 'aatharkar@altiostar.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(63, '', '', 'Altisource Business Solutions Pvt. Ltd.', 'IT ', 'Real Estate', 'Amith', 'Srinath', 'Manager Level', 'Information Technology', 'Project Manager', '9886636539', 'amith.srinath@altisource.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(64, '', '', 'Altisource Pvt. Ltd.', 'Information Technology', '', 'Sanjay Gore', '', '', 'Manager - IT & Release Management', '', '9686259003', 'sanjay.gore@altisource.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(65, '', '', 'Altisource Pvt. Ltd.', 'Information Technology', '', 'Sanjay', 'Gore', '', '', 'Manager - IT & Release Management', '9686259003', 'sanjay.gore@altisource.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(66, '', '', 'Altisource Pvt. Ltd.', 'Information Technology', '', 'Vivek', 'Mathur', '', '', 'Director - Quality Engineering', '9845277883', 'vivek.mathur@altisource.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(67, '', '', 'Altisource Pvt. Ltd.', 'IT services', '', 'Vivek mathur', '', '', 'Director - Quality Engineering', '', '9845277883', 'vivek.mathur@altisource.com', '', '', '', 'www.altisource.com', '1st Floor, B Wing, Block A, Salarpuria Softzone, Varthur Hobli', '', 'Bellandur', 'Bangalore', 560100, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(68, '', '', 'Altisource Pvt. Ltd.', '', '', 'Vivek', '', '', 'Director - Quality Engineering', '', '9845277883', 'vivek.mathur@altisource.com', '', '', '', '', 'No. 43, Block B3 & B4, Velankani Tech Park', '', '1st Phase, Electronic City ', 'Bangalore', 560100, 'Karnataka', 80, '66991700', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(69, '', '', 'Altisource Pvt. Ltd.', '', '', 'Vivek mathur', '', '', 'Director - Quality Engineering', '', '9845277883', 'vivek.mathur@altisource.com', '', '', '', '', 'No. 43, Block B3 & B4, Velankani Tech Park', '', '1st Phase, Electronic City ', 'Bangalore', 560100, 'Karnataka', 80, '66991786', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(70, '', '', 'Altisource Pvt. Ltd.', '', '', 'Sanjay Gore', '', '', 'Manager - IT & Release Management', '', '9686259003', 'sanjay.gore@altisource.com', '', '', '', '', 'Plot No. 65/2, 4th Floor, Bagmane Tridib, Bagmane Tech Park', '', 'C V Raman Nagar', 'Bangalore', 560038, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(71, '', '', 'Altran Technologies India Pvt. Ltd.', '', '', 'Lokesha', 'Shivaramu', 'Manager Level', 'Information Technology', 'Unit Manager - Technical', '8951876645', 'lokesha.shivaramu@altran.com', '', '', '', '', 'No. 43, Block B3 & B4, Velankani Tech Park', '', '1st Phase, Electronic City ', 'Bangalore', 560008, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(72, '', '', 'Amadeus', '', '', 'Bibin Vaidyan', '', '', '', '', '9902466199', 'bibin.vaidyan@amadeus.com', '', '', '', '', 'Brookefield', 'Kundanahalli Road', 'Whitefield', 'Bangalore', 560037, 'Karnataka', 80, '67435530', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(73, '', '', 'Amadeus', '', '', 'Hariprakash Logan', '', '', '', '', '9986837742', 'hariprakash.logan@amadeus.com', '', '', '', 'www.altisource.com', 'No. 43, Block B3 & B4, Velankani Tech Park', '', '1st Phase, Electronic City ', 'Bangalore', 560100, 'Karnataka', 80, '66991786', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(74, '', '', 'AMADEUS SOFTWARE LABS INDIA PVT LTD', '', '', 'Gaurav Pandey', '', '', '', '', '8861205434', 'gaurav.pandey@amadeus.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(75, '', '', 'Amagi Media Labs', '', '', 'Venkataramana', '', '', '', '', '9108553418', 'Venkataramana@amagi.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(76, '', '', 'Amagi Media Labs Pvt. Ltd.', '', '', 'Navneeth', '', '', '', 'IT Manager', '91 9742960363', 'itsupport@amagi.com\n\n\n', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(77, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(78, '', '', 'Amba Research', '', '', 'Sridhar', 'Damala', '', '', '', '9845002618', 'sridhard@ambaresearch.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(79, '', '', 'Amber Road ', '', '', 'Nishanth', 'Abraham', '', 'Information Technology', 'Manager', '8067334197', 'NishanthAbraham@AmberRoad.com ', '', '', '', '', 'C/O.Agility Logistics Pvt. Ltd. Shed No.8, 18 Km Old Ma Dras Road Virgo Nagar Post Bangalore 560 049 India ', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(80, '', '', 'AMI CORP INDIA ', '', '', 'Sayed', 'Ehasn', '', 'Information Technology', 'Manager', '9880417589', 'S.Ehsan@amicorp.com', '', '', '', '', 'C/O.Agility Logistics Pvt. Ltd. Shed No.8, 18 Km Old Ma Dras Road Virgo Nagar Post Bangalore 560 049 India ', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(81, '', '', 'ANDRITZ Technologies Pvt. Ltd. ', '', '', 'Ramesh Babu', '', '', '', 'Head IT', '91 9686198206', 'ramesh.babu@andritz.com\n\n', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(82, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(83, '', '', 'Anomishere Design Company Private Limited ', '', '', 'Raju CP', '', '', '', 'IT Head', '91 9535327533', 'raju@anomishere.com', '', '', '', '', 'AI-9, Off 9th Main Road', '', '', 'Anna Nagar, Chennai-40', 0, '', 0, '044-26212089', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(84, '', '', 'Ansaldo STS Transportation Systems India Pvt. Ltd.', '', '', 'KRISHNA KUMAR', 'Nthangavel', '', 'Information Technology', 'Manager', '9845922878', 'krishnakumar.nthangavel@ansaldo-sts.co.in', '', '', '', '', '', '', '', 'Chennai – 600 025', 0, '', 0, 'Phone:044-22203260', 'Fax:044-22351956', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(85, '', '', 'ANZ Operation & Technology India Pvt. Ltd.', '', '', 'Kishore', 'B', 'Manager Level', '', 'Consultant', '9243524245', 'kishore.b@anz.com', '', '', '', '', 'Annamalai University \n\n', '', '', 'parangipettai-608502,Tamil Nadu', 0, '', 0, '04144-243223', '04144-243200', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(86, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(87, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(88, '', '', 'Apollo Hospitals', '', '', 'Venkatesh', 'Joshi', '', 'Information Technology', 'Director', '9900236812', 'venkatesha_joshi@apollohospitals.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(89, '', '', 'Apollo Hospitals', '', '', 'Prajith T', '', '', 'Information Technology', 'IT Manager', '9902366660', 'prajith_t@apollohospitals.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(90, '', '', 'Appnomic Systems Pvt. Ltd.', '', '', 'Vijay', 'Daniel', 'Manager Level', 'Information Technology', 'Manager', '9663390045', 'vijay.s@appnomic.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(91, '', '', 'Ariba ', '', '', 'Shantharam', '', '', '', '', '9945 746666', 'shantharam.handa@sap.com', '', '', '', '', '154/11,,Opp IIM, Bannerghatta Road, Bangalore – 560076 India', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(92, '', '', 'ARIBA TECHNOLOGIES INDIA PVT LTD', '', '', 'Ponnappa Ithichanda, ', '', '', '', '', '9845061227', 'ponnappa.ithichanda@sap.com', '', '', '', '', '154/11,,Opp IIM, Bannerghatta Road, Bangalore – 560076 India', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(93, '', '', 'Aricent Technologies Pvt. Ltd.', '', '', 'Jitendra Pal', 'Thethi', 'GM Above', '', 'Assistant Vice President - Technology', '9620224927', 'jitendra.thethi@aricent.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(94, '', '', 'Aris Global Software Pvt. Ltd.', '', '', 'Keshavan Babu', '', '', 'Manager - IT Delivery', '', '9945696000', 'keshavan@arisglobal.co.in', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(95, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(96, '', '', 'Arista Networks India Pvt.Ltd', '', '', 'Mary Jude', '', '', '', '', '8105631319', 'mary@arista.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(97, '', '', 'Arista Networks India Pvt.Ltd', '', '', 'Kuldip', '', '', '', '', '9538000096', 'kuldip@arista.com', '', '', '', '', 'No. 43, Block B3 & B4, Velankani Tech Park', '', '1st Phase, Electronic City ', 'Bangalore', 560008, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(98, '', '', 'Arista Networks India Pvt.Ltd', '', '', 'Anudip', '', '', '', '', '9916214205', 'anudip@arista.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(99, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(100, '', '', 'Arvind Limited', '', '', 'Ezhil Arasan', '', '', '', 'IT Manager', '91 9980754748', 'ezhil.arasan@arvindexports.com\n\n', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(101, '', '', 'ARYAKA NETWORKS INC', '', '', 'Priyam Nayak (Senior Associate – Finance)', '', '', '', '', '8951806574', 'priyam@aryaka.com', '', '', '', 'www.arvind.com\n\n', '#26/2 ,27/2, Kenchenahalli, Mysore Road, Near Bangalore University, Bangalore – 560 059', 'Bangalore', '', 'Bangalore', 0, '', 0, '91- 80-33719000', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(102, '', '', 'ARYAKA NETWORKS INC', '', '', 'Ashok Malve', '', '', '', '', '900095800', 'amalve@aryaka.com ', '', '', '', 'www.arvind.com\n\n', '#26/2 ,27/2, Kenchenahalli, Mysore Road, Near Bangalore University, Bangalore – 560 059', 'Bangalore', '', 'Bangalore', 0, '', 0, '91- 80-33719000', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(103, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(104, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(105, '', '', 'Ashirvad Pipes Private Limited', '', '', 'Mahesh', '', '', '', 'IT Manager', '91 9880055488', 'mahesh.biradar@ashirvad.com\n\n', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(106, '', '', 'ASHIRVAD PIPES PVT. LTD.', '', '', 'Virupakshappa Angadi ', '', '', '', '', '9620619508', 'angadi.vc@ashirvad.com', '', '', '', 'www.ashirvad.com\n\n', '4-B, Attibele Industrial Area, Hosur Road,Bangalore - 562 107,Karnataka. India.', 'Bangalore', '', 'Bangalore', 0, '', 0, '91 80 2782 0542', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(107, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(108, '', '', 'Aster DM Health care', '', '', 'Santhosh kumar  ', '', '', '', 'IT Manager', '91 9900591275', 'santhosh.kumar@dmhealthcare.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(109, '', '', 'Astri sales & Service pvt ltd.', '', '', 'Rakesh', '', '', '', 'IT Manager', '91-8050753713', 'rkgupta@astrissp.com\n\n', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(110, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(111, '', '', 'Atos India Pvt. Ltd.', 'IT services', '', 'Bhuvan Girish', '', '', 'Head - Business', '', '9886880955', 'bhuvan.girish@atos.net', '', '', '', 'www.astrissp.com\n\n', 'No.67, 515, Command Colony, New Thippasandra, H A L 3rd Stage, New Thippasandra, Bengaluru, Karnataka 560075.', 'Bangalore', '', 'Bangalore', 0, '', 0, '91 80 2521 6206', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(112, '', '', 'Atos India Pvt. Ltd.', 'IT services', '', 'Bhuvan', 'Girish', '', '', 'Head - Business', '9886880955', 'bhuvan.girish@atos.net', '', '', '', 'www.astrissp.com\n\n', 'No.67, 515, Command Colony, New Thippasandra, H A L 3rd Stage, New Thippasandra, Bengaluru, Karnataka 560075.', 'Bangalore', '', 'Bangalore', 0, '', 0, '91 80 2521 6206', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(113, '', '', 'Atos India Pvt. Ltd.', 'IT services', '', 'Jijo', 'Mohan', '', '', 'Manager - Operations', '9902691146', 'jijo.mohank@atos.net', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(114, '', '', 'Atos India Pvt. Ltd.', '', '', 'Murali', '', '', 'General Manager - IT', '', '9986066632', 'muralisathyanarayana@atos.net', '', '', '', '', 'Brookefield', 'Kundanahalli Road', 'Whitefield', 'Bangalore', 560037, 'Karnataka', 80, '67435530', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(115, '', '', 'Atos India Pvt. Ltd.', '', '', 'Kaushik', 'Ray', 'Manager Level', 'Information Technology', 'Senior Manager', '9980496693', 'kaushik.ray@atos.net', '', '', '', '', 'Brookefield', '', 'Whitefield', 'Bangalore', 560032, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(116, '', '', 'Aujas Networks', '', '', 'Anuraj', '', '', 'Information Technology', 'Manager', '7204722095', 'anuraj.c@aujas.com', '', '', '', 'www.borderlessaccess.com', 'No. 29/2, Agrahara Village, Vrindavan Tech Village, Building No. 2 A, East Tower', '', 'Vathur Hobli', 'Bangalore', 560037, 'Karnataka', 80, '49313852', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(117, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(118, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(119, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(120, '', '', 'Axa Technologies Shared Services Pvt. Ltd.', '', '', 'Sanjeev', '', '', 'Senior Manager - Delivery', '', '8861016699', 'sanjeev.jha@axa-tss.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(121, '', '', 'Axis Cades', '', '', 'Sidram', '', '', '', '', '9986255566', 'sidram.s@axis.aero', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(122, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(123, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(124, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(125, '', '', 'Ayurwin Pharma Pvt Ltd', '', '', 'Vinay', '', '', 'IT Head', '', '91-9379622555', 'vinay@ayurwin.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(126, '', '', 'Azim Premji Foundation', '', '', 'Mithun', 'Bellary', 'Manager Level', 'Procurement', 'Manager', '7760698899', 'mithun.bellary@azimpremjifoundation.org', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(127, '', '', 'B.A. HOSPITAL & TRAINING CENTRE', '', '', 'Suresh Adiga .', '', '', '', '', '9945542864', 'suresh_padigara@yahoo.com, bahospital.thumbay@gmail.com', '', '', '', '', 'No-1094, 2nd Floor, 19th Main Road, Near Navrang Theater, Block 1, Rajajinagar, 1st Block, Rajaji Nagar, Bengaluru- 560010', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(128, '', '', 'Back Office Associates', '', '', 'Srinivas    ', '', '', '', 'IT Head', '91 9645006336', 'srinivaspendyala@boaweb.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(129, '', '', 'Back Office Associates', '', '', 'Srinivas    ', '', '', '', 'IT Head', '91 9645006336', 'srinivaspendyala@boaweb.com', '', '', '', '', 'No-1094, 2nd Floor, 19th Main Road, Near Navrang Theater, Block 1, Rajajinagar, 1st Block, Rajaji Nagar, Bengaluru- 560010', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(130, '', '', 'Baehal Pvt Ltd', '', '', 'Kishore', '', '', '', '', '080 2522 1263', 'bahospital.thumbay@gmail.com', '', '', '', 'www.bahospitalthumbay.com', 'THUMBAY - 574170, Mangalore (D.K.) Karnataka State', '', '', 'spoke to mr kishore ad mr mathew he told us to send the profile regarding at the following mail id b', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(131, '', '', 'BAeHAL Software Ltd.', '', '', 'Prasad', 'Kumar', 'Below Manager', 'Procurement', 'Senior Executive', '9886000773', 'prasad.kumar@baehal.com', '', '', '', 'www.boaweb.com', 'Innova Pearl, No. 17 Koramangala Industrial Layout,, 5th Block, Koramangala, Bengaluru, Karnataka 560034', 'Bangalore', '', 'Bangalore', 0, '', 0, '  91 80 671 97400 ', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(132, '', '', 'Bagmane Developer', '', '', 'Narendra kumar', '', '', 'Information Technology', 'Manager', '9844847725', 'narendra@bagmanegroup.com', '', '', '', 'www.boaweb.com', 'Innova Pearl, No. 17 Koramangala Industrial Layout,, 5th Block, Koramangala, Bengaluru, Karnataka 560034', 'Bangalore', '', 'Bangalore', 0, '', 0, '  91 80 671 97400 ', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(133, '', '', 'Bangalore Stock Exchange', '', '', 'Nagaraj', '', '', 'Information Technology', 'VP', '9845063309', 'vp_systems@bfsl.co.in', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(134, '', '', 'BEL', '', '', 'Rajesh', '', '', '', '', '9731480703', 'rajeshhs@bel.co.in', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(135, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(136, '', '', 'BGSE', '', '', 'Nagaraj', '', '', '', 'VP-Systems', '9845063309', 'vp_systems@bfsl.co.in', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(137, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(138, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(139, '', '', 'Bharat Electronics', '', '', 'Rajesh', '', '', '', '', '9731480703', 'rajeshhs@bel.co.in', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(140, '', '', 'Bharat Electronics limited', '', '', 'Devesh Kumar', '', '', '', 'IT Head', '91-9980923168', 'deveshkumarsingh@bel.co.in\n\n', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(141, '', '', 'BHARAT ELECTRONICS LTD.', '', '', 'Anoop N Jamboor', '', '', 'Dy Manager Procurement', '', '080-28383233', 'anoopnjamboor@bel.co.in', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(142, '', '', 'BHARAT ELECTRONICS LTD.', '', '', 'Shiva Kumar', '', '', '', '', '9886307936', 'shivakumarm@bel.co.in', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(143, '', '', 'BHARAT ELECTRONICS LTD.', '', '', 'Nagabushanam P J (Mgr/Pur/CMM)', '', '', '', '', '080-22195448', 'nagabushanampj@bel.co.in', '', '', '', 'www.bel-india.com\n\n', 'Bharat Electronics limited corporate office outer ring road Nagavara Road Bangalore(Karnataka) 560025', 'Bangalore', '', 'Bangalore', 0, '', 0, '91 80 22195839 / 91 ', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(144, '', '', 'Bharat Fritz Werner Limited', '', '', 'Vijay Kumar', '', '', '', 'IT Head', '91 9019126239', 'vijaykumar@bfw.co.in\n\n', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(145, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(146, '', '', 'Bhive Workspace', '', '', 'Irshad', '', '', 'Sales', 'Location head', '9108006844', 'irshad@bhiveworkspace.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(147, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(148, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(149, '', '', 'BL Kashyap & Sons Limited', '', '', 'Devendra ', 'Yadav', '', 'Manager - Systems', '', '9341220740', 'devendra@blkashyap.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(150, '', '', 'Black & Decker', '', '', 'Phanindra Kari', '', '', '', '', '9900039851', 'phanindra.karri@sbdinc.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(151, '', '', 'Blackbuck', '', '', 'chandru', '', '', 'Information Technology', 'Manager', '080 3966 1888', 'chandru@blackbuck.com', '', '', '', 'www.blkashyap.com', 'No 25&30 1st Phase', '', 'Marathalli', '', 560037, 'Karnataka', 0, '-', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(152, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(153, '', '', 'Bloom Energy', '', '', 'Suhas Pandit', '', '', '', '', '8040434242', 'suhas.pandit@bloomenergy.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(154, '', '', 'Blue Star Infotech Ltd', '', '', 'Satyan Warrier', '', '', 'Information Technology', 'Head', '9886624038', 'satyan.warrier@bsil.com', '', '', '', '', 'D- 109, LBR Complex, 1st Main Road, Anna Nagar', '', '', 'Chennai – 600 102.', 0, '', 0, 'Phone: 044 – 2663 29', 'Fax. No: 044 2663 2977', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(155, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(156, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(157, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(158, '', '', 'Borderless Access Panels Pvt. Ltd.', 'Market Research', '', 'Lakshmisha', 'S', '', '', 'Account Director', '9342055058', 'lakshmisha.s@gmail.com / lakshmisha.s@borderless.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(159, '', '', 'Borqs Software Solutions Pvt Ltd.', '', '', 'Latha', '', '', '', 'IT Manager', '91 80-4334 7100', 'latha.mv@borqs.com', '', '', '', 'www.brillio.com', 'Prestige Tech Park, Vathur Hobli', '', 'Marathahalli', 'Bangalore', 560080, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(160, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(161, '', '', 'Brigade Enterprises Ltd', 'Real Estate', '', 'Shivaraj', '', '', 'Assistant Manager', '', '9008302783', 'shivaraj@brigadegroup.com', '', '', '', 'www.amagi.com', '# #3 Edward Road, Vasanth Nagar, Bengaluru, Karnataka 560051', 'Bangalore', '', 'Bangalore', 0, '', 0, '91 80-4334 7100', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(162, '', '', 'Brillio Technologies Pvt Ltd ', '', '', 'Mohan Mathew', '', '', '', 'Manager-IT', '9986737564', 'mohan.mathew@brillio.com', '', '', '', '', 'MAC House" 3rd Floor, No-4,Sardar Patel Road,Guindy', '', '', 'Chennai-600032', 0, '', 0, '044-22200890', '044-22300194', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(163, '', '', 'Brillio Technologies Pvt. Ltd.', 'IT', '', 'Shakeel Ahmed Somankop', '', '', 'Lead - IT Infrastructure', '', '9986191994', 'shakeelas@brillio.com', '', '', '', '', 'No 563, Niran Arcade  New BEL Road', '', 'Peenya Indl Area', '', 560058, 'Karnataka', 0, '40338833', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(164, '', '', 'Brillio Technologies Pvt. Ltd.', 'IT', '', 'Ramu', '', '', 'Manager - IT Delivery', '', '8884200119', 'ramu.malepati@brillio.com', '', '', '', 'www.brillio.com', '#58,1st Main Road, Mini Forest JP Nagar, 3rd Phase Bangalore / Bengaluru Karnataka , 560078 ', 'Bangalore', '', 'Bangalore', 0, '', 0, '(080) 40136100 ', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(165, '', '', 'Brillio Technologies Pvt. Ltd.', 'Software', '', 'Shakeel Ahmed', 'Somankop', '', '', 'Lead - IT Infrastructure', '9986191994', 'shakeelas@brillio.com', '', '', '', 'www.cerner.com', 'No 94/1 C & 94/2 Phase 1, Veerasandra, Attibele Hobli', '', 'Koramangala', '', 560047, 'Karnataka', 0, '-', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(166, '', '', 'Britannia ', '', '', 'Nagaraj', '', '', '', '', '96327 30444', 'NAGARAJANV@BRITINDIA.COM', '', '', '', '', 'RMZ Ecospace, 4A Block, 5th Floor, Bellandur Village', '', 'Marathahalli', 'Bangalore', 560078, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(167, '', '', 'Britannia Industries Limited', 'Manufacturing', '', 'Nagarajan Vaidyanathan', '', '', 'Information Technology', 'Project manager', '080 3078 7411-D/ 394', 'nagarajanv@britannia.co.in', '', '', '', '', 'RMZ Ecospace, 4A Block, 5th Floor, Bellandur Village', 'Outer Ring Road', 'Marathahalli', 'Bangalore', 560078, 'Karnataka', 80, '40136100', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(168, '', '', 'BROADCOM INDIA PRIVATE LTD', 'IT', '', 'Mohammed', '', '', '', '', '6565596510', 'mrizwan@broadcom.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(169, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(170, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(171, '', '', 'CABS', '', '', 'Pradeepraja ', '', '', '', '', '25049210', 'pradeepraja@cabs.drdo.in', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(172, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(173, '', '', 'Cades Digitech Pvt. Ltd. (Axis Cades)', '', '', 'Chandrashekar', 'S', 'GM Above', 'Information Technology', 'Deputy General Manager', '9845347886', 'chandra.s@cadestech.com / chandra.s@axiscades.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(174, '', '', 'Cades Digitech Pvt. Ltd. (Axis Cades)', '', '', 'Rajesh', 'R', 'Manager Level', 'Information Technology', 'Manager', '7411661088', 'rajesh.rao@axiscades.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(175, '', '', 'CAIR', '', '', 'Sanjeev Gupta – Head  NWD', '', '', '', '', '9449804859', 'gsanjeev@cair.drdo.in', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(176, '', '', 'CAIR', '', '', 'Ashoke DL', '', '', '', '', '9686984939', 'ashokadl@cadence.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(177, '', '', 'Cambridge Public School', 'IT-Manager', 'Education', 'Anand', '', '', 'IT', ' 080 2572 3839', '080-25727335', 'support@cambridgepublicschool.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(178, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(179, '', '', 'Canara Bank', '', '', 'Yogesh Dewan', '', '', 'Manager - IT', '', '91-9449987420', 'yogeshdewan@canbank.co.in / yogeshdewan@gmail.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(180, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(181, '', '', 'CANARA ENGINEERING COLLEGE', '', '', 'Praveen Desai', '', '', '', '', '9448153180', 'praveen.desai@canaraengineering.in', '', '', '', 'httr://www.canarabank.com', '', '', '', 'BANGALORE', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(182, '', '', 'Canvera Digital Technologies Pvt Ltd.', '', '', 'Ranjith Kumar    ', '', '', '', 'IT Manager', '91 9686689814', 'ranjith.kumar@canvera.com', '', '', '', 'www.canarabank.com', 'Department of Information Technology Naveen Complex, 14, MG Road, Bangalore 560001', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(183, '', '', 'CAPCO', '', '', 'Subramanya paramashivaiah', '', '', '', '', '9845246090', 'subramanya.paramashivaiah@capco.com', '', '', '', 'www.canvera.com', '531/144, 150 Feet Ring Road, Agara, HSR Layout,Bangalore - 560102 India', 'Bangalore', '', 'Bangalore', 0, '', 0, '91 80-67231111', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(184, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(185, '', '', 'Capgemini India Pvt. Ltd.', '', '', 'Ongaliappan', 'Shhanmugasundaram', 'Manager Level', '', 'Senior Consultant', '7829211122', 'ongaliappn@gmail.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(186, '', '', 'Capgemini India Pvt. Ltd.', '', '', 'Venugopal', '', 'Manager Level', '', 'Project Manager - IT', '9845658748', 'venugopal.scindia@capgemini.com', '', '', '', '', 'Benjana Padavu, Bantwal 574219', '', '', 'spoke to miss rema of canara enginnering college she gave the Hod contact no 9986728550', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(187, '', '', 'Capillary technologies', '', '', 'Shijeesh', '', '', '', 'System Admin', '080-49074600', 'shijeesh.c@capillarytech.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(188, '', '', 'Captronic Systems Pvt. Ltd', '', '', 'Jitin Panickar', '', '', '', '', '9886188583', 'jitin@captronicsystems.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(189, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(190, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(191, '', '', 'Careernet ', '', '', 'Vishal Mathad', '', '', 'Manager MIS', '', '9845244224', 'vishal@careernet.co.in', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(192, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(193, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(194, '', '', 'CDAC', '', '', 'Dr.R.Balaji', '', '', 'Senior Technical Officer', '', '9448542676', 'balaji@cdac.in', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(195, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(196, '', '', 'Cegedim Software India Pvt. Ltd.', '', '', 'Rajendra', '', '', 'Manager - Global IT Delivery', '', '9880633135', 'rajendra.vasantrao@cegedim.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(197, '', '', 'Celstream Systems Pvt Ltd', '', '', 'Sunil Kumar', '', '', '', '', '9886606097', 'sunil@celstream.com', '', '', '', '', '', '', '', 'Bangalore', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(198, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(199, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(200, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(201, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(202, '', '', 'CenturyLink Technologies ', '', '', 'Narendra Marikale', '', '', 'Technical Director ', '', '9901877911', 'narendra.marikale2@qwest.com', '', '', '', '', 'Chengalpattu - 635 001', '', '', '', 0, '', 0, '044 - 27426274 / 274', '044 - 27426064', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(203, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27');
INSERT INTO `location` (`id`, `Representive`, `RMail`, `Company`, `Sector`, `SubSector`, `FirstName`, `LastName`, `Level`, `Dept`, `Designation`, `Mobile`, `Mail`, `ContactPerson2`, `ContactNumber`, `EmailID`, `Url`, `Address`, `Location`, `SubLocation`, `City`, `Pin`, `State`, `StdCode`, `LandlineNo`, `FaxNo`, `NoOfEmployees`, `CompanyType`, `GSTNo`, `ModificationDetail`) VALUES
(204, '', '', 'Cerner', '', '', 'Sathish Kamath', '', '', '', '', '9880019077', 'sathish.kamath@cerner.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(205, '', '', 'Cerner Healthcare Solutions ', '', '', 'K B Rao', '', '', 'Sr Team Lead, Middle East', '', '9972583918', 'kb.rao@cerner.com', '', '', '', '', 'No 4 Sarojini Street Chinna Chokkulam, ', '', '', 'Madurai-625002', 0, '', 0, '0452-2530746/2531430', '0452-2530660', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(206, '', '', 'Cerner Healthcare Solutions Pvt. Ltd.', 'Health Care', '', 'Bala Muralikrishna', 'Kakumani', '', '', 'Senior Manager - Technical & Projects', '9980120415', 'bala.kakumani@cerner.com', '', '', '', '', 'No. 45', '', '', 'Bangalore', 560071, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(207, '', '', 'Cerner Healthcare Solutions Pvt. Ltd.', 'IT ', '', 'Sujatha Marthandappa', '', '', 'Project Manager', '', '9980528303', 'sujathamj@gmail.com / sujatha.marthandappa@cerner.com', '', '', '', '', 'Manyatha Embassy Business Park, Level 8, Block C2, Cedar', 'Outer Ring Road', 'Nagavara', 'Bangalore', 560045, 'Karnataka', 80, '30780175', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(208, '', '', 'CGI - Logica', '', '', 'Ravindra Athalye ', '', '', 'Director, Delvery - Industry, Transportation and Public Sector', 'Director, Delvery - Industry, Transportation and Public Sector', '9845325823', 'ravindra.athalye@logicacmg.com', '', '', '', '', 'Divyasree Technopolis, No. 124-125', 'Off HAL Airport Road', '', 'Bangalore', 560008, 'Karnataka', 80, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(209, '', '', 'CGI Information Systems ', 'CTRM product development', '', 'Vivekananda Naskar', '', '', 'Manager - IT & Program', '', '9845456527', 'vivekananda.naskar@cgi.com', '', '', '', '', 'Manyatha Embassy Business Park, Level 8, Block C2, Cedar', '', 'Nagavara', 'Bangalore', 560100, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(210, '', '', 'CGI Information Systems ', 'IT', '', 'Vivekananda', 'Naskar', '', '', 'Manager - IT & Program', '9845456527', 'vivekananda.naskar@cgi.com', '', '', '', '', 'No. 43, Block B3 & B4, Velankani Tech Park', '', '1st Phase, Electronic City ', 'Bangalore', 560100, 'Karnataka', 80, '66991786', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(211, '', '', 'CGI Information Systems ', 'IT ', '', 'Radhakrishnan', '', '', '', 'Project Manager', '9945191827', 'pradhan6@yahoo.com', '', '', '', '', 'Plot No. 4A, 5 & 6, 2nd Phase', '', 'Peenya Industrial Area', 'Bangalore', 560048, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(212, '', '', 'CGI Information Systems ', '', '', 'Vivek ', '', '', 'Manager - Service Delivery ', '', '9008002141', 'vivek.verma@cgi.com', '', '', '', 'www.ekaplus.com', 'No. 29/2, Agrahara Village, Vrindavan Tech Village, Building No. 2 A, East Tower', '', 'Vathur Hobli', 'Bangalore', 560037, 'Karnataka', 80, '49313852', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(213, '', '', 'CGI Information Systems ', '', '', 'Radhakrishnan', '', '', 'Project Manager', '', '9945191827', 'pradhan6@yahoo.com', '', '', '', '', 'Divyasree Technopolis, No. 124-125', 'Off HAL Airport Road', '', 'Bangalore', 560008, 'Karnataka', 80, '41940000', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(214, '', '', 'CGI Information Systems ', '', '', 'Vivekananda Naskar', '', '', 'Manager - IT & Program', '', '9845456527', 'vivekananda.naskar@cgi.com', '', '', '', '', 'Divyasree Technopolis, No. 124-125', '', '', 'Bangalore', 560005, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(215, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(216, '', '', 'CHERRY HILL INTERIORS LTD', '', '', 'M Ganesh Kumar', '', '', '', '', '9663390144', 'ganesh@cherryhill.in', '', '', '', '', 'Divyasree Technopolis, No. 124-125', '', '', 'Bangalore', 560103, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(217, '', '', 'CHERRYHILL INTERIORS LTD', '', '', 'Ajay Kabbur', '', '', '', '', '9916002566', 'ajay@cherryhill.in', '', '', '', '', 'No 3/1 Platform Road', '', '-', '', 560105, 'Karnataka', 0, '30798080', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(218, '', '', 'Cheslind Textiles Ltd', '', '', 'Senthil ', 'Kumar', '', '', 'AVP - IT', '9965133722', 'systems@cheslind.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(219, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(220, '', '', 'CiberSites', '', '', 'Kishore Kumar', '', '', '', '', '9880152414', 'KMNaidu@ciber.com', 'M Ganesh Kumar', '9663390144', 'ganesh@cherryhill.in', 'Manjunatha', 'manjunath@cherryhill.in', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(221, '', '', 'Cipla', '', '', 'Vijendra', '', '', '', '', '9945566770', 'vijendra.r@cipla.com', '', '', '', 'www.cheslind.com', 'No 690 Udyambag', '', 'Koramangala', '', 560034, 'Karnataka', 0, '25538622', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(222, '', '', 'CITIZEN', '', '', 'Vijaya Kumar. V', '', '', '', '', '9886708158', 'vijayakumar@citizenwatches.co.in', '', '', '', '', '', '', '', 'Vellor-632002 ', 0, '', 0, 'Phone No:0416-228420', 'Fax:0416-2262703', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(223, '', '', 'Citrix Systems', '', '', 'Shubha Ramnath', '', '', 'Director, IT Services', '', '9972748459', 'Shubha.Ramnath@citrix.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(224, '', '', 'Claytronics', '', '', 'Umesh thalam', '', '', '', 'Director', '9886722627', 'umesh@claysol.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(225, '', '', 'CMR Institute', '', '', 'Ravi Chandar', '', '', 'Director', '', '9900488611', 'director.training@cmr.ac.in', '', '', '', '', '', '', '', 'Bangalore', 560058, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(226, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(227, '', '', 'Cogent', 'BPO', 'Telecom', 'Danish Raza', '', '', '', 'IT Manager', '080-41698490', 'ithelpdesk_bangalore@cogenteservices.com', '', '', '', '', 'Chinnavedampatti,', '', '', 'Coimbatore - 641 006', 0, '', 0, '0422-2667496', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(228, '', '', 'cognizant', '', '', 'Backiaraj Periasamy', '', '', 'Technical Manager', '', '91-9886036336', 'backiaraj.periasamy@cognizant.com', '', '', '', '', '', '', '', 'Bangalore', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(229, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(230, '', '', 'COLLABERA', '', '', 'Sivakumar R ', '', '', '', '', '9740562998', 'sivakumarr@collabera.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(231, '', '', 'Columbia Asia', '', '', 'Shanti Shenoy', '', '', '', '', '91 80 40211000', 'shanti.shenoy@columbiaasia.com', '', '', '', 'www.cognizant.com', '24/7 Customer, Embassy Glflinks Business Park, off Intermediate Ring Road, Bangalore-560071', '', '', 'spoke to mr beckiraj he is not working with 24/7 instead he is working in cognizant.told to sent me ', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(232, '', '', 'Commits (Convergence Institute of Media, Management and Information Technology Studies)', 'Media', '', 'Dcruz', '', '', 'IT support', '', '080-22581466', 'admin@commits.edu.in', '', '', '', '', '', 'HSR', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(233, '', '', 'Commits (Convergence Institute of Media, Management and Information Technology Studies)', 'Media', '', 'jeenam lopez', '', '', 'IT support', '', '080-22581466', 'admin@commits.edu.in', '', '', '', '', '', 'HSR', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(234, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(235, '', '', 'Concentrix BPO Pvt Ltd', 'BPO', '', 'Elangovan Kalidasan', '', '', '', '', '9845136259/ 08066145', 'elangovan.kalidasan@concentrix.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(236, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(237, '', '', 'CONTINENTAL AUTOMOTIVE COMPONENTS', '', '', 'Geeta Rammohan ', '', '', '', '', '9008011558', 'geeta.ram@Continental-corporation.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(238, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(239, '', '', 'COUPONS.COM INDIA PVT LTD', '', '', 'Prem Kumar ', '', '', '', '', '7411191131', 'PKumar@couponsinc.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(240, '', '', 'Cricbuzz (Akuate Internet Services Pvt Ltd)', '', '', 'Saurabh Tiwari', '', '', '', 'Head IT', '91 8050375354', 'saurabh.tiwari@cricbuzz.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(241, '', '', 'CrimsonLogic', '', '', 'PR Ravi Krishna Sai', '', '', 'Systems Analyst', '', '080677 09500', 'ravikrishna@crimsonlogic.co.in', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(242, '', '', 'CSC India Pvt. Ltd.', '', '', 'Pratap', 'Reddy', 'Manager Level', 'Information Technology', 'Manager', '9686855991', 'preddy21@csc.com / pratappreddy@gmail.com', '', '', '', 'www.cricbuzz.com', 'No. 190-B, 1st Floor, HN Plaza,100 Ft Ring road,6th Main, BSK 3rd Stage, Bangalore . 560085, INDIA', 'Bangalore', '', 'Bangalore', 0, '', 0, '91 80-26790623', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(243, '', '', 'CSC India Pvt. Ltd.', '', '', 'Ramesh', 'Subramaniam', 'GM Above', 'Information Technology', 'Director - Global Applications & Technology', '9845034984', 'rsubramania9@csc.com', '', '', '', 'www.cricbuzz.com', 'No. 190-B, 1st Floor, HN Plaza,100 Ft Ring road,6th Main, BSK 3rd Stage, Bangalore . 560085, INDIA', 'Bangalore', '', 'Bangalore', 0, '', 0, '91 80-26790623', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(244, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(245, '', '', 'CSR India', '', '', 'Shashidar Maiya', '', '', '', '', '9844113177', 'shashidhara.maiya@csr.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(246, '', '', 'Cubic Computing', '', '', 'Satish KV', '', '', '', 'Sales', '9844070989', 'sathish.kv@cubic.co.in', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(247, '', '', 'Cyient Ltd.', '', '', 'Deepak', 'G', 'Manager Level', 'Information Technology', 'Project Manager', '9901852179', 'togdeepak@gmail.com / deepak.gurunath@cyient.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(248, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(249, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(250, '', '', 'Dentsu Communication', '', '', 'Bharath Kumar    ', '', '', '', 'IT Manager', '91 9611166966', 'bharath.kumar@dentsuaegis.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(251, '', '', 'Design Sense software technologies pvt ltd', '', '', 'Nainar', '', '', '', 'Managing Director', '91-9880826518', 'nainar@thedesignsense.com\n\n', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(252, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(253, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(254, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(255, '', '', 'DhanlaxmiBank', '', '', 'Ani K Nair', '', '', '', '', '9539003712', 'aninair.k@dhanbank.co.in', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(256, '', '', 'DHFL Vysya Housing Finance', '', '', 'K L Rajesh', '', '', 'Information Technology', 'Chief Manager', '080 2227 9925-D/2221', 'kl.rajesh@dvhousing.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(257, '', '', 'Digicomm Semiconductor', '', '', 'Abhik', '', '', 'Sales ', 'Manager', '8817223368', 'abhik@digicomm.org', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(258, '', '', 'Digital Instruments & Control Systems', '', '', 'Vivek Anand', '', '', '', 'IT Manager', '91 9448045318', 'vivekpn@dicsglobal.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(259, '', '', 'Disha Communications Pvt Ltd', '', '', 'Dinesh K', '', '', '', 'Sr. System Administrator ', '91 9845689936', 'dinesh.k@dishacom.com\n\n', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(260, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(261, '', '', 'DSP TECHNOLOGY INDIA PVT LTD', '', '', 'Bharath Kumar', '', '', '', '', '080-40248374', 'bharath.kumar@dspg.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(262, '', '', 'DSP TECHNOLOGY INDIA PVT LTD', '', '', 'Spurthy Vijayakumar', '', '', '', '', '080-40248374', 'spurthy.vijayakumar@dspg.com', '', '', '', 'www.dreamfireinteractive.com', '8th Floor, HMG Ambassador, 137, Residency Rd, Bangalore-560025', '', '', 'the all no provided to this company are wrong', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(263, '', '', 'DSP TECHNOLOGY INDIA PVT LTD', '', '', 'Povana Devi', '', '', '', '', '080-40248374', 'puvana.devi@dspg.com', '', '', '', '', '', '', '', 'BANGALORE', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(264, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(265, '', '', 'DTDC', '', '', 'Satyajit Saker', '', '', '', '', '9972004444', 'satyajit@dtdc.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(266, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(267, '', '', 'Dxcorr Hardware Technologies Pvt Ltd', '', '', 'Pappu Ahir', '', '', '', '', '7795712499', 'pappu@dxcorr.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(268, '', '', 'DynPro', '', '', 'Girish M N', '', '', 'Team Leader - Taleny Acquisition', '', '91-9880726563', 'girish@dynproindia.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(269, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(270, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(271, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(272, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(273, '', '', 'Eblitz creations india private limited Group', '', '', 'Harsha Pulicheri', '', '', '', 'Manager -IT', '91 9886398567 /91 99', 'harsha@eblitzcreations.com', '', '', '', '', 'Bharati Salai, Ramapuram Campus', '', '', 'Chennai - 600 089', 0, '', 0, '044-22490 853/ 22490', '044-22492429', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(274, '', '', 'ECLINICAL WORKS INDIA PVT LTD', '', '', 'Anand Sannappa', '', '', '', '', '080-45114400', 'anand.sannappa@eclinicalworks.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(275, '', '', 'ECLINICAL WORKS INDIA PVT LTD', '', '', 'Surendhar Kumar', '', '', '', '', '8861001937', 'surendhar.kumar@eclinicalworks.com', '', '', '', '', 'Bharathi Salai, Ramapuram,', '', '', 'Chennai – 600 089.', 0, '', 0, 'Phone No:  044 – 249', 'Fax.No: 044 – 249 0430 ', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(276, '', '', 'EFI India Pvt. Ltd.', '', '', 'Hariprasad TN', '', '', '', '', '9945707707', 'hariprasad.tn@efi.com', '', '', '', 'www.eblitzcreations.com ', '#37, 6TH CROSS, 7TH MAIN, FRIENDS COLONY S.T BED, 4TH BLOCK KORAMANGALA,Bangalore.Karnataka - 560 034', 'Bangalore', '', 'Bangalore', 0, '', 0, '91 80 41680555(ext 2', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(277, '', '', 'Egis Geoplan Pvt Ltd', '', '', 'Krishna ', 'Mohan', '', '', 'Asst Manager - Systems', '9845794710', 'krishna@egis-geoplan.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(278, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(279, '', '', 'Eka Software Solutions Pvt Ltd', '', '', 'Nithin ', '', '', '', '', '9886328299', 'nithim.m@ekaplus.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(280, '', '', 'Eka Software Solutions Pvt. Ltd.', 'CTRM product development', '', 'Rahul', 'Jain', '', '', 'Director - Engineering', '9880298892', 'rahulkjain@gmail.com / rahulk.jain@ekaplus.com', '', '', '', '-', 'No 25&30 1st Phase', '', 'Marathalli', '', 560037, 'Karnataka', 0, '-', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(281, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(282, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(283, '', '', 'EMINENT', '', '', 'Gopalakrishnan T', '', 'MANAGING DIRECTOR', '', '', '91-9880031332', 'tgopal@eminentlabs.com', '', '', '', '', '24th Cross Kilari Road', '', '', '', 560093, 'Karnataka', 0, '40400800', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(284, '', '', 'EMMVEE Photovoltaic Power Private Limited', '', '', 'Gopal MB', '', '', '', 'General Manager -IT', '91 9880129988', 'gopal.mb@emmvee.in\n\n', 'Sindhu', '', 'Sindhu.Mohithe@emc.com', 'Mohamed Aleem', 'mohamed.aleem@emc.com', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(285, '', '', 'Emulex Communications Pvt. Ltd', '', '', 'Golla Pothana', '', '', '', '', '9900214516', 'golla.pothana@emulex.com', '', '', '', 'www.eminentlabs.com', '#10, 1st Main, Stag Extension, S.T. Bed, 4th Block, Koramangala, Bangalore 560095', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(286, '', '', 'ENDEAVOUR', '', '', 'Sudhir Nikam', '', '', 'IT Manager', '', '91-9731819694', 'sudhir.nikam@techendeavour.com', '', '', '', 'www.emmvee.com\n\n', '# 13/1, International Sonnappanahalli 562 157,, Airport Rd, Bengaluru, Karnataka', 'Bangalore', '', 'Bangalore', 0, '', 0, '91 80 4323 3511', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(287, '', '', 'Ensomerge', '', '', 'Mukund Mukesh', '', '', '', 'Director', '91 9886161482', 'mukund.mukesh@ensomerge.com', '', '', '', 'www.emmvee.com\n\n', '# 13/1, International Sonnappanahalli 562 157,, Airport Rd, Bengaluru, Karnataka', 'Bangalore', '', 'Bangalore', 0, '', 0, '91 80 4323 3511', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(288, '', '', 'Ensomerge BPO solutions', 'BPO', '', 'Jupin', '', '', '', 'IT Manager', '9886161482 /  974295', 'info@ensomerge.com/jupin.johny@ensomerge.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(289, '', '', 'ESPN', '', '', 'Mohan.G', '', '', '', '', '9620000025', 'Mohan.Gottimukkala@espn.com', '', '', '', 'www.softtech-engr.com', 'No.11 & 12, 4th Floor, Ave Maria Complex, 100 Feet Road, Jalahalli Cross, Bangalore', 'Bangalore', '', 'Bangalore', 0, '', 0, '9886161482 /  974295', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(290, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(291, '', '', 'Essae Digitronics', '', '', 'Pavithra', '', '', '', '', '080-41121195', 'pavithra@epitomeinfotech.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(292, '', '', 'Essilor', '', '', 'Sachin', '', '', 'Information Technology', 'Sr. Manager', '9972517655', 'sachin.bl@essilorindia.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(293, '', '', 'Essilor', '', '', 'Vikram', '', '', 'Information Technology', 'Manager', '9620255234', 'vikram.r@essilorindia.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(294, '', '', 'Essilor India Private Limited', '', '', 'Gurunandan G.', '', '', '', 'IT Head', '91 9972065629', 'guru@essilorindia.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(295, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(296, '', '', 'ESTUATE SOFTWARE PRIVATE LIMITED', '', '', 'Aditya', '', '', '', '', '9964951583', 'adithya.hj@estuate.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(297, '', '', 'EVOLVING SYSTEMS', '', '', 'Maqsood Pasha', '', '', '', 'Manager - Systems Engineering', '9845778998', 'maqsood.pasha@evolving.com', '', '', '', 'www.essilorindia.com', 'No. 71/1, S.C.Road,Brigade Plaza,6th Floor, AnandRao Circle, Gandhinagar, Bangalore - 560009.', 'Bangalore', '', 'Bangalore', 0, '', 0, '91-80-40921800/801', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(298, '', '', 'Excubator Consulting Private Limited', 'soft ware', '', 'Vivek', '', '', 'IT support', '', '080 4164 7830', 'vivek@excubator.org', '', '', '', '', '', 'HSR', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(299, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(300, '', '', 'Exide Life Insurance', '', '', 'Karthick Prabhu', 'Natesh', 'Manager Level', '', 'Senior Manager - IT delivery', '9900809097/948288788', 'karthick.prabhu@exidelife.in', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(301, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(302, '', '', 'Faurecia Emissions Control Technologies India Pvt Ltd', '', '', 'HARISH RANGASWAMAIAH', '', '', '', '', '088072 18357', 'harish.rangaswamaiah@faurecia.com', '', '', '', '', 'Vrindavan Tech Village, 2nd Phase, Aster Building 2A', '', 'Devarabeesanhalli', 'Bangalore', 560087, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(303, '', '', 'Fichtner Consulting Engineers India Private Limited ', 'soft ware', '', 'Kumar', '', '', 'IT support', '', '080-67593366', 'sysadmin@blr.fichtner.co.in', '', '', '', '', '', 'HSR', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(304, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(305, '', '', 'Fidelity Business Services Pvt. Ltd.', 'Financial Services', '', 'Iqbal', 'Ahmed', '', '', 'Team Lead ', '7259029393 / 9886651', 'iqbal.ahmed@fidelity.com / iqbal.ahmed@fmr.com', '', '', '', '', 'Soul Space Service Road', '', '-', '', 560001, 'Karnataka', 0, '-', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(306, '', '', 'Fidelity Business Services Pvt. Ltd.', '', '', 'Atul ', '', '', 'Project Manager', '', '9972393462', 'atul.maurya@fmr.com', '', '', '', '', 'Embassy Golf Link Business Park', '', '', 'Bangalore', 560071, 'Karnataka', 80, '40335867', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(307, '', '', 'Fidelity Business Services Pvt. Ltd.', '', '', 'Sampath', '', '', 'Manager - Database Administration', '', '8197826299', 'a366175@fmr.com', '', '', '', '', 'Embassy Golf Link Business Park', '', '', 'Bangalore', 560093, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(308, '', '', 'Fidelity Business Services Pvt. Ltd.', '', '', 'Snehasish', '', '', 'Director ', '', '9880007220', 'snehasish.das@fmr.com', '', '', '', '', 'No. 11/1 & 12/1, Maruthi Infotech Center', '', 'Domlur', 'Bangalore', 560093, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(309, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(310, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(311, '', '', 'First American India Pvt. Ltd.', 'Healthcare', '', 'Srikanta Sahoo', '', '', 'Manager - Delivery & Enterprise', 'Manager - Delivery & Enterprise', '8892073504', 'srikantak@yahoo.com / ssahoo@firstam.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(312, '', '', 'First Indian Corporation Private Limited', '', '', 'Shiva Prasad', '', '', 'Deputy Manager - Business Process Management', '', '91-9902777442', 'prashiva@firstam.com', '', '', '', '', 'No. 73/1, 7th Floor, Summit Tower B ', '', '', 'Bangalore', 560048, 'Karnataka', 80, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(313, '', '', 'First Source', '', '', 'Prasad KS', '', '', '', '', '9900172631', 'prasad.ks@firstsource.com', '', '', '', '', 'Level 6, Navigator Building, ITPB', 'Whitefield Road', '', 'Bangalore', 560066, 'Karnataka', 80, '67013333 Extn. 3956', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(314, '', '', 'Flipkart', '', '', 'Ajeesh', '', '', '', '', '9900909455', 'ajeesh@flipkart.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(315, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(316, '', '', 'FNF India', '', '', 'Vaman', '', '', '', '', '9845523606', 'vaman.s@fnfindia.co.in', '', '', '', '', '', '', '', 'BANGALORE', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(317, '', '', 'Focus Edumatics Private Limited', '', '', 'Ramesh NV', '', '', '', 'IT Manager', '91-9845595246 ', 'ramesh.nv@focusedumatics.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(318, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(319, '', '', 'Fouress Engineering India Ltd', '', '', 'Prabhu / Mr. P. B. Renuka Rao', '', '', '', 'System Admin / IT Manager', '91-9741191375 / +91-', 'prabhu@fouressindia.com / pbr@fouressindia.com \n\n', '', '', '', 'www.focusedumatics.com', '11/1, 24th ‘A’ Cross, K.R. Road, 2nd Stage, Banashankari, Bangalore-560 070 ', 'Bangalore', '', 'Bangalore', 0, '', 0, '91-80-2334 5566', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(320, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(321, '', '', 'Fulcrum Global', '', '', 'Harish Vasista', '', '', 'Information Technology', 'Manager', '9845067313', 'harish.vasishta@fulcrumglobal.net', '', '', '', 'www.fouressengg.com\n\n', 'Plot No. 2, Phase II, Peenya Industrial Estate Bangalore - 560 058', 'Bangalore', '', 'Bangalore', 0, '', 0, '91 80 28395734 -8', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(322, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(323, '', '', 'GCC SERVICES INDIA PVT LTD', '', '', 'Prasad, Mohan ', '', '', '', '', '9986166533', 'Mohan.Prasad@AB-Inbev.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(324, '', '', 'GCC SERVICES INDIA PVT LTD', '', '', 'S, Manoranjan ', '', '', '', '', '9902000344', 'Manoranjan.S@AB-Inbev.com', '', '', '', '', 'Civil Aerodrome Post,  Sulur ', '', '', 'Coimbatore 641014', 0, '', 0, 'Phone: 0422-2572719', 'Fax: 0422-210187', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(325, '', '', 'GCC SERVICES INDIA PVT LTD', '', '', 'Ranjan V, Sujana', '', '', '', '', '9845876132', 'Sujana.RanjanV@ab-inbev.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(326, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(327, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(328, '', '', 'GE Healthcare India Pvt. Ltd .', 'IT', 'Healthcare', 'Rajeesh ', 'Padmanabhan', '', '', 'SW Lead - Systems & Designer - Product Security', '9900123428', 'rajeesh.padmanabhan@ge.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(329, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(330, '', '', 'Gems Business', '', '', 'Kunal Nehruji', '', '', 'Manager-Student Relation', '', '9845344337', 'kunal.nehruji@gemsbschool.com', '', '', '', '', 'No. 73/1, 7th Floor, Summit Tower B ', '', '', 'Bangalore', 560103, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(331, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(332, '', '', 'Genotypic Technologies pvt ltd', '', '', 'Vinay', '', '', 'Manager-It', '', '9845403332', 'vinay@genotypic.co.in', '', '', '', '', '', '', '', 'Bangalore', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(333, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(334, '', '', 'Getit Infomediary Pvt. Ltd.', 'ICT', '', 'Vasudev Mukund Pai', '', '', 'Head - IT', '', '9900997455', 'it@getitinfomediary.in / pai.vasudev@yahoo.in', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(335, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(336, '', '', 'Goldman Sachs Services Pvt. Ltd.', '', '', 'Abishk', '', '', 'Vice President - IT', '', '9019237603', 'abhi_pat@hotmail.com', '', '', '', '', 'No. 73/1, 7th Floor, Summit Tower B ', '', '', 'Bangalore', 560103, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(337, '', '', 'GXS Technology', '', '', 'Bharanidharan R ', '', '', 'Program Manager', '', '9880493208', 'bharanidharan.r@gxs.com', '', '', '', '', 'No.19/5 & 19/6, 4th Floor, Kareem Towers', '', '', 'Bangalore', 560103, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(338, '', '', 'Halbit Avionics Pvt Ltd', '', '', 'Venkat Raja', '', '', '', 'IT Head', '91-99007 61013', 'it.admin@halbit.co.in', '', '', '', 'www.nvi.co.in', 'No 652, 3rd Flr  Raj Kumar Rd', '', 'Hoodi Village, Mahadevapura', '', 560048, 'Karnataka', 0, '67370000', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(339, '', '', 'Happiest minds', '', '', 'S Kumar', '', '', '', 'Senior Architect\n', 'NA', 'sukumar.shekhar@happiestminds.com', '', '', '', '', 'No. 73/1, 7th Floor, Summit Tower B ', '', '', 'Bangalore', 560048, 'Karnataka', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(340, '', '', 'Happiest Minds Pvt. Ltd.', '', '', 'Gopalakrishna', '', '', '', 'Chief Technology Officer', '9845205396', 'gopalakrishna.bylahalli@happiestminds.com', '', '', '', 'www.halbit.co.in', '2nd Floor, Old ADB building, 28 Gate Old airport departure lane, Konena Agrahara, HAL, Bengaluru, Karnataka 560017', 'Bangalore', '', 'Bangalore', 0, '', 0, '91 80 40491000 ext.1', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(341, '', '', 'Happiest Minds Pvt. Ltd.', '', '', 'Jagadeesh', '', '', 'Head - IT Delivery', 'Head - IT Delivery', '9900126929', 'jagadeesh.cv@happiestminds.com', '', '', '', 'www.happiestminds.com', 'Block II, Velankani Tech Park, 43 Electronic City, Hosur Road, Bangalore-560100', 'Bangalore', '', 'Bangalore', 0, '', 0, '91 9739006623', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(342, '', '', 'Happiest Minds Technologies Pvt. Ltd.', '', '', 'Chandra', 'Prakash', 'Manager Level', '', 'Manager & Solution Architect', '9880989860', 'chandra.prakash@happiestminds.com', '', '', '', '', '53, 1/2, Hosur Rd, Madiwala, 1st Block Koramangala, Bommanahalli.', '', 'Bommanahalli', 'Bangalore', 560068, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(343, '', '', 'Happiest Minds Technologies Pvt. Ltd.', '', '', 'Vamsi Krishna', 'Jakka', 'Manager Level', '', 'Assistant Manager - Technical', '8050012002 / 9740375', 'vamsi.j@happiestminds.com', '', '', '', '', 'Manyatha Embassy Business Park, Level 8, Block C2, Cedar', '', 'Nagavara', 'Bangalore', 560001, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(344, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(345, '', '', 'Harman', '', '', 'Raja', '', '', '', '', '9880589955', 'Raja.mattihalli@harman.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(346, '', '', 'Harman', '', '', 'Vinod Kumar', '', '', '', '', '9880132343', 'Vinodkumar.Ramanathan@harman.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(347, '', '', 'Harman International India Pvt. Ltd.', '', '', 'Debtaru', 'Basak', 'Manager Level', 'Information Technology', 'Manager', '9845188182', 'debtarubasak@yahoo.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(348, '', '', 'Harman International India Pvt. Ltd.', '', '', 'Robin', 'Thomas', 'Manager Level', '', 'Project Manager', '9886689789', 'robinmt@harman.com / robin.thomas@harman.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(349, '', '', 'Health Asyst ', 'Medical', '', 'Rijov', '', '', 'IT', 'IT Manager', '080-25200257', 'rijov@healthasyst.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(350, '', '', 'Health Asyst Pvt. Ltd.', '', '', 'Bhupesh', 'Nadkarni', 'GM Above', '', 'Vice President', '9844724752', 'bhupesh.nadkarni@healthasyst.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(351, '', '', 'Health Care Global', '', '', 'Girish Varote', '', '', '', '', '9611803030', 'girishvarote@hcgoncology.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(352, '', '', 'Hed Experts Private Limited', '', '', 'Vishnu Gonela', '', '', '', 'IT Manager', '91 9945826918', 'vishnu.gonela@hedexperts.com\n\n', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(353, '', '', 'Himalaya Herbal Healthcare Ltd.', '', '', 'Srinivas', '', '', '', 'Manage IT', '91- 9980800011', 'srini@himalayawellness.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(354, '', '', 'HIMATSINGKA SEIDE LTD', '', '', '', '', '', '', '', '080 2237 8000', 'it@himatsingka.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(355, '', '', 'Hinduja Global Solutions Ltd.', 'BPO', '', 'Ateequr Rahman', '', '', '', '', '9845237967', 'ateeq@teamhgs.com', '', '', '', 'www.hedexperts.com \n\n', '25/1, T. Chowdaiah Road Lower Palalce Orchards, Bangalore - 560003, Karnataka, India', 'Bangalore', '', 'Bangalore', 0, '', 0, '91 9945826918', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(356, '', '', 'Hinduja Global Solutions Ltd.', 'BPO', '', 'Kannan ', 'Ramakrishnan', '', 'Information Technology', 'General Manager', '9900114336', 'Kannan.R@teamhgs.com', '', '', '', 'himalayawellness.com', 'Makali, Bangalore, KA – 562162, India', 'Bangalore', '', 'Bangalore', 0, '', 0, '91- 80-23714444', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(357, '', '', 'Hinduja Global Solutions Ltd.', 'BPO', '', 'Naveen Kumar C N', '', '', '', '', '9845163284', 'naveen@teamhgs.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(358, '', '', 'Hinduja Global Solutions Ltd.', 'BPO', '', 'Prakash Kudalkar', '', '', '', '', '9845402437', 'Prakash.k@teamhgs.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(359, '', '', 'Hinduja Global Solutions Ltd.', 'BPO', '', 'Ravindra M G', '', '', 'Information Technology', 'Director', '9845734255', 'ravindramg@teamhgs.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(360, '', '', 'Hinduja Global Solutions Ltd.', 'BPO', '', 'Syed Nadeem', '', '', '', '', '9845997892', 'syednadeem@teamhgs.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(361, '', '', 'Hitachi Koki India Limited', '', '', 'Shija', '', '', '', 'IT Manager', '91-9980488871', 'erp@hitachi-koki.in', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(362, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(363, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(364, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(365, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(366, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(367, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(368, '', '', 'Huawei Technologies India Pvt. Ltd.', 'ICT', '', 'Anish Cheriyan', '', '', 'Director', '', '9886086173', 'anishcheriyan@huawei.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(369, '', '', 'Huawei Technologies India Pvt. Ltd.', 'ICT', '', 'Naveen Krishnan', '', '', 'Head - Delivery', '', '9886848946', 'naveen@huawei.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(370, '', '', 'Huawei Technologies India Pvt. Ltd.', 'ICT', '', 'Anish', 'Cheriyan', '', '', 'Director', '9886086173', 'anishcheriyan@huawei.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(371, '', '', 'Huawei Technologies India Pvt. Ltd.', 'ICT', '', 'Gowri Shankar', 'K', '', '', 'Director', '9972633500', 'kgowrishankar@huawei.com', '', '', '', 'www.huawei.com', 'No. 23, Level - 3/4/5/6 & 7, Leela Galleria, The Leela palace ', 'Old Airport Road', '', 'Bangalore', 560008, 'Karnataka', 80, '39808600', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(372, '', '', 'Huawei Technologies India Pvt. Ltd.', 'ICT', '', 'Veer Kamesh', 'V', '', '', 'Vice President - Engineering & Head - Quality & Operations', '9845255401', 'veerkamesh@huawei.com', '', '', '', 'www.huawei.com', 'No. 23, Level - 3/4/5/6 & 7, Leela Galleria, The Leela palace ', 'Old Airport Road', '', 'Bangalore', 560008, 'Karnataka', 80, '39808600 Extn. 6407', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(373, '', '', 'Huawei Technologies India Pvt. Ltd.', 'IT', '', 'Nagendran Ramalingam', '', '', 'Director - IT', '', '9845443064', 'nagendranr@huawei.com', '', '', '', '', 'No. 23, Level - 3/4/5/6 & 7, Leela Galleria, The Leela palace ', 'Old Airport Road', '', 'Bangalore', 560008, 'Karnataka', 80, '39808600', '41507025', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(374, '', '', 'Huawei Technologies India Pvt. Ltd.', 'IT', '', 'Naveen', 'Krishnan', '', '', 'Head - Delivery', '9886848946', 'naveen@huawei.com', '', '', '', '', 'No. 23, Level - 3/4/5/6 & 7, Leela Galleria, The Leela palace ', 'Old Airport Road', '', 'Bangalore', 560008, 'Karnataka', 80, '39808600', '41507025', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(375, '', '', 'Huawei Technologies India Pvt. Ltd.', 'IT Services', '', 'Veer Kamesh V', '', '', 'Vice President - Engineering & Head - Quality & Operations', '', '9845255401', 'veerkamesh@huawei.com', '', '', '', '', 'No. 23, Level - 3/4/5/6 & 7, Leela Galleria, The Leela palace ', 'Old Airport Road', '', 'Bangalore', 560008, 'Karnataka', 80, '39808600 Extn. 1148', '41507025', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(376, '', '', 'Huawei Technologies India Pvt. Ltd.', 'IT-Software', '', 'Karthigeyan S', '', '', 'Head - IT Service', '', '7760982422', 'karthik@huawei.com', '', '', '', 'www.huawei.com', 'No. 23, Level - 3/4/5/6 & 7, Leela Galleria, The Leela palace ', 'Old Airport Road', '', 'Bangalore', 560008, 'Karnataka', 80, '39809600 Extn. 7250', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(377, '', '', 'Huawei Technologies India Pvt. Ltd.', 'IT-Software', '', 'Nagendran', 'Ramalingam', '', '', 'Director - IT', '9845443064', 'nagendranr@huawei.com', '', '', '', '', 'No. 23, Level - 3/4/5/6 & 7, Leela Galleria, The Leela palace ', 'Old Airport Road', '', 'Bangalore', 560008, 'Karnataka', 80, '39808600 Extn. 6407', '41507025', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(378, '', '', 'Huawei Technologies India Pvt. Ltd.', 'Telecom', '', 'Gowri Shankar K', '', '', 'Director', '', '9972633500', 'kgowrishankar@huawei.com', '', '', '', '', 'Level 6, Navigator Building, ITPB', '', '', 'Bangalore', 560066, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(379, '', '', 'Huawei Technologies India Pvt. Ltd.', '', '', 'Stanley', '', '', 'Director', '', '9538259446', 'stanleyrajasekaran@gmail.com', '', '', '', '', 'No. 23, Level - 3/4/5/6 & 7, Leela Galleria, The Leela palace ', '', '', 'Bangalore', 560025, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(380, '', '', 'HUL', '', '', 'Kuldeep', '', '', '', '', '9741350507', 'kuldeep.goyal@unilever.com', '', '', '', '', 'No. 23, Level - 3/4/5/6 & 7, Leela Galleria, The Leela palace ', '', '', 'Bangalore', 560078, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(381, '', '', 'HUL', '', '', 'Deepak', '', '', '', '', '9591988985', 'deepak.gopal@tataglobalbeverages.com', '', '', '', '', 'RMZ Ecospace, 4A Block, 5th Floor, Bellandur Village', '', 'Marathahalli', 'Bangalore', 560078, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(382, '', '', 'Hurray', '', '', 'Sanjit Chatterjee', '', '', 'Training', 'CEO', '9900621169', 'sanjit@hurrayedutech.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(383, '', '', 'Hyatt Bangalore', '', '', 'Umesh Salvi', '', '', '', '', '9538871242', 'umesh.salvi@hyatt.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(384, '', '', 'ICD SECURITY SOLUTIONS (I) PVT LTD', '', '', 'Mallikarjuna', '', '', '', '', '080-42898000', 'arjuna.mallik@icdsecurity.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(385, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(386, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(387, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(388, '', '', 'IDEAL SYSTEMS', '', '', 'Binoy Abraham Pazhoor ', '', '', '', '', '9447062750', 'binoypazhoor@pazhoor.com', '', '', '', '', 'No. 151/90, Avvai shanmugham Road,(Llyods Road), Royapettah,', '', '', 'Chennai - 600 014', 0, '', 0, '044 - 2811 0510 / 28', '044 - 2811 0508', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(389, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(390, '', '', 'IDS Next Business Soilutions Pvt. Ltd.', '', '', 'Jolly', '', '', 'Manager - Operations', '', '9886099730', 'jolly.abraham@idsfortune.com', 'Binoy Abraham Pazhoor (Managing Director)', '9447062750', 'binoypazhoor@pazhoor.com', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(391, '', '', 'IDS Next Business Soilutions Pvt. Ltd.', '', '', 'Sandeep', '', '', 'Head - Operations', '', '9986011611', 'sandeep.menon@idsfortune.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(392, '', '', 'IFB Automotive Private Ltd', '', '', 'Sarath chandra ', '', '', '', '', '9379423570', 'sarath@ifbautomotive.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(393, '', '', 'IFB Automotive Private Ltd', '', '', 'Chaitanya Rama P', '', '', '', '', '9379508537', 'chaitanya_p@ifbautomotive.com', '', '', '', '', 'No. 490, HMT Layout', '', 'R T Nagar', 'Bangalore', 560071, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(394, '', '', 'IFB Automotive Private Ltd', '', '', 'Sachin B', '', '', '', 'IT manager', '9379508537', 'sachin_b@ifbautomotive.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(395, '', '', 'IFI Technology Pvt. Ltd.', '', '', 'Rohit', 'Khosla', 'GM Above', '', 'Managing Director', '9845217674', 'rohit@ifihomes.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(396, '', '', 'iGATE Global Solutions ', '', '', 'Jayadev Tenginakai ', '', '', 'Divisional Head - IT Operations', '', '9483161341', 'Jayadev.Tenginakai@igate.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(397, '', '', 'iGATE Global Solutions ', '', '', 'Shivam Chella Namasivayam', '', '', 'VP – IT & IS', '', '9845015367', 'shivam@igate.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(398, '', '', 'iGATE Global Solutions Ltd.', '', '', 'Murthy', 'Josyula', 'GM Above', '', 'Associate Vice President - IMS', '8971122311', 'srirama.josyula@igate.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(399, '', '', 'iGATE Global Solutions Ltd.', '', '', 'Ramesh', 'Chinni', 'Manager Level', '', 'Program Manager', '9844440030', 'ramesh.chinni@igate.com', '', '', '', '', 'No. 490, HMT Layout', '', 'R T Nagar', 'Bangalore', 560001, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(400, '', '', 'iGATE Global Solutions Pvt. Ltd.', 'IT Services', '', 'Gangadhar ', 'Dubey', '', '', 'Project Manager', '9686642383', 'gangadhar.dubey@igate.com', '', '', '', '', 'No. 23, Level - 3/4/5/6 & 7, Leela Galleria, The Leela palace ', '', '', 'Bangalore', 560066, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(401, '', '', 'iGATE Global Solutions Pvt. Ltd.', '', '', 'Girisan', '', '', 'Divisional Head - IV & V', '', '9845190292', 'girisan.ramankutty@igate.com', '', '', '', '', 'Plot No. 155-156 ', '', 'Whitefield', 'Bangalore', 560066, 'Karnataka', 80, '41004747', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(402, '', '', 'Ignis Technology Pvt. Ltd.', '', '', 'Sangram', '', '', '', 'Head - Delivery', '9980144432', 'skrout1974@gmail.com', '', '', '', '', 'No. 490, HMT Layout', '', 'R T Nagar', 'Bangalore', 560071, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(403, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(404, '', '', 'INCAP CONTRACT MANUFACTURING', '', '', 'Akarsh Praveen Raj, (Manager IT, Purchasing Officer)', '', '', '', '', '9731310941', 'akarsh.praveenraj@incapcms.com', '', '', '', 'www.informatica.com', 'No. 66/1, Bagmane Commerz 02, Bagmane Tech Park', '', 'C V Raman Nagar', 'Bangalore', 560093, 'Karnataka', 80, '41708941', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(405, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(406, '', '', 'INCAP CONTRACT MANUFACTURING', '', '', 'Vinay', '', '', '', 'Manager IT', '9686303903', 'it@incapcms.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27');
INSERT INTO `location` (`id`, `Representive`, `RMail`, `Company`, `Sector`, `SubSector`, `FirstName`, `LastName`, `Level`, `Dept`, `Designation`, `Mobile`, `Mail`, `ContactPerson2`, `ContactNumber`, `EmailID`, `Url`, `Address`, `Location`, `SubLocation`, `City`, `Pin`, `State`, `StdCode`, `LandlineNo`, `FaxNo`, `NoOfEmployees`, `CompanyType`, `GSTNo`, `ModificationDetail`) VALUES
(407, '', '', 'Indegene', '', '', 'Gesty Premanand', '', '', '', '', '9980519074', 'jesty@indegene.com', 'Murthy Munipalli (MD)', '8163981100', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(408, '', '', 'Indegene', '', '', 'Gesty Premanand', '', '', '', '', '9980519074', 'jesty@indegene.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(409, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(410, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(411, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(412, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(413, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(414, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(415, '', '', 'Indus Garments India Pvt. Ltd.', '', '', 'Vijay', 'Kumar', 'GM Above', 'Information Technology', 'Head', '9986691786', 'vijaykumarv1786@gmail.com / vijay@indusgarments.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(416, '', '', 'INFILECT TECHNOLOGIES PRIVATE LIMITED', '', '', 'vijay ', '', '', 'Co-Founder', '', '9611610011', 'vijay@infilect.com', '', '', '', '', '66,Rajaji Salai,', '', '', 'Chennai-600 001', 0, '', 0, '044-25222550', '020 - 2566 0551', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(417, '', '', 'Infinera India Private Limited', '', '', 'Bhaskar Pasupuleti', '', '', '', '', '9686452040', 'bpasupuleti@infinera.com', '', '', '', '', '', '', '', 'Chennai - 600036', 0, '', 0, '044-22575551', '044-22578082', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(418, '', '', 'Infinera India Private Limited', '', '', 'Sudhakar', '', '', '', '', '9731113490', 'asudhakar@infinera.com', 'Mr. Sudhakar', '9731113490', 'asudhakar@infinera.com', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(419, '', '', 'Infinite Computer Solutions', '', '', 'Manish Agarwal', '', '', 'AVP ', '', '9686452624', 'Manish@infics.com ', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(420, '', '', 'Inflow Technologies Pvt. Ltd.', '', '', 'R Sujai', 'Kumar', 'Manager Level', '', 'Senior Consultant - Technical', '9342844230', 'sujai.k@inflowtechnologies.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(421, '', '', 'Inflow Technologies Pvt. Ltd.', '', '', 'Ranganathan', 'S', 'Manager Level', '', 'Consultant - Technical', '9739062453', 'ranganathan@inflowtechnologies.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(422, '', '', 'Info way Technologies', '', '', 'Rajan Sangameswaren', '', '', '', 'VP Business enabler/Operations', '91 9482838384', 'rajan.s@infowayme.com', '', '', '', '', '', '', 'Domlur', 'Bangalore', 560103, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(423, '', '', 'Infolog Solutions Pvt. Ltd.', '', '', 'N Vasudeva', 'Rao', 'GM Above', '', 'Deputy General Manager - IT', '9845189566', 'vasu.nerella@infologsolutions.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(424, '', '', 'INFORMATICA', '', '', 'Ashoka', '', '', '', '', '9686964130', 'acj@informatica.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(425, '', '', 'INFORMATICA', '', '', 'Mohd. Illiyas', '', '', '', '', '9972903185', 'milyas@informatica.com', '', '', '', 'www.infowayme.com', 'NSG Towers, 2nd Floor, 9th Main, 80ft Road, Kalyan Nagar, HRBR Layout, Banaswadi, Bangalore – 560 043. INDIA', 'Bangalore', '', 'Bangalore', 0, '', 0, '91 80-42084381', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(426, '', '', 'Informatica Business Solutions Pvt. Ltd.', 'IT', 'Banking', 'Roshan Bhat', '', '', 'Senior Manager - IT Delivery & Development', '', '9886429546', 'rbhat@informatica.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(427, '', '', 'INFOVISION', '', '', 'Srikanta T R', '', '', '', '', '9844122219', 'srikanta@infovision.net', '', '', '', '', 'No. 66/1, Bagmane Commerz 02, Bagmane Tech Park', '', 'C V Raman Nagar', 'Bangalore', 560037, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(428, '', '', 'Ing Vysya', '', '', 'Sandip', '', '', '', '', '91-7259699555', 'sandip.sannyasi@ingvysyabank.com', '', '', '', '', 'Vrindavan Tech Village, 2nd Phase, Aster Building 2A', 'Outer Ring Road', 'Devarabeesanhalli', 'Bangalore', 560037, 'Karnataka', 80, '40819200', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(429, '', '', 'ING Vysya Bank ', '', '', 'Rajesh A Vasudevarao', '', '', 'Technical Architect ', '', '9686259003', 'rajesh.av@ingvysyabank.com ', '', '', '', '', 'Prestige Tech Park, Vathur Hobli', 'Sarjapur Outer Ring Road', 'Marathahalli', 'Bangalore', 560087, 'Karnataka', 80, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(430, '', '', 'ING Vysya Bank ', '', '', 'Karthick Prabhu', '', '', 'Dty Mgr, IT-Delivery ', '', '9886762896', 'karthick.prabhu@inglife.co.in', '', '', '', 'www.infovision.net', '5th Floor, Discoverer, ITP-B(ITPL), Whitefield, Bangalore 560066', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(431, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(432, '', '', 'Ing Vysya Bank Limited', '', '', 'Pradeep Surana', '', '', '', '', '9867708135', 'pradeep.surana@ingvysyabank.com', '', '', '', '', 'Manyatha Embassy Business Park, Level 8, Block C2, Cedar', '', 'Nagavara', 'Bangalore', 560100, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(433, '', '', 'ING Vysya Bank Ltd.', '', '', 'Ramesha', '', '', 'Associate Vice President -  IT Sourcing', 'Associate Vice President -  IT Sourcing', '9980982192  ', 'rameshbabu@ingvysyabank.com', '', '', '', '', '', '', '', 'Bangalore', 560103, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(434, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(435, '', '', 'Ingersoll Rand', '', '', 'Nagesh Javali', '', '', '', '', '9342535956', 'nagesh_javali@irco.com', '', '', '', '', 'No. 22, ITSMG, 4th Floor', '', '', 'Bangalore', 560037, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(436, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(437, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(438, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(439, '', '', 'INTELENET GLOBAL SERVICES', 'BPO', '', 'Satish', '', '', 'Information Technology', 'Manager', '080-66972016/13', 'elwin.dsouza1@intelenetglobal.com', '', '', '', '', '4 A, Dr. Jayalalitha Nagar, Mogappair,', '', '', 'Chennai – 37.', 0, '', 0, '044 – 2656 5991', '044 – 2656 5510', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(440, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(441, '', '', 'International Aerospace Manufacturing Pvt. Ltd.', '', '', 'John Justin', '', '', 'IT - Manager', 'IT - Manager', '91 9741228770/080-25', 'john.justin@iampl.co.in', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(442, '', '', 'Inteva Products India Automotive Private Limited', '', '', 'Govindraj ', '', '', '', '', '080 46552604', 'VGovindraj@intevaproducts.com ', '', '', '', '', 'Survey No.3, Kempapura Village, Varthur Hobli, Kempapura, Bellandur, Bengaluru, Karnataka 560017', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(443, '', '', 'Intuit', '', '', 'Narendra', '', '', '', '', '9945730730', 'naren_r@intuit.com,nratnakaram@ariba.com', '', '', '', '', 'Survey No.3, Kempapura Village, Varthur Hobli, Kempapura, Bellandur, Bengaluru, Karnataka 560017', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(444, '', '', 'Intuit', '', '', 'Nandish Madhu', '', '', '', '', '9972730204', 'Nandish_Madhu@intuit.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(445, '', '', 'Intuit', '', '', 'Velu Muniswamy', '', '', '', '', '99010 57546', 'Velu_Muniswamy@intuit.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(446, '', '', 'Intuit Technology Pvt. Ltd.', '', '', 'Munirathnam', '', '', 'Senior Manager - Developer', 'Senior Manager - Developer', '9880865190', 'munirathnam_chiguruvada@intuit.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(447, '', '', 'Intuit Technology Services Pvt. Ltd.', '', '', 'Rahul', 'Nathan', 'Manager Level', 'Information Technology', 'Manager', '9341921581', 'rahul_nathan@intuit.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(448, '', '', 'Invensys Operations Management', '', '', 'Johncey George', '', '', 'Director - Software Delivery Centre', '', '9845277883', 'johncey.george@skelta.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(449, '', '', 'IonIdea Interactive Pvt. Ltd.', '', '', 'Maruthi Chirravuri', '', '', '', 'Manager IT', '91-9901680596', 'Maruthi.chirravuri@ionidea.com\n\n', '', '', '', '', '', '', 'Electronic City', 'Bangalore', 560103, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(450, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(451, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(452, '', '', 'ISAC', '', '', 'S. RAMANATHAN – Head  CNG', '', '', '', '', '080 25083730', 'srn@isac.gov.in', 'Poornima Jaganath, ', '', 'Poornima.Jaganath@ironmountain.com', 'Eshwarbabu Srinivasan, ', 'eshwarbabu.srinivasan@ironmountain.com', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(453, '', '', 'ISAC', '', '', 'KRISHNAMOORTHY – HEAD -NWG', '', '', '', '', '080 25083717, 3502', 'krish@isac.gov.in', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(454, '', '', 'ISAC', '', '', 'Rakesh Pritmani – Computer Information Group', '', '', '', '', '25083508 / 948102550', 'pritmani@isac.gov.in', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(455, '', '', 'ISAC', '', '', 'RAKESH KUMAR – Computer Information Group', '', '', '', '', '25083732 / 998091361', 'rakesh@isac.gov.in', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(456, '', '', 'ISRO', '', '', 'Muralikrishna', '', '', '', '', '99-86-121142', 'muralikrishna@istrac.gov.in', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(457, '', '', 'ISRO', '', '', 'Joydeep', '', '', '', '', '+91 99-86-121142', 'joydeep@istrac.gov.in', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(458, '', '', 'ISRO HQ', '', '', 'K. Parthasarathy', '', '', '', '', '9845717468', 'parthas@isro.gov.in', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(459, '', '', 'ISRO ISTRAC', '', '', 'Alok Kumar Gupta- Scientist/Engineer-SE COMPUTERS', '', '', '', '', '080 28094416', 'alokgupta@istrac.gov.in', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(460, '', '', 'ISRO ISTRAC', '', '', 'Himanshu Pandey - Scientist/Engineer-SE COMPUTERS', '', '', '', '', '080-2809-4417', 'himanshu@istrac.org', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(461, '', '', 'ISRO -ISTRAC', '', '', 'Joydeep – Head  CNG', '', '', '', '', '080 28094418', 'joydeep@istrac.org', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(462, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(463, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(464, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(465, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(466, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(467, '', '', 'ITC Infotech ', '', '', 'Narendra Kumar B S ', '', '', 'Group Manager Testing Practice ', '', '9945696000', 'narendra.kumar@itcinfotech.com', '', '', '', '', 'A-2, Shree Krishna Nagar Apt, Anna Sandra Palya, Bangalore-17', '', '', 'The director is not present told to call by tommorow', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(468, '', '', 'ITC Infotech India Pvt. Ltd.', '', '', 'Rajarishi', '', '', '', 'Manager - Infra Services', '9739002473', 'rajarishi.av@itcinfotech.com', '', '', '', '', 'ISRO Satellite Centre Indian Space Research Organisation Department of Space, Government of India Airport Road, Vimanapura Post, Bangalore-560017', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(469, '', '', 'ITC Infotech India Pvt. Ltd.', '', '', 'Narendra', '', '', 'General Manager -IT Services', 'General Manager -IT Services', '9886366129', 'narendra.kumar@itcinfotech.com', '', '', '', '', 'No. 23, Level - 3/4/5/6 & 7, Leela Galleria, The Leela palace ', '', '', 'Bangalore', 560078, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(470, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(471, '', '', 'ITTI Pvt. Ltd.', '', '', 'Balan', 'B', 'GM Above', 'Information Technology', 'General Manager', '9945690647', 'balan@itti.com', '', '', '', '', 'No. 23, Level - 3/4/5/6 & 7, Leela Galleria, The Leela palace ', '', '', 'Bangalore', 560048, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(472, '', '', 'ITTI Pvt. Ltd.', '', '', 'Ravi', 'Chander', 'GM Above', 'Information Technology', 'General Manager', '9844093485', 'ravi@itti.com', '', '', '', '', 'No. 18', '', '', 'Bangalore', 560066, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(473, '', '', 'i-Vista Digital Solutions Pvt. Ltd.', '', '', 'Pratiksha', 'Mondle', 'Manager Level', '', 'Account Manager ', '9035679900', 'pratiksha.s@ivistasolutions.com', '', '', '', '', '50A, Greems Road,', '', '', 'Chennai - 600 006', 0, '', 0, '044 - 28292528 / 282', '044 - 28293512', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(474, '', '', 'JAYPEE GROUP', '', '', 'Prabhakar Rao', '', '', '', '', '9342502680', 'prabhakar.rao@jalindia.co.in', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(475, '', '', 'Jda Software Pvt. Ltd.', '', '', 'Nayeem', '', '', '', 'Project Manager', '9900117889', 'd.nayeem.khan@gmail.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(476, '', '', 'Jeans Knit', '', '', 'Mujeeb', '', '', '', '', '9845066079', 'mujeeb@ffi-global.com', '', '', '', '', '', '', '', 'Pondicherry 605006 ', 0, '', 0, 'Phone No:95413-22723', 'Fax:95413-2272067', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(477, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(478, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(479, '', '', 'JMD Business Solutions', '', '', 'shubhankar', '', '', 'Marketing Manager', '', '91-9986424464', 'rajeshvarne@jmdsolutions.co.in', '', '', '', '', 'Jeppiaar Nagar, Old Mamllapuram Road', '', '', 'Chennai -119', 0, '', 0, '044-24501644', '044-24502344', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(480, '', '', 'JNCSAR', '', '', 'Anshumali', '', '', '', '', '8022082938', 'ansumali@jncasr.ac.in', '', '', '', '', '18, Madurai Road, ', '', '', 'Trichirapalli 620008', 0, '', 0, 'Phone: 0431 2701752', 'Fax No: 0431 2701752', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(481, '', '', 'Jockey India Ltd', '', '', 'Vignesh', '', '', '', 'Manager – IT', '9663804442', 'vignesh.sivashankaran@jockeyindia.com', '', '', '', 'http://www.jmdsolutions.co.in', '#405, Pawmana Nagar, Kembathalli Man Road, Bannerghatta Road, Gottigere, Bangalore-560083', '', '', 'the customer told trhat it is a small scale organization so need of the mailing solutions and colaba', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(482, '', '', 'JP Morgan Chase & Co. Pvt. Ltd.', 'Financial Services', '', 'Meera Ramprasad', '', '', 'Manager - IT Services ', '', '9886623443', 'meeram7@gmail.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(483, '', '', 'JP Morgan Chase & Co. Pvt. Ltd.', 'Financial Services', '', 'Venkatesh', 'Venkataraman', '', '', 'Vice President', '8197987337', 'rhodeo@gmail.com', '', '', '', 'www.jockeyindia.com ', 'Abbaiah Reddy Industrial Area Jockey Campus, 6/2 & 6/4 Hongasandra , Bangalore / Bengaluru Karnataka , 560068', 'Bangalore', '', 'Bangalore', 0, '', 0, '91 80- 49464646/ 404', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(484, '', '', 'JP Morgan Chase & Co. Pvt. Ltd.', '', '', 'Meera', '', '', 'Manager - IT Services ', '', '9886623443', 'meeram7@gmail.com', '', '', '', '', 'Prestige Tech Park, Vathur Hobli', 'Sarjapur Outer Ring Road', 'Marathahalli', 'Bangalore', 560087, 'Karnataka', 80, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(485, '', '', 'JP Morgan Chase & Co. Pvt. Ltd.', '', '', 'Venkatesh Venkataraman', '', '', '', 'Vice President', '8197987337', 'rhodeo@gmail.com', '', '', '', '', 'No. 66/1, Bagmane Commerz 02, Bagmane Tech Park', '', 'C V Raman Nagar', 'Bangalore', 560093, 'Karnataka', 80, '41708941', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(486, '', '', 'JS Holding', '', '', 'Mallik', '', '', 'VP-Sales', '', '9036357788', 'mallik@jshomes.in', '', '', '', '', 'Prestige Tech Park, Vathur Hobli', 'Sarjapur Outer Ring Road', 'Marathahalli', 'Bangalore', 560087, 'Karnataka', 80, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(487, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(488, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(489, '', '', 'Jubilant Motor Works Pvt ltd', '', '', 'Mallika Arjun / Chandra Mohan', '', '', '', 'IT Manager / Senior IT Head', '7022007721', 'mallikarjuna_sarma@jubl.com / chandramohan_r@jubl.com ', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(490, '', '', 'Juniper Networks ', '', '', 'Badrinarayanan Jagannathan', '', '', 'Director IT ', '', '9886880955', 'jbadri@juniper.net', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(491, '', '', 'Juniper Networks India Pvt. Ltd.', '', '', 'Amit', 'Vaidya', 'Manager Level', 'Information Technology', 'Manager', '9480186782', 'avaidya@juniper.net', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(492, '', '', 'Juniper Networks India Pvt. Ltd.', '', '', 'Tapan', 'Swain', 'Below Manager', '', 'Engineer - IT', '9972572498', 'tapan.swain@gmail.com', '', '', '', 'www.jubilantmotorworks.com', ':  3rd Floor,138 Residency Road, Bangalore, Field Marshal Cariappa Rd, Ashok Nagar, Bengaluru, Karnataka 560025.', 'Bangalore', '', 'Bangalore', 0, '', 0, '7022007721', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(493, '', '', 'Juniper Networks Pvt. Ltd.', 'Software', '', 'Gunasankar Chinnu', '', '', 'Senior Manager - Configuration', '', '8892640888', 'cguna@juniper.net', '', '', '', '', 'No. 22, ITSMG, 4th Floor', '', '', 'Bangalore', 560037, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(494, '', '', 'Juniper Networks Pvt. Ltd.', 'Software', '', 'Peer S Mohamed', '', '', 'Senior Manager - IT Configuration', '', '9916353056', 'pmohamed@juniper.net', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(495, '', '', 'Juniper Networks Pvt. Ltd.', 'Software', '', 'Peer S', 'Mohamed', '', '', 'Senior Manager - IT Configuration', '9916353056', 'pmohamed@juniper.net', '', '', '', '', 'Plot No. 65/2, 4th Floor, Bagmane Tridib, Bagmane Tech Park', '', 'C V Raman Nagar', 'Bangalore', 560093, 'Karnataka', 80, '30711497', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(496, '', '', 'Juniper Networks Pvt. Ltd.', 'Telecom', '', 'Venkatesan P', '', '', 'Manager - IT & Delivery', '', '9900432196', 'pvenkat@juniper.net', '', '', '', '', 'Plot No. 65/2, 4th Floor, Bagmane Tridib, Bagmane Tech Park', '', 'C V Raman Nagar', 'Bangalore', 560093, 'Karnataka', 80, '30711497', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(497, '', '', 'Juniper Networks Pvt. Ltd.', '', '', 'Gunasankar', '', '', 'Senior Manager - Configuration', '', '8892640888', 'cguna@juniper.net', '', '', '', '', 'Plot No. 65/2, 4th Floor, Bagmane Tridib, Bagmane Tech Park', '', 'C V Raman Nagar', 'Bangalore', 560038, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(498, '', '', 'Juniper Networks Pvt. Ltd.', '', '', 'Vijay', '', '', 'Director ', '', '9845640320', 'vijayk@juniper.net', '', '', '', '', 'Prestige Tech Park, Vathur Hobli', '', 'Marathahalli', 'Bangalore', 560066, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(499, '', '', 'JVS Electronics Pvt Ltd', '', '', 'Bala ', 'Krishna', '', 'Head - Admin', '', '9611787044', 'admin@jvselectronics.in', '', '', '', 'www.lgsoftindia.com', 'Embassy Tech Square', 'Outer Ring Road', 'Marathahalli', 'Bangalore', 560103, 'Karnataka', 80, '66155000', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(500, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(501, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(502, '', '', 'Kalki Technologies ', '', '', 'Sushil Cherian', '', '', '', 'Co-Founder and CTO', '9902691146', 'sushil@kalkitech.com ', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(503, '', '', 'Kamla Dials & Devices Ltd', '', '', 'S K ', 'Manjula', '', '', 'COO', '9632016353', 'sk.manjula@eigeenengneering.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(504, '', '', 'Kannada Prabha', '', '', 'Thippeswamy M R', '', '', '', 'Head IT', '91 9986410375', 'thippeswamy@kannadaprabha.in\n\n', '', '', '', '', '', '', '', 'Bangalore', 560037, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(505, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(506, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(507, '', '', 'Karnataka Agro Chemicals', '', '', 'Hemeswar S', '', '', '', 'EDP Manager', '91 9845288909', 'edp@multiplexgroup.com', '', '', '', 'www.kannadaprabha.com\n\n', 'No-36, Crescent Road, Near Shivananda Circle, Bengaluru Karnataka , 560001', 'Bangalore', '', 'Bangalore', 0, '', 0, '91 80 33101344', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(508, '', '', 'Karnataka State Police Crime Records Bureau', '', '', 'Vinod ', 'Kumar', '', 'System Administrator', '', '9449188390', 'vinodscrb@ksp.gov.in', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(509, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(510, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(511, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(512, '', '', 'Kennametal shared services', '', '', 'Dipu J Pillai', '', '', '', '', '9902096762', 'dipu.pillai@kennametal.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(513, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(514, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(515, '', '', 'KLES Hospital & Medical Research Centre', '', '', 'Girish', '-', '', 'Manager - IT', '', '9886745203', 'support.kles@gmail.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(516, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(517, '', '', 'Kodiak Networks', '', '', 'Viswanath K', '', '', '', '', '9019171088', 'kviswanath@kodiakptt.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(518, '', '', 'Kodiak Networks', '', '', 'Gurudatt G', '', '', '', '', '9448684903', 'GGurudath@kodiakptt.com', '', '', '', 'www.klehospital.org', 'No 50/A 2nd Phase, Ring Road', '', '-', '', 590010, 'Karnataka', 0, '2473777', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(519, '', '', 'KODIAK NETWORKS (INDIA) PVT LTD', '', '', 'V Santosh', '', '', '', '', '080-40005555', 'vsanthosh@kodiaknetworks.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(520, '', '', 'KOHIMAVITTAL FINANCIALS', '', '', 'K.Vittal Shetty', '', '', 'Founder & Chief Executive', '', '91-9845699904/924310', 'kohimavittal@gmail.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(521, '', '', 'Kongovi Electronics Pvt Ltd', '', '', 'Krishnamoorthy', '', '', '', '', '9945241836', 's.krishnamoorthy@kongovi.in', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(522, '', '', 'KONGSBERG', '', '', 'Arjun Alva K', '', '', '', '', '9901973601', 'arjun.alva@advali.no', '', '', '', '', '5, KANMANI ILLAM, I Main, I Cross Sri.Raghavendra Layout, Bannerghatta Road IIMB Post Bangalore 560076', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(523, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(524, '', '', 'KPMG', '', '', 'Mohamed Kashifulla', '', '', '', '', '994 560 7770', 'kashifullamohamed@kpmg.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(525, '', '', 'Kristal Group', '', '', 'Harsha', '', '', '', 'IT Head', '91 9632660022', 'harsha@kristalgroup.com\n\n', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(526, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(527, '', '', 'KUDIFC', '', '', 'Satish Kr Singh', '', '', '', 'App Developer', '9980331062', 'satishkumar@kuidfc.com', '', '', '', 'www.kristalgroup.com\n\n', '#1,4th Cross, 29th Main, BTM II Stage, Bengaluru, Karnataka- 560 076', 'Bangalore', '', 'Bangalore', 0, '', 0, '080-41169142', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(528, '', '', 'KUDREMUKH', '', '', 'Ramanath Shanbhag', '', '', '', '', '9449871539', 'shan_rms@yahoo.com', '', '', '', '', 'KSR Kalvi Nagar, Thokkavady Post', '', '', 'Thiruchengode 637209', 0, '', 0, '04288 274741', '04288 274741, 04288 274745', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(529, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(530, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(531, '', '', 'L&T Valdel Engineering Ltd.', '', '', 'R Munibuddin Md', '', '', '', '', '9663381615', 'munib@Lntvaldel.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(532, '', '', 'L&T-Valdel Engineering Limited', '', '', 'Munib', '', '', '', 'IT Head', '91 9663381615', 'munib@lntvaldel.com\n\n', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(533, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(534, '', '', 'Lam Research (India) Pvt Ltd', '', '', 'Anish Bhanu', '', '', '', '', '9900522559', 'Anish.Bhanu@lamresearch.com', '', '', '', 'www.lntvaldel.com\n\n', 'Shrutha Complex,# 19 Primrose Road, Off M.G Road, Bangalore - 560 025.', 'Bangalore', '', 'Bangalore', 0, '', 0, '91 80-40330000', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(535, '', '', 'Lam Research (India) Pvt Ltd', '', '', 'Ashuthosh Belavadi Nagaraj', '', '', '', '', '9742344454', 'Ashuthosh.BelavadiNagaraj@lamresearch.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(536, '', '', 'Lam Research (India) Pvt Ltd', '', '', 'Vishwanath Solingar Thiruvengadam', '', '', '', '', '9980771975', 'Vishwanath.SolingarThiruvengadam@lamresearch.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(537, '', '', 'Landmark ltd ', '', '', 'Kannapan', '', '', '', '', '9945691969', 'kannappan.v@landmarkgroup.in\n\n', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(538, '', '', 'Langdon & Seah Consulting India Pvt Ltd', '', '', 'Shankar lingam', '', '', '', 'IT Manager', '91 9008980025', 'www.langdonseah.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(539, '', '', 'Larsen & Toubro Infotech Ltd', '', '', 'Rohan ', 'Tandel', '', '', 'Manager - IT', '9869868674', 'tandelrp@lntebg.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(540, '', '', 'Larsen & Turbo Technology Services Pvt. Ltd.', '', '', 'Shivakumar', '', '', 'Senior Manager - Projects', 'Senior Manager - Projects', '9945137253', 'shivakumar.bada@lntinfotech.com', '', '', '', 'shankarlingamb@in.langdonseah.com', '3rd Floor Raheja Chancery, # 133 Brigade Road, Bangalore, Karnataka, India', 'Bangalore', '', 'Bangalore', 0, '', 0, '91 80 41239141', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(541, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(542, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(543, '', '', 'Levi Strauss (India) Pvt Ltd.', '', '', 'Suneel Babu', '', '', '', '', '9342507359', 'sbabu@levi.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(544, '', '', 'LG SOFT INDIA PVT LTD', '', '', 'Prakash KN', '', '', '', '', '9845048632', 'prakash.kn@lge.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(545, '', '', 'LG Soft India Pvt. Ltd.', 'Information technology', '', 'Satya Prakash', '', '', 'Senior Manager - IT & Delivery', '', '9886751257', 'satya.prakash@lge.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(546, '', '', 'LG Soft India Pvt. Ltd.', 'R&D , Mobile.', '', 'Mohan Kumar K R', '', '', 'Senior Officer - Accounts', '', '9880147469', 'mohan.kumar@lge.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(547, '', '', 'LG Soft India Pvt. Ltd.', 'R&D , Mobile.', '', 'Ravi ', 'G H', '', '', 'Assistant Manager - IT', '9880124109', 'ravi.gh@lge.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(548, '', '', 'LG Soft India Pvt. Ltd.', 'Telecom', '', 'Sachin Puranik', '', '', 'Project Manager', '', '9535488614', 'sachin.puranik@gmail.com', '', '', '', 'www.lgsoftindia.com', 'Embassy Tech Square', 'Outer Ring Road', 'Marathahalli', 'Bangalore', 560103, 'Karnataka', 80, '66155000', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(549, '', '', 'LG Soft India Pvt. Ltd.', 'Telecom', '', 'Mohan', 'Kumar K R', '', '', 'Senior Officer - Accounts', '9880147469', 'mohan.kumar@lge.com', '', '', '', '', 'Embassy Tech Square', 'Outer Ring Road', 'Marathahalli', 'Bangalore', 560103, 'Karnataka', 80, '66155000 Extn. 6800', '66155100', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(550, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(551, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(552, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(553, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(554, '', '', 'Lifetree Convergence Ltd. (Technotree)', '', '', 'Shashi Kumar', 'P V', 'GM Above', 'Information Technology', 'General Manager', '9886677887', 'shashi.kumarpv@tecnotree.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(555, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(556, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(557, '', '', 'Logica', '', '', 'P.V. Lakshminarayana', '', '', '', '', '9900600557', 'lakshminarayana.pottumarti@logica.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(558, '', '', 'Logix', '', '', 'R. Prakash', '', '', '', '', '9632222309', 'prakash@logixworld.com', '', '', '', 'www.logica.com', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(559, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(560, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(561, '', '', 'LSI Logic India Pvt Ltd', '', '', 'Srilatha Krishnappa', '', '', '', '', '9886706228', 'Srilatha.Krishnappa@lsi.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(562, '', '', 'LSI Logic India Pvt Ltd', '', '', 'Manju Prasad', '', '', '', '', '9902577881', 'Manju.Prasad@lsi.com', '', '', '', '', '', '', '', 'Chennai - 600 034', 0, '', 0, '0091-044 2817 8200, ', '0091-044 28175566', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(563, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(564, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(565, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(566, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(567, '', '', 'Madhyamam Broadcasting Limited', '', '', 'Rajesh K', '', '', 'Information Technology', 'General Manager', '8943347428', 'rajesh@mediaonetv.in', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(568, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(569, '', '', 'Madura Coats', '', '', 'Satyavrat Mishra', '', '', 'Information Technology', 'General Manager', '9164118700', 'satyavrat.mishra@adityabirla.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(570, '', '', 'Magnosoft (Vendor for HPFS)', '', '', 'Subramanya Prasad', '', '', '', '', '9448824128', 'subramanya.prasad@magnasoft.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(571, '', '', 'Maini Materials', '', '', 'Rama Krishna', '', '', 'Information Technology', 'Manager', '080-4352 6555', 'mmm.systems@maini.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(572, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(573, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(574, '', '', 'Mangalore Refinery and Petrochemicals limited', '', '', 'Arul R.', '', '', '', '', '9448495153', 'arul@mrplindia.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(575, '', '', 'Manhattan Associates ', '', '', 'Saiprasad Chandrashekar', '', '', '', 'Program Director - Global R&D,', '9986066632', 'schandrashekar@manh.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(576, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(577, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(578, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(579, '', '', 'Manipal global', 'Education services', '', 'Santosh madhapur', '', '', 'Procurement', 'Procurement Manager', '9900996403', 'santosh.madapur@manipalglobal.com', '', '', '', '', 'Kuthethur P.O., Via Katipalla, Mangalore-575030', '', '', 'spoke to mr arul he has told to speak to Mr sharad budhale the Cgm .he was busy so told to call alte', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(580, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(581, '', '', 'Manipal Hospital', '', '', 'Ramachandra K', '', '', '', '', '9632033775', 'ramachandra.k@manipalhospitals.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(582, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(583, '', '', 'Manthan Systems India Pvt. Ltd.', 'Information technology', '', 'Bharathi', 'Chaluvadi', '', '', 'Manager - Quality', '9632546242', 'bharathi22in@gmail.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(584, '', '', 'Market xpander', '', '', 'Sudhakar Gorti', '', '', '', 'Chief Product Officer', '9741153900', 'sudhakar@leadsquared.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(585, '', '', 'Marlabs', '', '', 'Anoop Krishnan', '', '', '', '', '7760064488', 'Anoop.Krishnan@marlabs.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(586, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(587, '', '', 'Marlabs Software Pvt. Ltd.', '', '', 'Anil ', '', '', 'General Manager - IT', '', '9845314001 / 9902211', 'anil@marlabs.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(588, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(589, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(590, '', '', 'Mecon Ltd', '', '', 'ThangaPandian.M', '', '', '', 'Manager - IT', '9431118452', 'mtpandian@meconlimited.co.in', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(591, '', '', 'Megatron technologies', '', '', 'Nithin Kumar / Thirumalai', '', '', '', 'IT Manager', '9656210077/ 98427312', 'info@megatrontech.com / nitin.sai@me', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(592, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(593, '', '', 'Mercedes Benz Research and Development India Pvt. Ltd.', 'IT/ ITES', '', 'Hareram Singh', '', '', 'Senior Manager - IT Services & Program', '', '9900040521', 'hare.singh@daimler.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(594, '', '', 'MHPS', '', '', 'Arjun Nayar ', '', '', '', '', '9900-231-385 / +91-9', 'arjun_nayar@mhps.com ', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(595, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(596, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(597, '', '', 'Microchip', '', '', 'Vijay Krishna', '', '', '', '', '080 3090 4444', 'Vijaya.Krishna@microchip.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(598, '', '', 'Microchip', '', '', 'Ganesh Srinivas', '', '', '', '', '9986244822', 'Ganesh.Srinivas@microchip.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(599, '', '', 'Microgenesis', '', '', 'Dhananjaya', '', '', 'Information Technology', 'Director', '9844064665', 'dhananjaya@mgtechsoft.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(600, '', '', 'MicroLabs', '', '', 'Velu M', '', '', '', '', '9845546247', 'velum@microlabs.in', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(601, '', '', 'Microland Ltd', '', '', 'Vincent ', 'Raj', '', '', 'Manager - IT', '9845655243', 'vincents@microland.com; vincent_cct@rediffmail.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(602, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(603, '', '', 'Millipore India Pvt Ltd', '', '', 'Chalapathi', '', '', '', 'Dy System Administrator', '9035028610', 'g.chalapathi@merckgroup.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(604, '', '', 'MILLTEC Machinery', '', '', 'C S ', 'Gopi', '', 'Head - IT', '', '9945648434', 'it@milltecmachinery.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(605, '', '', 'MINACS PRIVATE LIMITED', 'BPO', '', 'Satya (Sathyanarayanan N)', '', '', '', '', '8151095100', 'Sathyanarayanan.N@minacs.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(606, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(607, '', '', 'MINACS PRIVATE LIMITED', 'BPO', '', 'Unnikrishnan Padmanabhan', '', '', '', '', '9900024599', 'unnikrishnan.p@minacs.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(608, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(609, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(610, '', '', 'MINDTREE LTD.', 'IT-SERVICES', '', 'Vikas Mehta (Senior Manager,Finance)', '', '', '', '', '98443 75532', 'vikas_mehta@mindtree.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(611, '', '', 'MINDTREE LTD.', 'IT-SERVICES', '', 'Haran Guptha, (Associate Manager - Procurement)', '', '', '', '', '953 561 2555', 'Haran_Guptha@mindtree.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(612, '', '', 'MINDTREE LTD.', 'IT-SERVICES', '', 'Babu Shetty ', '', '', '', '', '9886550077', 'babu.shetty@mindtree.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(613, '', '', 'Mindtree Ltd.', '', '', 'Arunvignesh', 'Venkatesh', 'Manager Level', '', 'Consultant', '8050535547', 'arunvignesh.venkatesh@mindtree.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(614, '', '', 'Mindtree Ltd.', '', '', 'Ganesan', 'Srinivasan', 'Manager Level', 'Information Technology', 'Manager', '9845253889', 'ganesan.srinivasan@mindtree.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(615, '', '', 'Mineral Enterprises Ltd', '', '', 'T.Sethumadhavan', '', '', '', '', '9880788826', 'sethumadhavan@mel.org.in', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(616, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(617, '', '', 'Misys', '', '', 'Basha', '', '', '', '', '9741200664', 'basha.m@misys.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(618, '', '', 'Mmc Hardmetal India Pvt Ltd', '', '', 'G ', 'Prashanth', '', '', '', '9342594834', 'gprashan@mmc.co.jp', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(619, '', '', 'Moog Controls India', '', '', 'K V ', 'Srinath', '', '', 'Sr System Admin', '9686449139', 'ksrinath@moog.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(620, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(621, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(622, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(623, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(624, '', '', 'Moveinsync', '', '', 'Murali', '', '', 'Information Technology', 'Manager', '9886712256', 'murlimac@moveinsync.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(625, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(626, '', '', 'Mphasis', '', '', 'Lawrence T', '', '', '', '', '9845213573', 'lawrence.t@mphasis.com ', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(627, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(628, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(629, '', '', 'MPHASIS ', '', '', 'Ashok (Senior Manager-Procurement)', '', '', '', '', '9945227635', 'Ashok.kedilaya@mphasis.com ', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(630, '', '', 'Mphasis An HP Co.', 'IT', '', 'Srinivas Nagaraj', '', '', 'Manager - Infrastructure Projects', '', '9845107632', 'srini.nagaraj@gmail.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27');
INSERT INTO `location` (`id`, `Representive`, `RMail`, `Company`, `Sector`, `SubSector`, `FirstName`, `LastName`, `Level`, `Dept`, `Designation`, `Mobile`, `Mail`, `ContactPerson2`, `ContactNumber`, `EmailID`, `Url`, `Address`, `Location`, `SubLocation`, `City`, `Pin`, `State`, `StdCode`, `LandlineNo`, `FaxNo`, `NoOfEmployees`, `CompanyType`, `GSTNo`, `ModificationDetail`) VALUES
(631, '', '', 'Mphasis Ltd.', '', '', 'Krishnan', '', '', '', 'Senior Manager - IT & Delivery', '9886082777', 'krishnan.r@msn.com', '', '', '', '', 'No. 1-2, 1st Floor, UB Plaza', 'Vittal Mallya Road', '', 'Bangalore', 560001, 'Karnataka', 80, '45119000', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(632, '', '', 'Mphasis Ltd.', '', '', 'Neehar ', '', '', '', 'Manager - IT Infrastructure ', '9591626868', 'neehar.choudhury@mphasis.com', '', '', '', '', 'No. 40, 3rd Main, 7th Phase, Shreyas Colony', '', 'J P Nagar', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(633, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(634, '', '', 'Multi Screen Media Pvt. Ltd.', '', '', 'Gautam', '', '', '', 'Senior Manager - IT & Delivery', '9920223996', 'gautama@setindia.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(635, '', '', 'Multiplex Group of Companies', '', '', 'Hemesh', '', '', '', 'Manager-IT', '9845288909', 'edp@multiplexgroup.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(636, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(637, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(638, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(639, '', '', 'Nabler Web Solutions Pvt Ltd', '', '', 'Subramanya', '', '', '', 'Manager', '91 988-655-9526', 'subramanya.b@nabler.com & ithelpdesk@nabler.com\n\n', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(640, '', '', 'Nafasys Technologies Pvt. Ltd.', '', '', 'Kadar', 'Ali', 'GM Above', 'Information Technology', 'Director', '9980065888', 'kadarali@nafasys.com / kadarali@yahoo.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(641, '', '', 'NAL', '', '', 'Vivek-CNSU', '', '', '', '', '25086584', 'vivekba@nal.res.in', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(642, '', '', 'NAL', '', '', 'R P Thangavelu - Sr. Principal Scientist ', '', '', '', '', '80-2505 1908', 'thangam@cmmacs.ernet.in / thangam@csir4pi.in', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(643, '', '', 'Narayana Health Corporate Office', 'Hospital', '', 'Sridharan', '', '', 'IT MANAGER', '', ' 08071 222 222', 'sridharan.s@narayanahealth.org', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(644, '', '', 'Nasdaq Omx Group', 'Finance', '', 'Sujoy', 'Augustine', '', '', 'Manager - Quality Assurance & Corporate Solutions', '9731397365', 'sujoy.augustine@nasdaqomx.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(645, '', '', 'National Aerospace Laboratories - NAL', '', '', 'M ', 'Ravindranath Nayak', '', '', 'System Administrator', '9448482947', 'mrnayak@nal.res.in', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(646, '', '', 'National Dairy Development Board - NDDB', '', '', 'K R ', 'Pillai', '', '', 'Lead  - IT', '9448789168', 'ravirupal@yahoo.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(647, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(648, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(649, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(650, '', '', 'NATIONAL INSTRUMENTS INDIA PRIVATE LIMITED', '', '', 'Vineeth Purushothaman', '', '', '', '', '9902700622', 'vineeth.purushothaman@ni.com', '', '', '', '', '(Southern Region) Taramani P.O.,', '', '', 'Chennai -  600 113.', 0, '', 0, 'Phone: 044  – 2254 1', 'Fax.No: 044 – 2235 0959', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(651, '', '', 'National Law School Of India University Bangalore', '', '', 'Chandra Shekar', '', '', '', 'System Administrator', '9880269181', 'chandrashekar@nls.ac.in', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(652, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(653, '', '', 'Neepa Systems Pvt. Ltd.', '', '', 'Ganesh', 'P', 'GM Above', 'Information Technology', 'Director', '8951803956', 'ganesh@neepasys.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(654, '', '', 'Ness Technologies (India) Pvt Ltd', '', '', 'Nilesh Agarwal', '', '', '', '', '9741533885', 'nilesh.agrawal@ness.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(655, '', '', 'NestAway Technologies Pvt. Ltd.', 'soft ware', '', 'Reddy Raghunath', '', '', 'IT support', '', '7676760000', 'reddy@nestaway.com', '', '', '', '', '', 'HSR', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(656, '', '', 'NetApp ', '', '', 'Siddhartha Nandi ', '', '', '', 'Director ATG', '8861016699', 'nandi@netapp.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(657, '', '', 'NetApp India Pvt. Ltd.', 'IT', '', 'Sireesh Beemineni', '', '', 'Manager - Technical Program ', '', '9448985881', 'sireeshnaidu@gmail.com / beemineni.sireesh@netapp.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(658, '', '', 'NetApp India Pvt. Ltd.', '', '', 'Seshu Kumar', 'Reddy', 'Manager Level', 'Information Technology', 'Program Manager', '9972974477', 'seshur@gmail.com / seshu@netapp.com', '', '', '', 'www.oracle.com ', '', '', '', 'Bangalore', 560037, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(659, '', '', 'NetApp India Pvt. Ltd.', '', '', 'Shivesh', '', '', '', 'Manager - Global Support Account ', '9980835252', 'shivesh@netapp.com', '', '', '', 'www.oracle.com ', 'No. 333, Millenium Tower, Brookefields', 'Kundanahalli Road', 'Mahadevapura', 'Bangalore', 560037, 'Karnataka', 80, '66597040', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(660, '', '', 'NetApp India Pvt. Ltd.', '', '', 'Sireesh', '', '', '', 'Manager - Technical Program ', '9448985881', 'sireeshnaidu@gmail.com / beemineni.sireesh@netapp.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(661, '', '', 'Netscout', '', '', 'Narayan', '', '', '', '', '9986508379', 'Narayana.Pillai@netscout.com>', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(662, '', '', 'NewNet Communications Pvt. Ltd.', '', '', 'Chandrashekhar', '', '', 'Manager - Configuration', '', '9845151204', 'chandrashekhar.magi@newnet.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(663, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(664, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(665, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(666, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(667, '', '', 'Northern Trust', '', '', 'Sunil Kumar GP', '', '', '', '', '9686691955', 'skg7@ntrs.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(668, '', '', 'Northern Trust', '', '', 'Arvind V', '', '', '', '', '9945528162', 'av84@ntrs.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(669, '', '', 'Nous Info Systems ', 'IT ', '', 'suresh john', '', '', 'IT ', 'IT - Manager', ' 080 4260 3000', 'sjohn@nousinfo.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(670, '', '', 'Nous Info Systems ', 'IT HEAD', '', 'Manjunath ', '', '', '', '', ' 080 4260 3000', 'manjun@nousinfo.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(671, '', '', 'Nova Medical Centers Pvt Ltd', '', '', 'K S ', 'Anoop', '', '', 'Sr Manager - System', '9008292510', 'anoop.ks@novamedicalcenters.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(672, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(673, '', '', 'Novell', '', '', 'Edwin', '', '', '', '', '9986424019', 'jedwin@novell.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(674, '', '', 'Novo Nordisk', '', '', 'Wilfred Parakash', '', '', '', '', '8861002792', 'wip@novonordisk.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(675, '', '', 'NovoNordisk', '', '', 'Raju Hosmani', '', '', '', '', '9611186718', 'rjhn@novonordisk.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(676, '', '', 'NTT Data', '', '', 'Yuvraj', '', '', '', '', '9901 911 211 ', 'Yuvaraj.Balasubramani@nttdata.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(677, '', '', 'NTT DATA', '', '', 'Nandha, Chelladurai', '', '', '', '', '080 4210 0400', 'Nandha.Chelladurai@nttdata.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(678, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(679, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(680, '', '', 'Nvidia Graphics', '', '', 'Durgaprasad', '', '', '', '', '9686942717', 'gdurgaprasad@nvidia.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(681, '', '', 'Nvidia Graphics', '', '', 'Virendra Matt', '', '', '', '', '9845455885', 'vmatt@nvidia.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(682, '', '', 'Odense Maritime Technology', '', '', 'Samson S G', '', '', '', '', '9742222214', 'sgsa@odensemaritime.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(683, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(684, '', '', 'OMEGA HEALTHCARE MANAGEMENT', '', '', 'Bangaru Babu ', '', '', '', '', '9845318716', 'Bangaru.Babu@omegahms.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(685, '', '', 'Onmobile Global Limited', '', '', 'Veerakumar', 'Mathivanan', '', '', 'Asst Manager - Systems', '9686675348', 'veerakumar.mathivanan@onmobile.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(686, '', '', 'ONMOBILE GLOBAL LTD', '', '', 'Amitabh Sharma', '', '', '', '', '8884524446', 'Amitabh.sharma@onmobile.com', '', '', '', 'www.onmobile.com', '', '', '', 'Bangalore', 560100, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(687, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(688, '', '', 'Oracle Financial Services Software Ltd.', 'IT', '', 'Vincent', 'Thennarasu Francis', '', '', 'Manager - Technical & Consulting', '9945519071', 'vincent_tf@yahoo.com / vincent.francis@oracle.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(689, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(690, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(691, '', '', 'Panacea Hospital Private Limited', '', '', 'Kiran', '', '', '', 'IT Head', '91 80 4330 7790', 'it@panaceahospital.com\n\n', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(692, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(693, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(694, '', '', 'Pearson', '', '', 'RaviShekharan', '', '', '', '', '9619190876', 'ravi.shekaran@pearson.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(695, '', '', 'Pearson', '', '', 'Aravind', 'Arakasali', '', 'Procurement', 'Manager', '9880608204', 'aravind.arkasali@pearson.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(696, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(697, '', '', 'Philips Electronics India Ltd', '', '', 'Rohit', '', '', '', '', '9845418518', 'rohit.viswanath@philips.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(698, '', '', 'PMC Sierra India Pvt Ltd', '', '', 'A R ', 'Aprameyan', '', '', 'Sr Manager - System', '9845624482', 'aprameyan007@gmail.com; aprameyan.ar@pmcs.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(699, '', '', 'PNRao suits', '', '', 'Jaffer', '', '', '', 'Manager IT', '080-46694100', 'it@pnrao.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(700, '', '', 'Praxair India', '', '', 'Joy Lewis', '', '', '', '', '9379967305', 'joy_lewis@praxair.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(701, '', '', 'Premier Tissues India Ltd', '', '', 'M Raviraj ', 'Rao', '', '', 'Manager - IT', '9448670903', 'ravirajraom@premiertissues.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(702, '', '', 'Prestige Estates Projects Ltd', '', '', 'A ', 'Ravindra', '', 'Head - Admin', '', '9945699036', 'ravindra@prestigeconstructions.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(703, '', '', 'Prestige Golf shire club', '', '', 'Syed.H', '', '', '', 'IT Manager', '91 7259586876', 'syed.h@golfshire.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(704, '', '', 'PricewaterhouseCoopers(Service Delivery Center Bangalore. Pvt lTd)', '', '', 'Prasanth Kizhakedath ', '', '', '', '', '9019808080', ' prasanth.kizhakedath@pwc.com', '', '', '', 'www.golfshire.com', 'Nandi Hills Road, Karahalli Post, Kundana Hobli, Devanahalli Taluk, Bangalore - 562 110', 'Bangalore', '', 'Bangalore', 0, '', 0, '91- 80- 28015800', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(705, '', '', 'PricewaterhouseCoopers(Service Delivery Center Bangalore. Pvt lTd)', '', '', 'Arjun Jayarajan ', '', '', '', '', '9731224843', 'arjun.jayarajan@insdc01.pwc.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(706, '', '', 'Printo', '', '', 'Murugan G', '', '', 'Information Technology', 'Manager', '9008511228', 'murugan.g@printo.in', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(707, '', '', 'Prione Business Services Private Limited', '', '', 'Naveen C', '', '', '', '', '96320 72005', 'naveenc@prione.in', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(708, '', '', 'Processor Systems Pvt. Ltd.', '', '', 'Ganesh', 'S P', 'Manager Level', 'Information Technology', 'Manager', '9845691756', 'ganesh@procsys.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(709, '', '', 'Quad Gen', 'Network', '', 'Avinash Devagan', '', '', 'IT', 'IT Manager', '8688899960', 'avinash.devagan@quadgenwireless.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(710, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(711, '', '', 'Quest', '', '', 'Sunil', '', '', 'Information Technology', 'Manager', '99865 73221', 'sunil.sundaram@quest-global.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(712, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(713, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(714, '', '', 'Quintiles', '', '', 'Srikanth Katuri', '', '', '', '', '9886395922', 'Srikanth.Katuri@Quintiles.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(715, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(716, '', '', 'R V College of Engineering', '', '', 'Prof. Raja Rao', '', '', '', ' College Advisor', '91 80 6717 8147(Dire', 'advisor@rvce.edu.in', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(717, '', '', 'RAMBUS INC', '', '', 'Sukant Mishra', '', '', '', '', '9900579378', 'sukant_mishra@rambus.com/smishra@rambus.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(718, '', '', 'Rambus Ship Technologies(India) Pvt Ltd', '', '', 'Vamsi krishna', '', '', '', '', '89718001900', 'vpolisetty@rambus.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(719, '', '', 'Ramya Prints', '', '', 'T.A. Parashiva Murthy', '', '', 'Operations', 'Chief Operating Officer', '9620280202', 'pmurthy.gm@ramyareprographic.in', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(720, '', '', 'Ranbaxy Laboratories Ltd', '', '', 'Munish Chhabra', '', '', '', '', '9818666862', 'munish.chhabra@ranbaxy.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(721, '', '', 'RCI India Private Limited', '', '', 'Jotendra Kumar', '', '', '', 'IT Head', '+91 9845696299', 'jotendra.kumar@rci.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(722, '', '', 'Red Apple Inc.', '', '', 'Suryanarayana', 'Pullaganti', 'GM Above', 'Information Technology', 'Vice President', '9845174083', 'suryap@redapple-inc.com / surya_pullaganti@yahoo.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(723, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(724, '', '', 'Reva University', '', '', 'Muniswamy', '', '', '', 'Assistant Manager IT', '91 9980051327', 'muniswamy@reva.edu.in\n\n', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(725, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(726, '', '', 'Rinac India', '', '', 'Shivom ', 'Bharathwag', '', '', 'Incharge - IT', '9945234243', 'shivom@rinac.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(727, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(728, '', '', 'Robert BoschEngineering& business Solutions Ltd.', '', '', 'Nagaraj Naik', '', '', '', '', '9739002266', 'nagaraj.naik@in.bosch.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(729, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(730, '', '', 'S P M L Engineering Life', '', '', 'Nagesh Aswartha', '', '', 'Assistant General Manager IT', '', '91-8095588119', 'nageshaswartha@spml.co.in', '', '', '', 'www.spml.co.in', 'Mfar Silverline Techpark, 2nd Floor, Plot No. 180 EPIP Zone-2nd Phase Whitefield, Bangalore 560066', '', '', 'spoke to sidhhi she has given divya email and told to send her the profile and the details as the as', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(731, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(732, '', '', 'SABIC Research & Technology Pvt. Ltd.', '', '', 'Kazim Merchant ', '', '', '', '', '080 67362301', 'kazim.merchant@sabic.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(733, '', '', 'Sabmiller ', '', '', 'Gopala Rao', '', '', '', '', '9731888844', 'Gopalarao.ghanta@in.sabmiller.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(734, '', '', 'Sabre Holdings Pvt Ltd', '', '', 'Balakrishna ', 'Virigineni', '', 'Sr System Admin', '', '8050507717', 'balu4him@gmail.com;balakrishna.virigineni@sabre.com ', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(735, '', '', 'Sabre Travel Technologies ', '', '', 'Nafees Ahmed', '', '', '', 'Program Manager', '9342055058', 'Nafees.Ahmed@sabre-holdings.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(736, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(737, '', '', 'Sai Reniit projects pvt Ltd', 'Construction', '', 'Mayank', '', '', 'IT support', '', '080-65555588', 'info@saireniit.com', '', '', '', '', '', 'HSR', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(738, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(739, '', '', 'Samsung', '', '', 'Amit Kumar Verma', '', '', '', '', '9980510892', 'amitk.verma@samsung.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(740, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(741, '', '', 'san ENGINEERING & LOCOMOTIVE CO. LTD.', '', '', 'NIRUP KUMAR', '', '', '', '', '9886864946', 'nirup@san-engineering.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(742, '', '', 'SANDISK INDIA DEVICE DESIGN CENTRE PRIVATE LIMITED', '', '', 'Jayaprakash Ramakrishna', '', '', '', '', '9986620601', 'jayaprakash.ramakrishna@sandisk.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(743, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(744, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(745, '', '', 'Sanovi Technologies Private Limited', '', '', 'Manjunath ', '', '', '', '', '9845301913', 'manjunath@sanovi.com\n', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(746, '', '', 'Sansera Engineering Pvt Ltd ', '', '', 'Veeresh', '-', '', 'IT', 'Head - IT', '9008520131', 'helpdesk@sanseraindia.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(747, '', '', 'SAP Labs India Private Limited ', '', '', 'Prasada Rao', '', '', '', '', '9886396003', 'rao.prasada@sap.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(748, '', '', 'SAP Labs India Private Limited ', '', '', 'Prakash P L', '', '', '', '', '988639600', 'prakash.p.l@sap.corp', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(749, '', '', 'SAP Labs India Pvt. Ltd.', '', '', 'Nanda', 'Kishore', 'GM Above', 'Information Technology', 'General Manager', '9845703535', 'nandkishor.b.kale@sap.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(750, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(751, '', '', 'Sapient Corporation India Pvt. Ltd.', 'IT Services', '', 'Abhiram', 'Gandhe', '', '', 'Manager - Technology', '8884555138', 'abhiram.gandhe@gmail.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(752, '', '', 'Sasken', '', '', 'Jayadatta Purushotham', '', '', '', '', '9886045407', 'jayadatta.purushothama@sasken.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(753, '', '', 'Sasken ', '', '', 'Sunil dath', '', '', '', '', '9886002009', 'sunil.dath@sasken.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(754, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(755, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(756, '', '', 'Schneider Electric', '', '', 'Prashant Mokashi', '', '', '', '', '9739904067', 'Prashant.Mokashi@schneider-electric.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(757, '', '', 'Schneider Electric India Pvt. Ltd.', '', '', 'Ram paketi', '', '', '', 'Senior Manager - IT & Network Delivery ', '9845099079', 'ram.peketi@schneider-electric.com', '', '', '', '', '4th Floor, Electra A Wing, Exora Business Park', 'Outer Ring Road', 'Marathahalli', 'Bangalore', 560103, 'Karnataka', 80, '43333333', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(758, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(759, '', '', 'Shamrao Vithal Bank', '', '', 'John Koshy', '', '', '', 'IT Manager ', '91-9731880053', 'johnk@svcbank.com \n\n', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(760, '', '', 'Shanti Iron & Steel', '', '', 'Bharamu ', 'Aptekar', '', 'Head - IT', '', '9164578771', 'sis@shantiiron.net', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(761, '', '', 'Sharp Software', '', '', 'Devaraj Govindaraj ', '', '', 'Vice President', '', '9880633135', 'gdevaraj@ssdi.sharp.co.in', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(762, '', '', 'Sharp Software Development India Pvt. Ltd.', '', '', 'Satish Babu', '', '', '', 'Manager - MIS', '9986008580', 'nsatishbabu@ssdi.sharp.co.in', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(763, '', '', 'Shell', '', '', 'Ashwin Mohan', '', '', '', '', '7259016695', 'ashwin.mohan@shell.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(764, '', '', 'Shell Transource Ltd', '', '', 'Sakthivel ', 'Murugan', '', '', 'Manager - IT', '9900041957', 'sakthivel.murugan@vertexgroup.co.in', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(765, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(766, '', '', 'Shirvanthe Technologies pvt.ltd.', '', '', 'Vasudev', '', '', '', 'IT Manager', '91-80-26652003', 'vasudev.p@shirvanthetechonologies.com\n\n', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(767, '', '', 'Shiva Analyticals India Pvt. Ltd.', '', '', 'Kalingaraj', 'Ramaraj', 'Manager Level', 'Information Technology', 'Manager', '9900895000', 'kalingaraj@shivaanalyticals.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(768, '', '', 'Shreeram Properties', '', '', 'Praveen', '', '', 'Information Technology', 'Manager', '7795071095', 'praveen.as@shriramproperties.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(769, '', '', 'Shriram Properties', '', '', 'A.S.Praveen', '', '', '', 'System Administrator', '7795071095', 'praveen@shriramproperties.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(770, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(771, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(772, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(773, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(774, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(775, '', '', 'SIGMA CONVERGENCE TECHNOLOGIES LTD', '', '', 'Kumari R', '', '', '', '', '9008211326', 'kumarin@sigmabyteav.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(776, '', '', 'SIGMA SOFTWARE SOLUTIONS', '', '', 'Prakash', '', '', '', '', '9740076715', 'prakashrao@sigmabyteav.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(777, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(778, '', '', 'SIX DEE TELECOM SOLUTIONS PVT LTD', '', '', 'Soumya Tripathy ', '', '', '', 'IT & Procurement Head', '9880216141', 'Saumya@6dtech.co.in', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(779, '', '', 'SLK BPO', '', '', 'Aravind Patil', '', '', '', '', '9611527004', 'aravind.patil@slkglobalbpo.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(780, '', '', 'SLK BPO', '', '', 'Pankaj Mittal', '', '', '', '', '8888877526', 'pankaj.mittal@slkglobalbpo.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(781, '', '', 'SLK Software', '', '', 'Girisha Ramachandra', '', '', '', '', '9880710001', 'girisha.ramachandra@slk-soft.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(782, '', '', 'SLK Software', '', '', 'Yogesh KM', '', '', '', '', '9880010616', 'km.yogesh@slk-soft.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(783, '', '', 'SLK Software Ltd', '', '', 'Salauddin ', 'Shaikh', '', 'Cheif Manager - IT', '', '9880065840', 'salauddin.s@slk-soft.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(784, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(785, '', '', 'SM Netserv Technologies Private Limited', '', '', 'Venkatesh', 'Venkatesh', '', 'Senior Manager - IT', 'Senior Manager - IT', '9845363232', 'venkatesh@smnetserv.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(786, '', '', 'Snapwiz', '', '', 'Subramanya', '', '', 'Information Technology', 'Administrator', '080-65836677', 'subramanya.naveen@snapwiz.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(787, '', '', 'Snapwiz', '', '', 'Subramanya', '', '', '', 'IT Administrator', '080-65836677', 'subramanya.naveen@snapwiz.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(788, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(789, '', '', 'Societe Generale Global Solution', '', '', 'Muralee Yogaratnam ', '', '', '', 'VP Technology ', '9980120415', 'muralee.yogaratnam@sgcib.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(790, '', '', 'Societe Generale Global Solution', '', '', 'Shashikumar Krishnamurthy', '', '', '', 'AVP-Technical Infrastructure Services', '9980528303', 'shashikumar.krishnamurthy@sgcib.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(791, '', '', 'Societe Generale Global Solution', '', '', 'Muralee Yogaratnam ', '', '', 'VP Technology ', '', '9980120415', 'muralee.yogaratnam@sgcib.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(792, '', '', 'Societe Generale Global Solution', '', '', 'Shashikumar Krishnamurthy', '', '', 'AVP-Technical Infrastructure Services', '', '9980528303', 'shashikumar.krishnamurthy@sgcib.com', '', '', '', '', '', '', '', 'Bangalore', 560045, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(793, '', '', 'Socitie Generale', '', '', 'Kiran Kumar', '', '', '', '', '+91 9620289515', 'kirankumar.hadimani@socgen.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(794, '', '', 'Solutions infini technologies India pvt ltd', '', '', 'Sankar Ganesh', '', '', 'Ext 4041', 'IT Head', '080 4027 5555', 'sankar.g@solutionsinfini.com\n\n', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(795, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(796, '', '', 'Sonata Software Limited', '', '', 'Shekar.V.C', '', '', '', '', '8030972331', 'vcsh@sonata-software.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(797, '', '', 'Sonata Software Ltd', '', '', 'Salauddin', '', '', '', '', '9880065840', 'salauddin.s@sonata-software.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(798, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(799, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(800, '', '', 'SONUS NETWORKS INDIA PVT LTD', '', '', 'Santosh Kamath', '', '', '', '', '9902564001', 'skamath@sonusnet.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(801, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(802, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(803, '', '', 'Sonus Networks India Pvt. Ltd.', '', '', 'Syed', '', '', '', 'Director - IT', '8971189910', 'sbabajan@sonusnet.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(804, '', '', 'Sony', '', '', 'Krishna Murthy', '', '', '', '', '9632788898', 'Krishnamurthy.narasappa@ap.sony.com', '', '', '', '', 'Prestige Shantiniketan, 12th Floor', '', 'Whitefield', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(805, '', '', 'Sony India Pvt. Ltd.', '', '', 'Biswajit', 'Pathak', 'Manager Level', 'Information Technology', 'Project Manager', '7829444463', 'Biswajit.Pathak@ap.sony.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(806, '', '', 'Sony India Pvt. Ltd.', '', '', 'Naveen', 'Rajan', 'Manager Level', 'Information Technology', 'Manager', '9845805505', 'naveen.rajan@ap.sony.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(807, '', '', 'Sony Software Centre', '', '', 'Jaison Joseph ', '', '', '', 'Group Manager  ', '9945191827', 'jaison.j@ap.sony.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(808, '', '', 'SPAN Infotech India Pvt. Ltd.', 'IT', 'Healthcare', 'Renukaprasad Shivarudriah', '', '', 'Project Manager', '', '9448531663', 'prasad_rs@spanservices.com / prasad@spanservices.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(809, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(810, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(811, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(812, '', '', 'Sree Subramanyeswara Co-operative Bank Limited', '', '', 'Premchand Jampani', '', '', '', 'Head - IT', '91 9686240601', 'NA', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(813, '', '', 'Sreenidhi Souharda Sahakari Bank Niyamitha', '', '', 'Lokeshwar Reddy', '', '', '', 'IT Head', '+91 9916999095', 'sssbn@sreenidhibank.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(814, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(815, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(816, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(817, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(818, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(819, '', '', 'St Johns', '', '', 'Deepak', '', '', '', '', '8971971889', 'deepakt@stjohns.in', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(820, '', '', 'St Johns National', '', '', 'Binil K Antony', '', '', '', '', '9886910491', 'binil@sjri.res.in', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(821, '', '', 'Stanadyne Amalgamations Private Ltd.', '', '', 'Krishna', '', '', '', 'Head IT', '91-9445214257', 'srkrishna@stanadyne.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(822, '', '', 'Steer Engineering', 'Manufacturing', '', 'Srinivasan', '', '', '', 'IT Manager', '7829372422', 'srinivasan.mohan@steerworld.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(823, '', '', 'Strides Shasun Private Ltd', '', '', 'Prakash Reddy/Ms.Ramya Raj', '', '', '', '', '080- 66580000', 'Prakasha.n@stridesarco.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(824, '', '', 'SucessFactors Business Solutions India Pvt Ltd', '', '', 'Vivekananda D.V', '', '', '', '', '9972818002', 'vivek@successfactors.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(825, '', '', 'Sudarshan Auto Electrical Components Pvt Ltd', '', '', 'Selva  ', 'Kumar', '', '', 'Manager - Systems', '9486317984', 'selvakumar@sudarshanauto.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(826, '', '', 'sunquest', '', '', 'Pramod Kumar Behura', '', '', '', 'Associate Manager - QC', '9900115985', 'pramod.behura@sunquestinfo.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(827, '', '', 'Super Tex Labels Pvt Ltd', '', '', 'Mahesh Rao', '', '', 'Manager IT', '', '9880042209', 'mahesh@supertexlabels.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(828, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(829, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(830, '', '', 'Symbol Tech', '', '', 'Vijayaraghavan C', '', '', '', '', '9845104333', 'ChannasV@motorolasolutions.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(831, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(832, '', '', 'Symmetrix Computer Systems Pvt Ltd', '', '', 'Vijay ', 'Kumar', '', '', 'Head - Technical', '9845168415', 'vijay@symmetrix.in', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(833, '', '', 'Symphony Services  Pvt Ltd', '', '', 'Praveen Pai', '', '', '', '', '9845009038', 'Praveen.Pai@symphonysv.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(834, '', '', 'Symphony Teleca', '', '', 'Anandeshwar Vellapore', '', '', '', 'Director - Quality Engineering ', '9008002141', 'anandeshwar.vellapore@symphonysv.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(835, '', '', 'Symphony Teleca Corporation Indi', '', '', 'Sandeep', '', '', '', '', '9741943400', 'Sandeep.Vutpala@Symphonyteleca.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(836, '', '', 'Synchronoss Technologies India Pvt. Ltd.', '', '', 'Dinil', 'Antony', 'Manager Level', 'Information Technology', 'Manager', '9880690781 / 9019065', 'dinil.antony@synchronoss.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(837, '', '', 'Syndicate Bank', '', '', 'Kalita.M', '', '', '', 'Head - IT', '9449860120', 'ditsecurity@syndicatebank.co.in;ditprocure@syndicatebank.co.in', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(838, '', '', 'Synopsis', '', '', 'Jayasimha Shatrughna', '', '', '', '', '9845557995', 'Jayasimha.Shatrughna@synopsys.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(839, '', '', 'SYNOPSYS INDIA PVT. LTD.', '', '', 'Umesh S(Finance Manager)', '', '', '', '', '98451 62967', 'Umesh.S@synopsys.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(840, '', '', 'SYNOPSYS INDIA PVT. LTD.', '', '', 'Siva kumar Pydi', '', '', '', '', '9963995118', 'SivaKumar.Pydi@synopsys.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(841, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(842, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(843, '', '', 'T D Power Systems Limited', '', '', 'N.Ramanna', '', '', '', ' IT Manager', '91-9448563046', 'nram@tdps.co.in', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(844, '', '', 'Talisma', '', '', 'Jick John', '', '', '', '', '9731368855', 'jicka@talisma.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(845, '', '', 'TALiSMA', '', '', 'Ravi Prasad S', '', '', '', '', '9845227498', 'raviprasad@talisma.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(846, '', '', 'Talisma Corporation', '', '', 'Rajendra CM', '', '', '', '', '9972393462', 'raj@talisma.com', '', '', '', 'www.talisma.in', '', '', '', 'BANGALORE', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(847, '', '', 'TALISMA CORPORATION PVT LTD', '', '', 'Jins ', '', '', '', '', '9845027953', 'jinsv@talisma.com', '', '', '', 'www.talisma.in', '214/6, Ramanamaharishi Road, Sadashivnagar, Bangalore-560080', '', '', 'spoke to mr natesh has sent the proposal for renewal, waiting from their side', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(848, '', '', 'Talisma Corporation Pvt. Ltd.', '', '', 'Shashidhar', '', '', 'Head - Database Administration', '', '9611107911', 'shachandra@hotmail.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(849, '', '', 'Tally solutions Pvt. Ltd', '', '', 'Ajay Joshi', '', '', '', '', '9900239605', 'ajay.joshi@tallysolutions.com', '', '', '', '', 'No. 214/6', '', 'Sadashivanagar', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(850, '', '', 'Target', '', '', 'Anindya', '', '', '', '', '9686055955', 'Anindya.Dey@target.com>', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(851, '', '', 'Target India', '', '', 'Ganesh Sheth', '', '', '', '', '9686055955', 'ganesh.sheth@target.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(852, '', '', 'Tata Advanced Materials Limited', '', '', 'Nayan Kumar', '', '', '', 'Deputy General Manager - IT   ', '91 9972598591', 'nayan@tamlindia.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(853, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(854, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(855, '', '', 'TAVANT TECHNOLOGIES', '', '', 'Siju', '', '', '', '', '9880239470', 'siju.aravindakshan@tavant.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(856, '', '', 'Tavant Technologies', '', '', 'Antony KG', '', '', '', '', '9742487538', 'antony.kg@tavant.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(857, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(858, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(859, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(860, '', '', 'Team Lease Services PVT Ltd', '', '', 'Anil', '', '', 'Sales', 'AGM', '9448282292', 'anil@asapinfosystems.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(861, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(862, '', '', 'Teamonk', '', '', 'Aravind', '', '', 'Sales', 'Aliance Manager', '9972122552', 'aravind.kudalkar@teamonkglobal.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(863, '', '', 'Tech Mahindra Ltd.', '', '', 'Sanjeev ', '', '', '', 'Head - SQA', '9845204603', 'sanjeevc@techmahindra.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(864, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(865, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(866, '', '', 'Technicolor', '', '', 'Manjunath Naidu', '', '', '', '', '9901199771', 'manjunath.naidu@technicolor.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(867, '', '', 'Technologia - Etisalat Software ', '', '', 'Govinda Raj H', '', '', '', 'Director ', '9845456527', 'govindaraj.h@technologiaworld.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(868, '', '', 'Techser Power Solutions Pvt Ltd', 'Power', '', 'Anil ', 'Kumar', '', 'Manager - IT', '', '9448990429', 'infosupport@techser.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27');
INSERT INTO `location` (`id`, `Representive`, `RMail`, `Company`, `Sector`, `SubSector`, `FirstName`, `LastName`, `Level`, `Dept`, `Designation`, `Mobile`, `Mail`, `ContactPerson2`, `ContactNumber`, `EmailID`, `Url`, `Address`, `Location`, `SubLocation`, `City`, `Pin`, `State`, `StdCode`, `LandlineNo`, `FaxNo`, `NoOfEmployees`, `CompanyType`, `GSTNo`, `ModificationDetail`) VALUES
(869, '', '', 'Tecosim Engg Services Pvt Ltd', '', '', 'Vinith ', 'John', '', 'Head - IT', '', '9986250320', 'v.john@tecosim.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(870, '', '', 'Tele Radiology Solutions Pvt. Ltd.', 'Health care', '', 'Anoop Raj', '', '', 'Manager - Deployment', '', '9036177151', 'anoop.raj@teleradtech.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(871, '', '', 'Teledna', '', '', 'Vinayaka R Moger', '', '', '', '', '9900075957', 'vinayaka@teledna.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(872, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(873, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(874, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(875, '', '', 'Tesco ', '', '', 'Jay Venkatraman', '', '', 'Director - Engineering ', '', '9880298892', 'jayaraman.venkatraman@in.tesco.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(876, '', '', 'Tessolve Semiconductor Pvt. Ltd.', '', '', 'Paulraj', 'Rayappan', 'Manager Level', 'Information Technology', 'Manager', '9738059942', 'paulraj.rayappan@tessolve.com', '', '', '', '', '', '', '', 'Bangalore', 560037, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(877, '', '', 'Tessolve Semiconductor Pvt. Ltd.', '', '', 'Senthil', 'Krishnamoorthy', 'GM Above', 'Information Technology', 'General Manager', '9445191959', 'senthil.krishnamoorthy@tessolve.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(878, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(879, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(880, '', '', 'The Bhavasara Kshatriya Co-Op Bank Ltd', '', '', 'Venkatesh Kakde', '', '', 'Information Technology', 'Head', '9739836871', 'venkateshkakde@gmail.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(881, '', '', 'The Chancery Pavilion', '', '', 'Kiran', '', '', '', 'IT Manager', '91 9611832200', 'kiran.k@chanceryhotels.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(882, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(883, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(884, '', '', 'The Karnataka State Cricket Association', '', '', 'Karthik R', '', '', '', 'IT Head', '080-40154015 / 98446', 'teamit@ksca.co.in  ', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(885, '', '', 'The Park Hotel', '', '', 'Manjunath', '', '', '', 'IT Manager', '91 9916332332', 'system.blr@theparkhotels.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(886, '', '', 'The Paul Hotels & Resorts', '', '', 'Rony Kuriyan', '', '', '', 'IT Manager', '91-9847435813', 'rony@thepaul.in', '', '', '', 'www.theparkhotels.com', 'No.14/7 Mahatma Gandhi Road, Bengaluru, Karnataka 560042', 'Bangalore', '', 'Bangalore', 0, '', 0, '91 80 25594666', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(887, '', '', 'Theorem Software', '', '', 'Praveen Bhadraiah', '', '', '', '', '9448204758', 'praveen@theorem.net', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(888, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(889, '', '', 'THOMSON REUTERS', '', '', 'Pavan Kasi', '', '', '', '', '9845197108', 'pavan.kasi@thomsonreuters.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(890, '', '', 'Thomson Reuters', '', '', 'Pradeep ', '', '', '', '', '9686579387', 'kondakunatha.pradeep@thomsonreuters.com', '', '', '', 'thomsonreuters.com', '', '', '', 'BANGALORE', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(891, '', '', 'Thoughtworks', '', '', 'Anirban', '', '', '', '', '9902586316', 'abhattac@thoughtworks.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(892, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(893, '', '', 'ThoughtWorks Technologies(India) Pvt Ltd', '', '', 'Anirban B', '', '', '', '', '9902586316', 'abhattac@thoughtworks.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(894, '', '', 'ThoughtWorks Technologies(India) Pvt Ltd', '', '', 'Satheesh Babu NS', '', '', '', '', '9900051040', 'snsbabu@thoughtworks.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(895, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(896, '', '', 'Tiribi System Pvt Ltd', '', '', 'Jagdish', '', '', '', 'IT Head', '91-9686189842', 'itsupport@tribi.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(897, '', '', 'TMEIC', '', '', 'Santhosh K', '', '', '', '', '9945684848', 'santhosh.krishnamurthy@tmeic.in', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(898, '', '', 'TNT India', '', '', 'Mallaraj Urs', '', '', '', '', '9880869252', 'mallaraj.urs@tnt.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(899, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(900, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(901, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(902, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(903, '', '', 'TrendMicro India Pvt Ltd', '', '', 'Arun N', '', '', 'IT Admin & QA-Manager', '', '9742090810', 'arun_n@trendmicro.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(904, '', '', 'Trigyn Technologies', '', '', 'Murali', '', '', '', 'IT Manager ', '91 9108551986', 'murali.b@trigyn.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(905, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(906, '', '', 'TVM Signalling', '', '', 'Peter', '', '', 'Information Technology', 'Manager', '8861006040', 'peter.n@tsts.in', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(907, '', '', 'TVM Signalling & Transportation Systems Pvt Ltd', '', '', 'Parameswaran', '', '', 'Head-IT', '', '9900077914', 'parameswaran.k@tsts.in', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(908, '', '', 'TVM Signalling & Transportation Systems Pvt Ltd', '', '', 'Peter ', 'Davis', '', 'Sr Manager - IT', '', '9916706502', 'peter.n@tsts.in', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(909, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(910, '', '', 'Tyco', '', '', 'Anurag', '', '', '', '', '91 9945-144-011', 'anurag.joshi@te.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(911, '', '', 'UKN PROPERTIES PVT. LTD', '', '', 'Siddu Dolli', '', '', '', '', '080 6616 1250', 'edp@ukn.co.in', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(912, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(913, '', '', 'Unisys', '', '', 'Vimal Das', '', '', '', '', '9742844776', 'vimaldas.divakaran@unisys.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(914, '', '', 'Unisys Global India Pvt. Ltd.', '', '', 'Anoop', 'Mavila Chathoth', 'Manager Level', 'Information Technology', 'Senior Consultant', '9663393940', 'anoop.mc@gmail.com / anoop.chathoth@unisys.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(915, '', '', 'Unisys India Pvt. Ltd', '', '', 'Sandeep Kulkarni', '', '', '', '', '9987191535', 'Sandeep.P.Kulkarni@in.unisys.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(916, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(917, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(918, '', '', 'Usha Armour Pvt Ltd', '', '', 'Vishnu ', 'Kumar', '', '', 'Head - IT', '9980847391', 'safety@ushaarmour.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(919, '', '', 'UTC Aerospace Systems Ltd', 'Aerospace', '', 'Brundaban ', 'Acharya', '', 'Manager - IT', '', '9341407717', 'brundaban.acharya@utas.utc.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(920, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(921, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(922, '', '', 'V2Soft Pvt Ltd ', '', '', 'Shankara', '', '', '', 'IT Manager', '91 9945955177', 'snarayana@v2soft.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(923, '', '', 'Vaishnavi Infrastructure Pvt Ltd', '', '', 'Soma Shekar', '', '', '', 'IT Manager', '91- 9845293950', 'somashekhar@vaishnavigroup.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(924, '', '', 'Valtech India Systems Private Limited', '', '', 'Girish Rajasekhariah', '', '', '', 'Head - IT Infrastructure & CISO', '91 9886757704', 'girish.rajasekhariah@valtech.co.in', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(925, '', '', 'Valtech India Systems Pvt Ltd', '', '', 'Girish ', 'Rajasekhar', '', '', 'Senior Administrator - IT', '9886757704', 'girish.rajasekhar@valtech.co.in', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(926, '', '', 'Valtech Software', '', '', 'Girish Rajasekhariah', '', '', '', 'CISO & Head IT & Infrastructure', '9482887888', 'girish.rajasekhar@valtech.co.in', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(927, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(928, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(929, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(930, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(931, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(932, '', '', 'Versa Networks', '', '', 'Chitresh Yadav', '', '', '', '', '9972390736', 'rakesh@versa-networks.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(933, '', '', 'Vertex Customer Solutions pvt Ltd.', '', '', 'Anbarasan', '', '', '', '', '9900041956', 'anbarasan.devanayagam@vertexgroup.co.in', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(934, '', '', 'VF Brands', '', '', 'Pavan Kumar Hosur', '', '', '', '', '8071030137', 'Pavan_Hosur@vfc.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(935, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(936, '', '', 'Vijaya Bank', '', '', 'Lalith', '', '', '', '', '95-38-708870', 'cmdit@vijayabank.co.in', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(937, '', '', 'Vijaya Bank', '', '', 'Prem', '', '', '', '', '91-64-654567', 'cmisdit@vijayabank.co.in', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(938, '', '', 'VIKRAM HOSPITAL', 'Medical', '', 'vinay', '', '', 'IT-manager', '', '9008489234', 'vinay.ts@vikramhospital.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(939, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(940, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(941, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(942, '', '', 'Volvo India Pvt Ltd.', '', '', 'Tapan Mishra', '', '', '', '', '9741179389', 'tapan.mishra@volvo.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(943, '', '', 'Volvo India Pvt Ltd.', '', '', 'Ravi Kumar', '', '', '', '', '9535706568', 'ravi.kumar@volvo.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(944, '', '', 'Volvo India Pvt Ltd.', '', '', 'N Suresh Kumar', '', '', '', '', '9845594282', 'suresh@volvo.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(945, '', '', 'Volvo India Pvt Ltd.', '', '', 'hinnesh', '', '', '', '', '8095175763', 'hineesh.kg.2@volvo.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(946, '', '', 'Volvo India Pvt Ltd.', '', '', 'Ramith', '', '', '', '', '9611099770', 'ramith.r@volvo.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(947, '', '', 'Volvo India Pvt Ltd.', '', '', 'Antony Luiz', '', '', '', '', '9986207376', 'antony.luiz@volvo.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(948, '', '', 'Volvo India Pvt Ltd.', '', '', 'Vishwas', '', '', '', '', '9886768320', 'vishwasj@volvo.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(949, '', '', 'Way2wealth Bokers Pvt Ltd', 'NBFC', '', 'A M ', 'Don Bosco', '', 'Manager - IT', '', '9945227227', 'don@way2wealth.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(950, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(951, '', '', 'Wendt (India) Ltd', '', '', 'Meeran Pastith/ Mr. Subhash Akhawat', '', '', '', 'Manager - IT / IT - Head', '91 9036153000', 'pasithmeeranm@wendtindia.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(952, '', '', 'Wonderla Holidays Ltd.', '', '', 'Koshy Jacob', '', '', '', '', '9880322344', 'koshyjacob@wonderla.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(953, '', '', 'Woodlands Hotel', '', '', 'Rajani', '', '', '', 'Manager IT', '080-40411111', 'info@woodlands.in', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(954, '', '', 'Wrintco', 'soft ware', '', 'Harmeeth', '', '', 'IT support', '', '080-64510812', 'contact@wrintco.in', '', '', '', '', '', 'HSR', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(955, '', '', 'WS Atkins India Private Limited', '', '', 'Anand Kumar', '', '', '', '', '9611786423', 'Anand.Kumar@atkinsglobal.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(956, '', '', 'Wyse Technology Software Development India Pvt Ltd.', '', '', 'Kannan Santhosh', '', '', '', '', '9845998903', 'ksanthosh@wyse.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(957, '', '', 'Wyse Technology Software Development India Pvt Ltd.', '', '', 'Vijaya Sekar', '', '', '', '', '9945242909', 'vsekar@wyse.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(958, '', '', 'Wyse Technology Software Development India Pvt Ltd.', '', '', 'Venu K Y', '', '', '', '', '9880330506', 'VKY@wyse.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(959, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(960, '', '', 'Xerox', '', '', 'Venugopal M', '', '', '', '', '9810602253', 'M.Venugopalan@xerox.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(961, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(962, '', '', 'Xerox Business Services India Pvt. Ltd.', '', '', 'Ahetejaz', 'Khan', 'Below Manager', '', 'Senior Architect - SQA', '7406023786', 'ahetejazahmad.khan@xerox.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(963, '', '', 'Xerox Business Services India Pvt. Ltd.', '', '', 'Deepthy ', 'J', 'Below Manager', '', 'Senior Engineer - SQA', '8861270745', 'Deepthy.Jagadish@xerox.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(964, '', '', 'Xerox Business Services India Pvt. Ltd.', '', '', 'Devanand', 'T R', 'Below Manager', '', 'Senior Engineer - SQA', '9164177654', 'Devanand.Rangaiah@xerox.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(965, '', '', 'Xerox Business Services India Pvt. Ltd.', '', '', 'Shailesh', 'Anand', 'Manager Level', '', 'Project Manager - SQA', '9538951640', 'shailesh.anand@xerox.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(966, '', '', 'Xerox Business Services India Pvt. Ltd.', '', '', 'Shivraj', 'Yadav', 'Below Manager', '', 'Senior Engineer - SQA', '9742576558', 'shivraj.yadav@xerox.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(967, '', '', 'yLeServices', '', '', 'Alok Nayak', '', '', '', 'Software Engineer', '91-9844566559', 'anayak@yleservices.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(968, '', '', 'yLeServices', '', '', 'Vinod Mattakkattu', '', '', 'Team Leader-APPS', '', '91-9379180780', 'vmattakattu@yleservices.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(969, '', '', 'Yokogawa', '', '', 'Gilbert', '', '', '', '', '9945302395', 'Gilbert.Dcunha@yti.yokogawa.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(970, '', '', 'Yokogawa', '', '', 'Nandish Patel', '', '', '', '', '9886262298', 'patil.nandish@in.yokagowa.in', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(971, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(972, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(973, '', '', 'ZEE ENTERTAINMENT ENTERPRISES LTD.', 'Media', 'Entertainment', 'Lakshmipathy', 'K', '', 'Information Technology', 'Assistant Manager', '9538868210', 'lakshmipathy.k@zee.esselgroup.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(974, '', '', 'Zeomega', '', '', 'Madhusudan G J ', '', '', '', '', '9008178266', 'jmadhusudan@zeomega.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(975, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(976, '', '', 'Zerodha', '', '', 'Kailash Chander', '', '', 'Manager Systems –ETS ', '', '9972133800', 'kchander@manh.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(977, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(978, '', '', 'Zinnov Management Consulting Pvt. Ltd.', '', '', 'Aruchamy', 'Jayaraj', 'GM Above', '', 'Head - IT', '9538240053', 'jayaraj@zinnov.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(979, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(980, '', '', 'Zuari Infraworld India Limited (Adventz Group)', '', '', 'Kiran Kumar', '', '', '', 'IT Head', '91-9845662773', 'kiran@adventzinfra.com', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(981, '', '', 'ss', '', '', 'ds', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(982, '', '', '10x Innovative Ltd.', '', '', 'Beslin Gebhone', '', '', '', 'Head - IT', '9986057420', 'beslin@10x.in', '', '', '', '', '', '', 'Whitefield', 'Bengaluru', 560066, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(983, '', '', 'Ace Manufacturing Systems Ltd.', '', '', 'Ram Gopal', '', '', '', 'Manager - IT', '9945230008', 'ramgopal@amslindia.co.in', '', '', '', '', 'No. 153 & 154, 2nd Stage, 4th Phase', '', 'Peenya Industrial Area', 'Bengaluru', 560058, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(984, '', '', 'Aditya Birla Group', '', '', 'Arkalgud Sheshadri', '', '', '', 'Senior Manager - IT', '9483161341', 'sheshadri.a@adityabirla.com', '', '', '', '', 'No. 45, Industry House, 2nd Floor', 'Race Course Road', '', 'Bengaluru', 560001, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(985, '', '', 'Agmatel India Pvt. Ltd.', '', '', 'A Madhu sudhan', '', '', '', 'Manager - IT', '9844220080', 'madhu@agmatel.com', '', '', '', '', 'No. 158/35, Industrial Town', '3rd Main Road', 'Rajaji Nagar', 'Bengaluru', 560068, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(986, '', '', 'Agmatel India Pvt. Ltd.', '', '', 'Shiv Prasad', '', '', '', 'Executive - IT', '9379942553', 'aiplkar@agmatel.com', '', '', '', '', 'No. 158/35, Industrial Town', '3rd Main Road', 'Rajaji Nagar', 'Bengaluru', 560068, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(987, '', '', 'Allegis Services Pvt. Ltd.', '', '', 'Renuka Ptasad', '', '', '', 'Senior Manager - IT', '9880095047', 'prasadzz@gmail.com', '', '', '', '', 'RMZ NXT Campus, Unit No. 401 - 402', 'Whitefield', '', 'Bengaluru', 560066, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(988, '', '', 'Allegis Services Pvt. Ltd.', '', '', 'S Saravana Babu', '', '', '', 'Senior Engineer - IT', '9886011143', 'sseelam@allegisgroup.com', '', '', '', '', 'RMZ NXT Campus, Unit No. 401 - 402', 'Whitefield', '', 'Bengaluru', 560066, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(989, '', '', 'Amantech Chemicals Pvt. Ltd.', '', '', 'Srinivas Marappa', '', '', '', 'Head - IT', '9900024718', 'srinivasm@amante.co.in', '', '', '', '', 'No. 874', 'Sri Krishna Temple Road', 'Indira Nagar', 'Bengaluru', 560038, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(990, '', '', 'AMC Engineering College', '', '', 'Chandra Mouli Venkata Srinivas Akana', '', '', '', 'Head - IT', '9741551588', 'mouliac@yahoo.co.in', '', '', '', '', '', 'Bannerghatta Road', '', 'Bengaluru', 560029, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(991, '', '', 'AMC Group Of Institutions', '', '', 'Sunil V', '', '', '', 'Head - IT', '9986953605', 'sunilv@amceducation.in', '', '', '', '', '18th K M', 'Bannerghatta Road', 'Kalkere', 'Bengaluru', 560083, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(992, '', '', 'Anomishere Design Company Pvt. Ltd.', '', '', 'Raju C P', '', '', '', 'Senior Manager - IT', '9535327533', 'raju@anomishere.com', '', '', '', '', '5th Main Road, Malleshwaram West', '', '', 'Bengaluru', 560003, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(993, '', '', 'ApON India Ltd.', '', '', 'S Hegde', '', '', '', 'Director - IT ', '7019374621', 'shegde@apon.co.in', '', '', '', '', '', '', 'Whitefield', 'Bengaluru', 560066, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(994, '', '', 'Bharat Electronics Ltd.', '', '', 'Shivanna C', '', '', '', 'Manager - IT', '9945101965', 'shivannac@bel.co.in', '', '', '', '', 'SSI / NS, Post Office ', '', 'Jalahalli', 'Bangalore', 560013, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(995, '', '', 'Bharat Heavy Electricals Ltd.', '', '', 'Doraisingh J K', '', '', '', 'Deputy General Manager - IT', '9972060315', 'doraisingh@bheledn.co.in', '', '', '', '', 'Post Box No. - 2606 ', 'Mysore Road', '', 'Bangalore', 560026, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(996, '', '', 'Bridgeicon Pvt. Ltd.', '', '', 'Kiran Kumar S N', '', '', '', 'Head - IT', '9880998684', 'kirankumar_sn@bridgeicon.com', '', '', '', '', '', '', 'Whitefield', 'Bengaluru', 560066, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(997, '', '', 'Canara bank', '', '', 'Nagendra Babu G V S', '', '', '', 'Senior Manager - IT', '9591463135', 'nagendrabgvs@canarabank.com', '', '', '', '', 'No. 14, Naveen Comlex', 'M G Road', '', 'Bengaluru', 560001, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(998, '', '', 'Castle Ind Technologies Pvt. Ltd.', '', '', 'Ashok Kumar', '', '', '', 'Director - IT ', '8951566408', 'ashok@castleind.in', '', '', '', '', 'No. 56/3, Vakil Square', 'Bannerghatta Road', '', 'Bengaluru', 560029, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(999, '', '', 'Center for e-Governance', '', '', 'Dinesh Krishnan', '', '', '', 'Consultant - Program Management', '8747844511', 'cons1-pm.semt@karnataka.gov.in', '', '', '', '', 'MS Buillding, M. G., Dr Ambedkar Veedhi, Ambedkar Veedhi, HBR Layout', '', '', 'Bangalore', 560001, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(1000, '', '', 'Cerner Healthcare Solutions India Pvt. Ltd.', '', '', 'Dinesh Naidu', '', '', '', 'Head - IT', '7899655324', 'arumugabarathi.selvaraj@cerner.com', '', '', '', '', 'Manyatha Embassy Business Park, Level 8, C-2 Block, Cedar ', 'Outer Ring Road ', 'Nagawara', 'Bengaluru', 560045, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(1001, '', '', 'Cerner Healthcare Solutions Pvt. Ltd.', '', '', 'Harsha Srihari', '', '', '', 'Senior Engineer - IT', '9986566257', 'harsha.srihari@cerner.com', '', '', '', '', 'Manyatha Embassy Business Park, Level 8, C-2 Block, Cedar ', 'Outer Ring Road ', 'Nagawara', 'Bangalore', 560045, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(1002, '', '', 'Dhanlaxmi Bank', '', '', 'H P Keshava Murthy ', '', '', '', 'Chief Manager - IT', '9742264102', 'keshvamurthyhp@dhanbank.co.in', '', '', '', '', 'Unit 13,8th Floor ,Innovator Building', 'Whitefield Road', '', 'Bengaluru', 560066, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(1003, '', '', 'Dickinson Fowler Pvt. Ltd.', '', '', 'Ramesh Babu S', '', '', '', 'Senior General Manager - Technical', '9886211775', 'rameshbabu@dickinsonfowler.com', '', '', '', '', 'ommasandra Industrial Area, Electronic City', '', '', 'Bengaluru', 560099, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(1004, '', '', 'Dynam Electro Controls Pvt. Ltd.', '', '', 'Narayana Rao', '', '', '', 'Chief Information Officer', '9945469146', 'n.s.narayanarao@dynam.in', '', '', '', '', '', '', '3rd Phase, Peenya Industrial Area', 'Bengaluru', 560058, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(1005, '', '', 'Ferrocements Pre Fab Ltd.', '', '', 'Prathiksha', '', '', '', 'Director - IT ', '9945513595', 'ferocem@yahoo.com', '', '', '', '', 'No. 14 / C', 'Attibele Industrial Area', '', 'Bengaluru', 562107, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(1006, '', '', 'Ferrocements Pre Fab Ltd.', '', '', 'Ramprasad D S', '', '', '', 'Director - IT ', '9845193440', 'ferocem@yahoo.com', '', '', '', '', 'No. 14 / C', 'Attibele Industrial Area', '', 'Bengaluru', 562107, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(1007, '', '', 'Firstsource solution Ltd.', '', '', 'M Azeezulla Sheriff', '', '', '', 'Deputy General Manager - IT', '9880504875', 'azeezulla.sheriff@firstsource.com', '', '', '', '', '', 'Sarjapura Main Road', '', 'Bengaluru', 560035, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(1008, '', '', 'Fowler Westrup India Pvt. Ltd.', '', '', 'Gunavanth P K', '', '', '', 'Deputy General Manager - IT', '9886896933', 'gunavanth@fowlerwestrup.com', '', '', '', '', 'Plot No. 249/250, 3rd Phase', '', 'Bommasandra Industrial Area', 'Bengaluru', 560099, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(1009, '', '', 'GMR Group Ltd.', '', '', 'Ameen Ahmed', '', '', '', 'Senior Manager - IT', '9980137226', 'cameen@gmail.com', '', '', '', '', 'No.4/1, IBC KP, 9th Floor', 'Bannarghatta Road', '', 'Bengaluru', 560029, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(1010, '', '', 'Gokaldas Exports Ltd.', '', '', 'Manish Shah', '', '', '', 'General Manager - IT', '9900998877', 'manish.shah@gokaldasexports.com', '', '', '', '', 'No. 70', 'Mission Road', '', 'Bengaluru', 560027, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(1011, '', '', 'Golden Seam Apparel Pvt. Ltd.', '', '', 'Vijay Kumar', '', '', '', 'Head - IT', '9986691786', 'vijay@goldenseam.in', '', '', '', '', 'Makali', '', '', 'Bengaluru', 562162, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(1012, '', '', 'HAL Overhaul Division', '', '', 'I K Gupta', '', '', '', 'Deputy General Manager - IT', '8762992510', 'ik.gupta@hal-india.com', '', '', '', '', 'Post Vimanpura', 'Old Airport Road', 'Marathhalli', 'Bangalore', 560017, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(1013, '', '', 'HAL Overhaul Division', '', '', 'Suresh K', '', '', '', 'Engineer - IT', '9901967547', 'suresh_geet@hotmail.com', '', '', '', '', 'Post Vimanpura', 'Old Airport Road', 'Marathhalli', 'Bengaluru', 560017, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(1014, '', '', 'HealthCare Global Ent Ltd.', '', '', 'Gudneppa Sunkad', '', '', '', 'Head - IT', '9611803032', 'gsunkad@hcgoncology.com', '', '', '', '', 'No. 8', 'P Kalingarao Road', 'Samgangi Ram Nagar', 'Bengaluru', 560027, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(1015, '', '', 'Hindustan Aeronautics Ltd.', '', '', 'Chandrasekhar', '', '', '', 'Deputy General Manager - IT', '9480424338', 'chandrasekhar@hal-india.com', '', '', '', '', 'Post Vimanpura', 'Old Airport Road', 'Marathhalli', 'Bangalore', 560017, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(1016, '', '', 'Hindustan Aeronautics Ltd.', '', '', 'Girish M', '', '', '', 'Engineer - IT', '8050926673', 'girishm80@gmail.com', '', '', '', '', 'Post Vimanpura', 'Old Airport Road', 'Marathhalli', 'Bangalore', 560017, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(1017, '', '', 'Hindustan Aeronautics Ltd.', '', '', 'J P Singh', '', '', '', 'Manager - IT', '9535519312', 'jps_2006@rediffmail.com', '', '', '', '', 'Post Vimanpura', 'Old Airport Road', 'Marathhalli', 'Bangalore', 560017, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(1018, '', '', 'Hindustan Aeronautics Ltd.', '', '', 'Manjunatha C R', '', '', '', 'Chief Manager - IT', '9449062415', 'crmanjunatha@hal-india.com', '', '', '', '', 'Post Vimanpura', 'Old Airport Road', 'Marathhalli', 'Bengaluru', 560017, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(1019, '', '', 'Hindustan Aeronautics Ltd.', '', '', 'Sharath Kumar', '', '', '', 'System Administrator', '9980878078', 'it.ohl@hal-india.com', '', '', '', '', 'Post Vimanpura', 'Old Airport Road', 'Marathhalli', 'Bangalore', 560017, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(1020, '', '', 'Infogain India Pvt. Ltd.', '', '', 'Kashinath Ammanna', '', '', '', 'Manager - IT', '9845990485', 'kashiammanna@yahoo.co.in', '', '', '', '', '', '', 'Koramangala', 'Bengaluru', 560034, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(1021, '', '', 'Information Management Services', '', '', 'Sunder Rao', '', '', '', 'Chief Executive Officer', '9886498151', 'sunder@vishwa.biz', '', '', '', '', 'No. 35, 2nd Block, Roche Enclave', '', 'Horamavu', 'Bangalore', 560043, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(1022, '', '', 'Karnataka Bank', '', '', 'Raghunath K', '', '', '', 'Senior Manager - IT', '9900100629', 'raghunath@ktkbank.com', '', '', '', '', 'Data Centre, No. 113', 'K H Road', 'Shanthi Nagar', 'Bengaluru', 560027, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(1023, '', '', 'Karnataka Bank', '', '', 'Suhas M', '', '', '', 'Manager - IT', '9008064479', 'suhasm@ktkbank.com', '', '', '', '', 'Data Centre, No. 113', 'K H Road', 'Shanthi Nagar', 'Bangalore', 560027, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(1024, '', '', 'Karnataka State Financial Corporation', '', '', 'Ramesh Venkataram', '', '', '', 'Senior Manager - IT', '8277377786', 'ramesh@ksfc.in', '', '', '', '', 'No. 1 / 1', 'Thimmaiah Road', 'Vasanth Nagar', 'Bengaluru', 560052, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(1025, '', '', 'KPMG Ltd.', '', '', 'Arun Kumar C M', '', '', '', 'Executive - IT', '9986329769', 'arunkumarcm@kpmg.com', '', '', '', '', '', 'Innner Ring Road', 'Koramangala', 'Bengaluru', 560071, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(1026, '', '', 'KPMG Ltd.', '', '', 'Naveen Kumar', '', '', '', 'Senior Manager - IT', '9845922666', 'naveenk@kpmg.com', '', '', '', '', '', 'Innner Ring Road', 'Koramangala', 'Bengaluru', 560071, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(1027, '', '', 'KPMG Ltd.', '', '', 'Vinod Jayanandan', '', '', '', 'Manager - IT', '9901355446', 'mailtovkj@gmail.com', '', '', '', '', '', 'Innner Ring Road', 'Koramangala', 'Bengaluru', 560071, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(1028, '', '', 'Kurlon Ltd.', '', '', 'Khushroo Engineer', '', '', '', 'Head - IT', '9343717070', 'kfe.ho@kurlon.org', '', '', '', '', 'No. 47, Manipal Centre, 3rd Floor, North Block', 'Dickenson Road', '', 'Bengaluru', 560042, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(1029, '', '', 'Larsen & Toubro Ltd.', '', '', 'Sivakumar Balasubramaniam', '', '', '', 'Senior Manager - IT', '7022021349', 'bsk.mipd@gmail.com', '', '', '', '', 'No. 145 / 2, Strategic Electronic Division, Komarla Solitaire', 'Old Madras Road', 'C V Raman Nagar', 'Bengaluru', 560093, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(1030, '', '', 'MIPL Global', '', '', 'Ajay Kumar', '', '', '', 'Senior Manager - IT', '9902020130', 'ajaykumar.h@mast.co.in', '', '', '', '', ' A, 16, 8th Main Rd, 2nd Phase', '', 'Malleshwaram West', 'Bengaluru', 560003, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(1031, '', '', 'MIPL Global', '', '', 'Praveen P', '', '', '', 'Head - IT', '9902020151', 'praveen.p@mast.co.in', '', '', '', '', ' A, 16, 8th Main Rd, 2nd Phase', '', 'Malleshwaram West', 'Bengaluru', 560003, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(1032, '', '', 'Money Process Tech', '', '', 'Manoharan P', '', '', '', 'Director - IT ', '8453545168', 'manoharanp@live.com', '', '', '', '', 'No. 45, 4th Cross, Pukraj Layout', 'Bannerghatta Road', '', 'Bengaluru', 560030, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(1033, '', '', 'Nokia Networks', '', '', 'Niranjan Dhanakoti', '', '', '', 'Senior Manager - IT Procurement', '9886412076', 'niranjan.dhanakoti@nokia.com', '', '', '', '', 'VTV Park', 'Outer Ring Road', 'Surjapur, Marathahalli', 'Bengaluru', 560103, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(1034, '', '', 'Nokia Solutions & Networks Pvt. Ltd.', '', '', 'Vinodchandra Pandey', '', '', '', 'Senior Manager - IT', '7829917401', 'vinodchandra.pandey@nokia.com', '', '', '', '', 'L - 5, Manyata Business Park', 'Outer Ring Road', 'Nagawara', 'Bengaluru', 560045, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(1035, '', '', 'Procesor Systems Ltd.', '', '', 'Ganesh S P', '', '', '', 'Senior Manager - IT', '9845691756', 'ganesh@procsys.com', '', '', '', '', 'No. 24', 'Richmond Road', '', 'Bengaluru', 560025, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(1036, '', '', 'SM Creative Electronics Ltd.', '', '', 'S Suresh', '', '', '', 'Vice President - IT', '9967057515', 's.suresh@smcel.com', '', '', '', '', '831, 2nd Cross Rd, 2nd Stage, Indiranagar, Koramangala 4th Block, Koramangala', '', '', 'Bengaluru', 560071, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(1037, '', '', 'Soma Enterprise Ltd.', '', '', 'Chaitanya J', '', '', '', 'Senior Manager - IT', '9902010768', 'chaitanyaj@soma.co.in ', '', '', '', '', '7A Veerasandara Industrial Area, Electronic City Phase II, Electronic City', '', '', 'Bengaluru', 560100, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(1038, '', '', 'SS Industries Ltd.', '', '', 'Ravi C M', '', '', '', 'Head - IT', '9986002670', 'it@ssind.in', '', '', '', '', '7A Veerasandara Industrial Area, Electronic City Phase II, Electronic City', '', '', 'Bengaluru', 560100, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(1039, '', '', 'Sun Innovation Systems Pvt. Ltd.', '', '', 'Senthil N', '', '', '', 'Director - IT ', '9902417606', 'senthil@suninnsys.com', '', '', '', '', 'No. 240, 2nd Main', '', 'HAL 3rd Stage', 'Bengaluru', 560075, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(1040, '', '', 'SynergyWin Solutions India Pvt. Ltd.', '', '', 'Prashanth T N', '', '', '', 'Director - IT ', '9880393101', 'prashanth@synergywin.com', '', '', '', '', ' 51, 8th Cross, 1st Main, Srinidhi Layout', '', '', 'Bengaluru', 560062, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(1041, '', '', 'Tata Consultancy Services Ltd.', '', '', 'Prasanth Kumar A', '', '', '', 'Transition Manager', '9900788699', 'prasanth.kumar1@tcs.com', '', '', '', '', 'Explorer Building, ITPL', 'Whitefield Road', '', 'Bengaluru', 560066, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(1042, '', '', 'The Printers Mysore Pvt. Ltd.', '', '', 'Janardhana Reddy Mukkamalla', '', '', '', 'Senior Manager - IT', '9945693749', 'janardhanareddy@printersmysoore.co.in', '', '', '', '', 'No. 75', 'M G Road', '', 'Bengaluru', 560001, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(1043, '', '', 'Torry Harris Business Solution PVt. Ltd.', '', '', 'Sridhar K', '', '', '', 'Senior System Administrator', '9886559050', 'sridhar_kulkarni@thbs.com', '', '', '', '', 'No. 71, Ground Floor, Sona Towers', 'Millers Road', '', 'Bengaluru', 560052, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(1044, '', '', 'Torry Harris Solutions Pvt. Ltd.', '', '', 'Dinesh C G B', '', '', '', 'Senior Manager - IT', '8861004881', 'dinesh_cgb@thbs.com', '', '', '', '', 'No. 71, Ground Floor, Sona Towers', 'Millers Road', '', 'Bengaluru', 560052, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(1045, '', '', 'Translab Innovations Pvt. Ltd.', '', '', 'Jauneet Singh', '', '', '', 'Senior Manager - IT', '8197446622', 'jauneet.singh@translabinnovations.com', '', '', '', '', '165/2, 13th Cross Rd, Vignan Nagar', '', '', 'Bengaluru', 560065, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(1046, '', '', 'TTP Technologies Pvt. Ltd.', '', '', 'Rajendra R', '', '', '', 'Head - IT', '9880857192', 'rajendra@ttpradiators.com', '', '', '', '', 'No. 486, ‘D’, IV Phase, Peenya Industrial Area', '', '', 'Bengaluru', 560058, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(1047, '', '', 'TTP Technologies Pvt. Ltd.', '', '', 'Sathish Shelty', '', '', '', 'Manager - IT', '9880857192', 'info@ttpradiators.com', '', '', '', '', 'No. 486, ‘D’, IV Phase, Peenya Industrial Area', '', '', 'Bengaluru', 560058, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(1048, '', '', 'UTL Technologies', '', '', 'Kishore', '', '', '', 'Principal Consultant', '9742310003', 'annamthyagarajakishore@gmail.com', '', '', '', '', ' #19/6, Ashokpuram School Road, Industrial Suburb', '', '', 'Bengaluru', 560022, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(1049, '', '', 'Vijaya bank', '', '', 'Rupesh Ranjan', '', '', '', 'Manager - IT', '8123832221', 'rupeshranjan@vijayabank.co.in', '', '', '', '', 'No. 41 / 2', 'M G Road', '', 'Bengaluru', 560001, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(1050, '', '', 'Vijaya Bank ', '', '', 'Animesh Kuity', '', '', '', 'Senior Manager - IT', '8884692773', 'animeshkuity@vijayabank.co.in', '', '', '', '', 'No. 41 / 2', 'M G Road', '', 'Bengaluru', 560001, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(1051, '', '', 'Winfantrace Ltd.', '', '', 'Ashwin Kumaar', '', '', '', 'Director - IT ', '9945666136', 'ashwin.kumaar@icloud.com', '', '', '', '', '', '', 'Whitefield', 'Bengaluru', 560066, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(1052, '', '', 'ABB India Ltd.', '', '', 'K N Lakshman', '', '', '', 'Assistant Vice President - IT', '9901491172', 'kn.lakshman@in.abb.com', '', '', '', '', 'No.5 & 6, 2nd Phase', '', 'Peenya Industrial Area', 'Bengaluru', 560058, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(1053, '', '', 'Alcatel Lucent India Ltd.', '', '', 'M R Gurumurthy', '', '', '', 'Deputy General Manager - IT', '9980293764', 'gurumurthy.mr@alcatel-lucent.com', '', '', '', '', 'Silver Oak, 7th Floor, Manyata Embassy', 'Outer Ring Road', 'Nagawara', 'Bengaluru', 560045, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(1054, '', '', 'Alten Calsoft Labs Ltd.', '', '', 'Rahul Raj', '', '', '', 'Head - IT', '9739149850', 'rahull.hk@gmail.com', '', '', '', '', 'No. 196, Behind Reliance Mart', 'Bannerghatta Road', 'Arekere Circle', 'Bengaluru', 560076, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(1055, '', '', 'Amanath Bank', '', '', 'Abdul Baseer', '', '', '', 'Head - IT', '9448015269', 'baseer@email.com', '', '', '', '', 'Ground Floor, Amanath House, Lal ', 'Masjid Rd, Shivaji Nagar', '', 'Bengaluru', 560051, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(1056, '', '', 'Amanath Bank', '', '', 'Altaf Sheikh', '', '', '', 'Senior Manager - IT', '9845918345', 'altaf_sm@yahoo.com', '', '', '', '', 'Ground Floor, Amanath House, Lal ', 'Masjid Rd, Shivaji Nagar', '', 'Bengaluru', 560051, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(1057, '', '', 'Anz Banking Group Ltd.', '', '', 'Venkatesh G S', '', '', '', 'Senior Manager - IT', '9880243420', 'ghattalv@anz.com', '', '', '', '', 'EGL Business Park', '', 'Cheryy Hills', 'Bengaluru', 560017, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(1058, '', '', 'Ascendas services India Pvt. Ltd.', '', '', 'Rajshekar Patil', '', '', '', 'Senior Manager - IT', '9741277661', 'rajshekar.patil@ascendas-singbridge.com', '', '', '', '', '1st Floor', '', 'Whitefield', 'Bengaluru', 560066, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(1059, '', '', 'B L Kashyap & Sons Ltd.', '', '', 'Devendra Yadav', '', '', '', 'Head - IT', '9341220740', 'devendra@blkashyap.com', '', '', '', '', '4th Floor, West Wing Soul Space Paradigm, Near Innovative Multiplex', 'Outer Ring Road', 'Marthahalli', 'Bengaluru', 560037, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(1060, '', '', 'Bank of Maharashtra', '', '', 'Surendran Hari', '', '', '', 'Senior Manager - IT', '8553903012', 'surendranhari@yahoo.com', '', '', '', '', 'No. 15', 'Police Station Road', 'Basawangudi', 'Bengaluru', 560002, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(1061, '', '', 'Bharat Electronics Ltd.', '', '', 'Gopinath Ms', '', '', '', 'Deputy General Manager - IT', '9980073071', 'gopinathms@bel.co.in', '', '', '', '', 'SSI / NS, Post Office ', '', 'Jalahalli', 'Bengaluru', 560013, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(1062, '', '', 'Bharat Heavy Electricals Ltd.', '', '', 'Prabhu Kumar S', '', '', '', 'Senior Deputy General Manager - IT', '9844445680', 'sprabhu@bheledn.co.in', '', '', '', '', 'Post Box No. - 2606 ', 'Mysore Road', '', 'Bengaluru', 560026, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(1063, '', '', 'CAE Simulation Technologies Pvt. Ltd.', '', '', 'Anand Huralikoppi', '', '', '', 'Senior Manager - IT', '9611199839', 'anandph@cae.cm', '', '', '', '', 'Survey No.26 & 27, 2nd & 3rd Floor, Ivc Road', '', 'Devanahalli', 'Bengaluru', 562110, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(1064, '', '', 'Canara bank', '', '', 'Kashif Mohammad', '', '', '', 'Senior Manager - IT', '7483380824', 'kashifmohammad@canarabank.com', '', '', '', '', 'No. 14, Naveen Comlex', 'M G Road', '', 'Bengaluru', 560001, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(1065, '', '', 'Coats India Ltd.', '', '', 'Soundar Rajan', '', '', '', 'Manager - IT', '9916517209', 'Soundarrajan.Thangaraj@coats.com', '', '', '', '', '2 - A Block, 7th Floor, Prestige Tec Park', 'Sarjapur - Marthalli Ring Road', 'Marathahalli', 'Bengaluru', 560037, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(1066, '', '', 'Coffee Day Global Ltd.', '', '', 'Sampath Kumar', '', '', '', 'Manager - IT', '7813815656', 'sampathv@cafecoffeeday.com', '', '', '', '', 'Square 23/2, Vittal Mallya Road', '', '', 'Bengaluru', 560001, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(1067, '', '', 'Deccan Herald', '', '', 'Gourishankar Bedsur', '', '', '', 'Manager - IT', '9844409937', 'gourishankar.bedsur@printersmysore.co.in', '', '', '', '', 'No. 75', 'M G Road', '', 'Bengaluru', 560001, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(1068, '', '', 'Dhanlaxmi Bank', '', '', 'Senthil Velan Murugesan', '', '', '', 'Senior Manager - IT', '9986218928', 'senthilvelanm@dhanbank.co.in', '', '', '', '', 'Unit 13,8th Floor ,Innovator Building', 'Whitefield Road', '', 'Bengaluru', 560066, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(1069, '', '', 'Ernst & Young Ltd.', '', '', 'Pavan Vannur', '', '', '', 'Manager - IT', '9986140755', 'pavansv@gmail.com', '', '', '', '', 'RMZ Infinity', 'Old Madras Road', '', 'Bengaluru', 560016, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(1070, '', '', 'Ernst & Young Ltd.', '', '', 'Praveen Kumar', '', '', '', 'Assistant Director - IT', '9986444873', 'praveen.ramesh@xe02.ey.com', '', '', '', '', 'RMZ Infinity', 'Old Madras Road', '', 'Bengaluru', 560016, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(1071, '', '', 'Evanssion Infotech Pvt. Ltd.', '', '', 'Floyd Britto', '', '', '', 'Vice President - IT', '9632201666', 'floyd@evanssion.com', '', '', '', '', '940, 3rd A Cross Rd, HRBR Layout 1st Block, HRBR Layout, Kalyan Nagar', '', '', 'Bengaluru', 560043, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(1072, '', '', 'Fico Debt Management Solutions', '', '', 'Arindrajit Ray', '', '', '', 'Director - IT ', '9845398271', 'arindrajitray@gmail.com', '', '', '', '', '', '', 'Koramangala', 'Bengaluru', 560034, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(1073, '', '', 'Globaledge Software Pvt. Ltd.', '', '', 'Gurusidhesh Hiremath', '', '', '', 'Head - IT', '9663396450', 'Gurusidhesh.gh@globaledgesoft.com', '', '', '', '', 'Global Village, RVCE Post, Mysore Road', '', '', 'Bengaluru', 560059, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(1074, '', '', 'Harman International', '', '', 'Debtaru Basak', '', '', '', 'Head - IT', '9845188182', 'debtarubasak@yahoo.com', '', '', '', '', 'No. 301, 3B Campus, 3rd Floor, RMZ Ecospace', 'Sarjapur Outer Ring Road', '', 'Bengaluru', 560103, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(1075, '', '', 'Hindalco Industries Ltd.', '', '', 'Debasis Mishra', '', '', '', 'Head - IT', '9964226222', 'debasis.mishra@adityabirla.com', '', '', '', '', '7th Floor, Industry House', 'Race Course Road', '', 'Bengaluru', 560001, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27');
INSERT INTO `location` (`id`, `Representive`, `RMail`, `Company`, `Sector`, `SubSector`, `FirstName`, `LastName`, `Level`, `Dept`, `Designation`, `Mobile`, `Mail`, `ContactPerson2`, `ContactNumber`, `EmailID`, `Url`, `Address`, `Location`, `SubLocation`, `City`, `Pin`, `State`, `StdCode`, `LandlineNo`, `FaxNo`, `NoOfEmployees`, `CompanyType`, `GSTNo`, `ModificationDetail`) VALUES
(1076, '', '', 'Hinduja Global Solutions Ltd.', '', '', 'Vikram S', '', '', '', 'Senior Manager - Servers', '7829911229', 'vikram.srirama@teamhgs.com', '', '', '', '', 'No. 4, 5, 6, HGS Chambers', 'Hosur Road', 'Bomanhalli', 'Bengaluru', 560068, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(1077, '', '', 'Huawei Technologies India Pvt. Ltd.', '', '', 'Stanley Raj', '', '', '', 'Director - Technology', '9538259446', 'stanleyrajasekaran@gmail.com / raj@huawei.com', '', '', '', '', 'No. 23, Leela Galleria, Leela Palace', 'Old Airport Road', 'Kodihalli', 'Bengaluru', 560008, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(1078, '', '', 'IFB Industries Ltd.', '', '', 'Sailesh', '', '', '', 'Head - IT', '9344504377', 'salesh@ifbautomotive.com', '', '', '', '', 'No. 16/17, Visveswaraiah Industrial Estate', 'Whitefield Main Road', 'Mahadevapura', 'Bengaluru', 560048, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(1079, '', '', 'IFB Industries Ltd.', '', '', 'Vasudev Rao', '', '', '', 'Manager - IT', '9343758904', 'vasudev@ifbbangalore.com', '', '', '', '', 'No. 16/17, Visveswaraiah Industrial Estate', 'Whitefield Main Road', 'Mahadevapura', 'Bengaluru', 560048, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(1080, '', '', 'Iker India Pvt. Ltd.', '', '', 'Rohan Gowda', '', '', '', 'Head - IT', '9886363632', 'rohangowda@iker.in', '', '', '', '', '', '', 'Whitefield', 'Bengaluru', 560066, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(1081, '', '', 'Inbiss Solutions Pvt. Ltd.', '', '', 'Mahesh Rao K', '', '', '', 'Chief Technology Officer', '9880334303', 'kmrao_in@yahoo.com', '', '', '', '', 'No. 8', 'Roshanbagh Road', 'V V Puram', 'Bengaluru', 560004, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(1082, '', '', 'India Medtronic Pvt. Ltd.', '', '', 'Lokesh L', '', '', '', 'Senior Manager - IT', '8884622266', 'lokesh.l@medtronic.com', '', '', '', '', '', '', 'Kodbisanhalli', 'Bengaluru', 560037, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(1083, '', '', 'Invictus Technologies Pvt. Ltd.', '', '', 'Chidanand T', '', '', '', 'Chief Technology Officer', '7348962101', 'invictusit@gmail.com', '', '', '', '', '378, 8th Cross, Basaveshwaranagaar Main Road, Near-Petrol Bunk, Rajaji Nagar', '', '', 'Bengaluru', 560044, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(1084, '', '', 'ITC Ltd.', '', '', 'Sulabh Singh', '', '', '', 'Head - IT', '9986115720', 'sulabh.singh@itcinfotech.com', '', '', '', '', 'Pulikeshinagar P.O', '', '', 'Bengaluru', 530005, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(1085, '', '', 'Karnataka Agro Chemicals Multiplex Fertilizers Pvt. Ltd.', '', '', 'Hemashwar', '', '', '', 'Head - IT', '9845288909', 'hemashwar@multiplexgroup.com', '', '', '', '', 'No. 180, 1st Main', '', 'Mahalakshmi Layout', 'Bengaluru', 560086, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(1086, '', '', 'Karnataka Bank', '', '', 'Manohar Hegde', '', '', '', 'Chief Manager - IT', '9448073680', 'hegdema@ktkbank.com', '', '', '', '', 'Data Centre, No. 113', 'K H Road', 'Shanthi Nagar', 'Bengaluru', 560027, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(1087, '', '', 'Kiocl Ltd.', '', '', 'Kesvel M', '', '', '', 'Senior Manager - IT', '9448744485', 'kesavel.m@kioclltd.com', '', '', '', '', '370A, 1st Cross Rd, Santhosapuram, Koramangala 3 Block, Koramangala', '', '', 'Bengaluru', 560034, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(1088, '', '', 'Kongsberg Software & Services Pvt. Ltd.', '', '', 'Arjun Alva', '', '', '', 'Senior Manager - IT', '9901973601', 'arjun.alva@kdi.kongsberg.com', '', '', '', '', 'No. 11 & 12 / 1, Maruthi Infro Tech Center, 5th Floor, B - Wing', 'Domlur', '', 'Bengaluru', 560071, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(1089, '', '', 'LG Electronics Ltd.', '', '', 'Chikkegowda B K', '', '', '', 'Head - IT', '7625036384', 'chikkegowda.bk@lge.com', '', '', '', '', 'Embassy Tech Square, Cessna Business Park Internal Road', '', '', 'Bengaluru', 560103, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(1090, '', '', 'Maini Precision Products Pvt. Ltd.', '', '', 'A V Manjunath', '', '', '', 'Head - IT', '9880560946', 'hemadri.muddikuppam@masgmr-aerotech.in', '', '', '', '', 'B - 165, 3rd Cross', '', 'Peenaya Industrial Estate', 'Bengaluru', 560058, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(1091, '', '', 'Metro Cash & Carry India Pvt. Ltd.', '', '', 'Sathish Karanth', '', '', '', 'General Manager - Systems', '9845316300', 'sathish.karanth@metro.co.in', '', '', '', '', 'No. 26/5, Industrial Suburbs', '', 'Subramanya Nagar', 'Bengaluru', 560055, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(1092, '', '', 'Micro Labs Ltd..', '', '', 'Velu M', '', '', '', 'Senior Manager - IT', '9845546247', 'velum@microlabs.in', '', '', '', '', 'No. 27', 'Rece Course Road', '', 'Bengaluru', 560001, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(1093, '', '', 'Mineral Enterprises Ltd.', '', '', 'Sethu Madhavan', '', '', '', 'Head - IT', '9880788826', 'sethumadhavan@mel.co.in', '', '', '', '', 'Khanija Bhavan, West Wing, 3rd Floor, No. 49', 'Race Course Road', '', 'Bengaluru', 560001, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(1094, '', '', 'Mithra Souharda Credit Co-Operative Ltd.', '', '', 'K Vittal Shetty', '', '', '', 'Chief Information Officer', '9980305789', 'mithraceo@gmail.com', '', '', '', '', '', '', 'Whitefield', 'Bengaluru', 560066, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(1095, '', '', 'Natural Capsules Ltd.', '', '', 'Mallikarjuna Rao', '', '', '', 'General Manager - IT', '9342508939', 'mailikarjuna@naturalcapsules.com', '', '', '', '', '102, Shreshta Bhumi, 87, K R Road', '', '', 'Bengaluru', 560004, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(1096, '', '', 'Onmobile Global Ltd.', '', '', 'Veerakumar M', '', '', '', 'Manager - IT', '9686675348', 'veerakumar.mathivanan@onmobile.com', '', '', '', '', 'No. 26', 'Bannerghatta Road', 'J P Nagar, 3rd Phase', 'Bengaluru', 560076, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(1097, '', '', 'Paragon Solutions Pvt. Ltd.', '', '', 'Sreedhar G N', '', '', '', 'Senior Manager - IT', '9632322968', 'snarayanaswamy@consultparagon.com', '', '', '', '', 'Vakil Square, 3rd Flr, Rear Wing 56/3, Bannerghatta Road, Gurappanapalya', '', '', 'Bengaluru', 560029, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(1098, '', '', 'Presidency University', '', '', 'Ranjit Singh', '', '', '', 'Head - IT', '9620008111', 'jranjitsingh@gmail.com', '', '', '', '', 'Itgalpur Rajanakunte, Yelahanka', '', '', 'Bengaluru', 560064, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(1099, '', '', 'Puravankara Projects Ltd.', '', '', 'Paul Raj M', '', '', '', 'General Manager - IT', '9845026271', 'paulraj.m@puravankara.com', '', '', '', '', '', 'Ulsoor Road', '', 'Bengaluru', 560025, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(1100, '', '', 'Robert Bosch Engineering & Business Solutions Ltd.', '', '', 'Uday Kumar', '', '', '', 'General Manager - IT Support', '9886077965', 'uday.kumar@in.bosch.com', '', '', '', '', 'Cyberpark, Plot No. 76 & 77', '', 'Phase I', 'Bengaluru', 560100, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(1101, '', '', 'Sap Labs', '', '', 'Vignesh T', '', '', '', 'Manager - IT', '9486627438', 'vignesh.t@sap.com', '', '', '', '', '138, EPIP Zone, Whitefield', '', '', 'Bengaluru', 560066, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(1102, '', '', 'Singapore Telecommunications Ltd.', '', '', 'Senthil Kumar', '', '', '', 'Senior Manager - IT', '9845989292', 'nskumar@singtel.com', '', '', '', '', '', '', 'Whitefield', 'Bengaluru', 560066, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(1103, '', '', 'Systemantics India Pvt. Ltd.', '', '', 'Ram Chandra', '', '', '', 'Chief Information Officer', '9845300593', 'h6abeta@gmail.com', '', '', '', '', 'No. 20, 1 A Cross, 17th A Main', 'J P Nagar', '', 'Bengaluru', 560078, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(1104, '', '', 'Tata Consultancy Services Ltd.', '', '', 'Sriganesh Rao', '', '', '', 'Director - IT ', '9845155800', 'sriganesh.rao@tcs.com', '', '', '', '', 'Explorer Building, ITPL', 'Whitefield Road', '', 'Bengaluru', 560066, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(1105, '', '', 'Tata Teleservices Ltd.', '', '', 'Pratichi Pattnaik', '', '', '', 'Head - IT', '9243499828', 'pratichi.pattnaik@tatatel.co.in', '', '', '', '', '', '', 'Koramangala', 'Bengaluru', 560095, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(1106, '', '', 'The Printers Mysore Pvt. Ltd.', '', '', 'Shambuling Doni', '', '', '', 'Senior Manager - IT', '9448650452', 'shambuling.doni@printersmysore.co.in', '', '', '', '', 'No. 75', 'M G Road', '', 'Bengaluru', 560001, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(1107, '', '', 'Total Environment Building Systems Pvt. Ltd.', '', '', 'Anil Baradia', '', '', '', 'Head - IT', '9901177911', 'anil.baradia@total-environment.com', '', '', '', '', 'No. 78, Imagine Building', 'ITPL Road', 'Whitefield', 'Bengaluru', 560066, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(1108, '', '', 'UBS India', '', '', 'Prasanth A', '', '', '', 'Senior Manager - IT', '9900788699', 'prasanth.kumar@ubs.com', '', '', '', '', '', '', 'Whitefield', 'Bengaluru', 560066, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(1109, '', '', 'United Telecoms Ltd.', '', '', 'Rarameshwar Revanappa', '', '', '', 'Head - IT', '9900431088', 'parameshwar@utlindia.com', '', '', '', '', '18A/19, II, Doddanakundi Industrial Area 2, Doddanekundi, Mahadevapura', '', '', 'Bengaluru', 560048, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(1110, '', '', 'Vertex Customer Solution India Pvt. Ltd.', '', '', 'Sakthivel Murugan', '', '', '', 'Deputy General Manager - IT', '9900041957', 'sakthivel.murugan@vertexgroup.co.in', '', '', '', '', '3rd Floor, TBR Towers', 'New Mission Road, JC Road', '', 'Bengaluru', 560002, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(1111, '', '', 'Vijaya Bank', '', '', 'Santhosh Aili', '', '', '', 'Senior Manager - IT', '9986629696', 'cbsprog11@vijayabank.co.on', '', '', '', '', 'No. 41 / 2', 'M G Road', '', 'Bengaluru', 560001, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(1112, '', '', 'Vijaya Bank', '', '', 'Sridhar Thota', '', '', '', 'Chief Manager - IT', '9480658678', 'sridharthota@vijayabank.co.in', '', '', '', '', 'No. 41 / 2', 'M G Road', '', 'Bengaluru', 560001, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(1113, '', '', 'Wibmo Inc.', '', '', 'Satish Kumar Dwibhashi', '', '', '', 'Vice President - IT', '9900177990', 'satish.dwibhashi@wibmo.com', '', '', '', '', '16/1, 2nd Floor, Cambridge Rd, Halasuru', '', '', 'Bengaluru', 560008, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27'),
(1114, '', '', 'Xerox Business Services India Pvt. Ltd.', '', '', 'Sundeep Kumar Trehon', '', '', '', 'Senior Director - IT', '9731966811', 'sundeep.trehon@xerox.com / sundeep.trehon@atos.net', '', '', '', '', 'Whitefield, ITPL Main Road', '', '', 'Bengaluru', 560066, '', 0, '', '', NULL, NULL, NULL, '2017-11-18 06:59:27');

-- --------------------------------------------------------

--
-- Table structure for table `loginhistory`
--

CREATE TABLE IF NOT EXISTS `loginhistory` (
`slno` int(255) NOT NULL,
  `LoginId` varchar(100) DEFAULT NULL,
  `LoginIn` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `ChkVal` int(11) NOT NULL DEFAULT '0',
  `LoginOut` timestamp NULL DEFAULT NULL,
  `ModificationDetail` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `loginhistory`
--

INSERT INTO `loginhistory` (`slno`, `LoginId`, `LoginIn`, `ChkVal`, `LoginOut`, `ModificationDetail`) VALUES
(1, 'purusottam@navikra.com', '2018-04-30 23:34:25', 1, '2018-04-30 23:35:37', '2018-04-30 23:34:25'),
(2, 'purusottam@navikra.com', '2018-04-30 23:35:22', 1, '2018-04-30 23:35:37', '2018-04-30 23:35:22'),
(3, 'purusottam@navikra.com', '2018-04-30 23:37:40', 1, '2018-04-30 23:46:03', '2018-04-30 23:37:40'),
(4, 'puru@gmail.com', '2018-04-30 23:46:13', 1, '2018-05-02 19:24:34', '2018-04-30 23:46:13'),
(5, 'raghu@navikra.com', '2018-05-01 00:08:20', 1, '2018-05-01 12:22:34', '2018-05-01 00:08:20'),
(6, 'purusottam@navikra.com', '2018-05-01 13:54:07', 1, '2018-05-02 13:23:46', '2018-05-01 13:54:07'),
(7, 'purusottam@navikra.com', '2018-05-02 10:34:15', 1, '2018-05-02 13:23:46', '2018-05-02 10:34:15'),
(8, 'purusottam@navikra.com', '2018-05-02 13:09:14', 1, '2018-05-02 13:23:46', '2018-05-02 13:09:14'),
(9, 'puru@gmail.com', '2018-05-02 13:23:55', 1, '2018-05-02 19:24:34', '2018-05-02 13:23:55'),
(10, 'purusottam@navikra.com', '2018-05-02 13:24:01', 1, '2018-05-02 13:57:30', '2018-05-02 13:24:01'),
(11, 'purusottam@navikra.com', '2018-05-02 19:18:18', 1, '2018-05-02 19:23:18', '2018-05-02 19:18:18'),
(12, 'purusottam@navikra.com', '2018-05-02 19:19:55', 1, '2018-05-02 19:23:18', '2018-05-02 19:19:55'),
(13, 'purusottam@navikra.com', '2018-05-02 19:23:11', 1, '2018-05-02 19:23:18', '2018-05-02 19:23:11'),
(14, 'raghu@navikra.com', '2018-05-02 19:23:37', 1, '2018-05-02 19:27:40', '2018-05-02 19:23:37'),
(15, 'puru@gmail.com', '2018-05-02 19:24:27', 1, '2018-05-02 19:24:34', '2018-05-02 19:24:27'),
(16, 'purusottam@navikra.com', '2018-05-02 19:27:02', 1, '2018-05-02 19:27:07', '2018-05-02 19:27:02'),
(17, 'puru@gmail.com', '2018-05-02 19:27:17', 1, '2018-05-02 19:27:20', '2018-05-02 19:27:17'),
(18, 'raghu@navikra.com', '2018-05-02 19:27:33', 1, '2018-05-02 19:27:40', '2018-05-02 19:27:33'),
(19, 'purusottam@navikra.com', '2018-05-02 19:29:51', 1, '2018-05-02 20:42:53', '2018-05-02 19:29:51'),
(20, 'puru@gmail.com', '2018-05-02 20:43:05', 1, '2018-05-02 20:43:12', '2018-05-02 20:43:05'),
(21, 'purusottam@navikra.com', '2018-05-02 20:44:52', 1, '2018-05-02 20:45:02', '2018-05-02 20:44:52'),
(22, 'raghu@navikra.com', '2018-05-02 20:46:14', 1, '2018-05-02 20:46:29', '2018-05-02 20:46:14'),
(23, 'purusottam@navikra.com', '2018-05-02 20:58:54', 1, '2018-05-02 21:21:40', '2018-05-02 20:58:54'),
(24, 'purusottam@navikra.com', '2018-05-05 09:34:01', 1, '2018-05-07 06:38:45', '2018-05-05 09:34:01'),
(25, 'purusottam@navikra.com', '2018-05-05 10:00:07', 1, '2018-05-07 06:38:45', '2018-05-05 10:00:07'),
(26, 'purusottam@navikra.com', '2018-05-06 14:32:52', 1, '2018-05-07 06:38:45', '2018-05-06 14:32:52'),
(27, 'purusottam@navikra.com', '2018-05-06 14:33:54', 1, '2018-05-07 06:38:45', '2018-05-06 14:33:54'),
(28, 'purusottam@navikra.com', '2018-05-06 14:35:57', 1, '2018-05-07 06:38:45', '2018-05-06 14:35:57'),
(29, 'purusottam@navikra.com', '2018-05-07 06:38:37', 1, '2018-05-07 06:38:45', '2018-05-07 06:38:37'),
(30, 'raghu@navikra.com', '2018-05-07 06:38:53', 0, NULL, '2018-05-07 06:38:53'),
(31, 'purusottam@navikra.com', '2018-05-07 07:35:35', 1, '2018-05-07 08:02:45', '2018-05-07 07:35:35'),
(32, 'raghu@navikra.com', '2018-05-07 08:03:09', 0, NULL, '2018-05-07 08:03:09'),
(33, 'purusottam@navikra.com', '2018-05-07 10:12:33', 0, NULL, '2018-05-07 10:12:33'),
(34, 'raghu@navikra.com', '2018-05-07 10:13:21', 0, NULL, '2018-05-07 10:13:21'),
(35, 'purusottam@navikra.com', '2018-05-07 10:28:30', 0, NULL, '2018-05-07 10:28:30'),
(36, 'purusottam@navikra.com', '2018-05-08 07:49:49', 0, NULL, '2018-05-08 07:49:49'),
(37, 'purusottam@navikra.com', '2018-05-10 05:46:22', 0, NULL, '2018-05-10 05:46:22'),
(38, 'purusottam@navikra.com', '2018-05-13 17:45:34', 0, NULL, '2018-05-13 17:45:34'),
(39, 'purusottam@navikra.com', '2018-05-14 19:19:24', 0, NULL, '2018-05-14 19:19:24');

-- --------------------------------------------------------

--
-- Table structure for table `meeting`
--

CREATE TABLE IF NOT EXISTS `meeting` (
`id` int(255) NOT NULL,
  `Representive` varchar(100) NOT NULL,
  `RMail` varchar(100) NOT NULL,
  `CampaignType` varchar(100) NOT NULL,
  `ProjectName` varchar(100) NOT NULL,
  `Date` date NOT NULL,
  `Time` time NOT NULL,
  `Company` varchar(100) NOT NULL,
  `Sector` varchar(100) DEFAULT NULL,
  `ContactPerson` varchar(100) NOT NULL,
  `Designation` varchar(100) NOT NULL,
  `Mobile` varchar(20) NOT NULL,
  `Mail` varchar(100) NOT NULL,
  `Response` varchar(100) NOT NULL,
  `FollowUp` text NOT NULL,
  `FollowUpDate` date DEFAULT NULL,
  `FollowUpTime` time DEFAULT NULL,
  `Requirement` text NOT NULL,
  `Remarks` text NOT NULL,
  `MeetingStatus` varchar(100) DEFAULT NULL,
  `ModificationDetail` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=57 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `meeting`
--

INSERT INTO `meeting` (`id`, `Representive`, `RMail`, `CampaignType`, `ProjectName`, `Date`, `Time`, `Company`, `Sector`, `ContactPerson`, `Designation`, `Mobile`, `Mail`, `Response`, `FollowUp`, `FollowUpDate`, `FollowUpTime`, `Requirement`, `Remarks`, `MeetingStatus`, `ModificationDetail`) VALUES
(1, 'Raghu Vamsi', 'raghu@navikra.com', 'TeleCalling Campaign', 'asd', '2017-11-16', '16:45:00', 'navikra', NULL, 'korai purusottam', 'tech architect', '7894237332', 'purusottam@navikra.com', 'Positive', 'Yes', NULL, NULL, 'aws', 'aws', NULL, '2017-11-18 06:57:35'),
(2, 'Raghu Vamsi', 'raghu@navikra.com', 'TeleCalling Campaign', 'asd', '2017-11-07', '15:34:00', 'navikra', NULL, 'korai purusottam', 'tech architect', '7894237332', 'purusottam@navikra.com', 'Positive', 'Yes', NULL, NULL, 'aws', 'aws', NULL, '2017-11-18 06:57:35'),
(3, 'Korai Purusottam', 'purusottam@navikra.com', 'Reference Campaign', 'asd', '2017-11-21', '15:04:00', 'abc', NULL, 'puru tiku', 'bkbkb', '768686868', 'hjgjgj@jhgj.com', 'Positive', 'Yes', NULL, NULL, 'aws', 'fdg', NULL, '2017-11-18 06:57:35'),
(4, 'Korai Purusottam', 'purusottam@navikra.com', 'Reference Campaign', 'asd', '2017-11-18', '11:11:00', 'navikra', NULL, 'raju ', '', '', '', 'Positive', 'Yes', NULL, NULL, 'sophos', 'endpoint', NULL, '2017-11-18 06:57:35'),
(5, 'Korai Purusottam', 'purusottam@navikra.com', 'Reference Campaign', 'asd', '2017-11-23', '16:44:00', 'navikra', NULL, 'raju ', '', '', '', 'Positive', 'Yes', NULL, NULL, 'sophos', 'endpoint', NULL, '2017-11-18 06:57:35'),
(6, 'Raghu Vamsi', 'raghu@navikra.com', 'TeleCalling Campaign', 'asd', '2017-11-19', '15:33:00', 'Yantriks India', NULL, 'promod ', '', '', '', 'Positive', 'Yes', NULL, '17:10:00', 'trend', '', NULL, '2017-11-18 07:11:05'),
(7, 'Raghu Vamsi', 'raghu@navikra.com', 'TeleCalling', 'asd', '2017-12-12', '15:02:00', 'Yantriks India', NULL, 'promod ', 'admin', '5656745645', 'sdf@dfgf.cvv', 'Positive', 'Yes', '2017-12-15', '15:00:00', 'jump', 'jump', NULL, '2017-12-04 23:04:57'),
(8, 'Korai Purusottam', 'purusottam@navikra.com', 'Direct', 'startup', '2017-12-08', '12:33:00', 'aaaaaaa', NULL, 'abcd ', 'asd', '3243242343', 'asda@dsf.cvb', 'Positive', 'Yes', '2017-12-09', '15:34:00', '', '', NULL, '2017-12-06 11:36:47'),
(9, 'Korai Purusottam', 'purusottam@navikra.com', 'Direct', 'asd', '2017-12-09', '17:06:00', 'aaaaaaa', NULL, 'abcd ', 'asd', '3243242343', 'asda@dsf.cvb', 'Positive', 'Yes', NULL, NULL, 'sad', 'asda', NULL, '2017-12-06 11:37:28'),
(10, 'Korai Purusottam', 'purusottam@navikra.com', 'Reference', 'asd', '2017-12-26', '18:07:00', 'testcompany', NULL, 'testname ', '', '6575765765', '', 'Positive', 'Yes', '2017-12-29', '18:07:00', '', '', NULL, '2017-12-23 18:33:12'),
(11, 'Korai Purusottam', 'purusottam@navikra.com', 'Reference', 'asd', '2017-12-29', '19:55:00', 'testcompany', NULL, 'testname ', '', '6575765765', '', 'Positive', 'Yes', NULL, NULL, '', '', NULL, '2017-12-23 18:34:21'),
(12, 'Korai Purusottam', 'purusottam@navikra.com', 'Reference', 'test', '2017-12-29', '00:00:00', 'testcompany', NULL, 'testname ', '', '6575765765', '', 'Positive', 'Yes', NULL, NULL, '', '', NULL, '2017-12-23 19:30:55'),
(13, 'Korai Purusottam', 'purusottam@navikra.com', 'Direct', 'test', '2017-12-30', '16:45:00', 'testcompany', NULL, 'testname ', '', '6575765765', '', 'Positive', 'Yes', '2018-01-07', '15:45:00', '', '', NULL, '2017-12-23 19:33:14'),
(14, 'Korai Purusottam', 'purusottam@navikra.com', 'Reference', 'abc', '2017-12-12', '12:03:00', 'testcompany', NULL, 'testname ', '', '6575765765', '', 'Positive', 'Yes', NULL, NULL, '', '', NULL, '2017-12-25 11:40:22'),
(15, 'Korai Purusottam', 'purusottam@navikra.com', 'Reference', 'abc', '2017-12-05', '15:35:00', 'testcompany', NULL, 'testname ', '', '6575765765', '', 'Positive', 'Yes', NULL, NULL, '', '', NULL, '2017-12-25 11:43:27'),
(16, 'Korai Purusottam', 'purusottam@navikra.com', 'Reference', 'abc', '2017-12-19', '14:13:00', 'testcompany', NULL, 'testname ', 'wewe', '6575765765', 'asdasd@sfsd.cvb', 'Positive', 'Yes', NULL, NULL, 'sophos', 'sophos', NULL, '2017-12-25 12:06:24'),
(17, 'Korai Purusottam', 'purusottam@navikra.com', 'Reference', 'abc', '2017-12-06', '12:33:00', 'testcompany', NULL, 'testname ', 'dsad', '6575765765', 'asdasd@sfsd.cvb', 'Positive', 'Yes', NULL, NULL, 'adsad', 'dsadsd', NULL, '2017-12-25 12:18:26'),
(18, 'Korai Purusottam', 'purusottam@navikra.com', 'Direct', 'abc', '2017-12-13', '14:34:00', 'testcompany', NULL, 'testname ', '', '6575765765', '', 'Positive', 'Yes', '2017-12-13', '13:12:00', '', '', NULL, '2017-12-26 05:44:14'),
(20, 'Korai Purusottam', 'purusottam@navikra.com', 'Direct', 'demo', '2017-12-30', '15:43:00', 'Vijaya Bank', NULL, 'Sridhar Thota ', 'Chief Manager - IT', '9480658678', 'sridharthota@vijayabank.co.in', 'Positive', 'Yes', NULL, NULL, '', '', NULL, '2017-12-28 12:28:07'),
(21, 'Korai Purusottam', 'purusottam@navikra.com', 'Direct', 'demo', '2017-12-29', '13:23:00', 'Vijaya Bank', NULL, 'Sridhar Thota ', 'Chief Manager - IT', '9480658678', 'sridharthota@vijayabank.co.in', 'Positive', 'Yes', NULL, NULL, 'asdasd', 'sdasadas', NULL, '2017-12-28 12:32:34'),
(22, 'Korai Purusottam', 'purusottam@navikra.com', 'TeleCalling', 'finaltest', '2017-12-28', '15:42:00', 'aaa', NULL, 'asdsad sdasdwfdsfff', 'sdfsdf', '3243242343', 'sdfs@dfg.bnm', 'Positive', 'Yes', NULL, NULL, 'asdsadad', 'asdsad', NULL, '2017-12-28 13:16:58'),
(23, 'Korai Purusottam', 'purusottam@navikra.com', 'TeleCalling', 'finaltest', '2017-12-28', '16:34:00', 'aaa', 'Hospitality', 'asdsad sdasdwfdsfff', 'sdfsdf', '3243242343', 'sdfs@dfg.bnm', 'Positive', 'Yes', NULL, NULL, 'asadsd', 'asdsad', NULL, '2017-12-28 13:21:25'),
(24, 'Korai Purusottam', 'purusottam@navikra.com', 'Direct', 'asd', '2018-01-04', '16:54:00', 'abc', 'Medical', 'puru tiku', 'bkbkb', '768686868', 'hjgjgj@jhgj.com', 'Positive', 'Yes', NULL, NULL, 'ad', 'fdg', NULL, '2018-01-04 13:10:31'),
(25, 'Korai Purusottam', 'purusottam@navikra.com', 'TeleCalling', 'Medical and Govt', '2018-01-19', '16:34:00', 'abcd', 'Govt', 'abcd ', 'IT Admin', '1111111111', 'abcd@gmail.com', 'Positive', 'Yes', '2018-01-20', '15:45:00', '', '', NULL, '2018-01-18 20:26:52'),
(26, 'Korai Purusottam', 'purusottam@navikra.com', 'TeleCalling', 'Medical and Govt', '2018-01-20', '16:53:00', 'abcd', 'Govt', 'abcd ', 'IT Admin', '1111111111', 'abcd@gmail.com', 'Positive', 'Yes', NULL, NULL, '', '', NULL, '2018-01-18 20:27:32'),
(27, 'Korai Purusottam', 'purusottam@navikra.com', 'Direct', 'Medical and Govt', '2018-01-19', '12:31:00', 'abcd', 'Govt', 'abcd ', '', '1111111111', '', 'Positive', 'Yes', NULL, NULL, '', '', NULL, '2018-01-19 03:25:21'),
(28, 'Korai Purusottam', 'purusottam@navikra.com', 'Direct', 'Medical and Govt', '2018-02-21', '14:34:00', 'Acer India Pvt. Ltd.', 'IT', 'Senthil Kumar Selvarajan', 'Manager', '9886606745', 'senthil.kumar@acer.com', 'Positive', 'Yes', '2018-03-01', '14:34:00', '', '', NULL, '2018-02-10 23:05:34'),
(29, 'Korai Purusottam', 'purusottam@navikra.com', 'Direct', 'Medical and Govt', '2018-02-14', '14:34:00', 'Acer India Pvt. Ltd.', 'IT', 'Senthil Kumar Selvarajan', 'Manager', '9886606745', 'senthil.kumar@acer.com', 'Positive', 'Yes', '2018-02-16', '14:34:00', 'aws', 'confirm urgent', 'Yes', '2018-02-11 20:40:12'),
(30, 'Korai Purusottam', 'purusottam@navikra.com', 'Direct', 'Medical and Govt', '2018-02-16', '17:45:00', 'Acer India Pvt. Ltd.', 'IT', 'Senthil Kumar Selvarajan', 'Manager', '9886606745', 'senthil.kumar@acer.com', 'Positive', 'Yes', '0000-00-00', '00:00:00', 'aws', 'close urgent', 'Yes', '2018-02-11 20:42:55'),
(31, 'Korai Purusottam', 'purusottam@navikra.com', 'Reference', 'Medical and Govt', '2018-02-13', '19:08:00', 'Altisource Business Solutions Pvt. Ltd.', 'IT ', 'Amith Srinath', 'Project Manager', '9886636539', 'amith.srinath@altisource.com', 'Positive', 'Yes', '0000-00-00', '00:00:00', 'crm', 'urgent', 'Yes', '2018-02-13 06:22:24'),
(32, 'Korai Purusottam', 'purusottam@navikra.com', 'Reference', 'Medical and Govt', '2018-02-15', '17:45:00', 'Altisource Business Solutions Pvt. Ltd.', 'IT ', 'Amith Srinath', 'Project Manager', '9886636539', 'amith.srinath@altisource.com', 'Positive', 'Yes', '0000-00-00', '00:00:00', 'crm', '', 'Yes', '2018-02-13 10:25:19'),
(33, 'Korai Purusottam', 'purusottam@navikra.com', 'Digital', 'Medical and Govt', '2018-02-13', '17:07:00', 'Brillio Technologies Pvt. Ltd.', 'IT', 'Ramu ', 'IT Admin', '8884200119', 'ramu.malepati@brillio.com', 'Positive', 'Yes', '0000-00-00', '00:00:00', 'dsa', 'sads', 'Yes', '2018-02-13 10:33:18'),
(34, 'Korai Purusottam', 'purusottam@navikra.com', 'Digital', 'Medical and Govt', '2018-02-13', '18:57:00', 'Brillio Technologies Pvt. Ltd.', 'IT', 'Ramu ', 'IT Admin', '8884200119', 'ramu.malepati@brillio.com', 'Positive', 'Yes', '0000-00-00', '00:00:00', 'df', 'asd', 'Yes', '2018-02-13 10:39:27'),
(35, 'Korai Purusottam', 'purusottam@navikra.com', 'Direct', 'Medical and Govt', '2018-02-13', '15:13:00', '24/7 Customer Pvt. Ltd.', 'IT', 'Vinayak Relekar', 'Director', '9845972305', 'Vinayak.relekar@247-inc.com', 'Positive', 'Yes', '0000-00-00', '00:00:00', 'aws', 'aws', 'Yes', '2018-02-13 12:02:52'),
(36, 'Korai Purusottam', 'purusottam@navikra.com', 'Reference', 'Medical and Govt', '2018-02-13', '14:34:00', '24/7 Customer Pvt. Ltd.', 'IT', 'Vinayak Relekar', 'Director', '9845972305', 'Vinayak.relekar@247-inc.com', 'Positive', 'Yes', '0000-00-00', '00:00:00', 'ibm', 'ibm', 'Yes', '2018-02-13 12:20:55'),
(37, 'Korai Purusottam', 'purusottam@navikra.com', 'Reference', 'Medical and Govt', '2018-02-13', '18:54:00', 'AKAMAI TECHNOLOGIES', 'IT', 'Guru / Prashant D', 'Manager', '9880599530', 'ggudla@akamai.com', 'Positive', 'Yes', '0000-00-00', '00:00:00', 'aws', 'devops', 'Yes', '2018-02-13 13:14:02'),
(38, 'Korai Purusottam', 'purusottam@navikra.com', 'Reference', 'Medical and Govt', '2018-02-13', '15:46:00', 'AKAMAI TECHNOLOGIES', 'IT', 'Guru / Prashant D', 'Manager', '9880599530', 'ggudla@akamai.com', 'Positive', 'Yes', '0000-00-00', '00:00:00', 'aws', 'bill', 'Yes', '2018-02-13 13:15:55'),
(39, 'Korai Purusottam', 'purusottam@navikra.com', 'Direct', 'Medical and Govt', '2018-02-13', '12:33:00', 'Aditya Birla Minacs IT Services Ltd.', 'IT', 'Suresh ', 'IT Admin', '9845015367', 'suresh.p@minacs.adityabirla.com', 'Positive', 'Yes', '2018-02-15', '15:24:00', 'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaasssssssssssssssssssssssssssssssssssddddddddddddddddddddddddddddddddddddddddddddddddddfffffff', 'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa', 'Yes', '2018-02-13 13:31:53'),
(40, 'Korai Purusottam', 'purusottam@navikra.com', 'Direct', 'Medical and Govt', '2018-02-15', '16:53:00', 'Oracle Financial Services Software Ltd.', 'IT', 'Vincent Thennarasu Francis', 'Manager - Technical & Consulting', '9945519071', 'vincent_tf@yahoo.com', 'Positive', 'Yes', '2018-02-16', '15:43:00', 'aws', 'urgent', 'Yes', '2018-02-15 09:45:51'),
(41, 'Korai Purusottam', 'purusottam@navikra.com', 'Direct', 'Medical and Govt', '2018-02-16', '14:34:00', 'Oracle Financial Services Software Ltd.', 'IT', 'Vincent Thennarasu Francis', 'Manager - Technical & Consulting', '9945519071', 'vincent_tf@yahoo.com', 'Positive', 'Yes', '2018-04-14', '00:00:00', '', '', 'Yes', '2018-02-15 09:49:25'),
(42, 'Korai Purusottam', 'purusottam@navikra.com', 'Direct', 'Medical and Govt', '2018-02-16', '00:00:00', 'abcd', 'Govt', 'abcd ', '', '1111111111', '', '', '', '0000-00-00', '00:00:00', '', '', '', '2018-02-16 02:29:43'),
(43, 'Korai Purusottam', 'purusottam@navikra.com', 'Direct', 'testcampaigncreate', '2018-02-16', '16:23:00', 'Sadasd', 'IT', 'dsfsdf sdfsdf', '', '', '', '', '', '0000-00-00', '00:00:00', '', '', 'Yes', '2018-02-16 02:37:28'),
(44, 'Korai Purusottam', 'purusottam@navikra.com', 'Direct', 'asd', '2018-03-29', '14:44:00', 'aaaaaaa', 'IT ', 'ABC', 'Manager', '9886636539', 'purusottam@navikra.com', 'Positive', 'Yes', '0000-00-00', '00:00:00', 'Endpoint', 'Sophos', 'Yes', '2018-03-30 09:01:33'),
(45, 'Korai Purusottam', 'purusottam@navikra.com', 'Direct', 'aws', '2018-04-03', '12:31:00', 'NetApp India Pvt. Ltd.', 'IT', 'Sireesh Beemineni ', 'IT Admin', '9448985881', 'sireeshnaidu@gmail.com', 'Positive', 'No', '0000-00-00', '00:00:00', 'CRM', 'CRM', 'Yes', '2018-04-03 10:18:33'),
(46, 'Korai Purusottam', 'purusottam@navikra.com', 'Direct', 'aws', '2018-04-16', '14:13:00', 'Acer India Pvt. Ltd.', 'IT', 'Senthil Kumar Selvarajan', 'Manager', '9886606745', 'senthil.kumar@acer.com', 'Positive', 'Yes', '2018-04-16', '15:42:00', 'ytyryut', 'ututu', 'Yes', '2018-04-15 21:59:34'),
(47, 'Korai Purusottam', 'purusottam@navikra.com', 'Direct', 'aws', '2018-04-16', '17:06:00', 'Actiance India Pvt. Ltd', 'IT', 'Mimit Mehta', 'Manager', '9485380690', 'MMehta@actiance.com', 'Positive', 'Yes', '2018-04-17', '16:53:00', 'sffsf', 'sdfsdf', 'Yes', '2018-04-15 22:01:30'),
(48, 'Korai Purusottam', 'purusottam@navikra.com', 'Reference', 'aws', '2018-04-16', '16:53:00', 'AKAMAI TECHNOLOGIES', 'IT', 'Guru / Prashant D', 'Manager', '9880599530', 'ggudla@akamai.com', 'Positive', 'Yes', '2018-04-17', '14:34:00', 'sfsdfsfsd', 'sdfsf', 'Yes', '2018-04-15 22:05:34'),
(49, '', 'bhargava@rabitasoft.com', 'Direct', 'aws', '2018-04-18', '15:43:00', 'Aditya Birla Minacs IT Services Ltd.', 'IT', 'Suresh ', 'Manager', '9845015367', 'suresh.p@minacs.adityabirla.com', 'Positive', 'Yes', '2018-04-19', '14:13:00', 'AWS', 'EC2', 'Yes', '2018-04-18 11:12:55'),
(50, '', 'bhargava@rabitasoft.com', 'Digital', 'aws', '2018-04-18', '14:43:00', '24/7 Customer Pvt. Ltd.', 'IT', 'Vinayak Relekar', 'Director', '9845972305', 'Vinayak.relekar@247-inc.com', 'Positive', 'Yes', '2018-04-18', '15:42:00', 'wdsfs', 'fdfsff', 'Yes', '2018-04-18 11:27:46'),
(51, 'Korai Purusottam', 'puru@gmail.com', 'Reference', 'aws', '2018-04-19', '17:06:00', 'abc', 'IT', 'Senthil Kumar Selvarajan', 'Manager', '9886606745', 'senthil.kumar@acer.com', '', 'Yes', '2018-04-19', '16:32:00', 'aws', 'ec2', 'Yes', '2018-04-16 18:30:01'),
(52, 'Korai Purusottam', 'puru@gmail.com', 'Campaign Type', 'Campaign Topic Name', '0000-00-00', '00:00:00', 'abcd', 'Sector', 'Contact Person', 'Designation', 'Mobile No', 'Mail', 'Response', 'FollowUp Decission', '0000-00-00', '00:00:00', 'Requirement', 'Remarks', 'Meeting Status', '2018-01-14 18:30:01'),
(53, 'Korai Purusottam', 'purusottam@navikra.com', '', '', '0000-00-00', '00:00:00', '', '', '', '', '', '', '', '', '0000-00-00', '00:00:00', '', '', '', '2018-04-23 14:32:04'),
(54, 'Korai Purusottam', 'purusottam@navikra.com', 'Direct', 'Medical and Govt', '2016-02-18', '14:40:00', 'abcdefghijk', 'Govt', 'abcd ', 'Manager', '1111111111', '', 'Positive/Negative', 'Yes', '0000-00-00', '00:00:00', '', 'remark', 'Yes', '2018-04-23 14:32:04'),
(55, 'Raghu Vamsi', 'raghu@navikra.com', 'Direct', 'testemp', '2018-05-07', '17:06:00', 'A K Aerotek Software Centre Pvt Ltd', 'IT', 'C P Parmesh ', 'Associate Manager  ', '9845447244', 'parmesh.cp@akaerotek.com', 'Positive', 'Yes', '2018-05-07', '19:08:00', 'gdgdg', 'yjgjfj', 'Yes', '2018-05-07 06:51:58'),
(56, 'Raghu Vamsi', 'raghu@navikra.com', 'Direct', 'testemp', '2018-05-07', '18:55:00', 'A K Aerotek Software Centre Pvt Ltd', 'IT', 'C P Parmesh ', 'Associate Manager  ', '9845447244', 'parmesh.cp@akaerotek.com', '', 'Yes', '0000-00-00', '00:00:00', 'gdgdg', 'yjgjfj', 'Yes', '2018-05-07 08:04:07');

-- --------------------------------------------------------

--
-- Table structure for table `navdev`
--

CREATE TABLE IF NOT EXISTS `navdev` (
`id` int(255) NOT NULL,
  `Customer` varchar(100) DEFAULT NULL,
  `Email` varchar(100) DEFAULT NULL,
  `Validity` varchar(100) NOT NULL DEFAULT '0',
  `ModificationDetails` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `navdev`
--

INSERT INTO `navdev` (`id`, `Customer`, `Email`, `Validity`, `ModificationDetails`) VALUES
(1, 'Navikra', 'testval@navikra.com', '30', '2018-03-01 14:11:55');

-- --------------------------------------------------------

--
-- Table structure for table `notice`
--

CREATE TABLE IF NOT EXISTS `notice` (
`id` int(255) NOT NULL,
  `From` varchar(100) DEFAULT NULL,
  `To` varchar(100) DEFAULT NULL,
  `Message` text,
  `ModificationDetail` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `notice`
--

INSERT INTO `notice` (`id`, `From`, `To`, `Message`, `ModificationDetail`) VALUES
(1, 'purusottam@navikra.com', 'Technical', 'Hello Test1', '2018-04-14 21:48:47'),
(2, 'purusottam@navikra.com', 'puru@gmail.com,r.kumarraju@navikra.com', 'Hello Test2', '2018-04-14 21:49:31'),
(3, 'purusottam@navikra.com', 'purusottam@navikra.com', 'Hello Test3', '2018-04-14 22:15:46'),
(4, 'purusottam@navikra.com', 'All', 'test123', '2018-04-19 22:20:49'),
(5, 'purusottam@navikra.com', 'All,Sales', 'test12345678', '2018-04-19 22:26:49'),
(6, 'purusottam@navikra.com', 'All', 'hello all', '2018-05-07 10:12:59');

-- --------------------------------------------------------

--
-- Table structure for table `opf`
--

CREATE TABLE IF NOT EXISTS `opf` (
`id` int(255) NOT NULL,
  `Representive` varchar(100) DEFAULT NULL,
  `RMail` varchar(100) DEFAULT NULL,
  `OPFNo` varchar(100) DEFAULT NULL,
  `QuotRefNo` varchar(100) DEFAULT NULL,
  `VendorName` varchar(100) DEFAULT NULL,
  `CreditCardName` varchar(100) DEFAULT NULL,
  `CardEndingNo` varchar(100) DEFAULT NULL,
  `WHTPercentage` varchar(100) NOT NULL DEFAULT '0',
  `ConversionRate` varchar(100) NOT NULL DEFAULT '0',
  `ModificationDetail` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `opf`
--

INSERT INTO `opf` (`id`, `Representive`, `RMail`, `OPFNo`, `QuotRefNo`, `VendorName`, `CreditCardName`, `CardEndingNo`, `WHTPercentage`, `ConversionRate`, `ModificationDetail`) VALUES
(1, 'Korai Purusottam', 'purusottam@navikra.com', 'OPF1', 'RBS/ABB/0019', 'adas', 'Visa', '9087', '100', '68', '2018-05-16 00:09:38'),
(2, 'Korai Purusottam', 'purusottam@navikra.com', 'OPF2', 'RBS/ABB/0018', 'adas', 'skd', '7576', '76575757575', '65', '2018-05-16 07:00:43'),
(3, 'Korai Purusottam', 'purusottam@navikra.com', 'OPF3', 'RBS/ABB/0015', 'sdcsd', 'ahsfgsh', '6878', '13323', '65', '2018-05-16 07:16:32'),
(4, 'Korai Purusottam', 'purusottam@navikra.com', 'OPF4', 'RBS/ABB/0018', 'sdcsd', 'adasd', '4232', '2313', '65', '2018-05-16 07:30:32'),
(5, 'Korai Purusottam', 'purusottam@navikra.com', 'OPF5', 'RBS/ABB/0017', 'sdcsd', 'jkhdk', '6789', '76575', '65', '2018-05-16 07:32:38'),
(6, 'Korai Purusottam', 'purusottam@navikra.com', 'OPF6', 'RBS/ABB/0020', 'jghjgj', 'kjad', '8979', '100', '65', '2018-05-16 08:01:27'),
(7, 'Korai Purusottam', 'purusottam@navikra.com', 'OPF7', 'RBS/ABB/0021', 'sdcsd', 'hffh', '6675', '10', '67', '2018-05-16 08:27:26'),
(8, 'Korai Purusottam', 'purusottam@navikra.com', 'OPF8', 'RBS/ABB/0022', 'sdcsd', 'jknd', '6763', '20', '70', '2018-05-16 08:53:40'),
(9, 'Korai Purusottam', 'purusottam@navikra.com', 'OPF9', 'RBS/ABB/0022', 'sdcsd', '', '', '10', '', '2018-05-16 11:51:23'),
(10, 'Korai Purusottam', 'purusottam@navikra.com', 'OPF10', 'RBS/ABB/0021', 'sdcsd', 'abcdefg', '2343', '20', '75', '2018-05-17 03:33:22');

-- --------------------------------------------------------

--
-- Table structure for table `po`
--

CREATE TABLE IF NOT EXISTS `po` (
`id` int(255) NOT NULL,
  `Representive` varchar(100) DEFAULT NULL,
  `RMail` varchar(100) DEFAULT NULL,
  `PONo` varchar(100) DEFAULT NULL,
  `VendorName` varchar(100) DEFAULT NULL,
  `Company` varchar(100) DEFAULT NULL,
  `Product` text,
  `Service` varchar(100) DEFAULT NULL,
  `PartDescription1` text,
  `Quantity1` varchar(100) DEFAULT NULL,
  `UnitPrice1` varchar(100) DEFAULT NULL,
  `PartDescription2` text,
  `Quantity2` varchar(100) DEFAULT NULL,
  `UnitPrice2` varchar(100) DEFAULT NULL,
  `PartDescription3` text,
  `Quantity3` varchar(100) DEFAULT NULL,
  `UnitPrice3` varchar(100) DEFAULT NULL,
  `PartDescription4` text,
  `Quantity4` varchar(100) DEFAULT NULL,
  `UnitPrice4` varchar(100) DEFAULT NULL,
  `PartDescription5` text NOT NULL,
  `Quantity5` varchar(100) NOT NULL,
  `UnitPrice5` varchar(100) NOT NULL,
  `Delivery` varchar(100) NOT NULL DEFAULT '0',
  `Payment` varchar(100) NOT NULL DEFAULT '0',
  `SubTotal` varchar(100) NOT NULL DEFAULT '0',
  `SGST` varchar(100) NOT NULL DEFAULT '0',
  `CGST` varchar(100) NOT NULL DEFAULT '0',
  `IGST` varchar(100) NOT NULL DEFAULT '0',
  `GrandTotal` varchar(100) NOT NULL DEFAULT '0',
  `ModificationDetail` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `po`
--

INSERT INTO `po` (`id`, `Representive`, `RMail`, `PONo`, `VendorName`, `Company`, `Product`, `Service`, `PartDescription1`, `Quantity1`, `UnitPrice1`, `PartDescription2`, `Quantity2`, `UnitPrice2`, `PartDescription3`, `Quantity3`, `UnitPrice3`, `PartDescription4`, `Quantity4`, `UnitPrice4`, `PartDescription5`, `Quantity5`, `UnitPrice5`, `Delivery`, `Payment`, `SubTotal`, `SGST`, `CGST`, `IGST`, `GrandTotal`, `ModificationDetail`) VALUES
(1, 'Korai Purusottam', 'purusottam@navikra.com', 'NTSPL-A001', 'Redington India Ltd', '24/7 Customer Pvt. Ltd.', 'IBM', 'Product', 'fgdg', '6', '100000', '', '', '', '', '', '', '', '', '', '', '', '', '4', 'Advance', '300000', '0', '0', '54000', '354000', '2018-02-21 23:00:34'),
(2, 'Raghu Vamsi', 'raghu@navikra.com', 'RABITA-A002', 'Slack', 'NetApp India Pvt. Ltd.', 'awsta', 'Installation', 'yuytyutu', '3', '6', '', '', '', '', '', '', '', '', '', '', '', '', '6', 'Advance', '18', '0', '0', '3.24', '21.24', '2018-05-07 08:23:20');

-- --------------------------------------------------------

--
-- Table structure for table `po_upload`
--

CREATE TABLE IF NOT EXISTS `po_upload` (
`id` int(255) NOT NULL,
  `Representative` varchar(100) DEFAULT NULL,
  `RMail` varchar(100) DEFAULT NULL,
  `Company` varchar(100) DEFAULT NULL,
  `Product` varchar(100) DEFAULT NULL,
  `ClosureDate` varchar(100) DEFAULT NULL,
  `ReferenceNo` varchar(255) DEFAULT NULL,
  `FileType` varchar(100) DEFAULT NULL,
  `FilePath` text,
  `ModificationDetail` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE IF NOT EXISTS `products` (
  `Representive` varchar(100) DEFAULT NULL,
  `RMail` varchar(100) DEFAULT NULL,
  `ProductId` varchar(100) NOT NULL,
  `Category` varchar(100) DEFAULT NULL,
  `ProductName` varchar(100) DEFAULT NULL,
  `Price` varchar(255) NOT NULL DEFAULT '0',
  `Services` varchar(100) DEFAULT NULL,
  `PartnershipLevel` varchar(100) DEFAULT NULL,
  `SalesCertification` varchar(100) DEFAULT NULL,
  `TechnicalCertification` varchar(100) DEFAULT NULL,
  `Demo` varchar(100) DEFAULT NULL,
  `POC` varchar(100) DEFAULT NULL,
  `PANAvailable` varchar(10) DEFAULT NULL,
  `ProductMargin` int(100) NOT NULL DEFAULT '0',
  `OEMName` varchar(100) DEFAULT NULL,
  `OEMAddress` text NOT NULL,
  `OEMCity` varchar(100) NOT NULL,
  `OEMState` varchar(100) NOT NULL,
  `OEMPin` int(100) NOT NULL DEFAULT '0',
  `OEMGSTPercentage` varchar(100) NOT NULL DEFAULT '0',
  `OEMGSTNo` varchar(100) DEFAULT NULL,
  `OEMPANNo` varchar(100) DEFAULT NULL,
  `OEMContactPerson` varchar(100) DEFAULT NULL,
  `OEMContactNo` varchar(100) DEFAULT NULL,
  `OEMContactMail` varchar(100) DEFAULT NULL,
  `OEMContactPerson2` varchar(100) DEFAULT NULL,
  `OEMContactNo2` varchar(100) DEFAULT NULL,
  `OEMContactMail2` varchar(100) DEFAULT NULL,
  `SellerName` varchar(100) DEFAULT NULL,
  `SellerAddress` text,
  `SellerCity` varchar(100) DEFAULT NULL,
  `SellerState` varchar(100) DEFAULT NULL,
  `SellerPin` varchar(100) DEFAULT NULL,
  `SellerGSTPercentage` int(100) NOT NULL DEFAULT '0',
  `SellerGSTNo` varchar(100) DEFAULT NULL,
  `SellerPANNo` varchar(100) DEFAULT NULL,
  `SellerContactPerson` varchar(100) DEFAULT NULL,
  `SellerContactNo` varchar(100) DEFAULT NULL,
  `SellerContactMail` varchar(100) DEFAULT NULL,
  `SellerContactPerson2` varchar(100) DEFAULT NULL,
  `SellerContactNo2` varchar(100) DEFAULT NULL,
  `SellerContactMail2` varchar(100) DEFAULT NULL,
  `ModificationDetail` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`Representive`, `RMail`, `ProductId`, `Category`, `ProductName`, `Price`, `Services`, `PartnershipLevel`, `SalesCertification`, `TechnicalCertification`, `Demo`, `POC`, `PANAvailable`, `ProductMargin`, `OEMName`, `OEMAddress`, `OEMCity`, `OEMState`, `OEMPin`, `OEMGSTPercentage`, `OEMGSTNo`, `OEMPANNo`, `OEMContactPerson`, `OEMContactNo`, `OEMContactMail`, `OEMContactPerson2`, `OEMContactNo2`, `OEMContactMail2`, `SellerName`, `SellerAddress`, `SellerCity`, `SellerState`, `SellerPin`, `SellerGSTPercentage`, `SellerGSTNo`, `SellerPANNo`, `SellerContactPerson`, `SellerContactNo`, `SellerContactMail`, `SellerContactPerson2`, `SellerContactNo2`, `SellerContactMail2`, `ModificationDetail`) VALUES
('Korai Purusottam', 'purusottam@navikra.com', 'prod1', 'Infrastructure', 'AWS', '0', NULL, 'Registered', '7', '4', 'Yes', 'Yes', NULL, 0, NULL, '', '', '', 0, '0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-04 11:21:12'),
('Korai Purusottam', 'purusottam@navikra.com', 'prod10', 'Infrastructure', 'ABC', '3', '3', 'Registered', '1', '2', 'Yes', 'Yes', NULL, 0, NULL, '', '', '', 0, '0', NULL, NULL, '3', '3', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-05-08 13:57:48'),
('Korai Purusottam', 'purusottam@navikra.com', 'prod11', 'Analytics', 'awsta', '11233', '454', 'Registered', '12', '312', 'Yes', 'Yes', 'Yes', 13, '', '', '', '', 0, '0', '', '', 'ramesh', '12345678900', '', '', '', '', '', '', '', '', '0', 0, '', '', 'ramesh', '12345678900', '', '', '', '', '2018-04-27 12:21:09'),
('Korai Purusottam', 'purusottam@navikra.com', 'prod12', 'Infrastructure', 'yjhd', '876868', 'yut', 'Registered', '34', '673', 'Yes', 'Yes', 'Yes', 0, NULL, '', '', '', 0, '0', NULL, NULL, 'yututyu', '675757575775', 'kjnkjn@kcydc.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-05-10 05:32:27'),
('Korai Purusottam', 'purusottam@navikra.com', 'PROD13', 'Infrastructure', 'asd', '234', 'asd', 'Registered', '', '', 'Yes', 'Yes', 'Yes', 6, NULL, '', '', '', 0, '0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-05-13 19:27:00'),
('Korai Purusottam', 'purusottam@navikra.com', 'PROD14', 'Infrastructure', 'jkdas', '2342', 'ada', 'Registered', '', '', 'Yes', 'Yes', 'Yes', 34, 'asd', '', '', '', 0, '', '', '', '', '', '', '', '', '', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-05-13 20:30:30'),
('Korai Purusottam', 'purusottam@navikra.com', 'PROD15', 'Infrastructure', 'sdjd', '267345', 'knkcn', 'Registered', '', '', 'Yes', 'Yes', 'Yes', 67, 'adas', '', '', '', 0, '', '', '', '', '', '', '', '', '', 'adas', '', '', '', '', 0, '', '', '', '', '', '', '', '', '2018-05-13 20:35:59'),
('Korai Purusottam', 'purusottam@navikra.com', 'PROD16', 'Infrastructure', 'jddj', '4545', 'jkdnk', 'Registered', '', '', 'Yes', 'Yes', 'Yes', 9, 'dcc', '', '', '', 0, '', '', '', '', '', '', '', '', '', 'sdcsd', '', '', '', '', 0, '', '', '', '', '', '', '', '', '2018-05-13 20:38:40'),
('Korai Purusottam', 'purusottam@navikra.com', 'PROD17', 'Security', 'hffhf', '565646', 'yuuadu', 'Registered', '', '', 'Yes', 'Yes', 'Yes', 12, 'jghjgj', '', '', '', 0, '', '', '', '', '', '', '', '', '', 'jghjgj', '', '', '', '', 0, '', '', '', '', '', '', '', '', '2018-05-13 21:37:58'),
('Korai Purusottam', 'purusottam@navikra.com', 'prod2', 'Infrastructure', 'IBM', '0', NULL, 'Registered', '5', '5', 'Yes', 'Yes', NULL, 0, NULL, '', '', '', 0, '0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-07 13:31:30'),
('Korai Purusottam', 'purusottam@navikra.com', 'prod3', 'Security', 'Sophos', '0', NULL, 'Registered', '5', '8', 'Yes', 'Yes', NULL, 10, NULL, '', '', '', 0, '0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-07 13:32:08'),
('Korai Purusottam', 'purusottam@navikra.com', 'prod4', 'Software Development & Testing', 'CRM', '0', 'Installation', 'Platinum', '2', '2', 'Yes', 'Yes', NULL, 0, NULL, '', '', '', 0, '0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-13 05:43:25'),
('Korai Purusottam', 'purusottam@navikra.com', 'prod5', 'Software Development & Testing', 'CRM', '0', 'Product', 'Platinum', '1', '1', 'Yes', 'Yes', NULL, 0, NULL, '', '', '', 0, '0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-13 06:50:55'),
('Korai Purusottam', 'purusottam@navikra.com', 'prod6', 'Infrastructure', 'A', '0', '7', 'Registered', '2', '7', 'Yes', 'Yes', NULL, 0, NULL, '', '', '', 0, '0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-03-09 12:33:51'),
('Korai Purusottam', 'purusottam@navikra.com', 'prod7', 'Infrastructure', 'AWS', '0', 'NULL', 'Registered', '7', '4', 'Yes', 'Yes', NULL, 0, NULL, '', '', '', 0, '0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-04-23 22:19:32'),
('Korai Purusottam', 'purusottam@navikra.com', 'prod8', 'Infrastructure', 'IBM', '0', 'NULL', 'Registered', '5', '5', 'Yes', 'Yes', NULL, 0, NULL, '', '', '', 0, '0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-04-23 22:19:32');

-- --------------------------------------------------------

--
-- Table structure for table `products_backup`
--

CREATE TABLE IF NOT EXISTS `products_backup` (
  `Representive` varchar(100) DEFAULT NULL,
  `RMail` varchar(100) DEFAULT NULL,
  `ProductId` varchar(100) NOT NULL,
  `Category` varchar(100) DEFAULT NULL,
  `ProductName` varchar(100) DEFAULT NULL,
  `Price` varchar(255) NOT NULL DEFAULT '0',
  `Services` varchar(100) DEFAULT NULL,
  `PartnershipLevel` varchar(100) DEFAULT NULL,
  `SalesCertification` varchar(100) DEFAULT NULL,
  `TechnicalCertification` varchar(100) DEFAULT NULL,
  `Demo` varchar(100) DEFAULT NULL,
  `POC` varchar(100) DEFAULT NULL,
  `PANAvailable` varchar(10) DEFAULT NULL,
  `ProductMargin` int(100) NOT NULL DEFAULT '0',
  `OEMName` varchar(100) DEFAULT NULL,
  `OEMAddress` text NOT NULL,
  `OEMCity` varchar(100) NOT NULL,
  `OEMState` varchar(100) NOT NULL,
  `OEMPin` int(100) NOT NULL DEFAULT '0',
  `OEMGSTPercentage` varchar(100) NOT NULL DEFAULT '0',
  `OEMGSTNo` varchar(100) DEFAULT NULL,
  `OEMPANNo` varchar(100) DEFAULT NULL,
  `OEMContactPerson` varchar(100) DEFAULT NULL,
  `OEMContactNo` varchar(100) DEFAULT NULL,
  `OEMContactMail` varchar(100) DEFAULT NULL,
  `OEMContactPerson2` varchar(100) DEFAULT NULL,
  `OEMContactNo2` varchar(100) DEFAULT NULL,
  `OEMContactMail2` varchar(100) DEFAULT NULL,
  `SellerName` varchar(100) DEFAULT NULL,
  `SellerAddress` text,
  `SellerCity` varchar(100) DEFAULT NULL,
  `SellerState` varchar(100) DEFAULT NULL,
  `SellerPin` varchar(100) DEFAULT NULL,
  `SellerGSTPercentage` int(100) NOT NULL DEFAULT '0',
  `SellerGSTNo` varchar(100) DEFAULT NULL,
  `SellerPANNo` varchar(100) DEFAULT NULL,
  `SellerContactPerson` varchar(100) DEFAULT NULL,
  `SellerContactNo` varchar(100) DEFAULT NULL,
  `SellerContactMail` varchar(100) DEFAULT NULL,
  `SellerContactPerson2` varchar(100) DEFAULT NULL,
  `SellerContactNo2` varchar(100) DEFAULT NULL,
  `SellerContactMail2` varchar(100) DEFAULT NULL,
  `ModificationDetail` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `quotation`
--

CREATE TABLE IF NOT EXISTS `quotation` (
`id` int(255) NOT NULL,
  `Representive` varchar(100) DEFAULT NULL,
  `RMail` varchar(100) DEFAULT NULL,
  `QuotNo` varchar(100) DEFAULT NULL,
  `Company` varchar(100) DEFAULT NULL,
  `Product` text,
  `Tax` varchar(100) DEFAULT NULL,
  `Currency` varchar(100) DEFAULT NULL,
  `HSNSAC` varchar(100) DEFAULT NULL,
  `Service` varchar(100) DEFAULT NULL,
  `PartDescription1` text,
  `Quantity1` varchar(100) DEFAULT NULL,
  `UnitPrice1` varchar(100) DEFAULT NULL,
  `PartDescription2` text,
  `Quantity2` varchar(100) DEFAULT NULL,
  `UnitPrice2` varchar(100) DEFAULT NULL,
  `PartDescription3` text,
  `Quantity3` varchar(100) DEFAULT NULL,
  `UnitPrice3` varchar(100) DEFAULT NULL,
  `PartDescription4` text,
  `Quantity4` varchar(100) DEFAULT NULL,
  `UnitPrice4` varchar(100) DEFAULT NULL,
  `PartDescription5` text NOT NULL,
  `Quantity5` varchar(100) NOT NULL,
  `UnitPrice5` varchar(100) NOT NULL,
  `Validity` varchar(100) NOT NULL DEFAULT '0',
  `Delivery` varchar(100) NOT NULL DEFAULT '0',
  `Payment` varchar(100) NOT NULL DEFAULT '0',
  `SubTotal` varchar(100) NOT NULL DEFAULT '0',
  `SGST` varchar(100) NOT NULL DEFAULT '0',
  `CGST` varchar(100) NOT NULL DEFAULT '0',
  `IGST` varchar(100) NOT NULL DEFAULT '0',
  `GrandTotal` varchar(100) NOT NULL DEFAULT '0',
  `Permission` int(11) NOT NULL DEFAULT '0',
  `ModificationDetail` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `quotation`
--

INSERT INTO `quotation` (`id`, `Representive`, `RMail`, `QuotNo`, `Company`, `Product`, `Tax`, `Currency`, `HSNSAC`, `Service`, `PartDescription1`, `Quantity1`, `UnitPrice1`, `PartDescription2`, `Quantity2`, `UnitPrice2`, `PartDescription3`, `Quantity3`, `UnitPrice3`, `PartDescription4`, `Quantity4`, `UnitPrice4`, `PartDescription5`, `Quantity5`, `UnitPrice5`, `Validity`, `Delivery`, `Payment`, `SubTotal`, `SGST`, `CGST`, `IGST`, `GrandTotal`, `Permission`, `ModificationDetail`) VALUES
(2, 'Korai Purusottam', 'purusottam@navikra.com', 'NTSPL/2018/1', 'Oracle Financial Services Software Ltd.', 'AWS', NULL, NULL, NULL, 'Installation', '12', '1', '100000', '', '', '', '', '', '', '', '', '', '', '', '', '2', '4', 'Full Payment', '500000', '0', '0', '90000', '590000', 0, '2018-02-21 12:53:22'),
(4, 'Korai Purusottam', 'purusottam@navikra.com', 'NTSPL/2018/2', 'AKAMAI TECHNOLOGIES', 'AWS', NULL, NULL, NULL, 'Product', 'EC2', '4', '100000', '', '', '', '', '', '', '', '', '', '', '', '', '6', '8', 'Full Payment', '500000', '45000', '45000', '0', '590000', 0, '2018-02-21 20:06:16'),
(6, 'Korai Purusottam', 'purusottam@navikra.com', 'NTSPL/2018/3', 'AKAMAI TECHNOLOGIES', 'AWS', NULL, NULL, NULL, 'Installation', 'abc', '4', '600000', '', '', '', '', '', '', '', '', '', '', '', '', '4', '5', 'Full Payment', '2400000', '216000', '216000', '0', '2832000', 0, '2018-02-21 21:05:03'),
(12, 'Korai Purusottam', 'purusottam@navikra.com', 'RABITA/2018/4', 'Oracle Financial Services Software Ltd.', 'awsta', NULL, NULL, NULL, 'Installation', 'qwda', '1', '100', '', '', '', '', '', '', '', '', '', '', '', '', '2', '4', 'Full Payment', '100', '0', '0', '18', '118', 0, '2018-04-29 20:39:30'),
(21, 'Korai Purusottam', 'purusottam@navikra.com', 'RBS/ABB/5', '24/7 Customer Pvt. Ltd.', 'awsta', NULL, NULL, NULL, 'Installation', 'sda', '1', '1212', '', '', '', '', '', '', '', '', '', '', '', '', '3', '4', 'Full Payment', '1212', '0', '0', '218.16', '1430.16', 0, '2018-04-29 22:42:04'),
(24, 'Korai Purusottam', 'purusottam@navikra.com', 'RBS/ABB/006', 'Oracle Financial Services Software Ltd.', 'awsta', NULL, NULL, NULL, 'Installation', 'sdddd', '2', '100', '', '', '', '', '', '', '', '', '', '', '', '', '2', '2', 'Full Payment', '0', '0', '0', '0', '0', 0, '2018-04-29 23:04:08'),
(25, 'Raghu Vamsi', 'raghu@navikra.com', NULL, 'Oracle Financial Services Software Ltd.', 'AWS', NULL, NULL, NULL, 'Installation', 'saad', '3', '21323', '', '', '', '', '', '', '', '', '', '', '', '', '5', '7', 'Full Payment', '0', '0', '0', '0', '0', 0, '2018-05-07 07:30:57'),
(26, 'Raghu Vamsi', 'raghu@navikra.com', 'RBS/ABB/008', 'A K Aerotek Software Centre Pvt Ltd', 'awsta', NULL, NULL, NULL, 'Installation', 'asdasda', '123', '10000000', '', '', '', '', '', '', '', '', '', '', '', '', '23', '23', 'Full Payment', '1230000000', '110700000', '110700000', '0', '1451400000', 0, '2018-05-07 08:08:19'),
(27, 'Korai Purusottam', 'purusottam@navikra.com', 'RBS/ABB/009', 'A K Aerotek Software Centre Pvt Ltd', 'awsta', NULL, NULL, NULL, '454', 'ghvhvh', '4', '67868768668', '', '', '', '', '', '', '', '', '', '', '', '', '5', '6', 'Advance', '271475074672', '24432756720.48', '24432756720.48', '0', '320340588112.96', 0, '2018-05-13 22:47:24'),
(28, 'Korai Purusottam', 'purusottam@navikra.com', 'RBS/ABB/0010', 'A K Aerotek Software Centre Pvt Ltd', 'AWS', 'Array', 'rupee', 'audkhkasdh', 'Installation', 'uadks', '5', '34343', '', '', '', '', '', '', '', '', '', '', '', '', '4', '4', 'Full Payment', '171715', '0', '0', '0', '202623.7', 0, '2018-05-14 20:42:07'),
(29, 'Korai Purusottam', 'purusottam@navikra.com', NULL, 'Oracle Financial Services Software Ltd.', 'AWS', 'SGST,CGST', 'rupee', '', 'Installation', 'kudfdsdjksfjsdkfjsdkfjksdjfksd', '6', '100000', '', '', '', '', '', '', '', '', '', '', '', '', '4', '4', 'Full Payment', '0', '0', '0', '0', '0', 0, '2018-05-14 20:54:18'),
(30, 'Korai Purusottam', 'purusottam@navikra.com', 'RBS/ABB/0012', 'A K Aerotek Software Centre Pvt Ltd', 'ABC', 'SGST,CGST', 'rupee', 'adas2345', 'Installation', 'sfkjshdsfkjshfjksdhkd', '3', '2232341', '', '', '', '', '', '', '', '', '', '', '', '', '5', '6', 'Full Payment', '6697023', '602732.07', '602732.07', '0', '7902487.14', 0, '2018-05-14 21:30:25'),
(31, 'Korai Purusottam', 'purusottam@navikra.com', 'RBS/ABB/0013', '24/7 Customer Pvt. Ltd.', 'awsta', 'IGST', 'dollar', 'jkfjknkn', 'Product', 'sdkfkjhjkh', '5', '10000000', '', '', '', '', '', '', '', '', '', '', '', '', '4', '10', 'Advance', '50000000', '0', '0', '9000000', '59000000', 0, '2018-05-14 21:33:52'),
(32, 'Korai Purusottam', 'purusottam@navikra.com', NULL, 'A K Aerotek Software Centre Pvt Ltd', 'AWS', 'SGST,CGST', 'rupee', 'adaddf', 'Installation', 'sdasd', '4', '10000000', '', '', '', '', '', '', '', '', '', '', '', '', '3', '4', 'Full Payment', '0', '0', '0', '0', '0', 0, '2018-05-14 23:00:01'),
(33, 'Korai Purusottam', 'purusottam@navikra.com', 'RBS/ABB/0015', 'A K Aerotek Software Centre Pvt Ltd', 'ABC', 'IGST', 'dollar', 'skjdanj', 'Product', 'ajkdhdk', '7', '10000000', '', '', '', '', '', '', '', '', '', '', '', '', '4', '7', 'Full Payment', '70000000', '0', '0', '0', '19600000', 0, '2018-05-14 23:04:55'),
(34, 'Korai Purusottam', 'purusottam@navikra.com', 'RBS/ABB/0016', 'Oracle Financial Services Software Ltd.', 'IBM', 'SGST,CGST', 'rupee', 'jkdzh', 'NULL', 'asjdnkj', '3', '10000000', '', '', '', '', '', '', '', '', '', '', '', '', '6', '9', 'Full Payment', '30000000', '2700000', '2700000', '0', '5400000', 0, '2018-05-14 23:14:26'),
(35, 'Korai Purusottam', 'purusottam@navikra.com', 'RBS/ABB/0017', '24/7 Customer Pvt. Ltd.', 'Sophos', 'SGST,CGST', 'rupee', 'djkckdj', 'yuuadu', 'skmksdmk', '3', '100', '', '', '', '', '', '', '', '', '', '', '', '', '7', '8', '50% Payment', '1999999998', '179999999.82', '179999999.82', '0', '359999999.64', 0, '2018-05-14 23:22:17'),
(36, 'Korai Purusottam', 'purusottam@navikra.com', 'RBS/ABB/0018', 'NetApp India Pvt. Ltd.', 'CRM', 'SGST,CGST', 'rupee', 'dkmck', 'Installation', 'ankddnkn', '10', '10000000', '', '', '', '', '', '', '', '', '', '', '', '', '5', '6', 'Full Payment', '100000000', '0', '0', '0', '18000000', 0, '2018-05-15 00:28:32'),
(37, 'Korai Purusottam', 'purusottam@navikra.com', 'RBS/ABB/0019', 'NetApp India Pvt. Ltd.', 'ABC', 'SGST,CGST', 'rupee', ',o.,olo.lmju', 'Installation', 'l..lll', '1', '10000000', '', '', '', '', '', '', '', '', '', '', '', '', '6', '5', 'Advance', '10000000', '900000', '900000', '0', '1800000', 0, '2018-05-15 09:42:42'),
(38, 'Korai Purusottam', 'purusottam@navikra.com', 'RBS/ABB/0020', '1point1', 'Sophos', 'SGST,CGST', 'rupee', 'kasdkj', 'Installation', 'jnknnkn', '5', '100', '', '', '', '', '', '', '', '', '', '', '', '', '6', '8', '30', '500', '45', '45', '0', '90', 0, '2018-05-16 08:00:36'),
(39, 'Korai Purusottam', 'purusottam@navikra.com', 'RBS/ABB/0021', '24/7 Customer Pvt. Ltd.', 'Sophos', 'IGST', 'dollar', 'gjgj', 'Product', 'tyfhfh', '5', '100', '', '', '', '', '', '', '', '', '', '', '', '', '4', '6', '60', '500', '0', '0', '140', '140', 0, '2018-05-16 08:26:21'),
(40, 'Korai Purusottam', 'purusottam@navikra.com', 'RBS/ABB/0022', 'Oracle Financial Services Software Ltd.', 'Sophos', 'IGST', 'dollar', 'fgsv', 'Installation', 'fff', '10', '100', '', '', '', '', '', '', '', '', '', '', '', '', '6', '9', '45', '1000', '0', '0', '280', '1280', 0, '2018-05-16 08:47:28');

-- --------------------------------------------------------

--
-- Table structure for table `quotation_last_update`
--

CREATE TABLE IF NOT EXISTS `quotation_last_update` (
`id` int(255) NOT NULL,
  `Representive` varchar(100) DEFAULT NULL,
  `RMail` varchar(100) DEFAULT NULL,
  `QuotNo` varchar(100) DEFAULT NULL,
  `Company` varchar(100) DEFAULT NULL,
  `Product` text,
  `Tax` varchar(100) DEFAULT NULL,
  `Currency` varchar(100) DEFAULT NULL,
  `HSNSAC` varchar(100) DEFAULT NULL,
  `Service` varchar(100) DEFAULT NULL,
  `PartDescription1` text,
  `Quantity1` varchar(100) DEFAULT NULL,
  `UnitPrice1` varchar(100) DEFAULT NULL,
  `PartDescription2` text,
  `Quantity2` varchar(100) DEFAULT NULL,
  `UnitPrice2` varchar(100) DEFAULT NULL,
  `PartDescription3` text,
  `Quantity3` varchar(100) DEFAULT NULL,
  `UnitPrice3` varchar(100) DEFAULT NULL,
  `PartDescription4` text,
  `Quantity4` varchar(100) DEFAULT NULL,
  `UnitPrice4` varchar(100) DEFAULT NULL,
  `PartDescription5` text NOT NULL,
  `Quantity5` varchar(100) NOT NULL,
  `UnitPrice5` varchar(100) NOT NULL,
  `Validity` varchar(100) NOT NULL DEFAULT '0',
  `Delivery` varchar(100) NOT NULL DEFAULT '0',
  `Payment` varchar(100) NOT NULL DEFAULT '0',
  `SubTotal` varchar(100) NOT NULL DEFAULT '0',
  `SGST` varchar(100) NOT NULL DEFAULT '0',
  `CGST` varchar(100) NOT NULL DEFAULT '0',
  `IGST` varchar(100) NOT NULL DEFAULT '0',
  `GrandTotal` varchar(100) NOT NULL DEFAULT '0',
  `Permission` int(11) NOT NULL DEFAULT '0',
  `ModificationDetail` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sector`
--

CREATE TABLE IF NOT EXISTS `sector` (
`slno` int(255) NOT NULL,
  `Representive` varchar(100) DEFAULT NULL,
  `RMail` varchar(100) DEFAULT NULL,
  `SectorName` varchar(100) DEFAULT NULL,
  `SectorId` varchar(100) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sector`
--

INSERT INTO `sector` (`slno`, `Representive`, `RMail`, `SectorName`, `SectorId`) VALUES
(2, 'Korai Purusottam', 'purusottam@navikra.com', 'Medical', '2'),
(5, 'Korai Purusottam', 'purusottam@navikra.com', 'Govt', '3'),
(6, 'Korai Purusottam', 'purusottam@navikra.com', 'IT', '3'),
(7, 'Korai Purusottam', 'purusottam@navikra.com', 'Media', '4'),
(8, 'Korai Purusottam', 'purusottam@navikra.com', 'Finance', '5');

-- --------------------------------------------------------

--
-- Table structure for table `setting_margin`
--

CREATE TABLE IF NOT EXISTS `setting_margin` (
`MarginId` int(255) NOT NULL,
  `Representive` varchar(100) DEFAULT NULL,
  `RMail` varchar(100) DEFAULT NULL,
  `MinMarginInIndia` varchar(100) NOT NULL DEFAULT '0',
  `MinMarginOutIndia` varchar(100) NOT NULL DEFAULT '0',
  `ModificationDetail` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `setting_margin`
--

INSERT INTO `setting_margin` (`MarginId`, `Representive`, `RMail`, `MinMarginInIndia`, `MinMarginOutIndia`, `ModificationDetail`) VALUES
(1, 'Korai Purusottam', 'purusottam@navikra.com', '8', '28', '2018-05-08 13:56:49');

-- --------------------------------------------------------

--
-- Table structure for table `setting_tax`
--

CREATE TABLE IF NOT EXISTS `setting_tax` (
`TaxId` int(255) NOT NULL,
  `Representive` varchar(100) DEFAULT NULL,
  `RMail` varchar(100) DEFAULT NULL,
  `TaxName` varchar(100) NOT NULL DEFAULT '0',
  `TaxPercentage` varchar(100) NOT NULL DEFAULT '0',
  `ModificationDetail` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `setting_tax`
--

INSERT INTO `setting_tax` (`TaxId`, `Representive`, `RMail`, `TaxName`, `TaxPercentage`, `ModificationDetail`) VALUES
(1, 'Korai Purusottam', 'purusottam@navikra.com', 'SGST', '9', '2018-05-14 12:09:14'),
(2, 'Korai Purusottam', 'purusottam@navikra.com', 'CGST', '9', '2018-05-14 12:09:37'),
(4, 'Korai Purusottam', 'purusottam@navikra.com', 'IGST', '28', '2018-05-14 12:25:51');

-- --------------------------------------------------------

--
-- Table structure for table `target`
--

CREATE TABLE IF NOT EXISTS `target` (
`slno` int(225) NOT NULL,
  `Representative` varchar(100) NOT NULL,
  `RMail` varchar(100) NOT NULL,
  `Employee` varchar(100) NOT NULL,
  `OrderPerMonth` int(225) DEFAULT '0',
  `OrderPerYear` int(225) NOT NULL DEFAULT '0',
  `MarginPerMonth` varchar(100) DEFAULT NULL,
  `MarginPerYear` varchar(100) DEFAULT NULL,
  `MeetingPerMonth` int(255) NOT NULL DEFAULT '0',
  `MeetingPerYear` int(225) NOT NULL DEFAULT '0',
  `FunnelPerMonth` int(255) NOT NULL DEFAULT '0',
  `FunnelPerYear` int(225) NOT NULL DEFAULT '0',
  `CustomersPerMonth` int(255) NOT NULL DEFAULT '0',
  `CustomersPerYear` int(255) NOT NULL DEFAULT '0',
  `ProductName` varchar(100) DEFAULT NULL,
  `SalesCertificationPerMonth` varchar(100) DEFAULT NULL,
  `SalesCertificationPerYear` varchar(100) DEFAULT NULL,
  `TechnicalCertificationPerMonth` varchar(100) DEFAULT NULL,
  `TechnicalCertificationPerYear` varchar(100) DEFAULT NULL,
  `DemoPerMonth` varchar(100) DEFAULT NULL,
  `DemoPerYear` varchar(100) DEFAULT NULL,
  `POCPerMonth` varchar(100) DEFAULT NULL,
  `POCPerYear` varchar(100) DEFAULT NULL,
  `ModificationDetail` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `target`
--

INSERT INTO `target` (`slno`, `Representative`, `RMail`, `Employee`, `OrderPerMonth`, `OrderPerYear`, `MarginPerMonth`, `MarginPerYear`, `MeetingPerMonth`, `MeetingPerYear`, `FunnelPerMonth`, `FunnelPerYear`, `CustomersPerMonth`, `CustomersPerYear`, `ProductName`, `SalesCertificationPerMonth`, `SalesCertificationPerYear`, `TechnicalCertificationPerMonth`, `TechnicalCertificationPerYear`, `DemoPerMonth`, `DemoPerYear`, `POCPerMonth`, `POCPerYear`, `ModificationDetail`) VALUES
(1, 'Korai Purusottam', 'purusottam@navikra.com', 'purusottam@navikra.com', 500000, 2147483647, '100000', '1200000', 30, 360, 10, 13546616, 120, 359, 'All', '5', '46', '5', '30', '5', '57', '5', '7679', '2018-01-05 08:01:32'),
(2, 'Korai Purusottam', 'purusottam@navikra.com', 'r.kumarraju@navikra.com', 500000, 1000000, '100000', '1200000', 30, 60, 10, 20, 120, 240, 'All', '5', '10', '5', '10', '5', '10', '5', '10', '2018-01-05 08:01:58'),
(3, 'Korai Purusottam', 'purusottam@navikra.com', 'purusottam@navikra.com', 500000, 2147483647, '100000', '1200000', 30, 360, 10, 13546616, 120, 359, 'All', '5', '46', '5', '30', '5', '57', '5', '7679', '2018-01-05 08:02:23'),
(4, 'Korai Purusottam', 'purusottam@navikra.com', 'r.kumarraju@navikra.com', 500000, 1000000, '100000', '1200000', 30, 60, 10, 20, 120, 240, 'All', '5', '10', '5', '10', '5', '10', '5', '10', '2018-01-05 08:04:29'),
(5, 'Korai Purusottam', 'purusottam@navikra.com', 'purusottam@navikra.com', 500000, 2147483647, '100000', '1200000', 30, 360, 10, 13546616, 120, 359, 'All', '5', '46', '5', '30', '5', '57', '5', '7679', '2018-01-05 08:05:14'),
(7, 'Korai Purusottam', 'purusottam@navikra.com', 'vikrant.kashyap@navikra.com', 500000, 1000000, '100000', '1200000', 30, 60, 10, 20, 120, 240, 'All', '5', '10', '5', '10', '5', '10', '5', '10', '2018-01-05 08:11:25'),
(8, 'Korai Purusottam', 'purusottam@navikra.com', 'vikrant.kashyap@navikra.com', 500000, 1000000, '100000', '1200000', 30, 60, 10, 20, 120, 240, 'All', '5', '10', '5', '10', '5', '10', '5', '10', '2018-02-05 08:11:58'),
(9, 'Korai Purusottam', 'purusottam@navikra.com', 'purusottam@navikra.com', 500000, 2147483647, '100000', '1200000', 30, 360, 10, 13546616, 120, 359, 'AWS', '5', '46', '5', '30', '5', '57', '5', '7679', '2018-01-05 12:33:57'),
(10, 'Raghu Vamsi', 'raghu@navikra.com', 'raghu@navikra.com', 500000, 500000, '100000', '1200000', 30, 30, 10, 10, 120, 120, 'All', '20', '20', '20', '20', '20', '20', '20', '20', '2018-01-05 13:58:19');

-- --------------------------------------------------------

--
-- Table structure for table `target_admin`
--

CREATE TABLE IF NOT EXISTS `target_admin` (
`slno` int(225) NOT NULL,
  `Representative` varchar(100) NOT NULL,
  `RMail` varchar(100) NOT NULL,
  `SBU` varchar(100) DEFAULT NULL,
  `RevenuePerMonth` int(225) DEFAULT '0',
  `RevenuePerYear` int(225) NOT NULL DEFAULT '0',
  `MarginPerMonth` varchar(100) DEFAULT NULL,
  `MarginPerYear` varchar(100) DEFAULT NULL,
  `MeetingPerMonth` int(255) NOT NULL DEFAULT '0',
  `MeetingPerYear` int(225) NOT NULL DEFAULT '0',
  `FunnelPerMonth` int(255) NOT NULL DEFAULT '0',
  `FunnelPerYear` int(225) NOT NULL DEFAULT '0',
  `CustomersPerMonth` int(255) NOT NULL DEFAULT '0',
  `CustomersPerYear` int(255) NOT NULL DEFAULT '0',
  `SalesCertificationPerMonth` varchar(100) DEFAULT NULL,
  `SalesCertificationPerYear` varchar(100) DEFAULT NULL,
  `TechnicalCertificationPerMonth` varchar(100) DEFAULT NULL,
  `TechnicalCertificationPerYear` varchar(100) DEFAULT NULL,
  `DemoPerMonth` varchar(100) DEFAULT NULL,
  `DemoPerYear` varchar(100) DEFAULT NULL,
  `POCPerMonth` varchar(100) DEFAULT NULL,
  `POCPerYear` varchar(100) DEFAULT NULL,
  `ModificationDetail` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `target_admin`
--

INSERT INTO `target_admin` (`slno`, `Representative`, `RMail`, `SBU`, `RevenuePerMonth`, `RevenuePerYear`, `MarginPerMonth`, `MarginPerYear`, `MeetingPerMonth`, `MeetingPerYear`, `FunnelPerMonth`, `FunnelPerYear`, `CustomersPerMonth`, `CustomersPerYear`, `SalesCertificationPerMonth`, `SalesCertificationPerYear`, `TechnicalCertificationPerMonth`, `TechnicalCertificationPerYear`, `DemoPerMonth`, `DemoPerYear`, `POCPerMonth`, `POCPerYear`, `ModificationDetail`) VALUES
(1, 'Korai Purusottam', 'purusottam@navikra.com', 'Infrastructure', 3455, 2147483647, '6576', '1000030929', 354657, 5910455, 3454, 2147483647, 43546, 666762448, '34556', '555590345', '354', '559363', '34556', '666704676', '345', '49134', '2018-03-02 21:23:10'),
(2, 'Korai Purusottam', 'purusottam@navikra.com', 'Security', 1000000, 1111111, '456', '1111567', 45, 2147483647, 345, 1111111456, 3456, 2147483647, '54', '111111165', '34', '11111111145', '4345', '11111115456', '35456', '111146567', '2018-03-02 21:24:13'),
(3, 'Korai Purusottam', 'purusottam@navikra.com', 'Infrastructure', 4657, 2147483647, '24354', '1000030929', 243, 5910455, 354, 2147483647, 4345, 666762448, '234', '555590345', '3454', '559363', '3454', '666704676', '4345', '49134', '2018-04-02 21:24:54'),
(4, 'Korai Purusottam', 'purusottam@navikra.com', 'Infrastructure', 2147483647, 2147483647, '999999999', '1000030929', 5555555, 5910455, 2147483647, 2147483647, 666666666, 666762448, '555555555', '555590345', '555555', '559363', '666666666', '666704676', '44444', '49134', '2018-04-08 23:17:15'),
(5, 'Korai Purusottam', 'purusottam@navikra.com', 'Security', 111111, 1111111, '1111111', '1111567', 2147483647, 2147483647, 1111111111, 1111111456, 2147483647, 2147483647, '111111111', '111111165', '11111111111', '11111111145', '11111111111', '11111115456', '111111111', '111146567', '2018-04-19 21:40:24');

-- --------------------------------------------------------

--
-- Table structure for table `target_manage_product`
--

CREATE TABLE IF NOT EXISTS `target_manage_product` (
`slno` int(225) NOT NULL,
  `Representative` varchar(100) NOT NULL,
  `RMail` varchar(100) NOT NULL,
  `ProductId` varchar(100) DEFAULT NULL,
  `ProductName` varchar(100) DEFAULT NULL,
  `RevenuePerMonth` varchar(225) DEFAULT '0',
  `RevenuePerYear` varchar(225) DEFAULT '0',
  `MarginPerMonth` varchar(100) DEFAULT NULL,
  `MarginPerYear` varchar(100) DEFAULT NULL,
  `MeetingPerMonth` varchar(255) NOT NULL DEFAULT '0',
  `MeetingPerYear` varchar(225) NOT NULL DEFAULT '0',
  `FunnelPerMonth` varchar(255) NOT NULL DEFAULT '0',
  `FunnelPerYear` varchar(225) NOT NULL DEFAULT '0',
  `CustomersPerMonth` varchar(255) NOT NULL DEFAULT '0',
  `CustomersPerYear` varchar(255) NOT NULL DEFAULT '0',
  `SalesCertificationPerMonth` varchar(100) DEFAULT NULL,
  `SalesCertificationPerYear` varchar(100) DEFAULT NULL,
  `TechnicalCertificationPerMonth` varchar(100) DEFAULT NULL,
  `TechnicalCertificationPerYear` varchar(100) DEFAULT NULL,
  `DemoPerMonth` varchar(100) DEFAULT NULL,
  `DemoPerYear` varchar(100) DEFAULT NULL,
  `POCPerMonth` varchar(100) DEFAULT NULL,
  `POCPerYear` varchar(100) DEFAULT NULL,
  `ModificationDetail` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `target_manage_product`
--

INSERT INTO `target_manage_product` (`slno`, `Representative`, `RMail`, `ProductId`, `ProductName`, `RevenuePerMonth`, `RevenuePerYear`, `MarginPerMonth`, `MarginPerYear`, `MeetingPerMonth`, `MeetingPerYear`, `FunnelPerMonth`, `FunnelPerYear`, `CustomersPerMonth`, `CustomersPerYear`, `SalesCertificationPerMonth`, `SalesCertificationPerYear`, `TechnicalCertificationPerMonth`, `TechnicalCertificationPerYear`, `DemoPerMonth`, `DemoPerYear`, `POCPerMonth`, `POCPerYear`, `ModificationDetail`) VALUES
(1, 'Korai Purusottam', 'purusottam@navikra.com', 'prod1', 'AWS', '1', '1', '2', '2', '3', '3', '4', '4', '5', '5', '6', '6', '8', '8', '7', '7', '9', '9', '2018-03-10 15:26:23'),
(2, 'Korai Purusottam', 'purusottam@navikra.com', 'prod2', 'IBM', '1', '1', '2', '2', '3', '3', '4', '4', '5', '5', '6', '6', '8', '8', '7', '7', '9', '9', '2018-03-10 15:31:49');

-- --------------------------------------------------------

--
-- Table structure for table `target_manage_resource`
--

CREATE TABLE IF NOT EXISTS `target_manage_resource` (
`slno` int(225) NOT NULL,
  `Representative` varchar(100) NOT NULL,
  `RMail` varchar(100) NOT NULL,
  `EmployeeMail` varchar(100) DEFAULT NULL,
  `EmployeeName` varchar(100) DEFAULT NULL,
  `RevenuePerMonth` varchar(225) DEFAULT '0',
  `RevenuePerYear` varchar(225) NOT NULL DEFAULT '0',
  `MarginPerMonth` varchar(100) DEFAULT NULL,
  `MarginPerYear` varchar(100) DEFAULT NULL,
  `MeetingPerMonth` varchar(255) NOT NULL DEFAULT '0',
  `MeetingPerYear` varchar(225) NOT NULL DEFAULT '0',
  `FunnelPerMonth` varchar(255) NOT NULL DEFAULT '0',
  `FunnelPerYear` varchar(225) NOT NULL DEFAULT '0',
  `CustomersPerMonth` varchar(255) NOT NULL DEFAULT '0',
  `CustomersPerYear` varchar(255) NOT NULL DEFAULT '0',
  `SalesCertificationPerMonth` varchar(100) DEFAULT NULL,
  `SalesCertificationPerYear` varchar(100) DEFAULT NULL,
  `TechnicalCertificationPerMonth` varchar(100) DEFAULT NULL,
  `TechnicalCertificationPerYear` varchar(100) DEFAULT NULL,
  `DemoPerMonth` varchar(100) DEFAULT NULL,
  `DemoPerYear` varchar(100) DEFAULT NULL,
  `POCPerMonth` varchar(100) DEFAULT NULL,
  `POCPerYear` varchar(100) DEFAULT NULL,
  `ModificationDetail` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `target_manage_resource`
--

INSERT INTO `target_manage_resource` (`slno`, `Representative`, `RMail`, `EmployeeMail`, `EmployeeName`, `RevenuePerMonth`, `RevenuePerYear`, `MarginPerMonth`, `MarginPerYear`, `MeetingPerMonth`, `MeetingPerYear`, `FunnelPerMonth`, `FunnelPerYear`, `CustomersPerMonth`, `CustomersPerYear`, `SalesCertificationPerMonth`, `SalesCertificationPerYear`, `TechnicalCertificationPerMonth`, `TechnicalCertificationPerYear`, `DemoPerMonth`, `DemoPerYear`, `POCPerMonth`, `POCPerYear`, `ModificationDetail`) VALUES
(1, 'Korai Purusottam', 'purusottam@navikra.com', 'r.kumarraju@navikra.com', 'Kumar Raju', '1', '1', '2', '2', '3', '3', '4', '4', '5', '5', '6', '6', '8', '8', '7', '7', '9', '9', '2018-03-10 15:26:50'),
(2, 'Korai Purusottam', 'purusottam@navikra.com', 'purusottam@navikra.com', 'Korai Purusottam', '1', '1', '2', '2', '3', '3', '4', '4', '5', '5', '6', '6', '8', '8', '7', '7', '9', '9', '2018-03-10 15:27:49');

-- --------------------------------------------------------

--
-- Table structure for table `team`
--

CREATE TABLE IF NOT EXISTS `team` (
  `Representive` varchar(100) DEFAULT NULL,
  `RMail` varchar(100) DEFAULT NULL,
  `empid` varchar(100) NOT NULL,
  `name` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `contact` varchar(20) NOT NULL,
  `desig` varchar(100) NOT NULL,
  `Salary` varchar(100) DEFAULT NULL,
  `doj` varchar(100) DEFAULT NULL,
  `Department` varchar(100) DEFAULT NULL,
  `EmployeeType` varchar(100) DEFAULT NULL,
  `Product` text,
  `SalesCertification` varchar(100) DEFAULT NULL,
  `TechnicalCertification` varchar(100) DEFAULT NULL,
  `Demo` varchar(100) DEFAULT NULL,
  `POC` varchar(100) DEFAULT NULL,
  `profileimage` text,
  `ModificationDetail` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `team`
--

INSERT INTO `team` (`Representive`, `RMail`, `empid`, `name`, `email`, `password`, `contact`, `desig`, `Salary`, `doj`, `Department`, `EmployeeType`, `Product`, `SalesCertification`, `TechnicalCertification`, `Demo`, `POC`, `profileimage`, `ModificationDetail`) VALUES
('Korai Purusottam', 'purusottam@navikra.com', 'ntspl11', 'dllnf', 'kp@gmail.com', '123', '6765575757', 'asda', '765757575', '2018-04-17', 'Technical', 'Admin', 'prod2', NULL, NULL, NULL, NULL, '', '2018-04-17 08:05:57'),
('Korai Purusottam', 'purusottam@navikra.com', 'ntspl6', 'Puru', 'puru@gmail.com', '123', '8895915069', 'Technical Architect', NULL, '2017-11-01', 'Technical', 'Admin', NULL, NULL, NULL, NULL, NULL, '', '2018-01-06 05:48:09'),
('Korai Purusottam', 'purusottam@navikra.com', 'ntspl1', 'Korai Purusottam', 'purusottam@navikra.com', '123', '7894237332', 'Technical Architect', '7000', '2017-11-01', 'Technical', 'Manager', NULL, '85', '85', '85', '85', 'purusottam@navikra.com.WhatsApp Image 2017-10-27 at 10.54.16 PM (1).jpeg', '2018-11-18 06:58:15'),
('Korai Purusottam', 'purusottam@navikra.com', 'ntspl2', 'Kumar Raju', 'r.kumarraju@navikra.com', 'kumarraju@3', '968672259', 'Business Development Executive', '15000', NULL, 'Sales', 'User', NULL, NULL, NULL, NULL, NULL, '', '2017-11-18 06:58:15'),
('Korai Purusottam', 'purusottam@navikra.com', 'ntspl3', 'Raghu Vamsi', 'raghu@navikra.com', 'raghu123', '9668834437', 'Business Development Executive', NULL, NULL, 'Technical', 'User', NULL, '20', '20', '20', '20', '', '2017-11-18 06:58:15'),
('Korai Purusottam', 'purusottam@navikra.com', 'ntspl5', 'Shyam', 'shyam.shankar@navikra.com', '1234', '8987656784', 'tech architect', NULL, NULL, 'Sales', 'User', NULL, NULL, NULL, NULL, NULL, '', '2017-12-27 08:13:35'),
('Korai Purusottam', 'purusottam@navikra.com', 'ntspl8', 'V', 'v@gmail.com', '1', '6546466464', 'tytyy', '56456464', '2018-03-11', 'Sales', 'Manager', ',,prod4', NULL, NULL, NULL, NULL, '', '2018-03-11 15:20:13'),
('Korai Purusottam', 'purusottam@navikra.com', 'ntspl4', 'Vikrant Kashyap', 'vikrant.kashyap@navikra.com', 'Vikrant@1', '7336785634', 'Executive', NULL, NULL, 'Sales', 'Admin', NULL, NULL, NULL, NULL, NULL, '', '2017-12-25 22:00:03'),
('Korai Purusottam', 'purusottam@navikra.com', 'ntspl9', 'th', 'yjhgj@fgfh.bnm', '5', '6757657576', 'yur', '5675665', '2018-03-12', 'Sales', 'Admin', 'prod1', NULL, NULL, NULL, NULL, '', '2018-03-12 08:35:07'),
('Puru', 'puru@gmail.com', 'ntspl10', 'rtu', 'yututu@jj.cvb', '5657', '6785675756', 'th', '765', '2018-03-01', 'Sales', 'Manager', ',,', NULL, NULL, NULL, NULL, '', '2018-03-12 10:01:13');

-- --------------------------------------------------------

--
-- Table structure for table `team_backup`
--

CREATE TABLE IF NOT EXISTS `team_backup` (
  `Representive` varchar(100) DEFAULT NULL,
  `RMail` varchar(100) DEFAULT NULL,
  `empid` varchar(100) NOT NULL,
  `name` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `contact` varchar(20) NOT NULL,
  `desig` varchar(100) NOT NULL,
  `Salary` varchar(100) DEFAULT NULL,
  `doj` varchar(100) DEFAULT NULL,
  `Department` varchar(100) DEFAULT NULL,
  `EmployeeType` varchar(100) DEFAULT NULL,
  `Product` text,
  `SalesCertification` varchar(100) DEFAULT NULL,
  `TechnicalCertification` varchar(100) DEFAULT NULL,
  `Demo` varchar(100) DEFAULT NULL,
  `POC` varchar(100) DEFAULT NULL,
  `ModificationDetail` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `team_backup`
--

INSERT INTO `team_backup` (`Representive`, `RMail`, `empid`, `name`, `email`, `password`, `contact`, `desig`, `Salary`, `doj`, `Department`, `EmployeeType`, `Product`, `SalesCertification`, `TechnicalCertification`, `Demo`, `POC`, `ModificationDetail`) VALUES
('Korai Purusottam', 'purusottam@navikra.com', '', 'A', 'a@gmail.com', '1', '7656765757', 'assd', '345535', '2018-03-11', 'Sales', 'Manager', '', NULL, NULL, NULL, NULL, '2018-03-11 17:18:19'),
('Korai Purusottam', 'purusottam@navikra.com', 'ntspl9', 'B', 'b@gmail.com', '1', '8686868686', 'yytyuu', '8766868', NULL, NULL, 'Manager', NULL, NULL, NULL, NULL, NULL, '2018-03-11 17:08:56'),
('Korai Purusottam', 'purusottam@navikra.com', 'ntspl10', 'D', 'd@gmail.com', '1', '9879868768', 'fhf', '65765', '2018-03-11', 'Technical', 'User', 'prod2', NULL, NULL, NULL, NULL, '2018-03-11 17:25:06'),
('Puru', 'puru@gmail.com', 'Employee Id', 'Employee Name', 'Email Id', 'Password', 'Mobile No', 'Designation', 'Salary', 'DOJ', 'Department', 'Employee Type', '', NULL, NULL, NULL, NULL, '2018-05-01 10:21:33'),
('Korai Purusottam', 'purusottam@navikra.com', 'ntspl8', 'zzz', 'fsdfs@dfsdf.vbn', 'aasddsa', '1232131231', 'sdwd', NULL, NULL, NULL, 'User', NULL, NULL, NULL, NULL, NULL, '2018-01-02 11:35:43'),
('Puru', 'puru@gmail.com', 'NTSPL015', 'dllnf', 'kpu@gmail.com', '123', '6765575757', 'asda', '765757575', '2018-04-17', 'Technical', 'Admin', '', NULL, NULL, NULL, NULL, '2018-05-01 10:21:25'),
('Korai Purusottam', 'purusottam@navikra.com', 'ntspl6', 'abc', 'vn@hhgh.com', '123', '8987656784', 'tech architect', NULL, NULL, NULL, 'User', NULL, NULL, NULL, NULL, NULL, '2018-01-02 11:36:52');

-- --------------------------------------------------------

--
-- Table structure for table `ticket`
--

CREATE TABLE IF NOT EXISTS `ticket` (
`ticket_id` int(250) NOT NULL,
  `department` varchar(20) DEFAULT NULL,
  `priority` varchar(20) DEFAULT NULL,
  `name` varchar(20) DEFAULT NULL,
  `email` varchar(20) DEFAULT NULL,
  `type` varchar(20) DEFAULT NULL,
  `reportto` varchar(20) DEFAULT NULL,
  `subject` varchar(100) DEFAULT NULL,
  `message` varchar(150) DEFAULT NULL,
  `filename` text,
  `status` int(20) NOT NULL DEFAULT '0',
  `AssignedTo` varchar(100) DEFAULT NULL,
  `AssignedBy` varchar(100) DEFAULT NULL,
  `comments` varchar(100) DEFAULT NULL,
  `ModificationDetail` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ticket`
--

INSERT INTO `ticket` (`ticket_id`, `department`, `priority`, `name`, `email`, `type`, `reportto`, `subject`, `message`, `filename`, `status`, `AssignedTo`, `AssignedBy`, `comments`, `ModificationDetail`) VALUES
(1, 'Sonali Nayak', 'Medium', '', '', 'Issue', '', 'sdgszhhx', 'hfgjhgxh', '', 1, 'raghu@navikra.com', 'purusottam@navikra.com', 'fgjfj', '2018-04-23 19:46:18'),
(2, 'Operations', 'Medium', 'user Hello', 'purusottam@navikra.c', 'Question', 'Subhom Kumar', 'fsgzzshh', 'fdhdxjdxj', '', 1, 'Sales', 'purusottam@navikra.com', 'sale', '2018-04-23 19:46:18'),
(3, 'Technical', 'High', 'user Hello', 'purusottam@navikra.c', 'Question', 'Puru', 'adwsdgdxfhxh', 'xdhdxhxdh', '', 0, NULL, NULL, NULL, '2018-04-23 19:47:37'),
(4, 'Technical', 'Medium', 'Korai Purusottam', 'purusottam@navikra.c', 'Issue', 'rtu', 'abcdefg', 'adasdad', '', 0, NULL, NULL, NULL, '2018-04-24 08:34:38');

-- --------------------------------------------------------

--
-- Table structure for table `tickethistory`
--

CREATE TABLE IF NOT EXISTS `tickethistory` (
`slno` int(255) NOT NULL,
  `TicketId` varchar(255) DEFAULT NULL,
  `Subject` text,
  `Message` text,
  `Priority` varchar(100) DEFAULT NULL,
  `AssignedTo` varchar(100) DEFAULT NULL,
  `AssignedBy` varchar(100) DEFAULT NULL,
  `ModificationDetail` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tickethistory`
--

INSERT INTO `tickethistory` (`slno`, `TicketId`, `Subject`, `Message`, `Priority`, `AssignedTo`, `AssignedBy`, `ModificationDetail`) VALUES
(1, '1', '', 'please do the work', '', 'purusottam@navikra.com', 'purusottam@navikra.com', '2018-04-24 08:48:21'),
(2, '1', '', 'do', '', 'puru@gmail.com', 'purusottam@navikra.com', '2018-04-24 09:21:16'),
(3, '1', '', 'work', '', 'puru@gmail.com,r.kumarraju@navikra.com', 'purusottam@navikra.com', '2018-04-24 09:22:08'),
(4, '1', '', 'do', '', 'puru@gmail.com', 'purusottam@navikra.com', '2018-04-24 09:24:48'),
(5, '2', '', 'sale', '', 'Sales', 'purusottam@navikra.com', '2018-04-24 20:48:45'),
(6, '1', '', 'ok', '', 'raghu@navikra.com', 'purusottam@navikra.com', '2018-05-03 19:48:51'),
(7, '1', '', 'fgjfj', '', 'raghu@navikra.com', 'purusottam@navikra.com', '2018-05-07 05:54:52');

-- --------------------------------------------------------

--
-- Table structure for table `training`
--

CREATE TABLE IF NOT EXISTS `training` (
`id` int(255) NOT NULL,
  `Representive` varchar(100) DEFAULT NULL,
  `RMail` varchar(100) DEFAULT NULL,
  `ProductName` varchar(100) DEFAULT NULL,
  `SalesCertificationPerMonth` varchar(100) DEFAULT NULL,
  `SalesCertificationPerYear` varchar(100) DEFAULT NULL,
  `TechnicalCertificationPerMonth` varchar(100) DEFAULT NULL,
  `TechnicalCertificationPerYear` varchar(100) DEFAULT NULL,
  `DemoPerMonth` varchar(100) DEFAULT NULL,
  `DemoPerYear` varchar(100) DEFAULT NULL,
  `POCPerMonth` varchar(100) DEFAULT NULL,
  `POCPerYear` varchar(100) DEFAULT NULL,
  `ModificationDetail` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `training`
--

INSERT INTO `training` (`id`, `Representive`, `RMail`, `ProductName`, `SalesCertificationPerMonth`, `SalesCertificationPerYear`, `TechnicalCertificationPerMonth`, `TechnicalCertificationPerYear`, `DemoPerMonth`, `DemoPerYear`, `POCPerMonth`, `POCPerYear`, `ModificationDetail`) VALUES
(1, 'Korai Purusottam', 'purusottam@navikra.com', 'All', '5', '85', '5', '85', '5', '85', '5', '85', '2018-01-05 12:40:21'),
(2, 'Korai Purusottam', 'purusottam@navikra.com', 'All', '5', '85', '5', '85', '5', '85', '5', '85', '2018-01-05 12:40:40'),
(3, 'Korai Purusottam', 'purusottam@navikra.com', 'All', '5', '85', '5', '85', '5', '85', '5', '85', '2018-01-05 12:42:22'),
(4, 'Korai Purusottam', 'purusottam@navikra.com', 'AWS', '5', '85', '5', '85', '5', '85', '5', '85', '2018-01-05 12:46:10'),
(5, 'Korai Purusottam', 'purusottam@navikra.com', 'All', '5', '85', '5', '85', '5', '85', '5', '85', '2018-01-05 12:47:21'),
(6, 'Korai Purusottam', 'purusottam@navikra.com', 'All', '5', '85', '5', '85', '5', '85', '5', '85', '2018-01-05 12:47:50'),
(7, 'Korai Purusottam', 'purusottam@navikra.com', 'All', '5', '85', '5', '85', '5', '85', '5', '85', '2018-01-05 12:48:55'),
(8, 'Korai Purusottam', 'purusottam@navikra.com', 'All', '5', '85', '5', '85', '5', '85', '5', '85', '2018-01-05 12:49:46'),
(9, 'Korai Purusottam', 'purusottam@navikra.com', 'All', '5', '85', '5', '85', '5', '85', '5', '85', '2018-01-05 12:52:50'),
(10, 'Korai Purusottam', 'purusottam@navikra.com', 'All', '5', '85', '5', '85', '5', '85', '5', '85', '2018-01-05 12:54:14'),
(11, 'Korai Purusottam', 'purusottam@navikra.com', 'Sophos', '5', '85', '5', '85', '5', '85', '5', '85', '2018-01-05 12:55:35'),
(12, 'Korai Purusottam', 'purusottam@navikra.com', 'All', '5', '85', '5', '85', '5', '85', '5', '85', '2018-01-05 13:11:00'),
(13, 'Korai Purusottam', 'purusottam@navikra.com', 'All', '5', '85', '5', '85', '5', '85', '5', '85', '2018-01-05 13:13:18'),
(14, 'Korai Purusottam', 'purusottam@navikra.com', 'All', '5', '85', '5', '85', '5', '85', '5', '85', '2018-01-05 13:26:49'),
(15, 'Korai Purusottam', 'purusottam@navikra.com', 'All', '5', '85', '5', '85', '5', '85', '5', '85', '2018-01-05 13:30:19'),
(16, 'Korai Purusottam', 'purusottam@navikra.com', 'All', '5', '85', '5', '85', '5', '85', '5', '85', '2018-01-05 13:39:04'),
(17, 'Korai Purusottam', 'purusottam@navikra.com', 'All', '5', '85', '5', '85', '5', '85', '5', '85', '2018-01-05 13:43:44'),
(18, '', 'raghu@navikra.com', 'All', '5', '20', '5', '20', '5', '20', '5', '20', '2018-01-05 13:44:50'),
(19, '', 'raghu@navikra.com', 'All', '5', '20', '5', '20', '5', '20', '5', '20', '2018-01-05 13:50:50'),
(20, '', 'raghu@navikra.com', 'All', '5', '20', '5', '20', '5', '20', '5', '20', '2018-01-05 13:50:50'),
(21, 'Raghu Vamsi', 'raghu@navikra.com', 'All', '5', '20', '5', '20', '5', '20', '5', '20', '2018-01-05 13:55:39');

-- --------------------------------------------------------

--
-- Table structure for table `vendor`
--

CREATE TABLE IF NOT EXISTS `vendor` (
`id` int(255) NOT NULL,
  `Representive` varchar(100) DEFAULT NULL,
  `RMail` varchar(100) DEFAULT NULL,
  `Name` varchar(100) DEFAULT NULL,
  `Address` text,
  `City` varchar(100) DEFAULT NULL,
  `State` varchar(100) DEFAULT NULL,
  `Pin` varchar(100) DEFAULT NULL,
  `GST` varchar(100) DEFAULT NULL,
  `GSTNo` varchar(100) DEFAULT NULL,
  `PANNo` varchar(100) DEFAULT NULL,
  `Product` varchar(100) DEFAULT NULL,
  `ModificationDetail` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vendor`
--

INSERT INTO `vendor` (`id`, `Representive`, `RMail`, `Name`, `Address`, `City`, `State`, `Pin`, `GST`, `GSTNo`, `PANNo`, `Product`, `ModificationDetail`) VALUES
(2, 'Puru', 'puru@gmail.com', 'IBM', 'Bangalore', 'Bangalore', 'Karnataka', '560102', '18', 'ABCDEFGHIJKLM123', '123456789', 'Maas360', '2018-05-02 10:09:54'),
(3, 'Puru', 'puru@gmail.com', 'Slack', 'Bangalore', 'Bangalore', 'Karnataka', '560102', '18', 'ABCDEFGHIJKLM123', '1234567789', 'Slack', '2018-05-02 10:39:50');

-- --------------------------------------------------------

--
-- Table structure for table `vendor_backup`
--

CREATE TABLE IF NOT EXISTS `vendor_backup` (
`id` int(255) NOT NULL,
  `Representive` varchar(100) DEFAULT NULL,
  `RMail` varchar(100) DEFAULT NULL,
  `cid` varchar(100) DEFAULT NULL,
  `Name` varchar(100) DEFAULT NULL,
  `Address` text,
  `City` varchar(100) DEFAULT NULL,
  `State` varchar(100) DEFAULT NULL,
  `Pin` varchar(100) DEFAULT NULL,
  `GST` varchar(100) DEFAULT NULL,
  `GSTNo` varchar(100) DEFAULT NULL,
  `PANNo` varchar(100) DEFAULT NULL,
  `Product` varchar(100) DEFAULT NULL,
  `ModificationDetail` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vendor_backup`
--

INSERT INTO `vendor_backup` (`id`, `Representive`, `RMail`, `cid`, `Name`, `Address`, `City`, `State`, `Pin`, `GST`, `GSTNo`, `PANNo`, `Product`, `ModificationDetail`) VALUES
(1, 'Puru', 'puru@gmail.com', '1', 'Redington India Ltd', 'Sree Narayan Tower, 144 I, II Floor Sarjapuur Road, 5th Sector HSR Layout', 'Bengaluru', 'Karnataka', '560034', '', '33AABCR0347P1ZA', '', 'AWS', '2018-05-02 10:42:20');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `hr_holiday`
--
ALTER TABLE `hr_holiday`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hr_leave`
--
ALTER TABLE `hr_leave`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `incentive`
--
ALTER TABLE `incentive`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `invoice`
--
ALTER TABLE `invoice`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `invoice_upload`
--
ALTER TABLE `invoice_upload`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `leads`
--
ALTER TABLE `leads`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `location`
--
ALTER TABLE `location`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `loginhistory`
--
ALTER TABLE `loginhistory`
 ADD PRIMARY KEY (`slno`);

--
-- Indexes for table `meeting`
--
ALTER TABLE `meeting`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `navdev`
--
ALTER TABLE `navdev`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notice`
--
ALTER TABLE `notice`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `opf`
--
ALTER TABLE `opf`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `po`
--
ALTER TABLE `po`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `PONo` (`PONo`);

--
-- Indexes for table `po_upload`
--
ALTER TABLE `po_upload`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
 ADD PRIMARY KEY (`ProductId`);

--
-- Indexes for table `products_backup`
--
ALTER TABLE `products_backup`
 ADD PRIMARY KEY (`ProductId`);

--
-- Indexes for table `quotation`
--
ALTER TABLE `quotation`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `QuotNo` (`QuotNo`);

--
-- Indexes for table `quotation_last_update`
--
ALTER TABLE `quotation_last_update`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `QuotNo` (`QuotNo`);

--
-- Indexes for table `sector`
--
ALTER TABLE `sector`
 ADD PRIMARY KEY (`slno`);

--
-- Indexes for table `setting_margin`
--
ALTER TABLE `setting_margin`
 ADD PRIMARY KEY (`MarginId`);

--
-- Indexes for table `setting_tax`
--
ALTER TABLE `setting_tax`
 ADD PRIMARY KEY (`TaxId`);

--
-- Indexes for table `target`
--
ALTER TABLE `target`
 ADD PRIMARY KEY (`slno`);

--
-- Indexes for table `target_admin`
--
ALTER TABLE `target_admin`
 ADD PRIMARY KEY (`slno`);

--
-- Indexes for table `target_manage_product`
--
ALTER TABLE `target_manage_product`
 ADD PRIMARY KEY (`slno`);

--
-- Indexes for table `target_manage_resource`
--
ALTER TABLE `target_manage_resource`
 ADD PRIMARY KEY (`slno`);

--
-- Indexes for table `team`
--
ALTER TABLE `team`
 ADD PRIMARY KEY (`email`), ADD UNIQUE KEY `empid` (`empid`);

--
-- Indexes for table `team_backup`
--
ALTER TABLE `team_backup`
 ADD PRIMARY KEY (`email`), ADD UNIQUE KEY `empid` (`empid`);

--
-- Indexes for table `ticket`
--
ALTER TABLE `ticket`
 ADD PRIMARY KEY (`ticket_id`);

--
-- Indexes for table `tickethistory`
--
ALTER TABLE `tickethistory`
 ADD PRIMARY KEY (`slno`);

--
-- Indexes for table `training`
--
ALTER TABLE `training`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vendor`
--
ALTER TABLE `vendor`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vendor_backup`
--
ALTER TABLE `vendor_backup`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `hr_holiday`
--
ALTER TABLE `hr_holiday`
MODIFY `id` int(255) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `hr_leave`
--
ALTER TABLE `hr_leave`
MODIFY `id` int(255) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `incentive`
--
ALTER TABLE `incentive`
MODIFY `id` int(255) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `invoice`
--
ALTER TABLE `invoice`
MODIFY `id` int(255) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `invoice_upload`
--
ALTER TABLE `invoice_upload`
MODIFY `id` int(255) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `leads`
--
ALTER TABLE `leads`
MODIFY `id` int(255) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `location`
--
ALTER TABLE `location`
MODIFY `id` int(255) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1115;
--
-- AUTO_INCREMENT for table `loginhistory`
--
ALTER TABLE `loginhistory`
MODIFY `slno` int(255) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=40;
--
-- AUTO_INCREMENT for table `meeting`
--
ALTER TABLE `meeting`
MODIFY `id` int(255) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=57;
--
-- AUTO_INCREMENT for table `navdev`
--
ALTER TABLE `navdev`
MODIFY `id` int(255) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `notice`
--
ALTER TABLE `notice`
MODIFY `id` int(255) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `opf`
--
ALTER TABLE `opf`
MODIFY `id` int(255) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `po`
--
ALTER TABLE `po`
MODIFY `id` int(255) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `po_upload`
--
ALTER TABLE `po_upload`
MODIFY `id` int(255) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `quotation`
--
ALTER TABLE `quotation`
MODIFY `id` int(255) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=41;
--
-- AUTO_INCREMENT for table `quotation_last_update`
--
ALTER TABLE `quotation_last_update`
MODIFY `id` int(255) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `sector`
--
ALTER TABLE `sector`
MODIFY `slno` int(255) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `setting_margin`
--
ALTER TABLE `setting_margin`
MODIFY `MarginId` int(255) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `setting_tax`
--
ALTER TABLE `setting_tax`
MODIFY `TaxId` int(255) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `target`
--
ALTER TABLE `target`
MODIFY `slno` int(225) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `target_admin`
--
ALTER TABLE `target_admin`
MODIFY `slno` int(225) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `target_manage_product`
--
ALTER TABLE `target_manage_product`
MODIFY `slno` int(225) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `target_manage_resource`
--
ALTER TABLE `target_manage_resource`
MODIFY `slno` int(225) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `ticket`
--
ALTER TABLE `ticket`
MODIFY `ticket_id` int(250) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `tickethistory`
--
ALTER TABLE `tickethistory`
MODIFY `slno` int(255) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `training`
--
ALTER TABLE `training`
MODIFY `id` int(255) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT for table `vendor`
--
ALTER TABLE `vendor`
MODIFY `id` int(255) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `vendor_backup`
--
ALTER TABLE `vendor_backup`
MODIFY `id` int(255) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

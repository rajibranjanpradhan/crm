<?php
   include "session_handler.php";
   ?>
<!DOCTYPE html>
<html class="no-js css-menubar" lang="en">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
      <meta name="description" content="bootstrap material admin template">
      <meta name="author" content="">
      <title>Quotation Generate | Bizapp CRM</title>
      <link rel="apple-touch-icon" href="../../assets/images/apple-touch-icon.png">
      <link rel="shortcut icon" href="../../assets/images/favicon.ico">
      <!-- Stylesheets -->
      <link rel="stylesheet" href="../../assets/global/css/bootstrap.min.css">
      <link rel="stylesheet" href="../../assets/global/css/bootstrap-extend.min.css">
      <link rel="stylesheet" href="../../assets/css/site.min.css">
      <!-- Plugins -->
      <link rel="stylesheet" href="../../assets/global/vendor/animsition/animsition.css">
      <link rel="stylesheet" href="../../assets/global/vendor/asscrollable/asScrollable.css">
      <link rel="stylesheet" href="../../assets/global/vendor/switchery/switchery.css">
      <link rel="stylesheet" href="../../assets/global/vendor/intro-js/introjs.css">
      <link rel="stylesheet" href="../../assets/global/vendor/slidepanel/slidePanel.css">
      <link rel="stylesheet" href="../../assets/global/vendor/jquery-mmenu/jquery-mmenu.css">
      <link rel="stylesheet" href="../../assets/global/vendor/flag-icon-css/flag-icon.css">
      <link rel="stylesheet" href="../../assets/global/vendor/waves/waves.css">
      <link rel="stylesheet" href="../../assets/global/vendor/chartist/chartist.css">
      <link rel="stylesheet" href="../../assets/global/vendor/jvectormap/jquery-jvectormap.css">
      <link rel="stylesheet" href="../../assets/global/vendor/chartist-plugin-tooltip/chartist-plugin-tooltip.css">
      <link rel="stylesheet" href="../../assets/examples/css/dashboard/v1.css">
      <!-- Fonts -->
      <link rel="stylesheet" href="../../assets/global/fonts/material-design/material-design.min.css">
      <link rel="stylesheet" href="../../assets/global/fonts/brand-icons/brand-icons.min.css">
      <link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Roboto:300,400,500,300italic'>
      <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
      <!-- table set -->
      <link rel='stylesheet' href='../../assets/css/customised-crm.css'>
      <script src="../../assets/js/customised-crm.js"></script>

      <link rel="stylesheet" href="../../assets/global/vendor/select2/select2.css">
    <link rel="stylesheet" href="../../assets/global/vendor/bootstrap-select/bootstrap-select.css">
    <link rel="stylesheet" href="../../assets/global/vendor/multi-select/multi-select.css">


    <link rel="stylesheet" href="../../assets/examples/css/uikit/modals.css">

    <?php include "includes/css/datepicker.php"; ?>
      <!--[if lt IE 9]>
      <script src="../../assets/global/vendor/html5shiv/html5shiv.min.js"></script>
      <![endif]-->
      <!--[if lt IE 10]>
      <script src="../../assets/global/vendor/media-match/media.match.min.js"></script>
      <script src="../../assets/global/vendor/respond/respond.min.js"></script>
      <![endif]-->
      <!-- Scripts -->
      <script src="../../assets/global/vendor/breakpoints/breakpoints.js"></script>
      <script>
         Breakpoints();
      </script>
   </head>
   <body class="animsition site-navbar-small dashboard">
      <!--[if lt IE 8]>
      <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
      <![endif]-->
      <?php include "navbar-header.php"; ?>
      <?php include "side-navigation.php"; ?>  
      <?php
    $_SESSION['accessgeneral']=0;
    $_SESSION['accesslicence']=0;
    $_SESSION['accesstax']=0;
    $_SESSION['accesstaxvalue']=2;
    $_SESSION['accessupload']=0;

    ?> 
      <!-- Page -->
      <div class="page">
         <div class="page-content" >
            <!-- Panel Basic -->
            
            <!-- input customer details  start -->
            <div class="panel">
               <div class="panel-body nav-tabs-animate nav-tabs-horizontal" data-plugin="tabs">
                  <h4 class="example-title">Generate Quotation</h4>
                  <ul class="nav nav-tabs nav-tabs-line" role="tablist">
                     <li class="nav-item" role="presentation">
                        <a class="active nav-link" data-toggle="tab" href="#general" aria-controls="general" role="tab">General</a>
                     </li>
                     <!-- <li class="nav-item" role="presentation">
                        <a class="nav-link" data-toggle="tab" href="#licence" aria-controls="or" role="tab">Licence</a>
                     </li> -->
                     <li class="nav-item" role="presentation">
                        <a class="nav-link" data-toggle="tab" href="#multipletax" aria-controls="multipletax" role="tab">Multiple Tax</a>
                     </li>
                     <!-- <li class="nav-item" role="presentation">
                        <a class="nav-link" data-toggle="tab" href="#multipleupload" aria-controls="multipleupload" role="tab">Multiple Upload</a>
                     </li> -->
                  </ul>
                  <!--  general start -->
                  <div class="tab-content">
                     <div class="tab-pane active animation-slide-left" id="general" role="tabpanel">
                        <div class="example">
                           <form action="../auth/order/quotation/generate.php" method="post"  enctype="multipart/form-data" autocomplete="off">
                              <input type="hidden" name="quotformname" value="general">
                              
                              <div class="row">
                                 <div class="form-group  col-md-4">
                                    <label class="form-control-label" >Customer Name</label>
                                    <select class="form-control" name="company"  data-plugin="select2" required="required" >
                                    <?php
                                 $project=mysqli_query($dbc,"select distinct(Company) from `customers`  order by id desc");
                                 echo '<option value="" >Select</option>';
                                 while($row=mysqli_fetch_assoc($project))
                                 {
                                     //$pro=$row['ProjectName'];
                                 
                                      echo '<option  value="'.$row['Company'].'" >'.$row['Company'].'</option>';
                                     
                                 }
                                 ?>
                                    </select>
                                 </div>
                                 <div class="form-group  col-md-2">
                                    <label class="form-control-label" >&nbsp;</label><br>
                                    <button class="btn btn-primary" data-target="#modalAddCustomer" data-toggle="modal" type="button"><i class="icon md-account "></i>Add New Customer</button>
                                 </div>
                                 <div class="form-group  col-md-2">
                                    <label class="form-control-label" >&nbsp;</label><br>
                                    <button class="btn btn-primary" data-target="#modalAddProduct" data-toggle="modal" type="button"><i class="icon md-mall  "></i>Add New Product</button>
                                 </div>
                                 </div>
                                 <hr>

                                 
                                 <div class="row">

                                 <div class="form-group  col-md-4">
                                    <label class="form-control-label" >Product <?php echo $_SESSION['accessgeneral']+1; ?></label>
                                    <select class="form-control" name="product[]"  data-plugin="select2" required="required" >
                                    <?php
                                 $project=mysqli_query($dbc,"select distinct(ProductName) from `products` ");
                                 echo '<option value="" >Select</option>';
                                 while($row=mysqli_fetch_assoc($project))
                                 {
                                     //$pro=$row['ProjectName'];
                                 
                                      echo '<option  value="'.$row['ProductName'].'">'.$row['ProductName'].'</option>';
                                     
                                 }
                                 ?>
                                    </select>
                                 </div>
                              
                                       <div class="form-group  col-md-4">
                                          <label class="form-control-label" >Part Description <?php echo $_SESSION['accessgeneral']+1; ?></label>
                                          <input type="text" class="form-control" name="partdesc[]"  placeholder="Part Description" >
                                       </div>

                                       <div class="form-group  col-md-4">
                                          <label class="form-control-label" >Quantity <?php echo $_SESSION['accessgeneral']+1; ?></label>
                                          <input type="number" class="form-control" name="qty[]"  placeholder="Quantity" >
                                       </div>

                                    </div>
                                       <div class="row">
                                    
                                       <div class="form-group  col-md-4">
                                          <label class="form-control-label" >Unit Price <?php echo $_SESSION['accessgeneral']+1; ?></label>
                                          <input type="number" class="form-control" name="unitprice[]" placeholder="Unit Price"  >
                                       </div>
                                       <div class="form-group  col-md-4">
                                          <label class="form-control-label" >OEM Quotation <?php echo $_SESSION['accessgeneral']+1; ?></label>
                                          <input type="file" class="form-control" name="uplfiles[]"  placeholder="OEM Quotation" >
                                       </div>
                                       <div class="form-group  col-md-4">
                                          <label class="form-control-label" >&nbsp;</label><br>
                                          <span class="input-group-btn">
                                          <button class="btn btn-success btn-add-basic" type="button" onClick="addMore();">

                                          <span class="icon md-plus" ></span>
                                          </button>
                                          <!-- <button class="btn btn-danger btn-add-basic" type="button" onClick="deleteRow();">

                                          <span class="icon md-minus" ></span>
                                          </button> -->
                                          </span>
                                       </div>
                                    </div>

                                    <SCRIPT>
                              function addMore() {
                                  $("<div>").load("quotgen-general.php", function() {
                                          $("#linegeneral").append($(this).html());
                                  }); 
                              }
                              function deleteRow() {
                                  $('div.linegeneral').each(function(index, item){
                                      jQuery(':checkbox', this).each(function () {
                                          if ($(this).is(':checked')) {
                                              $(item).remove();
                                          }
                                      });
                                  });
                              }
                           </SCRIPT>

                          
                                 <div id="linegeneral">
                                    
                                 </div>
                              
                                    <hr>
                                    <div class="row">

                                 <div class="form-group  col-md-4">
                                    <label class="form-control-label" >Currency</label>
                                    <select class="form-control" name="currency"  data-plugin="select2" required="required" >
                                    <option value="rupee">Rupee &#8377;</option>
                                       <option value="dollar">Dollar &#036;</option>
                                       <option value="euro">Euro &euro;</option>
                                       <option value="pound">Pound &pound;</option>
                                    </select>
                                 </div>
                              
                                       <div class="form-group  col-md-4">
                                          <label class="form-control-label" >HSN/SAC</label>
                                          <input type="text" class="form-control" name="hsnsac[]"  placeholder="HSN/SAC" >
                                       </div>

                                       <div class="form-group  col-md-4">
                                          <label class="form-control-label" >Tax</label>
                                          <select class="form-control" name="tax[]" multiple="multiple" data-plugin="select2" required="required" >
                                          <?php
                                       $project=mysqli_query($dbc,"select distinct(TaxName),TaxPercentage from `setting_tax`");
                                       
                                       while($row=mysqli_fetch_assoc($project))
                                       {
                                           //$pro=$row['ProjectName'];
                                       
                                            echo '<option  value="'.$row['TaxName'].'-'.$row['TaxPercentage'].'">'.$row['TaxName'].' '.$row['TaxPercentage'].'%'.'</option>';
                                           
                                       }
                                       ?>
                                          </select>
                                       </div>
                                    </div>
                                    <div class="row">

                                 <div class="form-group  col-md-4">
                                    <label class="form-control-label" >Validity (In Days)</label>
                                    <input type="number" class="form-control" name="validity"  placeholder="Validity (In Days)" >
                                 </div>
                              
                                       <div class="form-group  col-md-4">
                                          <label class="form-control-label" >Delivery (In Days)</label>
                                          <input type="number" class="form-control" name="delivery"  placeholder="Delivery (In Days)" >
                                       </div>

                                       <div class="form-group  col-md-4">
                                          <label class="form-control-label" >Payment</label>
                                          <select class="form-control" name="payment" data-plugin="select2" required="required" >
                                          <option selected value="100% Advance">100% Advance</option>
                                       <option value="30 Days">30 Days</option>
                                       <option value="45 Days">45 Days</option>
                                       <option value="60 Days">60 Days</option>
                                       <option value="90 Days">90 Days</option>
                                          </select>
                                       </div>
                                    </div>
                                    <div class="row">

                                 <div class="form-group  col-md-4">
                                    <label class="form-control-label" >Service Name</label>
                                    <input type="text" class="form-control" name="servicename"  placeholder="Service Name" >
                                 </div>
                              
                                       <div class="form-group  col-md-4">
                                          <label class="form-control-label" >Service Cost</label>
                                          <input type="number" class="form-control" name="servicecost"  placeholder="Service Cost" >
                                       </div>

                                       <div class="form-group  col-md-4">
                                          <label class="form-control-label" >Service Tax (%)</label>
                                          <input type="number" class="form-control" name="servicetax"  placeholder="Service Tax (%)" >
                                       </div>
                                    </div>

                                    <div class="row">

                                 <div class="form-group  col-md-4">
                                    <label class="form-control-label" >Freight Name</label>
                                    <input type="text" class="form-control" name="freightname"  placeholder="Freight Name" >
                                 </div>
                              
                                       <div class="form-group  col-md-4">
                                          <label class="form-control-label" >Freight Cost</label>
                                          <input type="number" class="form-control" name="freightcost"  placeholder="Freight Cost" >
                                       </div>

                                       <div class="form-group  col-md-4">
                                          <label class="form-control-label" >Freight Tax (%)</label>
                                          <input type="number" class="form-control" name="freighttax"  placeholder="Freight Tax (%)" >
                                       </div>
                                    </div>
                                    <div class="row">
                                       <div class="form-group  col-md-4">
                                    <label class="form-control-label" >Expected Closure</label>
                                    <input type="text" class="form-control" name="expectedclosure"  placeholder="Expected Closure" data-plugin="datepicker" >
                                 </div>

                                 <div class="form-group  col-md-4">
                                    <label class="form-control-label" >Note</label>
                                    <input type="text" class="form-control" name="remarks"  placeholder="Note" >
                                 </div>
                              
                                       <div class="form-group  col-md-4">
                                          <label class="form-control-label" >Other Attachment</label>
                                          <input type="file" class="form-control" name="attachment[]"  placeholder="Freight Cost" >
                                       </div>

                                       
                                    </div>

                                    <div class="row">
                                       <div class="form-group  col-md-4">
                                          <input type="checkbox"  name="addreq" value="Yes">Address Required in Quotation
                                       </div>
                                    </div>
                                    
                                 
                              <div class="form-group ">
                                 <button type="submit" name="submit" class="btn btn-primary">Submit</button>
                                 <button type="reset"  class="btn btn-default">Reset</button>
                              </div>
                           </form>
                        </div>
                     </div>
                     <!--  general end -->
                     <!--  Licence start -->
                     <div class="tab-pane animation-slide-left" id="licence" role="tabpanel">
                        <div class="example">
                           <form action="../auth/order/quotation/generate.php" method="post"  enctype="multipart/form-data" autocomplete="off">
                              <input type="hidden" name="quotformname" value="licence">
                              
                              <div class="row">
                                 <div class="form-group  col-md-4">
                                    <label class="form-control-label" >Customer Name</label>
                                    <select class="form-control" name="company"  data-plugin="select2" required="required" >
                                    <?php
                                 $project=mysqli_query($dbc,"select distinct(Company) from `customers`  order by id desc");
                                 echo '<option value="" >Select</option>';
                                 while($row=mysqli_fetch_assoc($project))
                                 {
                                     //$pro=$row['ProjectName'];
                                 
                                      echo '<option  value="'.$row['Company'].'" >'.$row['Company'].'</option>';
                                     
                                 }
                                 ?>
                                    </select>
                                 </div>
                                 <div class="form-group  col-md-2">
                                    <label class="form-control-label" >&nbsp;</label><br>
                                    <button class="btn btn-primary" data-target="#modalAddCustomer" data-toggle="modal" type="button"><i class="icon md-account "></i>Add New Customer</button>
                                 </div>
                                 <div class="form-group  col-md-2">
                                    <label class="form-control-label" >&nbsp;</label><br>
                                    <button class="btn btn-primary" data-target="#modalAddProduct" data-toggle="modal" type="button"><i class="icon md-mall  "></i>Add New Product</button>
                                 </div>
                                 </div>

                                 <hr>

                                 
                                 <div class="row">

                                 <div class="form-group  col-md-4">
                                    <label class="form-control-label" >Product <?php echo $_SESSION['accesslicence']+1; ?></label>
                                    <select class="form-control" name="product[]"  data-plugin="select2" required="required" >
                                    <?php
                                 $project=mysqli_query($dbc,"select distinct(ProductName) from `products` ");
                                 echo '<option value="" >Select</option>';
                                 while($row=mysqli_fetch_assoc($project))
                                 {
                                     //$pro=$row['ProjectName'];
                                 
                                      echo '<option  value="'.$row['ProductName'].'">'.$row['ProductName'].'</option>';
                                     
                                 }
                                 ?>
                                    </select>
                                 </div>
                              
                                       <div class="form-group  col-md-4">
                                          <label class="form-control-label" >Part Description <?php echo $_SESSION['accesslicence']+1; ?></label>
                                          <input type="text" class="form-control" name="partdesc[]"  placeholder="Part Description" >
                                       </div>

                                       <div class="form-group  col-md-4">
                                          <label class="form-control-label" >Quantity <?php echo $_SESSION['accesslicence']+1; ?></label>
                                          <input type="number" class="form-control" name="qty[]"  placeholder="Quantity" >
                                       </div>

                                    </div>
                                       <div class="row">
                                    
                                       <div class="form-group  col-md-4">
                                          <label class="form-control-label" >Unit Price</label>
                                          <input type="number" class="form-control" name="unitprice[]" placeholder="Unit Price"  >
                                       </div>
                                       <div class="form-group  col-md-4">
                                          <label class="form-control-label" >OEM Quotation <?php echo $_SESSION['accesslicence']+1; ?></label>
                                          <input type="file" class="form-control" name="uplfiles[]"  placeholder="OEM Quotation" >
                                       </div>
                                       <div class="form-group  col-md-2">
                                          <label class="form-control-label" >Licence For (In Years) <?php echo $_SESSION['accesslicence']+1; ?></label>
                                          <select class="form-control" name="licencefor[]"  data-plugin="select2" required="required" >
                                          <?php
                                       
                                          echo '<option value="" >Select</option>';
                                       for($l=1;$l<=10;$l++)
                                       {
                                           
                                       
                                            echo '<option  value="'.$l.'">'.$l.'</option>';
                                           
                                       }
                                       ?>
                                          </select>
                                       </div>
                                       <div class="form-group  col-md-2">
                                          <label class="form-control-label" >&nbsp;</label><br>
                                          <span class="input-group-btn">
                                          <button class="btn btn-success btn-add-basic" type="button" onClick="addMoreLicence();">

                                          <span class="icon md-plus" ></span>
                                          </button>
                                          <!-- <button class="btn btn-danger btn-add-basic" type="button" onClick="deleteRow();">

                                          <span class="icon md-minus" ></span>
                                          </button> -->
                                          </span>
                                       </div>
                                    </div>

                              <SCRIPT>
                              function addMoreLicence() {
                                  $("<div>").load("quotgen-licence.php", function() {
                                          $("#linelicence").append($(this).html());
                                  }); 
                              }
                              function deleteRowLicence() {
                                  $('div.linelicence').each(function(index, item){
                                      jQuery(':checkbox', this).each(function () {
                                          if ($(this).is(':checked')) {
                                              $(item).remove();
                                          }
                                      });
                                  });
                              }
                           </SCRIPT>

                          
                                 <div id="linelicence">
                                    
                                 </div>
                              
                                    <hr>
                                    <div class="row">

                                 <div class="form-group  col-md-4">
                                    <label class="form-control-label" >Currency</label>
                                    <select class="form-control" name="currency"  data-plugin="select2" required="required" >
                                    <option value="rupee">Rupee &#8377;</option>
                                       <option value="dollar">Dollar &#036;</option>
                                       <option value="euro">Euro &euro;</option>
                                       <option value="pound">Pound &pound;</option>
                                    </select>
                                 </div>
                              
                                       <div class="form-group  col-md-4">
                                          <label class="form-control-label" >HSN/SAC</label>
                                          <input type="text" class="form-control" name="hsnsac[]"  placeholder="HSN/SAC" >
                                       </div>

                                       <div class="form-group  col-md-4">
                                          <label class="form-control-label" >Tax</label>
                                          <select class="form-control" name="tax[]" multiple="multiple" data-plugin="select2" required="required" >
                                          <?php
                                       $project=mysqli_query($dbc,"select distinct(TaxName),TaxPercentage from `setting_tax`");
                                       
                                       while($row=mysqli_fetch_assoc($project))
                                       {
                                           //$pro=$row['ProjectName'];
                                       
                                            echo '<option  value="'.$row['TaxName'].'-'.$row['TaxPercentage'].'">'.$row['TaxName'].' '.$row['TaxPercentage'].'%'.'</option>';
                                           
                                       }
                                       ?>
                                          </select>
                                       </div>
                                       
                                    </div>
                                    <div class="row">

                                 <div class="form-group  col-md-4">
                                    <label class="form-control-label" >Validity (In Days)</label>
                                    <input type="number" class="form-control" name="validity"  placeholder="Validity (In Days)" >
                                 </div>
                              
                                       <div class="form-group  col-md-4">
                                          <label class="form-control-label" >Delivery (In Days)</label>
                                          <input type="number" class="form-control" name="delivery"  placeholder="Delivery (In Days)" >
                                       </div>

                                       <div class="form-group  col-md-4">
                                          <label class="form-control-label" >Payment</label>
                                          <select class="form-control" name="payment" data-plugin="select2" required="required" >
                                          <option selected value="100% Advance">100% Advance</option>
                                       <option value="30 Days">30 Days</option>
                                       <option value="45 Days">45 Days</option>
                                       <option value="60 Days">60 Days</option>
                                       <option value="90 Days">90 Days</option>
                                          </select>
                                       </div>
                                    </div>
                                    <div class="row">

                                 <div class="form-group  col-md-4">
                                    <label class="form-control-label" >Service Name</label>
                                    <input type="text" class="form-control" name="servicename"  placeholder="Service Name" >
                                 </div>
                              
                                       <div class="form-group  col-md-4">
                                          <label class="form-control-label" >Service Cost</label>
                                          <input type="number" class="form-control" name="servicecost"  placeholder="Service Cost" >
                                       </div>

                                       <div class="form-group  col-md-4">
                                          <label class="form-control-label" >Service Tax (%)</label>
                                          <input type="number" class="form-control" name="servicetax"  placeholder="Service Tax (%)" >
                                       </div>
                                    </div>

                                    <div class="row">

                                 <div class="form-group  col-md-4">
                                    <label class="form-control-label" >Freight Name</label>
                                    <input type="text" class="form-control" name="freightname"  placeholder="Freight Name" >
                                 </div>
                              
                                       <div class="form-group  col-md-4">
                                          <label class="form-control-label" >Freight Cost</label>
                                          <input type="number" class="form-control" name="freightcost"  placeholder="Freight Cost" >
                                       </div>

                                       <div class="form-group  col-md-4">
                                          <label class="form-control-label" >Freight Tax (%)</label>
                                          <input type="number" class="form-control" name="freighttax"  placeholder="Freight Tax (%)" >
                                       </div>
                                    </div>
                                    <div class="row">
                                       <div class="form-group  col-md-4">
                                    <label class="form-control-label" >Expected Closure</label>
                                    <input type="text" class="form-control" name="expectedclosure"  placeholder="Expected Closure" data-plugin="datepicker" >
                                 </div>

                                 <div class="form-group  col-md-4">
                                    <label class="form-control-label" >Note</label>
                                    <input type="text" class="form-control" name="remarks"  placeholder="Note" >
                                 </div>
                              
                                       <div class="form-group  col-md-4">
                                          <label class="form-control-label" >Other Attachment</label>
                                          <input type="file" class="form-control" name="attachment[]"  placeholder="Freight Cost" >
                                       </div>

                                       
                                    </div>
                                    <div class="row">
                                       <div class="form-group  col-md-4">
                                          <input type="checkbox"  name="addreq" value="Yes">Address Required in Quotation
                                       </div>
                                    </div>
                                    
                                 
                              <div class="form-group ">
                                 <button type="submit" name="submit" class="btn btn-primary">Submit</button>
                                 <button type="reset"  class="btn btn-default">Reset</button>
                              </div>
                           </form>
                        </div>
                           </div>
                           <!--  Licence end -->
                           <!--  Multiple Tax start -->
                     <div class="tab-pane animation-slide-left" id="multipletax" role="tabpanel">
                        <div class="example">
                           <form action="../auth/order/quotation/generate.php" method="post"  enctype="multipart/form-data" autocomplete="off">
                              <input type="hidden" name="quotformname" value="multipletax">
                              
                              <div class="row">
                                 <div class="form-group  col-md-4">
                                    <label class="form-control-label" >Customer Name</label>
                                    <select class="form-control" name="company"  data-plugin="select2" required="required" >
                                    <?php
                                 $project=mysqli_query($dbc,"select distinct(Company) from `customers`  order by id desc");
                                 echo '<option value="" >Select</option>';
                                 while($row=mysqli_fetch_assoc($project))
                                 {
                                     //$pro=$row['ProjectName'];
                                 
                                      echo '<option  value="'.$row['Company'].'" >'.$row['Company'].'</option>';
                                     
                                 }
                                 ?>
                                    </select>
                                 </div>
                                 <div class="form-group  col-md-2">
                                    <label class="form-control-label" >&nbsp;</label><br>
                                    <button class="btn btn-primary" data-target="#modalAddCustomer" data-toggle="modal" type="button"><i class="icon md-account "></i>Add New Customer</button>
                                 </div>
                                 <div class="form-group  col-md-2">
                                    <label class="form-control-label" >&nbsp;</label><br>
                                    <button class="btn btn-primary" data-target="#modalAddProduct" data-toggle="modal" type="button"><i class="icon md-mall  "></i>Add New Product</button>
                                 </div>
                                 </div>

                                 <hr>

                                 
                                 <div class="row">

                                 <div class="form-group  col-md-3">
                                    <label class="form-control-label" >Product <?php echo $_SESSION['accesstax']+1; ?></label>
                                    <select class="form-control" name="product[]"  data-plugin="select2" required="required" >
                                    <?php
                                 $project=mysqli_query($dbc,"select distinct(ProductName) from `products` ");
                                 echo '<option value="" >Select</option>';
                                 while($row=mysqli_fetch_assoc($project))
                                 {
                                     //$pro=$row['ProjectName'];
                                 
                                      echo '<option  value="'.$row['ProductName'].'">'.$row['ProductName'].'</option>';
                                     
                                 }
                                 ?>
                                    </select>
                                 </div>
                              
                                       <div class="form-group  col-md-3">
                                          <label class="form-control-label" >Part Description <?php echo $_SESSION['accesstax']+1; ?></label>
                                          <input type="text" class="form-control" name="partdesc[]"  placeholder="Part Description" >
                                       </div>

                                       <div class="form-group  col-md-3">
                                          <label class="form-control-label" >Quantity <?php echo $_SESSION['accesstax']+1; ?></label>
                                          <input type="number" class="form-control" name="qty[]"  placeholder="Quantity" >
                                       </div>
                                       <div class="form-group  col-md-3">
                                          <label class="form-control-label" >Unit Price <?php echo $_SESSION['accesstax']+1; ?></label>
                                          <input type="number" class="form-control" name="unitprice[]" placeholder="Unit Price"  >
                                       </div>

                                    </div>
                                       <div class="row">

                                          <div class="form-group  col-md-3">
                                          <label class="form-control-label" >HSN/SAC <?php echo $_SESSION['accesstax']+1; ?></label>
                                          <input type="text" class="form-control" name="hsnsac[]"  placeholder="HSN/SAC" >
                                       </div>

                                       <div class="form-group  col-md-3">
                                          <label class="form-control-label" >Tax <?php echo $_SESSION['accesstax']+1; ?></label>
                                          <select class="form-control" name="tax[]" multiple="multiple" data-plugin="select2" required="required" >
                                          <?php
                                       $project=mysqli_query($dbc,"select distinct(TaxName),TaxPercentage from `setting_tax`");
                                       
                                       while($row=mysqli_fetch_assoc($project))
                                       {
                                           //$pro=$row['ProjectName'];
                                       
                                            echo '<option  value="'.$row['TaxName'].'-'.$row['TaxPercentage'].'@'.$_SESSION['accesstaxvalue'].'">'.$row['TaxName'].' '.$row['TaxPercentage'].'%'.'</option>';
                                           
                                       }
                                       ?>
                                          </select>
                                       </div>
                                    
                                       
                                       <div class="form-group  col-md-3">
                                          <label class="form-control-label" >OEM Quotation <?php echo $_SESSION['accesstax']+1; ?></label>
                                          <input type="file" class="form-control" name="uplfiles[]"  placeholder="OEM Quotation" >
                                       </div>
                                       <div class="form-group  col-md-3">
                                          <label class="form-control-label" >&nbsp;</label><br>
                                          <span class="input-group-btn">
                                          <button class="btn btn-success btn-add-basic" type="button" onClick="addMoreTax();">

                                          <span class="icon md-plus" ></span>
                                          </button>
                                          <!-- <button class="btn btn-danger btn-add-basic" type="button" onClick="deleteRow();">

                                          <span class="icon md-minus" ></span>
                                          </button> -->
                                          </span>
                                       </div>
                                    </div>

                              <SCRIPT>
                                
                              function addMoreTax() {
                                 
                                  $("<div>").load("quotgen-multipletax.php", function() {
                                          $("#linemultipletax").append($(this).html());
                                  }); 
                              }
                              function deleteRowTax() {
                                  $('div.linemultipletax').each(function(index, item){
                                      jQuery(':checkbox', this).each(function () {
                                          if ($(this).is(':checked')) {
                                              $(item).remove();
                                          }
                                      });
                                  });
                              }
                           </SCRIPT>


                                 <div id="linemultipletax">
                                    
                                 </div>
                              
                                    <hr>
                                    <div class="row">

                                 <div class="form-group  col-md-6">
                                    <label class="form-control-label" >Currency</label>
                                    <select class="form-control" name="currency"  data-plugin="select2" required="required" >
                                    <option value="rupee">Rupee &#8377;</option>
                                       <option value="dollar">Dollar &#036;</option>
                                       <option value="euro">Euro &euro;</option>
                                       <option value="pound">Pound &pound;</option>
                                    </select>
                                 </div>

                                 <div class="form-group  col-md-6">
                                    <label class="form-control-label" >Validity (In Days)</label>
                                    <input type="number" class="form-control" name="validity"  placeholder="Validity (In Days)" >
                                 </div>
                              
                                       
                                       
                                    </div>
                                    <div class="row">

                                 
                              
                                       <div class="form-group  col-md-6">
                                          <label class="form-control-label" >Delivery (In Days)</label>
                                          <input type="number" class="form-control" name="delivery"  placeholder="Delivery (In Days)" >
                                       </div>

                                       <div class="form-group  col-md-6">
                                          <label class="form-control-label" >Payment</label>
                                          <select class="form-control" name="payment" data-plugin="select2" required="required" >
                                          <option selected value="100% Advance">100% Advance</option>
                                       <option value="30 Days">30 Days</option>
                                       <option value="45 Days">45 Days</option>
                                       <option value="60 Days">60 Days</option>
                                       <option value="90 Days">90 Days</option>
                                          </select>
                                       </div>
                                    </div>
                                    <div class="row">

                                 <div class="form-group  col-md-4">
                                    <label class="form-control-label" >Service Name</label>
                                    <input type="text" class="form-control" name="servicename"  placeholder="Service Name" >
                                 </div>
                              
                                       <div class="form-group  col-md-4">
                                          <label class="form-control-label" >Service Cost</label>
                                          <input type="number" class="form-control" name="servicecost"  placeholder="Service Cost" >
                                       </div>

                                       <div class="form-group  col-md-4">
                                          <label class="form-control-label" >Service Tax (%)</label>
                                          <input type="number" class="form-control" name="servicetax"  placeholder="Service Tax (%)" >
                                       </div>
                                    </div>

                                    <div class="row">

                                 <div class="form-group  col-md-4">
                                    <label class="form-control-label" >Freight Name</label>
                                    <input type="text" class="form-control" name="freightname"  placeholder="Freight Name" >
                                 </div>
                              
                                       <div class="form-group  col-md-4">
                                          <label class="form-control-label" >Freight Cost</label>
                                          <input type="number" class="form-control" name="freightcost"  placeholder="Freight Cost" >
                                       </div>

                                       <div class="form-group  col-md-4">
                                          <label class="form-control-label" >Freight Tax (%)</label>
                                          <input type="number" class="form-control" name="freighttax"  placeholder="Freight Tax (%)" >
                                       </div>
                                    </div>
                                    <div class="row">
                                       <div class="form-group  col-md-4">
                                    <label class="form-control-label" >Expected Closure</label>
                                    <input type="text" class="form-control" name="expectedclosure"  placeholder="Expected Closure" data-plugin="datepicker"  >
                                 </div>

                                 <div class="form-group  col-md-4">
                                    <label class="form-control-label" >Note</label>
                                    <input type="text" class="form-control" name="remarks"  placeholder="Note" >
                                 </div>
                              
                                       <div class="form-group  col-md-4">
                                          <label class="form-control-label" >Other Attachment</label>
                                          <input type="file" class="form-control" name="attachment[]"  placeholder="Freight Cost" >
                                       </div>

                                       
                                    </div>
                                    <div class="row">
                                       <div class="form-group  col-md-4">
                                          <input type="checkbox"  name="addreq" value="Yes">Address Required in Quotation
                                       </div>
                                    </div>
                                    
                                 
                              <div class="form-group ">
                                 <button type="submit" name="submit" class="btn btn-primary">Submit</button>
                                 <button type="reset"  class="btn btn-default">Reset</button>
                              </div>
                           </form>
                        </div>
                           </div>
                           <!--  Multiple Tax end -->
                           <!--  Multiple Upload start -->
                     <div class="tab-pane animation-slide-left" id="multipleupload" role="tabpanel">
                        <div class="example">
                           <form action="../auth/order/quotation/generate.php" method="post"  enctype="multipart/form-data" autocomplete="off">
                              <input type="hidden" name="quotformname" value="multipleupload">
                              
                              <div class="row">
                                 <div class="form-group  col-md-4">
                                    <label class="form-control-label" >Customer Name</label>
                                    <select class="form-control" name="company"  data-plugin="select2" required="required" >
                                    <?php
                                 $project=mysqli_query($dbc,"select distinct(Company) from `customers`  order by id desc");
                                 echo '<option value="" >Select</option>';
                                 while($row=mysqli_fetch_assoc($project))
                                 {
                                     //$pro=$row['ProjectName'];
                                 
                                      echo '<option  value="'.$row['Company'].'" >'.$row['Company'].'</option>';
                                     
                                 }
                                 ?>
                                    </select>
                                 </div>
                                 <div class="form-group  col-md-2">
                                    <label class="form-control-label" >&nbsp;</label><br>
                                    <button class="btn btn-primary" data-target="#modalAddCustomer" data-toggle="modal" type="button"><i class="icon md-account "></i>Add New Customer</button>
                                 </div>
                                 <div class="form-group  col-md-2">
                                    <label class="form-control-label" >&nbsp;</label><br>
                                    <button class="btn btn-primary" data-target="#modalAddProduct" data-toggle="modal" type="button"><i class="icon md-mall  "></i>Add New Product</button>
                                 </div>
                                 </div>

                                 <hr>

                                 
                                 <div class="row">

                                 <div class="form-group  col-md-3">
                                    <label class="form-control-label" >Product</label>
                                    <select class="form-control" name="product[]"  data-plugin="select2" required="required" >
                                    <?php
                                 $project=mysqli_query($dbc,"select distinct(ProductName) from `products` ");
                                 echo '<option value="" >Select</option>';
                                 while($row=mysqli_fetch_assoc($project))
                                 {
                                     //$pro=$row['ProjectName'];
                                 
                                      echo '<option  value="'.$row['ProductName'].'">'.$row['ProductName'].'</option>';
                                     
                                 }
                                 ?>
                                    </select>
                                 </div>
                              
                                       <div class="form-group  col-md-3">
                                          <label class="form-control-label" >Part Description</label>
                                          <input type="text" class="form-control" name="partdesc[]"  placeholder="Part Description" >
                                       </div>

                                       <div class="form-group  col-md-3">
                                          <label class="form-control-label" >Quantity</label>
                                          <input type="number" class="form-control" name="qty[]"  placeholder="Quantity" >
                                       </div>
                                       <div class="form-group  col-md-3">
                                          <label class="form-control-label" >Unit Price</label>
                                          <input type="number" class="form-control" name="unitprice[]" placeholder="Unit Price"  >
                                       </div>

                                    </div>
                                       <div class="row">

                                          <div class="form-group  col-md-3">
                                          <label class="form-control-label" >HSN/SAC</label>
                                          <input type="text" class="form-control" name="hsnsac[]"  placeholder="HSN/SAC" >
                                       </div>

                                       <div class="form-group  col-md-3">
                                          <label class="form-control-label" >Tax</label>
                                          <select class="form-control" name="tax[]" multiple="multiple" data-plugin="select2" required="required" >
                                          <?php
                                       $project=mysqli_query($dbc,"select distinct(TaxName),TaxPercentage from `setting_tax`");
                                       
                                       while($row=mysqli_fetch_assoc($project))
                                       {
                                           //$pro=$row['ProjectName'];
                                       
                                            echo '<option  value="'.$row['TaxName'].'-'.$row['TaxPercentage'].'">'.$row['TaxName'].' '.$row['TaxPercentage'].'%'.'</option>';
                                           
                                       }
                                       ?>
                                          </select>
                                       </div>
                                    
                                       
                                       <div class="form-group  col-md-3">
                                          <label class="form-control-label" >OEM Quotation</label>
                                          <input type="file" class="form-control" name="uplfiles[]"  placeholder="OEM Quotation" >
                                       </div>
                                       <div class="form-group  col-md-3">
                                          <label class="form-control-label" >&nbsp;</label><br>
                                          <span class="input-group-btn">
                                          <button class="btn btn-success btn-add-basic" type="button" onClick="addMoreUpload();">

                                          <span class="icon md-plus" ></span>
                                          </button>
                                          <!-- <button class="btn btn-danger btn-add-basic" type="button" onClick="deleteRow();">

                                          <span class="icon md-minus" ></span>
                                          </button> -->
                                          </span>
                                       </div>
                                    </div>

                              <SCRIPT>
                              function addMoreUpload() {
                                  $("<div>").load("quotgen-multipleupload.php", function() {
                                          $("#linemultipleupload").append($(this).html());
                                  }); 
                              }
                              function deleteRowUpload() {
                                  $('div.linemultipleupload').each(function(index, item){
                                      jQuery(':checkbox', this).each(function () {
                                          if ($(this).is(':checked')) {
                                              $(item).remove();
                                          }
                                      });
                                  });
                              }
                           </SCRIPT>

                          
                                 <div id="linemultipleupload">
                                    
                                 </div>
                              
                                    <hr>
                                    <div class="row">

                                 <div class="form-group  col-md-6">
                                    <label class="form-control-label" >Currency</label>
                                    <select class="form-control" name="currency"  data-plugin="select2" required="required" >
                                    <option value="rupee">Rupee &#8377;</option>
                                       <option value="dollar">Dollar &#036;</option>
                                       <option value="euro">Euro &euro;</option>
                                       <option value="pound">Pound &pound;</option>
                                    </select>
                                 </div>
                              
                                       
                                       <div class="form-group  col-md-6">
                                          <label class="form-control-label" >Licence For (In Years)</label>
                                          <select class="form-control" name="licencefor[]" multiple="multiple" data-plugin="select2" required="required" >
                                          <?php
                                       
                                       
                                       for($l=1;$l<=10;$l++)
                                       {
                                           //$pro=$row['ProjectName'];
                                       
                                            echo '<option  value="'.$l.'">'.$l.'</option>';
                                           
                                       }
                                       ?>
                                          </select>
                                       </div>
                                    </div>
                                    <div class="row">

                                 <div class="form-group  col-md-4">
                                    <label class="form-control-label" >Validity (In Days)</label>
                                    <input type="number" class="form-control" name="validity"  placeholder="Validity (In Days)" >
                                 </div>
                              
                                       <div class="form-group  col-md-4">
                                          <label class="form-control-label" >Delivery (In Days)</label>
                                          <input type="number" class="form-control" name="delivery"  placeholder="Delivery (In Days)" >
                                       </div>

                                       <div class="form-group  col-md-4">
                                          <label class="form-control-label" >Payment</label>
                                          <select class="form-control" name="payment" data-plugin="select2" required="required" >
                                          <option selected value="100% Advance">100% Advance</option>
                                       <option value="30 Days">30 Days</option>
                                       <option value="45 Days">45 Days</option>
                                       <option value="60 Days">60 Days</option>
                                       <option value="90 Days">90 Days</option>
                                          </select>
                                       </div>
                                    </div>
                                    <div class="row">

                                 <div class="form-group  col-md-4">
                                    <label class="form-control-label" >Service Name</label>
                                    <input type="text" class="form-control" name="servicename"  placeholder="Service Name" >
                                 </div>
                              
                                       <div class="form-group  col-md-4">
                                          <label class="form-control-label" >Service Cost</label>
                                          <input type="number" class="form-control" name="servicecost"  placeholder="Service Cost" >
                                       </div>

                                       <div class="form-group  col-md-4">
                                          <label class="form-control-label" >Service Tax (%)</label>
                                          <input type="number" class="form-control" name="servicetax"  placeholder="Service Tax (%)" >
                                       </div>
                                    </div>

                                    <div class="row">

                                 <div class="form-group  col-md-4">
                                    <label class="form-control-label" >Freight Name</label>
                                    <input type="text" class="form-control" name="freightname"  placeholder="Freight Name" >
                                 </div>
                              
                                       <div class="form-group  col-md-4">
                                          <label class="form-control-label" >Freight Cost</label>
                                          <input type="number" class="form-control" name="freightcost"  placeholder="Freight Cost" >
                                       </div>

                                       <div class="form-group  col-md-4">
                                          <label class="form-control-label" >Freight Tax (%)</label>
                                          <input type="number" class="form-control" name="freighttax"  placeholder="Freight Tax (%)" >
                                       </div>
                                    </div>
                                    <div class="row">
                                       <div class="form-group  col-md-4">
                                    <label class="form-control-label" >Expected Closure</label>
                                    <input type="text" class="form-control" name="expectedclosure"  placeholder="Expected Closure" data-plugin="datepicker" >
                                 </div>

                                 <div class="form-group  col-md-4">
                                    <label class="form-control-label" >Note</label>
                                    <input type="text" class="form-control" name="remarks"  placeholder="Note" >
                                 </div>
                              
                                       <div class="form-group  col-md-4">
                                          <label class="form-control-label" >Other Attachment</label>
                                          <input type="file" class="form-control" name="attachment[]"  placeholder="Freight Cost" >
                                       </div>

                                       
                                    </div>

                                    <div class="row">
                                       <div class="form-group  col-md-4">
                                          <input type="checkbox"  name="addreq" value="Yes">Address Required in Quotation
                                       </div>
                                    </div>
                                    
                                 
                              <div class="form-group ">
                                 <button type="submit" name="submit" class="btn btn-primary">Submit</button>
                                 <button type="reset"  class="btn btn-default">Reset</button>
                              </div>
                           </form>
                        </div>
                           </div>
                           <!--  Multiple Upload end -->
                        </div>
                     </div>
                  </div>
                  <!-- input customer details end -->
                <!-- End Panel Basic -->
                <!-- Modal Add Customer -->
                    <div class="modal fade" id="modalAddCustomer" aria-hidden="false" aria-labelledby="exampleFormModalLabel"
                      role="dialog" tabindex="-1">
                      <div class="modal-dialog modal-lg">
                        <form class="modal-content" action="../auth/customer/icmp.php" method="post" autocomplete="off">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">×</span>
                            </button>
                            <h4 class="modal-title" id="exampleFormModalLabel">Input Company Details</h4>
                          </div>
                          <div class="modal-body">
                           <h5 class="mb-lg">Company Information</h5>
                            <div class="row">
                                <input type="hidden" name="from" value="<?php echo $id; ?>">

                              <div class="form-group  col-md-6">
                                    <label class="form-control-label" >Company Name</label>
                                    <input type="text" class="form-control" name="company" placeholder="Company Name" onkeypress="return blockSpecialChar(event)" required="required" >
                                 </div>
                                 <div class="form-group  col-md-6">
                                    <label class="form-control-label" >Sector</label>
                                    <select class="form-control" name="sector"  data-plugin="select2" required="required" >
                                    <?php
                                       $comp=$_GET['c'];
                                       $project=mysqli_query($dbc,"select distinct(SectorName) from `sector`  ");
                                       echo '<option  value="">Select</option>';
                                       while($row=mysqli_fetch_assoc($project))
                                       {
                                         echo '<option  value="'.$row['SectorName'].'">'.$row['SectorName'].'</option>';
                                                                                 
                                       }
                                       ?>
                                    </select>
                                 </div>
                              </div>
                              <div class="row">
                                 <div class="form-group  col-md-6">
                                    <label class="form-control-label" >City</label>
                                    <input type="text" class="form-control" name="city" placeholder="City"  required="required" >
                                 </div>
                                 <div class="form-group  col-md-6">
                                    <label class="form-control-label" >State</label>
                                    <input type="text" class="form-control" name="state" placeholder="State"  required="required" >
                                 </div>
                              </div>
                              <h5 class="mb-lg">Contact Information</h5>
                              <div class="controls-basic">
                                 <div role="form" class="entry">
                                    <div class="row">
                                       <div class="form-group  col-md-6">
                                          <label class="form-control-label" >First Name</label>
                                          <input type="text" class="form-control" name="firstname" id="w4-f" onKeyUp="fname()" placeholder="First Name" required="required">
                                       </div>
                                       <div class="form-group  col-md-6">
                                          <label class="form-control-label" >Last Name</label>
                                          <input type="text" class="form-control" name="lastname" id="w4-l" onKeyUp="lname()" placeholder="Last Name" required="required" >
                                       </div>
                                    </div>
                                    <div class="row">
                                       <div class="form-group  col-md-6">
                                          <label class="form-control-label" >Email</label>
                                          <input type="email" class="form-control" name="email" placeholder="Email" required="required" >
                                       </div>
                                       <div class="form-group  col-md-6">
                                          <label class="form-control-label" >Mobile</label>
                                          <input type="text" class="form-control" name="mobile" id="w4-mob" placeholder="Mobile" onKeyUp="mob()" maxlength="12" required="required">
                                       </div>
                                    </div>
                                    <div class="row">
                                       <div class="form-group  col-md-6">
                                          <label class="form-control-label" >Designation</label>
                                          <input type="text" class="form-control" name="designation" placeholder="Designation" required="required" >
                                       </div>
                                       <div class="form-group  col-md-6">
                                          <label class="form-control-label" >&nbsp;</label><br>
                                          <span class="input-group-btn">
                                          <button class="btn btn-success btn-add-basic" type="button">
                                          <span class="icon md-plus" ></span>
                                          </button>
                                          </span>
                                       </div>
                                    </div>
                                 </div>
                              </div>


                              <div class="row">
                              
                              <div class="col-md-12 float-right">
                                <button class="btn btn-primary"  type="submit" name="submit">Submit</button>
                                <button class="btn btn-default"  type="reset" >Reset</button>
                                <button class="btn btn-default" data-dismiss="modal" type="reset" >Close</button>
                              </div>
                            </div>
                          </div>
                        </form>
                      </div>
                    </div>
                    <!-- End Modal add Customer -->
                    <!-- Modal add Product -->
                    <div class="modal fade" id="modalAddProduct" aria-hidden="false" aria-labelledby="exampleFormModalLabel"
                      role="dialog" tabindex="-1">
                      <div class="modal-dialog modal-lg">
                        <form class="modal-content" action="../auth/product/ins.php" method="post" autocomplete="off">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">×</span>
                            </button>
                            <h4 class="modal-title" id="exampleFormModalLabel">Input Product Details</h4>
                          </div>
                          <div class="modal-body">
                            <h5 class="mb-lg">OEM Information</h5>
                              <div class="row">
                                 <div class="form-group  col-md-6">
                                    <label class="form-control-label" >Name</label>
                                    <input type="text" class="form-control" name="oemname" placeholder="Name" onkeypress="return blockSpecialChar(event)" required="required" >
                                 </div>
                                 <div class="form-group  col-md-6">
                                    <label class="form-control-label" >Contact Peson</label>
                                    <input type="text" class="form-control" name="oemcontactperson" placeholder="Contact Person" onkeypress="return blockSpecialChar(event)" required="required" >
                                 </div>
                              </div>
                              <div class="row">
                                 <div class="form-group  col-md-6">
                                    <label class="form-control-label" >Contact Mail</label>
                                    <input type="email" class="form-control" name="oemcontactmail" placeholder="City"  required="required" >
                                 </div>
                                 <div class="form-group  col-md-6">
                                    <label class="form-control-label" >Contact No</label>
                                    <input type="number" class="form-control" name="oemcontactno" placeholder="Contact No" maxlength="10" required="required" >
                                 </div>
                              </div>
                              <h5 class="mb-lg">Product Information</h5>
                              
                                    <div class="row">
                                       <div class="form-group  col-md-6">
                                          <label class="form-control-label" >Category</label>
                                 <select name="category" class="form-control" data-plugin="select2">
                                        
                                    <option value="Infrastructure">Infrastructure</option>
                                    <option value="Security">Security</option>
                                            
                                    <option value="Software Development & Testing">Software Development & Testing</option>
                                    <option value="Digital Transformation">Digital Transformation</option>
                                    <option value="Analytics">Analytics</option>
                                                   
                                          </select>
                                       </div>
                                       <div class="form-group  col-md-6">
                                          <label class="form-control-label" >Product Name</label>
                                          <input type="text" class="form-control" name="productname" id="w4-l"  placeholder="Product Name" required="required" >
                                       </div>
                                    </div>
                                    <div class="row">
                                       <div class="form-group  col-md-6">
                                          <label class="form-control-label" >PAN Available</label>
                                       <select name="panavailable" class="form-control" data-plugin="select2">
                                          <option value="Yes">Yes</option>
                                          <option value="No">No</option>
                                       </select>
                                       </div>
                                       
                                    </div>


                                    <h5 class="mb-lg">Vendor/Seller Information</h5>
                                    <div class="controls">
                                 <div role="form" class="entry">
                              <div class="row">
                                 <div class="form-group  col-md-6">
                                    <label class="form-control-label" >Name</label>
                                    <input type="text" class="form-control" name="sellername[]" placeholder="Name" onkeypress="return blockSpecialChar(event)" required="required" >
                                 </div>
                                 <div class="form-group  col-md-6">
                                    <label class="form-control-label" >Contact Peson</label>
                                    <input type="text" class="form-control" name="sellercontactperson[]" placeholder="Contact Person" onkeypress="return blockSpecialChar(event)" required="required" >
                                 </div>
                              </div>
                              <div class="row">
                                 <div class="form-group  col-md-6">
                                    <label class="form-control-label" >Contact Mail</label>
                                    <input type="email" class="form-control" name="sellercontactmail[]" placeholder="Contact Mail"  required="required" >
                                 </div>
                                 <div class="form-group  col-md-5">
                                    <label class="form-control-label" >Contact No</label>
                                    <input type="number" class="form-control" name="sellercontactno[]" placeholder="Contact No"  required="required" >
                                 </div>
                                    <div class="form-group  col-md-1">
                                          <label class="form-control-label" >&nbsp;</label><br>
                                          <span class="input-group-btn">
                                          <button class="btn btn-success btn-add" type="button">
                                          <span class="icon md-plus" ></span>
                                          </button>
                                          </span>
                                       </div>
                                     </div>
                                   </div>
                                 </div>
                              <div class="row">
                              <div class="col-md-12 float-right">
                                <button class="btn btn-primary"  type="submit" name="submit">Submit</button>
                                <button class="btn btn-default"  type="reset" >Reset</button>
                                <button class="btn btn-default" data-dismiss="modal" type="reset" >Close</button>
                              </div>
                            </div>
                          </div>
                        </form>
                      </div>
                    </div>
                    <!-- End Modal add product -->
         </div>
      </div>
      <!-- End Page -->
      <!-- Footer -->
      <?php include "includes/footer.php"; ?>
      <!-- Core  -->
      <script src="../../assets/global/vendor/babel-external-helpers/babel-external-helpers.js"></script>
      <script src="../../assets/global/vendor/jquery/jquery.js"></script>
      <script src="../../assets/global/vendor/popper-js/umd/popper.min.js"></script>
      <script src="../../assets/global/vendor/bootstrap/bootstrap.js"></script>
      <script src="../../assets/global/vendor/animsition/animsition.js"></script>
      <script src="../../assets/global/vendor/mousewheel/jquery.mousewheel.js"></script>
      <script src="../../assets/global/vendor/asscrollbar/jquery-asScrollbar.js"></script>
      <script src="../../assets/global/vendor/asscrollable/jquery-asScrollable.js"></script>
      <script src="../../assets/global/vendor/waves/waves.js"></script>
      <!-- Plugins -->
      <script src="../../assets/global/vendor/jquery-mmenu/jquery.mmenu.min.all.js"></script>
      <script src="../../assets/global/vendor/switchery/switchery.js"></script>
      <script src="../../assets/global/vendor/intro-js/intro.js"></script>
      <script src="../../assets/global/vendor/screenfull/screenfull.js"></script>
      <script src="../../assets/global/vendor/slidepanel/jquery-slidePanel.js"></script>
      <script src="../../assets/global/vendor/chartist/chartist.min.js"></script>
      <script src="../../assets/global/vendor/chartist-plugin-tooltip/chartist-plugin-tooltip.js"></script>
      <script src="../../assets/global/vendor/jvectormap/jquery-jvectormap.min.js"></script>
      <script src="../../assets/global/vendor/jvectormap/maps/jquery-jvectormap-world-mill-en.js"></script>
      <script src="../../assets/global/vendor/matchheight/jquery.matchHeight-min.js"></script>
      <script src="../../assets/global/vendor/peity/jquery.peity.min.js"></script>
      <!-- Scripts -->
      <script src="../../assets/global/js/Component.js"></script>
      <script src="../../assets/global/js/Plugin.js"></script>
      <script src="../../assets/global/js/Base.js"></script>
      <script src="../../assets/global/js/Config.js"></script>
      <script src="section/Menubar.js"></script>
      <script src="section/Sidebar.js"></script>
      <script src="section/PageAside.js"></script>
      <script src="section/GridMenu.js"></script>
      <!-- Config -->
      <script src="../../assets/global/js/config/colors.js"></script>
      <script src="../../assets/js/config/tour.js"></script>
      <script>Config.set('assets', '../assets');</script>
      <!-- Page -->
      <script src="../../assets/js/Site.js"></script>
      <script src="../../assets/global/js/Plugin/asscrollable.js"></script>
      <script src="../../assets/global/js/Plugin/slidepanel.js"></script>
      <script src="../../assets/global/js/Plugin/switchery.js"></script>
      <script src="../../assets/global/js/Plugin/matchheight.js"></script>
      <script src="../../assets/global/js/Plugin/jvectormap.js"></script>
      <script src="../../assets/global/js/Plugin/peity.js"></script>
      <script src="../../assets/examples/js/dashboard/v1.js"></script>
      
      <script src="../../assets/global/vendor/select2/select2.full.min.js"></script>
<script src="../../assets/global/vendor/bootstrap-select/bootstrap-select.js"></script>
<script src="../../assets/global/vendor/multi-select/jquery.multi-select.js"></script>
<script src="../../assets/global/js/Plugin/select2.js"></script>
<script src="../../assets/global/js/Plugin/bootstrap-select.js"></script>
<script src="../../assets/global/js/Plugin/multi-select.js"></script>



      <?php include "includes/js/datepicker.php"; ?>
      <?php include "includes/js/dynamicaddition.php"; ?>
      <script src="../../assets/examples/js/forms/advanced.js"></script>
   </body>
</html>
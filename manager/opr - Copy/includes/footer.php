<footer class="site-footer">
      <div class="site-footer-legal">© 2018 <a href="http://www.bizapp.in" target="_blank" style="text-decoration: none;">Bizapp</a></div>
      <div class="site-footer-right">
        Product <i class="red-600 icon md-code"></i> by <a href="http://navikra.com/" target="_blank" style="text-decoration: none;">Navikra Tech Solutions Pvt Ltd.</a>
      </div>
</footer>
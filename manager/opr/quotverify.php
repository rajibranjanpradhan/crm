<?php
include "session_handler.php";
?>
<!DOCTYPE html>
<html class="no-js css-menubar" lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="description" content="bootstrap material admin template">
    <meta name="author" content="">
    
    <title>Quotverify | Bizapp CRM</title>
    
    <link rel="apple-touch-icon" href="../../assets/images/apple-touch-icon.png">
    <link rel="shortcut icon" href="../../assets/images/favicon.ico">
    
    <!-- Stylesheets -->
    <link rel="stylesheet" href="../../assets/global/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../assets/global/css/bootstrap-extend.min.css">
    <link rel="stylesheet" href="../../assets/css/site.min.css">
    
    <!-- Plugins -->
    <link rel="stylesheet" href="../../assets/global/vendor/animsition/animsition.css">
    <link rel="stylesheet" href="../../assets/global/vendor/asscrollable/asScrollable.css">
    <link rel="stylesheet" href="../../assets/global/vendor/switchery/switchery.css">
    <link rel="stylesheet" href="../../assets/global/vendor/intro-js/introjs.css">
    <link rel="stylesheet" href="../../assets/global/vendor/slidepanel/slidePanel.css">
    <link rel="stylesheet" href="../../assets/global/vendor/jquery-mmenu/jquery-mmenu.css">
    <link rel="stylesheet" href="../../assets/global/vendor/flag-icon-css/flag-icon.css">
    <link rel="stylesheet" href="../../assets/global/vendor/waves/waves.css">
        <link rel="stylesheet" href="../../assets/global/vendor/chartist/chartist.css">
        <link rel="stylesheet" href="../../assets/global/vendor/jvectormap/jquery-jvectormap.css">
        <link rel="stylesheet" href="../../assets/global/vendor/chartist-plugin-tooltip/chartist-plugin-tooltip.css">
        <link rel="stylesheet" href="../../assets/examples/css/dashboard/v1.css">
    
    
    <!-- Fonts -->
    <link rel="stylesheet" href="../../assets/global/fonts/material-design/material-design.min.css">
    <link rel="stylesheet" href="../../assets/global/fonts/brand-icons/brand-icons.min.css">
    <link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Roboto:300,400,500,300italic'>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

     <!-- table set -->
    

    <link rel='stylesheet' href='../../assets/css/customised-crm.css'>


    <?php include "includes/css/tables.php"; ?>
    
    <!--[if lt IE 9]>
    <script src="../../assets/global/vendor/html5shiv/html5shiv.min.js"></script>
    <![endif]-->
    
    <!--[if lt IE 10]>
    <script src="../../assets/global/vendor/media-match/media.match.min.js"></script>
    <script src="../../assets/global/vendor/respond/respond.min.js"></script>
    <![endif]-->
    
    <!-- Scripts -->
    <script src="../../assets/global/vendor/breakpoints/breakpoints.js"></script>
    <script>
      Breakpoints();
    </script>
  </head>
  <body class="animsition site-navbar-small dashboard">
    <!--[if lt IE 8]>
        <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->

    <?php include "navbar-header.php"; ?>
    <?php include "side-navigation.php"; ?>   
    

    <!-- Page -->
    <div class="page">
      <div class="page-content" >
        <div class="panel">
               <div class="panel-body container-fluid">
                  <div class="row row-lg">
                     <div class="col-md-12">
                        <div class="example-wrap">
                           <h4 class="example-title"></h4>
                           <div class="example">
                            <?php
                           $rep=$_SESSION['n'];
                               $rmail=$id;
                           
                           date_default_timezone_set('Asia/Kolkata');
                           
                           $date = date('d/m/Y');
                           $year = date('Y');
                           
                           $quotuniqueid=$_SESSION['quotuniqueid'];
                           
                           $fetchall=mysqli_query($dbc,"select * from `quotation` where `UniqueId`='$quotuniqueid' ");
                              
                               while($qr=mysqli_fetch_assoc($fetchall))
                               {
                                   $qid=$qr['id'];
                           
                               }
                               
                               $quotno=$companyquotformat."/00".$qid;
                               mysqli_query($dbc," update `quotation` set `QuotNo`='$quotno' where `UniqueId`='$quotuniqueid' ");
                               mysqli_query($dbc," update `quotation_all` set `QuotNo`='$quotno' where `UniqueId`='$quotuniqueid' ");
                           
                           
                           
                           /*$quotno="RBS/00".$ino;
                           
                               mysqli_query($dbc,"update `quotation` set `QuotNo`='$quotno' where `Funnelid`='$fid' ");
                           */
                           
                           
                           
                           $fetchall=mysqli_query($dbc,"select * from `quotation` where `UniqueId`='$quotuniqueid' ");
                           
                           while($row=mysqli_fetch_assoc($fetchall))
{
  $company=$row['Company'];
    /*$product=$row['Product'];*/
    $tax=$row['Tax'];
    $currency=$row['Currency'];
    /*$hsnsac=$row['HSNSAC'];*/
    $service=$row['Service'];

    $product1=$row['Product1'];
    $pdescription1=$row['PartDescription1'];
    $quantity1=$row['Quantity1'];
    $unitprice1=$row['UnitPrice1'];
    $hsnsac1=$row['HSNSAC1'];
    $licence1=$row['LicenceFor1'];

    $product2=$row['Product2'];
    $pdescription2=$row['PartDescription2'];
    $quantity2=$row['Quantity2'];
    $unitprice2=$row['UnitPrice2'];
    $hsnsac2=$row['HSNSAC2'];
    $licence2=$row['LicenceFor2'];

    $product3=$row['Product3'];
    $pdescription3=$row['PartDescription3'];
    $quantity3=$row['Quantity3'];
    $unitprice3=$row['UnitPrice3'];
    $hsnsac3=$row['HSNSAC3'];
    $licence3=$row['LicenceFor3'];

    $product4=$row['Product4'];
    $pdescription4=$row['PartDescription4'];
    $quantity4=$row['Quantity4'];
    $unitprice4=$row['UnitPrice4'];
    $hsnsac4=$row['HSNSAC4'];
    $licence4=$row['LicenceFor4'];

    $product5=$row['Product5'];
    $pdescription5=$row['PartDescription5'];
    $quantity5=$row['Quantity5'];
    $unitprice5=$row['UnitPrice5'];
    $hsnsac5=$row['HSNSAC5'];
    $licence5=$row['LicenceFor5'];

    $product6=$row['Product6'];
    $pdescription6=$row['PartDescription6'];
    $quantity6=$row['Quantity6'];
    $unitprice6=$row['UnitPrice6'];
    $hsnsac6=$row['HSNSAC6'];
    $licence6=$row['LicenceFor6'];

    $product7=$row['Product7'];
    $pdescription7=$row['PartDescription7'];
    $quantity7=$row['Quantity7'];
    $unitprice7=$row['UnitPrice7'];
    $hsnsac7=$row['HSNSAC7'];
    $licence7=$row['LicenceFor7'];

    $product8=$row['Product8'];
    $pdescription8=$row['PartDescription8'];
    $quantity8=$row['Quantity8'];
    $unitprice8=$row['UnitPrice8'];
    $hsnsac8=$row['HSNSAC8'];
    $licence8=$row['LicenceFor8'];

    $product9=$row['Product9'];
    $pdescription9=$row['PartDescription9'];
    $quantity9=$row['Quantity9'];
    $unitprice9=$row['UnitPrice9'];
    $hsnsac9=$row['HSNSAC9'];
    $licence9=$row['LicenceFor9'];

    $product10=$row['Product10'];
    $pdescription10=$row['PartDescription10'];
    $quantity10=$row['Quantity10'];
    $unitprice10=$row['UnitPrice10'];
    $hsnsac10=$row['HSNSAC10'];
    $licence10=$row['LicenceFor10'];

    $product11=$row['Product11'];
    $pdescription11=$row['PartDescription11'];
    $quantity11=$row['Quantity11'];
    $unitprice11=$row['UnitPrice11'];
    $hsnsac11=$row['HSNSAC11'];
    $licence11=$row['LicenceFor11'];

    $product12=$row['Product12'];
    $pdescription12=$row['PartDescription12'];
    $quantity12=$row['Quantity12'];
    $unitprice12=$row['UnitPrice12'];
    $hsnsac12=$row['HSNSAC12'];
    $licence12=$row['LicenceFor12'];

    $product13=$row['Product13'];
    $pdescription13=$row['PartDescription13'];
    $quantity13=$row['Quantity13'];
    $unitprice13=$row['UnitPrice13'];
    $hsnsac13=$row['HSNSAC13'];
    $licence13=$row['LicenceFor13'];

    $product14=$row['Product14'];
    $pdescription14=$row['PartDescription14'];
    $quantity14=$row['Quantity14'];
    $unitprice14=$row['UnitPrice14'];
    $hsnsac14=$row['HSNSAC14'];
    $licence14=$row['LicenceFor14'];

    $product15=$row['Product15'];
    $pdescription15=$row['PartDescription15'];
    $quantity15=$row['Quantity15'];
    $unitprice15=$row['UnitPrice15'];
    $hsnsac15=$row['HSNSAC15'];
    $licence15=$row['LicenceFor15'];

    $product16=$row['Product16'];
    $pdescription16=$row['PartDescription16'];
    $quantity16=$row['Quantity16'];
    $unitprice16=$row['UnitPrice16'];
    $hsnsac16=$row['HSNSAC16'];
    $licence16=$row['LicenceFor16'];

    $product17=$row['Product17'];
    $pdescription17=$row['PartDescription17'];
    $quantity17=$row['Quantity17'];
    $unitprice17=$row['UnitPrice17'];
    $hsnsac17=$row['HSNSAC17'];
    $licence17=$row['LicenceFor17'];

    $product18=$row['Product18'];
    $pdescription18=$row['PartDescription18'];
    $quantity18=$row['Quantity18'];
    $unitprice18=$row['UnitPrice18'];
    $hsnsac18=$row['HSNSAC18'];
    $licence18=$row['LicenceFor18'];

    $product19=$row['Product19'];
    $pdescription19=$row['PartDescription19'];
    $quantity19=$row['Quantity19'];
    $unitprice19=$row['UnitPrice19'];
    $hsnsac19=$row['HSNSAC19'];
    $licence19=$row['LicenceFor19'];

    $product20=$row['Product20'];
    $pdescription20=$row['PartDescription20'];
    $quantity20=$row['Quantity20'];
    $unitprice20=$row['UnitPrice20'];
    $hsnsac20=$row['HSNSAC20'];
    $licence20=$row['LicenceFor20'];

    $product21=$row['Product21'];
    $pdescription21=$row['PartDescription21'];
    $quantity21=$row['Quantity21'];
    $unitprice21=$row['UnitPrice21'];
    $hsnsac21=$row['HSNSAC21'];
    $licence21=$row['LicenceFor21'];

    $product22=$row['Product22'];
    $pdescription22=$row['PartDescription22'];
    $quantity22=$row['Quantity22'];
    $unitprice22=$row['UnitPrice22'];
    $hsnsac22=$row['HSNSAC22'];
    $licence22=$row['LicenceFor22'];

    $product23=$row['Product23'];
    $pdescription23=$row['PartDescription23'];
    $quantity23=$row['Quantity23'];
    $unitprice23=$row['UnitPrice23'];
    $hsnsac23=$row['HSNSAC23'];
    $licence23=$row['LicenceFor23'];

    $product24=$row['Product24'];
    $pdescription24=$row['PartDescription24'];
    $quantity24=$row['Quantity24'];
    $unitprice24=$row['UnitPrice24'];
    $hsnsac24=$row['HSNSAC24'];
    $licence24=$row['LicenceFor24'];

    $product25=$row['Product25'];
    $pdescription25=$row['PartDescription25'];
    $quantity25=$row['Quantity25'];
    $unitprice25=$row['UnitPrice25'];
    $hsnsac25=$row['HSNSAC25'];
    $licence25=$row['LicenceFor25'];

    $product26=$row['Product26'];
    $pdescription26=$row['PartDescription26'];
    $quantity26=$row['Quantity26'];
    $unitprice26=$row['UnitPrice26'];
    $hsnsac26=$row['HSNSAC26'];
    $licence26=$row['LicenceFor26'];

    $product27=$row['Product27'];
    $pdescription27=$row['PartDescription27'];
    $quantity27=$row['Quantity27'];
    $unitprice27=$row['UnitPrice27'];
    $hsnsac27=$row['HSNSAC27'];
    $licence27=$row['LicenceFor27'];

    $product28=$row['Product28'];
    $pdescription28=$row['PartDescription28'];
    $quantity28=$row['Quantity28'];
    $unitprice28=$row['UnitPrice28'];
    $hsnsac28=$row['HSNSAC28'];
    $licence28=$row['LicenceFor28'];

    $product29=$row['Product29'];
    $pdescription29=$row['PartDescription29'];
    $quantity29=$row['Quantity29'];
    $unitprice29=$row['UnitPrice29'];
    $hsnsac29=$row['HSNSAC29'];
    $licence29=$row['LicenceFor29'];

    $product30=$row['Product30'];
    $pdescription30=$row['PartDescription30'];
    $quantity30=$row['Quantity30'];
    $unitprice30=$row['UnitPrice30'];
    $hsnsac30=$row['HSNSAC30'];
    $licence30=$row['LicenceFor30'];

    $product31=$row['Product31'];
    $pdescription31=$row['PartDescription31'];
    $quantity31=$row['Quantity31'];
    $unitprice31=$row['UnitPrice31'];
    $hsnsac31=$row['HSNSAC31'];
    $licence31=$row['LicenceFor31'];

    $product32=$row['Product32'];
    $pdescription32=$row['PartDescription32'];
    $quantity32=$row['Quantity32'];
    $unitprice32=$row['UnitPrice32'];
    $hsnsac32=$row['HSNSAC32'];
    $licence32=$row['LicenceFor32'];

    $product33=$row['Product33'];
    $pdescription33=$row['PartDescription33'];
    $quantity33=$row['Quantity33'];
    $unitprice33=$row['UnitPrice33'];
    $hsnsac33=$row['HSNSAC33'];
    $licence33=$row['LicenceFor33'];

    $product34=$row['Product34'];
    $pdescription34=$row['PartDescription34'];
    $quantity34=$row['Quantity34'];
    $unitprice34=$row['UnitPrice34'];
    $hsnsac34=$row['HSNSAC34'];
    $licence34=$row['LicenceFor34'];

    $product35=$row['Product35'];
    $pdescription35=$row['PartDescription35'];
    $quantity35=$row['Quantity35'];
    $unitprice35=$row['UnitPrice35'];
    $hsnsac35=$row['HSNSAC35'];
    $licence35=$row['LicenceFor35'];

    $product36=$row['Product36'];
    $pdescription36=$row['PartDescription36'];
    $quantity36=$row['Quantity36'];
    $unitprice36=$row['UnitPrice36'];
    $hsnsac36=$row['HSNSAC36'];
    $licence36=$row['LicenceFor36'];

    $product37=$row['Product37'];
    $pdescription37=$row['PartDescription37'];
    $quantity37=$row['Quantity37'];
    $unitprice37=$row['UnitPrice37'];
    $hsnsac37=$row['HSNSAC37'];
    $licence37=$row['LicenceFor37'];

    $product38=$row['Product38'];
    $pdescription38=$row['PartDescription38'];
    $quantity38=$row['Quantity38'];
    $unitprice38=$row['UnitPrice38'];
    $hsnsac38=$row['HSNSAC38'];
    $licence38=$row['LicenceFor38'];

    $product39=$row['Product39'];
    $pdescription39=$row['PartDescription39'];
    $quantity39=$row['Quantity39'];
    $unitprice39=$row['UnitPrice39'];
    $hsnsac39=$row['HSNSAC39'];
    $licence39=$row['LicenceFor39'];

    $product40=$row['Product40'];
    $pdescription40=$row['PartDescription40'];
    $quantity40=$row['Quantity40'];
    $unitprice40=$row['UnitPrice40'];
    $hsnsac40=$row['HSNSAC40'];
    $licence40=$row['LicenceFor40'];

    $product41=$row['Product41'];
    $pdescription41=$row['PartDescription41'];
    $quantity41=$row['Quantity41'];
    $unitprice41=$row['UnitPrice41'];
    $hsnsac41=$row['HSNSAC41'];
    $licence41=$row['LicenceFor41'];

    $product42=$row['Product42'];
    $pdescription42=$row['PartDescription42'];
    $quantity42=$row['Quantity42'];
    $unitprice42=$row['UnitPrice42'];
    $hsnsac42=$row['HSNSAC42'];
    $licence42=$row['LicenceFor42'];

    $product43=$row['Product43'];
    $pdescription43=$row['PartDescription43'];
    $quantity43=$row['Quantity43'];
    $unitprice43=$row['UnitPrice43'];
    $hsnsac43=$row['HSNSAC43'];
    $licence43=$row['LicenceFor43'];

    $product44=$row['Product44'];
    $pdescription44=$row['PartDescription44'];
    $quantity44=$row['Quantity44'];
    $unitprice44=$row['UnitPrice44'];
    $hsnsac44=$row['HSNSAC44'];
    $licence44=$row['LicenceFor44'];

    $product45=$row['Product45'];
    $pdescription45=$row['PartDescription45'];
    $quantity45=$row['Quantity45'];
    $unitprice45=$row['UnitPrice45'];
    $hsnsac45=$row['HSNSAC45'];
    $licence45=$row['LicenceFor45'];

    $product46=$row['Product46'];
    $pdescription46=$row['PartDescription46'];
    $quantity46=$row['Quantity46'];
    $unitprice46=$row['UnitPrice46'];
    $hsnsac46=$row['HSNSAC46'];
    $licence46=$row['LicenceFor46'];

    $product47=$row['Product47'];
    $pdescription47=$row['PartDescription47'];
    $quantity47=$row['Quantity47'];
    $unitprice47=$row['UnitPrice47'];
    $hsnsac47=$row['HSNSAC47'];
    $licence47=$row['LicenceFor47'];

    $product48=$row['Product48'];
    $pdescription48=$row['PartDescription48'];
    $quantity48=$row['Quantity48'];
    $unitprice48=$row['UnitPrice48'];
    $hsnsac48=$row['HSNSAC48'];
    $licence48=$row['LicenceFor48'];

    $product49=$row['Product49'];
    $pdescription49=$row['PartDescription49'];
    $quantity49=$row['Quantity49'];
    $unitprice49=$row['UnitPrice49'];
    $hsnsac49=$row['HSNSAC49'];
    $licence49=$row['LicenceFor49'];

    $product50=$row['Product50'];
    $pdescription50=$row['PartDescription50'];
    $quantity50=$row['Quantity50'];
    $unitprice50=$row['UnitPrice50'];
    $hsnsac50=$row['HSNSAC50'];
    $licence50=$row['LicenceFor50'];

    $product51=$row['Product51'];
    $pdescription51=$row['PartDescription51'];
    $quantity51=$row['Quantity51'];
    $unitprice51=$row['UnitPrice51'];
    $hsnsac51=$row['HSNSAC51'];
    $licence51=$row['LicenceFor51'];

    $product52=$row['Product52'];
    $pdescription52=$row['PartDescription52'];
    $quantity52=$row['Quantity52'];
    $unitprice52=$row['UnitPrice52'];
    $hsnsac52=$row['HSNSAC52'];
    $licence52=$row['LicenceFor52'];

    $product53=$row['Product53'];
    $pdescription53=$row['PartDescription53'];
    $quantity53=$row['Quantity53'];
    $unitprice53=$row['UnitPrice53'];
    $hsnsac53=$row['HSNSAC53'];
    $licence53=$row['LicenceFor53'];

    $product54=$row['Product54'];
    $pdescription54=$row['PartDescription54'];
    $quantity54=$row['Quantity54'];
    $unitprice54=$row['UnitPrice54'];
    $hsnsac54=$row['HSNSAC54'];
    $licence54=$row['LicenceFor54'];

    $product55=$row['Product55'];
    $pdescription55=$row['PartDescription55'];
    $quantity55=$row['Quantity55'];
    $unitprice55=$row['UnitPrice55'];
    $hsnsac55=$row['HSNSAC55'];
    $licence55=$row['LicenceFor55'];

    $product56=$row['Product56'];
    $pdescription56=$row['PartDescription56'];
    $quantity56=$row['Quantity56'];
    $unitprice56=$row['UnitPrice56'];
    $hsnsac56=$row['HSNSAC56'];
    $licence56=$row['LicenceFor56'];

    $product57=$row['Product57'];
    $pdescription57=$row['PartDescription57'];
    $quantity57=$row['Quantity57'];
    $unitprice57=$row['UnitPrice57'];
    $hsnsac57=$row['HSNSAC57'];
    $licence57=$row['LicenceFor57'];

    $product58=$row['Product58'];
    $pdescription58=$row['PartDescription58'];
    $quantity58=$row['Quantity58'];
    $unitprice58=$row['UnitPrice58'];
    $hsnsac58=$row['HSNSAC58'];
    $licence58=$row['LicenceFor58'];

    $product59=$row['Product59'];
    $pdescription59=$row['PartDescription59'];
    $quantity59=$row['Quantity59'];
    $unitprice59=$row['UnitPrice59'];
    $hsnsac59=$row['HSNSAC59'];
    $licence59=$row['LicenceFor59'];

    $product60=$row['Product60'];
    $pdescription60=$row['PartDescription60'];
    $quantity60=$row['Quantity60'];
    $unitprice60=$row['UnitPrice60'];
    $hsnsac60=$row['HSNSAC60'];
    $licence60=$row['LicenceFor60'];

    $delivery=$row['Delivery'];
    $validity=$row['Validity'];
    $payment=$row['Payment'];

    $servicename=$row['ServiceName'];
    $servicecost=$row['ServiceCost'];
    $servicetax=$row['ServiceTax'];
    $freightname=$row['FreightName'];
    $freightcost=$row['FreightCost'];
    $freighttax=$row['FreightTax'];

    $quotpath1=$row['QuotationPath1'];
    $quotpath2=$row['QuotationPath2'];
    $quotpath3=$row['QuotationPath3'];
    $quotpath4=$row['QuotationPath4'];
    $quotpath5=$row['QuotationPath5'];
    $quotpath6=$row['QuotationPath6'];
    $quotpath7=$row['QuotationPath7'];
    $quotpath8=$row['QuotationPath8'];
    $quotpath9=$row['QuotationPath9'];
    $quotpath10=$row['QuotationPath10'];
    $quotpath11=$row['QuotationPath11'];
    $quotpath12=$row['QuotationPath12'];
    $quotpath13=$row['QuotationPath13'];
    $quotpath14=$row['QuotationPath14'];
    $quotpath15=$row['QuotationPath15'];
    $quotpath16=$row['QuotationPath16'];
    $quotpath17=$row['QuotationPath17'];
    $quotpath18=$row['QuotationPath18'];
    $quotpath19=$row['QuotationPath19'];
    $quotpath20=$row['QuotationPath20'];
    $quotpath21=$row['QuotationPath21'];
    $quotpath22=$row['QuotationPath22'];
    $quotpath23=$row['QuotationPath23'];
    $quotpath24=$row['QuotationPath24'];
    $quotpath25=$row['QuotationPath25'];
    $quotpath26=$row['QuotationPath26'];
    $quotpath27=$row['QuotationPath27'];
    $quotpath28=$row['QuotationPath28'];
    $quotpath29=$row['QuotationPath29'];
    $quotpath30=$row['QuotationPath30'];
    $quotpath31=$row['QuotationPath31'];
    $quotpath32=$row['QuotationPath32'];
    $quotpath33=$row['QuotationPath33'];
    $quotpath34=$row['QuotationPath34'];
    $quotpath35=$row['QuotationPath35'];
    $quotpath36=$row['QuotationPath36'];
    $quotpath37=$row['QuotationPath37'];
    $quotpath38=$row['QuotationPath38'];
    $quotpath39=$row['QuotationPath39'];
    $quotpath40=$row['QuotationPath40'];
    $quotpath41=$row['QuotationPath41'];
    $quotpath42=$row['QuotationPath42'];
    $quotpath43=$row['QuotationPath43'];
    $quotpath44=$row['QuotationPath44'];
    $quotpath45=$row['QuotationPath45'];
    $quotpath46=$row['QuotationPath46'];
    $quotpath47=$row['QuotationPath47'];
    $quotpath48=$row['QuotationPath48'];
    $quotpath49=$row['QuotationPath49'];
    $quotpath50=$row['QuotationPath50'];
    $quotpath51=$row['QuotationPath51'];
    $quotpath52=$row['QuotationPath52'];
    $quotpath53=$row['QuotationPath53'];
    $quotpath54=$row['QuotationPath54'];
    $quotpath55=$row['QuotationPath55'];
    $quotpath56=$row['QuotationPath56'];
    $quotpath57=$row['QuotationPath57'];
    $quotpath58=$row['QuotationPath58'];
    $quotpath59=$row['QuotationPath59'];
    $quotpath60=$row['QuotationPath60'];

    $otherfile=$row['Attachment1'];

    $remarks=$row['Remarks'];

    $quotformname=$row['QuotFormName'];

    $addressreq=$row['AddressReq'];

}
                           
                           $fetcit=mysqli_query($dbc,"select * from `customers` where `Company`='$company'");
                           while($frc=mysqli_fetch_assoc($fetcit))
                           {
                               $city=$frc['City'];
                               $firstname=$frc['FirstName'];
                               $address=$frc['Address'];
                           }
                           
                           if($currency == "rupee")
                           {
                               $cur="INR";
                           }
                           elseif($currency == "dollar")
                           {
                               $cur="USD";
                           }
                           elseif ($currency == "euro") {
                               $cur="EUR";
                           }
                           elseif ($currency == "pound") {
                               $cur="GBP";
                           }

                           /*$qtype=$_SESSION['qtype'];*/
                           $qu=$_SESSION['qu'];

                           if($qu == $quotuniqueid)
                           {
                              mysqli_query($dbc,"update  `quotation_all` set `UniqueId`='$quotuniqueid',`QuotNo`='$quotno',`Company`='$company',`Product1`='$product1',`Product2`='$product2',`Product3`='$product3',`Product4`='$product4',`Product5`='$product5',`Product6`='$product6',`Product7`='$product7',`Product8`='$product8',`Product9`='$product9',`Product10`='$product10',`Tax`='$tax',`Currency`='$currency',`HSNSAC`='$hsnsac',`ServiceName`='$servicename',`ServiceCost`='$servicecost',`ServiceTax`='$servicetax',`FreightName`='$freightname',`FreightCost`='$freightcost',`FreightTax`='$freighttax',`PartDescription1`='$pdescription1',`Quantity1`='$quantity1',`UnitPrice1`='$unitprice1',`PartDescription2`='$pdescription2',`Quantity2`='$quantity2',`UnitPrice2`='$unitprice2',`PartDescription3`='$pdescription3',`Quantity3`='$quantity3',`UnitPrice3`='$unitprice3',`PartDescription4`='$pdescription4',`Quantity4`='$quantity4',`UnitPrice4`='$unitprice4',`PartDescription5`='$pdescription5',`Quantity5`='$quantity5',`UnitPrice5`='$unitprice5',`PartDescription6`='$pdescription6',`Quantity6`='$quantity6',`UnitPrice6`='$unitprice6',`PartDescription7`='$pdescription7',`Quantity7`='$quantity7',`UnitPrice7`='$unitprice7',`PartDescription8`='$pdescription8',`Quantity8`='$quantity8',`UnitPrice8`='$unitprice8',`PartDescription9`='$pdescription9',`Quantity9`='$quantity9',`UnitPrice9`='$unitprice9',`PartDescription10`='$pdescription10',`Quantity10`='$quantity10',`UnitPrice10`='$unitprice10',`Validity`='$validity',`Delivery`='$delivery',`Payment`='$payment',`QuotationPath1`='$quotpath1',`QuotationPath2`='$quotpath2',`QuotationPath3`='$quotpath3',`QuotationPath4`='$quotpath4',`QuotationPath5`='$quotpath5',`QuotationPath6`='$quotpath6',`QuotationPath7`='$quotpath7',`QuotationPath8`='$quotpath8',`QuotationPath9`='$quotpath9',`QuotationPath10`='$quotpath10',`Attachment1`='$otherfile',`Remarks`='$remarks',`AddressReq`='$addressreq',`QuotFormName`='$quotformname',`LicenceFor1`='$licence1',`LicenceFor2`='$licence2',`LicenceFor3`='$licence3',`LicenceFor4`='$licence4',`LicenceFor5`='$licence5',`LicenceFor6`='$licence6',`LicenceFor7`='$licence7',`LicenceFor8`='$licence8',`LicenceFor9`='$licence9',`LicenceFor10`='$licence10',`Product11`='$product11',`PartDescription11`='$partdesc11',`Quantity11`='$quantity11',`UnitPrice11`='$unitprice11',`HSNSAC11`='$hsnsac11',`LicenceFor11`='$licence11',`QuotationPath11`='$quotpath11',`Product12`='$product12',`PartDescription12`='$partdesc12',`Quantity12`='$quantity12',`UnitPrice12`='$unitprice12',`HSNSAC12`='$hsnsac12',`LicenceFor12`='$licence12',`QuotationPath12`='$quotpath12',`Product13`='$product13',`PartDescription13`='$partdesc13',`Quantity13`='$quantity13',`UnitPrice13`='$unitprice13',`HSNSAC13`='$hsnsac13',`LicenceFor13`='$licence13',`QuotationPath13`='$quotpath13',`Product14`='$product14',`PartDescription14`='$partdesc14',`Quantity14`='$quantity14',`UnitPrice14`='$unitprice14',`HSNSAC14`='$hsnsac14',`LicenceFor14`='$licence14',`QuotationPath14`='$quotpath14',`Product15`='$product15',`PartDescription15`='$partdesc15',`Quantity15`='$quantity15',`UnitPrice15`='$unitprice15',`HSNSAC15`='$hsnsac15',`LicenceFor15`='$licence15',`QuotationPath15`='$quotpath15',`Product16`='$product16',`PartDescription16`='$partdesc16',`Quantity16`='$quantity16',`UnitPrice16`='$unitprice16',`HSNSAC16`='$hsnsac16',`LicenceFor16`='$licence16',`QuotationPath16`='$quotpath16',`Product17`='$product17',`PartDescription17`='$partdesc17',`Quantity17`='$quantity17',`UnitPrice17`='$unitprice17',`HSNSAC17`='$hsnsac17',`LicenceFor17`='$licence17',`QuotationPath17`='$quotpath17',`Product18`='$product18',`PartDescription18`='$partdesc18',`Quantity18`='$quantity18',`UnitPrice18`='$unitprice18',`HSNSAC18`='$hsnsac18',`LicenceFor18`='$licence18',`QuotationPath18`='$quotpath18',`Product19`='$product19',`PartDescription19`='$partdesc19',`Quantity19`='$quantity19',`UnitPrice19`='$unitprice19',`HSNSAC19`='$hsnsac19',`LicenceFor19`='$licence19',`QuotationPath19`='$quotpath19',`Product20`='$product20',`PartDescription20`='$partdesc20',`Quantity20`='$quantity20',`UnitPrice20`='$unitprice20',`HSNSAC20`='$hsnsac20',`LicenceFor20`='$licence20',`QuotationPath20`='$quotpath20',`Product21`='$product21',`PartDescription21`='$partdesc21',`Quantity21`='$quantity21',`UnitPrice21`='$unitprice21',`HSNSAC21`='$hsnsac21',`LicenceFor21`='$licence21',`QuotationPath21`='$quotpath21',`Product22`='$product22',`PartDescription22`='$partdesc22',`Quantity22`='$quantity22',`UnitPrice22`='$unitprice22',`HSNSAC22`='$hsnsac22',`LicenceFor22`='$licence22',`QuotationPath22`='$quotpath22',`Product23`='$product23',`PartDescription23`='$partdesc23',`Quantity23`='$quantity23',`UnitPrice23`='$unitprice23',`HSNSAC23`='$hsnsac23',`LicenceFor23`='$licence23',`QuotationPath23`='$quotpath23',`Product24`='$product24',`PartDescription24`='$partdesc24',`Quantity24`='$quantity24',`UnitPrice24`='$unitprice24',`HSNSAC24`='$hsnsac24',`LicenceFor24`='$licence24',`QuotationPath24`='$quotpath24',`Product25`='$product25',`PartDescription25`='$partdesc25',`Quantity25`='$quantity25',`UnitPrice25`='$unitprice25',`HSNSAC25`='$hsnsac25',`LicenceFor25`='$licence25',`QuotationPath25`='$quotpath25',`Product26`='$product26',`PartDescription26`='$partdesc26',`Quantity26`='$quantity26',`UnitPrice26`='$unitprice26',`HSNSAC26`='$hsnsac26',`LicenceFor26`='$licence26',`QuotationPath26`='$quotpath26',`Product27`='$product27',`PartDescription27`='$partdesc27',`Quantity27`='$quantity27',`UnitPrice27`='$unitprice27',`HSNSAC27`='$hsnsac27',`LicenceFor27`='$licence27',`QuotationPath27`='$quotpath27',`Product28`='$product28',`PartDescription28`='$partdesc28',`Quantity28`='$quantity28',`UnitPrice28`='$unitprice28',`HSNSAC28`='$hsnsac28',`LicenceFor28`='$licence28',`QuotationPath28`='$quotpath28',`Product29`='$product29',`PartDescription29`='$partdesc29',`Quantity29`='$quantity29',`UnitPrice29`='$unitprice29',`HSNSAC29`='$hsnsac29',`LicenceFor29`='$licence29',`QuotationPath29`='$quotpath29',`Product30`='$product30',`PartDescription30`='$partdesc30',`Quantity30`='$quantity30',`UnitPrice30`='$unitprice30',`HSNSAC30`='$hsnsac30',`LicenceFor30`='$licence30',`QuotationPath30`='$quotpath30',`Product31`='$product31',`PartDescription31`='$partdesc31',`Quantity31`='$quantity31',`UnitPrice31`='$unitprice31',`HSNSAC31`='$hsnsac31',`LicenceFor31`='$licence31',`QuotationPath31`='$quotpath31',`Product32`='$product32',`PartDescription32`='$partdesc32',`Quantity32`='$quantity32',`UnitPrice32`='$unitprice32',`HSNSAC32`='$hsnsac32',`LicenceFor32`='$licence32',`QuotationPath32`='$quotpath32',`Product33`='$product33',`PartDescription33`='$partdesc33',`Quantity33`='$quantity33',`UnitPrice33`='$unitprice33',`HSNSAC33`='$hsnsac33',`LicenceFor33`='$licence33',`QuotationPath33`='$quotpath33',`Product34`='$product34',`PartDescription34`='$partdesc34',`Quantity34`='$quantity34',`UnitPrice34`='$unitprice34',`HSNSAC34`='$hsnsac34',`LicenceFor34`='$licence34',`QuotationPath34`='$quotpath34',`Product35`='$product35',`PartDescription35`='$partdesc35',`Quantity35`='$quantity35',`UnitPrice35`='$unitprice35',`HSNSAC35`='$hsnsac35',`LicenceFor35`='$licence35',`QuotationPath35`='$quotpath35',`Product36`='$product36',`PartDescription36`='$partdesc36',`Quantity36`='$quantity36',`UnitPrice36`='$unitprice36',`HSNSAC36`='$hsnsac36',`LicenceFor36`='$licence36',`QuotationPath36`='$quotpath36',`Product37`='$product37',`PartDescription37`='$partdesc37',`Quantity37`='$quantity37',`UnitPrice37`='$unitprice37',`HSNSAC37`='$hsnsac37',`LicenceFor37`='$licence37',`QuotationPath37`='$quotpath37',`Product38`='$product38',`PartDescription38`='$partdesc38',`Quantity38`='$quantity38',`UnitPrice38`='$unitprice38',`HSNSAC38`='$hsnsac38',`LicenceFor38`='$licence38',`QuotationPath38`='$quotpath38',`Product39`='$product39',`PartDescription39`='$partdesc39',`Quantity39`='$quantity39',`UnitPrice39`='$unitprice39',`HSNSAC39`='$hsnsac39',`LicenceFor39`='$licence39',`QuotationPath39`='$quotpath39',`Product40`='$product40',`PartDescription40`='$partdesc40',`Quantity40`='$quantity40',`UnitPrice40`='$unitprice40',`HSNSAC40`='$hsnsac40',`LicenceFor40`='$licence40',`QuotationPath40`='$quotpath40',`Product41`='$product41',`PartDescription41`='$partdesc41',`Quantity41`='$quantity41',`UnitPrice41`='$unitprice41',`HSNSAC41`='$hsnsac41',`LicenceFor41`='$licence41',`QuotationPath41`='$quotpath41',`Product42`='$product42',`PartDescription42`='$partdesc42',`Quantity42`='$quantity42',`UnitPrice42`='$unitprice42',`HSNSAC42`='$hsnsac42',`LicenceFor42`='$licence42',`QuotationPath42`='$quotpath42',`Product43`='$product43',`PartDescription43`='$partdesc43',`Quantity43`='$quantity43',`UnitPrice43`='$unitprice43',`HSNSAC43`='$hsnsac43',`LicenceFor43`='$licence43',`QuotationPath43`='$quotpath43',`Product44`='$product44',`PartDescription44`='$partdesc44',`Quantity44`='$quantity44',`UnitPrice44`='$unitprice44',`HSNSAC44`='$hsnsac44',`LicenceFor44`='$licence44',`QuotationPath44`='$quotpath44',`Product45`='$product45',`PartDescription45`='$partdesc45',`Quantity45`='$quantity45',`UnitPrice45`='$unitprice45',`HSNSAC45`='$hsnsac45',`LicenceFor45`='$licence45',`QuotationPath45`='$quotpath45',`Product46`='$product46',`PartDescription46`='$partdesc46',`Quantity46`='$quantity46',`UnitPrice46`='$unitprice46',`HSNSAC46`='$hsnsac46',`LicenceFor46`='$licence46',`QuotationPath46`='$quotpath46',`Product47`='$product47',`PartDescription47`='$partdesc47',`Quantity47`='$quantity47',`UnitPrice47`='$unitprice47',`HSNSAC47`='$hsnsac47',`LicenceFor47`='$licence47',`QuotationPath47`='$quotpath47',`Product48`='$product48',`PartDescription48`='$partdesc48',`Quantity48`='$quantity48',`UnitPrice48`='$unitprice48',`HSNSAC48`='$hsnsac48',`LicenceFor48`='$licence48',`QuotationPath48`='$quotpath48',`Product49`='$product49',`PartDescription49`='$partdesc49',`Quantity49`='$quantity49',`UnitPrice49`='$unitprice49',`HSNSAC49`='$hsnsac49',`LicenceFor49`='$licence49',`QuotationPath49`='$quotpath49',`Product50`='$product50',`PartDescription50`='$partdesc50',`Quantity50`='$quantity50',`UnitPrice50`='$unitprice50',`HSNSAC50`='$hsnsac50',`LicenceFor50`='$licence50',`QuotationPath50`='$quotpath50',`Product51`='$product51',`PartDescription51`='$partdesc51',`Quantity51`='$quantity51',`UnitPrice51`='$unitprice51',`HSNSAC51`='$hsnsac51',`LicenceFor51`='$licence51',`QuotationPath51`='$quotpath51',`Product52`='$product52',`PartDescription52`='$partdesc52',`Quantity52`='$quantity52',`UnitPrice52`='$unitprice52',`HSNSAC52`='$hsnsac52',`LicenceFor52`='$licence52',`QuotationPath52`='$quotpath52',`Product53`='$product53',`PartDescription53`='$partdesc53',`Quantity53`='$quantity53',`UnitPrice53`='$unitprice53',`HSNSAC53`='$hsnsac53',`LicenceFor53`='$licence53',`QuotationPath53`='$quotpath53',`Product54`='$product54',`PartDescription54`='$partdesc54',`Quantity54`='$quantity54',`UnitPrice54`='$unitprice54',`HSNSAC54`='$hsnsac54',`LicenceFor54`='$licence54',`QuotationPath54`='$quotpath54',`Product55`='$product55',`PartDescription55`='$partdesc55',`Quantity55`='$quantity55',`UnitPrice55`='$unitprice55',`HSNSAC55`='$hsnsac55',`LicenceFor55`='$licence55',`QuotationPath55`='$quotpath55',`Product56`='$product56',`PartDescription56`='$partdesc56',`Quantity56`='$quantity56',`UnitPrice56`='$unitprice56',`HSNSAC56`='$hsnsac56',`LicenceFor56`='$licence56',`QuotationPath56`='$quotpath56',`Product57`='$product57',`PartDescription57`='$partdesc57',`Quantity57`='$quantity57',`UnitPrice57`='$unitprice57',`HSNSAC57`='$hsnsac57',`LicenceFor57`='$licence57',`QuotationPath57`='$quotpath57',`Product58`='$product58',`PartDescription58`='$partdesc58',`Quantity58`='$quantity58',`UnitPrice58`='$unitprice58',`HSNSAC58`='$hsnsac58',`LicenceFor58`='$licence58',`QuotationPath58`='$quotpath58',`Product59`='$product59',`PartDescription59`='$partdesc59',`Quantity59`='$quantity59',`UnitPrice59`='$unitprice59',`HSNSAC59`='$hsnsac59',`LicenceFor59`='$licence59',`QuotationPath59`='$quotpath59',`Product60`='$product60',`PartDescription60`='$partdesc60',`Quantity60`='$quantity60',`UnitPrice60`='$unitprice60',`HSNSAC60`='$hsnsac60',`LicenceFor60`='$licence60',`QuotationPath60`='$quotpath60' where `QuotNo`='$quotno' and `UniqueId`='$quotuniqueid' ");
                           }
                           else
                           {
                            $result=mysqli_query($dbc,"show columns from quotation_all");
                                    $k=0;
                                    $c=0;
                                    while($row = mysqli_fetch_array($result)){
                                        $str=$row['Field'];
                                       
                                       $newstr = filter_var($str, FILTER_SANITIZE_STRING);
                                       if($newstr="Product")
                                       {


                                       $int_id = filter_var($str, FILTER_SANITIZE_NUMBER_INT);
                                       
                                       if($int_id > $k)
                                       {
                                          global $c;
                                          $c=$int_id;
                                          $k=$int_id;
         
                                          }
                                       }
                                       
                                    }
                                    for ($i = 1; $i < 500; $i++) 
    {
        /*if (isset($_POST['partdesc'][$i], $_POST['qty'][$i],$_POST['unitprice'][$i])) { // Make sure both are filled in*/
            // Do stuff with this row of the form
            $product=$_POST['product'][$i];

            $partd=$_POST['partdesc'][$i];
            $qt=$_POST['qty'][$i];
            $unitp=$_POST['unitprice'][$i];
            $licencefor=$_POST['licencefor'][$i];
            $hsnsac=$_POST['hsnsac'][$i];

            

            if(!empty($product) and !empty($unitp) )
            {



            
            if($i=='1')
            {


           $ad=mysqli_query($dbc,"insert into `quotation_all` (`Representive`,`RMail`,`UniqueId`,`QuotNo`,`Company`,`Product1`,`Tax`,`Currency`,`HSNSAC1`,`ServiceName`,`ServiceCost`,`ServiceTax`,`FreightName`,`FreightCost`,`FreightTax`,`PartDescription1`,`Quantity1`,`UnitPrice1`,`Validity`,`Delivery`,`Payment`,`QuotationPath1`,`Attachment1`,`Remarks`,`AddressReq`,`LicenceFor1`,`ExpectedClosure`,`QuotFormName`) values ('$rep','$rmail','$quotuniqueid','$quotno','$company','$product','$tax','$currency','$hsnsac','$servicename','$servicecost','$servicetax','$freightname','$freightcost','$freighttax','$partd','$qt','$unitp','$validity','$delivery','$payment','$newFilePathq','$newFilePatha','$remarks','$addreq','$licencefor','$expectedclosure','$quotformname')");

            
            }
            elseif($i>=2)
            {
                if($i==$c)
                {
                    $productnew="Product".($c+1);
                                    $partdescnew="PartDescription".($c+1);
                                    $quantitynew="Quantity".($c+1);
                                    $unitpricenew="UnitPrice".($c+1);
                                    
                                    $hsnsacnew="HSNSAC".($c+1);
                                    $quotationpathnew="QuotationPath".($c+1);
                                    $attachmentnew="Attachment".($c+1);
                                    $licencefornew="LicenceFor".($c+1);
                                    mysqli_query($dbc,"ALTER TABLE `quotation_all` ADD `$productnew` TEXT NULL, ADD `$partdescnew` TEXT NULL,ADD `$quantitynew` TEXT NULL,ADD `$unitpricenew` TEXT NULL,ADD `$hsnsacnew` TEXT NULL,ADD `$quotationpathnew` TEXT NULL,ADD `$attachmentnew` TEXT NULL,ADD `$licencefor` TEXT NULL");
                                    $c=$c+1;
                }

                
                //Get the temp file path
                $tmpFilePath = $_FILES['uplfiles']['tmp_name'][$i];

                //Make sure we have a file path
                if ($tmpFilePath != ""){
                //Setup our new file path
                $newFilePath = "../../../../uploadfiles/quotation/".$quotuniqueid."-".$_FILES['uplfiles']['name'][$i];

                //Upload the file into the temp dir
                if(move_uploaded_file($tmpFilePath, $newFilePath)) {

                //Handle other code here
                    $newFilePathtwo = $quotuniqueid."-".$_FILES['uplfiles']['name'][$i];

                    }
                }
                $ii=$i+1;
                $productcolumn="Product".$ii;
                $partdescolumn="PartDescription".$ii;
                $quantitycolumn="Quantity".$ii;
                $unitpricecolumn="UnitPrice".$ii;
                $quotationpathcolumn="QuotationPath".$ii;
                $hsnsaccolumn="HSNSAC".$ii;
                $licenceforcolumn="LicenceFor".$ii;
                $ad=mysqli_query($dbc,"update `quotation_all` set `$productcolumn`='$product',`$partdescolumn`='$partd',`$quantitycolumn`='$qt',`$unitpricecolumn`='$unitp',`$quotationpathcolumn`='$newFilePathtwo',`$hsnsaccolumn`='$hsnsac',`$licenceforcolumn`='$licencefor' where  `RMail`='$rmail' and `UniqueId`='$quotuniqueid' ");
                
  
            }
            else
            {
                //echo '<script>alert("Sorry!! Maximum Description Reached");location.replace("../../../opr/quotgen.php");</script>';
            }

        }
        else
        {
            break;
        }

        
     }   
    
                              /*mysqli_query($dbc,"insert into `quotation_all` (`Representive`,`RMail`,`UniqueId`,`QuotNo`,`Funnelid`,`Company`,`Product1`,`Product2`,`Product3`,`Product4`,`Product5`,`Product6`,`Product7`,`Product8`,`Product9`,`Product10`,`Tax`,`Currency`,`HSNSAC`,`ServiceName`,`ServiceCost`,`ServiceTax`,`FreightName`,`FreightCost`,`FreightTax`,`PartDescription1`,`Quantity1`,`UnitPrice1`,`PartDescription2`,`Quantity2`,`UnitPrice2`,`PartDescription3`,`Quantity3`,`UnitPrice3`,`PartDescription4`,`Quantity4`,`UnitPrice4`,`PartDescription5`,`Quantity5`,`UnitPrice5`,`PartDescription6`,`Quantity6`,`UnitPrice6`,`PartDescription7`,`Quantity7`,`UnitPrice7`,`PartDescription8`,`Quantity8`,`UnitPrice8`,`PartDescription9`,`Quantity9`,`UnitPrice9`,`PartDescription10`,`Quantity10`,`UnitPrice10`,`Validity`,`Delivery`,`Payment`,`QuotationPath1`,`QuotationPath2`,`QuotationPath3`,`QuotationPath4`,`QuotationPath5`,`QuotationPath6`,`QuotationPath7`,`QuotationPath8`,`QuotationPath9`,`QuotationPath10`,`Attachment1`,`Remarks`,`AddressReq`,`QuotFormName`,`LicenceFor1`,`LicenceFor2`,`LicenceFor3`,`LicenceFor4`,`LicenceFor5`,`LicenceFor6`,`LicenceFor7`,`LicenceFor8`,`LicenceFor9`,`LicenceFor10`,`Product11`,`PartDescription11`,`Quantity11`,`UnitPrice11`,`HSNSAC11`,`LicenceFor11`,`QuotationPath11`,`Product12`,`PartDescription12`,`Quantity12`,`UnitPrice12`,`HSNSAC12`,`LicenceFor12`,`QuotationPath12`,`Product13`,`PartDescription13`,`Quantity13`,`UnitPrice13`,`HSNSAC13`,`LicenceFor13`,`QuotationPath13`,`Product14`,`PartDescription14`,`Quantity14`,`UnitPrice14`,`HSNSAC14`,`LicenceFor14`,`QuotationPath14`,`Product15`,`PartDescription15`,`Quantity15`,`UnitPrice15`,`HSNSAC15`,`LicenceFor15`,`QuotationPath15`,`Product16`,`PartDescription16`,`Quantity16`,`UnitPrice16`,`HSNSAC16`,`LicenceFor16`,`QuotationPath16`,`Product17`,`PartDescription17`,`Quantity17`,`UnitPrice17`,`HSNSAC17`,`LicenceFor17`,`QuotationPath17`,`Product18`,`PartDescription18`,`Quantity18`,`UnitPrice18`,`HSNSAC18`,`LicenceFor18`,`QuotationPath18`,`Product19`,`PartDescription19`,`Quantity19`,`UnitPrice19`,`HSNSAC19`,`LicenceFor19`,`QuotationPath19`,`Product20`,`PartDescription20`,`Quantity20`,`UnitPrice20`,`HSNSAC20`,`LicenceFor20`,`QuotationPath20`,`Product21`,`PartDescription21`,`Quantity21`,`UnitPrice21`,`HSNSAC21`,`LicenceFor21`,`QuotationPath21`,`Product22`,`PartDescription22`,`Quantity22`,`UnitPrice22`,`HSNSAC22`,`LicenceFor22`,`QuotationPath22`,`Product23`,`PartDescription23`,`Quantity23`,`UnitPrice23`,`HSNSAC23`,`LicenceFor23`,`QuotationPath23`,`Product24`,`PartDescription24`,`Quantity24`,`UnitPrice24`,`HSNSAC24`,`LicenceFor24`,`QuotationPath24`,`Product25`,`PartDescription25`,`Quantity25`,`UnitPrice25`,`HSNSAC25`,`LicenceFor25`,`QuotationPath25`,`Product26`,`PartDescription26`,`Quantity26`,`UnitPrice26`,`HSNSAC26`,`LicenceFor26`,`QuotationPath26`,`Product27`,`PartDescription27`,`Quantity27`,`UnitPrice27`,`HSNSAC27`,`LicenceFor27`,`QuotationPath27`,`Product28`,`PartDescription28`,`Quantity28`,`UnitPrice28`,`HSNSAC28`,`LicenceFor28`,`QuotationPath28`,`Product29`,`PartDescription29`,`Quantity29`,`UnitPrice29`,`HSNSAC29`,`LicenceFor29`,`QuotationPath29`,`Product30`,`PartDescription30`,`Quantity30`,`UnitPrice30`,`HSNSAC30`,`LicenceFor30`,`QuotationPath30`,`Product31`,`PartDescription31`,`Quantity31`,`UnitPrice31`,`HSNSAC31`,`LicenceFor31`,`QuotationPath31`,`Product32`,`PartDescription32`,`Quantity32`,`UnitPrice32`,`HSNSAC32`,`LicenceFor32`,`QuotationPath32`,`Product33`,`PartDescription33`,`Quantity33`,`UnitPrice33`,`HSNSAC33`,`LicenceFor33`,`QuotationPath33`,`Product34`,`PartDescription34`,`Quantity34`,`UnitPrice34`,`HSNSAC34`,`LicenceFor34`,`QuotationPath34`,`Product35`,`PartDescription35`,`Quantity35`,`UnitPrice35`,`HSNSAC35`,`LicenceFor35`,`QuotationPath35`,`Product36`,`PartDescription36`,`Quantity36`,`UnitPrice36`,`HSNSAC36`,`LicenceFor36`,`QuotationPath36`,`Product37`,`PartDescription37`,`Quantity37`,`UnitPrice37`,`HSNSAC37`,`LicenceFor37`,`QuotationPath37`,`Product38`,`PartDescription38`,`Quantity38`,`UnitPrice38`,`HSNSAC38`,`LicenceFor38`,`QuotationPath38`,`Product39`,`PartDescription39`,`Quantity39`,`UnitPrice39`,`HSNSAC39`,`LicenceFor39`,`QuotationPath39`,`Product40`,`PartDescription40`,`Quantity40`,`UnitPrice40`,`HSNSAC40`,`LicenceFor40`,`QuotationPath40`,`Product41`,`PartDescription41`,`Quantity41`,`UnitPrice41`,`HSNSAC41`,`LicenceFor41`,`QuotationPath41`,`Product42`,`PartDescription42`,`Quantity42`,`UnitPrice42`,`HSNSAC42`,`LicenceFor42`,`QuotationPath42`,`Product43`,`PartDescription43`,`Quantity43`,`UnitPrice43`,`HSNSAC43`,`LicenceFor43`,`QuotationPath43`,`Product44`,`PartDescription44`,`Quantity44`,`UnitPrice44`,`HSNSAC44`,`LicenceFor44`,`QuotationPath44`,`Product45`,`PartDescription45`,`Quantity45`,`UnitPrice45`,`HSNSAC45`,`LicenceFor45`,`QuotationPath45`,`Product46`,`PartDescription46`,`Quantity46`,`UnitPrice46`,`HSNSAC46`,`LicenceFor46`,`QuotationPath46`,`Product47`,`PartDescription47`,`Quantity47`,`UnitPrice47`,`HSNSAC47`,`LicenceFor47`,`QuotationPath47`,`Product48`,`PartDescription48`,`Quantity48`,`UnitPrice48`,`HSNSAC48`,`LicenceFor48`,`QuotationPath48`,`Product49`,`PartDescription49`,`Quantity49`,`UnitPrice49`,`HSNSAC49`,`LicenceFor49`,`QuotationPath49`,`Product50`,`PartDescription50`,`Quantity50`,`UnitPrice50`,`HSNSAC50`,`LicenceFor50`,`QuotationPath50`,`Product51`,`PartDescription51`,`Quantity51`,`UnitPrice51`,`HSNSAC51`,`LicenceFor51`,`QuotationPath51`,`Product52`,`PartDescription52`,`Quantity52`,`UnitPrice52`,`HSNSAC52`,`LicenceFor52`,`QuotationPath52`,`Product53`,`PartDescription53`,`Quantity53`,`UnitPrice53`,`HSNSAC53`,`LicenceFor53`,`QuotationPath53`,`Product54`,`PartDescription54`,`Quantity54`,`UnitPrice54`,`HSNSAC54`,`LicenceFor54`,`QuotationPath54`,`Product55`,`PartDescription55`,`Quantity55`,`UnitPrice55`,`HSNSAC55`,`LicenceFor55`,`QuotationPath55`,`Product56`,`PartDescription56`,`Quantity56`,`UnitPrice56`,`HSNSAC56`,`LicenceFor56`,`QuotationPath56`,`Product57`,`PartDescription57`,`Quantity57`,`UnitPrice57`,`HSNSAC57`,`LicenceFor57`,`QuotationPath57`,`Product58`,`PartDescription58`,`Quantity58`,`UnitPrice58`,`HSNSAC58`,`LicenceFor58`,`QuotationPath58`,`Product59`,`PartDescription59`,`Quantity59`,`UnitPrice59`,`HSNSAC59`,`LicenceFor59`,`QuotationPath59`,`Product60`,`PartDescription60`,`Quantity60`,`UnitPrice60`,`HSNSAC60`,`LicenceFor60`,`QuotationPath60`) values ('$rep','$rmail','$quotuniqueid','$quotno','$fid','$company','$product1','$product2','$product3','$product4','$product5','$product6','$product7','$product8','$product9','$product10','$tax','$currency','$hsnsac','$servicename','$servicecost','$servicetax','$freightname','$freightcost','$freighttax','$pdescription1','$quantity1','$unitprice1','$pdescription2','$quantity2','$unitprice2','$pdescription3','$quantity3','$unitprice3','$pdescription4','$quantity4','$unitprice4','$pdescription5','$quantity5','$unitprice5','$pdescription6','$quantity6','$unitprice6','$pdescription7','$quantity7','$unitprice7','$pdescription8','$quantity8','$unitprice8','$pdescription9','$quantity9','$unitprice9','$pdescription10','$quantity10','$unitprice10','$validity','$delivery','$payment','$quotpath1','$quotpath2','$quotpath3','$quotpath4','$quotpath5','$quotpath6','$quotpath7','$quotpath8','$quotpath9','$quotpath10','$otherfile','$remarks','$addressreq','$quotformname','$licence1','$licence2','$licence3','$licence4','$licence5','$licence6','$licence7','$licence8','$licence9','$licence10','$product11','$partdesc11','$quantity11','$unitprice11','$hsnsac11','$licence11','$quotpath11','$product12','$partdesc12','$quantity12','$unitprice12','$hsnsac12','$licence12','$quotpath12','$product13','$partdesc13','$quantity13','$unitprice13','$hsnsac13','$licence13','$quotpath13','$product14','$partdesc14','$quantity14','$unitprice14','$hsnsac14','$licence14','$quotpath14','$product15','$partdesc15','$quantity15','$unitprice15','$hsnsac15','$licence15','$quotpath15','$product16','$partdesc16','$quantity16','$unitprice16','$hsnsac16','$licence16','$quotpath16','$product17','$partdesc17','$quantity17','$unitprice17','$hsnsac17','$licence17','$quotpath17','$product18','$partdesc18','$quantity18','$unitprice18','$hsnsac18','$licence18','$quotpath18','$product19','$partdesc19','$quantity19','$unitprice19','$hsnsac19','$licence19','$quotpath19','$product20','$partdesc20','$quantity20','$unitprice20','$hsnsac20','$licence20','$quotpath20','$product21','$partdesc21','$quantity21','$unitprice21','$hsnsac21','$licence21','$quotpath21','$product22','$partdesc22','$quantity22','$unitprice22','$hsnsac22','$licence22','$quotpath22','$product23','$partdesc23','$quantity23','$unitprice23','$hsnsac23','$licence23','$quotpath23','$product24','$partdesc24','$quantity24','$unitprice24','$hsnsac24','$licence24','$quotpath24','$product25','$partdesc25','$quantity25','$unitprice25','$hsnsac25','$licence25','$quotpath25','$product26','$partdesc26','$quantity26','$unitprice26','$hsnsac26','$licence26','$quotpath26','$product27','$partdesc27','$quantity27','$unitprice27','$hsnsac27','$licence27','$quotpath27','$product28','$partdesc28','$quantity28','$unitprice28','$hsnsac28','$licence28','$quotpath28','$product29','$partdesc29','$quantity29','$unitprice29','$hsnsac29','$licence29','$quotpath29','$product30','$partdesc30','$quantity30','$unitprice30','$hsnsac30','$licence30','$quotpath30','$product31','$partdesc31','$quantity31','$unitprice31','$hsnsac31','$licence31','$quotpath31','$product32','$partdesc32','$quantity32','$unitprice32','$hsnsac32','$licence32','$quotpath32','$product33','$partdesc33','$quantity33','$unitprice33','$hsnsac33','$licence33','$quotpath33','$product34','$partdesc34','$quantity34','$unitprice34','$hsnsac34','$licence34','$quotpath34','$product35','$partdesc35','$quantity35','$unitprice35','$hsnsac35','$licence35','$quotpath35','$product36','$partdesc36','$quantity36','$unitprice36','$hsnsac36','$licence36','$quotpath36','$product37','$partdesc37','$quantity37','$unitprice37','$hsnsac37','$licence37','$quotpath37','$product38','$partdesc38','$quantity38','$unitprice38','$hsnsac38','$licence38','$quotpath38','$product39','$partdesc39','$quantity39','$unitprice39','$hsnsac39','$licence39','$quotpath39','$product40','$partdesc40','$quantity40','$unitprice40','$hsnsac40','$licence40','$quotpath40','$product41','$partdesc41','$quantity41','$unitprice41','$hsnsac41','$licence41','$quotpath41','$product42','$partdesc42','$quantity42','$unitprice42','$hsnsac42','$licence42','$quotpath42','$product43','$partdesc43','$quantity43','$unitprice43','$hsnsac43','$licence43','$quotpath43','$product44','$partdesc44','$quantity44','$unitprice44','$hsnsac44','$licence44','$quotpath44','$product45','$partdesc45','$quantity45','$unitprice45','$hsnsac45','$licence45','$quotpath45','$product46','$partdesc46','$quantity46','$unitprice46','$hsnsac46','$licence46','$quotpath46','$product47','$partdesc47','$quantity47','$unitprice47','$hsnsac47','$licence47','$quotpath47','$product48','$partdesc48','$quantity48','$unitprice48','$hsnsac48','$licence48','$quotpath48','$product49','$partdesc49','$quantity49','$unitprice49','$hsnsac49','$licence49','$quotpath49','$product50','$partdesc50','$quantity50','$unitprice50','$hsnsac50','$licence50','$quotpath50','$product51','$partdesc51','$quantity51','$unitprice51','$hsnsac51','$licence51','$quotpath51','$product52','$partdesc52','$quantity52','$unitprice52','$hsnsac52','$licence52','$quotpath52','$product53','$partdesc53','$quantity53','$unitprice53','$hsnsac53','$licence53','$quotpath53','$product54','$partdesc54','$quantity54','$unitprice54','$hsnsac54','$licence54','$quotpath54','$product55','$partdesc55','$quantity55','$unitprice55','$hsnsac55','$licence55','$quotpath55','$product56','$partdesc56','$quantity56','$unitprice56','$hsnsac56','$licence56','$quotpath56','$product57','$partdesc57','$quantity57','$unitprice57','$hsnsac57','$licence57','$quotpath57','$product58','$partdesc58','$quantity58','$unitprice58','$hsnsac58','$licence58','$quotpath58','$product59','$partdesc59','$quantity59','$unitprice59','$hsnsac59','$licence59','$quotpath59','$product60','$partdesc60','$quantity60','$unitprice60','$hsnsac60','$licence60','$quotpath60')");*/

                           }

                           

                           $quotupdatefetch=mysqli_query($dbc,"select * from `quotation_all` where `QuotNo`='$quotno' ");
                           $qtype=0;
                           while($qar=mysqli_fetch_assoc($quotupdatefetch))
                           {
                              $qtype=$qtype+1;
                           }
                           

                           ?>
                           <div class="row">
                                    <div class="form-group  col-md-10" style="font-weight: bold;">
                                       <?php echo "Date: ".$date; ?>
                              <br><?php if($qtype > 1){$qtype=$qtype-1; echo "Ref No: ".$quotno." R".$qtype; }else{ echo "Ref No: ".$quotno; } ?>
                                       
                                    </div>
                                    <div class="form-group col-md-1">
                                       <img src="../../assets/images/logo.png">
                                    </div>
                                    
                                 </div>
                                 <div class="row">
                           <div class="col-md-5 "></div>
                           <div class="col-md-3"><font style="text-align: center; font-weight: bold;font-size: 24px">QUOTATION</font></div>
                           <div class="col-md-4 "></div>
                        </div>
                        <?php
                        if($addressreq=="Yes")
  {
    $loc=$address;
  }
  else
  {
    $loc=$city;
  }
  ?>
                        <div class="row" style="font-size: 16px" >
                           <div class="form-group col-md-12">To, <br> <?php echo $company;  ?> <br><?php echo $loc;  ?> </div>
                        </div>
                        <div class="row" style="font-size: 16px">
                           <div class="col-md-12">Dear Sir/Madam, <br> We are pleased to send our best quote for the following products enquired. </div>
                        </div>
                        <br>
                        <div class="row" style="font-size: 16px">
                           <div class="col-md-12 table-responsive">
                              <table class="table table-bordered table-striped mb-none" >
                                 <thead>
                                    <tr>
                                       <th>Sl No.</th>
                                       <th>Product</th>
                                       <th>Description</th>
                                       <th>HSN/SAC</th>
                                       <th>Qty</th>
                                       <th>Unit Price(<?php echo $cur; ?>)</th>
                                       <th>Sub Total(<?php echo $cur; ?>)</th>
                                       <th colspan="2" align="center">GST Rate</th>
                                       <th colspan="2" align="center">GST Amount</th>
                                       <th>Total Price(<?php echo $cur; ?>)</th>
                                    </tr>
                                 </thead>
                                 <tbody>
                                    <?php 
                                       $total1=0;$total2=0;$total3=0;$total4=0;$total5=0;$total6=0;$total7=0;$total8=0;$total9=0;$total10=0;$igsttotal=0;$sgsttotal=0;$cgsttotal=0;$grandtotal=0;$subtotal=0;
                                       $grandtotal1=0;$grandtotal2=0;$grandtotal3=0;$grandtotal4=0;$grandtotal5=0;$grandtotal6=0;$grandtotal7=0;$grandtotal8=0;$grandtotal9=0;$grandtotal10=0;
                                       $w=60;$h=20;
                                       
                                       $chk=0;

                                       

                                       if($quotformname == "multipletax")
 {
  for($k=1;$k<=60;$k++)
                                       {
                                          
                                             $m=$k+1;
                                         
                                          
                                          for($l=1;$l<=$k;$l++)
                                          {
                                             $pdsc="pdescription".$l;
                                             $qt="quantity".$l;
                                             $up="unitprice".$l;
                                             $prod="product".$l;
                                             $hsnsac="hsnsac".$l;
                                             $licence="licence".$l;
                                          } 

                                          if(!empty($$prod))
                                             {
                                       
                                       
                                       $partdesc1=$$pdsc;
                                       $qt1=$$qt;
                                       $unitp1=sprintf("%.2f",$$up);
                                       $total1=sprintf("%.2f",$qt1*$unitp1);
                                       
                                       echo '<tr><td>'.$k.'</td><td>'.$$prod.'</td><td>'.$partdesc1.'</td><td>'.$$hsnsac.'</td><td>'.$qt1.'</td><td>'.$unitp1.'</td><td>'.$total1.'</td>';

                                       

                                       $total=sprintf("%.2f",$total+$total1);
                                       
                                       
                                       $array =  explode(',', $tax);
                                       $count=0;
                                       $grandtotal1=0;
                                       $subtotal1=0;
                                       $cgstshow=0;
                                       foreach ($array as $item) {
                                       global $count;
                                       $count=$count+1;
                                       }
                                       
                                       foreach ($array as $item) {
                                       
                                       
                                       $taxperarr =  explode('-', $item);


                                       
                                       foreach ($taxperarr as $perr) {
                                        $persep=explode('@', $perr);
                                        
                                          $taxpercentage=$persep[0];
                                        
                                          }

                                          

                                          foreach ($taxperarr as $perr) {
                                            
                                        $persep=explode('@', $perr);
                                        
                                          $showtax=$persep[1];

                                           
                                          if($l==$showtax)
                                          {

                                          
                                       
                                       $totalwtax1=sprintf("%.2f",($taxpercentage/100)*$total1);

                                       
                                       if($count>1 and $taxperarr[0]=="SGST" or $taxperarr[0]=="CGST")
                                       {
                                       
                                       
                                          echo '<td>'.$taxperarr[0].' '.$taxpercentage.'%'.'</td>';
                                          $cgstshow=1;
                                       
                                       }
                                       else
                                       {
                                          echo '<td colspan="2">'.$taxperarr[0].' '.$taxpercentage.'%'.'</td>';
                                                                              
                                       }
                                       
                                       
                                       $grandtotal1=$grandtotal1+$totalwtax1;

  
                                       
                                     
                                   }
                                     
                                       }
                                      
                                       
                                       
                                     }
                                       
                                       if($count>1 and $cgstshow==1)
                                       {
                                       
                                       echo '<td>'.$totalwtax1.'</td>';
                                       echo '<td>'.$totalwtax1.'</td>';
                                       
                                       
                                       }
                                       else
                                       {
                                       
                                       echo '<td colspan="2">'.$totalwtax1.'</td>';
                                       
                                       
                                       }

                                       $grandtotal=$grandtotal+$grandtotal1;
                                       
                                       
                                       $subtotal1=sprintf("%.2f",$grandtotal1+$total1);
                                       
                                       echo '<td align="center">'.$subtotal1.'</td></tr>';

                                          $subtotal=$subtotal+$subtotal1;
                                       
                                       }
                                       else
                                       {
                                          global $chk;
                                          $chk=$chk+1;
                                       }

                                       
                                          if ($chk>0) {
                                          break;
                                       }

                                       }

 }
 elseif($quotformname=="licence")
 {

 }
 else
 {


                                       
                                       for($k=1;$k<=60;$k++)
                                       {
                                          
                                             $m=$k+1;
                                         
                                          
                                          for($l=1;$l<=$k;$l++)
                                          {
                                             $pdsc="pdescription".$l;
                                             $qt="quantity".$l;
                                             $up="unitprice".$l;
                                             $prod="product".$l;
                                             $hsnsac="hsnsac1";
                                             $licence="licence".$l;
                                          } 

                                          if(!empty($$prod))
                                             {
                                       
                                       
                                       $partdesc1=$$pdsc;
                                       $qt1=$$qt;
                                       $unitp1=sprintf("%.2f",$$up);
                                       $total1=sprintf("%.2f",$qt1*$unitp1);
                                       
                                       

                                       echo '<tr><td>'.$k.'</td><td>'.$$prod.'</td><td>'.$partdesc1.'</td><td>'.$$hsnsac.'</td><td>'.$qt1.'</td><td>'.$unitp1.'</td><td>'.$total1.'</td>';

                                       $total=sprintf("%.2f",$total+$total1);
                                       
                                       
                                       $array =  explode(',', $tax);
                                       $count=0;
                                       $grandtotal1=0;
                                       $subtotal1=0;
                                       foreach ($array as $item) {
                                       global $count;
                                       $count=$count+1;
                                       }
                                       
                                       foreach ($array as $item) {
                                       
                                       
                                       $taxperarr =  explode('-', $item);
                                       
                                       foreach ($taxperarr as $per) {
                                       $taxpercentage=$per;
                                       
                                       }
                                       
                                       $totalwtax1=sprintf("%.2f",($taxpercentage/100)*$total1);
                                       
                                       if($count>1)
                                       {
                                       
                                       
                                        echo '<td>'.$taxperarr[0].' '.$taxpercentage.'%'.'</td>';
                                       
                                       }
                                       else
                                       {
                                       echo '<td colspan="2">'.$taxperarr[0].' '.$taxpercentage.'%'.'</td>';
                                                                              
                                       }
                                       
                                       
                                       $grandtotal1=$grandtotal1+$totalwtax1;
                                       
                                       
                                       }
                                       
                                       if($count>1)
                                       {
                                       
                                       echo '<td>'.$totalwtax1.'</td>';
                                       echo '<td>'.$totalwtax1.'</td>';
                                       
                                       
                                       }
                                       else
                                       {
                                       
                                       echo '<td colspan="2">'.$totalwtax1.'</td>';
                                       
                                       
                                       }

                                       $grandtotal=$grandtotal+$grandtotal1;
                                       
                                       
                                       $subtotal1=sprintf("%.2f",$grandtotal1+$total1);
                                       
                                       echo '<td align="center">'.$subtotal1.'</td></tr>';

                                          $subtotal=$subtotal+$subtotal1;
                                       
                                       }
                                       else
                                       {
                                          global $chk;
                                          $chk=$chk+1;
                                       }

                                       
                                          if ($chk>0) {
                                          break;
                                       }

                                       }




}






                                       
                                       
                                       
                                       
                                       
                                       $stax=0;$serviceamount=0;
                                       if(!empty($servicename) and !empty($servicecost) and !empty($servicetax))
                                       {
                                       $stax=($servicetax/100)*$servicecost;
                                       $serviceamount=$servicecost+$stax;
                                       echo '<tr><td></td><td></td><td>Service</td><td colspan="3">'.$servicename.'</td><td>'.$servicecost.'</td><td colspan="2">'.$servicetax.'%</td><td colspan="2" align="center">'.$stax.'</td><td  align="center">'.$serviceamount.'</td></tr>';
                                       }
                                       
                                       $ftax=0;$freightamount=0;
                                       if(!empty($freightname) and !empty($freightcost) and !empty($freighttax))
                                       {
                                       $ftax=($freighttax/100)*$freightcost;
                                       $freightamount=$freightcost+$ftax;
                                       echo '<tr><td></td><td></td><td>Freight</td><td colspan="3">'.$freightname.'</td><td>'.$freightcost.'</td><td colspan="2">'.$freighttax.'%</td><td colspan="2" align="center">'.$ftax.'</td><td  align="center">'.$freightamount.'</td></tr>';
                                       }
                                       
                                       
                                       $grandtax=sprintf("%.2f",$grandtotal+$stax+$ftax);
                                       $grandsub=sprintf("%.2f",$subtotal+$serviceamount+$freightamount);
                                       
                                      
                                       
                                       echo '<tr><td></td><td></td><td></td><td colspan="3"></td><td></td><td colspan="2">Grand Total</td><td colspan="2" align="center">'.$grandtax.'</td><td  align="center">'.$grandsub.'</td></tr>';
                                       
                                       mysqli_query($dbc,"update `quotation` set `SubTotal`='$subtotal',`GrandTotal`='$grandsub' where `Funnelid`='$fid' and `QuotNo`='$quotno' and `Company`='$company'  ");
                                       
                                       for($l=1;$l<=10;$l++)
                                       {
                                       if(!empty($l))
                                       {
                                       $product="product".$l;
                                       $prd[]=$$product;
                                       }
                                       
                                       }
                                       
                                       
                                       $requirements=implode(',',$prd);
                                       
                                       mysqli_query($dbc,"insert into `funnel` (`Representive`,`RMail`,`CampaignType`,`Company`,`Sector`,`Products`,`Probability`,`Revenue`,`Stage`,`Services`) values ('$rep','$rmail','Direct','$company','$sector','$requirements','50','$grandsub','Quotation','$service')");
                                       
                                       
                                       
                                       
                                       
                                       
                                       ?>
                                    
                                 </tbody>
                              </table>
                           </div>
                        </div>
                        <br>
                        <div class="row" >
                           <div class="col-md-12" style="text-align: left;font-weight: bold;" >NOTE: <?php echo $remarks; ?></div>
                        </div>
                        <br>

                        <div class="row" style="font-size: 16px">
                           <div class="col-md-12" style="font-weight: bold;">Terms & Conditions:</div>
                           <?php $j=1; ?>
                           
                           <div class="col-md-12" ><?php echo $j.'. Taxes & Duties: All Inclusive.' ?></div>
                           
                           <div class="col-md-12" ><?php $j=$j+1;
                              echo $j.'. Payment Terms: '.$payment.' from the date of invoice submission.' ?></div>
                           
                           <div class="col-md-12" ><?php $j=$j+1;
                              echo $j.'. Order Cancellation: Orders once placed cannot be cancelled under any circumstances.' ?></div>
                           
                           <div class="col-md-12" ><?php $j=$j+1;
                              echo $j.'. Purchase Order: PO to be placed in the name of '.$companyname.', '.$companycity.'.' ?></div>
                           
                           <div class="col-md-12" ><?php $j=$j+1; $maxdelivery=$delivery+1;
                              echo $j.'. Delivery:  Within '.$delivery.'-'.$maxdelivery.' days from the date of receipt of PO.' ?></div>
                           
                           <div class="col-md-12" ><?php $j=$j+1;
                              echo $j.'. Quote Validity: This quote is valid for '.$validity.' days only.' ?></div>
                              
                            <div class="col-md-12" ><?php $j=$j+1;
                              echo $j.'. If installation required extra charges will be applicable.' ?></div>
                           
                        </div> 
                        <br>
                        <div class="row" style="font-size: 16px">
                           <div class="form-group col-md-12" >Please do not hesitate to contact me in case of any clarifications.</div>
                        </div>
                        <div class="row" style="font-size: 16px">
                           <div class="col-md-12" >Thank you for giving us opportunity to serve you. Looking forward to your valuable order.</div>
                        </div>
                        <br>

                        <div class="row" style="font-size: 16px">
                           
                           <div class="col-md-12" >From <?php echo $companyname; ?></div>
                           <div class="col-md-12" ><?php echo $rep; ?></div>
                           <?php $proffet=mysqli_query($dbc,"select * from team where email='$id' ");
                              while($pr=mysqli_fetch_assoc($proffet))
                              {
                                  $mobno=$pr['contact'];
                              }
                              ?>
                           
                           <div class="col-md-12" >Ph: <?php echo $mobno; ?></div>
                        </div>
                        
                        <br>
                        <div class="row" style="font-size: 16px">
                           
                           <div class="col-md-12" style="text-align: center;" ><?php echo $companyaddress; ?></div>
                           
                           <div class="col-md-12" style="text-align: center;">Ph: +91 <?php echo $companycontactno; ?>, Email: <?php echo $rmail; ?>, URL: <?php echo $companywebsite; ?></div>
                        </div>
                        <br>
                        <div class="row" style="font-size: 16px">
                           
                           
                           <div class="col-md-12" style="text-align: center;">
                              <?php echo '<div class="dropdown">
                                 ';
                                 for($i=1;$i<=100;$i++)
                                 {
                                     $qp="quotpath".$i;
                                     if(!empty($$qp))
                                     {
                                        echo '<a href="../../uploadfiles/quotation/'.$$qp.'" target="_blank" class="btn btn-warning">R'.$i.' </a> ';
                                     }
                                 }
                                 
                                         
                                    echo ' </div>';
                                 
                                     ?>
                              <?php
                              if(!empty($otherfile))
                              {
                                ?>
                                <a href="../../uploadfiles/quotation/<?php echo urldecode($otherfile); ?>" target="_blank" class="btn btn-warning">Other File</a>
                                <?php
                              }
                                ?>
                                <a href="quotupdate.php?q=<?php echo $quotno; ?>&qtype=<?php echo $qtype; ?>"  class="btn btn-danger">Edit</a> <a href="TCPDF/crm/quotation.php?q=<?php echo $quotno; ?>" target="_blank" class="btn btn-primary">View & Send PDF</a>
                           </div>
                           
                        </div>

                              <!-- end quotverify content -->
                           </div>
                        </div>
                        <!-- End Example Basic Form -->
                     </div>
                  </div>
               </div>
            </div>

      </div>
    </div>
    <!-- End Page -->


    <!-- Footer -->
    <?php include "includes/footer.php"; ?>
    <!-- Core  -->
    <script src="../../assets/global/vendor/babel-external-helpers/babel-external-helpers.js"></script>
    <script src="../../assets/global/vendor/jquery/jquery.js"></script>
    <script src="../../assets/global/vendor/popper-js/umd/popper.min.js"></script>
    <script src="../../assets/global/vendor/bootstrap/bootstrap.js"></script>
    <script src="../../assets/global/vendor/animsition/animsition.js"></script>
    <script src="../../assets/global/vendor/mousewheel/jquery.mousewheel.js"></script>
    <script src="../../assets/global/vendor/asscrollbar/jquery-asScrollbar.js"></script>
    <script src="../../assets/global/vendor/asscrollable/jquery-asScrollable.js"></script>
    <script src="../../assets/global/vendor/waves/waves.js"></script>
    
    <!-- Plugins -->
    <script src="../../assets/global/vendor/jquery-mmenu/jquery.mmenu.min.all.js"></script>
    <script src="../../assets/global/vendor/switchery/switchery.js"></script>
    <script src="../../assets/global/vendor/intro-js/intro.js"></script>
    <script src="../../assets/global/vendor/screenfull/screenfull.js"></script>
    <script src="../../assets/global/vendor/slidepanel/jquery-slidePanel.js"></script>
        <script src="../../assets/global/vendor/chartist/chartist.min.js"></script>
        <script src="../../assets/global/vendor/chartist-plugin-tooltip/chartist-plugin-tooltip.js"></script>
        <script src="../../assets/global/vendor/jvectormap/jquery-jvectormap.min.js"></script>
        <script src="../../assets/global/vendor/jvectormap/maps/jquery-jvectormap-world-mill-en.js"></script>
        <script src="../../assets/global/vendor/matchheight/jquery.matchHeight-min.js"></script>
        <script src="../../assets/global/vendor/peity/jquery.peity.min.js"></script>
    
    <!-- Scripts -->
    <script src="../../assets/global/js/Component.js"></script>
    <script src="../../assets/global/js/Plugin.js"></script>
    <script src="../../assets/global/js/Base.js"></script>
    <script src="../../assets/global/js/Config.js"></script>
    
    <script src="section/Menubar.js"></script>
    <script src="section/Sidebar.js"></script>
    <script src="section/PageAside.js"></script>
    <script src="section/GridMenu.js"></script>
    
    <!-- Config -->
    <script src="../../assets/global/js/config/colors.js"></script>
    <script src="../../assets/js/config/tour.js"></script>
    <script>Config.set('assets', '../assets');</script>
    
    <!-- Page -->
    <script src="../../assets/js/Site.js"></script>
    <script src="../../assets/global/js/Plugin/asscrollable.js"></script>
    <script src="../../assets/global/js/Plugin/slidepanel.js"></script>
    <script src="../../assets/global/js/Plugin/switchery.js"></script>
        <script src="../../assets/global/js/Plugin/matchheight.js"></script>
        <script src="../../assets/global/js/Plugin/jvectormap.js"></script>
        <script src="../../assets/global/js/Plugin/peity.js"></script>
    
        <script src="../../assets/examples/js/dashboard/v1.js"></script>

        


    
  <?php include "../../assets/twak/twak.php"; ?>
  </body>
</html>

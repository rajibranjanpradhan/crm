<?php
   include "session_handler.php";
   ?>
<!DOCTYPE html>
<html class="no-js css-menubar" lang="en">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
      <meta name="description" content="bootstrap material admin template">
      <meta name="author" content="">
      <title>Edit Target | Bizapp CRM</title>
      <link rel="apple-touch-icon" href="../../assets/images/apple-touch-icon.png">
      <link rel="shortcut icon" href="../../assets/images/favicon.ico">
      <!-- Stylesheets -->
      <link rel="stylesheet" href="../../assets/global/css/bootstrap.min.css">
      <link rel="stylesheet" href="../../assets/global/css/bootstrap-extend.min.css">
      <link rel="stylesheet" href="../../assets/css/site.min.css">
      <!-- Plugins -->
      <link rel="stylesheet" href="../../assets/global/vendor/animsition/animsition.css">
      <link rel="stylesheet" href="../../assets/global/vendor/asscrollable/asScrollable.css">
      <link rel="stylesheet" href="../../assets/global/vendor/switchery/switchery.css">
      <link rel="stylesheet" href="../../assets/global/vendor/intro-js/introjs.css">
      <link rel="stylesheet" href="../../assets/global/vendor/slidepanel/slidePanel.css">
      <link rel="stylesheet" href="../../assets/global/vendor/jquery-mmenu/jquery-mmenu.css">
      <link rel="stylesheet" href="../../assets/global/vendor/flag-icon-css/flag-icon.css">
      <link rel="stylesheet" href="../../assets/global/vendor/waves/waves.css">
      <link rel="stylesheet" href="../../assets/global/vendor/chartist/chartist.css">
      <link rel="stylesheet" href="../../assets/global/vendor/jvectormap/jquery-jvectormap.css">
      <link rel="stylesheet" href="../../assets/global/vendor/chartist-plugin-tooltip/chartist-plugin-tooltip.css">
      <link rel="stylesheet" href="../../assets/examples/css/dashboard/v1.css">
      <!-- Fonts -->
      <link rel="stylesheet" href="../../assets/global/fonts/material-design/material-design.min.css">
      <link rel="stylesheet" href="../../assets/global/fonts/brand-icons/brand-icons.min.css">
      <link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Roboto:300,400,500,300italic'>
      <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
      <!-- table set -->
      <link rel='stylesheet' href='../../assets/css/customised-crm.css'>
      <script src="../../assets/js/customised-crm.js"></script>

      <?php include "includes/css/datepicker.php"; ?>
      <?php include "includes/css/clockpicker.php"; ?>
      

      

      <!--[if lt IE 9]>
      <script src="../../assets/global/vendor/html5shiv/html5shiv.min.js"></script>
      <![endif]-->
      <!--[if lt IE 10]>
      <script src="../../assets/global/vendor/media-match/media.match.min.js"></script>
      <script src="../../assets/global/vendor/respond/respond.min.js"></script>
      <![endif]-->
      <!-- Scripts -->
      <script src="../../assets/global/vendor/breakpoints/breakpoints.js"></script>
      <script>
         Breakpoints();
      </script>
   </head>
   <body class="animsition site-navbar-small dashboard">
      <!--[if lt IE 8]>
      <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
      <![endif]-->
      <?php include "navbar-header.php"; ?>
      <?php include "side-navigation.php"; ?>   
      <!-- Page -->
      <div class="page">
         <div class="page-content" >
            <!-- Panel Basic -->
            
            <!-- input customer details  start -->
            <div class="panel">
               <div class="panel-body nav-tabs-animate nav-tabs-horizontal" data-plugin="tabs">
                  <h4 class="example-title">Update Target</h4>
                  
                  <!--  basic start -->
                  <div class="tab-content">
                     <div class="tab-pane active animation-slide-left" id="basic" role="tabpanel">
                        <div class="example">
                           <?php
                      //$q=$_GET['v'];
                    

                      $c=$_GET['u'];
                      $d=$_GET['v'];
                      if($d=='resourcesubmit'){
                      $query=mysqli_query($dbc,"select * from `target_manage_resource` where `EmployeeMail`='$c' AND MONTH(ModificationDetail) = MONTH(CURRENT_DATE()) AND DATE(ModificationDetail) BETWEEN '$fydate' AND '$lydate' ");
                        }
                      if($d=='productsubmit'){
                      $query=mysqli_query($dbc,"select * from `target_manage_product` where `ProductName`='$c' AND MONTH(ModificationDetail) = MONTH(CURRENT_DATE()) AND DATE(ModificationDetail) BETWEEN '$fydate' AND '$lydate' ");
                        }
                      if($d=='customersubmit'){
                      $query=mysqli_query($dbc,"select * from `target_manage_customer` where `CustomerName`='$c' AND MONTH(ModificationDetail) = MONTH(CURRENT_DATE()) AND DATE(ModificationDetail) BETWEEN '$fydate' AND '$lydate' ");
                        }
                                            while($frow=mysqli_fetch_assoc($query))
                                            {
                                                    $employeemail=$frow['EmployeeMail'];
                                                    $productname=$frow['ProductName'];
                                                    $customername=$frow['CustomerName'];
                                                    $trevenue=$frow['RevenuePerMonth'];
                                                    $tmargin=$frow['MarginPerMonth'];
                                                    $tmeeting=$frow['MeetingPerMonth'];
                                                    $tfunnel=$frow['FunnelPerMonth'];
                                                    $tcustomer=$frow['CustomersPerMonth'];
                                                    $tsalescert=$frow['SalesCertificationPerMonth'];
                                                    $ttechcert=$frow['TechnicalCertificationPerMonth'];
                                                    $tdemo=$frow['DemoPerMonth'];
                                                    $tpoc=$frow['POCPerMonth'];
                                                    
                                               
                                            }       

                                        ?>

                           <form action="../auth/target/update.php" method="post"  >
                              
                              <div class="row">
                              <div class="form-group  col-md-6">
                                    <label class="form-control-label" >
                                      <?php if($d=='resourcesubmit'){ echo "Employee Mail";}
                                      if($d=='productsubmit'){ echo "Product Name"; }
                                      if($d=='customersubmit'){ echo "Customer Name";}
                                      ?>                                      
                                      </label>
                            <input type="text" class="form-control" value="<?php echo $c; ?>"  disabled><input type="hidden" class="form-control" name="formname" value="<?php echo $c; ?>" >
                                </div>
                              <div class="col-xl-6 form-group">
                                <label class="form-control-label">Revenue</label>
                            <input type="number" class="form-control" name="revenuepermonth" value="<?php echo $trevenue; ?>"  required>
                              </div>
                              <div class="col-xl-6 form-group">
                                <label class="form-control-label">Margin</label>
                            <input type="number" class="form-control" name="marginpermonth" value="<?php echo $tmargin; ?>" required>
                              </div>
                              <div class="col-xl-6 form-group">
                                <label class="form-control-label">Meetings</label>
                            <input type="number" class="form-control" name="meetingpermonth" value="<?php echo $tmeeting; ?>" required>
                              </div>
                              <div class="col-xl-6 form-group">
                                <label class="form-control-label">Funnel</label>
                            <input type="number" class="form-control" name="funnelpermonth" value="<?php echo $tfunnel; ?>" required>
                              </div>
                              <div class="col-xl-6 form-group">
                                <label class="form-control-label">Customers</label>
                            <input type="number" class="form-control" name="customerspermonth" value="<?php echo $tcustomer; ?>" required>
                              </div>
                              <div class="col-xl-6 form-group">
                                <label class="form-control-label">Sales Certification</label>
                            <input type="number" class="form-control" name="salescertification"  value="<?php echo $tsalescert; ?>" required>
                              </div>
                              <div class="col-xl-6 form-group">
                                <label class="form-control-label">Demo</label>
                          <input type="number" class="form-control" name="demo" value="<?php echo $tdemo ?>" required>
                              </div>
                              <div class="col-xl-6 form-group">
                                <label class="form-control-label">Technical Certification</label>
                            <input type="number" class="form-control" name="technicalcertification" value="<?php echo $ttechcert ?>" required>
                              </div>
                              <div class="col-xl-6 form-group">
                                <label class="form-control-label">POC</label>
                            <input type="number" class="form-control" name="poc" value="<?php echo $tpoc ?>" required>
                              </div>
                              <div class="col-xl-6 form-group">
                                <label class="form-control-label"><b style="color:red">*</b>Every Input must be for Month</label>
                            
                              </div>
                              <div class="col-md-12 float-right">
                                <input type="hidden" name="submittype" value="<?php  
                                if($d=='resourcesubmit'){ echo "resourcesubmit";}
                                if($d=='productsubmit'){ echo "productsubmit"; }
                                if($d=='customersubmit'){ echo "customersubmit"; }
                                ?>" >
                    <button class="btn btn-primary"  type="submit" name="submit">Submit</button>
                       <button class="btn btn-default" data-dismiss="modal" type="reset" >Close</button>
                              </div>
                            </div>
                           </form>
                        </div>
                     </div>
                     
                     <!--  basic end -->
                     
                        </div>
                     </div>
                  </div>
                  <!-- input customer details end -->
                <!-- End Panel Basic -->
         </div>
      </div>
      <!-- End Page -->
      <!-- Footer -->
      <?php include "includes/footer.php"; ?>
      <!-- Core  -->
      <script src="../../assets/global/vendor/babel-external-helpers/babel-external-helpers.js"></script>
      <script src="../../assets/global/vendor/jquery/jquery.js"></script>
      <script src="../../assets/global/vendor/popper-js/umd/popper.min.js"></script>
      <script src="../../assets/global/vendor/bootstrap/bootstrap.js"></script>
      <script src="../../assets/global/vendor/animsition/animsition.js"></script>
      <script src="../../assets/global/vendor/mousewheel/jquery.mousewheel.js"></script>
      <script src="../../assets/global/vendor/asscrollbar/jquery-asScrollbar.js"></script>
      <script src="../../assets/global/vendor/asscrollable/jquery-asScrollable.js"></script>
      <script src="../../assets/global/vendor/waves/waves.js"></script>
      <!-- Plugins -->
      <script src="../../assets/global/vendor/jquery-mmenu/jquery.mmenu.min.all.js"></script>
      <script src="../../assets/global/vendor/switchery/switchery.js"></script>
      <script src="../../assets/global/vendor/intro-js/intro.js"></script>
      <script src="../../assets/global/vendor/screenfull/screenfull.js"></script>
      <script src="../../assets/global/vendor/slidepanel/jquery-slidePanel.js"></script>
      <script src="../../assets/global/vendor/chartist/chartist.min.js"></script>
      <script src="../../assets/global/vendor/chartist-plugin-tooltip/chartist-plugin-tooltip.js"></script>
      <script src="../../assets/global/vendor/jvectormap/jquery-jvectormap.min.js"></script>
      <script src="../../assets/global/vendor/jvectormap/maps/jquery-jvectormap-world-mill-en.js"></script>
      <script src="../../assets/global/vendor/matchheight/jquery.matchHeight-min.js"></script>
      <script src="../../assets/global/vendor/peity/jquery.peity.min.js"></script>
      <!-- Scripts -->
      <script src="../../assets/global/js/Component.js"></script>
      <script src="../../assets/global/js/Plugin.js"></script>
      <script src="../../assets/global/js/Base.js"></script>
      <script src="../../assets/global/js/Config.js"></script>
      <script src="section/Menubar.js"></script>
      <script src="section/Sidebar.js"></script>
      <script src="section/PageAside.js"></script>
      <script src="section/GridMenu.js"></script>
      <!-- Config -->
      <script src="../../assets/global/js/config/colors.js"></script>
      <script src="../../assets/js/config/tour.js"></script>
      <script>Config.set('assets', '../assets');</script>
      <!-- Page -->
      <script src="../../assets/js/Site.js"></script>
      <script src="../../assets/global/js/Plugin/asscrollable.js"></script>
      <script src="../../assets/global/js/Plugin/slidepanel.js"></script>
      <script src="../../assets/global/js/Plugin/switchery.js"></script>
      <script src="../../assets/global/js/Plugin/matchheight.js"></script>
      <script src="../../assets/global/js/Plugin/jvectormap.js"></script>
      <script src="../../assets/global/js/Plugin/peity.js"></script>
      <script src="../../assets/examples/js/dashboard/v1.js"></script>
      <?php include "includes/css/select.php"; ?>
      <?php include "includes/js/select.php"; ?>
      <?php include "includes/js/dynamicaddition.php"; ?>
      <?php include "includes/js/datepicker.php"; ?>
      <?php include "includes/js/clockpicker.php"; ?>

      

      

   <?php include "../../assets/twak/twak.php"; ?>
  </body>
</html>
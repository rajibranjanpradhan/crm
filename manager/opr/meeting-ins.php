<?php
   include "session_handler.php";
   ?>
<!DOCTYPE html>
<html class="no-js css-menubar" lang="en">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
      <meta name="description" content="bootstrap material admin template">
      <meta name="author" content="">
      <title>Add Prospects | Bizapp CRM</title>
      <link rel="apple-touch-icon" href="../../assets/images/apple-touch-icon.png">
      <link rel="shortcut icon" href="../../assets/images/favicon.ico">
      <!-- Stylesheets -->
      <link rel="stylesheet" href="../../assets/global/css/bootstrap.min.css">
      <link rel="stylesheet" href="../../assets/global/css/bootstrap-extend.min.css">
      <link rel="stylesheet" href="../../assets/css/site.min.css">
      <!-- Plugins -->
      <link rel="stylesheet" href="../../assets/global/vendor/animsition/animsition.css">
      <link rel="stylesheet" href="../../assets/global/vendor/asscrollable/asScrollable.css">
      <link rel="stylesheet" href="../../assets/global/vendor/switchery/switchery.css">
      <link rel="stylesheet" href="../../assets/global/vendor/intro-js/introjs.css">
      <link rel="stylesheet" href="../../assets/global/vendor/slidepanel/slidePanel.css">
      <link rel="stylesheet" href="../../assets/global/vendor/jquery-mmenu/jquery-mmenu.css">
      <link rel="stylesheet" href="../../assets/global/vendor/flag-icon-css/flag-icon.css">
      <link rel="stylesheet" href="../../assets/global/vendor/waves/waves.css">
      <link rel="stylesheet" href="../../assets/global/vendor/chartist/chartist.css">
      <link rel="stylesheet" href="../../assets/global/vendor/jvectormap/jquery-jvectormap.css">
      <link rel="stylesheet" href="../../assets/global/vendor/chartist-plugin-tooltip/chartist-plugin-tooltip.css">
      <link rel="stylesheet" href="../../assets/examples/css/dashboard/v1.css">
      <!-- Fonts -->
      <link rel="stylesheet" href="../../assets/global/fonts/material-design/material-design.min.css">
      <link rel="stylesheet" href="../../assets/global/fonts/brand-icons/brand-icons.min.css">
      <link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Roboto:300,400,500,300italic'>
      <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
      <!-- table set -->
      <link rel='stylesheet' href='../../assets/css/customised-crm.css'>
      <script src="../../assets/js/customised-crm.js"></script>

      <?php include "includes/css/datepicker.php"; ?>
      <?php include "includes/css/clockpicker.php"; ?>
      

      

      <!--[if lt IE 9]>
      <script src="../../assets/global/vendor/html5shiv/html5shiv.min.js"></script>
      <![endif]-->
      <!--[if lt IE 10]>
      <script src="../../assets/global/vendor/media-match/media.match.min.js"></script>
      <script src="../../assets/global/vendor/respond/respond.min.js"></script>
      <![endif]-->
      <!-- Scripts -->
      <script src="../../assets/global/vendor/breakpoints/breakpoints.js"></script>
      <script>
         Breakpoints();
      </script>
   </head>
   <body class="animsition site-navbar-small dashboard">
      <!--[if lt IE 8]>
      <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
      <![endif]-->
      <?php include "navbar-header.php"; ?>
      <?php include "side-navigation.php"; ?>   
      <!-- Page -->
      <div class="page">
         <div class="page-content" >
            <!-- Panel Basic -->
            
            <!-- input customer details  start -->
            <div class="panel">
               <div class="panel-body nav-tabs-animate nav-tabs-horizontal" data-plugin="tabs">
                  <h4 class="example-title">Update Meeting Details</h4>
                  
                  <!--  basic start -->
                  <div class="tab-content">
                     <div class="tab-pane active animation-slide-left" id="basic" role="tabpanel">
                        <div class="example">
                           <?php
                                            //$q=$_GET['v'];

                                            
                                        
                                            $c="";
                                            $c=$_GET['c'];
                                            
                                            $query=mysqli_query($dbc,"select * from `customers` where  `Company`='$c'  ");
                                            while($row=mysqli_fetch_array($query))
                                            {
                                                $projectname=$row['ProjectName'];
                                                $company=$row['Company'];
                                                $sector=$row['Sector'];
                                                $campaigntype=$row['CampaignType'];
                                                $product=$row['Product'];
                                                $date=$row['Date'];
                                                $contactperson=$row['ContactName1'];
                                                $designation=$row['ContactDesignation1'];
                                                $email=$row['ContactMail1'];
                                                $mobile=$row['ContactMobile1'];
                                                $response=$row['Response'];
                                                $followup=$row['FollowUp'];
                                                $requirements=$row['Requirement'];
                                                $remarks=$row['Remarks'];
                                                $infra=$row['Infrastructure'];
                                               
                                            }    

                                        ?>

                           <form action="../auth/meeting/ins.php" method="post"  enctype="multipart/form-data" autocomplete="off">
                              
                              <div class="row">
                                 <div class="form-group  col-md-4">
                                    <label class="form-control-label" >Company Name</label>
                                    <select class="form-control" name="company"  data-plugin="select2" onchange="redirect(this.value);" required="required" >
                                    <?php
                           $project=mysqli_query($dbc,"select distinct(Company) from `customers` order by id desc");
                           if(!empty($company))
                           {
                            echo '<option selected  value="'.$company.'" >'.$company.'</option>';
                            
                           }
                           else
                           {
                            echo '<option  value="" >Select</option>';
                          }
                           while($row=mysqli_fetch_assoc($project))
                           {
                               //$pro=$row['ProjectName'];
                           
                                echo '<option  value="'.$row['Company'].'" >'.$row['Company'].'</option>';
                               
                           }
                           ?>
                                    </select>
                                 </div>
                                 <div class="form-group  col-md-4">
                                    <label class="form-control-label" >Sector</label>
                                    <select class="form-control" name="sector"  data-plugin="select2" required="required" >
                                    <?php
                                       
                                       $project=mysqli_query($dbc,"select distinct(SectorName) from `sector`  ");
                                       if($sector=="")
                           {
                            echo '<option  value="" >Select</option>';
                           }
                           else
                           {
                            echo '<option selected  value="'.$sector.'" >'.$sector.'</option>';
                          }
                                       while($row=mysqli_fetch_assoc($project))
                                       {
                                         echo '<option  value="'.$row['SectorName'].'">'.$row['SectorName'].'</option>';
                                                                                 
                                       }
                                       ?>
                                    </select>
                                 </div>
                                 <div class="form-group  col-md-4">
                                    <label class="form-control-label" >Contact Person</label>
                                    <input type="text" class="form-control" name="contactperson" value="<?php echo $contactperson; ?>" id="w4-f" onKeyUp="fname()" placeholder="Contact Person"   >
                                 </div>
                              </div>
                              <div class="row">
                                 
                                 <div class="form-group  col-md-4">
                                    <label class="form-control-label" >Designation</label>
                                    <input type="text" class="form-control" name="designation" value="<?php echo $designation; ?>" placeholder="Designation"  required="required" >
                                 </div>
                                 <div class="form-group  col-md-4">
                                          <label class="form-control-label" >Email</label>
                                          <input type="email" class="form-control" name="email" value="<?php echo $email; ?>" placeholder="Email" required="required" >
                                       </div>
                                       <div class="form-group  col-md-4">
                                          <label class="form-control-label" >Mobile No</label>
                                          <input type="text" class="form-control" name="mobile" value="<?php echo $mobile; ?>" id="w4-mob" placeholder="Mobile No" onKeyUp="mob()" maxlength="12">
                                       </div>
                              </div>
                              
                              <div class="row">
                                       <div class="form-group  col-md-4">
                                          <label class="form-control-label" >Campaign Type</label>
                                          <select class="form-control" name="campaigntype"  data-plugin="select2" required="required" >
                                    <?php
                                                                    $camp=mysqli_query($dbc,"select * from `campaignlist`");
                                                                    if($ctype=="")
                           {
                            echo '<option  value="" >Select</option>';
                           }
                           else
                           {
                            echo '<option selected  value="'.$ctype.'" >'.$ctype.'</option>';
                          }
                                                                    while($row=mysqli_fetch_assoc($camp))
                                                                    {
                                                                        //$pro=$row['ProjectName'];
                                                        
                                                                         echo '<option  value="'.$row['CampaignName'].'">'.$row['CampaignName'].'</option>';
                                                                        
                                                                    }
                                                                ?>
                                    </select>
                                       </div>
                                       <div class="form-group  col-md-4">
                                          <label class="form-control-label" >Project Name</label>
                                          <select class="form-control" name="project"  data-plugin="select2"  >
                                    <?php
                                                                    $proj=mysqli_query($dbc,"select * from `campaign-projects`");
                                                                    if($pro=="")
                           {
                            echo '<option  value="" >Select</option>';
                           }
                           else
                           {
                            echo '<option selected  value="'.$pro.'" >'.$pro.'</option>';
                          }
                                                                    while($row=mysqli_fetch_assoc($proj))
                                                                    {
                                                                        //$pro=$row['ProjectName'];
                                                        
                                                                         echo '<option  value="'.$row['ProjectName'].'">'.$row['ProjectName'].'</option>';
                                                                        
                                                                    }
                                                                ?>
                                    </select>
                                       </div>

                                       <div class="form-group  col-md-4">
                                          <label class="form-control-label" >Meeting Date</label>
                                          <input type="text" name="meetingdate" data-plugin="datepicker" class="form-control "  placeholder="Meeting Date"  >
                                       </div>
                                    </div>

                                    <div class="row">
                                       
                                       <div class="form-group  col-md-4">
                                          <label class="form-control-label" >Meeting Time</label>
                                          <input type="text" name="meetingtime" class="timepicker form-control" data-plugin="clockpicker" placeholder="Meeting Time" data-autoclose="true">
                                       </div>
                                       <div class="form-group  col-md-4">
                                          <label class="form-control-label" >Response</label>
                                          <select class="form-control" name="response"  data-plugin="select2"  >
                                             <option  value="">Select</option>
                                             <option  value="Positive">Positive</option>
                                             <option value="Negative">Negative</option>

                                          </select>
                                       </div>
                                       <div class="form-group  col-md-4">
                                          <label class="form-control-label" >Follow Up</label>
                                          <select class="form-control" name="followup"  data-plugin="select2"  >
                                             <option  value="">Select</option>
                                             <option  value="Yes" >Yes</option>
                                             <option value="No">No</option>

                                          </select>
                                       </div>
                                    </div>

                                   
                                    <div class="row">
                                       <div class="form-group  col-md-4">
                                          <label class="form-control-label" >FollowUp Date</label>
                                          <input type="text" name="followupdate" data-plugin="datepicker" class="form-control "  placeholder="FollowUp Date"  >
                                       </div>
                                       <div class="form-group  col-md-4">
                                          <label class="form-control-label" >FollowUp Time</label>
                                          <input type="text" name="followuptime" class="timepicker form-control" data-plugin="clockpicker" data-autoclose="true" placeholder="FollowUp Time">
                                       </div>
                                       <div class="form-group  col-md-4">
                                          <label class="form-control-label" >Meeting</label>
                                          <select class="form-control" name="meetingstatus"  data-plugin="select2"  >
                                             <option  value="">Select</option>
                                             <option  value="Yes" >Yes</option>
                                             <option value="No">No</option>

                                          </select>
                                       </div>
                                    </div>
                                    
                                    <div class="row">
                                       <div class="form-group  col-md-6">
                                          <label class="form-control-label" >Customer Requirements</label>
                                          <textarea class="form-control" name="requirements" rows="2" placeholder="Input Customer Requirements"></textarea>
                                       </div>
                                       <div class="form-group  col-md-6">
                                          <label class="form-control-label" >Customer Remarks</label>
                                          <textarea class="form-control" name="remarks" rows="2" placeholder="Input Customer Remarks"></textarea>
                                       </div>
                                       
                                    </div>
                                    <input type="hidden" name="frompage" value="meetingins">
                                   
                              
                                    
                                 
                              <div class="form-group ">
                                 <button type="submit" name="submit" class="btn btn-primary">Submit</button>
                                 <button type="reset"  class="btn btn-default">Reset</button>
                              </div>
                           </form>
                        </div>
                     </div>
                     <script type="text/javascript">
                              function redirect(x)
                              {
                                  var opted=x;
                                  location.replace("meeting-ins.php?c="+opted);
                              
                              }
                              
                           </script>
                     <!--  basic end -->
                     
                        </div>
                     </div>
                  </div>
                  <!-- input customer details end -->
                <!-- End Panel Basic -->
         </div>
      </div>
      <!-- End Page -->
      <!-- Footer -->
      <?php include "includes/footer.php"; ?>
      <!-- Core  -->
      <script src="../../assets/global/vendor/babel-external-helpers/babel-external-helpers.js"></script>
      <script src="../../assets/global/vendor/jquery/jquery.js"></script>
      <script src="../../assets/global/vendor/popper-js/umd/popper.min.js"></script>
      <script src="../../assets/global/vendor/bootstrap/bootstrap.js"></script>
      <script src="../../assets/global/vendor/animsition/animsition.js"></script>
      <script src="../../assets/global/vendor/mousewheel/jquery.mousewheel.js"></script>
      <script src="../../assets/global/vendor/asscrollbar/jquery-asScrollbar.js"></script>
      <script src="../../assets/global/vendor/asscrollable/jquery-asScrollable.js"></script>
      <script src="../../assets/global/vendor/waves/waves.js"></script>
      <!-- Plugins -->
      <script src="../../assets/global/vendor/jquery-mmenu/jquery.mmenu.min.all.js"></script>
      <script src="../../assets/global/vendor/switchery/switchery.js"></script>
      <script src="../../assets/global/vendor/intro-js/intro.js"></script>
      <script src="../../assets/global/vendor/screenfull/screenfull.js"></script>
      <script src="../../assets/global/vendor/slidepanel/jquery-slidePanel.js"></script>
      <script src="../../assets/global/vendor/chartist/chartist.min.js"></script>
      <script src="../../assets/global/vendor/chartist-plugin-tooltip/chartist-plugin-tooltip.js"></script>
      <script src="../../assets/global/vendor/jvectormap/jquery-jvectormap.min.js"></script>
      <script src="../../assets/global/vendor/jvectormap/maps/jquery-jvectormap-world-mill-en.js"></script>
      <script src="../../assets/global/vendor/matchheight/jquery.matchHeight-min.js"></script>
      <script src="../../assets/global/vendor/peity/jquery.peity.min.js"></script>
      <!-- Scripts -->
      <script src="../../assets/global/js/Component.js"></script>
      <script src="../../assets/global/js/Plugin.js"></script>
      <script src="../../assets/global/js/Base.js"></script>
      <script src="../../assets/global/js/Config.js"></script>
      <script src="section/Menubar.js"></script>
      <script src="section/Sidebar.js"></script>
      <script src="section/PageAside.js"></script>
      <script src="section/GridMenu.js"></script>
      <!-- Config -->
      <script src="../../assets/global/js/config/colors.js"></script>
      <script src="../../assets/js/config/tour.js"></script>
      <script>Config.set('assets', '../assets');</script>
      <!-- Page -->
      <script src="../../assets/js/Site.js"></script>
      <script src="../../assets/global/js/Plugin/asscrollable.js"></script>
      <script src="../../assets/global/js/Plugin/slidepanel.js"></script>
      <script src="../../assets/global/js/Plugin/switchery.js"></script>
      <script src="../../assets/global/js/Plugin/matchheight.js"></script>
      <script src="../../assets/global/js/Plugin/jvectormap.js"></script>
      <script src="../../assets/global/js/Plugin/peity.js"></script>
      <script src="../../assets/examples/js/dashboard/v1.js"></script>
      <?php include "includes/css/select.php"; ?>
      <?php include "includes/js/select.php"; ?>
      <?php include "includes/js/dynamicaddition.php"; ?>
      <?php include "includes/js/datepicker.php"; ?>
      <?php include "includes/js/clockpicker.php"; ?>

      

      

   <?php include "../../assets/twak/twak.php"; ?>
  </body>
</html>
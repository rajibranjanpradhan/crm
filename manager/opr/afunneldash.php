<?php
   include "session_handler.php";
?>
<?php
  
   $uid=null;
   $c=null;
   $uid=$_GET['u'];
   $v=$_GET['v'];
   $vfd=$_GET['vfd'];
   $c=$_GET['c'];
   
   
   ?>
<!DOCTYPE html>
<html class="no-js css-menubar" lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="description" content="bootstrap material admin template">
    <meta name="author" content="">
    
    <title>Dashboard | Bizapp CRM</title>
    
    <link rel="apple-touch-icon" href="../../assets/images/apple-touch-icon.png">
    <link rel="shortcut icon" href="../../assets/images/favicon.ico">
    

    <!-- Stylesheets -->
    <link rel="stylesheet" href="../../assets/global/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../assets/global/css/bootstrap-extend.min.css">
    <link rel="stylesheet" href="../../assets/css/site.min.css">
    
    <!-- Plugins -->
    <link rel="stylesheet" href="../../assets/global/vendor/animsition/animsition.css">
    <link rel="stylesheet" href="../../assets/global/vendor/asscrollable/asScrollable.css">
    <link rel="stylesheet" href="../../assets/global/vendor/switchery/switchery.css">
    <link rel="stylesheet" href="../../assets/global/vendor/intro-js/introjs.css">
    <link rel="stylesheet" href="../../assets/global/vendor/slidepanel/slidePanel.css">
    <link rel="stylesheet" href="../../assets/global/vendor/jquery-mmenu/jquery-mmenu.css">
    <link rel="stylesheet" href="../../assets/global/vendor/flag-icon-css/flag-icon.css">
    <link rel="stylesheet" href="../../assets/global/vendor/waves/waves.css">
        <link rel="stylesheet" href="../../assets/global/vendor/chartist/chartist.css">
        <link rel="stylesheet" href="../../assets/global/vendor/jvectormap/jquery-jvectormap.css">
        <link rel="stylesheet" href="../../assets/global/vendor/chartist-plugin-tooltip/chartist-plugin-tooltip.css">
        <link rel="stylesheet" href="../../assets/examples/css/dashboard/v1.css">
     <link rel="stylesheet" href="../../assets/global/vendor/multi-select/multi-select.css">
      <link rel="stylesheet" href="../../assets/global/vendor/bootstrap-select/bootstrap-select.css">
           <link rel="stylesheet" href="../../assets/global/vendor/select2/select2.css">
    
    <!-- Fonts -->
    <link rel="stylesheet" href="../../assets/global/fonts/material-design/material-design.min.css">
    <link rel="stylesheet" href="../../assets/global/fonts/brand-icons/brand-icons.min.css">
    <link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Roboto:300,400,500,300italic'>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

      <!-- table set -->
    <link rel='stylesheet' href='../../assets/css/datatableset.css'>
    <link rel='stylesheet' href='https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css'>

      <link rel="stylesheet" href="../../assets/css/customised-crm.css">

        <link rel="stylesheet" href="../../assets/global/vendor/bootstrap-datepicker/bootstrap-datepicker.css">
    
    <!--[if lt IE 9]>
    <script src="../../assets/global/vendor/html5shiv/html5shiv.min.js"></script>
    <![endif]-->
    
    <!--[if lt IE 10]>
    <script src="../../assets/global/vendor/media-match/media.match.min.js"></script>
    <script src="../../assets/global/vendor/respond/respond.min.js"></script>
    <![endif]-->
    
    <!-- Scripts -->
    <script src="../../assets/global/vendor/breakpoints/breakpoints.js"></script>
    <script>
      Breakpoints();
    </script>
          <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
  </head>
  <body class="animsition site-navbar-small dashboard">
    <!--[if lt IE 8]>
        <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->

    <?php include "navbar-header.php"; ?>
    <?php include "side-navigation.php"; ?>   
    

    <!-- Page -->
    <div class="page">
      <div class="page-content container-fluid">
        <div class="row" data-plugin="matchHeight" data-by-row="true">
         
         <div class="col-xl-12 col-md-12">
            <!-- Widget Linearea One-->
            <div class="card card-shadow" id="widgetLineareaOne">
              <div class="card-block p-5 pt-5">
                <div class="clearfix">
                  <div class="grey-800 float-left py-10">
                  <i class="icon md-account grey-600 font-size-20 vertical-align mr-2" style="text-align: right;"></i> 

                  <?php 
                                    if($uid == null)
                                    {
                                        echo "<b>Profile:</b> &nbsp;Company";
                                    }
                                    else
                                    {
                                    $fetname=mysqli_query($dbc,"select * from team where email='$uid' ");
                                    while($rwnm=mysqli_fetch_assoc($fetname))
                                    {
                                        $name=$rwnm['name'];
                                    }
                                    echo "<b>Profile:</b> ".$name; 
                                    }?>         
                  </div>
  
                </div>
               
                
              </div>
            </div>
            <!-- End Widget Linearea One -->
          </div>

          <div class="col-xl-3 col-md-6" data-target="#resourcewisemodal" data-toggle="modal">
            <!-- Widget Linearea One-->
            <div class="card card-shadow" id="widgetLineareaOne">
              <div class="card-block p-10 pt-10">
                <div class="clearfix">
                  <div class="grey-800 float-left py-10">
                    <i class="icon md-account grey-600 font-size-20 vertical-align-bottom mr-5"></i>                    Resource Wise Review
                  </div>
  
                </div>
               
                
              </div>
            </div>
            <!-- End Widget Linearea One -->
          </div>

          <!-- Modal -->
                    <div class="modal fade modal-3d-flip-horizontal" id="resourcewisemodal"
                      aria-hidden="true" aria-labelledby="exampleModalTitle" role="dialog"
                      tabindex="-1">
                      <div class="modal-dialog modal-simple">
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">×</span>
                            </button>
                            <h4 class="modal-title">Resource Wise Review</h4>
                          </div>
                          <div class="modal-body">
                            <form action="" method="get" novalidate="novalidate" autocomplete="off" >
                              <div>
                             <label class="form-control-label" >Department:</label>
                                    <select class="form-control" id="Rankd" name="Rank"  data-plugin="select2" required="required" >
                                     <option value="">Select Department</option>
                                              <option value="sales">Sales</option>
                                              <option value="technical">Technical</option>
                                    </select>
                                    </div>
                                      <div class="containersd">
                                             <div class="sales">
                                                <label class="form-control-label" >Employee:</label>
                                                <div>
                                                   <select name="project"   data-plugin="select2" class="form-control" onchange="redirect(this.value);" required >
                                                   <?php
                                                  $project=mysqli_query($dbc,"select * from `team` where `Department`='Sales'");
                                                  echo '<option >Select</option>';
                                                  while($row=mysqli_fetch_assoc($project))
                                                  {
                                                 //$pro=$row['ProjectName'];
                                             
                                                  echo '<option  value="'.$row['email'].'">'.$row['name'].'</option>';
                                                 
                                                 }
                                                 ?>
                                                </select>
                                                </div>
                                             </div>
                                             <div class="technical">
                                                <label class="form-control-label" >Employee:</label>
                                                <div>
                                                   <select class="form-control" data-plugin="select2"  name="project" id="ms_example6" onchange="redirect(this.value);" required >
                                                                                                    <?php
                                                    $project=mysqli_query($dbc,"select * from `team` where `Department`='Technical'");
                                                    echo '<option >Select</option>';
                                                  while($row=mysqli_fetch_assoc($project))
                                                  {
                                                    //$pro=$row['ProjectName'];
                                             
                                                  echo '<option  value="'.$row['email'].'">'.$row['name'].'</option>';
                                                 
                                                  }
                                                  ?>
                                                  </select>
                                                </div>
                                             </div>
                                          </div>
                            </form>
                          </div>
                          <div class="modal-footer">
                            <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                          </div>
                        </div>
                      </div>
                    </div>
                    <!-- End Modal -->


          <div class="col-xl-3 col-md-6" data-target="#periodwisemodal" data-toggle="modal">
            <!-- Widget Linearea Two -->
            <div class="card card-shadow" id="widgetLineareaTwo">
              <div class="card-block p-10 pt-10">
                <div class="clearfix">
                  <div class="grey-800 float-left py-10">
                    <i class="icon md-flash grey-600 font-size-20 vertical-align-bottom mr-5"></i>                    Period Wise Review
                  </div>
                 
                </div>
              </div>
            </div>
            <!-- End Widget Linearea Two -->
          </div>

          <!-- Modal -->
                    <div class="modal fade modal-3d-flip-horizontal" id="periodwisemodal"
                      aria-hidden="true" aria-labelledby="exampleModalTitle" role="dialog"
                      tabindex="-1">
                      <div class="modal-dialog modal-simple">
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">×</span>
                            </button>
                            <h4 class="modal-title">Period Wise Review</h4>
                          </div>
                          <div class="modal-body">
                             <form action="" method="get" class="form-horizontal" novalidate="novalidate"  autocomplete="off">
                              <div class="form-group">
                                  <label class="form-control-label" >Review Period:</label>
                                 <div>
                                    <select size="1"  data-plugin="select2" class="form-control Ranka populate" title="" name="Rank">
                                       <option value="Annual">Select</option>
                                       <option value="ryear">Yearly</option>
                                       <option value="rhalfyear">Half Yearly</option>
                                       <option value="rquater">Quarterly</option>
                                       <option value="amonth">Monthly</option>
                                       <!-- <option value="rweek">Weekly</option> -->
                                       <option value="adate">Date Range</option>
                                    </select>
                                 </div>
                                 <div class="containers">
                                    <div class="ryear">
                                       <label class="form-control-label">Year:</label>
                                       <div>
                                          <select data-plugin="select2" class="form-control populate" name="project"  id="ms_example6" onchange="redirectmn(this.value);" required >
                                             <option value="">Select</option>
                                             <!-- <option value="2019">2019</option> -->
                                             <?php
                                                $curyear=date('Y');
                                                
                                                ?>
                                             <option value="<?php echo $curyear; ?>"><?php echo $curyear; ?>-<?php echo $curyear+1; ?></option>
                                             <option value="<?php echo $curyear-1; ?>"><?php echo $curyear-1; ?>-<?php echo $curyear; ?></option>
                                             <option value="<?php echo $curyear-2; ?>"><?php echo $curyear-2; ?>-<?php echo $curyear-1; ?></option>
                                             <option value="<?php echo $curyear-3; ?>"><?php echo $curyear-3; ?>-<?php echo $curyear-2; ?></option>
                                             <option value="<?php echo $curyear-4; ?>"><?php echo $curyear-4; ?>-<?php echo $curyear-3; ?></option>
                                             <!-- <option value="11">Nov</option>
                                                <option value="12">Dec</option> -->
                                          </select>
                                       </div>
                                    </div>
                                    <div class="rhalfyear">
                                        <label class="form-control-label">Half Year:</label>
                                       <div>
                                          <select data-plugin="select2" class="form-control populate" name="project" id="ms_example6" onchange="redirectmn(this.value);" required >
                                             <option value="">Select</option>
                                             <!-- <option value="2019">2019</option> -->
                                             <option value="1sty">1st</option>
                                             <option value="2ndy">2nd</option>
                                             <!-- <option value="11">Nov</option>
                                                <option value="12">Dec</option> -->
                                          </select>
                                       </div>
                                    </div>
                                    <div class="rquater">
                                      
                                        <label class="form-control-label">Quarter:</label>
                                       <div>
                                          <select data-plugin="select2" class="form-control populate" name="project" id="ms_example6" onchange="redirectmn(this.value);" required >
                                             <option value="">Select</option>
                                             <!-- <option value="2019">2019</option> -->
                                             <option value="1stq">1st</option>
                                             <option value="2ndq">2nd</option>
                                             <option value="3rdq">3rd</option>
                                             <option value="4thq">4th</option>
                                             <!-- <option value="11">Nov</option>
                                                <option value="12">Dec</option> -->
                                          </select>
                                       </div>
                                    </div>
                                    <div class="rweek">
                                        <label class="form-control-label">Week:</label>
                                       <div>
                                          <select name="project"  data-plugin="select2" class="form-control populate" id="ms_example6" onchange="redirectmn(this.value);" required >
                                             <option value="">Select</option>
                                             <!-- <option value="2019">2019</option> -->
                                             <?php
                                                $val=1;
                                                while($val<53)
                                                {
                                                   echo '<option value="'.$val.'">'.$val.'</option>'; 
                                                   $val=$val+1;
                                                }
                                                ?>
                                          </select>
                                       </div>
                                    </div>
                                    <div class="amonth">
                                        <label class="form-control-label">Month:</label>
                                       <div>
                                          <select name="project"  data-plugin="select2" class="form-control populate" id="ms_example6" onchange="redirectmn(this.value);" required >
                                             <option value="">Select</option>
                                             <option value="04">Apr</option>
                                             <option value="05">May</option>
                                             <option value="06">Jun</option>
                                             <option value="07">Jul</option>
                                             <option value="08">Aug</option>
                                             <option value="09">Sep</option>
                                             <option value="10">Oct</option>
                                             <option value="11">Nov</option>
                                             <option value="12">Dec</option>
                                             <option value="01">Jan</option>
                                             <option value="02">Feb</option>
                                             <option value="03">Mar</option>
                                          </select>
                                       </div>
                                    </div>
                                    <!-- </div>
                                       <div class="container"> -->
                                    <div class="adate">
                                       <label class="form-control-label">Date:</label>
                                       <div>
                                          <input type="text" name="fdate" data-plugin="datepicker" class="form-control" onchange="redirectfd(this.value);" placeholder="From Date" required >
                                          <br>
                                          <input type="text" name="tdate" data-plugin="datepicker" class="form-control" onchange="redirectat(this.value);" placeholder="To Date" required >
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </form>
                          </div>
                          <div class="modal-footer">
                             <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                          </div>
                        </div>
                      </div>
                    </div>
                    <!-- End Modal -->


          <div class="col-xl-3 col-md-6" data-target="#productwisemodal" data-toggle="modal">
            <!-- Widget Linearea Three -->
            <div class="card card-shadow" id="widgetLineareaThree">
              <div class="card-block p-10 pt-10">
                <div class="clearfix">
                  <div class="grey-800 float-left py-10">
                    <i class="icon md-chart grey-600 font-size-20 vertical-align-bottom mr-5"></i>                    Product Wise Review
                  </div>
                </div>
                
              </div>
            </div>
            <!-- End Widget Linearea Three -->
          </div>

           <!-- Modal -->
                    <div class="modal fade modal-3d-flip-horizontal" id="productwisemodal"
                      aria-hidden="true" aria-labelledby="exampleModalTitle" role="dialog"
                      tabindex="-1">
                      <div class="modal-dialog modal-simple">
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">×</span>
                            </button>
                            <h4 class="modal-title">Product Wise Review</h4>
                          </div>
                          <div class="modal-body">
                            <form action="" method="get" class="form-horizontal" novalidate="novalidate" autocomplete="off" >
                                       <div class="form-group">
                                           <label class="form-control-label">Review For:</label>
                                          <div>
                                             <select size="1" id="Rankarf" data-plugin="select2" class="form-control populate" title="" name="Rank">
                                                <option value="">Select</option>
                                                <option value="rproduct">Product</option>
                                                <!-- <option value="ram">Account Manager</option> -->
                                                <!--  <option value="rregion">Region</option>
                                                   <option value="rsbu">SBU</option> -->
                                                <!-- <option value="rsbu">SBU</option> -->
                                                <!-- <option value="week">Weekly</option>
                                                   <option value="adate">Date Range</option> -->
                                             </select>
                                          </div>
                                          <div class="containersrf">
                                             <div class="rproduct">
                                                <label class="form-control-label">Product:</label>
                                                <div>
                                                   <select name="project"  data-plugin="select2" class="form-control populate" id="ms_example6" onchange="redirectmn(this.value);" required >
                                                      <option value="">Select</option>
                                                      <?php
                                                         $resfet=mysqli_query($dbc,"select distinct(ProductName) from products");
                                                         while($valrw=mysqli_fetch_assoc($resfet))
                                                         {
                                                             $pnam=$valrw['ProductName'];
                                                            echo '<option value="'.$pnam.'">'.$pnam.'</option>'; 
                                                            
                                                         }
                                                         ?>
                                                   </select>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </form>
                          </div>
                          <div class="modal-footer">
                             <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                          </div>
                        </div>
                      </div>
                    </div>
                    <!-- End Modal -->


          <div class="col-xl-3 col-md-6" data-target="#customerwisemodal" data-toggle="modal">
            <!-- Widget Linearea Four -->
            <div class="card card-shadow" id="widgetLineareaFour">
              <div class="card-block p-10 pt-10">
                <div class="clearfix">
                  <div class="grey-800 float-left py-10">
                    <i class="icon md-view-list grey-600 font-size-20 vertical-align-bottom mr-5"></i>                    Customer Wise Review
                  </div>
                  
                </div>
                
              </div>
            </div>
            <!-- End Widget Linearea Four -->
          </div>


          <!-- Modal -->
                    <div class="modal fade modal-3d-flip-horizontal" id="customerwisemodal"
                      aria-hidden="true" aria-labelledby="exampleModalTitle" role="dialog"
                      tabindex="-1">
                      <div class="modal-dialog modal-simple">
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">×</span>
                            </button>
                            <h4 class="modal-title">Customer Wise Review</h4>
                          </div>
                          <div class="modal-body">
                           
                             <form action="" method="get" class="form-horizontal" novalidate="novalidate" autocomplete="off" >
                                       <div class="form-group">
                                          <!-- <label class="col-sm-2">&nbsp;</label>
                                          <label class="col-sm-2 control-label"  for="w4-username"><b>Review For:</b></label>
                                          <div class="col-sm-8" style="margin-left: 30px" >
                                             <select size="1" id="Rankcust" class="form-control" data-plugin-multiselect data-plugin-options='{ "enableCaseInsensitiveFiltering": true }' title="" name="Rank">
                                                <option value="">Select</option>
                                                <option value="rcustomer">Customers</option>
                                                
                                             </select>
                                          </div> -->
                                          <div class="containercust">
                                             <div class="rcustomer">
                                                <label class="form control-label">Customer Name:</label>
                                                <div>
                                                   <select data-plugin="select2" class="form-control populate" name="project" id="ms_example6" onchange="redirectcusts(this.value);" required >
                                                      <option value="">Select</option>
                                                      <?php
                                                         $resfet=mysqli_query($dbc,"select distinct(Company) from funnel where Stage='Won'");
                                                         while($valrw=mysqli_fetch_assoc($resfet))
                                                         {
                                                             $cname=$valrw['Company'];
                                                            echo '<option value="'.$cname.'">'.$cname.'</option>'; 
                                                            
                                                         }
                                                         ?>
                                                   </select>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </form>
                          </div>
                          <div class="modal-footer">
                             <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                          </div>
                        </div>
                      </div>
                    </div>
                    <!-- End Modal -->


           <div class="col-xl-6 col-md-6">
            <!-- Widget Linearea Four -->
            <div class="card card-shadow" id="widgetLineareaOne">
              <div class="card-block p-10 pt-10">
                <div class="clearfix">
                  <div class="grey-800 float-left py-10">
                    <i class="icon md-accounts grey-600 font-size-24 vertical-align-bottom mr-5"></i>                    Customers
                  </div>
                   <?php 
                                             $cust=mysqli_query($dbc,"select count(DISTINCT(Company)) as count_column from `campaign` where RMail='$uid' ");
                                             $rc=mysqli_fetch_assoc($cust);
                                             $res=$rc['count_column'];
                                             
                                             ?>
                  <span class="float-right grey-700 font-size-30"><?php echo $res; ?></span>
                </div>
                <div class="mb-20 grey-500">
                  
                  <button type="button" class="btn btn-round btn-info btn-xs waves-effect waves-classic float-right" data-target=".example-modal-lg" data-toggle="modal" id="campaignedemodal">Campaigned</button>
                  <a href="custdash.php"><button type="button" class="btn btn-round btn-info btn-xs waves-effect waves-classic float-right" data-target=".example-modal-lg" data-toggle="modal" id="totalcustomer">Total</button></a>
                </div>
              </div>
            </div>

            <!-- Modal -->

                    <div class="modal fade example-modal-lg" id="campaignedemodal"
                      aria-hidden="true" aria-labelledby="exampleModalTitle" role="dialog"
                      tabindex="-1">
                      <div class="modal-dialog modal-simple modal-lg">
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">×</span>
                            </button>
                            <h4 class="modal-title">Campaigned Prospects</h4>
                          </div>
                          <div class="modal-body">
                           <?php
                                                $fetfunnel=mysqli_query($dbc,"select distinct(Company), ContactPerson, Designation, Mobile, Mail, CampaignDate   from `campaign` where RMail='$uid' ");
                                                //$fetfunnel=mysqli_query($dbc,"select * from `customers` ");
                                        
                                                echo '<table class="table table-bordered  table-striped mb-none" >';
                                                /*echo '<table class="table table-striped mb-none" id="datatable-tabletools" data-swf-path="assets/vendor/jquery-datatables/extras/TableTools/swf/copy_csv_xls_pdf.swf">';*/
                                                    echo '<thead>';
                                                        echo '<tr>';
                                                            echo '<th>Sl No.</th>';
                                                            echo '<th>Company</th>';
                                                            echo '<th>ContactPerson</th>';
                                                            echo '<th>Designation</th>';
                                                            echo '<th>Mobile</th>';
                                                            echo '<th>Mail</th>';
                                                            echo '<th>CampaignDate</th>';
                                                    
                                                        echo '</tr>';
                                                    echo '</thead>';
                                                    echo '<tbody>';
                                                    $cci=0;
                                                        while($frow=mysqli_fetch_assoc($fetfunnel))
                                                        {
                                                            global $cci;
                                                            $cci=$cci+1;
                                                            $company=$frow['Company'];
                                                            $product=$frow['ContactPerson'];
                                                            $detail=$frow['Designation'];
                                                            $revenue=$frow['Mobile'];
                                                            $margin=$frow['Mail'];
                                                            $campaigneddate=$frow['CampaignDate'];
                                                    

                                                    
                                                            echo '<tr>';
                                                            echo "<td>".$cci."</td>";
                                                            echo "<td>{$frow['Company']}</td>";
                                                            echo "<td>{$frow['ContactPerson']}</td>";
                                                            echo "<td>{$frow['Designation']}</td>";
                                                            echo "<td><a href=tel:{$frow['Mobile']}>{$frow['Mobile']}</a></td>";
                                                            echo "<td>{$frow['Mail']}</td>";
                                                            echo "<td>{$frow['CampaignDate']}</td>";
                                                        
                                                        
                                                            echo '</tr>';
                                                    
                                                        }
                                                    echo '</tbody>';
                                                echo '</table>';
                                            ?>

                          </div>
           
                          <div class="modal-footer">
                             <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                          </div>
                        </div>
                      </div>
                    </div>
                    <!-- End Modal -->


            <!-- End Widget Linearea Four -->
          </div>
          <div class="col-xl-6 col-md-6">
            <!-- Widget Linearea Four -->
            <div class="card card-shadow" id="widgetLineareaOne">
              <div class="card-block p-10 pt-10">
                <div class="clearfix">
                  <div class="grey-800 float-left py-10">
                    <i class="icon md-accounts grey-600 font-size-24 vertical-align-bottom mr-5"></i>                    Meetings
                  </div>
                   <?php 
                                             $cust=mysqli_query($dbc,"select count(DISTINCT(Company)) as count_column from `meeting` where RMail='$uid' ");
                                             $rc=mysqli_fetch_assoc($cust);
                                             $res=$rc['count_column'];
                                             
                                             ?>
                  <span class="float-right grey-700 font-size-30"><?php echo $res; ?></span>
                </div>
                <div class="mb-20 grey-500">
                  <button type="button" class="btn btn-round btn-info btn-xs waves-effect waves-classic float-right" data-target=".example1-modal-lg" data-toggle="modal" id="totalmeetingsmodal">Total</button><button type="button" class="btn btn-round btn-info btn-xs waves-effect waves-classic float-right" data-target=".example2-modal-lg" data-toggle="modal" id="monthlymeetingsmodal">Monthly</button><button type="button" class="btn btn-round btn-info btn-xs waves-effect waves-classic float-right" data-target=".example3-modal-lg" data-toggle="modal" id="annuallymeetingsmodal">Annually</button>
                </div>
              </div>
            </div>

            <!-- Total Meetings Modal -->
 <!-- Modal -->
                    <div class="modal fade example1-modal-lg" id="totalmeetingsmodal"
                      aria-hidden="true" aria-labelledby="exampleModalTitle" role="dialog"
                      tabindex="-1">
                      <div class="modal-dialog modal-simple modal-lg">
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">×</span>
                            </button>
                            <h4 class="modal-title">Total Meetings</h4>
                          </div>
                          <div class="modal-body">
                           <?php
                                    $fetfunnel=mysqli_query($dbc,"select m.*  from `meeting` m inner join (
                                        select Company, max(Date) as MaxDate
                                        from meeting where RMail='$uid'
                                        group by Company
                                    ) tm on m.Company = tm.Company and m.Date = tm.MaxDate group by m.Company ORDER BY `ModificationDetail` DESC");
                                    
                                    echo '<table class="table table-striped table-responsive-md table-bordered example1" >';
                                        echo '<thead>';
                                            echo '<tr>';
                                                echo '<th>Sl No.</th>';
                                                echo '<th>Company</th>';
                                                echo '<th>ContactPerson</th>';
                                                echo '<th>Designation</th>';
                                                /*echo '<th>Mobile</th>';
                                                echo '<th>Mail</th>';*/
                                                echo '<th>Last Meeting</th>';
                                                echo '<th>Status</th>';
                                                echo '<th>Requirement</th>';
                                    
                                        
                                            echo '</tr>';
                                        echo '</thead>';
                                        echo '<tbody>';
                                        $tmi=0;
                                            while($frow=mysqli_fetch_assoc($fetfunnel))
                                            {
                                                global $tmi;
                                                $tmi=$tmi+1;
                                                $company=$frow['Company'];
                                                $contactperson=$frow['ContactPerson'];
                                                
                                                $mobile=$frow['Mobile'];
                                                $mailid=$frow['Mail'];
                                                $lastmeeting=$frow['Date'];
                                                $pro=$frow['ProjectName'];
                                                $campaigntype=$frow['CampaignType'];
                                                $requirement=$frow['Requirement'];
                                                $sector=$frow['Sector'];
                                                
                                                $companytype=$frow['CompanyType'];
                                                $designation=$frow['Designation'];
                                    
                                    
                                                $st=mysqli_query($dbc,"select Stage from funnel where Company='$company' and MeetingDate='$lastmeeting' ");
                                                while ($str=mysqli_fetch_assoc($st)) {
                                                    $status=$str['Stage'];
                                                }
                                    
                                                if($status == null)
                                                {
                                                    echo '<tr>';
                                                echo "<td>".$tmi."</td>";
                                                /*echo '<td><a href="campins.php?c='.$company.'&f='.$contactperson.'&e='.$mailid.'&t='.$pro.'&ctype='.$campaigntype.'"  >'.$company.'</a></td>';*/
                                                echo '<td><div class="dropdown">
                                                    <a class="dropbtn" style="text-decoration: none">'.$company.'</a>
                                                        <div class="dropdown-content">
                                                            <a href="campins.php?c='.$company.'&f='.$contactperson.'&e='.$mailid.'&t='.$pro.'&ctype='.$campaigntype.'"  >Update Meeting</a>
                                                            <a href="proscat.php?c='.$company.'">View Profile</a>
                                                        </div>
                                                    </div></td>';
                                                echo "<td>{$frow['ContactPerson']}</td>";
                                                echo "<td>{$frow['Designation']}</td>";
                                                /*echo "<td><a href=tel:{$frow['Mobile']}>{$frow['Mobile']}</a></td>";
                                                echo "<td>{$frow['Mail']}</td>";*/
                                                echo "<td>{$frow['Date']}</td>";
                                                echo "<td>Meeting Stage</td>";
                                                echo '<td><span class="more">'.$requirement.'</span></td>';
                                                
                                                echo '</tr>';
                                                $status=null;
                                    
                                                }
                                                else
                                                {
                                    
                                        
                                                echo '<tr>';
                                                echo "<td>".$tmi."</td>";
                                                /*echo '<td><a href="campins.php?c='.$company.'&f='.$contactperson.'&e='.$mailid.'&t='.$pro.'&ctype='.$campaigntype.'"  >'.$company.'</a></td>';*/
                                                echo '<td><div class="dropdown">
                                                    <a class="dropbtn" style="text-decoration: none">'.$company.'</a>
                                                        <div class="dropdown-content">
                                                            <a href="campins.php?c='.$company.'&f='.$contactperson.'&e='.$mailid.'&t='.$pro.'&ctype='.$campaigntype.'"  >Update Meeting</a>
                                                            <a href="proscat.php?c='.$company.'">View Profile</a>
                                                        </div>
                                                    </div></td>';
                                                echo "<td>{$frow['ContactPerson']}</td>";
                                                echo "<td>{$frow['Designation']}</td>";
                                               /* echo "<td><a href=tel:{$frow['Mobile']}>{$frow['Mobile']}</a></td>";
                                                echo "<td>{$frow['Mail']}</td>";*/
                                                echo "<td>{$frow['Date']}</td>";
                                                echo "<td>".$status."</td>";
                                                echo '<td><span class="more">'.$requirement.'</span></td>';
                                                
                                            
                                            
                                                echo '</tr>';
                                                $status=null;
                                                }
                                        
                                            }
                                        echo '</tbody>';
                                    echo '</table>';
                                    ?>

                          </div>
                          <div class="modal-footer">
                             <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                          </div>
                        </div>
                      </div>
                    </div>
                    <!-- End Modal -->

<!-- Monthly Meetings Modal -->
 <!-- Modal -->
                    <div class="modal fade example2-modal-lg" id="monthlymeetingsmodal"
                      aria-hidden="true" aria-labelledby="exampleModalTitle" role="dialog"
                      tabindex="-1">
                      <div class="modal-dialog modal-simple modal-lg">
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">×</span>
                            </button>
                            <h4 class="modal-title">Monthly Meetings</h4>
                          </div>
                          <div class="modal-body">
                          <?php
                                    $fetfunnel=mysqli_query($dbc,"select m.*  from `meeting` m inner join (
                                        select Company, max(Date) as MaxDate
                                        from meeting where RMail='$uid' and MONTH(Date) = MONTH(CURRENT_DATE()) AND YEAR(Date) = YEAR(CURRENT_DATE())
                                        group by Company
                                    ) tm on m.Company = tm.Company and m.Date = tm.MaxDate group by m.Company order by `ModificationDetail` desc");
                                    
                                    echo '<table class="table table-striped table-responsive-md table-bordered example" >';
                                        echo '<thead>';
                                            echo '<tr>';
                                                echo '<th>Sl No.</th>';
                                                echo '<th>Company</th>';
                                                echo '<th>ContactPerson</th>';
                                                echo '<th>Designation</th>';
                                                /*echo '<th>Mobile</th>';
                                                echo '<th>Mail</th>';*/
                                                echo '<th>Last Meeting</th>';
                                                echo '<th>Status</th>';
                                                echo '<th>Requirement</th>';
                                    
                                        
                                            echo '</tr>';
                                        echo '</thead>';
                                        echo '<tbody>';
                                        $tmi=0;
                                            while($frow=mysqli_fetch_assoc($fetfunnel))
                                            {
                                                global $tmi;
                                                $tmi=$tmi+1;
                                                $company=$frow['Company'];
                                                $contactperson=$frow['ContactPerson'];
                                                
                                                $mobile=$frow['Mobile'];
                                                $mailid=$frow['Mail'];
                                                $lastmeeting=$frow['Date'];
                                                $pro=$frow['ProjectName'];
                                                $campaigntype=$frow['CampaignType'];
                                                $requirement=$frow['Requirement'];
                                                $sector=$frow['Sector'];
                                                
                                                $companytype=$frow['CompanyType'];
                                                $designation=$frow['Designation'];
                                    
                                    
                                                $st=mysqli_query($dbc,"select Stage from funnel where Company='$company' and MeetingDate='$lastmeeting' ");
                                                while ($str=mysqli_fetch_assoc($st)) {
                                                    $status=$str['Stage'];
                                                }
                                    
                                                if($status == null)
                                                {
                                                    echo '<tr>';
                                                echo "<td>".$tmi."</td>";
                                                /*echo '<td><a href="campins.php?c='.$company.'&f='.$contactperson.'&e='.$mailid.'&t='.$pro.'&ctype='.$campaigntype.'"  >'.$company.'</a></td>';*/
                                                echo '<td><div class="dropdown">
                                                    <a class="dropbtn" style="text-decoration: none">'.$company.'</a>
                                                        <div class="dropdown-content">
                                                            <a href="campins.php?c='.$company.'&f='.$contactperson.'&e='.$mailid.'&t='.$pro.'&ctype='.$campaigntype.'"  >Update Meeting</a>
                                                            <a href="proscat.php?c='.$company.'">View Profile</a>
                                                        </div>
                                                    </div></td>';
                                                echo "<td>{$frow['ContactPerson']}</td>";
                                                echo "<td>{$frow['Designation']}</td>";
                                                /*echo "<td><a href=tel:{$frow['Mobile']}>{$frow['Mobile']}</a></td>";
                                                echo "<td>{$frow['Mail']}</td>";*/
                                                echo "<td>{$frow['Date']}</td>";
                                                echo "<td>Meeting Stage</td>";
                                                echo '<td><span class="more">'.$requirement.'</span></td>';
                                                
                                                echo '</tr>';
                                                $status=null;
                                    
                                                }
                                                else
                                                {
                                    
                                        
                                                echo '<tr>';
                                                echo "<td>".$tmi."</td>";
                                                /*echo '<td><a href="campins.php?c='.$company.'&f='.$contactperson.'&e='.$mailid.'&t='.$pro.'&ctype='.$campaigntype.'"  >'.$company.'</a></td>';*/
                                                echo '<td><div class="dropdown">
                                                    <a class="dropbtn" style="text-decoration: none">'.$company.'</a>
                                                        <div class="dropdown-content">
                                                            <a href="campins.php?c='.$company.'&f='.$contactperson.'&e='.$mailid.'&t='.$pro.'&ctype='.$campaigntype.'"  >Update Meeting</a>
                                                            <a href="proscat.php?c='.$company.'">View Profile</a>
                                                        </div>
                                                    </div></td>';
                                                echo "<td>{$frow['ContactPerson']}</td>";
                                                echo "<td>{$frow['Designation']}</td>";
                                               /* echo "<td><a href=tel:{$frow['Mobile']}>{$frow['Mobile']}</a></td>";
                                                echo "<td>{$frow['Mail']}</td>";*/
                                                echo "<td>{$frow['Date']}</td>";
                                                echo "<td>".$status."</td>";
                                                echo '<td><span class="more">'.$requirement.'</span></td>';
                                                
                                            
                                            
                                                echo '</tr>';
                                                $status=null;
                                                }
                                        
                                            }
                                        echo '</tbody>';
                                    echo '</table>';
                                    ?>

                          </div>
                          <div class="modal-footer">
                             <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                          </div>
                        </div>
                      </div>
                    </div>
                    <!-- End Modal -->

                    <!-- Annually Meetings Modal -->
 <!-- Modal -->
                    <div class="modal fade example3-modal-lg" id="annuallymeetingsmodal"
                      aria-hidden="true" aria-labelledby="exampleModalTitle" role="dialog"
                      tabindex="-1">
                      <div class="modal-dialog modal-simple modal-lg">
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">×</span>
                            </button>
                            <h4 class="modal-title">Annual Meetings</h4>
                          </div>
                          <div class="modal-body">
                          <?php
                                    $fetfunnel=mysqli_query($dbc,"select m.*  from `meeting` m inner join (
                                        select Company, max(Date) as MaxDate
                                        from meeting where RMail='$uid' and  YEAR(Date) = YEAR(CURRENT_DATE())
                                        group by Company
                                    ) tm on m.Company = tm.Company and m.Date = tm.MaxDate group by m.Company order by `ModificationDetail` desc");
                                    
                                    echo '<table class="table table-striped table-responsive-md table-bordered example" >';
                                        echo '<thead>';
                                            echo '<tr>';
                                                echo '<th>Sl No.</th>';
                                                echo '<th>Company</th>';
                                                echo '<th>ContactPerson</th>';
                                                echo '<th>Designation</th>';
                                                /*echo '<th>Mobile</th>';
                                                echo '<th>Mail</th>';*/
                                                echo '<th>Last Meeting</th>';
                                                echo '<th>Status</th>';
                                                echo '<th>Requirement</th>';
                                    
                                        
                                            echo '</tr>';
                                        echo '</thead>';
                                        echo '<tbody>';
                                        $tmi=0;
                                            while($frow=mysqli_fetch_assoc($fetfunnel))
                                            {
                                                global $tmi;
                                                $tmi=$tmi+1;
                                                $company=$frow['Company'];
                                                $contactperson=$frow['ContactPerson'];
                                                
                                                $mobile=$frow['Mobile'];
                                                $mailid=$frow['Mail'];
                                                $lastmeeting=$frow['Date'];
                                                $pro=$frow['ProjectName'];
                                                $campaigntype=$frow['CampaignType'];
                                                $requirement=$frow['Requirement'];
                                                $sector=$frow['Sector'];
                                                
                                                $companytype=$frow['CompanyType'];
                                                $designation=$frow['Designation'];
                                    
                                    
                                                $st=mysqli_query($dbc,"select Stage from funnel where Company='$company' and MeetingDate='$lastmeeting' ");
                                                while ($str=mysqli_fetch_assoc($st)) {
                                                    $status=$str['Stage'];
                                                }
                                    
                                                if($status == null)
                                                {
                                                    echo '<tr>';
                                                echo "<td>".$tmi."</td>";
                                                /*echo '<td><a href="campins.php?c='.$company.'&f='.$contactperson.'&e='.$mailid.'&t='.$pro.'&ctype='.$campaigntype.'"  >'.$company.'</a></td>';*/
                                                echo '<td><div class="dropdown">
                                                    <a class="dropbtn" style="text-decoration: none">'.$company.'</a>
                                                        <div class="dropdown-content">
                                                            <a href="campins.php?c='.$company.'&f='.$contactperson.'&e='.$mailid.'&t='.$pro.'&ctype='.$campaigntype.'"  >Update Meeting</a>
                                                            <a href="proscat.php?c='.$company.'">View Profile</a>
                                                        </div>
                                                    </div></td>';
                                                echo "<td>{$frow['ContactPerson']}</td>";
                                                echo "<td>{$frow['Designation']}</td>";
                                                /*echo "<td><a href=tel:{$frow['Mobile']}>{$frow['Mobile']}</a></td>";
                                                echo "<td>{$frow['Mail']}</td>";*/
                                                echo "<td>{$frow['Date']}</td>";
                                                echo "<td>Meeting Stage</td>";
                                                echo '<td><span class="more">'.$requirement.'</span></td>';
                                                
                                                echo '</tr>';
                                                $status=null;
                                    
                                                }
                                                else
                                                {
                                    
                                        
                                                echo '<tr>';
                                                echo "<td>".$tmi."</td>";
                                                /*echo '<td><a href="campins.php?c='.$company.'&f='.$contactperson.'&e='.$mailid.'&t='.$pro.'&ctype='.$campaigntype.'"  >'.$company.'</a></td>';*/
                                                echo '<td><div class="dropdown">
                                                    <a class="dropbtn" style="text-decoration: none">'.$company.'</a>
                                                        <div class="dropdown-content">
                                                            <a href="campins.php?c='.$company.'&f='.$contactperson.'&e='.$mailid.'&t='.$pro.'&ctype='.$campaigntype.'"  >Update Meeting</a>
                                                            <a href="proscat.php?c='.$company.'">View Profile</a>
                                                        </div>
                                                    </div></td>';
                                                echo "<td>{$frow['ContactPerson']}</td>";
                                                echo "<td>{$frow['Designation']}</td>";
                                               /* echo "<td><a href=tel:{$frow['Mobile']}>{$frow['Mobile']}</a></td>";
                                                echo "<td>{$frow['Mail']}</td>";*/
                                                echo "<td>{$frow['Date']}</td>";
                                                echo "<td>".$status."</td>";
                                                echo '<td><span class="more">'.$requirement.'</span></td>';
                                                
                                            
                                            
                                                echo '</tr>';
                                                $status=null;
                                                }
                                        
                                            }
                                        echo '</tbody>';
                                    echo '</table>';
                                    ?>

                          </div>
                          <div class="modal-footer">
                             <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                          </div>
                        </div>
                      </div>
                    </div>
                    <!-- End Modal -->
            <!-- End Widget Linearea Four -->
          </div>
          <div class="col-xl-6 col-md-6">
            <!-- Widget Linearea Four -->
            <div class="card card-shadow" id="widgetLineareaOne">
              <div class="card-block p-10 pt-10">
                <div class="clearfix">
                  <div class="grey-800 float-left py-10">
                    <i class="icon md-chart grey-600 font-size-24 vertical-align-bottom mr-5"></i>                    Orders
                  </div>
                  <?php 
                                                            $cust=mysqli_query($dbc,"select count(Company) as count_column from `funnel` where RMail='$uid' and  `Stage`='Won'");
                                                            $rc=mysqli_fetch_assoc($cust);
                                                            $res=$rc['count_column'];
                                                            
                                                        ?>
                  <span class="float-right grey-700 font-size-30"><?php echo $res; ?></span>
                </div>
                <div class="mb-20 grey-500">
                  <button type="button" class="btn btn-round btn-info btn-xs waves-effect waves-classic float-right" data-target=".example4-modal-lg" data-toggle="modal" id="annualordersmodal">Annually</button><button type="button" class="btn btn-round btn-info btn-xs waves-effect waves-classic float-right" data-target=".example5-modal-lg" data-toggle="modal" id="monthlyordersmodal">Monthly</button><button type="button" class="btn btn-round btn-info btn-xs waves-effect waves-classic float-right" data-target=".example6-modal-lg" data-toggle="modal" id="weeklyordersmodal">Weekly</button><button type="button" class="btn btn-round btn-info btn-xs waves-effect waves-classic float-right" data-target=".example7-modal-lg" data-toggle="modal" id="todayordersmodal">Today</button><button type="button" class="btn btn-round btn-info btn-xs waves-effect waves-classic float-right" data-target=".example8-modal-lg" data-toggle="modal" id="totalordersmodal">Total</button>
                </div>
              </div>
            </div>

             <!-- Annually Orders Modal -->
 <!-- Modal -->
                    <div class="modal fade example4-modal-lg" id="annualordersmodal"
                      aria-hidden="true" aria-labelledby="exampleModalTitle" role="dialog"
                      tabindex="-1">
                      <div class="modal-dialog modal-simple modal-lg">
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">×</span>
                            </button>
                            <h4 class="modal-title">Total Annual Orders</h4>
                          </div>
                          <div class="modal-body">
                          <?php
                                                $fetfunnel=mysqli_query($dbc,"select * from `funnel` where RMail='$uid' and  `Stage`='Won' AND   DATE(ModificationDetail) BETWEEN '$fydate' AND '$lydate' ");
                                        
                                                echo '<table class="table table-bordered table-striped mb-none">';
                                                    echo '<thead>';
                                                        echo '<tr>';
                                                        echo '<th>Sl No.</th>';
                                                        echo '<th>Company</th>';
                                                        echo '<th>Product</th>';
                                                        echo '<th>Service</th>';
                                                        echo '<th>Detail</th>';
                                                        echo '<th>Revenue</th>';
                                                        echo '<th>Margin</th>';
                                                        echo '<th>Margin %</th>';
                                                        echo '<th>Closure Date</th>';
                                                        
                                                        echo '</tr>';
                                                    echo '</thead>';
                                                    echo '<tbody>';
                                                    $toai=0;
                                                        while($frow=mysqli_fetch_assoc($fetfunnel))
                                                        {
                                                            global $toai;
                                                            $toai=$toai+1;
                                                            $company=$frow['Company'];
                                                            $product=$frow['Products'];
                                                            $service=$frow['Services'];
                                                            $detail=$frow['Detail'];
                                                            $revenue=$frow['Revenue'];
                                                            $margin=$frow['Margin'];
                                                            $closuredate=$frow['ModificationDetail'];
                                                    
                                                            $closuredate_final=date("d/m/Y", strtotime($closuredate));

                                                            $marginpercent=($margin/$revenue)*100;
                                                            $marginpercent_final=number_format((float)$marginpercent, 2, '.', '');
                                                    
                                                            echo '<tr>';
                                                            echo "<td>".$toai."</td>";
                                                            echo "<td>{$frow['Company']}</td>";
                                                            echo "<td>{$frow['Products']}</td>";
                                                            echo "<td>{$frow['Services']}</td>";
                                                            /*echo "<td>{$frow['Detail']}</td>";*/
                                                            echo '<td><span class="more">'.$detail.'</span></td>';
                                                            echo "<td>{$frow['Revenue']}</td>";
                                                            echo "<td>{$frow['Margin']}</td>";
                                                            echo "<td>".$marginpercent_final."</td>";
                                                            echo "<td>{$closuredate_final}</td>";
                                                        
                                                        
                                                            echo '</tr>';
                                                    
                                                        }
                                                            $totalr=0;
                                                            $custr=mysqli_query($dbc,"select `Revenue` from `funnel` where RMail='$uid' and  `Stage`='Won' AND   DATE(ModificationDetail) BETWEEN '$fydate' AND '$lydate'  ");
                                                            while($rcr=mysqli_fetch_assoc($custr))
                                                            {
                                                                $rev=$rcr['Revenue'];
                                                                global $totalr;
                                                                $totalr=$totalr+$rev;
                                                            }
                                                            $trevenue=$totalr;

                                                            $totalm=0;
                                                            $custm=mysqli_query($dbc,"select `Margin` from `funnel` where RMail='$uid' and  `Stage`='Won' AND   DATE(ModificationDetail) BETWEEN '$fydate' AND '$lydate'  ");
                                                            while($rcm=mysqli_fetch_assoc($custm))
                                                            {
                                                                $mar=$rcm['Margin'];
                                                                global $totalm;
                                                                $totalm=$totalm+$mar;
                                                            }
                                                            $tmargin=$totalm;

                                                            echo '<tr>';
                                                            echo '<th>Total</th>';
                                                            echo '<th></th>';
                                                            echo '<th></th>';
                                                            echo '<th></th>';
                                                            echo '<th></th>';
                                                            echo '<th>'.$trevenue.'</th>';
                                                            echo '<th>'.$tmargin.'</th>';
                                                            echo '<th></th>';
                                                            echo '<th></th>';
                                                            echo '</tr>';
                                                    echo '</tbody>';
                                                echo '</table>';
                                            ?>

                          </div>
                          <div class="modal-footer">
                             <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                          </div>
                        </div>
                      </div>
                    </div>
                    <!-- End Modal -->

                     <!-- Monthly Orders Modal -->
 <!-- Modal -->
                    <div class="modal fade example5-modal-lg" id="monthlyordersmodal"
                      aria-hidden="true" aria-labelledby="exampleModalTitle" role="dialog"
                      tabindex="-1">
                      <div class="modal-dialog modal-simple modal-lg">
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">×</span>
                            </button>
                            <h4 class="modal-title">Total Monthly Orders</h4>
                          </div>
                          <div class="modal-body">
                          <?php
                                                $fetfunnel=mysqli_query($dbc,"select * from `funnel` where RMail='$uid' and  `Stage`='Won' AND  MONTH(ModificationDetail) = MONTH(CURRENT_DATE()) AND  DATE(ModificationDetail) BETWEEN '$fydate' AND '$lydate'  ");
                                        
                                                echo '<table class="table table-bordered table-striped mb-none">';
                                                    echo '<thead>';
                                                        echo '<tr>';
                                                        echo '<th>Sl No.</th>';
                                                        echo '<th>Company</th>';
                                                        echo '<th>Product</th>';
                                                        echo '<th>Service</th>';
                                                        echo '<th>Detail</th>';
                                                        echo '<th>Revenue</th>';
                                                        echo '<th>Margin</th>';
                                                        echo '<th>Margin %</th>';
                                                        echo '<th>Closure Date</th>';
                                                        
                                                        echo '</tr>';
                                                    echo '</thead>';
                                                    echo '<tbody>';
                                                    $tomi=0;
                                                        while($frow=mysqli_fetch_assoc($fetfunnel))
                                                        {
                                                            global $tomi;
                                                            $tomi=$tomi+1;
                                                            $company=$frow['Company'];
                                                            $product=$frow['Products'];
                                                            $service=$frow['Services'];
                                                            $detail=$frow['Detail'];
                                                            $revenue=$frow['Revenue'];
                                                            $margin=$frow['Margin'];
                                                            $closuredate=$frow['ModificationDetail'];
                                                    
                                                            $closuredate_final=date("d/m/Y", strtotime($closuredate));

                                                            $marginpercent=($margin/$revenue)*100;
                                                            $marginpercent_final=number_format((float)$marginpercent, 2, '.', '');
                                                    
                                                            echo '<tr>';
                                                            echo "<td>".$tomi."</td>";
                                                            echo "<td>{$frow['Company']}</td>";
                                                            echo "<td>{$frow['Products']}</td>";
                                                            echo "<td>{$frow['Services']}</td>";
                                                            /*echo "<td>{$frow['Detail']}</td>";*/
                                                            echo '<td><span class="more">'.$detail.'</span></td>';
                                                            echo "<td>{$frow['Revenue']}</td>";
                                                            echo "<td>{$frow['Margin']}</td>";
                                                            echo "<td>".$marginpercent_final."</td>";
                                                            echo "<td>{$closuredate_final}</td>";
                                                        
                                                        
                                                            echo '</tr>';
                                                    
                                                        }
                                                            $totalr=0;
                                                            $custr=mysqli_query($dbc,"select `Revenue` from `funnel` where RMail='$uid' and  `Stage`='Won' AND  MONTH(ModificationDetail) = MONTH(CURRENT_DATE()) AND  DATE(ModificationDetail) BETWEEN '$fydate' AND '$lydate'  ");
                                                            while($rcr=mysqli_fetch_assoc($custr))
                                                            {
                                                                $rev=$rcr['Revenue'];
                                                                global $totalr;
                                                                $totalr=$totalr+$rev;
                                                            }
                                                            $trevenue=$totalr;

                                                            $totalm=0;
                                                            $custm=mysqli_query($dbc,"select `Margin` from `funnel` where RMail='$uid' and  `Stage`='Won' AND  MONTH(ModificationDetail) = MONTH(CURRENT_DATE()) AND  DATE(ModificationDetail) BETWEEN '$fydate' AND '$lydate' ");
                                                            while($rcm=mysqli_fetch_assoc($custm))
                                                            {
                                                                $mar=$rcm['Margin'];
                                                                global $totalm;
                                                                $totalm=$totalm+$mar;
                                                            }
                                                            $tmargin=$totalm;

                                                            echo '<tr>';
                                                            echo '<th>Total</th>';
                                                            echo '<th></th>';
                                                            echo '<th></th>';
                                                            echo '<th></th>';
                                                            echo '<th></th>';
                                                            echo '<th>'.$trevenue.'</th>';
                                                            echo '<th>'.$tmargin.'</th>';
                                                            echo '<th></th>';
                                                            echo '<th></th>';
                                                            echo '</tr>';
                                                    echo '</tbody>';
                                                echo '</table>';
                                            ?>

                          </div>
                          <div class="modal-footer">
                             <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                          </div>
                        </div>
                      </div>
                    </div>
                    <!-- End Modal -->
 <!-- Weekly Orders Modal -->
 <!-- Modal -->
                    <div class="modal fade example6-modal-lg" id="weeklyordersmodal"
                      aria-hidden="true" aria-labelledby="exampleModalTitle" role="dialog"
                      tabindex="-1">
                      <div class="modal-dialog modal-simple modal-lg">
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">×</span>
                            </button>
                            <h4 class="modal-title">Total Weekly Orders</h4>
                          </div>
                          <div class="modal-body">
                          
                          <?php

                                 
                                 //$cdate=date('h:i a', strtotime($followuptime));

                                 $today=date("Y-m-d");
                                                        $cdate=date('h:i a', strtotime($followuptime));

                                                        $monday = strtotime("last monday");
                                                        $monday = date('w', $monday)==date('w') ? $monday+7*86400 : $monday;

                                                        $satday = strtotime(date("Y-m-d",$monday)." +5 days");

                                                        $this_week_sd = date("Y-m-d",$monday);
                                                        $this_week_ed = date("Y-m-d",$satday);

                                    $fetfromopf=mysqli_query($dbc,"select * from `opf` where  DATE(ModificationDetail) BETWEEN '$this_week_sd' AND '$this_week_ed'  order by `ModificationDetail` desc");

                                    echo '<table class="table table-striped table-responsive table-bordered example"  >';
                                        echo '<thead>';
                                            echo '<tr>';
                                            echo '<th>Sl No.</th>';
                                            echo '<th>Company</th>';
                                            echo '<th>Product</th>';
                                            echo '<th>Service</th>';
                                            echo '<th>Detail</th>';
                                            echo '<th>Revenue</th>';
                                            echo '<th>Margin</th>';
                                            echo '<th>Margin %</th>';
                                            echo '<th>Closure Date</th>';
                                            
                                            echo '</tr>';
                                        echo '</thead>';
                                        echo '<tbody>';
                                    $toir=0;
                                    $ttotal=0;
                                    $ttaxval=0;
                                    $trevenue=0;
                                    while($fopf=mysqli_fetch_assoc($fetfromopf))
                                    {
                                      $opfno=$fopf['OPFNo'];
                                      $quotrefno=$fopf['QuotRefNo'];
                                      $vendorname=$fopf['VendorName'];

                                      $closuredate=$fopf['ModificationDetail'];

                                      $vendorprice=0;$quantity=0;$qt=0;$reve=0;$marginvalue=0;$up=0;
                                      $productsordered=null;$partdesc=null;
                                      for($k=1;$k<=10;$k++)
                                      {
                                        $prod=$fopf['Product'.$k];
                                        if($prod != " " && $prod != null)
                                        {
                                          $productsordered=$productsordered." ".$prod.",";

                                        }

                                        $pdesc=$fopf['PartDescription'.$k];
                                        if($pdesc != " " && $pdesc != null)
                                        {
                                          $partdesc=$partdesc." ".$pdesc.",";

                                        }

                                      
                                        $vp=$fopf['VendorPrice'.$k];
                                        $qt=$fopf['Quantity'.$k];
                                        if($vp != " " && $vp != 0)
                                        {
                                          $vendorprice=$vendorprice+$vp;
                                          $reve=$reve+($vp*$qt);

                                        }

                                        $up=$fopf['UnitPrice'.$k];
                                        $qt=$fopf['Quantity'.$k];
                                        if($up != " " && $up != 0)
                                        {
                                          $unitprice=$unitprice+$up;
                                          $marginvalue=$marginvalue+($up*$qt);

                                        }
                                        
                                        $qt=$fopf['Quantity'.$k];
                                        if($qt != " " && $qt != 0)
                                        {
                                          $quantity=$quantity+$qt;

                                        }

                                        $fetfromquot=mysqli_query($dbc,"select * from `quotation` where  `QuotNo`='$quotrefno' and DATE(ModificationDetail)='$today' order by `ModificationDetail` desc");
                                        while ($fquot=mysqli_fetch_assoc($fetfromquot)) 
                                        {
                                          $company=$fquot['Company'];
                                          $tax=$fquot['Tax'];
                                          $currency=$fquot['Currency'];
                                          $servicename=$fquot['ServiceName'];
                                        }
                                        
                                      }
                                   


                                        
                                              global $toir;
                                                $toir=$toir+1;

                                                $closuredate_final=date("d/m/Y", strtotime($closuredate));

                                                $array =  explode(',', $tax);
                                                $count=0;
                                                $taxval=0;
                                                foreach ($array as $item) {
                                                global $count;
                                                  $count=$count+1;
                                                }

                                                foreach ($array as $item) {

                                                    $taxperarr =  explode('-', $item);

                                                    foreach ($taxperarr as $per) {
                                                      $taxpercentage=$per;

                                                    }

                                                    $taxval=$taxval+(($taxpercentage/100)*$marginvalue);

        
                                                }
                                                
                                                $revenue=sprintf("%.2f",$taxval+$marginvalue);
                                                $margin=sprintf("%.2f",$marginvalue-$reve);
                                                $percentage=sprintf("%.2f",($margin/$revenue)*100);
                                                echo '<tr>';
                                                echo "<td>".$toir."</td>";
                                                echo "<td>".$company."</td>";
                                                echo "<td>".$productsordered."</td>";
                                                echo "<td>".$servicename."</td>";
                                                /*echo "<td>{$frow['Detail']}</td>";*/
                                                echo '<td><span class="more">'.$partdesc.'</span></td>';
                                                echo "<td>".$revenue."</td>";
                                                echo '<td>'.$margin.'</td>';
                                                echo "<td>".$percentage."</td>";
                                                
                                                
                                                
                                                echo "<td>".$closuredate_final."</td>";
                                            
                                                echo '</tr>';

                                                $ttotal=$ttotal+$margin;
                                                $ttaxval=$ttaxval+$percentage;
                                                $trevenue=$trevenue+$revenue;
                                        
                                            

                                    }


                                               
                                               
                                        echo '</tbody>';
                                    echo '</table>';
                                    echo '<table class="table table-striped table-responsive-md table-bordered"  >';
                                     echo '<tr>';
                                              
                                                echo '<th>Total</th>';
                                                echo '<th>Revenue: '.$trevenue.'</th>';
                                                echo '<th>Margin: '.$ttotal.'</th>';
                                                echo '<th>Margin %: '.$ttaxval.'</th>';
                                               
                                    
                                                echo '</tr>';
                                    echo '</table>';
                                    ?>
                          </div>
                          <div class="modal-footer">
                             <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                          </div>
                        </div>
                      </div>
                    </div>
                    <!-- End Modal -->

<!-- Today Orders Modal -->
 <!-- Modal -->
                    <div class="modal fade example7-modal-lg" id="todayordersmodal"
                      aria-hidden="true" aria-labelledby="exampleModalTitle" role="dialog"
                      tabindex="-1">
                      <div class="modal-dialog modal-simple modal-lg">
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">×</span>
                            </button>
                            <h4 class="modal-title">Total Orders Today</h4>
                          </div>
                          <div class="modal-body">
                          <?php

                                 $today=date("Y-m-d");
                                 //$cdate=date('h:i a', strtotime($followuptime));

                                    $fetfromopf=mysqli_query($dbc,"select * from `opf` where  DATE(ModificationDetail)='$today'  order by `ModificationDetail` desc");

                                    echo '<table class="table table-striped table-responsive table-bordered example"  >';
                                        echo '<thead>';
                                            echo '<tr>';
                                            echo '<th>Sl No.</th>';
                                            echo '<th>Company</th>';
                                            echo '<th>Product</th>';
                                            echo '<th>Service</th>';
                                            echo '<th>Detail</th>';
                                            echo '<th>Revenue</th>';
                                            echo '<th>Margin</th>';
                                            echo '<th>Margin %</th>';
                                            echo '<th>Closure Date</th>';
                                            
                                            echo '</tr>';
                                        echo '</thead>';
                                        echo '<tbody>';
                                    $toir=0;
                                    $ttotal=0;
                                    $ttaxval=0;
                                    $trevenue=0;
                                    while($fopf=mysqli_fetch_assoc($fetfromopf))
                                    {
                                      $opfno=$fopf['OPFNo'];
                                      $quotrefno=$fopf['QuotRefNo'];
                                      $vendorname=$fopf['VendorName'];

                                      $closuredate=$fopf['ModificationDetail'];

                                      $vendorprice=0;$quantity=0;$qt=0;$reve=0;$marginvalue=0;$up=0;
                                      $productsordered=null;$partdesc=null;
                                      for($k=1;$k<=10;$k++)
                                      {
                                        $prod=$fopf['Product'.$k];
                                        if($prod != " " && $prod != null)
                                        {
                                          $productsordered=$productsordered." ".$prod.",";

                                        }

                                        $pdesc=$fopf['PartDescription'.$k];
                                        if($pdesc != " " && $pdesc != null)
                                        {
                                          $partdesc=$partdesc." ".$pdesc.",";

                                        }

                                      
                                        $vp=$fopf['VendorPrice'.$k];
                                        $qt=$fopf['Quantity'.$k];
                                        if($vp != " " && $vp != 0)
                                        {
                                          $vendorprice=$vendorprice+$vp;
                                          $reve=$reve+($vp*$qt);

                                        }

                                        $up=$fopf['UnitPrice'.$k];
                                        $qt=$fopf['Quantity'.$k];
                                        if($up != " " && $up != 0)
                                        {
                                          $unitprice=$unitprice+$up;
                                          $marginvalue=$marginvalue+($up*$qt);

                                        }
                                        
                                        $qt=$fopf['Quantity'.$k];
                                        if($qt != " " && $qt != 0)
                                        {
                                          $quantity=$quantity+$qt;

                                        }

                                        $fetfromquot=mysqli_query($dbc,"select * from `quotation` where  `QuotNo`='$quotrefno' and DATE(ModificationDetail)='$today' order by `ModificationDetail` desc");
                                        while ($fquot=mysqli_fetch_assoc($fetfromquot)) 
                                        {
                                          $company=$fquot['Company'];
                                          $tax=$fquot['Tax'];
                                          $currency=$fquot['Currency'];
                                          $servicename=$fquot['ServiceName'];
                                        }
                                        
                                      }
                                   


                                        
                                              global $toir;
                                                $toir=$toir+1;

                                                $closuredate_final=date("d/m/Y", strtotime($closuredate));

                                                $array =  explode(',', $tax);
                                                $count=0;
                                                $taxval=0;
                                                foreach ($array as $item) {
                                                global $count;
                                                  $count=$count+1;
                                                }

                                                foreach ($array as $item) {

                                                    $taxperarr =  explode('-', $item);

                                                    foreach ($taxperarr as $per) {
                                                      $taxpercentage=$per;

                                                    }

                                                    $taxval=$taxval+(($taxpercentage/100)*$marginvalue);

        
                                                }
                                                
                                                $revenue=sprintf("%.2f",$taxval+$marginvalue);
                                                $margin=sprintf("%.2f",$marginvalue-$reve);
                                                $percentage=sprintf("%.2f",($margin/$revenue)*100);
                                                echo '<tr>';
                                                echo "<td>".$toir."</td>";
                                                echo "<td>".$company."</td>";
                                                echo "<td>".$productsordered."</td>";
                                                echo "<td>".$servicename."</td>";
                                                /*echo "<td>{$frow['Detail']}</td>";*/
                                                echo '<td><span class="more">'.$partdesc.'</span></td>';
                                                echo "<td>".$revenue."</td>";
                                                echo '<td>'.$margin.'</td>';
                                                echo "<td>".$percentage."</td>";
                                                
                                                
                                                
                                                echo "<td>".$closuredate_final."</td>";
                                            
                                                echo '</tr>';

                                                $ttotal=$ttotal+$margin;
                                                $ttaxval=$ttaxval+$percentage;
                                                $trevenue=$trevenue+$revenue;
                                        
                                            

                                    }



                                    
                                     echo '</tbody>';
                                    echo '</table>';
                                    echo '<table class="table table-striped table-responsive-md table-bordered"  >';
                                     echo '<tr>';
                                              
                                                echo '<th>Total</th>';
                                                echo '<th>Revenue: '.$trevenue.'</th>';
                                                echo '<th>Margin: '.$ttotal.'</th>';
                                                echo '<th>Margin %: '.$ttaxval.'</th>';
                                               
                                    
                                                echo '</tr>';
                                    echo '</table>';
                                    ?>
                         
                          </div>
                          <div class="modal-footer">
                             <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                          </div>
                        </div>
                      </div>
                    </div>
                    <!-- End Modal -->


<!-- Today Orders Modal -->
 <!-- Modal -->
                    <div class="modal fade example8-modal-lg" id="totalordersmodal"
                      aria-hidden="true" aria-labelledby="exampleModalTitle" role="dialog"
                      tabindex="-1">
                      <div class="modal-dialog modal-simple modal-lg">
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">×</span>
                            </button>
                            <h4 class="modal-title">Total Orders</h4>
                          </div>
                          <div class="modal-body">
                          <?php
                                                $fetfunnel=mysqli_query($dbc,"select * from `funnel` where RMail='$uid' and  `Stage`='Won' order by id desc");
                                        
                                                /*echo '<table class="table table-striped mb-none">';*/
                                                echo '<table class="table table-bordered table-striped mb-none"  >';
                                                    echo '<thead>';
                                                        echo '<tr>';
                                                        echo '<th>Sl No.</th>';
                                                        echo '<th>Company</th>';
                                                        echo '<th>Product</th>';
                                                        echo '<th>Service</th>';
                                                        echo '<th>Detail</th>';
                                                        echo '<th>Revenue</th>';
                                                        echo '<th>Margin</th>';
                                                        echo '<th>Margin %</th>';
                                                        echo '<th>Closure Date</th>';
                                                        
                                                        echo '</tr>';
                                                    echo '</thead>';
                                                    echo '<tbody>';
                                                    $toir=0;
                                                        while($frow=mysqli_fetch_assoc($fetfunnel))
                                                        {
                                                            global $toir;
                                                            $toir=$toir+1;
                                                            $company=$frow['Company'];
                                                            $product=$frow['Products'];
                                                            $service=$frow['Services'];
                                                            $detail=$frow['Detail'];
                                                            $revenue=$frow['Revenue'];
                                                            $margin=$frow['Margin'];
                                                            $closuredate=$frow['ModificationDetail'];
                                                    
                                                            $closuredate_final=date("d/m/Y", strtotime($closuredate));

                                                            $marginpercent=($margin/$revenue)*100;
                                                            $marginpercent_final=number_format((float)$marginpercent, 2, '.', '');
                                                    
                                                            echo '<tr>';
                                                            echo "<td>".$toir."</td>";
                                                            echo "<td>{$frow['Company']}</td>";
                                                            echo "<td>{$frow['Products']}</td>";
                                                            echo "<td>{$frow['Services']}</td>";
                                                            /*echo "<td>{$frow['Detail']}</td>";*/
                                                            echo '<td><span class="more">'.$detail.'</span></td>';
                                                            echo "<td>{$frow['Revenue']}</td>";
                                                            echo "<td>{$frow['Margin']}</td>";
                                                            echo "<td>".$marginpercent_final."</td>";
                                                            echo "<td>{$closuredate_final}</td>";
                                                        
                                                            echo '</tr>';
                                                    
                                                        }
                                                            $totalr=0;
                                                            $custr=mysqli_query($dbc,"select `Revenue` from `funnel` where RMail='$uid' and  `Stage`='Won' ");
                                                            while($rcr=mysqli_fetch_assoc($custr))
                                                            {
                                                                $rev=$rcr['Revenue'];
                                                                global $totalr;
                                                                $totalr=$totalr+$rev;
                                                            }
                                                            $trevenue=$totalr;

                                                            $totalm=0;
                                                            $custm=mysqli_query($dbc,"select `Margin` from `funnel` where RMail='$uid' and  `Stage`='Won' ");
                                                            while($rcm=mysqli_fetch_assoc($custm))
                                                            {
                                                                $mar=$rcm['Margin'];
                                                                global $totalm;
                                                                $totalm=$totalm+$mar;
                                                            }
                                                            $tmargin=$totalm;

                                                            echo '<tr>';
                                                            echo '<th>Total</th>';
                                                            echo '<th></th>';
                                                            echo '<th></th>';
                                                            echo '<th></th>';
                                                            echo '<th></th>';
                                                            echo '<th>'.$trevenue.'</th>';
                                                            echo '<th>'.$tmargin.'</th>';
                                                            echo '<th></th>';
                                                            echo '<th></th>';

                                                            echo '</tr>';
                                                    echo '</tbody>';
                                                echo '</table>';
                                            ?>

                         
                          </div>
                          <div class="modal-footer">
                             <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                          </div>
                        </div>
                      </div>
                    </div>
                    <!-- End Modal -->

            <!-- End Widget Linearea Four -->
          </div>
          <div class="col-xl-6 col-md-6">
            <!-- Widget Linearea Four -->
            <div class="card card-shadow" id="widgetLineareaOne">
              <div class="card-block p-10 pt-10">
                <div class="clearfix">
                  <div class="grey-800 float-left py-10">
                    <i class="icon md-wifi-alt-2 grey-600 font-size-24 vertical-align-bottom mr-5"></i>                    Funnels
                  </div>
                  <?php 
                                                            /*$total=0;
                                                            $cust=mysqli_query($dbc,"select `Revenue` from `funnel` where  `Stage`='Won' ");
                                                            while($rc=mysqli_fetch_assoc($cust))
                                                            {
                                                                $rev=$rc['Revenue'];
                                                                global $total;
                                                                $total=$total+$rev;
                                                            }
                                                            $res=$total;*/
                                                            $cust=mysqli_query($dbc,"select count(id) as count_column from `funnel` where RMail='$uid'    ");
                                                                    $rc=mysqli_fetch_assoc($cust);
                                                                    $res=$rc['count_column'];
                                                            
                                                        ?>
               <span class="float-right grey-700 font-size-30"><?php echo $res; ?></span>
                </div>
                <div class="mb-20 grey-500">
                  <button type="button" class="btn btn-round btn-info btn-xs waves-effect waves-classic float-right"  data-target="#exampleTabs" data-toggle="modal">Probability</button><button type="button" class="btn btn-round btn-info btn-xs waves-effect waves-classic float-right" data-target=".example12-modal-lg" data-toggle="modal" id="todayfunnelmodal">Today</button><button type="button" class="btn btn-round btn-info btn-xs waves-effect waves-classic float-right" data-target=".example11-modal-lg" data-toggle="modal" id="weekfunnelmodal">Week</button><button type="button" class="btn btn-round btn-info btn-xs waves-effect waves-classic float-right" data-target=".example10-modal-lg" data-toggle="modal" id="monthlyfunnelmodal">Monthly</button><button type="button" class="btn btn-round btn-info btn-xs waves-effect waves-classic float-right" data-target=".example9-modal-lg" data-toggle="modal" id="annualfunnelmodal">Annually</button>
                </div>
              </div>
            </div>

            <!-- Annual Funnel Modal -->
 <!-- Modal -->
                    <div class="modal fade example9-modal-lg" id="annualfunnelmodal"
                      aria-hidden="true" aria-labelledby="exampleModalTitle" role="dialog"
                      tabindex="-1">
                      <div class="modal-dialog modal-simple modal-lg">
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">×</span>
                            </button>
                            <h4 class="modal-title">Annual Funnel</h4>
                          </div>
                          <div class="modal-body">
                           <?php
                                    $fetfunnel=mysqli_query($dbc,"select * from `funnel` where   DATE(ModificationDetail) BETWEEN '$fydate' AND '$lydate'  order by `ModificationDetail` desc ");
                                    
                                    echo '<table class="table table-striped table-responsive table-bordered example">';
                                        echo '<thead>';
                                            echo '<tr>';
                                                echo '<th>Sl No.</th>';
                                                echo '<th>Company</th>';
                                                echo '<th>Product</th>';
                                                echo '<th>Detail</th>';
                                                echo '<th>Revenue</th>';
                                                echo '<th>Margin</th>';
                                                echo '<th>Stage</th>';
                                                echo '<th>Probability</th>';
                                                echo '<th>Expected Date Of Closure</th>';
                                                echo '<th>Expected Closure Month</th>';
                                                echo '<th>Date of Closure</th>';
                                            echo '</tr>';
                                        echo '</thead>';
                                        echo '<tbody>';
                                        $totalrevenue=0;
                                        $totalmargin=0;
                                        $fai=0;
                                            while($frow=mysqli_fetch_assoc($fetfunnel))
                                            {
                                                global $totalrevenue;
                                                global $totalmargin;
                                    
                                                global $fai;
                                                $fai=$fai+1;
                                    
                                                $company=$frow['Company'];
                                                $product=$frow['Products'];
                                                $detail=$frow['Detail'];
                                                $revenue=$frow['Revenue'];
                                                $margin=$frow['Margin'];
                                                $stage=$frow['Stage'];
                                                $probability=$frow['Probability'];
                                                $dateofclosure=$frow['DateOfClosure'];
                                                $closuremonth=$frow['ExpectedClosure'];
                                                $closuredate=$frow['ModificationDetail'];
                                                
                                                $closuredate_final=date("Y-m-d", strtotime($closuredate));
                                    
                                                if($stage == "Won")
                                                {
                                                    echo '<tr>';
                                                    echo "<td>".$fai."</td>";
                                                    echo "<td>{$frow['Company']}</td>";
                                                    echo "<td>{$frow['Products']}</td>";
                                                    /*echo "<td>{$frow['Detail']}</td>";*/
                                                    echo '<td><span class="more">'.$detail.'</span></td>';
                                                    echo "<td>{$frow['Revenue']}</td>";
                                                    echo "<td>{$frow['Margin']}</td>";
                                                    echo "<td>{$frow['Stage']}</td>";
                                                    echo '<td><div class="progress progress-sm progress-half-rounded m-none mt-xs light">';
                                                        echo "<div class='progress-bar progress-bar-primary' role='progressbar' aria-valuenow='60' aria-valuemin='0' aria-valuemax='100' style='width: 100%;'>";
                                                            echo "{$frow['Probability']}";
                                                        echo '</div>';
                                                    echo '</div></td>';
                                                    echo "<td>{$frow['DateOfClosure']}</td>";
                                                    echo "<td>{$frow['ExpectedClosure']}</td>";
                                                    echo "<td>{$closuredate_final}</td>";
                                                    echo '</tr>';
                                    
                                                    $totalrevenue=$totalrevenue+$revenue;
                                                    $totalmargin=$totalmargin+$margin;
                                    
                                                }elseif($probability == "0")
                                                {
                                                    echo '<tr>';
                                                    echo "<td>".$fai."</td>";
                                                    echo "<td>{$frow['Company']}</td>";
                                                    echo "<td>{$frow['Products']}</td>";
                                                    /*echo "<td>{$frow['Detail']}</td>";*/
                                                    echo '<td><span class="more">'.$detail.'</span></td>';
                                                    echo "<td>{$frow['Revenue']}</td>";
                                                    echo "<td>{$frow['Margin']}</td>";
                                                    echo "<td>{$frow['Stage']}</td>";
                                                    echo '<td><div class="progress progress-sm progress-half-rounded m-none mt-xs light">';
                                                        echo "<div class='progress-bar progress-bar-danger' role='progressbar' aria-valuenow='60' aria-valuemin='0' aria-valuemax='100' style='width: 100%;'>";
                                                            echo "{$frow['Probability']}";
                                                        echo '</div>';
                                                    echo '</div></td>';
                                                    echo "<td>{$frow['DateOfClosure']}</td>";
                                                    echo "<td>{$frow['ExpectedClosure']}</td>";
                                                    echo "<td></td>";
                                                    
                                                    echo '</tr>';
                                    
                                                    $totalrevenue=$totalrevenue+$revenue;
                                                    $totalmargin=$totalmargin+$margin;
                                    
                                                } elseif($probability == "25")
                                                {
                                                    echo '<tr>';
                                                    echo "<td>".$fai."</td>";
                                                    echo "<td>{$frow['Company']}</td>";
                                                    echo "<td>{$frow['Products']}</td>";
                                                    /*echo "<td>{$frow['Detail']}</td>";*/
                                                    echo '<td><span class="more">'.$detail.'</span></td>';
                                                    echo "<td>{$frow['Revenue']}</td>";
                                                    echo "<td>{$frow['Margin']}</td>";
                                                    echo "<td>{$frow['Stage']}</td>";
                                                    echo '<td><div class="progress progress-sm progress-half-rounded m-none mt-xs light">';
                                                        echo "<div class='progress-bar progress-bar-danger' role='progressbar' aria-valuenow='60' aria-valuemin='0' aria-valuemax='100' style='width: 100%;'>";
                                                            echo "{$frow['Probability']}";
                                                        echo '</div>';
                                                    echo '</div></td>';
                                                    echo "<td>{$frow['DateOfClosure']}</td>";
                                                    echo "<td>{$frow['ExpectedClosure']}</td>";
                                                    echo "<td></td>";
                                                    echo '</tr>';
                                    
                                                    $totalrevenue=$totalrevenue+$revenue;
                                                    $totalmargin=$totalmargin+$margin;
                                    
                                                } elseif($probability == "50")
                                                {
                                                    echo '<tr>';
                                                    echo "<td>".$fai."</td>";
                                                    echo "<td>{$frow['Company']}</td>";
                                                    echo "<td>{$frow['Products']}</td>";
                                                    /*echo "<td>{$frow['Detail']}</td>";*/
                                                    echo '<td><span class="more">'.$detail.'</span></td>';
                                                    echo "<td>{$frow['Revenue']}</td>";
                                                    echo "<td>{$frow['Margin']}</td>";
                                                    echo "<td>{$frow['Stage']}</td>";
                                                    echo '<td><div class="progress progress-sm progress-half-rounded m-none mt-xs light">';
                                                        echo "<div class='progress-bar progress-bar-warning' role='progressbar' aria-valuenow='60' aria-valuemin='0' aria-valuemax='100' style='width: 100%;'>";
                                                            echo "{$frow['Probability']}";
                                                        echo '</div>';
                                                    echo '</div></td>';
                                                    echo "<td>{$frow['DateOfClosure']}</td>";
                                                    echo "<td>{$frow['ExpectedClosure']}</td>";
                                                    echo "<td></td>";
                                                    echo '</tr>';
                                    
                                                    $totalrevenue=$totalrevenue+$revenue;
                                                    $totalmargin=$totalmargin+$margin;
                                    
                                                } elseif($probability == "75")
                                                {
                                                    echo '<tr>';
                                                    echo "<td>".$fai."</td>";
                                                    echo "<td>{$frow['Company']}</td>";
                                                    echo "<td>{$frow['Products']}</td>";
                                                    /*echo "<td>{$frow['Detail']}</td>";*/
                                                    echo '<td><span class="more">'.$detail.'</span></td>';
                                                    echo "<td>{$frow['Revenue']}</td>";
                                                    echo "<td>{$frow['Margin']}</td>";
                                                    echo "<td>{$frow['Stage']}</td>";
                                                    echo '<td><div class="progress progress-sm progress-half-rounded m-none mt-xs light">';
                                                        echo "<div class='progress-bar progress-bar-warning' role='progressbar' aria-valuenow='60' aria-valuemin='0' aria-valuemax='100' style='width: 100%;'>";
                                                            echo "{$frow['Probability']}";
                                                        echo '</div>';
                                                    echo '</div></td>';
                                                    echo "<td>{$frow['DateOfClosure']}</td>";
                                                    echo "<td>{$frow['ExpectedClosure']}</td>";
                                                    echo "<td></td>";
                                                    echo '</tr>';
                                                    $totalrevenue=$totalrevenue+$revenue;
                                                    $totalmargin=$totalmargin+$margin;
                                    
                                                } elseif($probability == "100")
                                                {
                                                    echo '<tr>';
                                                    echo "<td>".$fai."</td>";
                                                    echo "<td>{$frow['Company']}</td>";
                                                    echo "<td>{$frow['Products']}</td>";
                                                    /*echo "<td>{$frow['Detail']}</td>";*/
                                                    echo '<td><span class="more">'.$detail.'</span></td>';
                                                    echo "<td>{$frow['Revenue']}</td>";
                                                    echo "<td>{$frow['Margin']}</td>";
                                                    echo "<td>{$frow['Stage']}</td>";
                                                    echo '<td><div class="progress progress-sm progress-half-rounded m-none mt-xs light">';
                                                        echo "<div class='progress-bar progress-bar-primary' role='progressbar' aria-valuenow='60' aria-valuemin='0' aria-valuemax='100' style='width: 100%;'>";
                                                            echo "{$frow['Probability']}";
                                                        echo '</div>';
                                                    echo '</div></td>';
                                                    echo "<td>{$frow['DateOfClosure']}</td>";
                                                    echo "<td>{$frow['ExpectedClosure']}</td>";
                                                    echo "<td>{$closuredate_final}</td>";
                                                    echo '</tr>';
                                    
                                                    $totalrevenue=$totalrevenue+$revenue;
                                                    $totalmargin=$totalmargin+$margin;
                                    
                                                }
                                                
                                    
                                            }
                                           
                                            
                                        
                                        echo '</tbody>';
                                    echo '</table>';

                                    echo '<table class="table table-striped table-responsive-md table-bordered"  >';
                                     echo '<tr>';
                                              
                                                echo "<th>Total</th>";
                                                echo "<th>Revenue: ".$totalrevenue."</th>";
                                            echo "<th>Margin: ".$totalmargin."</th>";
                                               
                                    
                                                echo '</tr>';
                                    echo '</table>';

                                    ?>

                          </div>
                          <div class="modal-footer">
                             <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                          </div>
                        </div>
                      </div>
                    </div>
                    <!-- End Modal -->

                     <!-- Monthly Funnel Modal -->
 <!-- Modal -->
                    <div class="modal fade example10-modal-lg" id="monthlyfunnelmodal"
                      aria-hidden="true" aria-labelledby="exampleModalTitle" role="dialog"
                      tabindex="-1">
                      <div class="modal-dialog modal-simple modal-lg">
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">×</span>
                            </button>
                            <h4 class="modal-title">Monthly Funnel</h4>
                          </div>
                          <div class="modal-body">
                           <?php
                                    $fetfunnel=mysqli_query($dbc,"select * from `funnel` where  MONTH(ModificationDetail) = MONTH(CURRENT_DATE()) AND  DATE(ModificationDetail) BETWEEN '$fydate' AND '$lydate'  order by `ModificationDetail` desc");
                                    
                                    echo '<table class="table table-striped table-responsive table-bordered example">';
                                        echo '<thead>';
                                            echo '<tr>';
                                                echo '<th>Sl No.</th>';
                                                echo '<th>Company</th>';
                                                echo '<th>Product</th>';
                                                echo '<th>Detail</th>';
                                                echo '<th>Revenue</th>';
                                                echo '<th>Margin</th>';
                                                echo '<th>Stage</th>';
                                                echo '<th>Probability</th>';
                                                echo '<th>Expected Date Of Closure</th>';
                                                echo '<th>Expected Closure Month</th>';
                                                echo '<th>Date of Closure</th>';
                                            echo '</tr>';
                                        echo '</thead>';
                                        echo '<tbody>';
                                        $totalrevenue=0;
                                        $totalmargin=0;
                                        $fmi=0;
                                            while($frow=mysqli_fetch_assoc($fetfunnel))
                                            {
                                                global $totalrevenue;
                                                global $totalmargin;
                                    
                                                global $fmi;
                                    
                                                $fmi=$fmi+1;
                                    
                                                $company=$frow['Company'];
                                                $product=$frow['Products'];
                                                $detail=$frow['Detail'];
                                                $revenue=$frow['Revenue'];
                                                $margin=$frow['Margin'];
                                                $stage=$frow['Stage'];
                                                $probability=$frow['Probability'];
                                                $dateofclosure=$frow['DateOfClosure'];
                                                $closuremonth=$frow['ExpectedClosure'];
                                                $closuredate=$frow['ModificationDetail'];
                                                
                                                $closuredate_final=date("Y-m-d", strtotime($closuredate));
                                    
                                                if($stage == "Won")
                                                {
                                                    echo '<tr>';
                                                    echo "<td>".$fmi."</td>";
                                                    echo "<td>{$frow['Company']}</td>";
                                                    echo "<td>{$frow['Products']}</td>";
                                                    /*echo "<td>{$frow['Detail']}</td>";*/
                                                    echo '<td><span class="more">'.$detail.'</span></td>';
                                                    echo "<td>{$frow['Revenue']}</td>";
                                                    echo "<td>{$frow['Margin']}</td>";
                                                    echo "<td>{$frow['Stage']}</td>";
                                                    echo '<td><div class="progress progress-sm progress-half-rounded m-none mt-xs light">';
                                                        echo "<div class='progress-bar progress-bar-primary' role='progressbar' aria-valuenow='60' aria-valuemin='0' aria-valuemax='100' style='width: 100%;'>";
                                                            echo "{$frow['Probability']}";
                                                        echo '</div>';
                                                    echo '</div></td>';
                                                    echo "<td>{$frow['DateOfClosure']}</td>";
                                                    echo "<td>{$frow['ExpectedClosure']}</td>";
                                                    echo "<td>{$closuredate_final}</td>";
                                                    echo '</tr>';
                                    
                                                    $totalrevenue=$totalrevenue+$revenue;
                                                    $totalmargin=$totalmargin+$margin;
                                    
                                                }elseif($probability == "0")
                                                {
                                                    echo '<tr>';
                                                    echo "<td>".$fmi."</td>";
                                                    echo "<td>{$frow['Company']}</td>";
                                                    echo "<td>{$frow['Products']}</td>";
                                                    /*echo "<td>{$frow['Detail']}</td>";*/
                                                    echo '<td><span class="more">'.$detail.'</span></td>';
                                                    echo "<td>{$frow['Revenue']}</td>";
                                                    echo "<td>{$frow['Margin']}</td>";
                                                    echo "<td>{$frow['Stage']}</td>";
                                                    echo '<td><div class="progress progress-sm progress-half-rounded m-none mt-xs light">';
                                                        echo "<div class='progress-bar progress-bar-danger' role='progressbar' aria-valuenow='60' aria-valuemin='0' aria-valuemax='100' style='width: 100%;'>";
                                                            echo "{$frow['Probability']}";
                                                        echo '</div>';
                                                    echo '</div></td>';
                                                    echo "<td>{$frow['DateOfClosure']}</td>";
                                                    echo "<td>{$frow['ExpectedClosure']}</td>";
                                                    echo "<td></td>";
                                                    
                                                    echo '</tr>';
                                    
                                                    $totalrevenue=$totalrevenue+$revenue;
                                                    $totalmargin=$totalmargin+$margin;
                                    
                                                } elseif($probability == "25")
                                                {
                                                    echo '<tr>';
                                                    echo "<td>".$fmi."</td>";
                                                    echo "<td>{$frow['Company']}</td>";
                                                    echo "<td>{$frow['Products']}</td>";
                                                    /*echo "<td>{$frow['Detail']}</td>";*/
                                                    echo '<td><span class="more">'.$detail.'</span></td>';
                                                    echo "<td>{$frow['Revenue']}</td>";
                                                    echo "<td>{$frow['Margin']}</td>";
                                                    echo "<td>{$frow['Stage']}</td>";
                                                    echo '<td><div class="progress progress-sm progress-half-rounded m-none mt-xs light">';
                                                        echo "<div class='progress-bar progress-bar-danger' role='progressbar' aria-valuenow='60' aria-valuemin='0' aria-valuemax='100' style='width: 100%;'>";
                                                            echo "{$frow['Probability']}";
                                                        echo '</div>';
                                                    echo '</div></td>';
                                                    echo "<td>{$frow['DateOfClosure']}</td>";
                                                    echo "<td>{$frow['ExpectedClosure']}</td>";
                                                    echo "<td></td>";
                                                    echo '</tr>';
                                    
                                                    $totalrevenue=$totalrevenue+$revenue;
                                                    $totalmargin=$totalmargin+$margin;
                                    
                                                } elseif($probability == "50")
                                                {
                                                    echo '<tr>';
                                                    echo "<td>".$fmi."</td>";
                                                    echo "<td>{$frow['Company']}</td>";
                                                    echo "<td>{$frow['Products']}</td>";
                                                    /*echo "<td>{$frow['Detail']}</td>";*/
                                                    echo '<td><span class="more">'.$detail.'</span></td>';
                                                    echo "<td>{$frow['Revenue']}</td>";
                                                    echo "<td>{$frow['Margin']}</td>";
                                                    echo "<td>{$frow['Stage']}</td>";
                                                    echo '<td><div class="progress progress-sm progress-half-rounded m-none mt-xs light">';
                                                        echo "<div class='progress-bar progress-bar-warning' role='progressbar' aria-valuenow='60' aria-valuemin='0' aria-valuemax='100' style='width: 100%;'>";
                                                            echo "{$frow['Probability']}";
                                                        echo '</div>';
                                                    echo '</div></td>';
                                                    echo "<td>{$frow['DateOfClosure']}</td>";
                                                    echo "<td>{$frow['ExpectedClosure']}</td>";
                                                    echo "<td></td>";
                                                    echo '</tr>';
                                    
                                                    $totalrevenue=$totalrevenue+$revenue;
                                                    $totalmargin=$totalmargin+$margin;
                                    
                                                } elseif($probability == "75")
                                                {
                                                    echo '<tr>';
                                                    echo "<td>".$fmi."</td>";
                                                    echo "<td>{$frow['Company']}</td>";
                                                    echo "<td>{$frow['Products']}</td>";
                                                    /*echo "<td>{$frow['Detail']}</td>";*/
                                                    echo '<td><span class="more">'.$detail.'</span></td>';
                                                    echo "<td>{$frow['Revenue']}</td>";
                                                    echo "<td>{$frow['Margin']}</td>";
                                                    echo "<td>{$frow['Stage']}</td>";
                                                    echo '<td><div class="progress progress-sm progress-half-rounded m-none mt-xs light">';
                                                        echo "<div class='progress-bar progress-bar-warning' role='progressbar' aria-valuenow='60' aria-valuemin='0' aria-valuemax='100' style='width: 100%;'>";
                                                            echo "{$frow['Probability']}";
                                                        echo '</div>';
                                                    echo '</div></td>';
                                                    echo "<td>{$frow['DateOfClosure']}</td>";
                                                    echo "<td>{$frow['ExpectedClosure']}</td>";
                                                    echo "<td></td>";
                                                    echo '</tr>';
                                                    $totalrevenue=$totalrevenue+$revenue;
                                                    $totalmargin=$totalmargin+$margin;
                                    
                                                } elseif($probability == "100")
                                                {
                                                    echo '<tr>';
                                                    echo "<td>".$fmi."</td>";
                                                    echo "<td>{$frow['Company']}</td>";
                                                    echo "<td>{$frow['Products']}</td>";
                                                    /*echo "<td>{$frow['Detail']}</td>";*/
                                                    echo '<td><span class="more">'.$detail.'</span></td>';
                                                    echo "<td>{$frow['Revenue']}</td>";
                                                    echo "<td>{$frow['Margin']}</td>";
                                                    echo "<td>{$frow['Stage']}</td>";
                                                    echo '<td><div class="progress progress-sm progress-half-rounded m-none mt-xs light">';
                                                        echo "<div class='progress-bar progress-bar-primary' role='progressbar' aria-valuenow='60' aria-valuemin='0' aria-valuemax='100' style='width: 100%;'>";
                                                            echo "{$frow['Probability']}";
                                                        echo '</div>';
                                                    echo '</div></td>';
                                                    echo "<td>{$frow['DateOfClosure']}</td>";
                                                    echo "<td>{$frow['ExpectedClosure']}</td>";
                                                    echo "<td>{$closuredate_final}</td>";
                                                    echo "<td></td>";
                                                    echo '</tr>';
                                    
                                                    $totalrevenue=$totalrevenue+$revenue;
                                                    $totalmargin=$totalmargin+$margin;
                                    
                                                }
                                                
                                    
                                            }
                                               
                                        
                                        echo '</tbody>';
                                    echo '</table>';

                                    echo '<table class="table table-striped table-responsive-md table-bordered"  >';
                                     echo '<tr>';
                                              
                                                echo "<th>Total</th>";
                                                echo "<th>Revenue: ".$totalrevenue."</th>";
                                            echo "<th>Margin: ".$totalmargin."</th>";
                                               
                                    
                                                echo '</tr>';
                                    echo '</table>';
                                    ?>

                          </div>
                          <div class="modal-footer">
                             <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                          </div>
                        </div>
                      </div>
                    </div>
                    <!-- End Modal -->

                     <!-- weekly Funnel Modal -->
 <!-- Modal -->
                    <div class="modal fade example11-modal-lg" id="weekfunnelmodal"
                      aria-hidden="true" aria-labelledby="exampleModalTitle" role="dialog"
                      tabindex="-1">
                      <div class="modal-dialog modal-simple modal-lg">
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">×</span>
                            </button>
                            <h4 class="modal-title">This Week's Funnel</h4>
                          </div>
                          <div class="modal-body">
                           <?php

                                     $today=date("Y-m-d");
                                                        $cdate=date('h:i a', strtotime($followuptime));

                                                        $monday = strtotime("last monday");
                                                        $monday = date('w', $monday)==date('w') ? $monday+7*86400 : $monday;

                                                        $satday = strtotime(date("Y-m-d",$monday)." +5 days");

                                                        $this_week_sd = date("Y-m-d",$monday);
                                                        $this_week_ed = date("Y-m-d",$satday);

                                    


                                    

                                    $fetfunnel=mysqli_query($dbc,"select * from `funnel` where  DATE(ModificationDetail) BETWEEN '$this_week_sd' AND '$this_week_ed'  order by `ModificationDetail` desc");
                                    
                                    echo '<table class="table table-striped table-responsive table-bordered example">';
                                        echo '<thead>';
                                            echo '<tr>';
                                                echo '<th>Sl No.</th>';
                                                echo '<th>Company</th>';
                                                echo '<th>Product</th>';
                                                echo '<th>Detail</th>';
                                                echo '<th>Revenue</th>';
                                                echo '<th>Margin</th>';
                                                echo '<th>Stage</th>';
                                                echo '<th>Probability</th>';
                                                echo '<th>Expected Date Of Closure</th>';
                                                echo '<th>Expected Closure Month</th>';
                                                echo '<th>Date of Closure</th>';
                                            echo '</tr>';
                                        echo '</thead>';
                                        echo '<tbody>';
                                        $totalrevenue=0;
                                        $totalmargin=0;
                                        $fmi=0;
                                            while($frow=mysqli_fetch_assoc($fetfunnel))
                                            {
                                                global $totalrevenue;
                                                global $totalmargin;
                                    
                                                global $fmi;
                                    
                                                $fmi=$fmi+1;
                                    
                                                $company=$frow['Company'];
                                                $product=$frow['Products'];
                                                $detail=$frow['Detail'];
                                                $revenue=$frow['Revenue'];
                                                $margin=$frow['Margin'];
                                                $stage=$frow['Stage'];
                                                $probability=$frow['Probability'];
                                                $dateofclosure=$frow['DateOfClosure'];
                                                $closuremonth=$frow['ExpectedClosure'];
                                                $closuredate=$frow['ModificationDetail'];
                                                
                                                $closuredate_final=date("Y-m-d", strtotime($closuredate));
                                    
                                                if($stage == "Won")
                                                {
                                                    echo '<tr>';
                                                    echo "<td>".$fmi."</td>";
                                                    echo "<td>{$frow['Company']}</td>";
                                                    echo "<td>{$frow['Products']}</td>";
                                                    /*echo "<td>{$frow['Detail']}</td>";*/
                                                    echo '<td><span class="more">'.$detail.'</span></td>';
                                                    echo "<td>{$frow['Revenue']}</td>";
                                                    echo "<td>{$frow['Margin']}</td>";
                                                    echo "<td>{$frow['Stage']}</td>";
                                                    echo '<td><div class="progress progress-sm progress-half-rounded m-none mt-xs light">';
                                                        echo "<div class='progress-bar progress-bar-primary' role='progressbar' aria-valuenow='60' aria-valuemin='0' aria-valuemax='100' style='width: 100%;'>";
                                                            echo "{$frow['Probability']}";
                                                        echo '</div>';
                                                    echo '</div></td>';
                                                    echo "<td>{$frow['DateOfClosure']}</td>";
                                                    echo "<td>{$frow['ExpectedClosure']}</td>";
                                                    echo "<td>{$closuredate_final}</td>";
                                                    echo '</tr>';
                                    
                                                    $totalrevenue=$totalrevenue+$revenue;
                                                    $totalmargin=$totalmargin+$margin;
                                    
                                                }elseif($probability == "0")
                                                {
                                                    echo '<tr>';
                                                    echo "<td>".$fmi."</td>";
                                                    echo "<td>{$frow['Company']}</td>";
                                                    echo "<td>{$frow['Products']}</td>";
                                                    /*echo "<td>{$frow['Detail']}</td>";*/
                                                    echo '<td><span class="more">'.$detail.'</span></td>';
                                                    echo "<td>{$frow['Revenue']}</td>";
                                                    echo "<td>{$frow['Margin']}</td>";
                                                    echo "<td>{$frow['Stage']}</td>";
                                                    echo '<td><div class="progress progress-sm progress-half-rounded m-none mt-xs light">';
                                                        echo "<div class='progress-bar progress-bar-danger' role='progressbar' aria-valuenow='60' aria-valuemin='0' aria-valuemax='100' style='width: 100%;'>";
                                                            echo "{$frow['Probability']}";
                                                        echo '</div>';
                                                    echo '</div></td>';
                                                    echo "<td>{$frow['DateOfClosure']}</td>";
                                                    echo "<td>{$frow['ExpectedClosure']}</td>";
                                                    echo "<td></td>";
                                                    
                                                    echo '</tr>';
                                    
                                                    $totalrevenue=$totalrevenue+$revenue;
                                                    $totalmargin=$totalmargin+$margin;
                                    
                                                } elseif($probability == "25")
                                                {
                                                    echo '<tr>';
                                                    echo "<td>".$fmi."</td>";
                                                    echo "<td>{$frow['Company']}</td>";
                                                    echo "<td>{$frow['Products']}</td>";
                                                    /*echo "<td>{$frow['Detail']}</td>";*/
                                                    echo '<td><span class="more">'.$detail.'</span></td>';
                                                    echo "<td>{$frow['Revenue']}</td>";
                                                    echo "<td>{$frow['Margin']}</td>";
                                                    echo "<td>{$frow['Stage']}</td>";
                                                    echo '<td><div class="progress progress-sm progress-half-rounded m-none mt-xs light">';
                                                        echo "<div class='progress-bar progress-bar-danger' role='progressbar' aria-valuenow='60' aria-valuemin='0' aria-valuemax='100' style='width: 100%;'>";
                                                            echo "{$frow['Probability']}";
                                                        echo '</div>';
                                                    echo '</div></td>';
                                                    echo "<td>{$frow['DateOfClosure']}</td>";
                                                    echo "<td>{$frow['ExpectedClosure']}</td>";
                                                    echo "<td></td>";
                                                    echo '</tr>';
                                    
                                                    $totalrevenue=$totalrevenue+$revenue;
                                                    $totalmargin=$totalmargin+$margin;
                                    
                                                } elseif($probability == "50")
                                                {
                                                    echo '<tr>';
                                                    echo "<td>".$fmi."</td>";
                                                    echo "<td>{$frow['Company']}</td>";
                                                    echo "<td>{$frow['Products']}</td>";
                                                    /*echo "<td>{$frow['Detail']}</td>";*/
                                                    echo '<td><span class="more">'.$detail.'</span></td>';
                                                    echo "<td>{$frow['Revenue']}</td>";
                                                    echo "<td>{$frow['Margin']}</td>";
                                                    echo "<td>{$frow['Stage']}</td>";
                                                    echo '<td><div class="progress progress-sm progress-half-rounded m-none mt-xs light">';
                                                        echo "<div class='progress-bar progress-bar-warning' role='progressbar' aria-valuenow='60' aria-valuemin='0' aria-valuemax='100' style='width: 100%;'>";
                                                            echo "{$frow['Probability']}";
                                                        echo '</div>';
                                                    echo '</div></td>';
                                                    echo "<td>{$frow['DateOfClosure']}</td>";
                                                    echo "<td>{$frow['ExpectedClosure']}</td>";
                                                    echo "<td></td>";
                                                    echo '</tr>';
                                    
                                                    $totalrevenue=$totalrevenue+$revenue;
                                                    $totalmargin=$totalmargin+$margin;
                                    
                                                } elseif($probability == "75")
                                                {
                                                    echo '<tr>';
                                                    echo "<td>".$fmi."</td>";
                                                    echo "<td>{$frow['Company']}</td>";
                                                    echo "<td>{$frow['Products']}</td>";
                                                    /*echo "<td>{$frow['Detail']}</td>";*/
                                                    echo '<td><span class="more">'.$detail.'</span></td>';
                                                    echo "<td>{$frow['Revenue']}</td>";
                                                    echo "<td>{$frow['Margin']}</td>";
                                                    echo "<td>{$frow['Stage']}</td>";
                                                    echo '<td><div class="progress progress-sm progress-half-rounded m-none mt-xs light">';
                                                        echo "<div class='progress-bar progress-bar-warning' role='progressbar' aria-valuenow='60' aria-valuemin='0' aria-valuemax='100' style='width: 100%;'>";
                                                            echo "{$frow['Probability']}";
                                                        echo '</div>';
                                                    echo '</div></td>';
                                                    echo "<td>{$frow['DateOfClosure']}</td>";
                                                    echo "<td>{$frow['ExpectedClosure']}</td>";
                                                    echo "<td></td>";
                                                    echo '</tr>';
                                                    $totalrevenue=$totalrevenue+$revenue;
                                                    $totalmargin=$totalmargin+$margin;
                                    
                                                } elseif($probability == "100")
                                                {
                                                    echo '<tr>';
                                                    echo "<td>".$fmi."</td>";
                                                    echo "<td>{$frow['Company']}</td>";
                                                    echo "<td>{$frow['Products']}</td>";
                                                    /*echo "<td>{$frow['Detail']}</td>";*/
                                                    echo '<td><span class="more">'.$detail.'</span></td>';
                                                    echo "<td>{$frow['Revenue']}</td>";
                                                    echo "<td>{$frow['Margin']}</td>";
                                                    echo "<td>{$frow['Stage']}</td>";
                                                    echo '<td><div class="progress progress-sm progress-half-rounded m-none mt-xs light">';
                                                        echo "<div class='progress-bar progress-bar-primary' role='progressbar' aria-valuenow='60' aria-valuemin='0' aria-valuemax='100' style='width: 100%;'>";
                                                            echo "{$frow['Probability']}";
                                                        echo '</div>';
                                                    echo '</div></td>';
                                                    echo "<td>{$frow['DateOfClosure']}</td>";
                                                    echo "<td>{$frow['ExpectedClosure']}</td>";
                                                    echo "<td>{$closuredate_final}</td>";
                                                    echo "<td></td>";
                                                    echo '</tr>';
                                    
                                                    $totalrevenue=$totalrevenue+$revenue;
                                                    $totalmargin=$totalmargin+$margin;
                                    
                                                }
                                                
                                    
                                            }
                                            echo '</tbody>';
                                    echo '</table>';

                                    echo '<table class="table table-striped table-responsive-md table-bordered"  >';
                                     echo '<tr>';
                                              
                                                echo "<th>Total</th>";
                                                echo "<th>Revenue: ".$totalrevenue."</th>";
                                            echo "<th>Margin: ".$totalmargin."</th>";
                                               
                                    
                                                echo '</tr>';
                                    echo '</table>';

                                    ?>

                          </div>
                          <div class="modal-footer">
                             <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                          </div>
                        </div>
                      </div>
                    </div>
                    <!-- End Modal -->

                     <!-- Today Funnel Modal -->
 <!-- Modal -->
                    <div class="modal fade example12-modal-lg" id="todayfunnelmodal"
                      aria-hidden="true" aria-labelledby="exampleModalTitle" role="dialog"
                      tabindex="-1">
                      <div class="modal-dialog modal-simple modal-lg">
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">×</span>
                            </button>
                            <h4 class="modal-title">Today's Funnel</h4>
                          </div>
                          <div class="modal-body">
                           <?php

                                     $today=date("Y-m-d");
                                    //$cdate=date('h:i a', strtotime($followuptime));

                                    

                                    $fetfunnel=mysqli_query($dbc,"select * from `funnel` where  DATE(ModificationDetail)='$today'  order by `ModificationDetail` desc");
                                    
                                    echo '<table class="table table-striped table-responsive table-bordered example">';
                                        echo '<thead>';
                                            echo '<tr>';
                                                echo '<th>Sl No.</th>';
                                                echo '<th>Company</th>';
                                                echo '<th>Product</th>';
                                                echo '<th>Detail</th>';
                                                echo '<th>Revenue</th>';
                                                echo '<th>Margin</th>';
                                                echo '<th>Stage</th>';
                                                echo '<th>Probability</th>';
                                                echo '<th>Expected Date Of Closure</th>';
                                                echo '<th>Expected Closure Month</th>';
                                                echo '<th>Date of Closure</th>';
                                            echo '</tr>';
                                        echo '</thead>';
                                        echo '<tbody>';
                                        $totalrevenue=0;
                                        $totalmargin=0;
                                        $fmi=0;
                                            while($frow=mysqli_fetch_assoc($fetfunnel))
                                            {
                                                global $totalrevenue;
                                                global $totalmargin;
                                    
                                                global $fmi;
                                    
                                                $fmi=$fmi+1;
                                    
                                                $company=$frow['Company'];
                                                $product=$frow['Products'];
                                                $detail=$frow['Detail'];
                                                $revenue=$frow['Revenue'];
                                                $margin=$frow['Margin'];
                                                $stage=$frow['Stage'];
                                                $probability=$frow['Probability'];
                                                $dateofclosure=$frow['DateOfClosure'];
                                                $closuremonth=$frow['ExpectedClosure'];
                                                $closuredate=$frow['ModificationDetail'];
                                                
                                                $closuredate_final=date("Y-m-d", strtotime($closuredate));
                                    
                                                if($stage == "Won")
                                                {
                                                    echo '<tr>';
                                                    echo "<td>".$fmi."</td>";
                                                    echo "<td>{$frow['Company']}</td>";
                                                    echo "<td>{$frow['Products']}</td>";
                                                    /*echo "<td>{$frow['Detail']}</td>";*/
                                                    echo '<td><span class="more">'.$detail.'</span></td>';
                                                    echo "<td>{$frow['Revenue']}</td>";
                                                    echo "<td>{$frow['Margin']}</td>";
                                                    echo "<td>{$frow['Stage']}</td>";
                                                    echo '<td><div class="progress progress-sm progress-half-rounded m-none mt-xs light">';
                                                        echo "<div class='progress-bar progress-bar-primary' role='progressbar' aria-valuenow='60' aria-valuemin='0' aria-valuemax='100' style='width: 100%;'>";
                                                            echo "{$frow['Probability']}";
                                                        echo '</div>';
                                                    echo '</div></td>';
                                                    echo "<td>{$frow['DateOfClosure']}</td>";
                                                    echo "<td>{$frow['ExpectedClosure']}</td>";
                                                    echo "<td>{$closuredate_final}</td>";
                                                    echo '</tr>';
                                    
                                                    $totalrevenue=$totalrevenue+$revenue;
                                                    $totalmargin=$totalmargin+$margin;
                                    
                                                }elseif($probability == "0")
                                                {
                                                    echo '<tr>';
                                                    echo "<td>".$fmi."</td>";
                                                    echo "<td>{$frow['Company']}</td>";
                                                    echo "<td>{$frow['Products']}</td>";
                                                    /*echo "<td>{$frow['Detail']}</td>";*/
                                                    echo '<td><span class="more">'.$detail.'</span></td>';
                                                    echo "<td>{$frow['Revenue']}</td>";
                                                    echo "<td>{$frow['Margin']}</td>";
                                                    echo "<td>{$frow['Stage']}</td>";
                                                    echo '<td><div class="progress progress-sm progress-half-rounded m-none mt-xs light">';
                                                        echo "<div class='progress-bar progress-bar-danger' role='progressbar' aria-valuenow='60' aria-valuemin='0' aria-valuemax='100' style='width: 100%;'>";
                                                            echo "{$frow['Probability']}";
                                                        echo '</div>';
                                                    echo '</div></td>';
                                                    echo "<td>{$frow['DateOfClosure']}</td>";
                                                    echo "<td>{$frow['ExpectedClosure']}</td>";
                                                    echo "<td></td>";
                                                    
                                                    echo '</tr>';
                                    
                                                    $totalrevenue=$totalrevenue+$revenue;
                                                    $totalmargin=$totalmargin+$margin;
                                    
                                                } elseif($probability == "25")
                                                {
                                                    echo '<tr>';
                                                    echo "<td>".$fmi."</td>";
                                                    echo "<td>{$frow['Company']}</td>";
                                                    echo "<td>{$frow['Products']}</td>";
                                                    /*echo "<td>{$frow['Detail']}</td>";*/
                                                    echo '<td><span class="more">'.$detail.'</span></td>';
                                                    echo "<td>{$frow['Revenue']}</td>";
                                                    echo "<td>{$frow['Margin']}</td>";
                                                    echo "<td>{$frow['Stage']}</td>";
                                                    echo '<td><div class="progress progress-sm progress-half-rounded m-none mt-xs light">';
                                                        echo "<div class='progress-bar progress-bar-danger' role='progressbar' aria-valuenow='60' aria-valuemin='0' aria-valuemax='100' style='width: 100%;'>";
                                                            echo "{$frow['Probability']}";
                                                        echo '</div>';
                                                    echo '</div></td>';
                                                    echo "<td>{$frow['DateOfClosure']}</td>";
                                                    echo "<td>{$frow['ExpectedClosure']}</td>";
                                                    echo "<td></td>";
                                                    echo '</tr>';
                                    
                                                    $totalrevenue=$totalrevenue+$revenue;
                                                    $totalmargin=$totalmargin+$margin;
                                    
                                                } elseif($probability == "50")
                                                {
                                                    echo '<tr>';
                                                    echo "<td>".$fmi."</td>";
                                                    echo "<td>{$frow['Company']}</td>";
                                                    echo "<td>{$frow['Products']}</td>";
                                                    /*echo "<td>{$frow['Detail']}</td>";*/
                                                    echo '<td><span class="more">'.$detail.'</span></td>';
                                                    echo "<td>{$frow['Revenue']}</td>";
                                                    echo "<td>{$frow['Margin']}</td>";
                                                    echo "<td>{$frow['Stage']}</td>";
                                                    echo '<td><div class="progress progress-sm progress-half-rounded m-none mt-xs light">';
                                                        echo "<div class='progress-bar progress-bar-warning' role='progressbar' aria-valuenow='60' aria-valuemin='0' aria-valuemax='100' style='width: 100%;'>";
                                                            echo "{$frow['Probability']}";
                                                        echo '</div>';
                                                    echo '</div></td>';
                                                    echo "<td>{$frow['DateOfClosure']}</td>";
                                                    echo "<td>{$frow['ExpectedClosure']}</td>";
                                                    echo "<td></td>";
                                                    echo '</tr>';
                                    
                                                    $totalrevenue=$totalrevenue+$revenue;
                                                    $totalmargin=$totalmargin+$margin;
                                    
                                                } elseif($probability == "75")
                                                {
                                                    echo '<tr>';
                                                    echo "<td>".$fmi."</td>";
                                                    echo "<td>{$frow['Company']}</td>";
                                                    echo "<td>{$frow['Products']}</td>";
                                                    /*echo "<td>{$frow['Detail']}</td>";*/
                                                    echo '<td><span class="more">'.$detail.'</span></td>';
                                                    echo "<td>{$frow['Revenue']}</td>";
                                                    echo "<td>{$frow['Margin']}</td>";
                                                    echo "<td>{$frow['Stage']}</td>";
                                                    echo '<td><div class="progress progress-sm progress-half-rounded m-none mt-xs light">';
                                                        echo "<div class='progress-bar progress-bar-warning' role='progressbar' aria-valuenow='60' aria-valuemin='0' aria-valuemax='100' style='width: 100%;'>";
                                                            echo "{$frow['Probability']}";
                                                        echo '</div>';
                                                    echo '</div></td>';
                                                    echo "<td>{$frow['DateOfClosure']}</td>";
                                                    echo "<td>{$frow['ExpectedClosure']}</td>";
                                                    echo "<td></td>";
                                                    echo '</tr>';
                                                    $totalrevenue=$totalrevenue+$revenue;
                                                    $totalmargin=$totalmargin+$margin;
                                    
                                                } elseif($probability == "100")
                                                {
                                                    echo '<tr>';
                                                    echo "<td>".$fmi."</td>";
                                                    echo "<td>{$frow['Company']}</td>";
                                                    echo "<td>{$frow['Products']}</td>";
                                                    /*echo "<td>{$frow['Detail']}</td>";*/
                                                    echo '<td><span class="more">'.$detail.'</span></td>';
                                                    echo "<td>{$frow['Revenue']}</td>";
                                                    echo "<td>{$frow['Margin']}</td>";
                                                    echo "<td>{$frow['Stage']}</td>";
                                                    echo '<td><div class="progress progress-sm progress-half-rounded m-none mt-xs light">';
                                                        echo "<div class='progress-bar progress-bar-primary' role='progressbar' aria-valuenow='60' aria-valuemin='0' aria-valuemax='100' style='width: 100%;'>";
                                                            echo "{$frow['Probability']}";
                                                        echo '</div>';
                                                    echo '</div></td>';
                                                    echo "<td>{$frow['DateOfClosure']}</td>";
                                                    echo "<td>{$frow['ExpectedClosure']}</td>";
                                                    echo "<td>{$closuredate_final}</td>";
                                                    echo "<td></td>";
                                                    echo '</tr>';
                                    
                                                    $totalrevenue=$totalrevenue+$revenue;
                                                    $totalmargin=$totalmargin+$margin;
                                    
                                                }
                                                
                                    
                                            }
                                           echo '</tbody>';
                                    echo '</table>';

                                    echo '<table class="table table-striped table-responsive-md table-bordered"  >';
                                     echo '<tr>';
                                              
                                                echo "<th>Total</th>";
                                                echo "<th>Revenue: ".$totalrevenue."</th>";
                                            echo "<th>Margin: ".$totalmargin."</th>";
                                               
                                    
                                                echo '</tr>';
                                    echo '</table>';
                                    ?>

                          </div>
                          <div class="modal-footer">
                             <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                          </div>
                        </div>
                      </div>
                    </div>
                    <!-- End Modal -->
   <div class="modal fade example12-modal-lg" id="exampleTabs" aria-hidden="true" aria-labelledby="exampleModalTabs"
                      role="dialog" tabindex="-1">
                      <div class="modal-dialog modal-simple modal-lg">
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">×</span>
                            </button>
                            <h4 class="modal-title" id="exampleModalTabs">Funnel Probability</h4>
                          </div>

                          <ul class="nav nav-tabs nav-tabs-line" role="tablist">
                            <li class="nav-item" role="presentation"><a class="nav-link active" data-toggle="tab" href="#exampleLine1"
                                aria-controls="exampleLine1" role="tab">25%</a></li>
                            <li class="nav-item" role="presentation"><a class="nav-link" data-toggle="tab" href="#exampleLine2"
                                aria-controls="exampleLine2" role="tab">50%</a></li>
                            <li class="nav-item" role="presentation"><a class="nav-link" data-toggle="tab" href="#exampleLine3"
                                aria-controls="exampleLine3" role="tab">75%</a></li>
                            <li class="nav-item" role="presentation"><a class="nav-link" data-toggle="tab" href="#exampleLine4"
                                aria-controls="exampleLine4" role="tab">100%</a></li>
                                <li class="nav-item" role="presentation"><a class="nav-link" data-toggle="tab" href="#exampleLine5"
                                aria-controls="exampleLine5" role="tab">0-100%</a></li>
                          </ul>

                          <div class="modal-body">
                            <div class="tab-content">
                              <div class="tab-pane active" id="exampleLine1" role="tabpanel">
                                <?php
                                                $fetfunnel=mysqli_query($dbc,"select * from `funnel` where  `Probability`='25' order by `ModificationDetail` desc");
                                                
                                                echo '<table class="table table-striped table-responsive table-bordered example">';
                                                    echo '<thead>';
                                                        echo '<tr>';
                                                            echo '<th>Sl No.</th>';
                                                            echo '<th>Company</th>';
                                                            echo '<th>Product</th>';
                                                            echo '<th>Detail</th>';
                                                            echo '<th>Revenue</th>';
                                                            echo '<th>Margin</th>';
                                                            echo '<th>Stage</th>';
                                                            echo '<th>Probability</th>';
                                                            echo '<th>Expected Date Of Closure</th>';
                                                            echo '<th>Expected Closure Month</th>';
                                                            echo '<th>Date of Closure</th>';
                                                        echo '</tr>';
                                                    echo '</thead>';
                                                    echo '<tbody>';
                                                       $totalrevenue=0;
                                                    $totalmargin=0;
                                                    $f25i=0;
                                                        while($frow=mysqli_fetch_assoc($fetfunnel))
                                                        {
                                                            global $totalrevenue;
                                                            global $totalmargin;
                                                
                                                            global $f25i;
                                                            $f25i=$f25+1;
                                                
                                                            $company=$frow['Company'];
                                                            $product=$frow['Products'];
                                                            $detail=$frow['Detail'];
                                                            $revenue=$frow['Revenue'];
                                                            $margin=$frow['Margin'];
                                                            $stage=$frow['Stage'];
                                                            $probability=$frow['Probability'];
                                                            $dateofclosure=$frow['DateOfClosure'];
                                                            $closuremonth=$frow['ExpectedClosure'];
                                                            $closuredate=$frow['ModificationDetail'];
                                                            
                                                            $closuredate_final=date("Y-m-d", strtotime($closuredate));
                                                
                                                            
                                                                echo '<tr>';
                                                                echo "<td>".$f25i."</td>";
                                                                echo "<td>{$frow['Company']}</td>";
                                                                echo "<td>{$frow['Products']}</td>";
                                                                /*echo "<td>{$frow['Detail']}</td>";*/
                                                                echo '<td><span class="more">'.$detail.'</span></td>';
                                                                echo "<td>{$frow['Revenue']}</td>";
                                                                echo "<td>{$frow['Margin']}</td>";
                                                                echo "<td>{$frow['Stage']}</td>";
                                                                echo '<td><div class="progress progress-sm progress-half-rounded m-none mt-xs light">';
                                                                    echo "<div class='progress-bar progress-bar-danger' role='progressbar' aria-valuenow='60' aria-valuemin='0' aria-valuemax='100' style='width: 25%;'>";
                                                                        echo "{$frow['Probability']}";
                                                                    echo '</div>';
                                                                echo '</div></td>';
                                                                echo "<td>{$frow['DateOfClosure']}</td>";
                                                                echo "<td>{$frow['ExpectedClosure']}</td>";
                                                                echo "<td>{$closuredate_final}</td>";
                                                                echo '</tr>';
                                                            
                                                            $totalrevenue=$totalrevenue+$revenue;
                                                                $totalmargin=$totalmargin+$margin;
                                                            
                                                            
                                                
                                                        }
                                                        echo '<tr>';
                                                        echo '<th>Total</th>';
                                                        echo '<th></th>';
                                                        echo '<th></th>';
                                                        echo '<th></th>';
                                                        echo '<th>';echo $totalrevenue;echo '</th>';
                                                        echo '<th>';echo $totalmargin;echo '</th>';
                                                        echo '<th></th>';
                                                        echo '<th></th>';
                                                        echo '<th></th>';
                                                        echo '<th></th>';
                                                        echo '<th></th>';
                                                        echo '</tr>';
                                                    echo '</tbody>';
                                                echo '</table>';
                                                ?>
                              </div>

                              <div class="tab-pane" id="exampleLine2" role="tabpanel">
                               <?php
                                                $fetfunnel=mysqli_query($dbc,"select * from `funnel` where  `Probability`='50' order by `ModificationDetail` desc ");
                                                
                                                echo '<table class="table table-striped table-responsive table-bordered example">';
                                                    echo '<thead>';
                                                        echo '<tr>';
                                                            echo '<th>Sl No.</th>';
                                                            echo '<th>Company</th>';
                                                            echo '<th>Product</th>';
                                                            echo '<th>Detail</th>';
                                                            echo '<th>Revenue</th>';
                                                            echo '<th>Margin</th>';
                                                            echo '<th>Stage</th>';
                                                            echo '<th>Probability</th>';
                                                            echo '<th>Expected Date Of Closure</th>';
                                                            echo '<th>Expected Closure Month</th>';
                                                            echo '<th>Date of Closure</th>';
                                                        echo '</tr>';
                                                    echo '</thead>';
                                                    echo '<tbody>';
                                                        $totalrevenue=0;
                                                    $totalmargin=0;
                                                    $f50i=0;
                                                        while($frow=mysqli_fetch_assoc($fetfunnel))
                                                        {
                                                            global $totalrevenue;
                                                            global $totalmargin;
                                                
                                                            global $f50i;
                                                            $f50i=$f50i+1;
                                                
                                                            $company=$frow['Company'];
                                                            $product=$frow['Products'];
                                                            $detail=$frow['Detail'];
                                                            $revenue=$frow['Revenue'];
                                                            $margin=$frow['Margin'];
                                                            $stage=$frow['Stage'];
                                                            $probability=$frow['Probability'];
                                                            $dateofclosure=$frow['DateOfClosure'];
                                                            $closuremonth=$frow['ExpectedClosure'];
                                                            $closuredate=$frow['ModificationDetail'];
                                                            
                                                            $closuredate_final=date("Y-m-d", strtotime($closuredate));
                                                
                                                            
                                                                echo '<tr>';
                                                                echo "<td>".$f50i."</td>";
                                                                echo "<td>{$frow['Company']}</td>";
                                                                echo "<td>{$frow['Products']}</td>";
                                                                /*echo "<td>{$frow['Detail']}</td>";*/
                                                                echo '<td><span class="more">'.$detail.'</span></td>';
                                                                echo "<td>{$frow['Revenue']}</td>";
                                                                echo "<td>{$frow['Margin']}</td>";
                                                                echo "<td>{$frow['Stage']}</td>";
                                                                echo '<td><div class="progress progress-sm progress-half-rounded m-none mt-xs light">';
                                                                    echo "<div class='progress-bar progress-bar-warning' role='progressbar' aria-valuenow='60' aria-valuemin='0' aria-valuemax='100' style='width: 50%;'>";
                                                                        echo "{$frow['Probability']}";
                                                                    echo '</div>';
                                                                echo '</div></td>';
                                                                echo "<td>{$frow['DateOfClosure']}</td>";
                                                                echo "<td>{$frow['ExpectedClosure']}</td>";
                                                                echo "<td>{$closuredate_final}</td>";
                                                                echo '</tr>';
                                                            
                                                          $totalrevenue=$totalrevenue+$revenue;
                                                                $totalmargin=$totalmargin+$margin;
                                                            
                                                            
                                                
                                                        }
                                                        echo '<tr>';
                                                        echo '<th>Total</th>';
                                                        echo '<th></th>';
                                                        echo '<th></th>';
                                                        echo '<th></th>';
                                                        echo '<th>';echo $totalrevenue;echo '</th>';
                                                        echo '<th>';echo $totalmargin;echo '</th>';
                                                        echo '<th></th>';
                                                        echo '<th></th>';
                                                        echo '<th></th>';
                                                        echo '<th></th>';
                                                        echo '<th></th>';
                                                        echo '</tr>';
                                                    echo '</tbody>';
                                                echo '</table>';
                                                ?>
                              </div>

                              <div class="tab-pane" id="exampleLine3" role="tabpanel">
                               <?php
                                                $fetfunnel=mysqli_query($dbc,"select * from `funnel` where  `Probability`='75' order by `ModificationDetail` desc");
                                                
                                                echo '<table class="table table-striped table-responsive table-bordered example">';
                                                    echo '<thead>';
                                                        echo '<tr>';
                                                            echo '<th>Sl No.</th>';
                                                            echo '<th>Company</th>';
                                                            echo '<th>Product</th>';
                                                            echo '<th>Detail</th>';
                                                            echo '<th>Revenue</th>';
                                                            echo '<th>Margin</th>';
                                                            echo '<th>Stage</th>';
                                                            echo '<th>Probability</th>';
                                                            echo '<th>Expected Date Of Closure</th>';
                                                            echo '<th>Expected Closure Month</th>';
                                                            echo '<th>Date of Closure</th>';
                                                        echo '</tr>';
                                                    echo '</thead>';
                                                    echo '<tbody>';
                                                    $totalrevenue=0;
                                                    $totalmargin=0;
                                                    $f75i=0;
                                                        while($frow=mysqli_fetch_assoc($fetfunnel))
                                                        {
                                                            global $totalrevenue;
                                                            global $totalmargin;
                                                
                                                            global $f75i;
                                                            $f75i=$f75i+1;
                                                
                                                
                                                            $company=$frow['Company'];
                                                            $product=$frow['Products'];
                                                            $detail=$frow['Detail'];
                                                            $revenue=$frow['Revenue'];
                                                            $margin=$frow['Margin'];
                                                            $stage=$frow['Stage'];
                                                            $probability=$frow['Probability'];
                                                            $dateofclosure=$frow['DateOfClosure'];
                                                            $closuremonth=$frow['ExpectedClosure'];
                                                            $closuredate=$frow['ModificationDetail'];
                                                            
                                                            $closuredate_final=date("Y-m-d", strtotime($closuredate));
                                                
                                                            
                                                                echo '<tr>';
                                                                echo "<td>".$f75i."</td>";
                                                                echo "<td>{$frow['Company']}</td>";
                                                                echo "<td>{$frow['Products']}</td>";
                                                                /*echo "<td>{$frow['Detail']}</td>";*/
                                                                echo '<td><span class="more">'.$detail.'</span></td>';
                                                                echo "<td>{$frow['Revenue']}</td>";
                                                                echo "<td>{$frow['Margin']}</td>";
                                                                echo "<td>{$frow['Stage']}</td>";
                                                                echo '<td><div class="progress progress-sm progress-half-rounded m-none mt-xs light">';
                                                                    echo "<div class='progress-bar progress-bar' role='progressbar' aria-valuenow='60' aria-valuemin='0' aria-valuemax='100' style='width: 75%;background-color: #3CB371'>";
                                                                        echo "{$frow['Probability']}";
                                                                    echo '</div>';
                                                                echo '</div></td>';
                                                                echo "<td>{$frow['DateOfClosure']}</td>";
                                                                echo "<td>{$frow['ExpectedClosure']}</td>";
                                                                echo "<td>{$closuredate_final}</td>";
                                                                echo '</tr>';
                                                            
                                                                $totalrevenue=$totalrevenue+$revenue;
                                                                $totalmargin=$totalmargin+$margin;
                                                            
                                                            
                                                
                                                        }
                                                        echo '<tr>';
                                                        echo '<th>Total</th>';
                                                        echo '<th></th>';
                                                        echo '<th></th>';
                                                        echo '<th></th>';
                                                        echo '<th>';echo $totalrevenue;echo '</th>';
                                                        echo '<th>';echo $totalmargin;echo '</th>';
                                                        echo '<th></th>';
                                                        echo '<th></th>';
                                                        echo '<th></th>';
                                                        echo '<th></th>';
                                                        echo '<th></th>';
                                                        echo '</tr>';
                                                    echo '</tbody>';
                                                echo '</table>';
                                                ?>
                              </div>

                              <div class="tab-pane" id="exampleLine4" role="tabpanel">
                               <?php
                                                $fetfunnel=mysqli_query($dbc,"select * from `funnel` where  `Probability`='100' order by `ModificationDetail` desc");
                                                
                                                echo '<table class="table table-striped table-responsive table-bordered example">';
                                                    echo '<thead>';
                                                        echo '<tr>';
                                                            echo '<th>Sl No.</th>';
                                                            echo '<th>Company</th>';
                                                            echo '<th>Product</th>';
                                                            echo '<th>Detail</th>';
                                                            echo '<th>Revenue</th>';
                                                            echo '<th>Margin</th>';
                                                            echo '<th>Stage</th>';
                                                            echo '<th>Probability</th>';
                                                            echo '<th>Expected Date Of Closure</th>';
                                                            echo '<th>Expected Closure Month</th>';
                                                            echo '<th>Date of Closure</th>';
                                                        echo '</tr>';
                                                    echo '</thead>';
                                                    echo '<tbody>';
                                                    $totalrevenue=0;
                                                    $totalmargin=0;
                                                    $f100i=0;
                                                        while($frow=mysqli_fetch_assoc($fetfunnel))
                                                        {
                                                            global $f100i;
                                                            $f100i=$f100i+1;
                                                            $company=$frow['Company'];
                                                            $product=$frow['Products'];
                                                            $detail=$frow['Detail'];
                                                            $revenue=$frow['Revenue'];
                                                            $margin=$frow['Margin'];
                                                            $stage=$frow['Stage'];
                                                            $probability=$frow['Probability'];
                                                            $dateofclosure=$frow['DateOfClosure'];
                                                            $closuremonth=$frow['ExpectedClosure'];
                                                            $closuredate=$frow['ModificationDetail'];
                                                            
                                                            $closuredate_final=date("Y-m-d", strtotime($closuredate));
                                                
                                                            global $totalmargin;
                                                            global $totalrevenue;
                                                
                                                            
                                                                echo '<tr>';
                                                                echo "<td>".$f100i."</td>";
                                                                echo "<td>{$frow['Company']}</td>";
                                                                echo "<td>{$frow['Products']}</td>";
                                                                /*echo "<td>{$frow['Detail']}</td>";*/
                                                                echo '<td><span class="more">'.$detail.'</span></td>';
                                                                echo "<td>{$frow['Revenue']}</td>";
                                                                echo "<td>{$frow['Margin']}</td>";
                                                                echo "<td>{$frow['Stage']}</td>";
                                                                echo '<td><div class="progress progress-sm progress-half-rounded m-none mt-xs light">';
                                                                    echo "<div class='progress-bar progress-bar-primary' role='progressbar' aria-valuenow='60' aria-valuemin='0' aria-valuemax='100' style='width: 100%;'>";
                                                                        echo "{$frow['Probability']}";
                                                                    echo '</div>';
                                                                echo '</div></td>';
                                                                echo "<td>{$frow['DateOfClosure']}</td>";
                                                                echo "<td>{$frow['ExpectedClosure']}</td>";
                                                                echo "<td>{$closuredate_final}</td>";
                                                                echo '</tr>';
                                                                $totalrevenue=$totalrevenue+$revenue;
                                                                $totalmargin=$totalmargin+$margin;
                                                            
                                                            
                                                
                                                        }
                                                        echo '<tr>';
                                                        echo '<th>Total</th>';
                                                        echo '<th></th>';
                                                        echo '<th></th>';
                                                        echo '<th></th>';
                                                        echo '<th>';echo $totalrevenue;echo '</th>';
                                                        echo '<th>';echo $totalmargin;echo '</th>';
                                                        echo '<th></th>';
                                                        echo '<th></th>';
                                                        echo '<th></th>';
                                                        echo '<th></th>';
                                                        
                                                        echo '<th></th>';
                                                        echo '</tr>';
                                                    echo '</tbody>';
                                                echo '</table>';
                                                ?>
                              </div>
                               <div class="tab-pane" id="exampleLine5" role="tabpanel">
                               <?php
                                                $fetfunnel=mysqli_query($dbc,"select * from `funnel` order by `ModificationDetail` desc ");
                                                
                                                echo '<table class="table table-striped table-responsive table-bordered example">';
                                                    echo '<thead>';
                                                        echo '<tr>';
                                                            echo '<th>Sl No.</th>';
                                                            echo '<th>Company</th>';
                                                            echo '<th>Product</th>';
                                                            echo '<th>Detail</th>';
                                                            echo '<th>Revenue</th>';
                                                            echo '<th>Margin</th>';
                                                            echo '<th>Stage</th>';
                                                            echo '<th>Probability</th>';
                                                            echo '<th>Expected Date Of Closure</th>';
                                                            echo '<th>Expected Closure Month</th>';
                                                            echo '<th>Date of Closure</th>';
                                                        echo '</tr>';
                                                    echo '</thead>';
                                                    echo '<tbody>';
                                                        $totalrevenue=0;
                                                    $totalmargin=0;
                                                    $falli=0;
                                                        while($frow=mysqli_fetch_assoc($fetfunnel))
                                                        {
                                                            global $totalrevenue;
                                                            global $totalmargin;
                                                
                                                            global $falli;
                                                            $falli=$falli+1;
                                                
                                                            $company=$frow['Company'];
                                                            $product=$frow['Products'];
                                                            $detail=$frow['Detail'];
                                                            $revenue=$frow['Revenue'];
                                                            $margin=$frow['Margin'];
                                                            $stage=$frow['Stage'];
                                                            $probability=$frow['Probability'];
                                                            $dateofclosure=$frow['DateOfClosure'];
                                                            $closuremonth=$frow['ExpectedClosure'];
                                                            $closuredate=$frow['ModificationDetail'];
                                                            
                                                            $closuredate_final=date("Y-m-d", strtotime($closuredate));
                                                
                                                            
                                                                echo '<tr>';
                                                                echo "<td>".$falli."</td>";
                                                                echo "<td>{$frow['Company']}</td>";
                                                                echo "<td>{$frow['Products']}</td>";
                                                                /*echo "<td>{$frow['Detail']}</td>";*/
                                                                echo '<td><span class="more">'.$detail.'</span></td>';
                                                                echo "<td>{$frow['Revenue']}</td>";
                                                                echo "<td>{$frow['Margin']}</td>";
                                                                echo "<td>{$frow['Stage']}</td>";
                                                                echo '<td><div class="progress progress-sm progress-half-rounded m-none mt-xs light">';
                                                                    echo "<div class='progress-bar ' role='progressbar' aria-valuenow='60' aria-valuemin='0' aria-valuemax='100' style='width: 100%;background-color:#20B2AA '>";
                                                                        echo "{$frow['Probability']}";
                                                                    echo '</div>';
                                                                echo '</div></td>';
                                                                echo "<td>{$frow['DateOfClosure']}</td>";
                                                                echo "<td>{$frow['ExpectedClosure']}</td>";
                                                                echo "<td>{$closuredate_final}</td>";
                                                                echo '</tr>';
                                                            
                                                            $totalrevenue=$totalrevenue+$revenue;
                                                                $totalmargin=$totalmargin+$margin;
                                                            
                                                            
                                                
                                                        }
                                                        echo '<tr>';
                                                        echo '<th>Total</th>';
                                                        echo '<th></th>';
                                                        echo '<th></th>';
                                                        echo '<th></th>';
                                                        echo '<th>';echo $totalrevenue;echo '</th>';
                                                        echo '<th>';echo $totalmargin;echo '</th>';
                                                        echo '<th></th>';
                                                        echo '<th></th>';
                                                        echo '<th></th>';
                                                        echo '<th></th>';
                                                        echo '<th></th>';
                                                        echo '</tr>';
                                                    echo '</tbody>';
                                                echo '</table>';
                                                ?>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>


            <!-- End Widget Linearea Four -->
          </div>

    </div>
    <!-- End Page -->
  </div>
</div>


    <!-- Footer -->
    <?php include "includes/footer.php"; ?>
    <!-- Core  -->

    <script src="../../assets/global/vendor/babel-external-helpers/babel-external-helpers.js"></script>
    <script src="../../assets/global/vendor/jquery/jquery.js"></script>
    <script src="../../assets/global/vendor/popper-js/umd/popper.min.js"></script>
    <script src="../../assets/global/vendor/bootstrap/bootstrap.js"></script>
    <script src="../../assets/global/vendor/animsition/animsition.js"></script>
    <script src="../../assets/global/vendor/mousewheel/jquery.mousewheel.js"></script>
    <script src="../../assets/global/vendor/asscrollbar/jquery-asScrollbar.js"></script>
    <script src="../../assets/global/vendor/asscrollable/jquery-asScrollable.js"></script>
    <script src="../../assets/global/vendor/waves/waves.js"></script>

        <script src="../../assets/global/vendor/bootstrap-datepicker/bootstrap-datepicker.js"></script>
    
    <!-- Plugins -->
    <script src="../../assets/global/vendor/jquery-mmenu/jquery.mmenu.min.all.js"></script>
    <script src="../../assets/global/vendor/switchery/switchery.js"></script>
    <script src="../../assets/global/vendor/intro-js/intro.js"></script>
    <script src="../../assets/global/vendor/screenfull/screenfull.js"></script>
    <script src="../../assets/global/vendor/slidepanel/jquery-slidePanel.js"></script>
        <script src="../../assets/global/vendor/chartist/chartist.min.js"></script>
        <script src="../../assets/global/vendor/chartist-plugin-tooltip/chartist-plugin-tooltip.js"></script>
        <script src="../../assets/global/vendor/jvectormap/jquery-jvectormap.min.js"></script>
        <script src="../../assets/global/vendor/jvectormap/maps/jquery-jvectormap-world-mill-en.js"></script>
        <script src="../../assets/global/vendor/matchheight/jquery.matchHeight-min.js"></script>
        <script src="../../assets/global/vendor/peity/jquery.peity.min.js"></script>
    
            <script src="../../assets/global/vendor/multi-select/jquery.multi-select.js"></script>
                 <script src="../../assets/global/js/Plugin/multi-select.js"></script>
                  <script src="../../assets/global/js/Plugin/bootstrap-select.js"></script>
                     <script src="../../assets/global/vendor/bootstrap-select/bootstrap-select.js"></script>
    <!-- Scripts -->
    <script src="../../assets/global/js/Component.js"></script>
    <script src="../../assets/global/js/Plugin.js"></script>
    <script src="../../assets/global/js/Base.js"></script>
    <script src="../../assets/global/js/Config.js"></script>
    
    <script src="section/Menubar.js"></script>
    <script src="section/Sidebar.js"></script>
    <script src="section/PageAside.js"></script>
    <script src="section/GridMenu.js"></script>
    
    <!-- Config -->
    <script src="../../assets/global/js/config/colors.js"></script>
    <script src="../../assets/js/config/tour.js"></script>
    <script>Config.set('assets', '../assets');</script>
    
    <!-- Page -->
    <script src="../../assets/js/Site.js"></script>
    <script src="../../assets/global/js/Plugin/asscrollable.js"></script>
    <script src="../../assets/global/js/Plugin/slidepanel.js"></script>
    <script src="../../assets/global/js/Plugin/switchery.js"></script>
        <script src="../../assets/global/js/Plugin/matchheight.js"></script>
        <script src="../../assets/global/js/Plugin/jvectormap.js"></script>
        <script src="../../assets/global/js/Plugin/peity.js"></script>
    
        <script src="../../assets/examples/js/dashboard/v1.js"></script>
        <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
        <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
        <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>

                  <script >
          $(document).ready(function() {
    $('.example').DataTable();
} );
        </script>
<script>
            $(document).ready(function() {
    $('.example1').DataTable();
} );
        </script>
        <script>
            $(document).ready(function() {
    $('.example2').DataTable();
} );
        </script>
        <script>
            $(document).ready(function() {
    $('.example3').DataTable();
} );
        </script>
        <script>
            $(document).ready(function() {
    $('.example4').DataTable();
} );
        </script>
         <script>
            $(document).ready(function() {
    $('.example5').DataTable();
} );
        </script>
         <script>
            $(document).ready(function() {
    $('.example6').DataTable();
} );
        </script>
        <script>
            $(document).ready(function() {
    $('.example7').DataTable();
} );
        </script>
        <script>
            $(document).ready(function() {
    $('.example8').DataTable();
} );
        </script>
        <script>
            $(document).ready(function() {
    $('.example9').DataTable();
} );
        </script>
        <script>
            $(document).ready(function() {
    $('.example10').DataTable();
} );
        </script>
        <script>
            $(document).ready(function() {
    $('.example11').DataTable();
} );
        </script>
        <script>
            $(document).ready(function() {
    $('.example12').DataTable();
} );
        </script>
        <script>
            $(document).ready(function() {
    $('.example13').DataTable();
} );
        </script>
        <script>
            $(document).ready(function() {
    $('.example14').DataTable();
} );
        </script>
   <script src="select-option-afunnel.js"></script>
  <?php include "../../assets/twak/twak.php"; ?>
  </body>
</html>

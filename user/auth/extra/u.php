<?php
session_start();
error_reporting(E_PARSE | E_ERROR);
$id=$_SESSION['id'];
if(!$_SESSION['id'])
{
	echo '<script>location.replace("../../index.php");</script>';
}

require_once "../conn/conn.php";

?>



<!doctype html>
<html class="fixed">
	<head>

		<!-- Basic -->
		<meta charset="UTF-8">

		<title>Insert | Navikra Location</title>
		<meta name="keywords" content="Navikra Location" />
		<meta name="description" content="Navikra Location">
		<meta name="author" content="navikra.com">

		<!-- Mobile Metas -->
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

		<!-- Web Fonts  -->
		<link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800|Shadows+Into+Light" rel="stylesheet" type="text/css">

		<!-- Vendor CSS -->
		<link rel="stylesheet" href="../assets/vendor/bootstrap/css/bootstrap.css" />

		<link rel="stylesheet" href="../assets/vendor/font-awesome/css/font-awesome.css" />
		<link rel="stylesheet" href="../assets/vendor/magnific-popup/magnific-popup.css" />
		<link rel="stylesheet" href="../assets/vendor/bootstrap-datepicker/css/datepicker3.css" />

		<!-- Specific Page Vendor CSS -->
		<link rel="stylesheet" href="../assets/vendor/jquery-ui/css/ui-lightness/jquery-ui-1.10.4.custom.css" />
		<link rel="stylesheet" href="../assets/vendor/bootstrap-multiselect/bootstrap-multiselect.css" />
		<link rel="stylesheet" href="../assets/vendor/morris/morris.css" />

		<!-- Theme CSS -->
		<link rel="stylesheet" href="../assets/stylesheets/theme.css" />

		<!-- Skin CSS -->
		<link rel="stylesheet" href="../assets/stylesheets/skins/default.css" />

		<!-- Theme Custom CSS -->
		<link rel="stylesheet" href="../assets/stylesheets/theme-custom.css">

		<!-- Head Libs -->
		<script src="../assets/vendor/modernizr/modernizr.js"></script>

		<!-- search -->
		<!-- <script type="text/javascript" src="../assets/ajax/search.js" ></script> -->
		
		<script type="text/javascript" src="../assets/ajax/jquery-1.8.0.min.js"></script>
		<script type="text/javascript">
$(function(){
$(".search").keyup(function() 
{ 
var searchid = $(this).val();
var dataString = 'search='+ searchid;
if(searchid!='')
{
    $.ajax({
    type: "POST",
    url: "search.php",
    data: dataString,
    cache: false,
    success: function(html)
    {
    $("#result").html(html).show();
    }
    });
}return false;    
});

jQuery("#result").live("click",function(e){ 
    var $clicked = $(e.target);
    var $name = $clicked.find('.name').html();
    var decoded = $("<div/>").html($name).text();
    $('#searchid').val(decoded);
});
jQuery(document).live("click", function(e) { 
    var $clicked = $(e.target);
    if (! $clicked.hasClass("search")){
    jQuery("#result").fadeOut(); 
    }
});
$('#searchid').click(function(){
    jQuery("#result").fadeIn();
});
});
</script>
		

	</head>
	<body>
		<section class="body">

			<!-- start: header -->
			<header class="header">
				<div class="logo-container">
					<a href="../" class="logo">
						<img src="../assets/images/logo.png" height="35" alt="Navikra Location" />
					</a>
					<div class="visible-xs toggle-sidebar-left" data-toggle-class="sidebar-left-opened" data-target="html" data-fire-event="sidebar-left-opened">
						<i class="fa fa-bars" aria-label="Toggle sidebar"></i>
					</div>
				</div>
			
				<!-- start: search & user box -->
				<div class="header-right">
			
				
                    
                    <?php
                        $ad=mysql_query("select * from team where email='$id'");
                        while($r=mysql_fetch_assoc($ad))
                        {
                            $name=$r['name'];
                            $role=$r['desig'];
                        }
                    ?>
			
					<span class="separator"></span>
			
					<div id="userbox" class="userbox">
						<a href="#" data-toggle="dropdown">
							<figure class="profile-picture">
								<img src="../assets/images/!logged-user.jpg" alt="Joseph Doe" class="img-circle" data-lock-picture="../assets/images/!logged-user.jpg" />
							</figure>
							<div class="profile-info" data-lock-name="John Doe" data-lock-email="<?php echo $id; ?>">
								<span class="name"><?php echo $name; ?></span>
								<span class="role"><?php echo $role; ?></span>
							</div>
			
							<i class="fa custom-caret"></i>
						</a>
			
						<div class="dropdown-menu">
							<ul class="list-unstyled">
								<li class="divider"></li>
								<li>
									<a role="menuitem" tabindex="-1" href="#"><i class="fa fa-user"></i> My Profile</a>
								</li>
								<li>
									<!-- <a role="menuitem" tabindex="-1" href="#" data-lock-screen="true"><i class="fa fa-lock"></i> Lock Screen</a> -->
								</li>
								<li>
									<a role="menuitem" tabindex="-1" href="../../index.php"><i class="fa fa-power-off"></i> Logout</a>
								</li>
							</ul>
						</div>
					</div>
				</div>
				<!-- end: search & user box -->
			</header>
			<!-- end: header -->

			

			<div class="inner-wrapper">
				<!-- start: sidebar -->
				<aside id="sidebar-left" class="sidebar-left">
				
					<div class="sidebar-header">
						<div class="sidebar-title">
							Navigation
						</div>
						<div class="sidebar-toggle hidden-xs" data-toggle-class="sidebar-left-collapsed" data-target="html" data-fire-event="sidebar-left-toggle">
							<i class="fa fa-bars" aria-label="Toggle sidebar"></i>
						</div>
					</div>
				
					<div class="nano">
						<div class="nano-content">
							<nav id="menu" class="nav-main" role="navigation">
								<ul class="nav nav-main">
									<li >
										<a href="../opr/dashboard.php">
											<i class="fa fa-home" aria-hidden="true"></i>
											<span>Dashboard</span>
										</a>
									</li>
									<!-- <li>
										<a href="mailbox-folder.html">
											<span class="pull-right label label-primary">182</span>
											<i class="fa fa-envelope" aria-hidden="true"></i>
											<span>Mailbox</span>
										</a>
                                    </li> -->
                                    <li class="nav-active">
										<a href="../opr/insert.php">
											<!-- <span class="pull-right label label-primary">182</span> -->
											<i class="fa fa-plus" aria-hidden="true"></i>
											<span>Insert</span>
										</a>
                                    </li>
                                    <li>
										<a href="../opr/upd.php">
											
											<i class="fa fa-refresh" aria-hidden="true"></i>
											<span>Update</span>
										</a>
                                    </li>
									<li>
										<a href="../opr/csv.php">
											
											<i class="fa fa-refresh" aria-hidden="true"></i>
											<span>Update CSV</span>
										</a>
                                    </li>
									<!-- <li class="nav-parent">
										<a>
											<i class="fa fa-copy" aria-hidden="true"></i>
											<span>Pages</span>
										</a>
										<ul class="nav nav-children">
											<li>
												<a href="pages-signup.html">
													 Sign Up
												</a>
											</li>
											<li>
												<a href="pages-signin.html">
													 Sign In
												</a>
											</li>
											<li>
												<a href="pages-recover-password.html">
													 Recover Password
												</a>
											</li>
											<li>
												<a href="pages-lock-screen.html">
													 Locked Screen
												</a>
											</li>
											<li>
												<a href="pages-user-profile.html">
													 User Profile
												</a>
											</li>
											<li>
												<a href="pages-session-timeout.html">
													 Session Timeout
												</a>
											</li>
											<li>
												<a href="pages-calendar.html">
													 Calendar
												</a>
											</li>
											<li>
												<a href="pages-timeline.html">
													 Timeline
												</a>
											</li>
											<li>
												<a href="pages-media-gallery.html">
													 Media Gallery
												</a>
											</li>
											<li>
												<a href="pages-invoice.html">
													 Invoice
												</a>
											</li>
											<li>
												<a href="pages-blank.html">
													 Blank Page
												</a>
											</li>
											<li>
												<a href="pages-404.html">
													 404
												</a>
											</li>
											<li>
												<a href="pages-500.html">
													 500
												</a>
											</li>
											<li>
												<a href="pages-log-viewer.html">
													 Log Viewer
												</a>
											</li>
											<li >
												<a href="pages-search-results.html">
													 Search Results
												</a>
											</li>
										</ul>
									</li> -->
									
									
								</ul>
							</nav>
				
							<!-- <hr class="separator" />
				
							<div class="sidebar-widget widget-tasks">
								<div class="widget-header">
									<h6>Projects</h6>
									<div class="widget-toggle">+</div>
								</div>
								<div class="widget-content">
									<ul class="list-unstyled m-none">
										<li><a href="#">Porto HTML5 Template</a></li>
										<li><a href="#">Tucson Template</a></li>
										<li><a href="#">Porto Admin</a></li>
									</ul>
								</div>
							</div>-->				
                           
							<hr class="separator" />
				
							<div class="sidebar-widget widget-stats">
								<div class="widget-header">
									<h6>Company Stats</h6>
									<div class="widget-toggle">+</div>
								</div>
								<div class="widget-content">
									<ul>
										<li>
											<span class="stats-title">Stat 1</span>
											<span class="stats-complete">85%</span>
											<div class="progress">
												<div class="progress-bar progress-bar-primary progress-without-number" role="progressbar" aria-valuenow="85" aria-valuemin="0" aria-valuemax="100" style="width: 85%;">
													<span class="sr-only">85% Complete</span>
												</div>
											</div>
										</li>
										<li>
											<span class="stats-title">Stat 2</span>
											<span class="stats-complete">70%</span>
											<div class="progress">
												<div class="progress-bar progress-bar-primary progress-without-number" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100" style="width: 70%;">
													<span class="sr-only">70% Complete</span>
												</div>
											</div>
										</li>
										<li>
											<span class="stats-title">Stat 3</span>
											<span class="stats-complete">2%</span>
											<div class="progress">
												<div class="progress-bar progress-bar-primary progress-without-number" role="progressbar" aria-valuenow="2" aria-valuemin="0" aria-valuemax="100" style="width: 2%;">
													<span class="sr-only">2% Complete</span>
												</div>
											</div>
										</li>
									</ul>
								</div>
							</div>
						</div>
				
					</div>
				
				</aside>
				<!-- end: sidebar -->

				<section role="main" class="content-body">
					<header class="page-header">
						<h2>Insert Data</h2>
					
						<div class="right-wrapper pull-right">
							<ol class="breadcrumbs">
								<li>
									<a href="../opr/dashboard.php">
										<i class="fa fa-home"></i>
									</a>
								</li>
								<!-- <li><span>Forms</span></li> -->
								<li><span>Insert</span></li>
							</ol>
					
							<a class="sidebar-right-toggle" ><i class="fa fa-chevron-down"></i></a>
						</div>
					</header>

					<!-- start: page -->
						
						<div class="row">
							<div class="col-xs-12">
								<section class="panel form-wizard" id="w4">
									<header class="panel-heading">
										<!-- <div class="panel-actions">
											<a href="#" class="panel-action panel-action-toggle" data-panel-toggle></a>
											<a href="#" class="panel-action panel-action-dismiss" data-panel-dismiss></a>
										</div> -->
						
										<h2 class="panel-title">Input Details</h2>
									</header>
									<div class="panel-body">
										<div class="wizard-progress wizard-progress-lg">
											<div class="steps-progress">
												<div class="progress-indicator"></div>
											</div>
											<ul class="wizard-steps">
												<li class="active">
													<a href="#w4-account" data-toggle="tab"><span>1</span>Company Info</a>
												</li>
												<!-- <li>
													<a href="#w4-profile" data-toggle="tab"><span>2</span>Contact Info</a>
												</li>
												<li>
													<a href="#w4-billing" data-toggle="tab"><span>3</span>Billing Info</a>
												</li> -->
												<li>
													<a href="#w4-confirm" data-toggle="tab"><span>2</span>Contact Info</a>
												</li>
											</ul>
										</div>

                                        <?php
											//$q=$_GET['v'];
											$e=$_SESSION['e'];
											$m=$_SESSION['m'];
                                            $query=mysql_query("select * from location where Mail like '%$e%' or Mobile like '%$m%'  order by id LIMIT 6");
                                            while($row=mysql_fetch_array($query))
                                            {
                                                $company=$row['Company'];
                                                $sector=$row['Sector'];
                                                $subsector=$row['Sub-Sector'];
                                                $fname=$row['First-Name'];
                                                $lname=$row['Last-Name'];
                                                $level=$row['Level'];
                                                $department=$row['Dept'];
                                                $designation=$row['Designation'];
                                                $email=$row['Mail'];
                                                $mobile=$row['Mobile'];
                                                $contactpersont=$row['Contact-Person2'];
                                                $contactnumber=$row['Contact-Number'];
                                                $emailid=$row['Email-ID'];
                                                $url=$row['Url'];
                                                $std=$row['Std-Code'];
                                                $land=$row['Landline-No'];
                                                $address=$row['Address'];
                                                $location=$row['Location'];
                                                $sublocation=$row['Sub-Location'];
                                                $city=$row['City'];
                                                $pin=$row['Pin'];
                                                $state=$row['State'];
                                                $stdcode=$row['Std-Code'];
                                                $landlineno=$row['Landline-No'];
                                                $faxno=$row['Fax-No'];
                                            }    

                                        ?>

										<form action="ud.php" method="post" class="form-horizontal" novalidate="novalidate"  >
											<div class="tab-content">
											<div id="w4-account" class="tab-pane active">
											<div class="form-group">
												<label class="col-sm-2 control-label" for="w4-username">Company</label>
												<div class="col-sm-9">
													<input type="text" class="form-control" name="company" id="w4-username" value="<?php echo $company; ?>" required>
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-2 control-label" for="w4-password">Sector</label>
												<div class="col-sm-9">
													<input type="text" class="form-control" name="sector" value="<?php echo $sector; ?>" id="w4-password" >
												</div>
											</div>

											<div class="form-group">
												<label class="col-sm-2 control-label" for="w4-username">Sub Sector</label>
												<div class="col-sm-9">
													<input type="text" class="form-control" name="subsector" value="<?php echo $subsector; ?>" id="w4-username" >
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-2 control-label" for="w4-password">Url</label>
												<div class="col-sm-9">
													<input type="text" class="form-control" name="url" value="<?php echo $url; ?>" id="w4-password" >
												</div>
											</div>

											<div class="form-group">
												<label class="col-sm-2 control-label" for="w4-username">Address</label>
												<div class="col-sm-9">
													<input type="text" class="form-control" name="address" value="<?php echo $address; ?>" id="w4-username" >
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-2 control-label" for="w4-password">Location</label>
												<div class="col-sm-9">
													<input type="text" class="form-control" name="location" value="<?php echo $location; ?>" id="w4-password" >
												</div>
											</div>

											<div class="form-group">
												<label class="col-sm-2 control-label" for="w4-username">Sub Location</label>
												<div class="col-sm-9">
													<input type="text" class="form-control" name="sublocation" value="<?php echo $sublocation; ?>" id="w4-username" >
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-2 control-label" for="w4-password">City</label>
												<div class="col-sm-9">
													<input type="text" class="form-control" name="city" value="<?php echo $city; ?>" id="w4-password" >
												</div>
											</div>

											<div class="form-group">
												<label class="col-sm-2 control-label" for="w4-username">Pin</label>
												<div class="col-sm-9">
													<input type="text" class="form-control" name="pin" id="w4-pin" maxlength="6" value="<?php echo $pin; ?>"  onKeyUp="pins()" >
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-2 control-label" for="w4-password">State</label>
												<div class="col-sm-9">
													<input type="text" class="form-control" name="state" value="<?php echo $state; ?>" id="w4-password" >
												</div>
											</div>

											<div class="form-group">
												<label class="col-sm-2 control-label" for="w4-username">Std Code</label>
												<div class="col-sm-9">
													<input type="text" class="form-control" name="stdcode" id="w4-std" value="<?php echo $stdcode; ?>" onKeyUp="stdc()" >
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-2 control-label" for="w4-password">Landline No</label>
												<div class="col-sm-9">
													<input type="text" class="form-control" name="landlineno" id="w4-land" value="<?php echo $landlineno; ?>" onKeyUp="landl()" >
												</div>
											</div>

											<div class="form-group">
												<label class="col-sm-2 control-label" for="w4-username">Fax No</label>
												<div class="col-sm-9">
													<input type="text" class="form-control" name="faxno" value="<?php echo $faxno; ?>" id="w4-username" >
												</div>
											</div>
											



										</div>
												<!-- <div id="w4-profile" class="tab-pane">
													<div class="form-group">
														<label class="col-sm-2 control-label" for="w4-first-name">First Name</label>
														<div class="col-sm-9">
															<input type="text" class="form-control" name="first-name" id="w4-first-name" >
														</div>
													</div>
													<div class="form-group">
														<label class="col-sm-2 control-label" for="w4-last-name">Last Name</label>
														<div class="col-sm-9">
															<input type="text" class="form-control" name="last-name" id="w4-last-name" >
														</div>
													</div>
												</div>
												<div id="w4-billing" class="tab-pane">
													<div class="form-group">
														<label class="col-sm-2 control-label" for="w4-cc">Card Number</label>
														<div class="col-sm-9">
															<input type="text" class="form-control" name="cc-number" id="w4-cc" >
														</div>
													</div>
													<div class="form-group">
														<label class="col-sm-2 control-label" for="inputSuccess">Expiration</label>
														<div class="col-sm-5">
															<select class="form-control" name="exp-month" >
																<option>January</option>
																<option>February</option>
																<option>March</option>
																<option>April</option>
																<option>May</option>
																<option>June</option>
																<option>July</option>
																<option>August</option>
																<option>September</option>
																<option>October</option>
																<option>November</option>
																<option>December</option>
															</select>
														</div>
														<div class="col-sm-4">
															<select class="form-control" name="exp-year" >
																<option>2014</option>
																<option>2015</option>
																<option>2016</option>
																<option>2017</option>
																<option>2018</option>
																<option>2019</option>
																<option>2020</option>
																<option>2021</option>
																<option>2022</option>
															</select>
														</div>
													</div>
												</div> -->
												<div id="w4-confirm" class="tab-pane">
												<div class="form-group">
													<label class="col-sm-2 control-label" for="w4-email">First Name</label>
													<div class="col-sm-9">
														<input type="text" class="form-control" name="firstname" value="<?php echo $fname; ?>" id="w4-f" onKeyUp="fname()" >
													</div>
												</div>
												<div class="form-group">
													<label class="col-sm-2 control-label" for="w4-email">Last Name</label>
													<div class="col-sm-9">
														<input type="text" class="form-control" name="lastname" id="w4-l" value="<?php echo $lname; ?>" onKeyUp="lname()" >
													</div>
												</div>
												<div class="form-group">
													<label class="col-sm-2 control-label" for="w4-email">Level</label>
													<div class="col-sm-9">
														<input type="text" class="form-control" name="level" value="<?php echo $level; ?>" id="w4-email" >
													</div>
												</div>
												<div class="form-group">
													<label class="col-sm-2 control-label" for="w4-email">Department</label>
													<div class="col-sm-9">
														<input type="text" class="form-control" name="department" value="<?php echo $department; ?>" id="w4-email" >
													</div>
												</div>
												<div class="form-group">
													<label class="col-sm-2 control-label" for="w4-email">Designation</label>
													<div class="col-sm-9">
														<input type="text" class="form-control" name="designation" value="<?php echo $designation; ?>" id="w4-email" >
													</div>
												</div>
												<div class="form-group">
													<label class="col-sm-2 control-label" for="w4-email">Mobile</label>
													<div class="col-sm-9">
														<input type="text" class="form-control" name="mobile" id="w4-mob" value="<?php echo $mobile; ?>" onKeyUp="mob()" maxlength="10">
													</div>
												</div>
												<div class="form-group">
													<label class="col-sm-2 control-label" for="w4-email">Email</label>
													<div class="col-sm-9">
														<input type="email" class="form-control" name="email" value="<?php echo $email; ?>" id="w4-email" >
													</div>
												</div>
												<div class="form-group">
													<label class="col-sm-2 control-label" for="w4-email">Contact Person 2</label>
													<div class="col-sm-9">
														<input type="text" class="form-control" name="contactpersont" id="w4-cp" value="<?php echo $contactpersont; ?>" onKeyUp="cpt()" >
													</div>
												</div>
												<div class="form-group">
													<label class="col-sm-2 control-label" for="w4-email">Contact Number</label>
													<div class="col-sm-9">
														<input type="text" class="form-control" name="contactnumber" id="w4-cn" value="<?php echo $contactnumber; ?>" onKeyUp="cn()" maxlength="10" >
													</div>
												</div>
												<div class="form-group">
													<label class="col-sm-2 control-label" for="w4-email">Email ID</label>
													<div class="col-sm-9">
														<input type="email" class="form-control" name="emailid" id="w4-email" value="<?php echo $emailid; ?>" >
													</div>
												</div>
												<div class="form-group">
													<div class="col-sm-2"></div>
													<div class="col-sm-9">
														<div class="checkbox-custom">
															<input type="checkbox" name="terms" id="w4-terms" required>
															<label for="w4-terms">I agree to the terms of service</label>
															
														</div>
													</div>
												</div>
												<div class="form-group">
													<div class="col-sm-2"></div>
													<div class="col-sm-9">
														<div class="checkbox-custom">
														<input type="submit" name="submit" value="Submit" class="btn btn-success pull-right" />
															
														</div>
													</div>
												</div>
											</div>
											
										
									</div>
									<div class="panel-footer">
										<ul class="pager">
											<li class="previous disabled">
												<a><i class="fa fa-angle-left"></i> Previous</a>
											</li>
											<li class="finish hidden pull-right">
												<!-- <a>Finish</a> -->
												<!-- <input type="submit" name="submit" value="Submit" class="btn btn-success" /> -->
												</form>
											</li>
											<li class="next">
												<a>Next <i class="fa fa-angle-right"></i></a>
											</li>
										</ul>
										
									</div>
								</section>
							</div>
						</div>
						
					<!-- end: page -->
				</section>
			</div>

			<aside id="sidebar-right" class="sidebar-right">
				<div class="nano">
					<div class="nano-content">
						<a href="#" class="mobile-close visible-xs">
							Collapse <i class="fa fa-chevron-right"></i>
						</a>
			
						<div class="sidebar-right-wrapper">
			
							<div class="sidebar-widget widget-calendar">
								<h6>Upcoming Tasks</h6>
								<div data-plugin-datepicker data-plugin-skin="dark" ></div>
			
								<ul>
									<li>
										<time datetime="2014-04-19T00:00+00:00">04/19/2014</time>
										<span>Company Meeting</span>
									</li>
								</ul>
							</div>
			
							<div class="sidebar-widget widget-friends">
								<h6>Friends</h6>
								<ul>
									<li class="status-online">
										<figure class="profile-picture">
											<img src="../assets/images/!sample-user.jpg" alt="Joseph Doe" class="img-circle">
										</figure>
										<div class="profile-info">
											<span class="name">Joseph Doe Junior</span>
											<span class="title">Hey, how are you?</span>
										</div>
									</li>
									<li class="status-online">
										<figure class="profile-picture">
											<img src="../assets/images/!sample-user.jpg" alt="Joseph Doe" class="img-circle">
										</figure>
										<div class="profile-info">
											<span class="name">Joseph Doe Junior</span>
											<span class="title">Hey, how are you?</span>
										</div>
									</li>
									<li class="status-offline">
										<figure class="profile-picture">
											<img src="../assets/images/!sample-user.jpg" alt="Joseph Doe" class="img-circle">
										</figure>
										<div class="profile-info">
											<span class="name">Joseph Doe Junior</span>
											<span class="title">Hey, how are you?</span>
										</div>
									</li>
									<li class="status-offline">
										<figure class="profile-picture">
											<img src="../assets/images/!sample-user.jpg" alt="Joseph Doe" class="img-circle">
										</figure>
										<div class="profile-info">
											<span class="name">Joseph Doe Junior</span>
											<span class="title">Hey, how are you?</span>
										</div>
									</li>
								</ul>
							</div>
			
						</div>
					</div>
				</div>
			</aside>
		</section>

		<!-- Vendor -->
		<script src="../assets/vendor/jquery/jquery.js"></script>
		<script src="../assets/vendor/jquery-browser-mobile/jquery.browser.mobile.js"></script>
		<script src="../assets/vendor/bootstrap/js/bootstrap.js"></script>
		<script src="../assets/vendor/nanoscroller/nanoscroller.js"></script>
		<script src="../assets/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
		<script src="../assets/vendor/magnific-popup/magnific-popup.js"></script>
		<script src="../assets/vendor/jquery-placeholder/jquery.placeholder.js"></script>
		
		<!-- Specific Page Vendor -->
		<script src="../assets/vendor/jquery-validation/jquery.validate.js"></script>
		<script src="../assets/vendor/bootstrap-wizard/jquery.bootstrap.wizard.js"></script>
		<script src="../assets/vendor/pnotify/pnotify.custom.js"></script>
		
		<!-- Theme Base, Components and Settings -->
		<script src="../assets/javascripts/theme.js"></script>
		
		<!-- Theme Custom -->
		<script src="../assets/javascripts/theme.custom.js"></script>
		
		<!-- Theme Initialization Files -->
		<script src="../assets/javascripts/theme.init.js"></script>


		<!-- Examples -->
		<script src="../assets/javascripts/forms/examples.wizard.js"></script>
		<script>
		function pins()
		{
			var x=/\D/ig
			var y=document.getElementById("w4-pin")
			var z=y.value.search(x)
			var a=/\W/ig
			var b=document.getElementById("w4-pin")
			var c=y.value.search(a)
			if(z!=-1)	
			{
				y.value=y.value.replace(/\D/g,'')
			}
			if(c!=-1)
			{
				y.value=y.value.replace(/\W/g,'')
			}

		}

		function stdc()
		{
			var x=/\D/ig
			var y=document.getElementById("w4-std")
			var z=y.value.search(x)
			var a=/\W/ig
			var b=document.getElementById("w4-std")
			var c=y.value.search(a)
			if(z!=-1)	
			{
				y.value=y.value.replace(/\D/g,'')
			}
			if(c!=-1)
			{
				y.value=y.value.replace(/\W/g,'')
			}

		}

		function mob()
		{
			var x=/\D/ig
			var y=document.getElementById("w4-mob")
			var z=y.value.search(x)
			var a=/\W/ig
			var b=document.getElementById("w4-mob")
			var c=y.value.search(a)
			if(z!=-1)	
			{
				y.value=y.value.replace(/\D/g,'')
			}
			if(c!=-1)
			{
				y.value=y.value.replace(/\W/g,'')
			}

		}

		function cn()
		{
			var x=/\D/ig
			var y=document.getElementById("w4-cn")
			var z=y.value.search(x)
			var a=/\W/ig
			var b=document.getElementById("w4-cn")
			var c=y.value.search(a)
			if(z!=-1)	
			{
				y.value=y.value.replace(/\D/g,'')
			}
			if(c!=-1)
			{
				y.value=y.value.replace(/\W/g,'')
			}

		}

		function landl()
		{
			var x=/\D/ig
			var y=document.getElementById("w4-land")
			var z=y.value.search(x)
			var a=/\W/ig
			var b=document.getElementById("w4-land")
			var c=y.value.search(a)
			if(z!=-1)	
			{
				y.value=y.value.replace(/\D/g,'')
			}
			if(c!=-1)
			{
				y.value=y.value.replace(/\W/g,'')
			}

		}

		function fname()
		{
			var x=/\d/ig
			var y=document.getElementById("w4-f")
			var z=y.value.search(x)
			var a=/\W/ig
			var b=document.getElementById("w4-f")
			var c=y.value.search(a)
			if(z!=-1)
			{
				y.value=y.value.replace(/\d/g,'')
			}
			if(c!=-1)
			{
				y.value=y.value.replace(/W/g,'')
			}
		}

		function lname()
		{
			var x=/\d/ig
			var y=document.getElementById("w4-l")
			var z=y.value.search(x)
			var a=/\W/ig
			var b=document.getElementById("w4-l")
			var c=y.value.search(a)
			if(z!=-1)
			{
				y.value=y.value.replace(/\d/g,'')
			}
			if(c!=-1)
			{
				y.value=y.value.replace(/W/g,'')
			}
		}

		function cpt()
		{
			var x=/\d/ig
			var y=document.getElementById("w4-cp")
			var z=y.value.search(x)
			var a=/\W/ig
			var b=document.getElementById("w4-cp")
			var c=y.value.search(a)
			if(z!=-1)
			{
				y.value=y.value.replace(/\d/g,'')
			}
			if(c!=-1)
			{
				y.value=y.value.replace(/W/g,'')
			}
		}
		</script>
	</body>
</html>
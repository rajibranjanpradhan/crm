<?php
session_start();
error_reporting(E_PARSE | E_ERROR);
$id=$_SESSION['id'];
if(!$_SESSION['id'])
{
	echo '<script>location.replace("../../../../../index.php");</script>';
}

require_once "../../../conn/conn.php";
$chk=mysqli_query($dbc,"select * from team where email='$id'");
while($fchk=mysqli_fetch_assoc($chk))
{
	$type=$fchk['EmployeeType'];
	if( $type !="Manager")
	{
		echo '<script>location.replace("../../../../index.php");</script>';
	}
}


?>

<?php

$rep=$_SESSION['n'];
	$rmail=$id;

date_default_timezone_set('Asia/Kolkata');

$date = date('d/m/Y');
$year = date('Y');


$fetchall=mysqli_query($dbc,"select * from `quotation`");
$inoprev=mysqli_num_rows($fetchall);
$ino=$inoprev;

if(isset($_POST['accept']))
{
	$_SESSION['qproduct']=$_POST['product'];
	$_SESSION['qcompany']=$_POST['company'];
	$slno=$_POST['slno'];
	$quotno="RBS/00".$ino;
	mysqli_query($dbc,"update `quotation` set `QuotNo`='$quotno',`Permission`='0' where `id`='$slno'");
}


$product=$_SESSION['qproduct'];

$company=$_SESSION['qcompany'];

$fid=$_SESSION['fid'];



while($row=mysqli_fetch_assoc($fetchall))
{
	$company=$row['Company'];
	$product=$row['Product'];
	$tax=$row['Tax'];
	$currency=$row['Currency'];
	$hsnsac=$row['HSNSAC'];
	$service=$row['Service'];
	$pdescription1=$row['PartDescription1'];
	$quantity1=$row['Quantity1'];
	$unitprice1=$row['UnitPrice1'];

	$pdescription2=$row['PartDescription2'];
	$quantity2=$row['Quantity2'];
	$unitprice2=$row['UnitPrice2'];

	$pdescription3=$row['PartDescription3'];
	$quantity3=$row['Quantity3'];
	$unitprice3=$row['UnitPrice3'];

	$pdescription4=$row['PartDescription4'];
	$quantity4=$row['Quantity4'];
	$unitprice4=$row['UnitPrice4'];

	$pdescription5=$row['PartDescription5'];
	$quantity5=$row['Quantity5'];
	$unitprice5=$row['UnitPrice5'];

	$delivery=$row['Delivery'];
	$validity=$row['Validity'];
	$payment=$row['Payment'];

}

   
require("fpdf/fpdf.php");

class myPDF extends FPDF {
    function myCell($w,$h,$x,$t){
        $height=$h/3;
        $first=$height+2;
        $second=$height+$height+$height+3;
        $third=$height+$height+$height+$height+$height+3;
        $len=strlen($t);
        if($len>20){
            $txt=str_split($t,20);
            $this->SetX($x);
            $this->Cell($w,$first,$txt[0],'','','');
            $this->SetX($x);
            $this->Cell($w,$second,$txt[1],'','','');
            $this->SetX($x);
            $this->Cell($w,$third,$txt[2],'','','');
            $this->SetX($x);
            $this->Cell($w,$h,'','LTRB',0,'C',0);
        }
        else{
            $this->SetX($x);
            $this->Cell($w,$h,$t,'LTRB',0,'C',0);
        }
    }
}


$pdf= new myPDF();
$pdf->AddPage();
$pdf->SetFont("Times","",14);
$pdf->Ln();



//$pdf->Cell(170,10,"REGISTRATION FORM",0,1,"C");
$pdf->Image('../../../../../log/assets/images/logo.png', 140, 10, 60, '', '', '', false, 150);

$pdf->Cell(1,10,"",0,1);
$pdf->Cell(50,10,"Date: $date",0,1);
//$pdf->Cell(5,10,"$date",0,1);

$quotno="RBS/00".$ino;

$pdf->Cell(50,10,"Ref No: RBS/00$ino",0,0);
$pdf->Cell(80,10,"$b",0,1);

/*$pdf->Cell(50,10,"Regd. No:",0,0);
$pdf->Cell(80,10,"$c",0,1);

$pdf->Cell(50,10,"Branch:",0,0);
$pdf->Cell(80,10,"$d",0,1);

$pdf->Cell(50,10,"Sem:",0,0);
$pdf->Cell(80,10,"$e",0,1);*/

$fetcit=mysqli_query($dbc,"select * from customers where `Company`='$company'");
while($frc=mysqli_fetch_assoc($fetcit))
{
	$city=$frc['City'];
}

if($currency == "rupee")
{
	$cur="INR";
}
elseif($currency == "dollar")
{
	$cur="USD";
}
elseif ($currency == "euro") {
	$cur="EUR";
}
elseif ($currency == "pound") {
	$cur="GBP";
}




$pdf->SetFont("Times","B",12);
$pdf->Cell(170,18,"QUOTATION",0,1,"C");

$pdf->SetFont("Times","",12);

$pdf->Cell(50,4,"To,",0,1);
$pdf->Cell(50,6,"$company",0,1);
$pdf->Cell(50,10,"$city",0,1);


$pdf->SetFont("Times","",12);

$pdf->Cell(50,10,"Dear Sir/Madam,",0,1);
$pdf->Cell(50,10,"We are pleased to send our best quote for the following products enquired.",0,1);



$pdf->SetFont("Times","B",12);

$pdf->Cell(20,10,"Sl No.",1,0,"C");
/*$pdf->Cell(40,10,"Product",1,0,"C");*/
$pdf->Cell(60,10,"Description",1,0,"C");
$pdf->Cell(30,10,"HSN/SAC",1,0,"C");
$pdf->Cell(20,10,"Qty",1,0,"C");
$pdf->Cell(30,10,"Unit Price($cur)",1,0,"C");
$pdf->Cell(32,10,"Total Price($cur)",1,1,"C");

$pdf->SetFont("Times","",12);
//if(empty(1))
$total1=0;$total2=0;$total3=0;$total4=0;$total5=0;$igsttotal=0;$sgsttotal=0;$cgsttotal=0;$grandtotal=0;$subtotal=0;
$w=60;$h=20;

if(empty($pdescription2) and empty($pdescription3) and empty($pdescription4)  and empty($pdescription5))
{

//$total=$_SESSION['qty1']*$_SESSION['unitprice1'];

	$partdesc1=$pdescription1;
	$qt1=$quantity1;
	$unitp1=sprintf("%.2f",$unitprice1);
	$total1=sprintf("%.2f",$qt1*$unitp1);


	/*$pdf->Cell(20,10,"1",1,0,"C");
	
	$pdf->Cell(60,10,"$partdesc1",1,0,"C");
	$pdf->Cell(30,10,"$hsnsac",1,0,"C");
	$pdf->Cell(20,10,"$qt1",1,0,"C");
	$pdf->Cell(30,10,"$unitp1",1,0,"C");
	$pdf->Cell(32,10,"$total1",1,1,"C");*/

	$x=$pdf->getx();
$pdf->myCell(20,$h,$x,'1');
$x=$pdf->getx();
$pdf->myCell($w,$h,$x,$partdesc1);
$x=$pdf->getx();
$pdf->myCell(30,$h,$x,$hsnsac);
$x=$pdf->getx();
$pdf->myCell(20,$h,$x,$qt1);
$x=$pdf->getx();
$pdf->myCell(30,$h,$x,$unitp1);
$x=$pdf->getx();
$pdf->myCell(32,$h,$x,$total1);
$pdf->Ln();

}
else if(empty($pdescription3) and empty($pdescription4)  and empty($pdescription5))
{
	$partdesc1=$pdescription1;
	$qt1=$quantity1;
	$unitp1=sprintf("%.2f",$unitprice1);
	$total1=sprintf("%.2f",$qt1*$unitp1);

	$partdesc2=$pdescription2;
	$qt2=$quantity2;
	$unitp2=sprintf("%.2f",$unitprice2);
	$total2=sprintf("%.2f",$qt2*$unitp2);


	$x=$pdf->getx();
$pdf->myCell(20,$h,$x,'1');
$x=$pdf->getx();
$pdf->myCell($w,$h,$x,$partdesc1);
$x=$pdf->getx();
$pdf->myCell(30,$h,$x,$hsnsac);
$x=$pdf->getx();
$pdf->myCell(20,$h,$x,$qt1);
$x=$pdf->getx();
$pdf->myCell(30,$h,$x,$unitp1);
$x=$pdf->getx();
$pdf->myCell(32,$h,$x,$total1);
$pdf->Ln();

	$x=$pdf->getx();
$pdf->myCell(20,$h,$x,'2');
$x=$pdf->getx();
$pdf->myCell($w,$h,$x,$partdesc2);
$x=$pdf->getx();
$pdf->myCell(30,$h,$x,$hsnsac);
$x=$pdf->getx();
$pdf->myCell(20,$h,$x,$qt2);
$x=$pdf->getx();
$pdf->myCell(30,$h,$x,$unitp2);
$x=$pdf->getx();
$pdf->myCell(32,$h,$x,$total2);
$pdf->Ln();
}

else if(empty($pdescription4)  and empty($pdescription5))
{
	$partdesc1=$pdescription1;
	$qt1=$quantity1;
	$unitp1=sprintf("%.2f",$unitprice1);
	$total1=sprintf("%.2f",$qt1*$unitp1);

	$partdesc2=$pdescription2;
	$qt2=$quantity2;
	$unitp2=sprintf("%.2f",$unitprice2);
	$total2=sprintf("%.2f",$qt2*$unitp2);

	$partdesc3=$pdescription3;
	$qt3=$quantity3;
	$unitp3=sprintf("%.2f",$unitprice3);
	$total3=sprintf("%.2f",$qt3*$unitp3);


	$x=$pdf->getx();
$pdf->myCell(20,$h,$x,'1');
$x=$pdf->getx();
$pdf->myCell($w,$h,$x,$partdesc1);
$x=$pdf->getx();
$pdf->myCell(30,$h,$x,$hsnsac);
$x=$pdf->getx();
$pdf->myCell(20,$h,$x,$qt1);
$x=$pdf->getx();
$pdf->myCell(30,$h,$x,$unitp1);
$x=$pdf->getx();
$pdf->myCell(32,$h,$x,$total1);
$pdf->Ln();

$x=$pdf->getx();
$pdf->myCell(20,$h,$x,'2');
$x=$pdf->getx();
$pdf->myCell($w,$h,$x,$partdesc2);
$x=$pdf->getx();
$pdf->myCell(30,$h,$x,$hsnsac);
$x=$pdf->getx();
$pdf->myCell(20,$h,$x,$qt2);
$x=$pdf->getx();
$pdf->myCell(30,$h,$x,$unitp2);
$x=$pdf->getx();
$pdf->myCell(32,$h,$x,$total2);
$pdf->Ln();

$x=$pdf->getx();
$pdf->myCell(20,$h,$x,'3');
$x=$pdf->getx();
$pdf->myCell($w,$h,$x,$partdesc3);
$x=$pdf->getx();
$pdf->myCell(30,$h,$x,$hsnsac);
$x=$pdf->getx();
$pdf->myCell(20,$h,$x,$qt3);
$x=$pdf->getx();
$pdf->myCell(30,$h,$x,$unitp3);
$x=$pdf->getx();
$pdf->myCell(32,$h,$x,$total3);
$pdf->Ln();


}

else if( empty($pdescription5))
{
	$partdesc1=$pdescription1;
	$qt1=$quantity1;
	$unitp1=sprintf("%.2f",$unitprice1);
	$total1=sprintf("%.2f",$qt1*$unitp1);

	$partdesc2=$pdescription2;
	$qt2=$quantity2;
	$unitp2=sprintf("%.2f",$unitprice2);
	$total2=sprintf("%.2f",$qt2*$unitp2);

	$partdesc3=$pdescription3;
	$qt3=$quantity3;
	$unitp3=sprintf("%.2f",$unitprice3);
	$total3=sprintf("%.2f",$qt3*$unitp3);

	$partdesc4=$pdescription4;
	$qt4=$quantity4;
	$unitp4=sprintf("%.2f",$unitprice4);
	$total4=sprintf("%.2f",$qt4*$unitp4);


	$x=$pdf->getx();
$pdf->myCell(20,$h,$x,'1');
$x=$pdf->getx();
$pdf->myCell($w,$h,$x,$partdesc1);
$x=$pdf->getx();
$pdf->myCell(30,$h,$x,$hsnsac);
$x=$pdf->getx();
$pdf->myCell(20,$h,$x,$qt1);
$x=$pdf->getx();
$pdf->myCell(30,$h,$x,$unitp1);
$x=$pdf->getx();
$pdf->myCell(32,$h,$x,$total1);
$pdf->Ln();

$x=$pdf->getx();
$pdf->myCell(20,$h,$x,'2');
$x=$pdf->getx();
$pdf->myCell($w,$h,$x,$partdesc2);
$x=$pdf->getx();
$pdf->myCell(30,$h,$x,$hsnsac);
$x=$pdf->getx();
$pdf->myCell(20,$h,$x,$qt2);
$x=$pdf->getx();
$pdf->myCell(30,$h,$x,$unitp2);
$x=$pdf->getx();
$pdf->myCell(32,$h,$x,$total2);
$pdf->Ln();

$x=$pdf->getx();
$pdf->myCell(20,$h,$x,'3');
$x=$pdf->getx();
$pdf->myCell($w,$h,$x,$partdesc3);
$x=$pdf->getx();
$pdf->myCell(30,$h,$x,$hsnsac);
$x=$pdf->getx();
$pdf->myCell(20,$h,$x,$qt3);
$x=$pdf->getx();
$pdf->myCell(30,$h,$x,$unitp3);
$x=$pdf->getx();
$pdf->myCell(32,$h,$x,$total3);
$pdf->Ln();

$x=$pdf->getx();
$pdf->myCell(20,$h,$x,'4');
$x=$pdf->getx();
$pdf->myCell($w,$h,$x,$partdesc4);
$x=$pdf->getx();
$pdf->myCell(30,$h,$x,$hsnsac);
$x=$pdf->getx();
$pdf->myCell(20,$h,$x,$qt4);
$x=$pdf->getx();
$pdf->myCell(30,$h,$x,$unitp4);
$x=$pdf->getx();
$pdf->myCell(32,$h,$x,$total4);
$pdf->Ln();

}
else 
{
	$partdesc1=$pdescription1;
	$qt1=$quantity1;
	$unitp1=sprintf("%.2f",$unitprice1);
	$total1=sprintf("%.2f",$qt1*$unitp1);

	$partdesc2=$pdescription2;
	$qt2=$quantity2;
	$unitp2=sprintf("%.2f",$unitprice2);
	$total2=sprintf("%.2f",$qt2*$unitp2);

	$partdesc3=$pdescription3;
	$qt3=$quantity3;
	$unitp3=sprintf("%.2f",$unitprice3);
	$total3=sprintf("%.2f",$qt3*$unitp3);

	$partdesc4=$pdescription4;
	$qt4=$quantity4;
	$unitp4=sprintf("%.2f",$unitprice4);
	$total4=sprintf("%.2f",$qt4*$unitp4);

	$partdesc5=$pdescription5;
	$qt5=$quantity5;
	$unitp5=sprintf("%.2f",$unitprice5);
	$total5=sprintf("%.2f",$qt5*$unitp5);


	$x=$pdf->getx();
$pdf->myCell(20,$h,$x,'1');
$x=$pdf->getx();
$pdf->myCell($w,$h,$x,$partdesc1);
$x=$pdf->getx();
$pdf->myCell(30,$h,$x,$hsnsac);
$x=$pdf->getx();
$pdf->myCell(20,$h,$x,$qt1);
$x=$pdf->getx();
$pdf->myCell(30,$h,$x,$unitp1);
$x=$pdf->getx();
$pdf->myCell(32,$h,$x,$total1);
$pdf->Ln();

$x=$pdf->getx();
$pdf->myCell(20,$h,$x,'2');
$x=$pdf->getx();
$pdf->myCell($w,$h,$x,$partdesc2);
$x=$pdf->getx();
$pdf->myCell(30,$h,$x,$hsnsac);
$x=$pdf->getx();
$pdf->myCell(20,$h,$x,$qt2);
$x=$pdf->getx();
$pdf->myCell(30,$h,$x,$unitp2);
$x=$pdf->getx();
$pdf->myCell(32,$h,$x,$total2);
$pdf->Ln();

$x=$pdf->getx();
$pdf->myCell(20,$h,$x,'3');
$x=$pdf->getx();
$pdf->myCell($w,$h,$x,$partdesc3);
$x=$pdf->getx();
$pdf->myCell(30,$h,$x,$hsnsac);
$x=$pdf->getx();
$pdf->myCell(20,$h,$x,$qt3);
$x=$pdf->getx();
$pdf->myCell(30,$h,$x,$unitp3);
$x=$pdf->getx();
$pdf->myCell(32,$h,$x,$total3);
$pdf->Ln();

$x=$pdf->getx();
$pdf->myCell(20,$h,$x,'4');
$x=$pdf->getx();
$pdf->myCell($w,$h,$x,$partdesc4);
$x=$pdf->getx();
$pdf->myCell(30,$h,$x,$hsnsac);
$x=$pdf->getx();
$pdf->myCell(20,$h,$x,$qt4);
$x=$pdf->getx();
$pdf->myCell(30,$h,$x,$unitp4);
$x=$pdf->getx();
$pdf->myCell(32,$h,$x,$total4);
$pdf->Ln();

$x=$pdf->getx();
$pdf->myCell(20,$h,$x,'5');
$x=$pdf->getx();
$pdf->myCell($w,$h,$x,$partdesc5);
$x=$pdf->getx();
$pdf->myCell(30,$h,$x,$hsnsac);
$x=$pdf->getx();
$pdf->myCell(20,$h,$x,$qt5);
$x=$pdf->getx();
$pdf->myCell(30,$h,$x,$unitp5);
$x=$pdf->getx();
$pdf->myCell(32,$h,$x,$total5);
$pdf->Ln();


}

	$subtotal=sprintf("%.2f",$total1+$total2+$total3+$total4+$total5);
	$pdf->Cell(20,10,"",1,0,"C");
	
	$pdf->Cell(60,10,"",1,0,"C");
	$pdf->Cell(30,10,"",1,0,"C");
	$pdf->Cell(20,10,"",1,0,"C");
	$pdf->Cell(30,10,"Sub Total",1,0,"C");
	$pdf->Cell(32,10,"$subtotal",1,1,"C");




$array =  explode(',', $tax);
    foreach ($array as $item) {

    	$fettax=mysqli_query($dbc,"select * from setting_tax where `TaxName`='$item'");
    	while($frt=mysqli_fetch_assoc($fettax))
		{
			$taxpercentage=$frt['TaxPercentage'];

    	}

    	$totalwtax=sprintf("%.2f",($taxpercentage/100)*$subtotal);

    	$pdf->Cell(20,10,"",1,0,"C");
		$pdf->Cell(60,10,"",1,0,"C");
		$pdf->Cell(30,10,"",1,0,"C");
		$pdf->Cell(20,10,"",1,0,"C");
		$pdf->Cell(30,10,"$item $taxpercentage%",1,0,"C");
		$pdf->Cell(32,10,"$totalwtax",1,1,"C");


		$grandtotal=$grandtotal+$totalwtax;

		

		
    }



$final_grandtotal=sprintf("%.2f", $grandtotal+$subtotal);



$pdf->Cell(20,10,"",1,0,"C");
$pdf->Cell(60,10,"",1,0,"C");
$pdf->Cell(30,10,"",1,0,"C");
$pdf->Cell(20,10,"",1,0,"C");
$pdf->Cell(30,10,"Grand Total",1,0,"C");
$pdf->Cell(32,10,"$final_grandtotal",1,1,"C");

$ad=mysqli_query($dbc,"update `quotation` set `QuotNo`='$quotno',`SubTotal`='$subtotal',`GrandTotal`='$final_grandtotal' where   `Company`='$company' and `FunnelId`='$fid' ");

/*$ad=mysqli_query($dbc,"update `quotation` set `QuotNo`='$quotno',`Representive`='$rep',`RMail`='$rmail',`PartDescription1`='$pdescription1',`PartDescription2`='$pdescription2',`PartDescription3`='$pdescription3',`PartDescription4`='$pdescription4',`PartDescription5`='$pdescription5',`Quantity1`='$quantity1',`Quantity2`='$quantity2',`Quantity3`='$quantity3',`Quantity4`='$quantity4',`Quantity5`='$quantity5',`UnitPrice1`='$unitprice1',`UnitPrice2`='$unitprice2',`UnitPrice3`='$unitprice3',`UnitPrice4`='$unitprice4',`UnitPrice5`='$unitprice5',`SubTotal`='$subtotal',`SGST`='$sgsttotal',`CGST`='$cgsttotal',`IGST`='$igsttotal',`GrandTotal`='$grandtotal' where   `Company`='$company' and `Product`='$product' and `Service`='$service' ");*/

$pdf->Cell(30,5,"",0,1,"C");

$maxdelivery=$delivery+1;

$j=1;
$pdf->SetFont("Times","B",12);
$pdf->Cell(50,10,"Terms & Condition:",0,1);

$pdf->SetFont("Times","",12);

$pdf->Cell(50,6,"$j. Taxes & Duties: All Inclusive",0,1);
$j=$j+1;
$pdf->Cell(50,6,"$j. Payment Terms: Within $payment days from the date date of invoice submission",0,1);
$j=$j+1;
$pdf->Cell(50,6,"$j. Order Cancellation: Orders once placed cannot be cancelled under any circumstances",0,1);
$j=$j+1;
$pdf->Cell(50,6,"$j. Purchase Order: PO to be placed in the name of RABITA SOFTWARE, Bangalore",0,1);
$j=$j+1;
$pdf->Cell(50,6,"$j. Delivery:  Within $delivery-$maxdelivery weeks from the date of receipt of PO",0,1);
$j=$j+1;
$pdf->Cell(50,6,"$j. Quote Validity: This quote is valid for $validity days only",0,1);

$pdf->Cell(50,10,"Please do not hesitate to contact me in case of any clarifications.",0,1);

$pdf->Cell(50,8,"Thank you for giving us opportunity to serve you. Look forward to your valuable order.",0,1);

$pdf->Cell(50,6,"Thanking You,",0,1);

$pdf->Cell(50,6,"For Rabita Software",0,1);

$pdf->Cell(50,6,"$rep",0,1);

$proffet=mysqli_query($dbc,"select * from team where email='$id' ");
while($pr=mysqli_fetch_assoc($proffet))
{
	$mobno=$pr['contact'];
}

$pdf->Cell(50,6,"Ph: $mobno",0,1);


$pdf->Cell(50,3,"-----------------------------------------------------------------------------------------------------------------------------------",0,1);
$pdf->Cell(50,6,'"Badami Arcade", No. 657, 2nd Main, 7th Block, 2nd Phase, BSK 3rd Stage, Bangalore - 560085 ',0,1);
$pdf->Cell(50,6,"Ph: +91 080 26722917, 26722194, 26722413  Email: sales@rabitasoft.com , URL: www.rabitasoft.com",0,1);




// email stuff (change data below)
$to = $rmail; 
$from = "purusottam@navikra.com"; 
$subject = "Proposal for $product to $company"; 
$message = "<p>Please see the attachment.</p>";

// a random hash will be necessary to send mixed content
$separator = md5(time());

// carriage return type (we use a PHP end of line constant)
$eol = PHP_EOL;

// attachment name
$filename = "quotation.pdf";

// encode data (puts attachment in proper format)
$pdfdoc = $pdf->Output("", "S");
$attachment = chunk_split(base64_encode($pdfdoc));

// main header
$headers  = "From: ".$from.$eol;
$headers .= "MIME-Version: 1.0".$eol; 
$headers .= "Content-Type: multipart/mixed; boundary=\"".$separator."\"";

// no more headers after this, we start the body! //

$body = "--".$separator.$eol;
$body .= "Content-Transfer-Encoding: 7bit".$eol.$eol;
$body .= "This is a MIME encoded message.".$eol;

// message
$body .= "--".$separator.$eol;
$body .= "Content-Type: text/html; charset=\"iso-8859-1\"".$eol;
$body .= "Content-Transfer-Encoding: 8bit".$eol.$eol;
$body .= $message.$eol;

// attachment
$body .= "--".$separator.$eol;
$body .= "Content-Type: application/octet-stream; name=\"".$filename."\"".$eol; 
$body .= "Content-Transfer-Encoding: base64".$eol;
$body .= "Content-Disposition: attachment".$eol.$eol;
$body .= $attachment.$eol;
$body .= "--".$separator."--";

// send message
mail($to, $subject, $body, $headers);





$pdf->output();

?>

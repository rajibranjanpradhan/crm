<?php
session_start();
error_reporting(E_PARSE | E_ERROR);
$id=$_SESSION['id'];
if(!$_SESSION['id'])
{
	echo '<script>location.replace("../../../../index.php");</script>';
}

require_once "../../../conn/conn.php";



?>

<?php

$rep=$_SESSION['n'];
    $rmail=$id;

date_default_timezone_set('Asia/Kolkata');

$date = date('d/m/Y');
$year = date('Y');

$quotrefno=$_SESSION['quotrefno'];

$fetchall=mysqli_query($dbc,"select * from `quotation` where `QuotNo`='$quotrefno' ");
$inoprev=mysqli_num_rows($fetchall);
$ino=$inoprev;



while($row=mysqli_fetch_assoc($fetchall))
{
    $company=$row['Company'];
    $product=$row['Product'];
    $tax=$row['Tax'];
    $currency=$row['Currency'];
    

}

   
require("fpdf/fpdf.php");

class myPDF extends FPDF {
    function myCell($w,$h,$x,$t){
        $height=$h/3;
        $first=$height+2;
        $second=$height+$height+$height+3;
        $third=$height+$height+$height+$height+$height+3;
        $len=strlen($t);
        if($len>20){
            $txt=str_split($t,20);
            $this->SetX($x);
            $this->Cell($w,$first,$txt[0],'','','');
            $this->SetX($x);
            $this->Cell($w,$second,$txt[1],'','','');
            $this->SetX($x);
            $this->Cell($w,$third,$txt[2],'','','');
            $this->SetX($x);
            $this->Cell($w,$h,'','LTRB',0,'C',0);
        }
        else{
            $this->SetX($x);
            $this->Cell($w,$h,$t,'LTRB',0,'C',0);
        }
    }

    function myCelldesc($w1,$h,$x,$t){
        $height=$h/3;
        $first=$height+2;
        $second=$height+$height+$height+3;
        $third=$height+$height+$height+$height+$height+3;
        $len=strlen($t);
        if($len>15){
            $txt=str_split($t,15);
            $this->SetX($x);
            $this->Cell($w1,$first,$txt[0],'','','');
            $this->SetX($x);
            $this->Cell($w1,$second,$txt[1],'','','');
            $this->SetX($x);
            $this->Cell($w1,$third,$txt[2],'','','');
            $this->SetX($x);
            $this->Cell($w1,$h,'','LTRB',0,'C',0);
        }
        else{
            $this->SetX($x);
            $this->Cell($w1,$h,$t,'LTRB',0,'C',0);
        }
    }
}


$pdf= new myPDF();
$pdf->AddPage("L","A4");
$pdf->SetFont("Times","",14);
$pdf->Ln();

$pdf->SetFont("Times","B",20);
$pdf->Cell(250,18,"ORDER PROCESSING FORMAT",0,1,"C");

//$pdf->Cell(170,10,"REGISTRATION FORM",0,1,"C");
$pdf->Image('../../../assets/images/logo.png', 230, 10, 60, '', '', '', false, 150);

$pdf->SetFont("Times","B",14);
$pdf->Cell(1,10,"",0,1);

$fetopf=mysqli_query($dbc,"select * from `opf` where `QuotRefNo`='$quotrefno' ");
while($opfr=mysqli_fetch_assoc($fetopf))
{
    $opfno=$opfr['OPFNo'];
}


$pdf->Cell(80,10,"OPF No: $opfno",0,0);
/*$pdf->Cell(80,10,"$b",0,1);*/
$pdf->Cell(80,10,"OPF Date: $date",0,0);

$pdf->Cell(80,10,"Created By Sales Rep: $rep",0,1);
//$pdf->Cell(5,10,"$date",0,1);





/*$pdf->Cell(50,10,"Regd. No:",0,0);
$pdf->Cell(80,10,"$c",0,1);

$pdf->Cell(50,10,"Branch:",0,0);
$pdf->Cell(80,10,"$d",0,1);

$pdf->Cell(50,10,"Sem:",0,0);
$pdf->Cell(80,10,"$e",0,1);*/

$fetcit=mysqli_query($dbc,"select * from customers where `Company`='$company'");
while($frc=mysqli_fetch_assoc($fetcit))
{
    $city=$frc['City'];
}

if($currency == "rupee")
{
    $cur="INR";
}
elseif($currency == "dollar")
{
    $cur="USD";
}
elseif ($currency == "euro") {
    $cur="EUR";
}
elseif ($currency == "pound") {
    $cur="GBP";
}






$pdf->SetFont("Times","B",12);

$pdf->Cell(50,10,"Customer Details",0,1);



$pdf->SetFont("Times","B",12);

$pdf->Cell(17,10,"Sl No.",1,0,"C");
$pdf->Cell(40,10,"Customer Name",1,0,"C");
$pdf->Cell(36,10,"Product(s) ordered",1,0,"C");
$pdf->Cell(33,10,"Part Descriptions",1,0,"C");
$pdf->Cell(20,10,"Qty",1,0,"C");
$pdf->Cell(33,10,"Unit Price($cur)",1,0,"C");
$pdf->Cell(33,10,"Total Price($cur)",1,0,"C");
$pdf->Cell(28,10,"Service Tax",1,0,"C");
$pdf->Cell(40,10,"Grand Total($cur)",1,1,"C");


$pdf->SetFont("Times","",12);
//if(empty(1))
$total1=0;$total2=0;$total3=0;$total4=0;$total5=0;$igsttotal=0;$sgsttotal=0;$cgsttotal=0;$grandtotal=0;$subtotal=0;
$w=40;$h=10;$w1=33;

//$total=$_SESSION['qty1']*$_SESSION['unitprice1'];

    $quotrefno=$_SESSION['quotrefno'];
    $sellername=$_SESSION['sellername'];
    $creditcardname=$_SESSION['creditcardname'];
    $cardnoending=$_SESSION['cardnoending'];
    $whtpercent=$_SESSION['whtpercent'];
    $conversionrate=$_SESSION['conversionrate'];

    $fetcust=mysqli_query($dbc,"select * from quotation where `QuotNo`='$quotrefno' ");

    while($fcustr=mysqli_fetch_assoc($fetcust))
    {
        $company=$fcustr['Company'];

        $product1=$fcustr['Product1'];
        $partdesc1=$fcustr['PartDescription1'];
        $qty1=$fcustr['Quantity1'];
        $uprice1=sprintf("%.2f",$fcustr['UnitPrice1']);

        $product2=$fcustr['Product2'];
        $partdesc2=$fcustr['PartDescription2'];
        $qty2=$fcustr['Quantity2'];
        $uprice2=sprintf("%.2f",$fcustr['UnitPrice2']);

        $product3=$fcustr['Product3'];
        $partdesc3=$fcustr['PartDescription3'];
        $qty3=$fcustr['Quantity3'];
        $uprice3=sprintf("%.2f",$fcustr['UnitPrice3']);

        $product4=$fcustr['Product4'];
        $partdesc4=$fcustr['PartDescription4'];
        $qty4=$fcustr['Quantity4'];
        $uprice4=sprintf("%.2f",$fcustr['UnitPrice4']);

        $product5=$fcustr['Product5'];
        $partdesc5=$fcustr['PartDescription5'];
        $qty5=$fcustr['Quantity5'];
        $uprice5=sprintf("%.2f",$fcustr['UnitPrice5']);

        $product6=$fcustr['Product6'];
        $partdesc6=$fcustr['PartDescription6'];
        $qty6=$fcustr['Quantity6'];
        $uprice6=sprintf("%.2f",$fcustr['UnitPrice6']);

        $product7=$fcustr['Product7'];
        $partdesc7=$fcustr['PartDescription7'];
        $qty7=$fcustr['Quantity7'];
        $uprice7=sprintf("%.2f",$fcustr['UnitPrice7']);

        $product8=$fcustr['Product8'];
        $partdesc8=$fcustr['PartDescription8'];
        $qty8=$fcustr['Quantity8'];
        $uprice8=sprintf("%.2f",$fcustr['UnitPrice8']);

        $product9=$fcustr['Product9'];
        $partdesc9=$fcustr['PartDescription9'];
        $qty9=$fcustr['Quantity9'];
        $uprice9=sprintf("%.2f",$fcustr['UnitPrice9']);

        $product10=$fcustr['Product10'];
        $partdesc10=$fcustr['PartDescription10'];
        $qty10=$fcustr['Quantity10'];
        $uprice10=sprintf("%.2f",$fcustr['UnitPrice10']);


        $currency=$fcustr['Currency'];
        $tax=$fcustr['Tax'];
        
        /*$uprice=sprintf("%.2f",$fcustr['UnitPrice1']);*/
        $subtotal=sprintf("%.2f",$fcustr['SubTotal']);
        $ograndtotal=sprintf("%.2f",$fcustr['GrandTotal']);
        $sgst=$fcustr['SGST'];
        $cgst=$fcustr['CGST'];
        $igst=$fcustr['IGST'];
        if($sgst>0 and $cgst>0)
        {
            $taxval=sprintf("%.2f",$sgst+$cgst);

            

        }
        else
        {
            $taxval=sprintf("%.2f",$igst);

            
        }

        
    }

    $vtaxpercent=sprintf("%.2f",($taxval/$subtotal)*100);

    $grandtotal=sprintf("%.2f",$subtotal+$taxval);

    

    /*$pdf->Cell(17,10,"1",1,0,"C");

$pdf->Cell(64,10,"$company",1,0,"C");
$pdf->Cell(46,10,"$product",1,0,"C");
$pdf->Cell(20,10,"$qty",1,0,"C");
$pdf->Cell(33,10,"$uprice",1,0,"C");
$pdf->Cell(33,10,"$subtotal",1,0,"C");
$pdf->Cell(28,10,"$taxval",1,0,"C");
$pdf->Cell(40,10,"$grandtotal",1,1,"C");*/


/*$x=$pdf->getx();
$pdf->myCell(17,$h,$x,'1');
$x=$pdf->getx();
$pdf->myCell($w,$h,$x,$company);
$x=$pdf->getx();*/
 if(empty($product2) and empty($product3) and empty($product4) and empty($product5) and empty($product6) and empty($product7) and empty($product8) and empty($product9) and empty($product10) )
 {
    $x=$pdf->getx();
    $pdf->myCell(17,$h,$x,'1');
    $x=$pdf->getx();
    $pdf->myCell($w,$h,$x,$company);

    $x=$pdf->getx();
    $pdf->myCell($w,$h,$x,$product1);
    $x=$pdf->getx();
    $pdf->myCell($w,$h,$x,$partdesc1);

    $x=$pdf->getx();
    $pdf->myCell(20,$h,$x,$qty1);
    $x=$pdf->getx();
    $pdf->myCell(33,$h,$x,$uprice1);

    $total1=sprintf("%.2f",$qty1*$uprice1);

    $x=$pdf->getx();
    $pdf->myCell(33,$h,$x,$total1);


    $array =  explode(',', $tax);
$count=0;
foreach ($array as $item) {
    global $count;
    $count=$count+1;
    }

    foreach ($array as $item) {

        $taxperarr =  explode('-', $item);

        foreach ($taxperarr as $per) {
            $taxpercentage=$per;

            }

        $totalwtax1=sprintf("%.2f",($taxpercentage/100)*$total1);

        /*if($count>1)
        {
            $x=$pdf->getx();
            $pdf->myCell(32/2,$h,$x,$taxperarr[0].' '.$taxpercentage.'%');
            

        }
        else
        {
            $x=$pdf->getx();
            $pdf->myCell(32,$h,$x,$taxperarr[0].' '.$taxpercentage.'%');
        }*/

        $taxval1=$grandtotal1+$totalwtax1;

    }

    if($count>1)
        {
            /*$x=$pdf->getx();
            $pdf->myCell(33/2,$h,$x,$totalwtax1);
            $x=$pdf->getx();
            $pdf->myCell(33/2,$h,$x,$totalwtax1);*/
            $taxvaltotal1=$taxval1 + $taxval1;
            
            $x=$pdf->getx();
            $pdf->myCell(28,$h,$x,$taxvaltotal1);

        }
        else
        {
            $taxvaltotal1=$taxval1;

            $x=$pdf->getx();
            $pdf->myCell(28,$h,$x,$taxvaltotal1);
        }


    $grandtotal1=sprintf("%.2f",$total1+$taxvaltotal1);

    
    $x=$pdf->getx();
    $pdf->myCell(40,$h,$x,$grandtotal1);
    $pdf->Ln();
 }
 elseif(empty($product3) and empty($product4) and empty($product5) and empty($product6) and empty($product7) and empty($product8) and empty($product9) and empty($product10) )
 {
    $x=$pdf->getx();
    $pdf->myCell(17,$h,$x,'1');
    $x=$pdf->getx();
    $pdf->myCell($w,$h,$x,$company);

    $x=$pdf->getx();
    $pdf->myCell($w,$h,$x,$product1);
    $x=$pdf->getx();
    $pdf->myCell($w,$h,$x,$partdesc1);

    $x=$pdf->getx();
    $pdf->myCell(20,$h,$x,$qty1);
    $x=$pdf->getx();
    $pdf->myCell(33,$h,$x,$uprice1);

    $total1=sprintf("%.2f",$qty1*$uprice1);

    $x=$pdf->getx();
    $pdf->myCell(33,$h,$x,$total1);


    $array =  explode(',', $tax);
    $count=0;
    foreach ($array as $item) {
    global $count;
    $count=$count+1;
    }

    foreach ($array as $item) {

        $taxperarr =  explode('-', $item);

        foreach ($taxperarr as $per) {
            $taxpercentage=$per;

            }

        $totalwtax1=sprintf("%.2f",($taxpercentage/100)*$total1);

        $taxval1=$grandtotal1+$totalwtax1;

    }

    if($count>1)
        {
           
            $taxvaltotal1=$taxval1 + $taxval1;
            
            $x=$pdf->getx();
            $pdf->myCell(28,$h,$x,$taxvaltotal1);

        }
        else
        {
            $taxvaltotal1=$taxval1;

            $x=$pdf->getx();
            $pdf->myCell(28,$h,$x,$taxvaltotal1);
        }


    $grandtotal1=sprintf("%.2f",$total1+$taxvaltotal1);

    
    $x=$pdf->getx();
    $pdf->myCell(40,$h,$x,$grandtotal1);
    $pdf->Ln();

    $x=$pdf->getx();
    $pdf->myCell(17,$h,$x,'2');
    $x=$pdf->getx();
    $pdf->myCell($w,$h,$x,$company);

    $x=$pdf->getx();
    $pdf->myCell($w,$h,$x,$product2);
    $x=$pdf->getx();
    $pdf->myCell($w,$h,$x,$partdesc2);

    $x=$pdf->getx();
    $pdf->myCell(20,$h,$x,$qty2);
    $x=$pdf->getx();
    $pdf->myCell(33,$h,$x,$uprice2);

    $total2=sprintf("%.2f",$qty2*$uprice2);

    $x=$pdf->getx();
    $pdf->myCell(33,$h,$x,$total2);


    $array =  explode(',', $tax);
    $count=0;
    foreach ($array as $item) {
    global $count;
    $count=$count+1;
    }

    foreach ($array as $item) {

        $taxperarr =  explode('-', $item);

        foreach ($taxperarr as $per) {
            $taxpercentage=$per;

            }

        $totalwtax2=sprintf("%.2f",($taxpercentage/100)*$total2);

        $taxval2=$grandtotal2+$totalwtax2;

    }

    if($count>1)
        {
           
            $taxvaltotal2=$taxval2 + $taxval2;
            
            $x=$pdf->getx();
            $pdf->myCell(28,$h,$x,$taxvaltotal2);

        }
        else
        {
            $taxvaltotal2=$taxval2;

            $x=$pdf->getx();
            $pdf->myCell(28,$h,$x,$taxvaltotal2);
        }


    $grandtotal2=sprintf("%.2f",$total2+$taxvaltotal2);

    
    $x=$pdf->getx();
    $pdf->myCell(40,$h,$x,$grandtotal2);
    $pdf->Ln();


 }
 elseif(empty($product4) and empty($product5) and empty($product6) and empty($product7) and empty($product8) and empty($product9) and empty($product10) )
 {
    $x=$pdf->getx();
    $pdf->myCell(17,$h,$x,'1');
    $x=$pdf->getx();
    $pdf->myCell($w,$h,$x,$company);

    $x=$pdf->getx();
    $pdf->myCell($w,$h,$x,$product1);
    $x=$pdf->getx();
    $pdf->myCell($w,$h,$x,$partdesc1);

    $x=$pdf->getx();
    $pdf->myCell(20,$h,$x,$qty1);
    $x=$pdf->getx();
    $pdf->myCell(33,$h,$x,$uprice1);

    $total1=sprintf("%.2f",$qty1*$uprice1);

    $x=$pdf->getx();
    $pdf->myCell(33,$h,$x,$total1);


    $array =  explode(',', $tax);
    $count=0;
    foreach ($array as $item) {
    global $count;
    $count=$count+1;
    }

    foreach ($array as $item) {

        $taxperarr =  explode('-', $item);

        foreach ($taxperarr as $per) {
            $taxpercentage=$per;

            }

        $totalwtax1=sprintf("%.2f",($taxpercentage/100)*$total1);

        $taxval1=$grandtotal1+$totalwtax1;

    }

    if($count>1)
        {
           
            $taxvaltotal1=$taxval1 + $taxval1;
            
            $x=$pdf->getx();
            $pdf->myCell(28,$h,$x,$taxvaltotal1);

        }
        else
        {
            $taxvaltotal1=$taxval1;

            $x=$pdf->getx();
            $pdf->myCell(28,$h,$x,$taxvaltotal1);
        }


    $grandtotal1=sprintf("%.2f",$total1+$taxvaltotal1);

    
    $x=$pdf->getx();
    $pdf->myCell(40,$h,$x,$grandtotal1);
    $pdf->Ln();

    $x=$pdf->getx();
    $pdf->myCell(17,$h,$x,'2');
    $x=$pdf->getx();
    $pdf->myCell($w,$h,$x,$company);

    $x=$pdf->getx();
    $pdf->myCell($w,$h,$x,$product2);
    $x=$pdf->getx();
    $pdf->myCell($w,$h,$x,$partdesc2);

    $x=$pdf->getx();
    $pdf->myCell(20,$h,$x,$qty2);
    $x=$pdf->getx();
    $pdf->myCell(33,$h,$x,$uprice2);

    $total2=sprintf("%.2f",$qty2*$uprice2);

    $x=$pdf->getx();
    $pdf->myCell(33,$h,$x,$total2);


    $array =  explode(',', $tax);
    $count=0;
    foreach ($array as $item) {
    global $count;
    $count=$count+1;
    }

    foreach ($array as $item) {

        $taxperarr =  explode('-', $item);

        foreach ($taxperarr as $per) {
            $taxpercentage=$per;

            }

        $totalwtax2=sprintf("%.2f",($taxpercentage/100)*$total2);

        $taxval2=$grandtotal2+$totalwtax2;

    }

    if($count>1)
        {
           
            $taxvaltotal2=$taxval2 + $taxval2;
            
            $x=$pdf->getx();
            $pdf->myCell(28,$h,$x,$taxvaltotal2);

        }
        else
        {
            $taxvaltotal2=$taxval2;

            $x=$pdf->getx();
            $pdf->myCell(28,$h,$x,$taxvaltotal2);
        }


    $grandtotal2=sprintf("%.2f",$total2+$taxvaltotal2);

    
    $x=$pdf->getx();
    $pdf->myCell(40,$h,$x,$grandtotal2);
    $pdf->Ln();

    $x=$pdf->getx();
    $pdf->myCell(17,$h,$x,'3');
    $x=$pdf->getx();
    $pdf->myCell($w,$h,$x,$company);

    $x=$pdf->getx();
    $pdf->myCell($w,$h,$x,$product3);
    $x=$pdf->getx();
    $pdf->myCell($w,$h,$x,$partdesc3);

    $x=$pdf->getx();
    $pdf->myCell(20,$h,$x,$qty3);
    $x=$pdf->getx();
    $pdf->myCell(33,$h,$x,$uprice3);

    $total3=sprintf("%.2f",$qty3*$uprice3);

    $x=$pdf->getx();
    $pdf->myCell(33,$h,$x,$total3);


    $array =  explode(',', $tax);
    $count=0;
    foreach ($array as $item) {
    global $count;
    $count=$count+1;
    }

    foreach ($array as $item) {

        $taxperarr =  explode('-', $item);

        foreach ($taxperarr as $per) {
            $taxpercentage=$per;

            }

        $totalwtax3=sprintf("%.2f",($taxpercentage/100)*$total3);

        $taxval3=$grandtotal3+$totalwtax3;

    }

    if($count>1)
        {
           
            $taxvaltotal3=$taxval3 + $taxval3;
            
            $x=$pdf->getx();
            $pdf->myCell(28,$h,$x,$taxvaltotal3);

        }
        else
        {
            $taxvaltotal3=$taxval3;

            $x=$pdf->getx();
            $pdf->myCell(28,$h,$x,$taxvaltotal3);
        }


    $grandtotal3=sprintf("%.2f",$total3+$taxvaltotal3);

    
    $x=$pdf->getx();
    $pdf->myCell(40,$h,$x,$grandtotal3);
    $pdf->Ln();
 }
 elseif(empty($product5) and empty($product6) and empty($product7) and empty($product8) and empty($product9) and empty($product10) )
 {
    $x=$pdf->getx();
    $pdf->myCell(17,$h,$x,'1');
    $x=$pdf->getx();
    $pdf->myCell($w,$h,$x,$company);

    $x=$pdf->getx();
    $pdf->myCell($w,$h,$x,$product1);
    $x=$pdf->getx();
    $pdf->myCell($w,$h,$x,$partdesc1);

    $x=$pdf->getx();
    $pdf->myCell(20,$h,$x,$qty1);
    $x=$pdf->getx();
    $pdf->myCell(33,$h,$x,$uprice1);

    $total1=sprintf("%.2f",$qty1*$uprice1);

    $x=$pdf->getx();
    $pdf->myCell(33,$h,$x,$total1);


    $array =  explode(',', $tax);
    $count=0;
    foreach ($array as $item) {
    global $count;
    $count=$count+1;
    }

    foreach ($array as $item) {

        $taxperarr =  explode('-', $item);

        foreach ($taxperarr as $per) {
            $taxpercentage=$per;

            }

        $totalwtax1=sprintf("%.2f",($taxpercentage/100)*$total1);

        $taxval1=$grandtotal1+$totalwtax1;

    }

    if($count>1)
        {
           
            $taxvaltotal1=$taxval1 + $taxval1;
            
            $x=$pdf->getx();
            $pdf->myCell(28,$h,$x,$taxvaltotal1);

        }
        else
        {
            $taxvaltotal1=$taxval1;

            $x=$pdf->getx();
            $pdf->myCell(28,$h,$x,$taxvaltotal1);
        }


    $grandtotal1=sprintf("%.2f",$total1+$taxvaltotal1);

    
    $x=$pdf->getx();
    $pdf->myCell(40,$h,$x,$grandtotal1);
    $pdf->Ln();

    $x=$pdf->getx();
    $pdf->myCell(17,$h,$x,'2');
    $x=$pdf->getx();
    $pdf->myCell($w,$h,$x,$company);

    $x=$pdf->getx();
    $pdf->myCell($w,$h,$x,$product2);
    $x=$pdf->getx();
    $pdf->myCell($w,$h,$x,$partdesc2);

    $x=$pdf->getx();
    $pdf->myCell(20,$h,$x,$qty2);
    $x=$pdf->getx();
    $pdf->myCell(33,$h,$x,$uprice2);

    $total2=sprintf("%.2f",$qty2*$uprice2);

    $x=$pdf->getx();
    $pdf->myCell(33,$h,$x,$total2);


    $array =  explode(',', $tax);
    $count=0;
    foreach ($array as $item) {
    global $count;
    $count=$count+1;
    }

    foreach ($array as $item) {

        $taxperarr =  explode('-', $item);

        foreach ($taxperarr as $per) {
            $taxpercentage=$per;

            }

        $totalwtax2=sprintf("%.2f",($taxpercentage/100)*$total2);

        $taxval2=$grandtotal2+$totalwtax2;

    }

    if($count>1)
        {
           
            $taxvaltotal2=$taxval2 + $taxval2;
            
            $x=$pdf->getx();
            $pdf->myCell(28,$h,$x,$taxvaltotal2);

        }
        else
        {
            $taxvaltotal2=$taxval2;

            $x=$pdf->getx();
            $pdf->myCell(28,$h,$x,$taxvaltotal2);
        }


    $grandtotal2=sprintf("%.2f",$total2+$taxvaltotal2);

    
    $x=$pdf->getx();
    $pdf->myCell(40,$h,$x,$grandtotal2);
    $pdf->Ln();

    $x=$pdf->getx();
    $pdf->myCell(17,$h,$x,'3');
    $x=$pdf->getx();
    $pdf->myCell($w,$h,$x,$company);

    $x=$pdf->getx();
    $pdf->myCell($w,$h,$x,$product3);
    $x=$pdf->getx();
    $pdf->myCell($w,$h,$x,$partdesc3);

    $x=$pdf->getx();
    $pdf->myCell(20,$h,$x,$qty3);
    $x=$pdf->getx();
    $pdf->myCell(33,$h,$x,$uprice3);

    $total3=sprintf("%.2f",$qty3*$uprice3);

    $x=$pdf->getx();
    $pdf->myCell(33,$h,$x,$total3);


    $array =  explode(',', $tax);
    $count=0;
    foreach ($array as $item) {
    global $count;
    $count=$count+1;
    }

    foreach ($array as $item) {

        $taxperarr =  explode('-', $item);

        foreach ($taxperarr as $per) {
            $taxpercentage=$per;

            }

        $totalwtax3=sprintf("%.2f",($taxpercentage/100)*$total3);

        $taxval3=$grandtotal3+$totalwtax3;

    }

    if($count>1)
        {
           
            $taxvaltotal3=$taxval3 + $taxval3;
            
            $x=$pdf->getx();
            $pdf->myCell(28,$h,$x,$taxvaltotal3);

        }
        else
        {
            $taxvaltotal3=$taxval3;

            $x=$pdf->getx();
            $pdf->myCell(28,$h,$x,$taxvaltotal3);
        }


    $grandtotal3=sprintf("%.2f",$total3+$taxvaltotal3);

    
    $x=$pdf->getx();
    $pdf->myCell(40,$h,$x,$grandtotal3);
    $pdf->Ln();

    $x=$pdf->getx();
    $pdf->myCell(17,$h,$x,'4');
    $x=$pdf->getx();
    $pdf->myCell($w,$h,$x,$company);

    $x=$pdf->getx();
    $pdf->myCell($w,$h,$x,$product4);
    $x=$pdf->getx();
    $pdf->myCell($w,$h,$x,$partdesc4);

    $x=$pdf->getx();
    $pdf->myCell(20,$h,$x,$qty4);
    $x=$pdf->getx();
    $pdf->myCell(33,$h,$x,$uprice4);

    $total3=sprintf("%.2f",$qty3*$uprice4);

    $x=$pdf->getx();
    $pdf->myCell(33,$h,$x,$total4);


    $array =  explode(',', $tax);
    $count=0;
    foreach ($array as $item) {
    global $count;
    $count=$count+1;
    }

    foreach ($array as $item) {

        $taxperarr =  explode('-', $item);

        foreach ($taxperarr as $per) {
            $taxpercentage=$per;

            }

        $totalwtax4=sprintf("%.2f",($taxpercentage/100)*$total4);

        $taxval4=$grandtotal4+$totalwtax4;

    }

    if($count>1)
        {
           
            $taxvaltotal4=$taxval4 + $taxval4;
            
            $x=$pdf->getx();
            $pdf->myCell(28,$h,$x,$taxvaltotal4);

        }
        else
        {
            $taxvaltotal4=$taxval4;

            $x=$pdf->getx();
            $pdf->myCell(28,$h,$x,$taxvaltotal4);
        }


    $grandtotal4=sprintf("%.2f",$total4+$taxvaltotal4);

    
    $x=$pdf->getx();
    $pdf->myCell(40,$h,$x,$grandtotal4);
    $pdf->Ln();
 }
 elseif(empty($product6) and empty($product7) and empty($product8) and empty($product9) and empty($product10) )
 {
    $x=$pdf->getx();
    $pdf->myCell(17,$h,$x,'1');
    $x=$pdf->getx();
    $pdf->myCell($w,$h,$x,$company);

    $x=$pdf->getx();
    $pdf->myCell($w,$h,$x,$product1);
    $x=$pdf->getx();
    $pdf->myCell($w,$h,$x,$partdesc1);

    $x=$pdf->getx();
    $pdf->myCell(20,$h,$x,$qty1);
    $x=$pdf->getx();
    $pdf->myCell(33,$h,$x,$uprice1);

    $total1=sprintf("%.2f",$qty1*$uprice1);

    $x=$pdf->getx();
    $pdf->myCell(33,$h,$x,$total1);


    $array =  explode(',', $tax);
    $count=0;
    foreach ($array as $item) {
    global $count;
    $count=$count+1;
    }

    foreach ($array as $item) {

        $taxperarr =  explode('-', $item);

        foreach ($taxperarr as $per) {
            $taxpercentage=$per;

            }

        $totalwtax1=sprintf("%.2f",($taxpercentage/100)*$total1);

        $taxval1=$grandtotal1+$totalwtax1;

    }

    if($count>1)
        {
           
            $taxvaltotal1=$taxval1 + $taxval1;
            
            $x=$pdf->getx();
            $pdf->myCell(28,$h,$x,$taxvaltotal1);

        }
        else
        {
            $taxvaltotal1=$taxval1;

            $x=$pdf->getx();
            $pdf->myCell(28,$h,$x,$taxvaltotal1);
        }


    $grandtotal1=sprintf("%.2f",$total1+$taxvaltotal1);

    
    $x=$pdf->getx();
    $pdf->myCell(40,$h,$x,$grandtotal1);
    $pdf->Ln();

    $x=$pdf->getx();
    $pdf->myCell(17,$h,$x,'2');
    $x=$pdf->getx();
    $pdf->myCell($w,$h,$x,$company);

    $x=$pdf->getx();
    $pdf->myCell($w,$h,$x,$product2);
    $x=$pdf->getx();
    $pdf->myCell($w,$h,$x,$partdesc2);

    $x=$pdf->getx();
    $pdf->myCell(20,$h,$x,$qty2);
    $x=$pdf->getx();
    $pdf->myCell(33,$h,$x,$uprice2);

    $total2=sprintf("%.2f",$qty2*$uprice2);

    $x=$pdf->getx();
    $pdf->myCell(33,$h,$x,$total2);


    $array =  explode(',', $tax);
    $count=0;
    foreach ($array as $item) {
    global $count;
    $count=$count+1;
    }

    foreach ($array as $item) {

        $taxperarr =  explode('-', $item);

        foreach ($taxperarr as $per) {
            $taxpercentage=$per;

            }

        $totalwtax2=sprintf("%.2f",($taxpercentage/100)*$total2);

        $taxval2=$grandtotal2+$totalwtax2;

    }

    if($count>1)
        {
           
            $taxvaltotal2=$taxval2 + $taxval2;
            
            $x=$pdf->getx();
            $pdf->myCell(28,$h,$x,$taxvaltotal2);

        }
        else
        {
            $taxvaltotal2=$taxval2;

            $x=$pdf->getx();
            $pdf->myCell(28,$h,$x,$taxvaltotal2);
        }


    $grandtotal2=sprintf("%.2f",$total2+$taxvaltotal2);

    
    $x=$pdf->getx();
    $pdf->myCell(40,$h,$x,$grandtotal2);
    $pdf->Ln();

    $x=$pdf->getx();
    $pdf->myCell(17,$h,$x,'3');
    $x=$pdf->getx();
    $pdf->myCell($w,$h,$x,$company);

    $x=$pdf->getx();
    $pdf->myCell($w,$h,$x,$product3);
    $x=$pdf->getx();
    $pdf->myCell($w,$h,$x,$partdesc3);

    $x=$pdf->getx();
    $pdf->myCell(20,$h,$x,$qty3);
    $x=$pdf->getx();
    $pdf->myCell(33,$h,$x,$uprice3);

    $total3=sprintf("%.2f",$qty3*$uprice3);

    $x=$pdf->getx();
    $pdf->myCell(33,$h,$x,$total3);


    $array =  explode(',', $tax);
    $count=0;
    foreach ($array as $item) {
    global $count;
    $count=$count+1;
    }

    foreach ($array as $item) {

        $taxperarr =  explode('-', $item);

        foreach ($taxperarr as $per) {
            $taxpercentage=$per;

            }

        $totalwtax3=sprintf("%.2f",($taxpercentage/100)*$total3);

        $taxval3=$grandtotal3+$totalwtax3;

    }

    if($count>1)
        {
           
            $taxvaltotal3=$taxval3 + $taxval3;
            
            $x=$pdf->getx();
            $pdf->myCell(28,$h,$x,$taxvaltotal3);

        }
        else
        {
            $taxvaltotal3=$taxval3;

            $x=$pdf->getx();
            $pdf->myCell(28,$h,$x,$taxvaltotal3);
        }


    $grandtotal3=sprintf("%.2f",$total3+$taxvaltotal3);

    
    $x=$pdf->getx();
    $pdf->myCell(40,$h,$x,$grandtotal3);
    $pdf->Ln();

    $x=$pdf->getx();
    $pdf->myCell(17,$h,$x,'4');
    $x=$pdf->getx();
    $pdf->myCell($w,$h,$x,$company);

    $x=$pdf->getx();
    $pdf->myCell($w,$h,$x,$product4);
    $x=$pdf->getx();
    $pdf->myCell($w,$h,$x,$partdesc4);

    $x=$pdf->getx();
    $pdf->myCell(20,$h,$x,$qty4);
    $x=$pdf->getx();
    $pdf->myCell(33,$h,$x,$uprice4);

    $total3=sprintf("%.2f",$qty3*$uprice4);

    $x=$pdf->getx();
    $pdf->myCell(33,$h,$x,$total4);


    $array =  explode(',', $tax);
    $count=0;
    foreach ($array as $item) {
    global $count;
    $count=$count+1;
    }

    foreach ($array as $item) {

        $taxperarr =  explode('-', $item);

        foreach ($taxperarr as $per) {
            $taxpercentage=$per;

            }

        $totalwtax4=sprintf("%.2f",($taxpercentage/100)*$total4);

        $taxval4=$grandtotal4+$totalwtax4;

    }

    if($count>1)
        {
           
            $taxvaltotal4=$taxval4 + $taxval4;
            
            $x=$pdf->getx();
            $pdf->myCell(28,$h,$x,$taxvaltotal4);

        }
        else
        {
            $taxvaltotal4=$taxval4;

            $x=$pdf->getx();
            $pdf->myCell(28,$h,$x,$taxvaltotal4);
        }


    $grandtotal4=sprintf("%.2f",$total4+$taxvaltotal4);

    
    $x=$pdf->getx();
    $pdf->myCell(40,$h,$x,$grandtotal4);
    $pdf->Ln();

    $x=$pdf->getx();
    $pdf->myCell(17,$h,$x,'5');
    $x=$pdf->getx();
    $pdf->myCell($w,$h,$x,$company);

    $x=$pdf->getx();
    $pdf->myCell($w,$h,$x,$product5);
    $x=$pdf->getx();
    $pdf->myCell($w,$h,$x,$partdesc5);

    $x=$pdf->getx();
    $pdf->myCell(20,$h,$x,$qty5);
    $x=$pdf->getx();
    $pdf->myCell(33,$h,$x,$uprice5);

    $total3=sprintf("%.2f",$qty3*$uprice5);

    $x=$pdf->getx();
    $pdf->myCell(33,$h,$x,$total5);


    $array =  explode(',', $tax);
    $count=0;
    foreach ($array as $item) {
    global $count;
    $count=$count+1;
    }

    foreach ($array as $item) {

        $taxperarr =  explode('-', $item);

        foreach ($taxperarr as $per) {
            $taxpercentage=$per;

            }

        $totalwtax5=sprintf("%.2f",($taxpercentage/100)*$total5);

        $taxval5=$grandtotal5+$totalwtax5;

    }

    if($count>1)
        {
           
            $taxvaltotal5=$taxval5 + $taxval5;
            
            $x=$pdf->getx();
            $pdf->myCell(28,$h,$x,$taxvaltotal5);

        }
        else
        {
            $taxvaltotal5=$taxval5;

            $x=$pdf->getx();
            $pdf->myCell(28,$h,$x,$taxvaltotal5);
        }


    $grandtotal5=sprintf("%.2f",$total5+$taxvaltotal5);

    
    $x=$pdf->getx();
    $pdf->myCell(40,$h,$x,$grandtotal5);
    $pdf->Ln();
 }
 elseif(empty($product7) and empty($product8) and empty($product9) and empty($product10) )
 {
    $pdf->myCell(36,$h,$x,$product1.",".$product2.",".$product3.",".$product4.",".$product5.",".$product6);
    $x=$pdf->getx();
    $pdf->myCell(33,$h,$x,$partdesc1.",".$partdesc2.",".$partdesc3.",".$partdesc4.",".$partdesc5.",".$partdesc6);
 }
 elseif(empty($product8) and empty($product9) and empty($product10) )
 {
    $pdf->myCell(36,$h,$x,$product1.",".$product2.",".$product3.",".$product4.",".$product5.",".$product6.",".$product7);
    $x=$pdf->getx();
    $pdf->myCell(33,$h,$x,$partdesc1.",".$partdesc2.",".$partdesc3.",".$partdesc4.",".$partdesc5.",".$partdesc6.",".$partdesc7);
 }
 elseif(empty($product9) and empty($product10) )
 {
    $pdf->myCell(36,$h,$x,$product1.",".$product2.",".$product3.",".$product4.",".$product5.",".$product6.",".$product7.",".$product8);
    $x=$pdf->getx();
    $pdf->myCell(33,$h,$x,$partdesc1.",".$partdesc2.",".$partdesc3.",".$partdesc4.",".$partdesc5.",".$partdesc6.",".$partdesc7.",".$partdesc8);
 }
 elseif(empty($product10) )
 {
    $pdf->myCell(36,$h,$x,$product1.",".$product2.",".$product3.",".$product4.",".$product5.",".$product6.",".$product7.",".$product8.",".$product9);
    $x=$pdf->getx();
    $pdf->myCell(33,$h,$x,$partdesc1.",".$partdesc2.",".$partdesc3.",".$partdesc4.",".$partdesc5.",".$partdesc6.",".$partdesc7.",".$partdesc8.",".$partdesc9);
 }
 else
 {
    $pdf->myCell(36,$h,$x,$product1.",".$product2.",".$product3.",".$product4.",".$product5.",".$product6.",".$product7.",".$product8.",".$product9.",".$product10);
    $x=$pdf->getx();
    $pdf->myCell(33,$h,$x,$partdesc1.",".$partdesc2.",".$partdesc3.",".$partdesc4.",".$partdesc5.",".$partdesc6.",".$partdesc7.",".$partdesc8.",".$partdesc9.",".$partdesc10);
 }
 

    $x=$pdf->getx();
    $pdf->myCell(17,$h,$x,'');
    $x=$pdf->getx();
    $pdf->myCell($w,$h,$x,"");

    $x=$pdf->getx();
    $pdf->myCell($w,$h,$x,"");
    $x=$pdf->getx();
    $pdf->myCell($w,$h,$x,"");

    $x=$pdf->getx();
    $pdf->myCell(20,$h,$x,"");
    $x=$pdf->getx();
    $pdf->myCell(33,$h,$x,"TOTAL");

    $totalall=sprintf("%.2f",$total1+$total2+$total3+$total4+$total5+$total6+$total7+$total8+$total9+$total10);
    $x=$pdf->getx();
    $pdf->myCell(33,$h,$x,$totalall);

    $totaltax=$taxvaltotal1+$taxvaltotal2+$taxvaltotal3+$taxvaltotal4+$taxvaltotal5+$taxvaltotal6+$taxvaltotal7+$taxvaltotal8+$taxvaltotal9+$taxvaltotal10;
    $x=$pdf->getx();
    $pdf->myCell(28,$h,$x,$totaltax);

    $grandtotal=sprintf("%.2f",$grandtotal1+$grandtotal2+$grandtotal3+$grandtotal4+$grandtotal5+$grandtotal6+$grandtotal7+$grandtotal8+$grandtotal9+$grandtotal10);
    $x=$pdf->getx();
    $pdf->myCell(40,$h,$x,$grandtotal);
    $pdf->Ln();
 




/*$x=$pdf->getx();
$pdf->myCell(20,$h,$x,$qty);
$x=$pdf->getx();
$pdf->myCell(33,$h,$x,$uprice);
$x=$pdf->getx();
$pdf->myCell(33,$h,$x,$subtotal);
$x=$pdf->getx();
$pdf->myCell(28,$h,$x,$taxval);
$x=$pdf->getx();
$pdf->myCell(40,$h,$x,$grandtotal);
$pdf->Ln();*/



$fetproddet=mysqli_query($dbc,"select * from `products` where `ProductName`='$product' ");
while($fpr=mysqli_fetch_assoc($fetproddet))
{
    $productmargin=$fpr['ProductMargin'];
}

$vuprice=sprintf("%.2f",$uprice-(($productmargin/100)*$uprice));
$vtotalprice=sprintf("%.2f",$qty*$vuprice);
$vtaxval=sprintf("%.2f",($vtaxpercent/100)*$vtotalprice);
$vgrandtotal=sprintf("%.2f",$vtotalprice+$vtaxval);


$pdf->SetFont("Times","B",12);
$pdf->Cell(50,10,"Vendor/Seller Details",0,1);


$pdf->Cell(17,$h,"Sl No.",1,0,"C");
/*$pdf->Cell(40,10,"Product",1,0,"C");*/
$pdf->Cell($w,$h,"Vendor Name",1,0,"C");
$pdf->Cell($w,$h,"Product(s) ordered",1,0,"C");
$pdf->Cell($w,$h,"Part Descriptions",1,0,"C");
$pdf->Cell(20,$h,"Qty",1,0,"C");
$pdf->Cell(33,$h,"Unit Price($cur)",1,0,"C");
$pdf->Cell(33,$h,"Total Price($cur)",1,0,"C");
$pdf->Cell(28,$h,"Service Tax",1,0,"C");
$pdf->Cell(40,$h,"Grand Total($cur)",1,1,"C");



$pdf->SetFont("Times","",12);

/*$pdf->Cell(17,10,"1",1,0,"C");

$pdf->Cell(64,10,"$sellername",1,0,"C");
$pdf->Cell(46,10,"$product",1,0,"C");
$pdf->Cell(20,10,"$qty",1,0,"C");
$pdf->Cell(33,10,"$vuprice",1,0,"C");
$pdf->Cell(33,10,"$vtotalprice",1,0,"C");
$pdf->Cell(28,10,"$vtaxval",1,0,"C");
$pdf->Cell(40,10,"$vgrandtotal",1,1,"C");*/


if(empty($product2) and empty($product3) and empty($product4) and empty($product5) and empty($product6) and empty($product7) and empty($product8) and empty($product9) and empty($product10) )
 {
    $x=$pdf->getx();
    $pdf->myCell(17,$h,$x,'1');
    $x=$pdf->getx();
    $pdf->myCell($w,$h,$x,$sellername);

    $x=$pdf->getx();
    $pdf->myCell($w,$h,$x,$product1);
    $x=$pdf->getx();
    $pdf->myCell($w,$h,$x,$partdesc1);

    $x=$pdf->getx();
    $pdf->myCell(20,$h,$x,$qty1);
    $x=$pdf->getx();
    $pdf->myCell(33,$h,$x,$uprice1);

    $total1=sprintf("%.2f",$qty1*$uprice1);

    $x=$pdf->getx();
    $pdf->myCell(33,$h,$x,$total1);


    $array =  explode(',', $tax);
$count=0;
foreach ($array as $item) {
    global $count;
    $count=$count+1;
    }

    foreach ($array as $item) {

        $taxperarr =  explode('-', $item);

        foreach ($taxperarr as $per) {
            $taxpercentage=$per;

            }

        $totalwtax1=sprintf("%.2f",($taxpercentage/100)*$total1);

        /*if($count>1)
        {
            $x=$pdf->getx();
            $pdf->myCell(32/2,$h,$x,$taxperarr[0].' '.$taxpercentage.'%');
            

        }
        else
        {
            $x=$pdf->getx();
            $pdf->myCell(32,$h,$x,$taxperarr[0].' '.$taxpercentage.'%');
        }*/

        $taxval1=$grandtotal1+$totalwtax1;

    }

    if($count>1)
        {
            /*$x=$pdf->getx();
            $pdf->myCell(33/2,$h,$x,$totalwtax1);
            $x=$pdf->getx();
            $pdf->myCell(33/2,$h,$x,$totalwtax1);*/
            $taxvaltotal1=$taxval1 + $taxval1;
            
            $x=$pdf->getx();
            $pdf->myCell(28,$h,$x,$taxvaltotal1);

        }
        else
        {
            $taxvaltotal1=$taxval1;

            $x=$pdf->getx();
            $pdf->myCell(28,$h,$x,$taxvaltotal1);
        }


    $grandtotal1=sprintf("%.2f",$total1+$taxvaltotal1);

    
    $x=$pdf->getx();
    $pdf->myCell(40,$h,$x,$grandtotal1);
    $pdf->Ln();
 }
 elseif(empty($product3) and empty($product4) and empty($product5) and empty($product6) and empty($product7) and empty($product8) and empty($product9) and empty($product10) )
 {
    $x=$pdf->getx();
    $pdf->myCell(17,$h,$x,'1');
    $x=$pdf->getx();
    $pdf->myCell($w,$h,$x,$sellername);

    $x=$pdf->getx();
    $pdf->myCell($w,$h,$x,$product1);
    $x=$pdf->getx();
    $pdf->myCell($w,$h,$x,$partdesc1);

    $x=$pdf->getx();
    $pdf->myCell(20,$h,$x,$qty1);
    $x=$pdf->getx();
    $pdf->myCell(33,$h,$x,$uprice1);

    $total1=sprintf("%.2f",$qty1*$uprice1);

    $x=$pdf->getx();
    $pdf->myCell(33,$h,$x,$total1);


    $array =  explode(',', $tax);
    $count=0;
    foreach ($array as $item) {
    global $count;
    $count=$count+1;
    }

    foreach ($array as $item) {

        $taxperarr =  explode('-', $item);

        foreach ($taxperarr as $per) {
            $taxpercentage=$per;

            }

        $totalwtax1=sprintf("%.2f",($taxpercentage/100)*$total1);

        $taxval1=$grandtotal1+$totalwtax1;

    }

    if($count>1)
        {
           
            $taxvaltotal1=$taxval1 + $taxval1;
            
            $x=$pdf->getx();
            $pdf->myCell(28,$h,$x,$taxvaltotal1);

        }
        else
        {
            $taxvaltotal1=$taxval1;

            $x=$pdf->getx();
            $pdf->myCell(28,$h,$x,$taxvaltotal1);
        }


    $grandtotal1=sprintf("%.2f",$total1+$taxvaltotal1);

    
    $x=$pdf->getx();
    $pdf->myCell(40,$h,$x,$grandtotal1);
    $pdf->Ln();

    $x=$pdf->getx();
    $pdf->myCell(17,$h,$x,'2');
    $x=$pdf->getx();
    $pdf->myCell($w,$h,$x,$sellername);

    $x=$pdf->getx();
    $pdf->myCell($w,$h,$x,$product2);
    $x=$pdf->getx();
    $pdf->myCell($w,$h,$x,$partdesc2);

    $x=$pdf->getx();
    $pdf->myCell(20,$h,$x,$qty2);
    $x=$pdf->getx();
    $pdf->myCell(33,$h,$x,$uprice2);

    $total2=sprintf("%.2f",$qty2*$uprice2);

    $x=$pdf->getx();
    $pdf->myCell(33,$h,$x,$total2);


    $array =  explode(',', $tax);
    $count=0;
    foreach ($array as $item) {
    global $count;
    $count=$count+1;
    }

    foreach ($array as $item) {

        $taxperarr =  explode('-', $item);

        foreach ($taxperarr as $per) {
            $taxpercentage=$per;

            }

        $totalwtax2=sprintf("%.2f",($taxpercentage/100)*$total2);

        $taxval2=$grandtotal2+$totalwtax2;

    }

    if($count>1)
        {
           
            $taxvaltotal2=$taxval2 + $taxval2;
            
            $x=$pdf->getx();
            $pdf->myCell(28,$h,$x,$taxvaltotal2);

        }
        else
        {
            $taxvaltotal2=$taxval2;

            $x=$pdf->getx();
            $pdf->myCell(28,$h,$x,$taxvaltotal2);
        }


    $grandtotal2=sprintf("%.2f",$total2+$taxvaltotal2);

    
    $x=$pdf->getx();
    $pdf->myCell(40,$h,$x,$grandtotal2);
    $pdf->Ln();


 }
 elseif(empty($product4) and empty($product5) and empty($product6) and empty($product7) and empty($product8) and empty($product9) and empty($product10) )
 {
    $x=$pdf->getx();
    $pdf->myCell(17,$h,$x,'1');
    $x=$pdf->getx();
    $pdf->myCell($w,$h,$x,$sellername);

    $x=$pdf->getx();
    $pdf->myCell($w,$h,$x,$product1);
    $x=$pdf->getx();
    $pdf->myCell($w,$h,$x,$partdesc1);

    $x=$pdf->getx();
    $pdf->myCell(20,$h,$x,$qty1);
    $x=$pdf->getx();
    $pdf->myCell(33,$h,$x,$uprice1);

    $total1=sprintf("%.2f",$qty1*$uprice1);

    $x=$pdf->getx();
    $pdf->myCell(33,$h,$x,$total1);


    $array =  explode(',', $tax);
    $count=0;
    foreach ($array as $item) {
    global $count;
    $count=$count+1;
    }

    foreach ($array as $item) {

        $taxperarr =  explode('-', $item);

        foreach ($taxperarr as $per) {
            $taxpercentage=$per;

            }

        $totalwtax1=sprintf("%.2f",($taxpercentage/100)*$total1);

        $taxval1=$grandtotal1+$totalwtax1;

    }

    if($count>1)
        {
           
            $taxvaltotal1=$taxval1 + $taxval1;
            
            $x=$pdf->getx();
            $pdf->myCell(28,$h,$x,$taxvaltotal1);

        }
        else
        {
            $taxvaltotal1=$taxval1;

            $x=$pdf->getx();
            $pdf->myCell(28,$h,$x,$taxvaltotal1);
        }


    $grandtotal1=sprintf("%.2f",$total1+$taxvaltotal1);

    
    $x=$pdf->getx();
    $pdf->myCell(40,$h,$x,$grandtotal1);
    $pdf->Ln();

    $x=$pdf->getx();
    $pdf->myCell(17,$h,$x,'2');
    $x=$pdf->getx();
    $pdf->myCell($w,$h,$x,$sellername);

    $x=$pdf->getx();
    $pdf->myCell($w,$h,$x,$product2);
    $x=$pdf->getx();
    $pdf->myCell($w,$h,$x,$partdesc2);

    $x=$pdf->getx();
    $pdf->myCell(20,$h,$x,$qty2);
    $x=$pdf->getx();
    $pdf->myCell(33,$h,$x,$uprice2);

    $total2=sprintf("%.2f",$qty2*$uprice2);

    $x=$pdf->getx();
    $pdf->myCell(33,$h,$x,$total2);


    $array =  explode(',', $tax);
    $count=0;
    foreach ($array as $item) {
    global $count;
    $count=$count+1;
    }

    foreach ($array as $item) {

        $taxperarr =  explode('-', $item);

        foreach ($taxperarr as $per) {
            $taxpercentage=$per;

            }

        $totalwtax2=sprintf("%.2f",($taxpercentage/100)*$total2);

        $taxval2=$grandtotal2+$totalwtax2;

    }

    if($count>1)
        {
           
            $taxvaltotal2=$taxval2 + $taxval2;
            
            $x=$pdf->getx();
            $pdf->myCell(28,$h,$x,$taxvaltotal2);

        }
        else
        {
            $taxvaltotal2=$taxval2;

            $x=$pdf->getx();
            $pdf->myCell(28,$h,$x,$taxvaltotal2);
        }


    $grandtotal2=sprintf("%.2f",$total2+$taxvaltotal2);

    
    $x=$pdf->getx();
    $pdf->myCell(40,$h,$x,$grandtotal2);
    $pdf->Ln();

    $x=$pdf->getx();
    $pdf->myCell(17,$h,$x,'3');
    $x=$pdf->getx();
    $pdf->myCell($w,$h,$x,$sellername);

    $x=$pdf->getx();
    $pdf->myCell($w,$h,$x,$product3);
    $x=$pdf->getx();
    $pdf->myCell($w,$h,$x,$partdesc3);

    $x=$pdf->getx();
    $pdf->myCell(20,$h,$x,$qty3);
    $x=$pdf->getx();
    $pdf->myCell(33,$h,$x,$uprice3);

    $total3=sprintf("%.2f",$qty3*$uprice3);

    $x=$pdf->getx();
    $pdf->myCell(33,$h,$x,$total3);


    $array =  explode(',', $tax);
    $count=0;
    foreach ($array as $item) {
    global $count;
    $count=$count+1;
    }

    foreach ($array as $item) {

        $taxperarr =  explode('-', $item);

        foreach ($taxperarr as $per) {
            $taxpercentage=$per;

            }

        $totalwtax3=sprintf("%.2f",($taxpercentage/100)*$total3);

        $taxval3=$grandtotal3+$totalwtax3;

    }

    if($count>1)
        {
           
            $taxvaltotal3=$taxval3 + $taxval3;
            
            $x=$pdf->getx();
            $pdf->myCell(28,$h,$x,$taxvaltotal3);

        }
        else
        {
            $taxvaltotal3=$taxval3;

            $x=$pdf->getx();
            $pdf->myCell(28,$h,$x,$taxvaltotal3);
        }


    $grandtotal3=sprintf("%.2f",$total3+$taxvaltotal3);

    
    $x=$pdf->getx();
    $pdf->myCell(40,$h,$x,$grandtotal3);
    $pdf->Ln();
 }
 elseif(empty($product5) and empty($product6) and empty($product7) and empty($product8) and empty($product9) and empty($product10) )
 {
    $x=$pdf->getx();
    $pdf->myCell(17,$h,$x,'1');
    $x=$pdf->getx();
    $pdf->myCell($w,$h,$x,$sellername);

    $x=$pdf->getx();
    $pdf->myCell($w,$h,$x,$product1);
    $x=$pdf->getx();
    $pdf->myCell($w,$h,$x,$partdesc1);

    $x=$pdf->getx();
    $pdf->myCell(20,$h,$x,$qty1);
    $x=$pdf->getx();
    $pdf->myCell(33,$h,$x,$uprice1);

    $total1=sprintf("%.2f",$qty1*$uprice1);

    $x=$pdf->getx();
    $pdf->myCell(33,$h,$x,$total1);


    $array =  explode(',', $tax);
    $count=0;
    foreach ($array as $item) {
    global $count;
    $count=$count+1;
    }

    foreach ($array as $item) {

        $taxperarr =  explode('-', $item);

        foreach ($taxperarr as $per) {
            $taxpercentage=$per;

            }

        $totalwtax1=sprintf("%.2f",($taxpercentage/100)*$total1);

        $taxval1=$grandtotal1+$totalwtax1;

    }

    if($count>1)
        {
           
            $taxvaltotal1=$taxval1 + $taxval1;
            
            $x=$pdf->getx();
            $pdf->myCell(28,$h,$x,$taxvaltotal1);

        }
        else
        {
            $taxvaltotal1=$taxval1;

            $x=$pdf->getx();
            $pdf->myCell(28,$h,$x,$taxvaltotal1);
        }


    $grandtotal1=sprintf("%.2f",$total1+$taxvaltotal1);

    
    $x=$pdf->getx();
    $pdf->myCell(40,$h,$x,$grandtotal1);
    $pdf->Ln();

    $x=$pdf->getx();
    $pdf->myCell(17,$h,$x,'2');
    $x=$pdf->getx();
    $pdf->myCell($w,$h,$x,$sellername);

    $x=$pdf->getx();
    $pdf->myCell($w,$h,$x,$product2);
    $x=$pdf->getx();
    $pdf->myCell($w,$h,$x,$partdesc2);

    $x=$pdf->getx();
    $pdf->myCell(20,$h,$x,$qty2);
    $x=$pdf->getx();
    $pdf->myCell(33,$h,$x,$uprice2);

    $total2=sprintf("%.2f",$qty2*$uprice2);

    $x=$pdf->getx();
    $pdf->myCell(33,$h,$x,$total2);


    $array =  explode(',', $tax);
    $count=0;
    foreach ($array as $item) {
    global $count;
    $count=$count+1;
    }

    foreach ($array as $item) {

        $taxperarr =  explode('-', $item);

        foreach ($taxperarr as $per) {
            $taxpercentage=$per;

            }

        $totalwtax2=sprintf("%.2f",($taxpercentage/100)*$total2);

        $taxval2=$grandtotal2+$totalwtax2;

    }

    if($count>1)
        {
           
            $taxvaltotal2=$taxval2 + $taxval2;
            
            $x=$pdf->getx();
            $pdf->myCell(28,$h,$x,$taxvaltotal2);

        }
        else
        {
            $taxvaltotal2=$taxval2;

            $x=$pdf->getx();
            $pdf->myCell(28,$h,$x,$taxvaltotal2);
        }


    $grandtotal2=sprintf("%.2f",$total2+$taxvaltotal2);

    
    $x=$pdf->getx();
    $pdf->myCell(40,$h,$x,$grandtotal2);
    $pdf->Ln();

    $x=$pdf->getx();
    $pdf->myCell(17,$h,$x,'3');
    $x=$pdf->getx();
    $pdf->myCell($w,$h,$x,$sellername);

    $x=$pdf->getx();
    $pdf->myCell($w,$h,$x,$product3);
    $x=$pdf->getx();
    $pdf->myCell($w,$h,$x,$partdesc3);

    $x=$pdf->getx();
    $pdf->myCell(20,$h,$x,$qty3);
    $x=$pdf->getx();
    $pdf->myCell(33,$h,$x,$uprice3);

    $total3=sprintf("%.2f",$qty3*$uprice3);

    $x=$pdf->getx();
    $pdf->myCell(33,$h,$x,$total3);


    $array =  explode(',', $tax);
    $count=0;
    foreach ($array as $item) {
    global $count;
    $count=$count+1;
    }

    foreach ($array as $item) {

        $taxperarr =  explode('-', $item);

        foreach ($taxperarr as $per) {
            $taxpercentage=$per;

            }

        $totalwtax3=sprintf("%.2f",($taxpercentage/100)*$total3);

        $taxval3=$grandtotal3+$totalwtax3;

    }

    if($count>1)
        {
           
            $taxvaltotal3=$taxval3 + $taxval3;
            
            $x=$pdf->getx();
            $pdf->myCell(28,$h,$x,$taxvaltotal3);

        }
        else
        {
            $taxvaltotal3=$taxval3;

            $x=$pdf->getx();
            $pdf->myCell(28,$h,$x,$taxvaltotal3);
        }


    $grandtotal3=sprintf("%.2f",$total3+$taxvaltotal3);

    
    $x=$pdf->getx();
    $pdf->myCell(40,$h,$x,$grandtotal3);
    $pdf->Ln();

    $x=$pdf->getx();
    $pdf->myCell(17,$h,$x,'4');
    $x=$pdf->getx();
    $pdf->myCell($w,$h,$x,$sellername);

    $x=$pdf->getx();
    $pdf->myCell($w,$h,$x,$product4);
    $x=$pdf->getx();
    $pdf->myCell($w,$h,$x,$partdesc4);

    $x=$pdf->getx();
    $pdf->myCell(20,$h,$x,$qty4);
    $x=$pdf->getx();
    $pdf->myCell(33,$h,$x,$uprice4);

    $total3=sprintf("%.2f",$qty3*$uprice4);

    $x=$pdf->getx();
    $pdf->myCell(33,$h,$x,$total4);


    $array =  explode(',', $tax);
    $count=0;
    foreach ($array as $item) {
    global $count;
    $count=$count+1;
    }

    foreach ($array as $item) {

        $taxperarr =  explode('-', $item);

        foreach ($taxperarr as $per) {
            $taxpercentage=$per;

            }

        $totalwtax4=sprintf("%.2f",($taxpercentage/100)*$total4);

        $taxval4=$grandtotal4+$totalwtax4;

    }

    if($count>1)
        {
           
            $taxvaltotal4=$taxval4 + $taxval4;
            
            $x=$pdf->getx();
            $pdf->myCell(28,$h,$x,$taxvaltotal4);

        }
        else
        {
            $taxvaltotal4=$taxval4;

            $x=$pdf->getx();
            $pdf->myCell(28,$h,$x,$taxvaltotal4);
        }


    $grandtotal4=sprintf("%.2f",$total4+$taxvaltotal4);

    
    $x=$pdf->getx();
    $pdf->myCell(40,$h,$x,$grandtotal4);
    $pdf->Ln();
 }
 elseif(empty($product6) and empty($product7) and empty($product8) and empty($product9) and empty($product10) )
 {
    $x=$pdf->getx();
    $pdf->myCell(17,$h,$x,'1');
    $x=$pdf->getx();
    $pdf->myCell($w,$h,$x,$sellername);

    $x=$pdf->getx();
    $pdf->myCell($w,$h,$x,$product1);
    $x=$pdf->getx();
    $pdf->myCell($w,$h,$x,$partdesc1);

    $x=$pdf->getx();
    $pdf->myCell(20,$h,$x,$qty1);
    $x=$pdf->getx();
    $pdf->myCell(33,$h,$x,$uprice1);

    $total1=sprintf("%.2f",$qty1*$uprice1);

    $x=$pdf->getx();
    $pdf->myCell(33,$h,$x,$total1);


    $array =  explode(',', $tax);
    $count=0;
    foreach ($array as $item) {
    global $count;
    $count=$count+1;
    }

    foreach ($array as $item) {

        $taxperarr =  explode('-', $item);

        foreach ($taxperarr as $per) {
            $taxpercentage=$per;

            }

        $totalwtax1=sprintf("%.2f",($taxpercentage/100)*$total1);

        $taxval1=$grandtotal1+$totalwtax1;

    }

    if($count>1)
        {
           
            $taxvaltotal1=$taxval1 + $taxval1;
            
            $x=$pdf->getx();
            $pdf->myCell(28,$h,$x,$taxvaltotal1);

        }
        else
        {
            $taxvaltotal1=$taxval1;

            $x=$pdf->getx();
            $pdf->myCell(28,$h,$x,$taxvaltotal1);
        }


    $grandtotal1=sprintf("%.2f",$total1+$taxvaltotal1);

    
    $x=$pdf->getx();
    $pdf->myCell(40,$h,$x,$grandtotal1);
    $pdf->Ln();

    $x=$pdf->getx();
    $pdf->myCell(17,$h,$x,'2');
    $x=$pdf->getx();
    $pdf->myCell($w,$h,$x,$sellername);

    $x=$pdf->getx();
    $pdf->myCell($w,$h,$x,$product2);
    $x=$pdf->getx();
    $pdf->myCell($w,$h,$x,$partdesc2);

    $x=$pdf->getx();
    $pdf->myCell(20,$h,$x,$qty2);
    $x=$pdf->getx();
    $pdf->myCell(33,$h,$x,$uprice2);

    $total2=sprintf("%.2f",$qty2*$uprice2);

    $x=$pdf->getx();
    $pdf->myCell(33,$h,$x,$total2);


    $array =  explode(',', $tax);
    $count=0;
    foreach ($array as $item) {
    global $count;
    $count=$count+1;
    }

    foreach ($array as $item) {

        $taxperarr =  explode('-', $item);

        foreach ($taxperarr as $per) {
            $taxpercentage=$per;

            }

        $totalwtax2=sprintf("%.2f",($taxpercentage/100)*$total2);

        $taxval2=$grandtotal2+$totalwtax2;

    }

    if($count>1)
        {
           
            $taxvaltotal2=$taxval2 + $taxval2;
            
            $x=$pdf->getx();
            $pdf->myCell(28,$h,$x,$taxvaltotal2);

        }
        else
        {
            $taxvaltotal2=$taxval2;

            $x=$pdf->getx();
            $pdf->myCell(28,$h,$x,$taxvaltotal2);
        }


    $grandtotal2=sprintf("%.2f",$total2+$taxvaltotal2);

    
    $x=$pdf->getx();
    $pdf->myCell(40,$h,$x,$grandtotal2);
    $pdf->Ln();

    $x=$pdf->getx();
    $pdf->myCell(17,$h,$x,'3');
    $x=$pdf->getx();
    $pdf->myCell($w,$h,$x,$sellername);

    $x=$pdf->getx();
    $pdf->myCell($w,$h,$x,$product3);
    $x=$pdf->getx();
    $pdf->myCell($w,$h,$x,$partdesc3);

    $x=$pdf->getx();
    $pdf->myCell(20,$h,$x,$qty3);
    $x=$pdf->getx();
    $pdf->myCell(33,$h,$x,$uprice3);

    $total3=sprintf("%.2f",$qty3*$uprice3);

    $x=$pdf->getx();
    $pdf->myCell(33,$h,$x,$total3);


    $array =  explode(',', $tax);
    $count=0;
    foreach ($array as $item) {
    global $count;
    $count=$count+1;
    }

    foreach ($array as $item) {

        $taxperarr =  explode('-', $item);

        foreach ($taxperarr as $per) {
            $taxpercentage=$per;

            }

        $totalwtax3=sprintf("%.2f",($taxpercentage/100)*$total3);

        $taxval3=$grandtotal3+$totalwtax3;

    }

    if($count>1)
        {
           
            $taxvaltotal3=$taxval3 + $taxval3;
            
            $x=$pdf->getx();
            $pdf->myCell(28,$h,$x,$taxvaltotal3);

        }
        else
        {
            $taxvaltotal3=$taxval3;

            $x=$pdf->getx();
            $pdf->myCell(28,$h,$x,$taxvaltotal3);
        }


    $grandtotal3=sprintf("%.2f",$total3+$taxvaltotal3);

    
    $x=$pdf->getx();
    $pdf->myCell(40,$h,$x,$grandtotal3);
    $pdf->Ln();

    $x=$pdf->getx();
    $pdf->myCell(17,$h,$x,'4');
    $x=$pdf->getx();
    $pdf->myCell($w,$h,$x,$sellername);

    $x=$pdf->getx();
    $pdf->myCell($w,$h,$x,$product4);
    $x=$pdf->getx();
    $pdf->myCell($w,$h,$x,$partdesc4);

    $x=$pdf->getx();
    $pdf->myCell(20,$h,$x,$qty4);
    $x=$pdf->getx();
    $pdf->myCell(33,$h,$x,$uprice4);

    $total3=sprintf("%.2f",$qty3*$uprice4);

    $x=$pdf->getx();
    $pdf->myCell(33,$h,$x,$total4);


    $array =  explode(',', $tax);
    $count=0;
    foreach ($array as $item) {
    global $count;
    $count=$count+1;
    }

    foreach ($array as $item) {

        $taxperarr =  explode('-', $item);

        foreach ($taxperarr as $per) {
            $taxpercentage=$per;

            }

        $totalwtax4=sprintf("%.2f",($taxpercentage/100)*$total4);

        $taxval4=$grandtotal4+$totalwtax4;

    }

    if($count>1)
        {
           
            $taxvaltotal4=$taxval4 + $taxval4;
            
            $x=$pdf->getx();
            $pdf->myCell(28,$h,$x,$taxvaltotal4);

        }
        else
        {
            $taxvaltotal4=$taxval4;

            $x=$pdf->getx();
            $pdf->myCell(28,$h,$x,$taxvaltotal4);
        }


    $grandtotal4=sprintf("%.2f",$total4+$taxvaltotal4);

    
    $x=$pdf->getx();
    $pdf->myCell(40,$h,$x,$grandtotal4);
    $pdf->Ln();

    $x=$pdf->getx();
    $pdf->myCell(17,$h,$x,'5');
    $x=$pdf->getx();
    $pdf->myCell($w,$h,$x,$sellername);

    $x=$pdf->getx();
    $pdf->myCell($w,$h,$x,$product5);
    $x=$pdf->getx();
    $pdf->myCell($w,$h,$x,$partdesc5);

    $x=$pdf->getx();
    $pdf->myCell(20,$h,$x,$qty5);
    $x=$pdf->getx();
    $pdf->myCell(33,$h,$x,$uprice5);

    $total3=sprintf("%.2f",$qty3*$uprice5);

    $x=$pdf->getx();
    $pdf->myCell(33,$h,$x,$total5);


    $array =  explode(',', $tax);
    $count=0;
    foreach ($array as $item) {
    global $count;
    $count=$count+1;
    }

    foreach ($array as $item) {

        $taxperarr =  explode('-', $item);

        foreach ($taxperarr as $per) {
            $taxpercentage=$per;

            }

        $totalwtax5=sprintf("%.2f",($taxpercentage/100)*$total5);

        $taxval5=$grandtotal5+$totalwtax5;

    }

    if($count>1)
        {
           
            $taxvaltotal5=$taxval5 + $taxval5;
            
            $x=$pdf->getx();
            $pdf->myCell(28,$h,$x,$taxvaltotal5);

        }
        else
        {
            $taxvaltotal5=$taxval5;

            $x=$pdf->getx();
            $pdf->myCell(28,$h,$x,$taxvaltotal5);
        }


    $grandtotal5=sprintf("%.2f",$total5+$taxvaltotal5);

    
    $x=$pdf->getx();
    $pdf->myCell(40,$h,$x,$grandtotal5);
    $pdf->Ln();
 }
 elseif(empty($product7) and empty($product8) and empty($product9) and empty($product10) )
 {
    $pdf->myCell(36,$h,$x,$product1.",".$product2.",".$product3.",".$product4.",".$product5.",".$product6);
    $x=$pdf->getx();
    $pdf->myCell(33,$h,$x,$partdesc1.",".$partdesc2.",".$partdesc3.",".$partdesc4.",".$partdesc5.",".$partdesc6);
 }
 elseif(empty($product8) and empty($product9) and empty($product10) )
 {
    $pdf->myCell(36,$h,$x,$product1.",".$product2.",".$product3.",".$product4.",".$product5.",".$product6.",".$product7);
    $x=$pdf->getx();
    $pdf->myCell(33,$h,$x,$partdesc1.",".$partdesc2.",".$partdesc3.",".$partdesc4.",".$partdesc5.",".$partdesc6.",".$partdesc7);
 }
 elseif(empty($product9) and empty($product10) )
 {
    $pdf->myCell(36,$h,$x,$product1.",".$product2.",".$product3.",".$product4.",".$product5.",".$product6.",".$product7.",".$product8);
    $x=$pdf->getx();
    $pdf->myCell(33,$h,$x,$partdesc1.",".$partdesc2.",".$partdesc3.",".$partdesc4.",".$partdesc5.",".$partdesc6.",".$partdesc7.",".$partdesc8);
 }
 elseif(empty($product10) )
 {
    $pdf->myCell(36,$h,$x,$product1.",".$product2.",".$product3.",".$product4.",".$product5.",".$product6.",".$product7.",".$product8.",".$product9);
    $x=$pdf->getx();
    $pdf->myCell(33,$h,$x,$partdesc1.",".$partdesc2.",".$partdesc3.",".$partdesc4.",".$partdesc5.",".$partdesc6.",".$partdesc7.",".$partdesc8.",".$partdesc9);
 }
 else
 {
    $pdf->myCell(36,$h,$x,$product1.",".$product2.",".$product3.",".$product4.",".$product5.",".$product6.",".$product7.",".$product8.",".$product9.",".$product10);
    $x=$pdf->getx();
    $pdf->myCell(33,$h,$x,$partdesc1.",".$partdesc2.",".$partdesc3.",".$partdesc4.",".$partdesc5.",".$partdesc6.",".$partdesc7.",".$partdesc8.",".$partdesc9.",".$partdesc10);
 }
 

    $x=$pdf->getx();
    $pdf->myCell(17,$h,$x,'');
    $x=$pdf->getx();
    $pdf->myCell($w,$h,$x,"");

    $x=$pdf->getx();
    $pdf->myCell($w,$h,$x,"");
    $x=$pdf->getx();
    $pdf->myCell($w,$h,$x,"");

    $x=$pdf->getx();
    $pdf->myCell(20,$h,$x,"");
    $x=$pdf->getx();
    $pdf->myCell(33,$h,$x,"TOTAL");

    $totalall=sprintf("%.2f",$total1+$total2+$total3+$total4+$total5+$total6+$total7+$total8+$total9+$total10);
    $x=$pdf->getx();
    $pdf->myCell(33,$h,$x,$totalall);

    $totaltax=$taxvaltotal1+$taxvaltotal2+$taxvaltotal3+$taxvaltotal4+$taxvaltotal5+$taxvaltotal6+$taxvaltotal7+$taxvaltotal8+$taxvaltotal9+$taxvaltotal10;
    $x=$pdf->getx();
    $pdf->myCell(28,$h,$x,$totaltax);

    $grandtotal=sprintf("%.2f",$grandtotal1+$grandtotal2+$grandtotal3+$grandtotal4+$grandtotal5+$grandtotal6+$grandtotal7+$grandtotal8+$grandtotal9+$grandtotal10);
    $x=$pdf->getx();
    $pdf->myCell(40,$h,$x,$grandtotal);
    $pdf->Ln();



if($currency != "rupee")
{

    $pdf->SetFont("Times","B",12);
$pdf->Cell(50,10,"International Vendor/Seller Details",0,1);


$pdf->Cell(17,10,"Sl No.",1,0,"C");
/*$pdf->Cell(40,10,"Product",1,0,"C");*/
$pdf->Cell($w,10,"Vendor Name",1,0,"C");
$pdf->Cell(36,10,"Product(s) ordered",1,0,"C");
$pdf->Cell(33,10,"Part Descriptions",1,0,"C");
$pdf->Cell(20,10,"Qty",1,0,"C");
$pdf->Cell(33,10,"Unit Price($cur)",1,0,"C");
$pdf->Cell(33,10,"Total Price($cur)",1,0,"C");
$pdf->Cell(28,10,"INR to $cur",1,0,"C");
$pdf->Cell(40,10,"WHT@20% or 10%",1,1,"C");

$whtvalue=($vtotalprice*$conversionrate)+($whtpercent/100)*$vtotalprice;


$pdf->SetFont("Times","",12);

/*$pdf->Cell(17,10,"1",1,0,"C");

$pdf->Cell(64,10,"$sellername",1,0,"C");
$pdf->Cell(46,10,"$product",1,0,"C");
$pdf->Cell(20,10,"$qty",1,0,"C");
$pdf->Cell(33,10,"$vuprice",1,0,"C");
$pdf->Cell(33,10,"$vtotalprice",1,0,"C");
$pdf->Cell(28,10,"$conversionrate",1,0,"C");
$pdf->Cell(40,10,"$whtvalue",1,1,"C");*/

$x=$pdf->getx();
$pdf->myCell(17,$h,$x,'1');
$x=$pdf->getx();
$pdf->myCell($w,$h,$x,$sellername);
$x=$pdf->getx();
 if(empty($product2) and empty($product3) and empty($product4) and empty($product5) and empty($product6) and empty($product7) and empty($product8) and empty($product9) and empty($product10) )
 {
    $pdf->myCell(36,$h,$x,$product1);
    $x=$pdf->getx();
    $pdf->myCell(33,$h,$x,$partdesc1);
 }
 elseif(empty($product3) and empty($product4) and empty($product5) and empty($product6) and empty($product7) and empty($product8) and empty($product9) and empty($product10) )
 {
    $pdf->myCell(36,$h,$x,$product1.",".$product2);
    $x=$pdf->getx();
    $pdf->myCelldesc($w1,$h,$x,$partdesc1.",".$partdesc2);
 }
 elseif(empty($product4) and empty($product5) and empty($product6) and empty($product7) and empty($product8) and empty($product9) and empty($product10) )
 {
    $pdf->myCell(36,$h,$x,$product1.",".$product2.",".$product3);
    $x=$pdf->getx();
    $pdf->myCell(33,$h,$x,$partdesc1.",".$partdesc2.",".$partdesc3);
 }
 elseif(empty($product5) and empty($product6) and empty($product7) and empty($product8) and empty($product9) and empty($product10) )
 {
    $pdf->myCell(36,$h,$x,$product1.",".$product2.",".$product3.",".$product4);
    $x=$pdf->getx();
    $pdf->myCell(33,$h,$x,$partdesc1.",".$partdesc2.",".$partdesc3.",".$partdesc4);
 }
 elseif(empty($product6) and empty($product7) and empty($product8) and empty($product9) and empty($product10) )
 {
    $pdf->myCell(36,$h,$x,$product1.",".$product2.",".$product3.",".$product4.",".$product5);
    $x=$pdf->getx();
    $pdf->myCell(33,$h,$x,$partdesc1.",".$partdesc2.",".$partdesc3.",".$partdesc4.",".$partdesc5);
 }
 elseif(empty($product7) and empty($product8) and empty($product9) and empty($product10) )
 {
    $pdf->myCell(36,$h,$x,$product1.",".$product2.",".$product3.",".$product4.",".$product5.",".$product6);
    $x=$pdf->getx();
    $pdf->myCell(33,$h,$x,$partdesc1.",".$partdesc2.",".$partdesc3.",".$partdesc4.",".$partdesc5.",".$partdesc6);
 }
 elseif(empty($product8) and empty($product9) and empty($product10) )
 {
    $pdf->myCell(36,$h,$x,$product1.",".$product2.",".$product3.",".$product4.",".$product5.",".$product6.",".$product7);
    $x=$pdf->getx();
    $pdf->myCell(33,$h,$x,$partdesc1.",".$partdesc2.",".$partdesc3.",".$partdesc4.",".$partdesc5.",".$partdesc6.",".$partdesc7);
 }
 elseif(empty($product9) and empty($product10) )
 {
    $pdf->myCell(36,$h,$x,$product1.",".$product2.",".$product3.",".$product4.",".$product5.",".$product6.",".$product7.",".$product8);
    $x=$pdf->getx();
    $pdf->myCell(33,$h,$x,$partdesc1.",".$partdesc2.",".$partdesc3.",".$partdesc4.",".$partdesc5.",".$partdesc6.",".$partdesc7.",".$partdesc8);
 }
 elseif(empty($product10) )
 {
    $pdf->myCell(36,$h,$x,$product1.",".$product2.",".$product3.",".$product4.",".$product5.",".$product6.",".$product7.",".$product8.",".$product9);
    $x=$pdf->getx();
    $pdf->myCell(33,$h,$x,$partdesc1.",".$partdesc2.",".$partdesc3.",".$partdesc4.",".$partdesc5.",".$partdesc6.",".$partdesc7.",".$partdesc8.",".$partdesc9);
 }
 else
 {
    $pdf->myCell(36,$h,$x,$product1.",".$product2.",".$product3.",".$product4.",".$product5.",".$product6.",".$product7.",".$product8.",".$product9.",".$product10);
    $x=$pdf->getx();
    $pdf->myCell(33,$h,$x,$partdesc1.",".$partdesc2.",".$partdesc3.",".$partdesc4.",".$partdesc5.",".$partdesc6.",".$partdesc7.",".$partdesc8.",".$partdesc9.",".$partdesc10);
 }
 
 




$x=$pdf->getx();
$pdf->myCell(20,$h,$x,$qty);
$x=$pdf->getx();
$pdf->myCell(33,$h,$x,$vuprice);
$x=$pdf->getx();
$pdf->myCell(33,$h,$x,$vtotalprice);
$x=$pdf->getx();
$pdf->myCell(28,$h,$x,$conversionrate);
$x=$pdf->getx();
$pdf->myCell(40,$h,$x,$whtvalue);
$pdf->Ln();


}

$pdf->SetFont("Times","B",12);
$pdf->Cell(50,10,"Credit Card Details",0,1);


$pdf->Cell(17,10,"Sl No.",1,0,"C");
/*$pdf->Cell(40,10,"Product",1,0,"C");*/
$pdf->Cell(64,10,"Credit Card Name",1,0,"C");
$pdf->Cell(46,10,"Credit Card No. Ending",1,0,"C");
$pdf->Cell(40,10,"Amount ($cur)",1,0,"C");
$pdf->Cell(33,10,"Conversion Rate",1,1,"C");


$whtvalue=($vtotalprice*$conversionrate)+($whtpercent/100)*$vtotalprice;


$pdf->SetFont("Times","",12);

$pdf->Cell(17,10,"1",1,0,"C");
/*$pdf->Cell(40,10,"Product",1,0,"C");*/
$pdf->Cell(64,10,"$creditcardname",1,0,"C");
$pdf->Cell(46,10,"$cardnoending",1,0,"C");
$pdf->Cell(40,10,"$grandtotal",1,0,"C");
$pdf->Cell(33,10,"$conversionrate",1,1,"C");







//$ad=mysqli_query($dbc,"update `quotation` set `QuotNo`='$quotno',`SubTotal`='$subtotal',`GrandTotal`='$grandtotal' where   `Company`='$company' and `Product`='$product' and `Service`='$service' ");

/*$ad=mysqli_query($dbc,"update `quotation` set `QuotNo`='$quotno',`Representive`='$rep',`RMail`='$rmail',`PartDescription1`='$pdescription1',`PartDescription2`='$pdescription2',`PartDescription3`='$pdescription3',`PartDescription4`='$pdescription4',`PartDescription5`='$pdescription5',`Quantity1`='$quantity1',`Quantity2`='$quantity2',`Quantity3`='$quantity3',`Quantity4`='$quantity4',`Quantity5`='$quantity5',`UnitPrice1`='$unitprice1',`UnitPrice2`='$unitprice2',`UnitPrice3`='$unitprice3',`UnitPrice4`='$unitprice4',`UnitPrice5`='$unitprice5',`SubTotal`='$subtotal',`SGST`='$sgsttotal',`CGST`='$cgsttotal',`IGST`='$igsttotal',`GrandTotal`='$grandtotal' where   `Company`='$company' and `Product`='$product' and `Service`='$service' ");*/

$grossprofit=$subtotal-$vtotalprice;

$pdf->SetFont("Times","B",12);

$pdf->Cell(50,20,"GP: $grossprofit $cur",0,1);







// email stuff (change data below)
$to = $rmail; 
$from = "purusottam@navikra.com"; 
$subject = "Proposal for $product to $company"; 
$message = "<p>Please see the attachment.</p>";

// a random hash will be necessary to send mixed content
$separator = md5(time());

// carriage return type (we use a PHP end of line constant)
$eol = PHP_EOL;

// attachment name
$filename = "opf.pdf";

// encode data (puts attachment in proper format)
$pdfdoc = $pdf->Output("", "S");
$attachment = chunk_split(base64_encode($pdfdoc));

// main header
$headers  = "From: ".$from.$eol;
$headers .= "MIME-Version: 1.0".$eol; 
$headers .= "Content-Type: multipart/mixed; boundary=\"".$separator."\"";

// no more headers after this, we start the body! //

$body = "--".$separator.$eol;
$body .= "Content-Transfer-Encoding: 7bit".$eol.$eol;
$body .= "This is a MIME encoded message.".$eol;

// message
$body .= "--".$separator.$eol;
$body .= "Content-Type: text/html; charset=\"iso-8859-1\"".$eol;
$body .= "Content-Transfer-Encoding: 8bit".$eol.$eol;
$body .= $message.$eol;

// attachment
$body .= "--".$separator.$eol;
$body .= "Content-Type: application/octet-stream; name=\"".$filename."\"".$eol; 
$body .= "Content-Transfer-Encoding: base64".$eol;
$body .= "Content-Disposition: attachment".$eol.$eol;
$body .= $attachment.$eol;
$body .= "--".$separator."--";

// send message
mail($to, $subject, $body, $headers);





$pdf->output();

?>



<script type="text/javascript">(function(window, location) {
history.replaceState(null, document.title, location.pathname+"#!/history");
history.pushState(null, document.title, location.pathname);

window.addEventListener("popstate", function() {
  if(location.hash === "#!/history") {
    history.replaceState(null, document.title, location.pathname);
    setTimeout(function(){
      location.replace("../../../opr/opfdash.php");
    },0);
  }
}, false);
}(window, location));

</script>
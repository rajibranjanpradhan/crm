<?php
include "session_handler.php";
?>
<!DOCTYPE html>
<html class="no-js css-menubar" lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="description" content="bootstrap material admin template">
    <meta name="author" content="">
    
    <title>Update Funnel | Bizapp CRM</title>
    
    <link rel="apple-touch-icon" href="../../assets/images/apple-touch-icon.png">
    <link rel="shortcut icon" href="../../assets/images/favicon.ico">
    
    <!-- Stylesheets -->
    <link rel="stylesheet" href="../../assets/global/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../assets/global/css/bootstrap-extend.min.css">
    <link rel="stylesheet" href="../../assets/css/site.min.css">
    
    <!-- Plugins -->
    <link rel="stylesheet" href="../../assets/global/vendor/animsition/animsition.css">
    <link rel="stylesheet" href="../../assets/global/vendor/asscrollable/asScrollable.css">
    <link rel="stylesheet" href="../../assets/global/vendor/switchery/switchery.css">
    <link rel="stylesheet" href="../../assets/global/vendor/intro-js/introjs.css">
    <link rel="stylesheet" href="../../assets/global/vendor/slidepanel/slidePanel.css">
    <link rel="stylesheet" href="../../assets/global/vendor/jquery-mmenu/jquery-mmenu.css">
    <link rel="stylesheet" href="../../assets/global/vendor/flag-icon-css/flag-icon.css">
    <link rel="stylesheet" href="../../assets/global/vendor/waves/waves.css">
        <link rel="stylesheet" href="../../assets/global/vendor/chartist/chartist.css">
        <link rel="stylesheet" href="../../assets/global/vendor/jvectormap/jquery-jvectormap.css">
        <link rel="stylesheet" href="../../assets/global/vendor/chartist-plugin-tooltip/chartist-plugin-tooltip.css">
        <link rel="stylesheet" href="../../assets/examples/css/dashboard/v1.css">
    
    
    <!-- Fonts -->
    <link rel="stylesheet" href="../../assets/global/fonts/material-design/material-design.min.css">
    <link rel="stylesheet" href="../../assets/global/fonts/brand-icons/brand-icons.min.css">
    <link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Roboto:300,400,500,300italic'>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

     <!-- table set -->
    <link rel='stylesheet' href='../../assets/css/datatableset.css'>
    <link rel='stylesheet' href='https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css'>
    
    <!--[if lt IE 9]>
    <script src="../../assets/global/vendor/html5shiv/html5shiv.min.js"></script>
    <![endif]-->
    
    <!--[if lt IE 10]>
    <script src="../../assets/global/vendor/media-match/media.match.min.js"></script>
    <script src="../../assets/global/vendor/respond/respond.min.js"></script>
    <![endif]-->
    
    <!-- Scripts -->
    <script src="../../assets/global/vendor/breakpoints/breakpoints.js"></script>
    <script>
      Breakpoints();
    </script>
    
  </head>
  <body class="animsition site-navbar-small dashboard">
    <?php include "navbar-header.php"; ?>
    <?php include "side-navigation.php"; ?>   
    

    <!-- Page -->
    <div class="page">
      <!-- write body content here -->
      <div class="page-content">
         <!-- Panel Basic -->
          <div class="panel">
            <header class="panel-heading">
            
            <h3 class="panel-title example-title">Update Funnel</h3>
            </header>
          <div class="panel-body">
           
              <?php
                    /*$fetdetail=mysqli_query($dbc,"select * from `$pro` where `Sector`='$item' or SubLocation='$location' or NoOfEmployees='$noofemployees' ");*/

                      echo '<table class="table table-striped table-responsive-md table-bordered example">';
                              echo '<thead>';
                              echo '<tr>';
                          echo '<th>Sl No.</th>';
                                                    echo '<th>Representive</th>';
                                                    echo '<th>Funnel Id</th>';
                                                    echo '<th>Company</th>';
                                                    echo '<th>CampaignType</th>';
                                                    echo '<th>MeetingDate</th>';
                                                    echo '<th>ProjectName</th>';
                                                    echo '<th>Product</th>';
                                                    echo '<th>Service</th>';
                                                    echo '<th>Revenue</th>';
                                                    echo '<th>Stage</th>';
                                                    echo '<th>Expected Date of Closure</th>';
                                                    
                                                    /*echo '<th>Remove</th>';*/
                                                    
                                                    /*echo '<th>Remove</th>';*/
                             echo '</tr>';
                             echo '</thead>';
                             echo '<tbody>';
                      
                        $counti=0;


                    $fetdetail=mysqli_query($dbc,"select * from funnel where `RMail`='$id' order by `ModificationDetail` desc ");
                    
                        while($frow=mysqli_fetch_assoc($fetdetail))
                        {
                          
                                                    $cid=$frow['cid'];
                                                    $fid=$frow['id'];
                                                    $representive=$frow['Representive'];
                                                    $company=$frow['Company'];
                                                    $campaigntype=$frow['CampaignType'];
                                                    $meetingdate=$frow['MeetingDate'];
                                                    $projectname=$frow['ProjectName'];
                                                    $products=$frow['Products'];
                                                    $service=$frow['Services'];
                                                    $revenue=$frow['Revenue'];
                                                    $stage=$frow['Stage'];
                                                    
                                                      global $counti;
                                                    $counti=$counti+1;
                                                   
                          
                          
                            echo '<tr>';
                            echo "<td>".$counti."</td>";
                            echo "<td>{$frow['Representive']}</td>";
                            echo "<td><a href='funnel-ins.php?ctype=$campaigntype&c=$company&md=$meetingdate&proj=$projectname&fid=$fid' >".$fid."</a></td>";
                            echo "<td><a href='funnel-ins.php?ctype=$campaigntype&c=$company&md=$meetingdate&proj=$projectname&fid=$fid' >{$frow['Company']}</a></td>";
                                                        echo "<td>{$frow['CampaignType']}</td>";
                                                        echo "<td>{$frow['MeetingDate']}</td>";
                                                        echo "<td>{$frow['ProjectName']}</td>";
                                                        echo "<td>{$frow['Products']}</td>";
                                                        echo "<td>{$frow['Services']}</td>";
                                                        echo "<td>{$frow['Revenue']}</td>";
                                                        echo "<td>{$frow['Stage']}</td>";
                                                        echo "<td>{$frow['DateOfClosure']}</td>";
                                                        
                                                        /*echo "<td><a  href='../auth/funnel/delfun.php?u=$fid' class='delete text-muted text-uppercase btn btn-danger'   ><font color='white'>Delete</font></a></td>";*/

                                                        echo '</tr>';
  
                      }
                      echo '</tbody>';
                    echo '</table>';
                  ?>                      
          
          </div>
        </div>
        <!-- End Panel Basic -->
      </div>
      
</div>
    <!-- End Page -->


    <!-- Footer -->
    <?php include "includes/footer.php"; ?>

    
    <!-- Core  -->
    <script src="../../assets/global/vendor/babel-external-helpers/babel-external-helpers.js"></script>
    <script src="../../assets/global/vendor/jquery/jquery.js"></script>
    <script src="../../assets/global/vendor/popper-js/umd/popper.min.js"></script>
    <script src="../../assets/global/vendor/bootstrap/bootstrap.js"></script>
    <script src="../../assets/global/vendor/animsition/animsition.js"></script>
    <script src="../../assets/global/vendor/mousewheel/jquery.mousewheel.js"></script>
    <script src="../../assets/global/vendor/asscrollbar/jquery-asScrollbar.js"></script>
    <script src="../../assets/global/vendor/asscrollable/jquery-asScrollable.js"></script>
    <script src="../../assets/global/vendor/waves/waves.js"></script>
    
    <!-- Plugins -->
    <script src="../../assets/global/vendor/jquery-mmenu/jquery.mmenu.min.all.js"></script>
    <script src="../../assets/global/vendor/switchery/switchery.js"></script>
    <script src="../../assets/global/vendor/intro-js/intro.js"></script>
    <script src="../../assets/global/vendor/screenfull/screenfull.js"></script>
    <script src="../../assets/global/vendor/slidepanel/jquery-slidePanel.js"></script>
        <script src="../../assets/global/vendor/chartist/chartist.min.js"></script>
        <script src="../../assets/global/vendor/chartist-plugin-tooltip/chartist-plugin-tooltip.js"></script>
        <script src="../../assets/global/vendor/jvectormap/jquery-jvectormap.min.js"></script>
        <script src="../../assets/global/vendor/jvectormap/maps/jquery-jvectormap-world-mill-en.js"></script>
        <script src="../../assets/global/vendor/matchheight/jquery.matchHeight-min.js"></script>
        <script src="../../assets/global/vendor/peity/jquery.peity.min.js"></script>
    
    <!-- Scripts -->
    <script src="../../assets/global/js/Component.js"></script>
    <script src="../../assets/global/js/Plugin.js"></script>
    <script src="../../assets/global/js/Base.js"></script>
    <script src="../../assets/global/js/Config.js"></script>
    
    <script src="section/Menubar.js"></script>
    <script src="section/Sidebar.js"></script>
    <script src="section/PageAside.js"></script>
    <script src="section/GridMenu.js"></script>
    
    <!-- Config -->
    <script src="../../assets/global/js/config/colors.js"></script>
    <script src="../../assets/js/config/tour.js"></script>
    <script>Config.set('assets', '../assets');</script>
    
    <!-- Page -->
    <script src="../../assets/js/Site.js"></script>
    <script src="../../assets/global/js/Plugin/asscrollable.js"></script>
    <script src="../../assets/global/js/Plugin/slidepanel.js"></script>
    <script src="../../assets/global/js/Plugin/switchery.js"></script>
        <script src="../../assets/global/js/Plugin/matchheight.js"></script>
        <script src="../../assets/global/js/Plugin/jvectormap.js"></script>
        <script src="../../assets/global/js/Plugin/peity.js"></script>
    
        <script src="../../assets/examples/js/dashboard/v1.js"></script>



        <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
        <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
        <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
        <script >
          $(document).ready(function() {
    $('.example').DataTable();
} );
        </script>


  
  <?php include "../../assets/twak/twak.php"; ?>
  </body>
</html>

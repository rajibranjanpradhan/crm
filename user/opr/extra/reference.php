<?php
session_start();
error_reporting(E_PARSE | E_ERROR);
$id=$_SESSION['id'];

if(!$_SESSION['id'])
{
	echo '<script>location.replace("../../index.php");</script>';
} 

require_once "../conn/conn.php";
$chk=mysqli_query($dbc,"select * from team where email='$id'");
while($fchk=mysqli_fetch_assoc($chk))
{
	$type=$fchk['EmployeeType'];
	if($type != "Admin")
	{
		echo '<script>location.replace("../../index.php");</script>';
	}
}

?>

<style>
.a{
    margin-top:-47px;
    margin-left:80px;
    margin-bottom:-20px;
}

</style>
<!doctype html>
<html class="fixed">
	<head>

		<!-- Basic -->
		<meta charset="UTF-8">

		<title>Reference Campaign | Navikra CRM</title>
		<meta name="keywords" content="Navikra CRM" />
		<meta name="description" content="Navikra CRM">
		<meta name="author" content="navikra.com">

		<link rel="apple-touch-icon" sizes="180x180" href="../../../log/assets/images/apple-touch-icon.png">
    	<link rel="icon" type="image/png" href="../../../log/assets/images/favicon-32x32.png" sizes="32x32">
    	<link rel="icon" type="image/png" href="../../../log/assets/images/favicon-16x16.png" sizes="16x16">

		<!-- Mobile Metas -->
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

		<!-- Web Fonts  -->
		<link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800|Shadows+Into+Light" rel="stylesheet" type="text/css">

		<!-- Vendor CSS -->
		<link rel="stylesheet" href="../../../log/assets/vendor/bootstrap/css/bootstrap.css" />

		<link rel="stylesheet" href="../../../log/assets/vendor/font-awesome/css/font-awesome.css" />
		<link rel="stylesheet" href="../../../log/assets/vendor/magnific-popup/magnific-popup.css" />
		<link rel="stylesheet" href="../../../log/assets/vendor/bootstrap-datepicker/css/datepicker3.css" />

		<!-- Specific Page Vendor CSS -->
		<link rel="stylesheet" href="../../../log/assets/vendor/jquery-ui/css/ui-lightness/jquery-ui-1.10.4.custom.css" />
		<link rel="stylesheet" href="../../../log/assets/vendor/bootstrap-multiselect/bootstrap-multiselect.css" />
		<link rel="stylesheet" href="../../../log/assets/vendor/morris/morris.css" />

		<!-- Theme CSS -->
		<link rel="stylesheet" href="../../../log/assets/stylesheets/theme.css" />

		<!-- Skin CSS -->
		<link rel="stylesheet" href="../../../log/assets/stylesheets/skins/default.css" />

		<!-- Theme Custom CSS -->
		<link rel="stylesheet" href="../../../log/assets/stylesheets/theme-custom.css">

		<!-- Head Libs -->
		<script src="../../../log/assets/vendor/modernizr/modernizr.js"></script>

    <!-- fa-product icon -->
    <link href="http://netdna.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.css" rel="stylesheet" />

		<!-- search -->
		<!-- <script type="text/javascript" src="assets/ajax/search.js" ></script> -->
		
		<script type="text/javascript" src="../../../log/assets/ajax/jquery-1.8.0.min.js"></script>
		<script type="text/javascript">
$(function(){
$(".search").keyup(function() 
{ 
var searchid = $(this).val();
var dataString = 'search='+ searchid;
if(searchid!='')
{
    $.ajax({
    type: "POST",
    url: "../auth/campaign/dsearch.php",
    data: dataString,
    cache: false,
    success: function(html)
    {
    $("#result").html(html).show();
    }
    });
}return false;    
});

jQuery("#result").live("click",function(e){ 
    var $clicked = $(e.target);
    var $name = $clicked.find('.name').html();
    var decoded = $("<div/>").html($name).text();
    $('#searchid').val(decoded);
});
jQuery(document).live("click", function(e) { 
    var $clicked = $(e.target);
    if (! $clicked.hasClass("search")){
    jQuery("#result").fadeOut(); 
    }
});
$('#searchid').click(function(){
    jQuery("#result").fadeIn();
});
});
</script>



		

	</head>
	<body>
		<section class="body">

			<!-- start: header -->
			<header class="header">
				<div class="logo-container">
					<a href="dashboard.php" class="logo">
						<img src="../../../log/assets/images/logo.png" height="35" alt="Navikra CRM" />
					</a>
					<div class="visible-xs toggle-sidebar-left" data-toggle-class="sidebar-left-opened" data-target="html" data-fire-event="sidebar-left-opened">
						<i class="fa fa-bars" aria-label="Toggle sidebar"></i>
					</div>
				</div>
			
				<!-- start: search & user box -->
				<div class="header-right">
			
				
                    
                    <?php
                        $ad=mysqli_query($dbc,"select * from team where email='$id'");
                        while($r=mysqli_fetch_assoc($ad))
                        {
                            $name=$r['name'];
                            $role=$r['desig'];
                        }
                    ?>
			
					<span class="separator"></span>
			
					<div id="userbox" class="userbox">
						<a href="#" data-toggle="dropdown">
							<figure class="profile-picture">
								<img src="../../../log/assets/images/!logged-user.jpg" alt="Joseph Doe" class="img-circle" data-lock-picture="assets/images/!logged-user.jpg" />
							</figure>
							<div class="profile-info" data-lock-name="John Doe" data-lock-email="<?php echo $id; ?>">
								<span class="name"><?php echo $name; ?></span>
								<span class="role"><?php echo $role; ?></span>
							</div>
			
							<i class="fa custom-caret"></i>
						</a>
			
						<div class="dropdown-menu">
							<ul class="list-unstyled">
								<li class="divider"></li>
								<li>
									<a role="menuitem" tabindex="-1" href="profile.php"><i class="fa fa-user"></i> My Profile</a>
								</li>
								<li>
									<!-- <a role="menuitem" tabindex="-1" href="#" data-lock-screen="true"><i class="fa fa-lock"></i> Lock Screen</a> -->
								</li>
								<li>
									<a role="menuitem" tabindex="-1" href="../../index.php"><i class="fa fa-power-off"></i> Logout</a>
								</li>
							</ul>
						</div>
					</div>
				</div>
				<!-- end: search & user box -->
			</header>
			<!-- end: header -->

			

			<div class="inner-wrapper">
				<!-- start: sidebar -->
				<aside id="sidebar-left" class="sidebar-left">
				
					<div class="sidebar-header">
						<div class="sidebar-title">
							Navigation
						</div>
						<div class="sidebar-toggle hidden-xs" data-toggle-class="sidebar-left-collapsed" data-target="html" data-fire-event="sidebar-left-toggle">
							<i class="fa fa-bars" aria-label="Toggle sidebar"></i>
						</div>
					</div>
				
					<div class="nano">
						<div class="nano-content">
							<nav id="menu" class="nav-main" role="navigation">
								<ul class="nav nav-main">
									<li >
										<a href="dashboard.php">
											<i class="fa fa-home" aria-hidden="true"></i>
											<span>Dashboard</span>
										</a>
									</li>
									<!-- <li>
										<a href="mailbox-folder.html">
											<span class="pull-right label label-primary">182</span>
											<i class="fa fa-envelope" aria-hidden="true"></i>
											<span>Mailbox</span>
										</a>
                                    </li> -->
                                    <li class="nav-parent">
                                    <a >
                                    
                                       <i class="fa fa-users" aria-hidden="true"></i>
                                       <span>Customers</span>
                                    </a>
                                    <ul class="nav nav-children">


                                            <li >
                                                <a href="custdash.php">
                                                 Dashboard
                                                </a>
                                        
                                            </li>
                                           <li >
                                                <a href="addcust.php">
                                                 Add Customer
                                                </a>
                                        
                                           </li>
                                           <li >
                                                <a href="customerprofile.php">
                                                 Add Customer Profile
                                                </a>
                                        
                                           </li>
                                           <li >
                                                <a href="campcust.php">
                                                 Campaign Customer
                                                </a>
                                        
                                           </li>
                                    
                                          <!--  <li >
                                              <a href="targetset.php">
                                                 Set Target
                                              </a>
                                        
                                           </li> -->
                                    

                                    </ul>
                                 </li>
                                    <li class="nav-parent nav-active nav-expanded">
										<a >
											<!-- <span class="pull-right label label-primary">182</span> -->
											<i class="fa fa-plus" aria-hidden="true"></i>
											<span>Campaign</span>
										</a>

										<ul class="nav nav-children">

											<li >
												<a href="create-campaign.php">
													Create 
												</a>
											</li>
											<li class="nav-parent " >
												<a >
													Direct 
												</a>
												<ul class="nav nav-children">
													<li>
														<a href="dirdash.php">
													 		Show
														</a>
													</li>
													<li >
														<a href="direct.php">
													 		Insert
														</a>
													</li>
												</ul>
											</li>
											
											<li class="nav-parent">
												<a >
													 Digital
												</a>
												<ul class="nav nav-children">
													<li>
														<a href="digdash.php">
													 		Show
														</a>
													</li>
													<li>
														<a href="digital.php">
													 		Insert
														</a>
													</li>
												</ul>
											</li>

											<li class="nav-parent">
												<a >
													 Telecalling
												</a>
												<ul class="nav nav-children">
													<li>
														<a href="teldash.php">
													 		Show
														</a>
													</li>
													<li>
														<a href="telecalling.php">
													 		Insert
														</a>
													</li>
												</ul>
											</li>
											<li class="nav-parent nav-active nav-expanded">
												<a >
													 Reference
												</a>
												<ul class="nav nav-children">
													<li>
														<a href="refdash.php">
													 		Show
														</a>
													</li>
													<li class="nav-active">
														<a href="reference.php">
													 		Insert
														</a>
													</li>
												</ul>
											</li>

											
										</ul>

                                    </li>


									<li class=" nav-parent ">
										<a>
											<!-- <span class="pull-right label label-primary">182</span> -->
											<i class="fa fa-user" aria-hidden="true"></i>
											<span>Meeting</span>
										</a>

										<ul class="nav nav-children">


											<li >
												<a href="meetingdash.php">
													Show 
												</a>
												
											</li>
											
											<li  >
												<a href="meeting.php">
													 Update
												</a>
												
											</li>

										</ul>

                                    </li>

									<li class="nav-parent">
										<a >
											
											<i class="fa fa-refresh" aria-hidden="true"></i>
											<span>Funnel</span>
										</a>
										<ul class="nav nav-children">


											<li >
												<a href="funneldash.php">
													Show 
												</a>
												
											</li>
											
											<li >
												<a href="funnel.php">
													 Update
												</a>
												
											</li>

										</ul>
                                    </li>

                                    <li class="nav-parent">
                                    <a >
                                        <!-- <span class="pull-right label label-primary">182</span> -->
                                        <i class="fa fa-shopping-cart" aria-hidden="true"></i>
                                        <span>Order</span>
                                    </a>

                                    <ul class="nav nav-children">

                                        


                                        <li class="nav-parent" >
                                            <a >
                                                Quotation 
                                            </a>
                                            <ul class="nav nav-children">
                                                <li>
                                                    <a href="quotdash.php">
                                                         Dashboard
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="quotation-upload.php">
                                                         Upload
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="quotgen.php">
                                                         Generate
                                                    </a>
                                                </li>
                                            </ul>
                                        </li>
                                        
                                        <li class="nav-parent">
                                            <a >
                                                 PO
                                            </a>
                                            <ul class="nav nav-children">
                                                <li>
                                                    <a href="podash.php">
                                                         Dashboard
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="po-upload.php">
                                                         Upload
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="pogen.php">
                                                         Generate
                                                    </a>
                                                </li>
                                            </ul>
                                        </li>

                                        <li class="nav-parent">
                                            <a >
                                                 Invoice
                                            </a>
                                            <ul class="nav nav-children">
                                               <li>
                                                    <a href="invdash.php">
                                                         Dashboard
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="invoice-upload.php">
                                                         Upload
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="invgen.php">
                                                         Generate
                                                    </a>
                                                </li>
                                                
                                            </ul>
                                        </li>
                                        

                                        
                                    </ul>

                                </li>
                                 

									
                                    <li class="nav-parent">
									<a >
										
										<i class="fa fa-check" aria-hidden="true"></i>
										<span>Target Vs Achievement</span>
									</a>
									<ul class="nav nav-children">
	
	
										<!-- <li >
											<a href="funneldash.php">
												View 
											</a>
											
										</li> -->
										<li >
											<a href="achievement.php">
												 Achievement
											</a>
											
										</li>
										
										<li >
											<a href="targetset.php">
												Set Target
											</a>
											
										</li>
										
	
									</ul>
								</li>
								<li class="nav-parent">
                                    <a >
                                        <!-- <span class="pull-right label label-primary">182</span> -->
                                        <i class="fa fa-users" aria-hidden="true"></i>
                                        <span>HR</span>
                                    </a>

                                    <ul class="nav nav-children">

                                        


                                        <li class="nav-parent" >
                                            <a >
                                                Employee 
                                            </a>
                                            <ul class="nav nav-children">
                                                <li>
                                                    <a href="emplist.php">
                                                         Employee List
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="addemp.php">
                                                         Add
                                                    </a>
                                                </li>
                                            </ul>
                                        </li>
                                        
                                        <li class="nav-parent">
                                            <a >
                                                 Holiday
                                            </a>
                                            <ul class="nav nav-children">
                                                <li>
                                                    <a href="holidaylist.php">
                                                         Holiday List
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="addholiday.php">
                                                         Add
                                                    </a>
                                                </li>
                                            </ul>
                                        </li>

                                        <li class="nav-parent">
                                            <a >
                                                 Leave
                                            </a>
                                            <ul class="nav nav-children">
                                                <li>
                                                    <a href="leavedash.php">
                                                         Applications
                                                    </a>
                                                </li>
                                                
                                            </ul>
                                        </li>
                                        

                                        
                                    </ul>

                                </li>

                                <li class="nav-parent">
                                    <a >
                                    
                                       <i class="fa fa-shopping-bag" aria-hidden="true"></i>
                                       <span>Products</span>
                                    </a>
                                    <ul class="nav nav-children">


                                            <li >
                                                <a href="proddash.php">
                                                 Dashboard
                                                </a>
                                        
                                            </li>
                                           <li >
                                                <a href="addprod.php">
                                                 Add
                                                </a>
                                        
                                           </li>
                                    
                                          

                                    </ul>
                                 </li>

                                 <li class="nav-parent">
                                    <a >
                                    
                                       <i class="fa fa-cogs" aria-hidden="true"></i>
                                       <span>Change</span>
                                    </a>
                                    <ul class="nav nav-children">


                                            <li >
                                                <a href="changepros.php">
                                                 Prospects
                                                </a>
                                        
                                            </li>
                                           <li >
                                                <a href="changecamp.php">
                                                 Campaign
                                                </a>
                                        
                                           </li>
                                           <li >
                                                <a href="#">
                                                 Meeting
                                                </a>
                                        
                                           </li>
                                           <li >
                                                <a href="#">
                                                 Funnel
                                                </a>
                                        
                                           </li>
                                           <li >
                                                <a href="#">
                                                 Order
                                                </a>
                                        
                                           </li>
                                           <li >
                                                <a href="#">
                                                 Target
                                                </a>
                                        
                                           </li>
                                           <li >
                                                <a href="#">
                                                 HR
                                                </a>
                                        
                                           </li>
                                           <li >
                                                <a href="#">
                                                 Products
                                                </a>
                                        
                                           </li>

                                    
                                          

                                    </ul>
                                 </li>

                                 
									<li>
										<a href="#">
											
											<i class="fa fa-book" aria-hidden="true"></i>
											<span>Reports</span>
										</a>
                                    </li>

									<!-- <li>
										<a href="upd.php">
											
											<i class="fa fa-refresh" aria-hidden="true"></i>
											<span>Update</span>
										</a>
                                    </li>
									<li>
										<a href="csv.php">
											
											<i class="fa fa-refresh" aria-hidden="true"></i>
											<span>Update CSV</span>
										</a>
                                    </li> -->


									<!-- <li class="nav-parent">
										<a>
											<i class="fa fa-copy" aria-hidden="true"></i>
											<span>Pages</span>
										</a>
										<ul class="nav nav-children">
											<li>
												<a href="pages-signup.html">
													 Sign Up
												</a>
											</li>
											<li>
												<a href="pages-signin.html">
													 Sign In
												</a>
											</li>
											<li>
												<a href="pages-recover-password.html">
													 Recover Password
												</a>
											</li>
											<li>
												<a href="pages-lock-screen.html">
													 Locked Screen
												</a>
											</li>
											<li>
												<a href="pages-user-profile.html">
													 User Profile
												</a>
											</li>
											<li>
												<a href="pages-session-timeout.html">
													 Session Timeout
												</a>
											</li>
											<li>
												<a href="pages-calendar.html">
													 Calendar
												</a>
											</li>
											<li>
												<a href="pages-timeline.html">
													 Timeline
												</a>
											</li>
											<li>
												<a href="pages-media-gallery.html">
													 Media Gallery
												</a>
											</li>
											<li>
												<a href="pages-invoice.html">
													 Invoice
												</a>
											</li>
											<li>
												<a href="pages-blank.html">
													 Blank Page
												</a>
											</li>
											<li>
												<a href="pages-404.html">
													 404
												</a>
											</li>
											<li>
												<a href="pages-500.html">
													 500
												</a>
											</li>
											<li>
												<a href="pages-log-viewer.html">
													 Log Viewer
												</a>
											</li>
											<li >
												<a href="pages-search-results.html">
													 Search Results
												</a>
											</li>
										</ul>
									</li> -->
									
									
								</ul>
							</nav>
				
							<!-- <hr class="separator" />
				
							<div class="sidebar-widget widget-tasks">
								<div class="widget-header">
									<h6>Projects</h6>
									<div class="widget-toggle">+</div>
								</div>
								<div class="widget-content">
									<ul class="list-unstyled m-none">
										<li><a href="#">Porto HTML5 Template</a></li>
										<li><a href="#">Tucson Template</a></li>
										<li><a href="#">Porto Admin</a></li>
									</ul>
								</div>
							</div>
 -->				

                           
							<hr class="separator" />
				
							<div class="sidebar-widget widget-stats">
								<div class="widget-header">
									<h6>Company Stats</h6>
									<div class="widget-toggle">+</div>
								</div>
								<div class="widget-content">
									<ul>
										<li>
											<span class="stats-title">Stat 1</span>
											<span class="stats-complete">85%</span>
											<div class="progress">
												<div class="progress-bar progress-bar-primary progress-without-number" role="progressbar" aria-valuenow="85" aria-valuemin="0" aria-valuemax="100" style="width: 85%;">
													<span class="sr-only">85% Complete</span>
												</div>
											</div>
										</li>
										<li>
											<span class="stats-title">Stat 2</span>
											<span class="stats-complete">70%</span>
											<div class="progress">
												<div class="progress-bar progress-bar-primary progress-without-number" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100" style="width: 70%;">
													<span class="sr-only">70% Complete</span>
												</div>
											</div>
										</li>
										<li>
											<span class="stats-title">Stat 3</span>
											<span class="stats-complete">2%</span>
											<div class="progress">
												<div class="progress-bar progress-bar-primary progress-without-number" role="progressbar" aria-valuenow="2" aria-valuemin="0" aria-valuemax="100" style="width: 2%;">
													<span class="sr-only">2% Complete</span>
												</div>
											</div>
										</li>
									</ul>
								</div>
							</div>
						</div>
				
					</div>
				
				</aside>
				<!-- end: sidebar -->

				<section role="main" class="content-body">
					<header class="page-header">
						<h2>Reference Campaign Search</h2>
					
						<div class="right-wrapper pull-right">
							<ol class="breadcrumbs">
								<li>
									<a href="dashboard.php">
										<i class="fa fa-home"></i>
									</a>
								</li>
								<li><span>Campaign</span></li>
								<li><span>Reference</span></li>
								<li><span>Search</span></li>
							</ol>
					
							<a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fa fa-chevron-left"></i></a>
							<!-- <a class="sidebar-right-toggle" ><i class="fa fa-chevron-down"></i></a> -->
						</div>
					</header>

					<!-- start: page -->
					


					<div class="row">
                            <div class="col-xs-12">
                                <section class="panel form-wizard" id="w4">
                                    <header class="panel-heading">
                                        <!-- <div class="panel-actions">
                                            <a href="#" class="panel-action panel-action-toggle" data-panel-toggle></a>
                                            <a href="#" class="panel-action panel-action-dismiss" data-panel-dismiss></a>
                                        </div> -->
                        
                                        <h2 class="panel-title">Create Project</h2>
                                    </header>
                                    <div class="panel-body">
                                        
                                                    <form action="" method="get" class="form-horizontal" novalidate="novalidate"  >
                                            
                                                


                                                    <div class="form-group">

                                                    	<label class="col-sm-2 control-label" for="w4-username">Project</label>
                                                        <div class="col-sm-3" style="align:right">
                                                            
                                                            <select class="form-control" name="project"  data-plugin-multiselect data-plugin-options='{ "enableCaseInsensitiveFiltering": true }' id="ms_example6" required >
																<?php
																	$project=mysqli_query($dbc,"select * from `campaign-projects`");
																	echo '<option >Select</option>';
																	while($row=mysqli_fetch_assoc($project))
																	{
																		//$pro=$row['ProjectName'];
														
																		 echo '<option  value="'.$row['ProjectName'].'">'.$row['ProjectName'].'</option>';
																		
																	}
																?>
															</select>
                                                        </div>

                                                        <label class="col-sm-2 control-label" for="w4-username">Sector</label>
                                                        <div class="col-sm-4">
                                                           
                                                            <select class="form-control" name="sector[]" multiple="multiple" data-plugin-multiselect data-plugin-options='{ "enableCaseInsensitiveFiltering": true }' id="ms_example6"  >
                                                            
                                                            <option >Select</option>
                                                            <option value="IT">IT</option>
                                                            <option value="Manufacturing">Manufacturing</option>
                                                            <option value="Medical">Medical</option>
                                                            <option value="Education">Education</option>
                                                            <option value="Construction">Construction</option>
                                                            <option value="Pharma">Pharma</option>
                                                            <option value="NBFC">NBFC</option>
                                                            <option value="Finance">Finance</option>
                                                            <option value="Logistic">Logistic</option>
                                                            <option value="Hospitality">Hospitality</option>
                                                            <option value="Govt">Govt</option>
                                                            <option value="Media">Media</option>
															<option value="Gaming">Gaming</option>
                                                            


                                                        
                                                    </select>
                                                        </div>
                                                    
                                                    
                                                        
                                                    </div>

                                                    <div class="form-group">

                                                    	<label class="col-sm-2 control-label" for="w4-username">No of Employees</label>
                                                        <div class="col-sm-3" style="align:right">
                                                            
                                                            <select class="form-control" name="noofemployees" data-plugin-multiselect data-plugin-options='{ "enableCaseInsensitiveFiltering": true }' id="ms_example6"  >
                                                            	
                                                            	<option >Select</option>
                                                            	<option value="0-50">0-50</option>
                                                            	<option value="51-250">51-250</option>
                                                            	<option value="251-500">251-500</option>
                                                            	<option value="501-1000">501-1000</option>
                                                            	<option value="Above 1000">Above 1000</option>
                                                            	
                                                    </select>
                                                        </div>

                                                        <label class="col-sm-2 control-label" for="w4-username">Location</label>
                                                        <div class="col-sm-4">
                                                           
                                                            <select class="form-control" name="location"  data-plugin-multiselect data-plugin-options='{ "enableCaseInsensitiveFiltering": true }' id="ms_example6" required >
																<?php
																	$project=mysqli_query($dbc,"select distinct(SubLocation) from `customers`");
																	echo '<option >Select</option>';
																	while($row=mysqli_fetch_assoc($project))
																	{
																		//$pro=$row['ProjectName'];
														
																		 echo '<option  value="'.$row['SubLocation'].'">'.$row['SubLocation'].'</option>';
																		
																	}
																?>
															</select>
                                                        </div>
                                                    
                                                    
                                                        
                                                    </div>


                                                    
                                                    <div class="form-group">
                                                        <div class="col-sm-2"></div>
                                                        <div class="col-sm-9">
                                                            <div class="checkbox-custom">
                                                            <input type="submit" name="submit" value="Submit" class="btn btn-success pull-right" />
                                                                
                                                            </div>
                                                        </div>
                                                    </div>
                                                    
                                            
                                        </form>
                                                  
                                    </div>
                                    
                                </section>
                            </div>
                        </div>



                        <div class="row">
					
					<div class="col-lg-12 col-md-12">
						<section class="panel">
							<header class="panel-heading panel-heading-transparent">
								<!-- <div class="panel-actions">
									<a href="#" class="panel-action panel-action-toggle" data-panel-toggle></a>
									<a href="#" class="panel-action panel-action-dismiss" data-panel-dismiss></a>
								</div> -->

								<h2 class="panel-title">Customers List</h2>
							</header>
							<div class="panel-body">
								<div class="table-responsive">
									<?php
										/*$fetdetail=mysqli_query($dbc,"select * from `$pro` where `Sector`='$item' or SubLocation='$location' or NoOfEmployees='$noofemployees' ");*/

										echo '<table class="table table-bordered table-striped mb-none" id="datatable-tabletools" data-swf-path="assets/vendor/jquery-datatables/extras/TableTools/swf/copy_csv_xls_pdf.swf">';
											echo '<thead>';
												echo '<tr>';
													echo '<th>Company</th>';
                                                    echo '<th>Sector</th>';
                                                    echo '<th>Contact-Person</th>';
                                                    echo '<th>Designation</th>';
                                                    echo '<th>Department</th>';
                                                    echo '<th>Mobile-No</th>';
                                                    echo '<th>Mail-ID</th>';
                                                    echo '<th>Address</th>';
                                                    echo '<th>Location</th>';
                                                    echo '<th>Sub-Location</th>';
                                                    echo '<th>No Of Employees</th>';
                                                    echo '<th>CompanyType</th>';
                                                    echo '<th>Update</th>';
                                                    /*echo '<th>Remove</th>';*/
												echo '</tr>';
											echo '</thead>';
											echo '<tbody>';
											if(isset($_GET['submit']))
									{
										
										$sector=implode(',', $_GET['sector']);
										$pro=$_GET['project'];
										$noofemployees=$_GET['noofemployees'];
										$location=$_GET['location'];
										$array =  explode(',', $sector);


										foreach ($array as $item) {




										$fetdetail=mysqli_query($dbc,"select * from `$pro` where `Sector`='$item' or SubLocation='$location' or NoOfEmployees='$noofemployees' ");
												while($frow=mysqli_fetch_assoc($fetdetail))
												{
                                                    $cid=$frow['cid'];
													$company=$frow['Company'];
                                                    $sector=$frow['Sector'];
                                                    $firstname=$frow['FirstName'];
                                                    $lastname=$frow['LastName'];
                                                    $designation=$frow['Designation'];
                                                    $department=$frow['Dept'];
                                                    $mobile=$frow['Mobile'];
                                                    $mailid=$frow['Mail'];
                                                    $address=$frow['Address'];
                                                    $location=$frow['Location'];
                                                    $sublocation=$frow['SubLocation'];
                                                    $noofemployees=$frow['NoOfEmployees'];
                                                    $companytype=$frow['CompanyType'];
													
													
														echo '<tr>';
														echo "<td><a href='rins.php?c=".$company."&f=".$firstnamename."&l=".$lastname."&e=".$mailid."&t=$pro' >{$frow['Company']}</a></td>";
                                                        echo "<td>{$frow['Sector']}</td>";
                                                        echo "<td>{$frow['FirstName']} {$frow['LastName']}</td>";
                                                        echo "<td>{$frow['Designation']}</td>";
                                                        echo "<td>{$frow['Dept']}</td>";
                                                        echo "<td><a href=tel:{$frow['Mobile']}>{$frow['Mobile']}</a></td>";
                                                        echo "<td>{$frow['Mail']}</td>";
                                                        echo "<td>{$frow['Address']}</td>";
                                                        echo "<td>{$frow['Location']}</td>";
                                                        echo "<td>{$frow['SubLocation']}</td>";
                                                        echo "<td>{$frow['NoOfEmployees']}</td>";
                                                        echo "<td>".$companytype."</td>";
                                                        echo "<td><a  href='editcust.php?u=$cid' class='text-muted text-uppercase btn btn-primary'  ><font color='white'>Edit</font></a></td>";
                                                        /*echo "<td><a  href='../auth/customer/delcust.php?u=$cid' class='delete text-muted text-uppercase btn btn-danger'   ><font color='white'>Delete</font></a></td>";*/


														
													}	
												}
											}
											echo '</tbody>';
										echo '</table>';
									?>
                                    <!-- <a id="showcustomer" class="text-muted text-uppercase btn btn-primary"  ><font color="white">view</font></a> -->
                                                    
									
								</div>
							</div>
						</section>
					</div>
                    
				</div>





						
					</div>
					<!-- end: page -->
				</section>

				<aside id="sidebar-right" class="sidebar-right">
                <div class="nano">
                    <div class="nano-content">
                        <a href="#" class="mobile-close visible-xs">
                            Collapse <i class="fa fa-chevron-right"></i>
                        </a>
            
                        <div class="sidebar-right-wrapper">
            
                            <div class="sidebar-widget widget-calendar">
                                <h6>Upcoming Tasks</h6>
                                <div data-plugin-datepicker data-plugin-skin="dark" ></div>
            
                                <ul>
                                    <li>
                                        <!-- <time datetime="2014-04-19T00:00+00:00">04/19/2014</time> -->
                                        <span>Company</span>
                                    </li>
                                </ul>
                            </div>
            
                            <div class="sidebar-widget widget-friends">
                                <h6 style="color: red">Meeting</h6>
                                <ul>

                                    <?php
                                            $fetfunnel=mysqli_query($dbc,"select * from `meeting` where  `FollowUp`='Yes' order by FollowUpDate desc");

                                           
                                                    while($frow=mysqli_fetch_assoc($fetfunnel))
                                                    {
                                                        $company=$frow['Company'];
                                                        $followupdate=$frow['FollowUpDate'];
                                                        $followuptime=$frow['FollowUpTime'];
                                                        $project=$frow['ProjectName'];
                                                        $requirement=$frow['Requirement'];
                                                        $remark=$frow['Remarks'];
                                                        $infrastructure=$frow['Infrastructure'];
                                                        $contactperson=$frow['ContactPerson'];
                                                        $designation=$frow['Designation'];
                                                        $mobile=$frow['Mobile'];
                                                        $mail=$frow['Mail'];
                                                        


                                                        $today=date("Y-m-d");
                                                        $cdate=date('h:i a', strtotime($followuptime));

                                                        



                                                        if($followupdate == $today)
                                                        {

                                                            echo "<li class=status-online>
                                        
                                        <div class=profile-info>
                                            <span class=name>{$frow['Company']}</span>
                                            <span class=title>Contact Person: {$frow['ContactPerson']}</span>
                                            <span class=title>Mobile: <a href=tel:{$frow['Mobile']}>{$frow['Mobile']}</a></span>
                                            <span class=title>Meeting Time: $cdate</span>
                                        </div>
                                    </li>";
   
                                                        } else
                                                        {
                                                            /*echo '<tr>';
                                                            echo "<td>Sorry There is no FollowUp Today</td>";
                                                            
                                                            echo '</tr>';*/
                                                        } 
                                                        

                                                    }
                                                    
                                                    
                                               
                                        ?>
                                    
                                </ul>
                            </div>




                            <div class="sidebar-widget widget-friends">
                                <h6 style="color: red">Funnel Follow Up</h6>
                                <ul>

                                    <?php
                                            $fetfunnel=mysqli_query($dbc,"select * from `funnel` where  MONTH(ModificationDetail) = MONTH(CURRENT_DATE()) AND YEAR(ModificationDetail) = YEAR(CURRENT_DATE()) ");

                                           
                                                    while($frow=mysqli_fetch_assoc($fetfunnel))
                                                {
                                                    global $totalrevenue;
                                                    global $totalmargin;

                                                    $company=$frow['Company'];
                                                    $product=$frow['Products'];
                                                    $detail=$frow['Detail'];
                                                    $revenue=$frow['Revenue'];
                                                    $margin=$frow['Margin'];
                                                    $stage=$frow['Stage'];
                                                    $probability=$frow['Probability'];
                                                    $dateofclosure=$frow['DateOfClosure'];
                                                    $closuremonth=$frow['ExpectedClosure'];
                                                    $closuredate=$frow['ModificationDetail'];
                                                    
                                                    $closuredate_final=date("Y-m-d", strtotime($closuredate));
                                                        


                                                        $today=date("Y-m-d");
                                                        $cdate=date('h:i a', strtotime($followuptime));

                                                        



                                                        if($dateofclosure == $today)
                                                        {

                                                            echo "<li class=status-online>
                                        
                                        <div class=profile-info>
                                            <span class=name>{$frow['Company']}</span>
                                            <span class=title>Product: {$frow['Products']}</span>
                                            
                                        </div>
                                    </li>";
   
                                                        } else
                                                        {
                                                            /*echo '<tr>';
                                                            echo "<td>Sorry There is no FollowUp Today</td>";
                                                            
                                                            echo '</tr>';*/
                                                        } 
                                                        

                                                    }
                                                    
                                                    
                                               
                                        ?>
                                   
                                </ul>
                            </div>

            
                        </div>
                    </div>
                </div>
            </aside>
				
			</div>

		</section>

		<!-- Vendor -->
		<script src="../../../log/assets/vendor/jquery/jquery.js"></script>
		<script src="../../../log/assets/vendor/jquery-browser-mobile/jquery.browser.mobile.js"></script>
		<script src="../../../log/assets/vendor/bootstrap/js/bootstrap.js"></script>
		<script src="../../../log/assets/vendor/nanoscroller/nanoscroller.js"></script>
		<script src="../../../log/assets/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
		<script src="../../../log/assets/vendor/magnific-popup/magnific-popup.js"></script>
		<script src="../../../log/assets/vendor/jquery-placeholder/jquery.placeholder.js"></script>

		<!-- <script src="../../../log/assets/vendor/select2/select2.js"></script> -->
		<script src="../../../log/assets/vendor/jquery-datatables/media/js/jquery.dataTables.js"></script>
        <script src="../../../log/assets/vendor/jquery-datatables/extras/TableTools/js/dataTables.tableTools.min.js"></script>
        <script src="../../../log/assets/vendor/jquery-datatables-bs3/assets/js/datatables.js"></script>

        <script src="../../../log/assets/javascripts/tables/examples.datatables.default.js"></script>
        <script src="../../../log/assets/javascripts/tables/examples.datatables.row.with.details.js"></script>
        <script src="../../../log/assets/javascripts/tables/examples.datatables.tabletools.js"></script>

        <script src="../../../log/assets/vendor/jquery-ui/js/jquery-ui-1.10.4.custom.js"></script>
        <script src="../../../log/assets/vendor/jquery-ui-touch-punch/jquery.ui.touch-punch.js"></script>
       <!--  <script src="../../../log/assets/vendor/select2/select2.js"></script> -->
        <script src="../../../log/assets/vendor/bootstrap-multiselect/bootstrap-multiselect.js"></script>
        

		
		<!-- Theme Base, Components and Settings -->
		<script src="../../../log/assets/javascripts/theme.js"></script>
		
		<!-- Theme Custom -->
		<script src="../../../log/assets/javascripts/theme.custom.js"></script>
		
		<!-- Theme Initialization Files -->
		<script src="../../../log/assets/javascripts/theme.init.js"></script>



		<script src="../../../log/assets/javascripts/forms/examples.wizard.js"></script>
		<script src="../../../log/assets/javascripts/forms/examples.advanced.form.js"></script>

		<script>
										function emptyVal(mytext)
												{
													if(mytext.value == 0)
  												 {
													window.location.reload();
  												 }
												}

										</script>

	</body>
</html>
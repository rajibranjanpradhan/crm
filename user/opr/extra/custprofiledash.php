<?php
include "session_handler.php";
?>

<!doctype html>
<html class="fixed">
	<head>

		<!-- Basic -->
		<meta charset="UTF-8">

		<title>Customers | Navikra CRM</title>
		<meta name="keywords" content="Navikra CRM" />
		<meta name="description" content="Navikra CRM">
		<meta name="author" content="navikra.com">

		<link rel="apple-touch-icon" sizes="180x180" href="../../../log/assets/images/apple-touch-icon.png">
    	<link rel="icon" type="image/png" href="../../../log/assets/images/favicon-32x32.png" sizes="32x32">
    	<link rel="icon" type="image/png" href="../../../log/assets/images/favicon-16x16.png" sizes="16x16">

		<!-- Mobile Metas -->
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

		<!-- Web Fonts  -->
		<link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800|Shadows+Into+Light" rel="stylesheet" type="text/css">

		<!-- Vendor CSS -->
		<link rel="stylesheet" href="../../../log/assets/vendor/bootstrap/css/bootstrap.css" />

		<link rel="stylesheet" href="../../../log/assets/vendor/font-awesome/css/font-awesome.css" />
		<link rel="stylesheet" href="../../../log/assets/vendor/magnific-popup/magnific-popup.css" />
		<link rel="stylesheet" href="../../../log/assets/vendor/bootstrap-datepicker/css/datepicker3.css" />

		<!-- Specific Page Vendor CSS -->
		<link rel="stylesheet" href="../../../log/assets/vendor/jquery-ui/css/ui-lightness/jquery-ui-1.10.4.custom.css" />
		<link rel="stylesheet" href="../../../log/assets/vendor/bootstrap-multiselect/bootstrap-multiselect.css" />
		<link rel="stylesheet" href="../../../log/assets/vendor/morris/morris.css" />

		<!-- Theme CSS -->
		<link rel="stylesheet" href="../../../log/assets/stylesheets/theme.css" />

		<!-- Skin CSS -->
		<link rel="stylesheet" href="../../../log/assets/stylesheets/skins/default.css" />

		<!-- Theme Custom CSS -->
		<link rel="stylesheet" href="../../../log/assets/stylesheets/theme-custom.css">

		<!-- Head Libs -->
		<script src="../../../log/assets/vendor/modernizr/modernizr.js"></script>

    <!-- fa-product icon -->
    <link href="http://netdna.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.css" rel="stylesheet" />

		<!-- Hide and Show -->
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

        <!-- Delete Confirmation -->
        <script src="http://code.jquery.com/jquery-1.11.1.min.js"></script>
        <script language="JavaScript" type="text/javascript">
            $(document).ready(function(){
             $("a.delete").click(function(e){
             if(!confirm('Are you sure?')){
                  e.preventDefault();
                 return false;
                 }
                return true;
             });
            });
        </script>


	</head>
	<body>
		<section class="body">

			<!-- start: header -->
			<header class="header">
            <div class="logo-container">
                <a href="dashboard.php" class="logo">
                    <img src="../../../log/assets/images/logo.png" height="48" alt="Navikra CRM" />
                </a>
                <div class="visible-xs toggle-sidebar-left" data-toggle-class="sidebar-left-opened" data-target="html" data-fire-event="sidebar-left-opened">
                    <i class="fa fa-bars" aria-label="Toggle sidebar"></i>
                </div>
            </div>
        
            <!-- start: search & user box -->
            <div class="header-right">
        
            <?php include "notification.php"; ?>
                
                <?php
                    $ad=mysqli_query($dbc,"select * from team where email='$id'");
                    while($r=mysqli_fetch_assoc($ad))
                    {
                        $name=$r['name'];
                        $role=$r['desig'];
                    }
                ?>
        
                <span class="separator"></span>
        
                <div id="userbox" class="userbox">
                    <a href="#" data-toggle="dropdown">
                        <figure class="profile-picture">
                            <img src="../../../log/assets/images/!logged-user.jpg" alt="Joseph Doe" class="img-circle" data-lock-picture="assets/images/!logged-user.jpg" />
                        </figure>
                        <div class="profile-info" data-lock-name="John Doe" data-lock-email="<?php echo $id; ?>">
                            <span class="name"><?php echo $name; ?></span>
                            <span class="role"><?php echo $role; ?></span>
                        </div>
        
                        <i class="fa custom-caret"></i>
                    </a>
        
                    <div class="dropdown-menu">
                        <ul class="list-unstyled">
                            <li class="divider"></li>
                            <li>
                                <a role="menuitem" tabindex="-1" href="profile.php"><i class="fa fa-user"></i> My Profile</a>
                            </li>
                            <li>
                                <!-- <a role="menuitem" tabindex="-1" href="#" data-lock-screen="true"><i class="fa fa-lock"></i> Lock Screen</a> -->
                            </li>
                            <li>
                                <a role="menuitem" tabindex="-1" href="../../index.php"><i class="fa fa-power-off"></i> Logout</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <!-- end: search & user box -->
        </header>
        <!-- end: header -->

        

        <div class="inner-wrapper">
            <!-- start: sidebar -->
            <aside id="sidebar-left" class="sidebar-left">
            
                <div class="sidebar-header">
                    <div class="sidebar-title">
                        Navigation
                    </div>
                    <div class="sidebar-toggle hidden-xs" data-toggle-class="sidebar-left-collapsed" data-target="html" data-fire-event="sidebar-left-toggle">
                        <i class="fa fa-bars" aria-label="Toggle sidebar"></i>
                    </div>
                </div>
            
                <div class="nano">
                    <div class="nano-content">
                        <nav id="menu" class="nav-main" role="navigation">
                            <ul class="nav nav-main">
                                <li >
                                    <a href="dashboard.php">
                                        <i class="fa fa-home" aria-hidden="true"></i>
                                        <span>Dashboard</span>
                                    </a>
                                </li>
                                <!-- <li>
                                    <a href="mailbox-folder.html">
                                        <span class="pull-right label label-primary">182</span>
                                        <i class="fa fa-envelope" aria-hidden="true"></i>
                                        <span>Mailbox</span>
                                    </a>
                                </li> -->
                                 <li class="nav-parent nav-active nav-expanded">
                                    <a >
                                    
                                       <i class="fa fa-users" aria-hidden="true"></i>
                                       <span>Prospects</span>
                                    </a>
                                    <ul class="nav nav-children">


                                            <li class="nav-active">
                                                <a href="custdash.php">
                                                 Dashboard
                                                </a>
                                        
                                            </li>
                                           <li >
                                                <a href="addcust.php">
                                                 Add Prospect
                                                </a>
                                        
                                           </li>
                                           <li >
                                                <a href="customerprofile.php">
                                                 Add Prospect Profile
                                                </a>
                                        
                                           </li>
                                           <li >
                                                <a href="campcust.php">
                                                 Campaign Prospects
                                                </a>
                                        
                                           </li>
                                    
                                          <!--  <li >
                                              <a href="targetset.php">
                                                 Set Target
                                              </a>
                                        
                                           </li> -->
                                    

                                    </ul>
                                 </li>
                                <li class="nav-parent">
                                    <a >
                                        <!-- <span class="pull-right label label-primary">182</span> -->
                                        <i class="fa fa-plus" aria-hidden="true"></i>
                                        <span>Campaign</span>
                                    </a>

                                    <ul class="nav nav-children">

                                        <li >
                                            
                                            <a href="create-campaign.php">
                                                Create 
                                            </a>
                                            
                                        </li>


                                        <li class="nav-parent" >
                                            <a >
                                                Direct 
                                            </a>
                                            <ul class="nav nav-children">
                                                <li>
                                                    <a href="dirdash.php">
                                                         Show
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="direct.php">
                                                         Insert
                                                    </a>
                                                </li>
                                            </ul>
                                        </li>
                                        
                                        <li class="nav-parent">
                                            <a >
                                                 Digital
                                            </a>
                                            <ul class="nav nav-children">
                                                <li>
                                                    <a href="digdash.php">
                                                         Show
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="digital.php">
                                                         Insert
                                                    </a>
                                                </li>
                                            </ul>
                                        </li>

                                        <li class="nav-parent">
                                            <a >
                                                 Telecalling
                                            </a>
                                            <ul class="nav nav-children">
                                                <li>
                                                    <a href="teldash.php">
                                                         Show
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="telecalling.php">
                                                         Insert
                                                    </a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="nav-parent">
                                            <a >
                                                 Reference
                                            </a>
                                            <ul class="nav nav-children">
                                                <li>
                                                    <a href="refdash.php">
                                                         Show
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="reference.php">
                                                         Insert
                                                    </a>
                                                </li>
                                            </ul>
                                        </li>

                                        
                                    </ul>

                                </li>

                                <li >
                           <a href="lead.php">
                              <!-- <span class="pull-right label label-primary">182</span> -->
                              <i class="fa fa-user" aria-hidden="true"></i>
                              <span>Lead</span>
                           </a>
                           
                        </li>

                                <li class="nav-parent">
                                    <a>
                                        <!-- <span class="pull-right label label-primary">182</span> -->
                                        <i class="fa fa-user" aria-hidden="true"></i>
                                        <span>Meeting</span>
                                    </a>

                                    <ul class="nav nav-children">


                                        <li >
                                            <a href="meetingdash.php">
                                                Dashboard 
                                            </a>
                                            
                                        </li>
                                        
                                        <!-- <li >
                                            <a href="meeting.php">
                                                 Update
                                            </a>
                                            
                                        </li> -->

                                    </ul>

                                </li>

                                <li class="nav-parent">
                                    <a >
                                        
                                        <i class="fa fa-refresh" aria-hidden="true"></i>
                                        <span>Funnel</span>
                                    </a>
                                    <ul class="nav nav-children">


                                        <li >
                                            <a href="funneldash.php">
                                                Show 
                                            </a>
                                            
                                        </li>
                                        
                                        <li >
                                            <a href="funnel.php">
                                                 Update
                                            </a>
                                            
                                        </li>

                                    </ul>
                                </li>

                                <li class="nav-parent">
                                    <a >
                                        <!-- <span class="pull-right label label-primary">182</span> -->
                                        <i class="fa fa-shopping-cart" aria-hidden="true"></i>
                                        <span>Order</span>
                                    </a>

                                    <ul class="nav nav-children">

                                        


                                        <li class="nav-parent" >
                                            <a >
                                                Quotation 
                                            </a>
                                            <ul class="nav nav-children">
                                                <li>
                                                    <a href="quotdash.php">
                                                         Dashboard
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="quotation-upload.php">
                                                         Upload
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="quotgen.php">
                                                         Generate
                                                    </a>
                                                </li>
                                            </ul>
                                        </li>
                                        
                                        <li class="nav-parent">
                                            <a >
                                                 PO
                                            </a>
                                            <ul class="nav nav-children">
                                                <li>
                                                    <a href="podash.php">
                                                         Dashboard
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="po-upload.php">
                                                         Upload
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="pogen.php">
                                                         Generate
                                                    </a>
                                                </li>
                                            </ul>
                                        </li>

                                        <li class="nav-parent">
                                            <a >
                                                 Invoice
                                            </a>
                                            <ul class="nav nav-children">
                                               <li>
                                                    <a href="invdash.php">
                                                         Dashboard
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="invoice-upload.php">
                                                         Upload
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="invgen.php">
                                                         Generate
                                                    </a>
                                                </li>
                                                
                                            </ul>
                                        </li>
                                        

                                        
                                    </ul>

                                </li>
                                 

                                
                                <li class="nav-parent">
								    <a >
									
									   <i class="fa fa-check" aria-hidden="true"></i>
									   <span>Target Vs Achievement</span>
								    </a>
								    <ul class="nav nav-children">


									   <!-- <li >
										  <a href="funneldash.php">
										  	View 
										  </a>
										
								    	</li> -->
									       <li >
										  <a href="achievement.php">
											 Achievement
										  </a>
										
									       </li>
									
									       <li >
										      <a href="targetset.php">
											     Set Target
										      </a>
										
									       </li>
									

								    </ul>
							     </li>

                                 <li class="nav-parent">
                                    <a >
                                        <!-- <span class="pull-right label label-primary">182</span> -->
                                        <i class="fa fa-users" aria-hidden="true"></i>
                                        <span>HR</span>
                                    </a>

                                    <ul class="nav nav-children">

                                        


                                        <li class="nav-parent" >
                                            <a >
                                                Employee 
                                            </a>
                                            <ul class="nav nav-children">
                                                <li>
                                                    <a href="emplist.php">
                                                         Employee List
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="addemp.php">
                                                         Add
                                                    </a>
                                                </li>
                                            </ul>
                                        </li>
                                        
                                        <li class="nav-parent">
                                            <a >
                                                 Holiday
                                            </a>
                                            <ul class="nav nav-children">
                                                <li>
                                                    <a href="holidaylist.php">
                                                         Holiday List
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="addholiday.php">
                                                         Add
                                                    </a>
                                                </li>
                                            </ul>
                                        </li>

                                        <li class="nav-parent">
                                            <a >
                                                 Leave
                                            </a>
                                            <ul class="nav nav-children">
                                                <li>
                                                    <a href="leavedash.php">
                                                         Applications
                                                    </a>
                                                </li>
                                                
                                            </ul>
                                        </li>
                                        

                                        
                                    </ul>

                                </li>

                                    <li class="nav-parent">
                                    <a >
                                    
                                       <i class="fa fa-shopping-bag" aria-hidden="true"></i>
                                       <span>Products</span>
                                    </a>
                                    <ul class="nav nav-children">


                                            <li >
                                                <a href="proddash.php">
                                                 Dashboard
                                                </a>
                                        
                                            </li>
                                           <li >
                                                <a href="addprod.php">
                                                 Add
                                                </a>
                                        
                                           </li>
                                    
                                          

                                    </ul>
                                 </li>

                                 <li class="nav-parent">
                                    <a >
                                    
                                       <i class="fa fa-cogs" aria-hidden="true"></i>
                                       <span>Change</span>
                                    </a>
                                    <ul class="nav nav-children">


                                            <li >
                                                <a href="changepros.php">
                                                 Prospects
                                                </a>
                                        
                                            </li>
                                           <li >
                                                <a href="changecamp.php">
                                                 Campaign
                                                </a>
                                        
                                           </li>
                                           <li >
                                                <a href="#">
                                                 Meeting
                                                </a>
                                        
                                           </li>
                                           <li >
                                                <a href="#">
                                                 Funnel
                                                </a>
                                        
                                           </li>
                                           <li >
                                                <a href="#">
                                                 Order
                                                </a>
                                        
                                           </li>
                                           <li >
                                                <a href="#">
                                                 Target
                                                </a>
                                        
                                           </li>
                                           <li >
                                                <a href="#">
                                                 HR
                                                </a>
                                        
                                           </li>
                                           <li >
                                                <a href="#">
                                                 Products
                                                </a>
                                        
                                           </li>

                                    
                                          

                                    </ul>
                                 </li>

                                   
                                <li>
                                    <a href="#">
                                        
                                        <i class="fa fa-book" aria-hidden="true"></i>
                                        <span>Reports</span>
                                    </a>
                                </li>

                                


                                
                                
                                
                            </ul>
                        </nav>
            
                        <!-- <hr class="separator" />
            
                        <div class="sidebar-widget widget-tasks">
                            <div class="widget-header">
                                <h6>Projects</h6>
                                <div class="widget-toggle">+</div>
                            </div>
                            <div class="widget-content">
                                <ul class="list-unstyled m-none">
                                    <li><a href="#">Porto HTML5 Template</a></li>
                                    <li><a href="#">Tucson Template</a></li>
                                    <li><a href="#">Porto Admin</a></li>
                                </ul>
                            </div>
                        </div>
-->				
                       
                         
                    </div>
            
                </div>
            
            </aside>
            <!-- end: sidebar -->


				<section role="main" class="content-body">
					<header class="page-header">
						<h2>Prospect Profile Dashboard</h2>
					
						<div class="right-wrapper pull-right">
							<ol class="breadcrumbs">
								<li>
									<a href="dashboard.php">
										<i class="fa fa-home"></i>
									</a>
								</li>
								<li><span>Profile</span></li>
                                <li><span>Dashboard</span></li>
							</ol>
					
							<a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fa fa-chevron-left"></i></a>
						</div>
					</header>

					<!-- start: page -->
					
				

				
				<div class="row">
					
					<div class="col-lg-12 col-md-12">
						<section class="panel">
							<!-- <header class="panel-heading panel-heading-transparent"> -->
								<!-- <div class="panel-actions">
									<a href="#" class="panel-action panel-action-toggle" data-panel-toggle></a>
									<a href="#" class="panel-action panel-action-dismiss" data-panel-dismiss></a>
								</div> -->

								<!-- <h2 class="panel-title">Profile</h2>
							</header> -->
							<div class="panel-body">
								<div class="table-responsive">
									<?php
                                    $cname=$_GET['u'];
                                    $cid=$_GET['v'];
										$fetdetail=mysqli_query($dbc,"select * from `customersprofile` where `Company`='$cname' ");

										/*echo '<table class="table table-bordered table-striped mb-none" id="datatable-tabletools" data-swf-path="assets/vendor/jquery-datatables/extras/TableTools/swf/copy_csv_xls_pdf.swf">';*/
                                        echo '<table class="table table-bordered table-striped mb-none" id="datatable-tabletools" >';
											echo '<thead>';
                                                echo '<tr>';
                                                    echo '<th></th>';
                                                    echo '<th></th>';
                                                    echo '<th colspan="4">Server</th>';
                                                    echo '<th colspan="4">Storage</th>';
                                                    echo '<th colspan="4">OS</th>';
                                                    echo '<th colspan="3">Virtualization</th>';
                                                    echo '<th colspan="4">Backup</th>';
                                                    echo '<th colspan="4">Cloud</th>';
                                                    echo '<th colspan="3">Endpoint Management</th>';
                                                    echo '<th colspan="2">Firewall</th>';
                                                    echo '<th colspan="2">DLP</th>';
                                                    echo '<th colspan="2">Mobile Security</th>';
                                                    echo '<th colspan="2">VAPT</th>';
                                                    echo '<th colspan="2">Remote Desktop</th>';
                                                    echo '<th colspan="2">Help Desk</th>';
                                                    echo '<th colspan="2">ERP</th>';
                                                    echo '<th colspan="2">CRM</th>';
                                                    echo '<th colspan="2">Automation</th>';
                                                    echo '<th colspan="3">Mail</th>';
                                                    echo '<th colspan="2">AD</th>';
                                                    echo '<th colspan="2">Analytics Tool</th>';
                                                    echo '<th colspan="2">ISO 27001</th>';
                                                    echo '<th colspan="2">ITIL</th>';
                                                    
                                                    echo '<th>Update</th>';
                                                    
                                                echo '</tr>';
												echo '<tr>';
													echo '<th>Sl No.</th>';
                                                    echo '<th>Company</th>';

                                                    echo '<th>No of Server</th>';
                                                    echo '<th>OEM</th>';
                                                    echo '<th>Age</th>';
                                                    echo '<th>Configuration</th>';

                                                    echo '<th>Storage Type</th>';
                                                    echo '<th>OEM</th>';
                                                    echo '<th>No of Storage Box</th>';
                                                    echo '<th>Data Size</th>';

                                                    echo '<th>OS Name</th>';
                                                    echo '<th>OS Detail</th>';
                                                    echo '<th>OEM</th>';
                                                    echo '<th>OS Version</th>';

                                                    echo '<th>OEM</th>';
                                                    echo '<th>Version</th>';
                                                    echo '<th>Renewal Date</th>';

                                                    echo '<th>Data Size</th>';
                                                    echo '<th>OEM</th>';
                                                    echo '<th>Version</th>';
                                                    echo '<th>Renewal Date</th>';

                                                    echo '<th>OEM</th>';
                                                    echo '<th>Use Case</th>';
                                                    echo '<th>Monthly Billing</th>';
                                                    echo '<th>Managed By</th>';

                                                    echo '<th>No of Endpoints</th>';
                                                    echo '<th>OEM</th>';
                                                    echo '<th>Renewal Date</th>';

                                                    echo '<th>Age</th>';
                                                    echo '<th>OEM</th>';

                                                    echo '<th>Renewal Date</th>';
                                                    echo '<th>OEM</th>';

                                                    echo '<th>OEM</th>';
                                                    echo '<th>OEM</th>';

                                                    echo '<th>Status</th>';
                                                    echo '<th>Plan</th>';

                                                    echo '<th>Using Remote Desktop</th>';
                                                    echo '<th>OEM</th>';

                                                    echo '<th>Using Help Desk</th>';
                                                    echo '<th>OEM</th>';

                                                    echo '<th>Using ERP</th>';
                                                    echo '<th>OEM</th>';

                                                    echo '<th>Using CRM</th>';
                                                    echo '<th>OEM</th>';

                                                    echo '<th>Using Automation</th>';
                                                    echo '<th>OEM</th>';
                                                    
                                                    echo '<th>No of Users</th>';
                                                    echo '<th>OEM</th>';
                                                    echo '<th>Renewal Date</th>';

                                                    echo '<th>Using AD</th>';
                                                    echo '<th>OEM</th>';

                                                    echo '<th>Using Tool</th>';
                                                    echo '<th>OEM</th>';

                                                    echo '<th>Status</th>';
                                                    echo '<th>Plan</th>';

                                                    echo '<th>Status</th>';
                                                    echo '<th>Plan</th>';

                                                    echo '<th>Update</th>';
                                                    
												echo '</tr>';
											echo '</thead>';
											echo '<tbody>';
												while($frow=mysqli_fetch_assoc($fetdetail))
												{
                                                    $ccid=$frow['id'];
                                                    $cpid=$frow['cid'];
													$company=$frow['Company'];
                                                    $noofserver=$frow['NoOfServer'];
                                                    $serveroem=$frow['ServerOEM'];
                                                    $serverage=$frow['ServerAge'];
                                                    $serverconfig=$frow['ServerConfig'];
                                                    $storagetype=$frow['StorageType'];
                                                    $storageoem=$frow['StorageOEM'];
                                                    $noofstoragebox=$frow['NoOfStorageBox'];
                                                    $storagedatasize=$frow['StorageDataSize'];
                                                    $osname=$frow['OSName'];
                                                    $osdetail=$frow['OSDetail'];
                                                    $osoem=$frow['OSOEM'];
                                                    $osversion=$frow['OSVersion'];
                                                    $virtualizationoem=$frow['VirtualizationOEM'];
                                                    $virtualizationversion=$frow['VirtualizationVersion'];
                                                    $virtualizationrenewaldate=$frow['VirtualizationRenewalDate'];
                                                    $backupdatasize=$frow['BackupDataSize'];
                                                    $backupoem=$frow['BackupOEM'];
                                                    $backupversion=$frow['BackupVersion'];
                                                    $backuprenewaldate=$frow['BackupRenewalDate'];
                                                    $cloudoem=$frow['CloudOEM'];
                                                    $cloudusecase=$frow['CloudUseCase'];
                                                    $cloudmonthlybilling=$frow['CloudMonthlyBilling'];
                                                    $cloudmanagedby=$frow['CloudManagedBy'];
                                                    $noofendpoints=$frow['NoOfEndpoints'];
                                                    $endpointoem=$frow['EndpointOEM'];
                                                    $endpointrenewaldate=$frow['EndpointRenewalDate'];
                                                    $firewallage=$frow['FirewallAge'];
                                                    $firewalloem=$frow['FirewallOEM'];
                                                    $dlprenewaldate=$frow['DLPRenewalDate'];
                                                    $dlpoem=$frow['DLPOEM'];
                                                    $mobilesecurity=$frow['MobileSecurity'];
                                                    $mobilesecurityoem=$frow['MobileSecurityOEM'];
                                                    $vaptstatus=$frow['VAPTStatus'];
                                                    $vaptplan=$frow['VAPTPlan'];
                                                    $usingremotedesktop=$frow['UsingRemoteDesktop'];
                                                    $remotedesktopoem=$frow['RemoteDesktopOEM'];
                                                    $usinghelpdesk=$frow['UsingHelpDesk'];
                                                    $helpdeskoem=$frow['HelpDeskOEM'];
                                                    $usingerp=$frow['UsingERP'];
                                                    $erpoem=$frow['ERPOEM'];
                                                    $usingcrm=$frow['UsingCRM'];
                                                    $crmoem=$frow['CRMOEM'];
                                                    $usingautomation=$frow['UsingAutomation'];
                                                    $automationoem=$frow['AutomationOEM'];
                                                    $mailnoofusers=$frow['MailNoOfUsers'];
                                                    $mailoem=$frow['MailOEM'];
                                                    $mailrenewaldate=$frow['MailRenewalDate'];
                                                    $usingad=$frow['UsingAD'];
                                                    $adoem=$frow['ADOEM'];
                                                    $usinganalyticstool=$frow['UsingAnalyticsTool'];
                                                    $analyticstooloem=$frow['AnalyticsToolOEM'];
                                                    $iso27001status=$frow['ISO27001Status'];
                                                    $iso27001plan=$frow['ISO27001Plan'];
                                                    $itilstatus=$frow['ITILStatus'];
                                                    $itilplan=$frow['ITILPlan'];


													
														echo '<tr>';
														echo "<td>".$cid."</td>";
                                                        echo "<td>{$frow['Company']}</td>";
                                                        echo "<td>{$frow['NoOfServer']}</td>";
                                                        echo "<td>{$frow['ServerOEM']}</td>";
                                                        echo "<td>{$frow['ServerAge']}</td>";
                                                        echo "<td>{$frow['ServerConfig']}</td>";
                                                        echo "<td>{$frow['StorageType']}</td>";
                                                        echo "<td>{$frow['StorageOEM']}</td>";
                                                        echo "<td>{$frow['NoOfStorageBox']}</td>";
                                                        echo "<td>{$frow['StorageDataSize']}</td>";
                                                        echo "<td>{$frow['OSName']}</td>";
                                                        echo "<td>{$frow['OSDetail']}</td>";
                                                        echo "<td>{$frow['OSOEM']}</td>";
                                                        echo "<td>{$frow['OSVersion']}</td>";
                                                        echo "<td>{$frow['VirtualizationOEM']}</td>";
                                                        echo "<td>{$frow['VirtualizationVersion']}</td>";
                                                        echo "<td>{$frow['VirtualizationRenewalDate']}</td>";
                                                        echo "<td>{$frow['BackupDataSize']}</td>";
                                                        echo "<td>{$frow['BackupOEM']}</td>";
                                                        echo "<td>{$frow['BackupVersion']}</td>";
                                                        echo "<td>{$frow['BackupRenewalDate']}</td>";
                                                        echo "<td>{$frow['CloudOEM']}</td>";
                                                        echo "<td>{$frow['CloudUseCase']}</td>";
                                                        echo "<td>{$frow['CloudMonthlyBilling']}</td>";
                                                        echo "<td>{$frow['CloudManagedBy']}</td>";
                                                        echo "<td>{$frow['NoOfEndpoints']}</td>";
                                                        echo "<td>{$frow['EndpointOEM']}</td>";
                                                        echo "<td>{$frow['EndpointRenewalDate']}</td>";
                                                        echo "<td>{$frow['FirewallAge']}</td>";
                                                        echo "<td>{$frow['FirewallOEM']}</td>";
                                                        echo "<td>{$frow['DLPRenewalDate']}</td>";
                                                        echo "<td>{$frow['DLPOEM']}</td>";
                                                        echo "<td>{$frow['MobileSecurity']}</td>";
                                                        echo "<td>{$frow['MobileSecurityOEM']}</td>";
                                                        echo "<td>{$frow['VAPTStatus']}</td>";
                                                        echo "<td>{$frow['VAPTPlan']}</td>";
                                                        echo "<td>{$frow['UsingRemoteDesktop']}</td>";
                                                        echo "<td>{$frow['RemoteDesktopOEM']}</td>";
                                                        echo "<td>{$frow['UsingHelpDesk']}</td>";
                                                        echo "<td>{$frow['HelpDeskOEM']}</td>";
                                                        echo "<td>{$frow['UsingERP']}</td>";
                                                        echo "<td>{$frow['ERPOEM']}</td>";
                                                        echo "<td>{$frow['UsingCRM']}</td>";
                                                        echo "<td>{$frow['CRMOEM']}</td>";
                                                        echo "<td>{$frow['UsingAutomation']}</td>";
                                                        echo "<td>{$frow['AutomationOEM']}</td>";
                                                        echo "<td>{$frow['MailNoOfUsers']}</td>";
                                                        echo "<td>{$frow['MailOEM']}</td>";
                                                        echo "<td>{$frow['MailRenewalDate']}</td>";
                                                        echo "<td>{$frow['UsingAD']}</td>";
                                                        echo "<td>{$frow['ADOEM']}</td>";
                                                        echo "<td>{$frow['UsingAnalyticsTool']}</td>";
                                                        echo "<td>{$frow['AnalyticsToolOEM']}</td>";
                                                        echo "<td>{$frow['ISO27001Status']}</td>";
                                                        echo "<td>{$frow['ISO27001Plan']}</td>";
                                                        echo "<td>{$frow['ITILStatus']}</td>";
                                                        echo "<td>{$frow['ITILPlan']}</td>";


                                                        
                                                        echo "<td><a  href='editcustprofile.php?u=$ccid&v=$cid' class='text-muted text-uppercase btn btn-primary'  ><font color='white'>Edit</font></a></td>";
                                                        /*echo "<td><a  href='../auth/customer/delcust.php?u=$cid' class='delete text-muted text-uppercase btn btn-danger'   ><font color='white'>Delete</font></a></td>";*/


														
														
												}
											echo '</tbody>';
										echo '</table>';
									?>
                                    <!-- <a id="showcustomer" class="text-muted text-uppercase btn btn-primary"  ><font color="white">view</font></a> -->
                                                    
									
								</div>
							</div>
						</section>
					</div>
                    
				</div>
                    <!-- <div class="row">
						<div class="col-md-12">
							<section class="panel">
								<header class="panel-heading">
									<div class="panel-actions">
										<a href="#" class="panel-action panel-action-toggle" data-panel-toggle></a>
										<a href="#" class="panel-action panel-action-dismiss" data-panel-dismiss></a>
									</div>

									<h2 class="panel-title">Best Seller</h2>
									<p class="panel-subtitle">Customize the graphs as much as you want, there are so many options and features to display information using Porto Admin Template.</p>
								</header>
								<div class="panel-body">

									Flot: Basic
									<div class="chart chart-md" id="flotDashBasic"></div>
									<script>

										var flotDashBasicData = [{
											data: [
												[0, 170],
												[1, 169],
												[2, 173],
												[3, 188],
												[4, 147],
												[5, 113],
												[6, 128],
												[7, 169],
												[8, 173],
												[9, 128],
												[10, 128]
											],
											label: "Series 1",
											color: "#0088cc"
										}, {
											data: [
												[0, 115],
												[1, 124],
												[2, 114],
												[3, 121],
												[4, 115],
												[5, 83],
												[6, 102],
												[7, 148],
												[8, 147],
												[9, 103],
												[10, 113]
											],
											label: "Series 2",
											color: "#2baab1"
										}, {
											data: [
												[0, 70],
												[1, 69],
												[2, 73],
												[3, 88],
												[4, 47],
												[5, 13],
												[6, 28],
												[7, 69],
												[8, 73],
												[9, 28],
												[10, 28]
											],
											label: "Series 3",
											color: "#734ba9"
										}];

										

									</script>

								</div>
							</section>
						</div>
						
					</div> -->
                    
					<!-- end: page -->
				</section>
			</div>

			<aside id="sidebar-right" class="sidebar-right">
                <div class="nano">
                    <div class="nano-content">
                        <a href="#" class="mobile-close visible-xs">
                            Collapse <i class="fa fa-chevron-right"></i>
                        </a>
            
                        <div class="sidebar-right-wrapper">
            
                            <div class="sidebar-widget widget-calendar">
                                <h6>Upcoming Tasks</h6>
                                <div data-plugin-datepicker data-plugin-skin="dark" ></div>
            
                                <ul>
                                    <li>
                                        <!-- <time datetime="2014-04-19T00:00+00:00">04/19/2014</time> -->
                                        <span>Company</span>
                                    </li>
                                </ul>
                            </div>
            
                            <div class="sidebar-widget widget-friends">
                                <h6 style="color: red">Meeting</h6>
                                <ul>

                                    <?php
                                            $fetfunnel=mysqli_query($dbc,"select * from `meeting` where  `FollowUp`='Yes' order by FollowUpDate desc");

                                           
                                                    while($frow=mysqli_fetch_assoc($fetfunnel))
                                                    {
                                                        $company=$frow['Company'];
                                                        $followupdate=$frow['FollowUpDate'];
                                                        $followuptime=$frow['FollowUpTime'];
                                                        $project=$frow['ProjectName'];
                                                        $requirement=$frow['Requirement'];
                                                        $remark=$frow['Remarks'];
                                                        $infrastructure=$frow['Infrastructure'];
                                                        $contactperson=$frow['ContactPerson'];
                                                        $designation=$frow['Designation'];
                                                        $mobile=$frow['Mobile'];
                                                        $mail=$frow['Mail'];
                                                        


                                                        $today=date("Y-m-d");
                                                        $cdate=date('h:i a', strtotime($followuptime));

                                                        



                                                        if($followupdate == $today)
                                                        {

                                                            echo "<li class=status-online>
                                        
                                        <div class=profile-info>
                                            <span class=name>{$frow['Company']}</span>
                                            <span class=title>Contact Person: {$frow['ContactPerson']}</span>
                                            <span class=title>Mobile: <a href=tel:{$frow['Mobile']}>{$frow['Mobile']}</a></span>
                                            <span class=title>Meeting Time: $cdate</span>
                                        </div>
                                    </li>";
   
                                                        } else
                                                        {
                                                            /*echo '<tr>';
                                                            echo "<td>Sorry There is no FollowUp Today</td>";
                                                            
                                                            echo '</tr>';*/
                                                        } 
                                                        

                                                    }
                                                    
                                                    
                                               
                                        ?>
                                    
                                </ul>
                            </div>




                            <div class="sidebar-widget widget-friends">
                                <h6 style="color: red">Funnel Follow Up</h6>
                                <ul>

                                    <?php
                                            $fetfunnel=mysqli_query($dbc,"select * from `funnel` where  MONTH(ModificationDetail) = MONTH(CURRENT_DATE()) AND  DATE(ModificationDetail) BETWEEN '$fydate' AND '$lydate'  ");

                                           
                                                    while($frow=mysqli_fetch_assoc($fetfunnel))
                                                {
                                                    global $totalrevenue;
                                                    global $totalmargin;

                                                    $company=$frow['Company'];
                                                    $product=$frow['Products'];
                                                    $detail=$frow['Detail'];
                                                    $revenue=$frow['Revenue'];
                                                    $margin=$frow['Margin'];
                                                    $stage=$frow['Stage'];
                                                    $probability=$frow['Probability'];
                                                    $dateofclosure=$frow['DateOfClosure'];
                                                    $closuremonth=$frow['ExpectedClosure'];
                                                    $closuredate=$frow['ModificationDetail'];
                                                    
                                                    $closuredate_final=date("Y-m-d", strtotime($closuredate));
                                                        


                                                        $today=date("Y-m-d");
                                                        $cdate=date('h:i a', strtotime($followuptime));

                                                        



                                                        if($dateofclosure == $today)
                                                        {

                                                            echo "<li class=status-online>
                                        
                                        <div class=profile-info>
                                            <span class=name>{$frow['Company']}</span>
                                            <span class=title>Product: {$frow['Products']}</span>
                                            
                                        </div>
                                    </li>";
   
                                                        } else
                                                        {
                                                            /*echo '<tr>';
                                                            echo "<td>Sorry There is no FollowUp Today</td>";
                                                            
                                                            echo '</tr>';*/
                                                        } 
                                                        

                                                    }
                                                    
                                                    
                                               
                                        ?>
                                   
                                </ul>
                            </div>

            
                        </div>
                    </div>
                </div>
            </aside>
		</section>

		<!-- Vendor -->
		<script src="../../../log/assets/vendor/jquery/jquery.js"></script>
		<script src="../../../log/assets/vendor/jquery-browser-mobile/jquery.browser.mobile.js"></script>
		<script src="../../../log/assets/vendor/bootstrap/js/bootstrap.js"></script>
		<script src="../../../log/assets/vendor/nanoscroller/nanoscroller.js"></script>
		<script src="../../../log/assets/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
		<script src="../../../log/assets/vendor/magnific-popup/magnific-popup.js"></script>
		<script src="../../../log/assets/vendor/jquery-placeholder/jquery.placeholder.js"></script>
		
		<!-- Specific Page Vendor -->
		<script src="../../../log/assets/vendor/jquery-ui/js/jquery-ui-1.10.4.custom.js"></script>
		<script src="../../../log/assets/vendor/jquery-ui-touch-punch/jquery.ui.touch-punch.js"></script>
		<script src="../../../log/assets/vendor/jquery-appear/jquery.appear.js"></script>
		<script src="../../../log/assets/vendor/bootstrap-multiselect/bootstrap-multiselect.js"></script>
		<script src="../../../log/assets/vendor/jquery-easypiechart/jquery.easypiechart.js"></script>
		<script src="../../../log/assets/vendor/flot/jquery.flot.js"></script>
		<script src="../../../log/assets/vendor/flot-tooltip/jquery.flot.tooltip.js"></script>
		<script src="../../../log/assets/vendor/flot/jquery.flot.pie.js"></script>
		<script src="../../../log/assets/vendor/flot/jquery.flot.categories.js"></script>
		<script src="../../../log/assets/vendor/flot/jquery.flot.resize.js"></script>
		<script src="../../../log/assets/vendor/jquery-sparkline/jquery.sparkline.js"></script>
		<script src="../../../log/assets/vendor/raphael/raphael.js"></script>
		<script src="../../../log/assets/vendor/morris/morris.js"></script>
		<script src="../../../log/assets/vendor/gauge/gauge.js"></script>
		<script src="../../../log/assets/vendor/snap-svg/snap.svg.js"></script>
		<script src="../../../log/assets/vendor/liquid-meter/liquid.meter.js"></script>
		<script src="../../../log/assets/vendor/jqvmap/jquery.vmap.js"></script>
		<script src="../../../log/assets/vendor/jqvmap/data/jquery.vmap.sampledata.js"></script>
		<script src="../../../log/assets/vendor/jqvmap/maps/jquery.vmap.world.js"></script>
		<script src="../../../log/assets/vendor/jqvmap/maps/continents/jquery.vmap.africa.js"></script>
		<script src="../../../log/assets/vendor/jqvmap/maps/continents/jquery.vmap.asia.js"></script>
		<script src="../../../log/assets/vendor/jqvmap/maps/continents/jquery.vmap.australia.js"></script>
		<script src="../../../log/assets/vendor/jqvmap/maps/continents/jquery.vmap.europe.js"></script>
		<script src="../../../log/assets/vendor/jqvmap/maps/continents/jquery.vmap.north-america.js"></script>
		<script src="../../../log/assets/vendor/jqvmap/maps/continents/jquery.vmap.south-america.js"></script>


        <!-- <script src="../../../log/assets/vendor/select2/select2.js"></script> -->
        <script src="../../../log/assets/vendor/jquery-datatables/media/js/jquery.dataTables.js"></script>
        <script src="../../../log/assets/vendor/jquery-datatables/extras/TableTools/js/dataTables.tableTools.min.js"></script>
        <script src="../../../log/assets/vendor/jquery-datatables-bs3/assets/js/datatables.js"></script>
		
		<!-- Theme Base, Components and Settings -->
		<script src="../../../log/assets/javascripts/theme.js"></script>
		
		<!-- Theme Custom -->
		<script src="../../../log/assets/javascripts/theme.custom.js"></script>
		
		<!-- Theme Initialization Files -->
		<script src="../../../log/assets/javascripts/theme.init.js"></script>


		<!-- Examples -->
		<script src="../../../log/assets/javascripts/dashboard/examples.dashboard.js"></script>


        <script src="../../../log/assets/javascripts/tables/examples.datatables.default.js"></script>
        <script src="../../../log/assets/javascripts/tables/examples.datatables.row.with.details.js"></script>
        <script src="../../../log/assets/javascripts/tables/examples.datatables.tabletools.js"></script>

        <!-- modal form -->
      <script src="../assets/javascripts/ui-elements/examples.modals.js"></script>  

	</body>
</html>
<?php
include "session_handler.php";
?>



<!doctype html>
<html class="fixed">
	<head>

		<!-- Basic -->
		<meta charset="UTF-8">

		<title>Insert | Navikra CRM</title>
		<meta name="keywords" content="Navikra CRM" />
		<meta name="description" content="Navikra CRM">
		<meta name="author" content="navikra.com">

		<link rel="apple-touch-icon" sizes="180x180" href="../assets/images/apple-touch-icon.png">
    	<link rel="icon" type="image/png" href="../assets/images/favicon-32x32.png" sizes="32x32">
    	<link rel="icon" type="image/png" href="../assets/images/favicon-16x16.png" sizes="16x16">

		<!-- Mobile Metas -->
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

		<!-- Web Fonts  -->
		<link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800|Shadows+Into+Light" rel="stylesheet" type="text/css">

		<!-- Vendor CSS -->
		<link rel="stylesheet" href="../assets/vendor/bootstrap/css/bootstrap.css" />
		
				<link rel="stylesheet" href="../assets/vendor/font-awesome/css/font-awesome.css" />
				<link rel="stylesheet" href="../assets/vendor/magnific-popup/magnific-popup.css" />
				<link rel="stylesheet" href="../assets/vendor/bootstrap-datepicker/css/datepicker3.css" />
		
				<!-- Specific Page Vendor CSS -->
				<link rel="stylesheet" href="../assets/vendor/jquery-ui/css/ui-lightness/jquery-ui-1.10.4.custom.css" />
				<link rel="stylesheet" href="../assets/vendor/bootstrap-multiselect/bootstrap-multiselect.css" />
				<link rel="stylesheet" href="../assets/vendor/morris/morris.css" />

				
		
				<!-- Theme CSS -->
				<link rel="stylesheet" href="../assets/stylesheets/theme.css" />
		
				<!-- Skin CSS -->
				<link rel="stylesheet" href="../assets/stylesheets/skins/default.css" />
		
				<!-- Theme Custom CSS -->
				<link rel="stylesheet" href="../assets/stylesheets/theme-custom.css">
		
				<!-- Head Libs -->
				<script src="../assets/vendor/modernizr/modernizr.js"></script>

    <!-- fa-product icon -->
    <link href="http://netdna.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.css" rel="stylesheet" />

		<!-- search -->
		<!-- <script type="text/javascript" src="assets/ajax/search.js" ></script> -->
		
		<script type="text/javascript" src="../assets/ajax/jquery-1.8.0.min.js"></script>

		
		

	</head>
	<body>
		<section class="body">

			<!-- start: header -->
			<header class="header">
				<div class="logo-container">
					<a href="dashboard.php" class="logo">
						<img src="../assets/images/logo.png" height="48" alt="Navikra CRM" />
					</a>
					<div class="visible-xs toggle-sidebar-left" data-toggle-class="sidebar-left-opened" data-target="html" data-fire-event="sidebar-left-opened">
						<i class="fa fa-bars" aria-label="Toggle sidebar"></i>
					</div>
				</div>
			
				<!-- start: search & user box -->
				<div class="header-right">
			
				<?php include "notification.php"; ?>
                    
                    <?php
                        $ad=mysqli_query($dbc,"select * from team where email='$id'");
                        while($r=mysqli_fetch_assoc($ad))
                        {
                            $name=$r['name'];
                            $role=$r['desig'];
                        }
                    ?>
			
					<span class="separator"></span>
			
					<div id="userbox" class="userbox">
						<a href="#" data-toggle="dropdown">
							<figure class="profile-picture">
								<img src="../assets/images/!logged-user.jpg" alt="Joseph Doe" class="img-circle" data-lock-picture="assets/images/!logged-user.jpg" />
							</figure>
							<div class="profile-info" data-lock-name="John Doe" data-lock-email="<?php echo $id; ?>">
								<span class="name"><?php echo $name; $_SESSION['n']=$name; ?></span>
								<span class="role"><?php echo $role; ?></span>
							</div>
			
							<i class="fa custom-caret"></i>
						</a>
			
						<div class="dropdown-menu">
							<ul class="list-unstyled">
								<li class="divider"></li>
								<li>
									<a role="menuitem" tabindex="-1" href="profile.php"><i class="fa fa-user"></i> My Profile</a>
								</li>
								<li>
									<!-- <a role="menuitem" tabindex="-1" href="#" data-lock-screen="true"><i class="fa fa-lock"></i> Lock Screen</a> -->
								</li>
								<li>
									<a role="menuitem" tabindex="-1" href="../../index.php"><i class="fa fa-power-off"></i> Logout</a>
								</li>
							</ul>
						</div>
					</div>
				</div>
				<!-- end: search & user box -->
			</header>
			<!-- end: header -->

			

			<div class="inner-wrapper">
				<!-- start: sidebar -->
				<aside id="sidebar-left" class="sidebar-left">
				
					<div class="sidebar-header">
						<div class="sidebar-title">
							Navigation
						</div>
						<div class="sidebar-toggle hidden-xs" data-toggle-class="sidebar-left-collapsed" data-target="html" data-fire-event="sidebar-left-toggle">
							<i class="fa fa-bars" aria-label="Toggle sidebar"></i>
						</div>
					</div>
				
					<div class="nano">
						<div class="nano-content">
							<nav id="menu" class="nav-main" role="navigation">
								<ul class="nav nav-main">
									<li >
										<a href="dashboard.php">
											<i class="fa fa-home" aria-hidden="true"></i>
											<span>Dashboard</span>
										</a>
									</li>
									<!-- <li>
										<a href="mailbox-folder.html">
											<span class="pull-right label label-primary">182</span>
											<i class="fa fa-envelope" aria-hidden="true"></i>
											<span>Mailbox</span>
										</a>
                                    </li> -->
                                    <li class="nav-parent">
                                    <a >
                                    
                                       <i class="fa fa-users" aria-hidden="true"></i>
                                       <span>Prospects</span>
                                    </a>
                                    <ul class="nav nav-children">


                                            <li >
                                                <a href="custdash.php">
                                                 Dashboard
                                                </a>
                                        
                                            </li>
                                           <li >
                                                <a href="addcust.php">
                                                 Add Prospect
                                                </a>
                                        
                                           </li>
                                           <li >
                                                <a href="customerprofile.php">
                                                 Add Prospect Profile
                                                </a>
                                        
                                           </li>
                                           <li >
                                                <a href="campcust.php">
                                                 Campaign Prospects
                                                </a>
                                        
                                           </li>
                                    
                                          <!--  <li >
                                              <a href="targetset.php">
                                                 Set Target
                                              </a>
                                        
                                           </li> -->
                                    

                                    </ul>
                                 </li>
                                <li class="nav-parent">
                                    <a >
                                        <!-- <span class="pull-right label label-primary">182</span> -->
                                        <i class="fa fa-plus" aria-hidden="true"></i>
                                        <span>Campaign</span>
                                    </a>

                                    <ul class="nav nav-children">

                                        <li >
                                            
                                            <a href="create-campaign.php">
                                                Create 
                                            </a>
                                            
                                        </li>


                                        <li class="nav-parent" >
                                            <a >
                                                Direct 
                                            </a>
                                            <ul class="nav nav-children">
                                                <li>
                                                    <a href="dirdash.php">
                                                         Show
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="direct.php">
                                                         Insert
                                                    </a>
                                                </li>
                                            </ul>
                                        </li>
                                        
                                        <li class="nav-parent">
                                            <a >
                                                 Digital
                                            </a>
                                            <ul class="nav nav-children">
                                                <li>
                                                    <a href="digdash.php">
                                                         Show
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="digital.php">
                                                         Insert
                                                    </a>
                                                </li>
                                            </ul>
                                        </li>

                                        <li class="nav-parent">
                                            <a >
                                                 Telecalling
                                            </a>
                                            <ul class="nav nav-children">
                                                <li>
                                                    <a href="teldash.php">
                                                         Show
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="telecalling.php">
                                                         Insert
                                                    </a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="nav-parent">
                                            <a >
                                                 Reference
                                            </a>
                                            <ul class="nav nav-children">
                                                <li>
                                                    <a href="refdash.php">
                                                         Show
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="reference.php">
                                                         Insert
                                                    </a>
                                                </li>
                                            </ul>
                                        </li>

                                        
                                    </ul>

                                </li>

                                <li >
                           <a href="lead.php">
                              <!-- <span class="pull-right label label-primary">182</span> -->
                              <i class="fa fa-user" aria-hidden="true"></i>
                              <span>Lead</span>
                           </a>
                           
                        </li>


                                <li class="nav-parent">
                                    <a>
                                        <!-- <span class="pull-right label label-primary">182</span> -->
                                        <i class="fa fa-user" aria-hidden="true"></i>
                                        <span>Meeting</span>
                                    </a>

                                    <ul class="nav nav-children">


                                        <li >
                                            <a href="meetingdash.php">
                                                Dashboard 
                                            </a>
                                            
                                        </li>
                                        
                                        <!-- <li >
                                            <a href="meeting.php">
                                                 Update
                                            </a>
                                            
                                        </li> -->

                                    </ul>

                                </li>

									<li class="nav-parent">
										<a >
										
											<i class="fa fa-refresh" aria-hidden="true"></i>
											<span>Funnel</span>
										</a>
										<ul class="nav nav-children">


											<li >
												<a href="funneldash.php">
													Show 
												</a>
											
											</li>
										
											<li >
												<a href="funnel.php">
													 Update
												</a>
											
											</li>

										</ul>
									</li>

									<li class="nav-parent">
                                    <a >
                                        <!-- <span class="pull-right label label-primary">182</span> -->
                                        <i class="fa fa-shopping-cart" aria-hidden="true"></i>
                                        <span>Order</span>
                                    </a>

                                    <ul class="nav nav-children">

                                        


                                        <li class="nav-parent" >
                                            <a >
                                                Quotation 
                                            </a>
                                            <ul class="nav nav-children">
                                                <li>
                                                    <a href="quotdash.php">
                                                         Dashboard
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="quotation-upload.php">
                                                         Upload
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="quotgen.php">
                                                         Generate
                                                    </a>
                                                </li>
                                            </ul>
                                        </li>
                                        
                                        <li class="nav-parent">
                                            <a >
                                                 PO
                                            </a>
                                            <ul class="nav nav-children">
                                                <li>
                                                    <a href="podash.php">
                                                         Dashboard
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="po-upload.php">
                                                         Upload
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="pogen.php">
                                                         Generate
                                                    </a>
                                                </li>
                                            </ul>
                                        </li>

                                        <li class="nav-parent">
                                            <a >
                                                 Invoice
                                            </a>
                                            <ul class="nav nav-children">
                                               <li>
                                                    <a href="invdash.php">
                                                         Dashboard
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="invoice-upload.php">
                                                         Upload
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="invgen.php">
                                                         Generate
                                                    </a>
                                                </li>
                                                
                                            </ul>
                                        </li>
                                        

                                        
                                    </ul>

                                </li>
                                 

									
                                    <li class="nav-parent">
									<a >
										
										<i class="fa fa-check" aria-hidden="true"></i>
										<span>Target Vs Achievement</span>
									</a>
									<ul class="nav nav-children">
	
	
										<!-- <li >
											<a href="funneldash.php">
												View 
											</a>
											
										</li> -->
										<li >
											<a href="achievement.php">
												 Achievement
											</a>
											
										</li>
										
										<li >
											<a href="targetset.php">
												Set Target
											</a>
											
										</li>
										
	
									</ul>
								</li>
								<li class="nav-parent">
                                    <a >
                                        <!-- <span class="pull-right label label-primary">182</span> -->
                                        <i class="fa fa-users" aria-hidden="true"></i>
                                        <span>HR</span>
                                    </a>

                                    <ul class="nav nav-children">

                                        


                                        <li class="nav-parent" >
                                            <a >
                                                Employee 
                                            </a>
                                            <ul class="nav nav-children">
                                                <li>
                                                    <a href="emplist.php">
                                                         Employee List
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="addemp.php">
                                                         Add
                                                    </a>
                                                </li>
                                            </ul>
                                        </li>
                                        
                                        <li class="nav-parent">
                                            <a >
                                                 Holiday
                                            </a>
                                            <ul class="nav nav-children">
                                                <li>
                                                    <a href="holidaylist.php">
                                                         Holiday List
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="addholiday.php">
                                                         Add
                                                    </a>
                                                </li>
                                            </ul>
                                        </li>

                                        <li class="nav-parent">
                                            <a >
                                                 Leave
                                            </a>
                                            <ul class="nav nav-children">
                                                <li>
                                                    <a href="leavedash.php">
                                                         Applications
                                                    </a>
                                                </li>
                                                
                                            </ul>
                                        </li>
                                        

                                        
                                    </ul>

                                </li>

                                 <li class="nav-parent">
                                    <a >
                                    
                                       <i class="fa fa-shopping-bag" aria-hidden="true"></i>
                                       <span>Products</span>
                                    </a>
                                    <ul class="nav nav-children">


                                            <li >
                                                <a href="proddash.php">
                                                 Dashboard
                                                </a>
                                        
                                            </li>
                                           <li >
                                                <a href="addprod.php">
                                                 Add
                                                </a>
                                        
                                           </li>
                                    
                                          

                                    </ul>
                                 </li>

                                 <li class="nav-parent">
                                    <a >
                                    
                                       <i class="fa fa-cogs" aria-hidden="true"></i>
                                       <span>Change</span>
                                    </a>
                                    <ul class="nav nav-children">


                                            <li >
                                                <a href="changepros.php">
                                                 Prospects
                                                </a>
                                        
                                            </li>
                                           <li >
                                                <a href="changecamp.php">
                                                 Campaign
                                                </a>
                                        
                                           </li>
                                           <li >
                                                <a href="#">
                                                 Meeting
                                                </a>
                                        
                                           </li>
                                           <li >
                                                <a href="#">
                                                 Funnel
                                                </a>
                                        
                                           </li>
                                           <li >
                                                <a href="#">
                                                 Order
                                                </a>
                                        
                                           </li>
                                           <li >
                                                <a href="#">
                                                 Target
                                                </a>
                                        
                                           </li>
                                           <li >
                                                <a href="#">
                                                 HR
                                                </a>
                                        
                                           </li>
                                           <li >
                                                <a href="#">
                                                 Products
                                                </a>
                                        
                                           </li>

                                    
                                          

                                    </ul>
                                 </li>

                                 
									<li>
										<a href="#">
											
											<i class="fa fa-book" aria-hidden="true"></i>
											<span>Reports</span>
										</a>
                                    </li>

									
									
								</ul>
							</nav>
				
							
                           
							<!-- <hr class="separator" /> -->
				
							
						</div>
				
					</div>
				
				</aside>
				<!-- end: sidebar -->
				<section role="main" class="content-body">
					<header class="page-header">
						<h2>Insert Data</h2>
					
						<div class="right-wrapper pull-right">
							<ol class="breadcrumbs">
								<li>
									<a href="dashboard.php">
										<i class="fa fa-home"></i>
									</a>
								</li>
								<!-- <li><span>Forms</span></li> -->
								<li><span>Insert</span></li>
							</ol>
					
							<a class="sidebar-right-toggle" ><i class="fa fa-chevron-down"></i></a>
						</div>
					</header>

					<!-- start: page -->

					<!-- start: page -->
											
					<div class="row">
						<div class="col-xs-12">
							<section class="panel form-wizard" id="w4">
								<!-- <header class="panel-heading">
										<h2 class="panel-title">Upload Customers Detail</h2>
								</header>
								<div class="panel-body">
									<form action="../auth/customer/ucust.php" method="post" class="form-horizontal" novalidate="novalidate" enctype="multipart/form-data"  >


													<div class="form-group">
										
													</div>
									
													<div class="form-group">
														<label class="col-sm-2 control-label" for="w4-username">Upload</label>
														<div class="col-sm-9">
															<input type="file" class="form-control" name="file" id="w4-username" >
														</div>
													</div>
													
													<div class="form-group">
														<div class="col-sm-2"></div>
														<div class="col-sm-9">
															<div class="checkbox-custom">
																<input type="submit" name="upload_submit" value="Submit" class="btn btn-success pull-right" />
																
															</div>
														</div>
													</div>
													<div class="form-group">
														<div class="col-sm-9"></div>
														<div class="col-sm-9">
															<div class="checkbox-custom">
															<h5><font color="red">*</font> Upload Maximum 100 Row</h5>
																
															</div>
														</div>
													</div>
												
											</form>
											</div> -->
								
								
						

						
					<!-- end: page -->
				


					<!-- j -->
								<?php
								$profileidno=$_GET['u'];
                                    $cid=$_GET['v'];
										$fetdetail=mysqli_query($dbc,"select * from `customersprofile` where `id`='$profileidno' ");

								while($frow=mysqli_fetch_assoc($fetdetail))
												{
                                                    $ccid=$frow['id'];
                                                    $cpid=$frow['cid'];
													$company=$frow['Company'];
                                                    $noofserver=$frow['NoOfServer'];
                                                    $serveroem=$frow['ServerOEM'];
                                                    $serverage=$frow['ServerAge'];
                                                    $serverconfig=$frow['ServerConfig'];
                                                    $storagetype=$frow['StorageType'];
                                                    $storageoem=$frow['StorageOEM'];
                                                    $noofstoragebox=$frow['NoOfStorageBox'];
                                                    $storagedatasize=$frow['StorageDataSize'];
                                                    $osname=$frow['OSName'];
                                                    $osdetail=$frow['OSDetail'];
                                                    $osoem=$frow['OSOEM'];
                                                    $osversion=$frow['OSVersion'];
                                                    $virtualizationoem=$frow['VirtualizationOEM'];
                                                    $virtualizationversion=$frow['VirtualizationVersion'];
                                                    $virtualizationrenewaldate=$frow['VirtualizationRenewalDate'];
                                                    $backupdatasize=$frow['BackupDataSize'];
                                                    $backupoem=$frow['BackupOEM'];
                                                    $backupversion=$frow['BackupVersion'];
                                                    $backuprenewaldate=$frow['BackupRenewalDate'];
                                                    $cloudoem=$frow['CloudOEM'];
                                                    $cloudusecase=$frow['CloudUseCase'];
                                                    $cloudmonthlybilling=$frow['CloudMonthlyBilling'];
                                                    $cloudmanagedby=$frow['CloudManagedBy'];
                                                    $noofendpoints=$frow['NoOfEndpoints'];
                                                    $endpointoem=$frow['EndpointOEM'];
                                                    $endpointrenewaldate=$frow['EndpointRenewalDate'];
                                                    $firewallage=$frow['FirewallAge'];
                                                    $firewalloem=$frow['FirewallOEM'];
                                                    $dlprenewaldate=$frow['DLPRenewalDate'];
                                                    $dlpoem=$frow['DLPOEM'];
                                                    $mobilesecurity=$frow['MobileSecurity'];
                                                    $mobilesecurityoem=$frow['MobileSecurityOEM'];
                                                    $vaptstatus=$frow['VAPTStatus'];
                                                    $vaptplan=$frow['VAPTPlan'];
                                                    $usingremotedesktop=$frow['UsingRemoteDesktop'];
                                                    $remotedesktopoem=$frow['RemoteDesktopOEM'];
                                                    $usinghelpdesk=$frow['UsingHelpDesk'];
                                                    $helpdeskoem=$frow['HelpDeskOEM'];
                                                    $usingerp=$frow['UsingERP'];
                                                    $erpoem=$frow['ERPOEM'];
                                                    $usingcrm=$frow['UsingCRM'];
                                                    $crmoem=$frow['CRMOEM'];
                                                    $usingautomation=$frow['UsingAutomation'];
                                                    $automationoem=$frow['AutomationOEM'];
                                                    $mailnoofusers=$frow['MailNoOfUsers'];
                                                    $mailoem=$frow['MailOEM'];
                                                    $mailrenewaldate=$frow['MailRenewalDate'];
                                                    $usingad=$frow['UsingAD'];
                                                    $adoem=$frow['ADOEM'];
                                                    $usinganalyticstool=$frow['UsingAnalyticsTool'];
                                                    $analyticstooloem=$frow['AnalyticsToolOEM'];
                                                    $iso27001status=$frow['ISO27001Status'];
                                                    $iso27001plan=$frow['ISO27001Plan'];
                                                    $itilstatus=$frow['ITILStatus'];
                                                    $itilplan=$frow['ITILPlan'];



														
														
												}

												?>
											
					
								<header class="panel-heading">
										<h2 class="panel-title">Input Customer Details</h2>
									</header>
								<div class="panel-body">
									<div class="wizard-progress wizard-progress-lg">
										<div class="steps-progress">
											<div class="progress-indicator"></div>
										</div>
										<ul class="wizard-steps">
											<li class="active">
												<a href="#w4-infra1" data-toggle="tab"><span>1</span>Infrastructure</a>
											</li>
											<li>
												<a href="#w4-infra2" data-toggle="tab"><span>2</span>Infrastructure</a>
											</li>
											<!-- <li>
												<a href="#w4-billing" data-toggle="tab"><span>3</span>Billing Info</a>
											</li> -->
											<li>
												<a href="#w4-confirm" data-toggle="tab"><span>3</span>Compliance</a>
											</li>
										</ul>
									</div>
					
									<form action="../auth/customer/editcustprofile.php" method="post" class="form-horizontal" novalidate="novalidate" enctype="multipart/form-data" >
										<div class="tab-content">
											<div id="w4-infra1" class="tab-pane active">

											

												<input type="hidden" name="ccid" value="<?php echo $ccid; ?>">
												<input type="hidden" name="cid" value="<?php echo $cid; ?>">

												<div class="form-group">
													<label class="col-sm-2 control-label" for="w4-username">Company</label>
													<div class="col-sm-4">
														
                                                           
                                                            <input type="hidden" name="company" value="<?php echo $company; ?>">
                                                            <input type="text" value="<?php echo $company; ?>" class="form-control" disabled>
													</div>
												</div>

												<div class="form-group">
													<label class="col-sm-12 control-label" for="w4-password" style="font-weight: bold;text-align: center;color: black">SERVER</label>
													
												</div>

												

												<div class="form-group">
													<label class="col-sm-2 control-label" for="w4-password">No of Server</label>
													<div class="col-sm-4">
														<input type="text" class="form-control" name="noofserver" value="<?php echo $noofserver; ?>" id="w4-password" >
													</div>
												<!-- </div>

												<div class="form-group"> -->
													<label class="col-sm-1 control-label" for="w4-username">OEM</label>
													<div class="col-sm-4">
														<input type="text" class="form-control" name="serveroem" value="<?php echo $serveroem; ?>" id="w4-username" >
													</div>
												</div>
												

												<div class="form-group">
													<label class="col-sm-2 control-label" for="w4-username">Age</label>
													<div class="col-sm-4">
														<input type="text" class="form-control" name="serverage" value="<?php echo $serverage; ?>" id="w4-username" >
													</div>
												<!-- </div>
												<div class="form-group"> -->
													<label class="col-sm-1 control-label" for="w4-password">Configuration</label>
													<div class="col-sm-4">
														<input type="text" class="form-control" name="serverconfig" value="<?php echo $serverconfig; ?>" id="w4-password" >
													</div>
												</div>

												<div class="form-group">
													<label class="col-sm-12 control-label" for="w4-password" style="font-weight: bold;text-align: center;color: black">STORAGE</label>
													
												</div>

												<div class="form-group">
													<label class="col-sm-2 control-label" for="w4-username">Storage Type</label>
													<div class="col-sm-4">
														<select class="form-control" name="storagetype" data-plugin-multiselect data-plugin-options='{ "enableCaseInsensitiveFiltering": true }' id="ms_example6"  >
															<option value="<?php echo $storagetype; ?>"><?php echo $storagetype; ?></option>
															<option value="SAN">SAN</option>
															<option value="NAS">NAS</option>
															<option value="Tape">Tape</option>
															<option value="HardDisk">Hard Disk</option>
															
														
														</select>
													</div>
												<!-- </div>

												<div class="form-group"> -->
													<label class="col-sm-1 control-label" for="w4-username">OEM</label>
													<div class="col-sm-4">
														<input type="text" class="form-control" name="storageoem" value="<?php echo $storageoem; ?>" id="w4-username" >
													</div>
												</div>
												

												<div class="form-group">
													<label class="col-sm-2 control-label" for="w4-username">No of Storage Box</label>
													<div class="col-sm-4">
														<input type="text" class="form-control" name="noofstoragebox" value="<?php echo $noofstoragebox; ?>"  id="w4-pin"  onKeyUp="pins()" >
													</div>
												<!-- </div>
												<div class="form-group"> -->
													<label class="col-sm-1 control-label" for="w4-password">Data Size</label>
													<div class="col-sm-4">
														<input type="text" class="form-control" name="storagedatasize" value="<?php echo $storagedatasize; ?>" id="w4-password" >
													</div>
												</div>

												<div class="form-group">
													<label class="col-sm-12 control-label" for="w4-password" style="font-weight: bold;text-align: center;color: black">OS</label>
													
												</div>

												<div class="form-group">
													<label class="col-sm-2 control-label" for="w4-username">OS Name</label>
													<div class="col-sm-4">
														<select class="form-control" name="osname" data-plugin-multiselect data-plugin-options='{ "enableCaseInsensitiveFiltering": true }' id="ms_example6"  >
															
															<option value="<?php echo $osname; ?>"><?php echo $osname; ?></option>
															<option value="Windows">Windows</option>
															<option value="Open Source">Open Source</option>
															
															
														
														</select>
													</div>
												<!-- </div>

												<div class="form-group"> -->
													<label class="col-sm-1 control-label" for="w4-username">OS Detail</label>
													<div class="col-sm-4">
														<input type="text" class="form-control" name="osdetail" value="<?php echo $osdetail; ?>" id="w4-username" >
													</div>
												</div>

												<div class="form-group">
													<label class="col-sm-2 control-label" for="w4-username">OEM</label>
													<div class="col-sm-4">
														<input type="text" class="form-control" name="osoem" value="<?php echo $osoem; ?>" id="w4-std"  >
													</div>
												<!-- </div>
												<div class="form-group"> -->
													<label class="col-sm-1 control-label" for="w4-password">OS Version</label>
													<div class="col-sm-4">
														<input type="text" class="form-control" name="osversion" value="<?php echo $osversion; ?>" id="w4-land" onKeyUp="landl()" >
													</div>
												</div>

												<div class="form-group">
													<label class="col-sm-12 control-label" for="w4-password" style="font-weight: bold;text-align: center;color: black">VIRTUALIZATION</label>
													
												</div>

												<div class="form-group">
													<label class="col-sm-2 control-label" for="w4-username">OEM</label>
													<div class="col-sm-4">
														<input type="text" class="form-control" name="virtualizationoem" value="<?php echo $virtualizationoem; ?>" id="w4-username" >
													</div>
												<!-- </div>
												<div class="form-group"> -->
													<label class="col-sm-1 control-label" for="w4-username">Version</label>
													<div class="col-sm-4">
														<input type="text" class="form-control" name="virtualizationversion" value="<?php echo $virtualizationversion; ?>" id="w4-username">
													</div>
												</div>

												<div class="form-group"> 
													<label class="col-sm-2 control-label" for="w4-username">Renewal Date</label>
													<div class="col-sm-4">
														<div class="input-group">
																<span class="input-group-addon">
																	<i class="fa fa-calendar"></i>
																</span>
																<input type="text" name="virtualizationrenewaldate" value="<?php echo $virtualizationrenewaldate; ?>" data-plugin-datepicker class="form-control" >
														</div>
													</div>

													
												</div>

												<div class="form-group">
													<label class="col-sm-12 control-label" for="w4-password" style="font-weight: bold;text-align: center;color: black">BACKUP</label>
													
												</div>

												<div class="form-group">
													<label class="col-sm-2 control-label" for="w4-username">Data Size</label>
													<div class="col-sm-4">
														<input type="text" class="form-control" name="backupdatasize" value="<?php echo $backupdatasize; ?>" id="w4-username" >
													</div>
												<!-- </div>
												<div class="form-group"> -->
													<label class="col-sm-1 control-label" for="w4-username">OEM</label>
													<div class="col-sm-4">
														<input type="text" class="form-control" name="backupoem" value="<?php echo $backupoem; ?>" id="w4-username">
													</div>
												</div>

												<div class="form-group"> 
													<label class="col-sm-2 control-label" for="w4-username">Version</label>
													<div class="col-sm-4">
														<input type="text" class="form-control" name="backupversion" value="<?php echo $backupversion; ?>" id="w4-password" >
													</div>
													<!-- </div>
													<div class="form-group"> -->
													<label class="col-sm-1 control-label" for="w4-username">Renewal Date</label>
													<div class="col-sm-4">
														<div class="input-group">
																<span class="input-group-addon">
																	<i class="fa fa-calendar"></i>
																</span>
																<input type="text" name="backuprenewaldate" value="<?php echo $backuprenewaldate; ?>" data-plugin-datepicker class="form-control" >
														</div>
													</div>

													
												</div>

												<div class="form-group">
													<label class="col-sm-12 control-label" for="w4-password" style="font-weight: bold;text-align: center;color: black">CLOUD</label>
													
												</div>

												<div class="form-group">
													<label class="col-sm-2 control-label" for="w4-username">OEM</label>
													<div class="col-sm-4">
														<input type="text" class="form-control" name="cloudoem" value="<?php echo $cloudoem; ?>" id="w4-username" >
													</div>
												<!-- </div>
												<div class="form-group"> -->
													<label class="col-sm-1 control-label" for="w4-username">Use Case</label>
													<div class="col-sm-4">
														<input type="text" class="form-control" name="cloudusecase" value="<?php echo $cloudusecase; ?>" id="w4-username">
													</div>
												</div>

												<div class="form-group"> 
													<label class="col-sm-2 control-label" for="w4-username">Monthly Billing</label>
													<div class="col-sm-4">
														<input type="text" class="form-control" name="cloudmonthlybilling" value="<?php echo $cloudmonthlybilling; ?>" id="w4-password" >
													</div>
													<!-- </div>
													<div class="form-group"> -->
													<label class="col-sm-1 control-label" for="w4-username">Managed By</label>
													<div class="col-sm-4">
														<input type="text" class="form-control" name="cloudmanagedby" value="<?php echo $cloudmanagedby; ?>" id="w4-username">
													</div>

													
												</div>

												<div class="form-group">
													<label class="col-sm-12 control-label" for="w4-password" style="font-weight: bold;text-align: center;color: black">ENDPOINT MANAGEMENT</label>
													
												</div>

												<div class="form-group">
													<label class="col-sm-2 control-label" for="w4-username">No of Endpoints</label>
													<div class="col-sm-4">
														<input type="text" class="form-control" name="noofendpoints" value="<?php echo $noofendpoints; ?>" id="w4-username" >
													</div>
												<!-- </div>
												<div class="form-group"> -->
													<label class="col-sm-1 control-label" for="w4-username">OEM</label>
													<div class="col-sm-4">
														<input type="text" class="form-control" name="endpointoem" value="<?php echo $endpointoem; ?>" id="w4-username">
													</div>
												</div>

												<div class="form-group"> 
													<label class="col-sm-2 control-label" for="w4-username">Renewal Date</label>
													<div class="col-sm-4">
														<div class="input-group">
																<span class="input-group-addon">
																	<i class="fa fa-calendar"></i>
																</span>
																<input type="text" name="endpointrenewaldate" value="<?php echo $endpointrenewaldate; ?>" data-plugin-datepicker class="form-control" >
														</div>
													</div>
													
												</div>

												<div class="form-group">
													<label class="col-sm-12 control-label" for="w4-password" style="font-weight: bold;text-align: center;color: black">FIREWALL</label>
													
												</div>

												<div class="form-group">
													<label class="col-sm-2 control-label" for="w4-username">Age</label>
													<div class="col-sm-4">
														<input type="text" class="form-control" name="firewallage" value="<?php echo $firewallage; ?>" id="w4-username" >
													</div>
												<!-- </div>
												<div class="form-group"> -->
													<label class="col-sm-1 control-label" for="w4-username">OEM</label>
													<div class="col-sm-4">
														<input type="text" class="form-control" name="firewalloem" value="<?php echo $firewalloem; ?>" id="w4-username">
													</div>
												</div>

												<div class="form-group">
													<label class="col-sm-12 control-label" for="w4-password" style="font-weight: bold;text-align: center;color: black">DLP</label>
													
												</div>

												<div class="form-group">
													<label class="col-sm-2 control-label" for="w4-username">Renewal Date</label>
													<div class="col-sm-4">
														<div class="input-group">
																<span class="input-group-addon">
																	<i class="fa fa-calendar"></i>
																</span>
																<input type="text" name="dlprenewaldate" value="<?php echo $dlprenewaldate; ?>" data-plugin-datepicker class="form-control" >
														</div>
													</div>
												<!-- </div>
												<div class="form-group"> -->
													<label class="col-sm-1 control-label" for="w4-username">OEM</label>
													<div class="col-sm-4">
														<input type="text" class="form-control" name="dlpoem" value="<?php echo $dlpoem; ?>" id="w4-username">
													</div>
												</div>

												<div class="form-group">
													<label class="col-sm-12 control-label" for="w4-password" style="font-weight: bold;text-align: center;color: black">MOBILE SECURITY</label>
													
												</div>

												<div class="form-group">
													<label class="col-sm-2 control-label" for="w4-username">Using Security</label>
													<div class="col-sm-4">
														<select class="form-control" name="mobilesecurity" data-plugin-multiselect data-plugin-options='{ "enableCaseInsensitiveFiltering": true }' id="ms_example6"  >
															
															<option value="<?php echo $mobilesecurity; ?>"><?php echo $mobilesecurity; ?></option>
															<option value="Yes">Yes</option>
															<option value="No">No</option>
															
															
														</select>
													</div>
												<!-- </div>
												<div class="form-group"> -->
													<label class="col-sm-1 control-label" for="w4-username">OEM</label>
													<div class="col-sm-4">
														<input type="text" class="form-control" name="mobilesecurityoem" value="<?php echo $mobilesecurityoem; ?>" id="w4-username">
													</div>
												</div>



											</div>


											<div id="w4-infra2" class="tab-pane">
												<div class="form-group">
													<label class="col-sm-12 control-label" for="w4-password" style="font-weight: bold;text-align: center;color: black">VAPT</label>
													
												</div>

												<div class="form-group">
													<label class="col-sm-2 control-label" for="w4-username">Status</label>
													<div class="col-sm-4">
														<select class="form-control" name="vaptstatus"  data-plugin-multiselect data-plugin-options='{ "enableCaseInsensitiveFiltering": true }' id="ms_example6"  >
															<option value="<?php echo $vaptstatus; ?>"><?php echo $vaptstatus; ?></option>
															<option value="Done">Done</option>
															<option value="Not Done">Not Done</option>
															
															
														</select>
													</div>
													<!-- </div>
												<div class="form-group"> -->
													<label class="col-sm-1 control-label" for="w4-username">Plan</label>
													<div class="col-sm-4">
														<input type="text" class="form-control" name="vaptplan" value="<?php echo $vaptplan; ?>" id="w4-username">
													</div>
												
												</div>

												<div class="form-group">
													<label class="col-sm-12 control-label" for="w4-password" style="font-weight: bold;text-align: center;color: black">REMOTE DESKTOP</label>
													
												</div>

												<div class="form-group">
													<label class="col-sm-2 control-label" for="w4-username">Using Remote Desktop</label>
													<div class="col-sm-4">
														<select class="form-control" name="usingremotedesktop" data-plugin-multiselect data-plugin-options='{ "enableCaseInsensitiveFiltering": true }' id="ms_example6"  >
															
															<option value="<?php echo $usingremotedesktop; ?>"><?php echo $usingremotedesktop; ?></option>
															<option value="Yes">Yes</option>
															<option value="No">No</option>
															
															
														</select>
													</div>
												<!-- </div>
												<div class="form-group"> -->
													<label class="col-sm-1 control-label" for="w4-username">OEM</label>
													<div class="col-sm-4">
														<input type="text" class="form-control" name="remotedesktopoem" value="<?php echo $remotedesktopoem; ?>" id="w4-username">
													</div>
												</div>

												<div class="form-group">
													<label class="col-sm-12 control-label" for="w4-password" style="font-weight: bold;text-align: center;color: black">HELP DESK</label>
													
												</div>

												<div class="form-group">
													<label class="col-sm-2 control-label" for="w4-username">Using Help Desk</label>
													<div class="col-sm-4">
														<select class="form-control" name="usinghelpdesk" data-plugin-multiselect data-plugin-options='{ "enableCaseInsensitiveFiltering": true }' id="ms_example6"  >
															
															<option value="<?php echo $usinghelpdesk; ?>"><?php echo $usinghelpdesk; ?></option>
															<option value="Yes">Yes</option>
															<option value="No">No</option>
															
															
														</select>
													</div>
												<!-- </div>
												<div class="form-group"> -->
													<label class="col-sm-1 control-label" for="w4-username">OEM</label>
													<div class="col-sm-4">
														<input type="text" class="form-control" name="helpdeskoem" value="<?php echo $helpdeskoem; ?>" id="w4-username">
													</div>
												</div>

												<div class="form-group">
													<label class="col-sm-12 control-label" for="w4-password" style="font-weight: bold;text-align: center;color: black">ERP</label>
													
												</div>

												<div class="form-group">
													<label class="col-sm-2 control-label" for="w4-username">Using ERP</label>
													<div class="col-sm-4">
														<select class="form-control" name="usingerp" data-plugin-multiselect data-plugin-options='{ "enableCaseInsensitiveFiltering": true }' id="ms_example6"  >
															
															<option value="<?php echo $usingerp; ?>"><?php echo $usingerp; ?></option>
															<option value="Yes">Yes</option>
															<option value="No">No</option>
															
															
														</select>
													</div>
												<!-- </div>
												<div class="form-group"> -->
													<label class="col-sm-1 control-label" for="w4-username">OEM</label>
													<div class="col-sm-4">
														<input type="text" class="form-control" name="erpoem" value="<?php echo $erpoem; ?>" id="w4-username">
													</div>
												</div>

												<div class="form-group">
													<label class="col-sm-12 control-label" for="w4-password" style="font-weight: bold;text-align: center;color: black">CRM</label>
													
												</div>

												<div class="form-group">
													<label class="col-sm-2 control-label" for="w4-username">Using CRM</label>
													<div class="col-sm-4">
														<select class="form-control" name="usingcrm" data-plugin-multiselect data-plugin-options='{ "enableCaseInsensitiveFiltering": true }' id="ms_example6"  >
															
															<option value="<?php echo $usingcrm; ?>"><?php echo $usingcrm; ?></option>
															<option value="Yes">Yes</option>
															<option value="No">No</option>
															
															
														</select>
													</div>
												<!-- </div>
												<div class="form-group"> -->
													<label class="col-sm-1 control-label" for="w4-username">OEM</label>
													<div class="col-sm-4">
														<input type="text" class="form-control" name="crmoem" value="<?php echo $crmoem; ?>" id="w4-username">
													</div>
												</div>

												<div class="form-group">
													<label class="col-sm-12 control-label" for="w4-password" style="font-weight: bold;text-align: center;color: black">AUTOMATION</label>
													
												</div>

												<div class="form-group">
													<label class="col-sm-2 control-label" for="w4-username">Using Automation</label>
													<div class="col-sm-4">
														<select class="form-control" name="usingautomation" data-plugin-multiselect data-plugin-options='{ "enableCaseInsensitiveFiltering": true }' id="ms_example6"  >
															
															<option value="<?php echo $usingautomation; ?>"><?php echo $usingautomation; ?></option>
															<option value="Yes">Yes</option>
															<option value="No">No</option>
															
															
														</select>
													</div>
												<!-- </div>
												<div class="form-group"> -->
													<label class="col-sm-1 control-label" for="w4-username">OEM</label>
													<div class="col-sm-4">
														<input type="text" class="form-control" name="automationoem" value="<?php echo $automationoem; ?>" id="w4-username">
													</div>
												</div>

												<div class="form-group">
													<label class="col-sm-12 control-label" for="w4-password" style="font-weight: bold;text-align: center;color: black">MAIL</label>
													
												</div>

												<div class="form-group">
													<label class="col-sm-2 control-label" for="w4-username">No of Users</label>
													<div class="col-sm-4">
														<input type="text" class="form-control" name="mailnoofusers" value="<?php echo $mailnoofusers; ?>" id="w4-username">
													</div>
												<!-- </div>
												<div class="form-group"> -->
													<label class="col-sm-1 control-label" for="w4-username">OEM</label>
													<div class="col-sm-4">
														<input type="text" class="form-control" name="mailoem" value="<?php echo $mailoem; ?>" id="w4-username">
													</div>
												</div>

												<div class="form-group">
													<label class="col-sm-2 control-label" for="w4-username">Renewal Date</label>
													<div class="col-sm-4">
														<div class="input-group">
																<span class="input-group-addon">
																	<i class="fa fa-calendar"></i>
																</span>
																<input type="text" name="mailrenewaldate" value="<?php echo $mailrenewaldate; ?>" data-plugin-datepicker class="form-control" >
														</div>
													</div>
												
												</div>

												<div class="form-group">
													<label class="col-sm-12 control-label" for="w4-password" style="font-weight: bold;text-align: center;color: black">AD</label>
													
												</div>

												<div class="form-group">
													<label class="col-sm-2 control-label" for="w4-username">Using AD</label>
													<div class="col-sm-4">
														<select class="form-control" name="usingad" data-plugin-multiselect data-plugin-options='{ "enableCaseInsensitiveFiltering": true }' id="ms_example6"  >
															
															<option selected value="<?php echo $usingad; ?>"><?php echo $usingad; ?></option>
															<option value="Yes">Yes</option>
															<option value="No">No</option>
															
															
														</select>
													</div>
												<!-- </div>
												<div class="form-group"> -->
													<label class="col-sm-1 control-label" for="w4-username">OEM</label>
													<div class="col-sm-4">
														<input type="text" class="form-control" name="adoem" value="<?php echo $adoem; ?>" id="w4-username">
													</div>
												</div>

												<div class="form-group">
													<label class="col-sm-12 control-label" for="w4-password" style="font-weight: bold;text-align: center;color: black">ANALYTICS TOOL</label>
													
												</div>

												<div class="form-group">
													<label class="col-sm-2 control-label" for="w4-username">Using Tool</label>
													<div class="col-sm-4">
														<select class="form-control" name="usinganalyticstool" data-plugin-multiselect data-plugin-options='{ "enableCaseInsensitiveFiltering": true }' id="ms_example6"  >
															
															<option value="<?php echo $usinganalyticstool; ?>"><?php echo $usinganalyticstool; ?></option>
															<option value="Yes">Yes</option>
															<option value="No">No</option>
															
															
														</select>
													</div>
												<!-- </div>
												<div class="form-group"> -->
													<label class="col-sm-1 control-label" for="w4-username">OEM</label>
													<div class="col-sm-4">
														<input type="text" class="form-control" name="analyticstooloem" value="<?php echo $analyticstooloem; ?>" id="w4-username">
													</div>
												</div>


											</div>
											
											<div id="w4-confirm" class="tab-pane">

												<div class="form-group">
													<label class="col-sm-12 control-label" for="w4-password" style="font-weight: bold;text-align: center;color: black">ISO 27001</label>
													
												</div>
												
												<div class="form-group">
													<label class="col-sm-2 control-label" for="w4-username">Status</label>
													<div class="col-sm-4">
														<select class="form-control" name="iso27001status" data-plugin-multiselect data-plugin-options='{ "enableCaseInsensitiveFiltering": true }' id="ms_example6"  >
															
															<option selected value="<?php echo $iso27001status; ?>"><?php echo $iso27001status ?></option>
															<option value="Yes">Yes</option>
															<option value="No">No</option>
															
															
														</select>
													</div>
												<!-- </div>
												<div class="form-group"> -->
													<label class="col-sm-1 control-label" for="w4-username">Plan</label>
													<div class="col-sm-4">
														<input type="text" class="form-control" name="iso27001plan" value="<?php echo $iso27001plan; ?>" id="w4-username">
													</div>
												</div>

												<div class="form-group">
													<label class="col-sm-12 control-label" for="w4-password" style="font-weight: bold;text-align: center;color: black">ITIL</label>
													
												</div>
												
												<div class="form-group">
													<label class="col-sm-2 control-label" for="w4-username">Status</label>
													<div class="col-sm-4">
														<select class="form-control" name="itilstatus" data-plugin-multiselect data-plugin-options='{ "enableCaseInsensitiveFiltering": true }' id="ms_example6"  >
															
															<option selected value="<?php echo $itilstatus; ?>"><?php echo $itilstatus; ?></option>
															<option value="Yes">Yes</option>
															<option value="No">No</option>
															
															
														</select>
													</div>
												<!-- </div>
												<div class="form-group"> -->
													<label class="col-sm-1 control-label" for="w4-username">Plan</label>
													<div class="col-sm-4">
														<input type="text" class="form-control" name="itilplan" value="<?php echo $itilplan; ?>" id="w4-username">
													</div>
												</div>

												<!-- <div class="form-group">
													<div class="col-sm-2"></div>
													<div class="col-sm-9">
														<div class="checkbox-custom">
															<input type="checkbox" name="terms" id="w4-terms" required>
															<label for="w4-terms">I agree to the terms of service</label>
															
														</div>
													</div>
												</div> -->
												<div class="form-group">
													<div class="col-sm-2"></div>
													<div class="col-sm-9">
														<div class="checkbox-custom">
														<input type="submit" name="submit" value="Submit" class="btn btn-success pull-right" />
															
														</div>
													</div>
												</div>
											</div>
										</div>
										
									
								</div>
								<div class="panel-footer">
									<ul class="pager">
										<li class="previous disabled">
											<a><i class="fa fa-angle-left"></i> Previous</a>
										</li>
										<li class="finish hidden pull-right">
											<!-- <a>Finish</a> -->
											<!-- <input type="submit" name="submit" value="Submit" class="btn btn-success" /> -->
											</form>
										</li>
										<li class="next">
											<a>Next <i class="fa fa-angle-right"></i></a>
										</li>
									</ul>
									
								</div>
							</section>
						</div>
					</div>

						
					<!-- end: page -->
				</section>
			</div>

			<aside id="sidebar-right" class="sidebar-right">
                <div class="nano">
                    <div class="nano-content">
                        <a href="#" class="mobile-close visible-xs">
                            Collapse <i class="fa fa-chevron-right"></i>
                        </a>
            
                        <div class="sidebar-right-wrapper">
            
                            <div class="sidebar-widget widget-calendar">
                                <h6>Upcoming Tasks</h6>
                                <div data-plugin-datepicker data-plugin-skin="dark" ></div>
            
                                <ul>
                                    <li>
                                        <!-- <time datetime="2014-04-19T00:00+00:00">04/19/2014</time> -->
                                        <span>Company</span>
                                    </li>
                                </ul>
                            </div>
            
                            <div class="sidebar-widget widget-friends">
                                <h6 style="color: red">Meeting</h6>
                                <ul>

                                    <?php
                                            $fetfunnel=mysqli_query($dbc,"select * from `meeting` where  `FollowUp`='Yes' order by FollowUpDate desc");

                                           
                                                    while($frow=mysqli_fetch_assoc($fetfunnel))
                                                    {
                                                        $company=$frow['Company'];
                                                        $followupdate=$frow['FollowUpDate'];
                                                        $followuptime=$frow['FollowUpTime'];
                                                        $project=$frow['ProjectName'];
                                                        $requirement=$frow['Requirement'];
                                                        $remark=$frow['Remarks'];
                                                        $infrastructure=$frow['Infrastructure'];
                                                        $contactperson=$frow['ContactPerson'];
                                                        $designation=$frow['Designation'];
                                                        $mobile=$frow['Mobile'];
                                                        $mail=$frow['Mail'];
                                                        


                                                        $today=date("Y-m-d");
                                                        $cdate=date('h:i a', strtotime($followuptime));

                                                        



                                                        if($followupdate == $today)
                                                        {

                                                            echo "<li class=status-online>
                                        
                                        <div class=profile-info>
                                            <span class=name>{$frow['Company']}</span>
                                            <span class=title>Contact Person: {$frow['ContactPerson']}</span>
                                            <span class=title>Mobile: <a href=tel:{$frow['Mobile']}>{$frow['Mobile']}</a></span>
                                            <span class=title>Meeting Time: $cdate</span>
                                        </div>
                                    </li>";
   
                                                        } else
                                                        {
                                                            /*echo '<tr>';
                                                            echo "<td>Sorry There is no FollowUp Today</td>";
                                                            
                                                            echo '</tr>';*/
                                                        } 
                                                        

                                                    }
                                                    
                                                    
                                               
                                        ?>
                                    
                                </ul>
                            </div>




                            <div class="sidebar-widget widget-friends">
                                <h6 style="color: red">Funnel Follow Up</h6>
                                <ul>

                                    <?php
                                            $fetfunnel=mysqli_query($dbc,"select * from `funnel` where  MONTH(ModificationDetail) = MONTH(CURRENT_DATE()) AND YEAR(ModificationDetail) = YEAR(CURRENT_DATE()) ");

                                           
                                                    while($frow=mysqli_fetch_assoc($fetfunnel))
                                                {
                                                    global $totalrevenue;
                                                    global $totalmargin;

                                                    $company=$frow['Company'];
                                                    $product=$frow['Products'];
                                                    $detail=$frow['Detail'];
                                                    $revenue=$frow['Revenue'];
                                                    $margin=$frow['Margin'];
                                                    $stage=$frow['Stage'];
                                                    $probability=$frow['Probability'];
                                                    $dateofclosure=$frow['DateOfClosure'];
                                                    $closuremonth=$frow['ExpectedClosure'];
                                                    $closuredate=$frow['ModificationDetail'];
                                                    
                                                    $closuredate_final=date("Y-m-d", strtotime($closuredate));
                                                        


                                                        $today=date("Y-m-d");
                                                        $cdate=date('h:i a', strtotime($followuptime));

                                                        



                                                        if($dateofclosure == $today)
                                                        {

                                                            echo "<li class=status-online>
                                        
                                        <div class=profile-info>
                                            <span class=name>{$frow['Company']}</span>
                                            <span class=title>Product: {$frow['Products']}</span>
                                            
                                        </div>
                                    </li>";
   
                                                        } else
                                                        {
                                                            /*echo '<tr>';
                                                            echo "<td>Sorry There is no FollowUp Today</td>";
                                                            
                                                            echo '</tr>';*/
                                                        } 
                                                        

                                                    }
                                                    
                                                    
                                               
                                        ?>
                                   
                                </ul>
                            </div>

            
                        </div>
                    </div>
                </div>
            </aside>
		</section>

		<!-- Vendor -->
		<script src="../assets/vendor/jquery/jquery.js"></script>
		<script src="../assets/vendor/jquery-browser-mobile/jquery.browser.mobile.js"></script>
		<script src="../assets/vendor/bootstrap/js/bootstrap.js"></script>
		<script src="../assets/vendor/nanoscroller/nanoscroller.js"></script>
		<script src="../assets/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
		<script src="../assets/vendor/magnific-popup/magnific-popup.js"></script>
		<script src="../assets/vendor/jquery-placeholder/jquery.placeholder.js"></script>
		
		<!-- Specific Page Vendor -->
		<script src="../assets/vendor/jquery-validation/jquery.validate.js"></script>
		<script src="../assets/vendor/bootstrap-wizard/jquery.bootstrap.wizard.js"></script>
		<script src="../assets/vendor/pnotify/pnotify.custom.js"></script>

		<script src="../assets/vendor/jquery-ui/js/jquery-ui-1.10.4.custom.js"></script>
		<script src="../assets/vendor/jquery-ui-touch-punch/jquery.ui.touch-punch.js"></script>
		<script src="../assets/vendor/select2/select2.js"></script>
		<script src="../assets/vendor/bootstrap-multiselect/bootstrap-multiselect.js"></script>
		
		
		
	
		
		
		
		
		<script src="../assets/vendor/ios7-switch/ios7-switch.js"></script>
		
		
		<!-- Theme Base, Components and Settings -->
		<script src="../assets/javascripts/theme.js"></script>
		
		<!-- Theme Custom -->
		<script src="../assets/javascripts/theme.custom.js"></script>
		
		<!-- Theme Initialization Files -->
		<script src="../assets/javascripts/theme.init.js"></script>


		<!-- Examples -->
		<script src="../assets/javascripts/forms/examples.wizard.js"></script>
		<script src="../assets/javascripts/forms/examples.advanced.form.js"></script>

        <!-- modal form -->
      <script src="../assets/javascripts/ui-elements/examples.modals.js"></script>
		<script>
		function pins()
		{
			var x=/\D/ig
			var y=document.getElementById("w4-pin")
			var z=y.value.search(x)
			var a=/\W/ig
			var b=document.getElementById("w4-pin")
			var c=y.value.search(a)
			if(z!=-1)	
			{
				y.value=y.value.replace(/\D/g,'')
			}
			if(c!=-1)
			{
				y.value=y.value.replace(/\W/g,'')
			}

		}

		function stdc()
		{
			var x=/\D/ig
			var y=document.getElementById("w4-std")
			var z=y.value.search(x)
			var a=/\W/ig
			var b=document.getElementById("w4-std")
			var c=y.value.search(a)
			if(z!=-1)	
			{
				y.value=y.value.replace(/\D/g,'')
			}
			if(c!=-1)
			{
				y.value=y.value.replace(/\W/g,'')
			}

		}

		function mob()
		{
			var x=/\D/ig
			var y=document.getElementById("w4-mob")
			var z=y.value.search(x)
			var a=/\W/ig
			var b=document.getElementById("w4-mob")
			var c=y.value.search(a)
			if(z!=-1)	
			{
				y.value=y.value.replace(/\D/g,'')
			}
			if(c!=-1)
			{
				y.value=y.value.replace(/\W/g,'')
			}

		}

		function cn()
		{
			var x=/\D/ig
			var y=document.getElementById("w4-cn")
			var z=y.value.search(x)
			var a=/\W/ig
			var b=document.getElementById("w4-cn")
			var c=y.value.search(a)
			if(z!=-1)	
			{
				y.value=y.value.replace(/\D/g,'')
			}
			if(c!=-1)
			{
				y.value=y.value.replace(/\W/g,'')
			}

		}

		function landl()
		{
			var x=/\D/ig
			var y=document.getElementById("w4-land")
			var z=y.value.search(x)
			var a=/\W/ig
			var b=document.getElementById("w4-land")
			var c=y.value.search(a)
			if(z!=-1)	
			{
				y.value=y.value.replace(/\D/g,'')
			}
			if(c!=-1)
			{
				y.value=y.value.replace(/\W/g,'')
			}

		}

		function fname()
		{
			var x=/\d/ig
			var y=document.getElementById("w4-f")
			var z=y.value.search(x)
			var a=/\W/ig
			var b=document.getElementById("w4-f")
			var c=y.value.search(a)
			if(z!=-1)
			{
				y.value=y.value.replace(/\d/g,'')
			}
			if(c!=-1)
			{
				y.value=y.value.replace(/W/g,'')
			}
		}

		function lname()
		{
			var x=/\d/ig
			var y=document.getElementById("w4-l")
			var z=y.value.search(x)
			var a=/\W/ig
			var b=document.getElementById("w4-l")
			var c=y.value.search(a)
			if(z!=-1)
			{
				y.value=y.value.replace(/\d/g,'')
			}
			if(c!=-1)
			{
				y.value=y.value.replace(/W/g,'')
			}
		}

		function cpt()
		{
			var x=/\d/ig
			var y=document.getElementById("w4-cp")
			var z=y.value.search(x)
			var a=/\W/ig
			var b=document.getElementById("w4-cp")
			var c=y.value.search(a)
			if(z!=-1)
			{
				y.value=y.value.replace(/\d/g,'')
			}
			if(c!=-1)
			{
				y.value=y.value.replace(/W/g,'')
			}
		}
		</script>
	</body>
</html>
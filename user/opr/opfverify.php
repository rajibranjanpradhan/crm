<?php
include "session_handler.php";
?>
<!DOCTYPE html>
<html class="no-js css-menubar" lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="description" content="bootstrap material admin template">
    <meta name="author" content="">
    
    <title>Customers | Bizapp CRM</title>
    
    <link rel="apple-touch-icon" href="../../assets/images/apple-touch-icon.png">
    <link rel="shortcut icon" href="../../assets/images/favicon.ico">
    
    <!-- Stylesheets -->
    <link rel="stylesheet" href="../../assets/global/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../assets/global/css/bootstrap-extend.min.css">
    <link rel="stylesheet" href="../../assets/css/site.min.css">
    
    <!-- Plugins -->
    <link rel="stylesheet" href="../../assets/global/vendor/animsition/animsition.css">
    <link rel="stylesheet" href="../../assets/global/vendor/asscrollable/asScrollable.css">
    <link rel="stylesheet" href="../../assets/global/vendor/switchery/switchery.css">
    <link rel="stylesheet" href="../../assets/global/vendor/intro-js/introjs.css">
    <link rel="stylesheet" href="../../assets/global/vendor/slidepanel/slidePanel.css">
    <link rel="stylesheet" href="../../assets/global/vendor/jquery-mmenu/jquery-mmenu.css">
    <link rel="stylesheet" href="../../assets/global/vendor/flag-icon-css/flag-icon.css">
    <link rel="stylesheet" href="../../assets/global/vendor/waves/waves.css">
        <link rel="stylesheet" href="../../assets/global/vendor/chartist/chartist.css">
        <link rel="stylesheet" href="../../assets/global/vendor/jvectormap/jquery-jvectormap.css">
        <link rel="stylesheet" href="../../assets/global/vendor/chartist-plugin-tooltip/chartist-plugin-tooltip.css">
        <link rel="stylesheet" href="../../assets/examples/css/dashboard/v1.css">
    
    
    <!-- Fonts -->
    <link rel="stylesheet" href="../../assets/global/fonts/material-design/material-design.min.css">
    <link rel="stylesheet" href="../../assets/global/fonts/brand-icons/brand-icons.min.css">
    <link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Roboto:300,400,500,300italic'>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

     <!-- table set -->
    

    <link rel='stylesheet' href='../../assets/css/customised-crm.css'>


    <?php include "includes/css/tables.php"; ?>
    
    <!--[if lt IE 9]>
    <script src="../../assets/global/vendor/html5shiv/html5shiv.min.js"></script>
    <![endif]-->
    
    <!--[if lt IE 10]>
    <script src="../../assets/global/vendor/media-match/media.match.min.js"></script>
    <script src="../../assets/global/vendor/respond/respond.min.js"></script>
    <![endif]-->
    
    <!-- Scripts -->
    <script src="../../assets/global/vendor/breakpoints/breakpoints.js"></script>
    <script>
      Breakpoints();
    </script>
  </head>
  <body class="animsition site-navbar-small dashboard">
    <!--[if lt IE 8]>
        <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->

    <?php include "navbar-header.php"; ?>
    <?php include "side-navigation.php"; ?>   
    

    <!-- Page -->
    <div class="page">
      <div class="page-content" >
        <div class="panel">
               <div class="panel-body container-fluid">
                  <div class="row row-lg">
                     <div class="col-md-12">
                        <div class="example-wrap">
                           <h4 class="example-title"></h4>
                           <div class="example">
                            <?php



date_default_timezone_set('Asia/Kolkata');

$date = date('d/m/Y');
$year = date('Y');



$opfuniqueid=$_SESSION['opfuniqueid'];
                           
                           $fetchall=mysqli_query($dbc,"select * from `opf` where `UniqueId`='$opfuniqueid' ");
                              
                               while($qr=mysqli_fetch_assoc($fetchall))
                               {
                                   $oid=$qr['id'];
                           
                               }

                               $opfno="OPF0".$oid;
                               mysqli_query($dbc," update `opf` set `OPFNo`='$opfno' where `UniqueId`='$opfuniqueid' ");

                               mysqli_query($dbc," update `opf_all` set `OPFNo`='$opfno' where `UniqueId`='$opfuniqueid' ");






$product=$_SESSION['qproduct'];

$company=$_SESSION['qcompany'];

$fetopfall=mysqli_query($dbc,"select * from `opf` where `UniqueId`='$opfuniqueid' ");

while($row=mysqli_fetch_assoc($fetopfall))
{

    $salesrep=$row['Representive'];
  $quotrefno=$row['QuotRefNo'];

  $opfformname=$row['OPFFormName'];

    $opfcurrency=$row['OPFCurrency'];

    $endusername=$row['EndUserName'];
    $enduseremailid=$row['EndUserEMailId'];

    $deliverymode=$row['DeliveryMode'];
    $deliverycharges=$row['DeliveryCharges'];

    $creditcardname=$row['CreditCardName'];
    $cardendingno=$row['CardEndingNo'];
    $whtpercentage=$row['WHTPercentage'];
    $conversionrate=$row['ConversionRate'];

    

    
    $vendorprice1=$row['VendorPrice1'];
    $vendorprice2=$row['VendorPrice2'];
    $vendorprice3=$row['VendorPrice3'];
    $vendorprice4=$row['VendorPrice4'];
    $vendorprice5=$row['VendorPrice5'];
    $vendorprice6=$row['VendorPrice6'];
    $vendorprice7=$row['VendorPrice7'];
    $vendorprice8=$row['VendorPrice8'];
    $vendorprice9=$row['VendorPrice9'];
    $vendorprice10=$row['VendorPrice10'];
    $vendorprice11=$row['VendorPrice11'];
    $vendorprice12=$row['VendorPrice12'];
    $vendorprice13=$row['VendorPrice13'];
    $vendorprice14=$row['VendorPrice14'];
    $vendorprice15=$row['VendorPrice15'];
    $vendorprice16=$row['VendorPrice16'];
    $vendorprice17=$row['VendorPrice17'];
    $vendorprice18=$row['VendorPrice18'];
    $vendorprice19=$row['VendorPrice19'];
    $vendorprice20=$row['VendorPrice20'];
    $vendorprice21=$row['VendorPrice21'];
    $vendorprice22=$row['VendorPrice22'];
    $vendorprice23=$row['VendorPrice23'];
    $vendorprice24=$row['VendorPrice24'];
    $vendorprice25=$row['VendorPrice25'];
    $vendorprice26=$row['VendorPrice26'];
    $vendorprice27=$row['VendorPrice27'];
    $vendorprice28=$row['VendorPrice28'];
    $vendorprice29=$row['VendorPrice29'];
    $vendorprice30=$row['VendorPrice30'];
    $vendorprice31=$row['VendorPrice31'];
    $vendorprice32=$row['VendorPrice32'];
    $vendorprice33=$row['VendorPrice33'];
    $vendorprice34=$row['VendorPrice34'];
    $vendorprice35=$row['VendorPrice35'];
    $vendorprice36=$row['VendorPrice36'];
    $vendorprice37=$row['VendorPrice37'];
    $vendorprice38=$row['VendorPrice38'];
    $vendorprice39=$row['VendorPrice39'];
    $vendorprice40=$row['VendorPrice40'];
    $vendorprice41=$row['VendorPrice41'];
    $vendorprice42=$row['VendorPrice42'];
    $vendorprice43=$row['VendorPrice43'];
    $vendorprice44=$row['VendorPrice44'];
    $vendorprice45=$row['VendorPrice45'];
    $vendorprice46=$row['VendorPrice46'];
    $vendorprice47=$row['VendorPrice47'];
    $vendorprice48=$row['VendorPrice48'];
    $vendorprice49=$row['VendorPrice49'];
    $vendorprice50=$row['VendorPrice50'];
    $vendorprice51=$row['VendorPrice51'];
    $vendorprice52=$row['VendorPrice52'];
    $vendorprice53=$row['VendorPrice53'];
    $vendorprice54=$row['VendorPrice54'];
    $vendorprice55=$row['VendorPrice55'];
    $vendorprice56=$row['VendorPrice56'];
    $vendorprice57=$row['VendorPrice57'];
    $vendorprice58=$row['VendorPrice58'];
    $vendorprice59=$row['VendorPrice59'];
    $vendorprice60=$row['VendorPrice60'];

    
    $vendorname1=$row['VendorName1'];
    $product1=$row['Product1'];
    $pdescription1=$row['PartDescription1'];
    $quantity1=$row['Quantity1'];
    $unitprice1=$row['UnitPrice1'];

    $vendorname2=$row['VendorName2'];
    $product2=$row['Product2'];
    $pdescription2=$row['PartDescription2'];
    $quantity2=$row['Quantity2'];
    $unitprice2=$row['UnitPrice2'];

    $vendorname3=$row['VendorName3'];
    $product3=$row['Product3'];
    $pdescription3=$row['PartDescription3'];
    $quantity3=$row['Quantity3'];
    $unitprice3=$row['UnitPrice3'];

    $vendorname4=$row['VendorName4'];
    $product4=$row['Product4'];
    $pdescription4=$row['PartDescription4'];
    $quantity4=$row['Quantity4'];
    $unitprice4=$row['UnitPrice4'];

    $vendorname5=$row['VendorName5'];
    $product5=$row['Product5'];
    $pdescription5=$row['PartDescription5'];
    $quantity5=$row['Quantity5'];
    $unitprice5=$row['UnitPrice5'];

    $vendorname6=$row['VendorName6'];
    $product6=$row['Product6'];
    $pdescription6=$row['PartDescription6'];
    $quantity6=$row['Quantity6'];
    $unitprice6=$row['UnitPrice6'];

    $vendorname7=$row['VendorName7'];
    $product7=$row['Product7'];
    $pdescription7=$row['PartDescription7'];
    $quantity7=$row['Quantity7'];
    $unitprice7=$row['UnitPrice7'];

    $vendorname8=$row['VendorName8'];
    $product8=$row['Product8'];
    $pdescription8=$row['PartDescription8'];
    $quantity8=$row['Quantity8'];
    $unitprice8=$row['UnitPrice8'];

    $vendorname9=$row['VendorName9'];
    $product9=$row['Product9'];
    $pdescription9=$row['PartDescription9'];
    $quantity9=$row['Quantity9'];
    $unitprice9=$row['UnitPrice9'];

    $vendorname10=$row['VendorName10'];
    $product10=$row['Product10'];
    $pdescription10=$row['PartDescription10'];
    $quantity10=$row['Quantity10'];
    $unitprice10=$row['UnitPrice10'];

    $vendorname11=$row['VendorName11'];
    $product11=$row['Product11'];
    $pdescription11=$row['PartDescription11'];
    $quantity11=$row['Quantity11'];
    $unitprice11=$row['UnitPrice11'];

    $vendorname12=$row['VendorName12'];
    $product12=$row['Product12'];
    $pdescription12=$row['PartDescription12'];
    $quantity12=$row['Quantity12'];
    $unitprice12=$row['UnitPrice12'];

    $vendorname13=$row['VendorName13'];
    $product13=$row['Product13'];
    $pdescription13=$row['PartDescription13'];
    $quantity13=$row['Quantity13'];
    $unitprice13=$row['UnitPrice13'];

    $vendorname14=$row['VendorName14'];
    $product14=$row['Product14'];
    $pdescription14=$row['PartDescription14'];
    $quantity14=$row['Quantity14'];
    $unitprice14=$row['UnitPrice14'];

    $vendorname15=$row['VendorName15'];
    $product15=$row['Product15'];
    $pdescription15=$row['PartDescription15'];
    $quantity15=$row['Quantity15'];
    $unitprice15=$row['UnitPrice15'];

    $vendorname16=$row['VendorName16'];
    $product16=$row['Product16'];
    $pdescription16=$row['PartDescription16'];
    $quantity16=$row['Quantity16'];
    $unitprice16=$row['UnitPrice16'];

    $vendorname17=$row['VendorName17'];
    $product17=$row['Product17'];
    $pdescription17=$row['PartDescription17'];
    $quantity17=$row['Quantity17'];
    $unitprice17=$row['UnitPrice17'];

    $vendorname18=$row['VendorName18'];
    $product18=$row['Product18'];
    $pdescription18=$row['PartDescription18'];
    $quantity18=$row['Quantity18'];
    $unitprice18=$row['UnitPrice18'];

    $vendorname19=$row['VendorName19'];
    $product19=$row['Product19'];
    $pdescription19=$row['PartDescription19'];
    $quantity19=$row['Quantity19'];
    $unitprice19=$row['UnitPrice19'];

    $vendorname20=$row['VendorName20'];
    $product20=$row['Product20'];
    $pdescription20=$row['PartDescription20'];
    $quantity20=$row['Quantity20'];
    $unitprice20=$row['UnitPrice20'];

    $vendorname21=$row['VendorName21'];
    $product21=$row['Product21'];
    $pdescription21=$row['PartDescription21'];
    $quantity21=$row['Quantity21'];
    $unitprice21=$row['UnitPrice21'];

    $vendorname22=$row['VendorName22'];
    $product22=$row['Product22'];
    $pdescription22=$row['PartDescription22'];
    $quantity22=$row['Quantity22'];
    $unitprice22=$row['UnitPrice22'];

    $vendorname23=$row['VendorName23'];
    $product23=$row['Product23'];
    $pdescription23=$row['PartDescription23'];
    $quantity23=$row['Quantity23'];
    $unitprice23=$row['UnitPrice23'];

    $vendorname24=$row['VendorName24'];
    $product24=$row['Product24'];
    $pdescription24=$row['PartDescription24'];
    $quantity24=$row['Quantity24'];
    $unitprice24=$row['UnitPrice24'];

    $vendorname25=$row['VendorName25'];
    $product25=$row['Product25'];
    $pdescription25=$row['PartDescription25'];
    $quantity25=$row['Quantity25'];
    $unitprice25=$row['UnitPrice25'];

    $vendorname26=$row['VendorName26'];
    $product26=$row['Product26'];
    $pdescription26=$row['PartDescription26'];
    $quantity26=$row['Quantity26'];
    $unitprice26=$row['UnitPrice26'];

    $vendorname27=$row['VendorName27'];
    $product27=$row['Product27'];
    $pdescription27=$row['PartDescription27'];
    $quantity27=$row['Quantity27'];
    $unitprice27=$row['UnitPrice27'];

    $vendorname28=$row['VendorName28'];
    $product28=$row['Product28'];
    $pdescription28=$row['PartDescription28'];
    $quantity28=$row['Quantity28'];
    $unitprice28=$row['UnitPrice28'];

    $vendorname29=$row['VendorName29'];
    $product29=$row['Product29'];
    $pdescription29=$row['PartDescription29'];
    $quantity29=$row['Quantity29'];
    $unitprice29=$row['UnitPrice29'];

    $vendorname30=$row['VendorName30'];
    $product30=$row['Product30'];
    $pdescription30=$row['PartDescription30'];
    $quantity30=$row['Quantity30'];
    $unitprice30=$row['UnitPrice30'];

    $vendorname31=$row['VendorName31'];
    $product31=$row['Product31'];
    $pdescription31=$row['PartDescription31'];
    $quantity31=$row['Quantity31'];
    $unitprice31=$row['UnitPrice31'];

    $vendorname32=$row['VendorName32'];
    $product32=$row['Product32'];
    $pdescription32=$row['PartDescription32'];
    $quantity32=$row['Quantity32'];
    $unitprice32=$row['UnitPrice32'];

    $vendorname33=$row['VendorName33'];
    $product33=$row['Product33'];
    $pdescription33=$row['PartDescription33'];
    $quantity33=$row['Quantity33'];
    $unitprice33=$row['UnitPrice33'];

    $vendorname34=$row['VendorName34'];
    $product34=$row['Product34'];
    $pdescription34=$row['PartDescription34'];
    $quantity34=$row['Quantity34'];
    $unitprice34=$row['UnitPrice34'];

    $vendorname35=$row['VendorName35'];
    $product35=$row['Product35'];
    $pdescription35=$row['PartDescription35'];
    $quantity35=$row['Quantity35'];
    $unitprice35=$row['UnitPrice35'];

    $vendorname36=$row['VendorName36'];
    $product36=$row['Product36'];
    $pdescription36=$row['PartDescription36'];
    $quantity36=$row['Quantity36'];
    $unitprice36=$row['UnitPrice36'];

    $vendorname37=$row['VendorName37'];
    $product37=$row['Product37'];
    $pdescription37=$row['PartDescription37'];
    $quantity37=$row['Quantity37'];
    $unitprice37=$row['UnitPrice37'];

    $vendorname38=$row['VendorName38'];
    $product38=$row['Product38'];
    $pdescription38=$row['PartDescription38'];
    $quantity38=$row['Quantity38'];
    $unitprice38=$row['UnitPrice38'];

    $vendorname39=$row['VendorName39'];
    $product39=$row['Product39'];
    $pdescription39=$row['PartDescription39'];
    $quantity39=$row['Quantity39'];
    $unitprice39=$row['UnitPrice39'];

    $vendorname40=$row['VendorName40'];
    $product40=$row['Product40'];
    $pdescription40=$row['PartDescription40'];
    $quantity40=$row['Quantity40'];
    $unitprice40=$row['UnitPrice40'];

    $vendorname41=$row['VendorName41'];
    $product41=$row['Product41'];
    $pdescription41=$row['PartDescription41'];
    $quantity41=$row['Quantity41'];
    $unitprice41=$row['UnitPrice41'];

    $vendorname42=$row['VendorName42'];
    $product42=$row['Product42'];
    $pdescription42=$row['PartDescription42'];
    $quantity42=$row['Quantity42'];
    $unitprice42=$row['UnitPrice42'];

    $vendorname43=$row['VendorName43'];
    $product43=$row['Product43'];
    $pdescription43=$row['PartDescription43'];
    $quantity43=$row['Quantity43'];
    $unitprice43=$row['UnitPrice43'];

    $vendorname44=$row['VendorName44'];
    $product44=$row['Product44'];
    $pdescription44=$row['PartDescription44'];
    $quantity44=$row['Quantity44'];
    $unitprice44=$row['UnitPrice44'];

    $vendorname45=$row['VendorName45'];
    $product45=$row['Product45'];
    $pdescription45=$row['PartDescription45'];
    $quantity45=$row['Quantity45'];
    $unitprice45=$row['UnitPrice45'];

    $vendorname46=$row['VendorName46'];
    $product46=$row['Product46'];
    $pdescription46=$row['PartDescription46'];
    $quantity46=$row['Quantity46'];
    $unitprice46=$row['UnitPrice46'];

    $vendorname47=$row['VendorName47'];
    $product47=$row['Product47'];
    $pdescription47=$row['PartDescription47'];
    $quantity47=$row['Quantity47'];
    $unitprice47=$row['UnitPrice47'];

    $vendorname48=$row['VendorName48'];
    $product48=$row['Product48'];
    $pdescription48=$row['PartDescription48'];
    $quantity48=$row['Quantity48'];
    $unitprice48=$row['UnitPrice48'];

    $vendorname49=$row['VendorName49'];
    $product49=$row['Product49'];
    $pdescription49=$row['PartDescription49'];
    $quantity49=$row['Quantity49'];
    $unitprice49=$row['UnitPrice49'];

    $vendorname50=$row['VendorName50'];
    $product50=$row['Product50'];
    $pdescription50=$row['PartDescription50'];
    $quantity50=$row['Quantity50'];
    $unitprice50=$row['UnitPrice50'];

    $vendorname51=$row['VendorName51'];
    $product51=$row['Product51'];
    $pdescription51=$row['PartDescription51'];
    $quantity51=$row['Quantity51'];
    $unitprice51=$row['UnitPrice51'];

    $vendorname52=$row['VendorName52'];
    $product52=$row['Product52'];
    $pdescription52=$row['PartDescription52'];
    $quantity52=$row['Quantity52'];
    $unitprice52=$row['UnitPrice52'];

    $vendorname53=$row['VendorName53'];
    $product53=$row['Product53'];
    $pdescription53=$row['PartDescription53'];
    $quantity53=$row['Quantity53'];
    $unitprice53=$row['UnitPrice53'];

    $vendorname54=$row['VendorName54'];
    $product54=$row['Product54'];
    $pdescription54=$row['PartDescription54'];
    $quantity54=$row['Quantity54'];
    $unitprice54=$row['UnitPrice54'];

    $vendorname55=$row['VendorName55'];
    $product55=$row['Product55'];
    $pdescription55=$row['PartDescription55'];
    $quantity55=$row['Quantity55'];
    $unitprice55=$row['UnitPrice55'];

    $vendorname56=$row['VendorName56'];
    $product56=$row['Product56'];
    $pdescription56=$row['PartDescription56'];
    $quantity56=$row['Quantity56'];
    $unitprice56=$row['UnitPrice56'];

    $vendorname57=$row['VendorName57'];
    $product57=$row['Product57'];
    $pdescription57=$row['PartDescription57'];
    $quantity57=$row['Quantity57'];
    $unitprice57=$row['UnitPrice57'];

    $vendorname58=$row['VendorName58'];
    $product58=$row['Product58'];
    $pdescription58=$row['PartDescription58'];
    $quantity58=$row['Quantity58'];
    $unitprice58=$row['UnitPrice58'];

    $vendorname59=$row['VendorName59'];
    $product59=$row['Product59'];
    $pdescription59=$row['PartDescription59'];
    $quantity59=$row['Quantity59'];
    $unitprice59=$row['UnitPrice59'];

    $vendorname60=$row['VendorName60'];
    $product60=$row['Product60'];
    $pdescription60=$row['PartDescription60'];
    $quantity60=$row['Quantity60'];
    $unitprice60=$row['UnitPrice60'];

    

    $servicename=$row['ServiceName'];
    $servicecost=$row['ServiceCost'];
    $servicetax=$row['ServiceTax'];
    $freightname=$row['FreightName'];
    $freightcost=$row['FreightCost'];
    $freighttax=$row['FreightTax'];

    $quotpath1=$row['QuotationPath1'];
    $quotpath2=$row['QuotationPath2'];
    $quotpath3=$row['QuotationPath3'];
    $quotpath4=$row['QuotationPath4'];
    $quotpath5=$row['QuotationPath5'];
    $quotpath6=$row['QuotationPath6'];
    $quotpath7=$row['QuotationPath7'];
    $quotpath8=$row['QuotationPath8'];
    $quotpath9=$row['QuotationPath9'];
    $quotpath10=$row['QuotationPath10'];
    $quotpath11=$row['QuotationPath11'];
    $quotpath12=$row['QuotationPath12'];
    $quotpath13=$row['QuotationPath13'];
    $quotpath14=$row['QuotationPath14'];
    $quotpath15=$row['QuotationPath15'];
    $quotpath16=$row['QuotationPath16'];
    $quotpath17=$row['QuotationPath17'];
    $quotpath18=$row['QuotationPath18'];
    $quotpath19=$row['QuotationPath19'];
    $quotpath20=$row['QuotationPath20'];
    $quotpath21=$row['QuotationPath21'];
    $quotpath22=$row['QuotationPath22'];
    $quotpath23=$row['QuotationPath23'];
    $quotpath24=$row['QuotationPath24'];
    $quotpath25=$row['QuotationPath25'];
    $quotpath26=$row['QuotationPath26'];
    $quotpath27=$row['QuotationPath27'];
    $quotpath28=$row['QuotationPath28'];
    $quotpath29=$row['QuotationPath29'];
    $quotpath30=$row['QuotationPath30'];
    $quotpath31=$row['QuotationPath31'];
    $quotpath32=$row['QuotationPath32'];
    $quotpath33=$row['QuotationPath33'];
    $quotpath34=$row['QuotationPath34'];
    $quotpath35=$row['QuotationPath35'];
    $quotpath36=$row['QuotationPath36'];
    $quotpath37=$row['QuotationPath37'];
    $quotpath38=$row['QuotationPath38'];
    $quotpath39=$row['QuotationPath39'];
    $quotpath40=$row['QuotationPath40'];
    $quotpath41=$row['QuotationPath41'];
    $quotpath42=$row['QuotationPath42'];
    $quotpath43=$row['QuotationPath43'];
    $quotpath44=$row['QuotationPath44'];
    $quotpath45=$row['QuotationPath45'];
    $quotpath46=$row['QuotationPath46'];
    $quotpath47=$row['QuotationPath47'];
    $quotpath48=$row['QuotationPath48'];
    $quotpath49=$row['QuotationPath49'];
    $quotpath50=$row['QuotationPath50'];
    $quotpath51=$row['QuotationPath51'];
    $quotpath52=$row['QuotationPath52'];
    $quotpath53=$row['QuotationPath53'];
    $quotpath54=$row['QuotationPath54'];
    $quotpath55=$row['QuotationPath55'];
    $quotpath56=$row['QuotationPath56'];
    $quotpath57=$row['QuotationPath57'];
    $quotpath58=$row['QuotationPath58'];
    $quotpath59=$row['QuotationPath59'];
    $quotpath60=$row['QuotationPath60'];


    $popath=$row['PO'];


}


$fetchall=mysqli_query($dbc,"select * from `quotation` where `QuotNo`='$quotrefno' ");

while($row=mysqli_fetch_assoc($fetchall))
{
    $company=$row['Company'];
    /*$product=$row['Product'];*/
    $tax=$row['Tax'];
    $currency=$row['Currency'];
    $hsnsac=$row['HSNSAC'];
    $service=$row['Service'];

    

    $delivery=$row['Delivery'];
    $validity=$row['Validity'];
    $payment=$row['Payment'];

    $servicename=$row['ServiceName'];
    $servicecost=$row['ServiceCost'];
    $servicetax=$row['ServiceTax'];
    $freightname=$row['FreightName'];
    $freightcost=$row['FreightCost'];
    $freighttax=$row['FreightTax'];

    $quotformname=$row['QuotFormName'];

    

}


                           $fetcit=mysqli_query($dbc,"select * from customers where `Company`='$company'");
while($frc=mysqli_fetch_assoc($fetcit))
{
    $city=$frc['City'];
    $firstname=$frc['FirstName'];
    $address=$frc['Address'];
}

if($currency == "rupee")
{
    $cur="INR";
}
elseif($currency == "dollar")
{
    $cur="USD";
}
elseif ($currency == "euro") {
    $cur="EUR";
}
elseif ($currency == "pound") {
    $cur="GBP";
}

$ou=$_SESSION['ou'];




                           if($ou == $opfuniqueid)
                           {


                            for ($i = 1; $i < 60; $i++) {
        
            // Do stuff with this row of the form
            $product=$_POST['product'][$i];

            $partd=$_POST['partdesc'][$i];
            $qt=$_POST['qty'][$i];
            $unitp=$_POST['unitprice'][$i];

            $vendorprice=$_POST['vendorprice'][$i];

            $vendorname=$_POST['sellername'][$i];


            if(!empty($product) and !empty($unitp))
            {

            if($i=='1')
            {
                
            mysqli_query($dbc,"update  `opf_all` set `UniqueId`='$quotuniqueid',`OPFNo`='$opfno',`QuotRefNo`='$quotrefno',`EndUserName`='$endusername',`EndUserEMailId`='$enduseremailid',`DeliveryMode`='$deliverymode',`DeliveryCharges`='$deliverycharges',`VendorName1`='$vendorname1',`VendorPrice1`='$vendorprice1',`Product1`='$product1',`PartDescription1`='$pdescription1',`Quantity1`='$quantity1',`UnitPrice1`='$unitprice1',`QuotationPath1`='$quotpath1',`PO`='$popath' where  `UniqueId`='$opfuniqueid' ");

           
            }
            elseif($i>1)
            {
              
                $productcolumn="Product".$i;
                $partdescolumn="PartDescription".$i;
                $quantitycolumn="Quantity".$i;
                $unitpricecolumn="UnitPrice".$i;
                $quotationpathcolumn="QuotationPath".$i;
                
                $vendornamecolumn="VendorName".$i;
                $vendorpricecolumn="VendorPrice".$i;
                
               

                $ad=mysqli_query($dbc,"update `opf_all` set `$vendornamecolumn`='$vendorname',`$vendorpricecolumn`='$vendorprice',`$productcolumn`='$product',`$partdescolumn`='$partd',`$quantitycolumn`='$qt',`$unitpricecolumn`='$unitp',`$quotationpathcolumn`='$newFilePathq'  where `OPFNo`='$opfno' and `UniqueId`='$opfuniqueid'  ");

               
            }
        
    }
}

                           }
                           else
                           {
                            /*check columns available start*/
    
                                $result=mysqli_query($dbc,"show columns from opf");
                                    $k=0;
                                    $c=0;
                                    while($row = mysqli_fetch_array($result)){
                                        $str=$row['Field'];
                                       
                                       $newstr = filter_var($str, FILTER_SANITIZE_STRING);
                                       if($newstr="Product")
                                       {


                                       $int_id = filter_var($str, FILTER_SANITIZE_NUMBER_INT);
                                       
                                       if($int_id > $k)
                                       {
                                          global $c;
                                          $c=$int_id;
                                          $k=$int_id;
         
                                          }
                                       }
                                       
                                    }
/*check columns available end*/  
    for ($i = 1; $i < 500; $i++){
        
            // Do stuff with this row of the form
            $product=$_POST['product'][$i];

            $partd=$_POST['partdesc'][$i];
            $qt=$_POST['qty'][$i];
            $unitp=$_POST['unitprice'][$i];

            $vendorprice=$_POST['vendorprice'][$i];

            $vendorname=$_POST['sellername'][$i];
            

            if(!empty($product) and !empty($unitp))
            {

            if($i=='0')
            {
                //Get the temp file path
                $tmpFilePath = $_FILES['uplfiles']['tmp_name'][$i];

                //Make sure we have a file path
                if ($tmpFilePath != ""){
                //Setup our new file path
                $newFilePath = "../../../../uploadfiles/opf/".$opfuniqueid."-".$_FILES['uplfiles']['name'][$i];

                //Upload the file into the temp dir
                if(move_uploaded_file($tmpFilePath, $newFilePath)) {

                //Handle other code here
                    $newFilePathq = $opfuniqueid."-".$_FILES['uplfiles']['name'][$i];

                    }
                }

                //Get the temp file path
                $tmpFilePath = $_FILES['pofile']['tmp_name'];

                //Make sure we have a file path
                if ($tmpFilePath != ""){
                //Setup our new file path
                $newFilePath = "../../../../uploadfiles/opf/po-".$opfuniqueid."-".$_FILES['pofile']['name'];

                //Upload the file into the temp dir
                if(move_uploaded_file($tmpFilePath, $newFilePath)) {

                //Handle other code here
                    $newFilePathpo ="po-".$opfuniqueid."-".$_FILES['pofile']['name'];

                    }
                }

                

            $ad=mysqli_query($dbc,"insert into `opf` (`Representive`,`RMail`,`UniqueId`,`QuotRefNo`,`OPFFormName`,`OPFCurrency`,`EndUserName`,`EndUserEMailId`,`DeliveryMode`,`DeliveryCharges`,`VendorName1`,`VendorPrice1`,`Product1`,`PartDescription1`,`Quantity1`,`UnitPrice1`,`QuotationPath1`,`CreditCardName`,`CardEndingNo`,`WHTPercentage`,`ConversionRate`,`PO`) values ('$rep','$rmail','$opfuniqueid','$quotrefno','$opfformname','$opfcurrency','$endusername','$enduseremailid','$deliverymode','$deliverycharges','$vendorname','$vendorprice','$product','$partd','$qt','$unitp','$newFilePathq','$creditcardname','$cardnoending','$whtpercent','$conversionrate','$newFilePathpo')");

            $ad=mysqli_query($dbc,"insert into `opf_all` (`Representive`,`RMail`,`UniqueId`,`QuotRefNo`,`OPFFormName`,`OPFCurrency`,`EndUserName`,`EndUserEMailId`,`DeliveryMode`,`DeliveryCharges`,`VendorName1`,`VendorPrice1`,`Product1`,`PartDescription1`,`Quantity1`,`UnitPrice1`,`QuotationPath1`,`CreditCardName`,`CardEndingNo`,`WHTPercentage`,`ConversionRate`,`PO`) values ('$rep','$rmail','$opfuniqueid','$quotrefno','$opfformname','$opfcurrency','$endusername','$enduseremailid','$deliverymode','$deliverycharges','$vendorname','$vendorprice','$product','$partd','$qt','$unitp','$newFilePathq','$creditcardname','$cardnoending','$whtpercent','$conversionrate','$newFilePathpo')");



            $ad=mysqli_query($dbc,"update `quotation` set `Product1`='$product',`PartDescription1`='$partd',`Quantity1`='$qt',`UnitPrice1`='$unitp',`QuotationPath1`='$newFilePathq' where `QuotNo`='$quotrefno' ");

           
            }
            elseif($i>=1)
            {
                if($i==$c)
                {
                                    $productnew="Product".($c+1);
                                    $partdescnew="PartDescription".($c+1);
                                    $quantitynew="Quantity".($c+1);
                                    $unitpricenew="UnitPrice".($c+1);
                                    
                                    $hsnsacnew="HSNSAC".($c+1);
                                    $quotationpathnew="QuotationPath".($c+1);
                                    $attachmentnew="Attachment".($c+1);
                                    $licencefornew="LicenceFor".($c+1);

                                    $vendornamenew="VendorName".($c+1);
                                    $vendorpricenew="VendorPrice".($c+1);

                                    

                                    mysqli_query($dbc,"ALTER TABLE `opf_all` ADD `$productnew` TEXT NULL, ADD `$partdescnew` TEXT NULL,ADD `$quantitynew` TEXT NULL,ADD `$unitpricenew` TEXT NULL,ADD `$quotationpathnew` TEXT NULL,ADD `$vendornamenew` TEXT NULL,ADD `$vendorpricenew` TEXT NULL");

                                    
                                    $c=$c+1;
                }

                //Get the temp file path
                $tmpFilePath = $_FILES['uplfiles']['tmp_name'][$i];

                //Make sure we have a file path
                if ($tmpFilePath != ""){
                //Setup our new file path
                $newFilePath = "../../../../uploadfiles/opf/".$opfuniqueid."-".$_FILES['uplfiles']['name'][$i];

                //Upload the file into the temp dir
                if(move_uploaded_file($tmpFilePath, $newFilePath)) {

                //Handle other code here
                    $newFilePathq = $opfuniqueid."-".$_FILES['uplfiles']['name'][$i];

                    }
                }
                $ii=$i+1;
                $productcolumn="Product".$ii;
                $partdescolumn="PartDescription".$ii;
                $quantitycolumn="Quantity".$ii;
                $unitpricecolumn="UnitPrice".$ii;
                $quotationpathcolumn="QuotationPath".$ii;
                $hsnsaccolumn="HSNSAC".$ii;
                $licenceforcolumn="LicenceFor".$ii;

                $vendornamecolumn="VendorName".$ii;
                $vendorpricecolumn="VendorPrice".$ii;
                
             
                $ad=mysqli_query($dbc,"update `opf_all` set `$vendornamecolumn`='$vendorname',`$vendorpricecolumn`='$vendorprice',`$productcolumn`='$product',`$partdescolumn`='$partd',`$quantitycolumn`='$qt',`$unitpricecolumn`='$unitp',`$quotationpathcolumn`='$newFilePathq'  where  `UniqueId`='$opfuniqueid'  ");

            }
       
    }
        else
        {
            break;
        }

        
     }   
    
                           }

                           

                           $quotupdatefetch=mysqli_query($dbc,"select * from `opf_all` where `OPFNo`='$opfno' ");
                           $otype=0;
                           while($qar=mysqli_fetch_assoc($quotupdatefetch))
                           {
                              $otype=$otype+1;
                           }



                           ?>
                           <div class="row">

                                    <div class="form-group col-md-4" style="text-align: center;">
                                       <img src="../../assets/images/logo.png" >
                                    </div>

                                    <div class="col-md-4"><font style="text-align: center; font-weight: bold;font-size: 24px">ORDER PROCESSING FORMAT</font></div>

                                    <div class="col-md-4">&nbsp;</div>

                                    
                                    
                                    
                                 </div>
                                 <div class="row">
                                    <div class="col-md-3" style="font-weight: bold;text-align: center;font-size: 16px"><?php if($otype > 1){$otype=$otype-1; echo "Ref No: ".$opfno." R".$otype; }else{ echo "Ref No: ".$opfno; } ?>
                                            </div>



                                            <div class="col-md-4 " style="font-weight: bold;text-align: center;font-size: 16px"><?php echo "Date: ".$date; ?>
                                            </div>

                                            <div class="col-md-4 " style="font-weight: bold;text-align: center;font-size: 16px">Created By Sales Rep: <?php echo $salesrep; ?>
                                                
                                            </div>
                        </div>
                        
                        
                        <br><br>
                        <div class="row" style="font-size: 15px">
                            <div class="col-md-12 pull-left" style="font-weight: bold;">Customer Details</div>
                                            
                        </div>
                        <br>

                        <div class="row" style="font-size: 16px">
                           <div class="col-md-12 table-responsive">
                              <table class="table table-bordered table-striped mb-none" >
                                 <thead>
                                    <tr>
                                       <th>Sl No.</th>
                                        <th>Customer Name</th>
                                        <th>Product</th>
                                        <th>Description</th>
                                                        
                                        <th>Qty</th>
                                        <th>Unit Price(<?php echo $cur; ?>)</th>
                                        <th>Sub Total(<?php echo $cur; ?>)</th>
                                        <th colspan="2" align="center">GST Rate</th>
                                        <th colspan="2" align="center">GST Amount</th>
                                        <th>Total Price(<?php echo $cur; ?>)</th>
                                    </tr>
                                 </thead>
                                 <tbody>
                                    <?php 
                                       $ctotal=0;$total1=0;$total2=0;$total3=0;$total4=0;$total5=0;$total6=0;$total7=0;$total8=0;$total9=0;$total10=0;$igsttotal=0;$sgsttotal=0;$cgsttotal=0;$grandtotal=0;$subtotal=0;
                                        $grandtotal1=0;$grandtotal2=0;$grandtotal3=0;$grandtotal4=0;$grandtotal5=0;
                                        $grandtotal6=0;$grandtotal7=0;$grandtotal8=0;$grandtotal9=0;$grandtotal10=0;

                                        $customercurrency=$cur;

/*multiplevendor start*/
                                        /*if($opfformname=="multiplevendor")
                                        {*/



 /*multipletax start*/                                  
if($quotformname == "multipletax")
 {
    $chk=0;

                                    for($k=1;$k<=60;$k++)
                                    {
                                          
                                             $m=$k+1;
                                         
                                          
                                          for($l=1;$l<=$k;$l++)
                                          {
                                             $pdsc="pdescription".$l;
                                             $qt="quantity".$l;
                                             $up="unitprice".$l;
                                             $prod="product".$l;
                                             
                                          } 

                                          if(!empty($$prod))
                                             {
                                       
                                       
                                       $partdesc1=$$pdsc;
                                       $qt1=$$qt;
                                       $unitp1=sprintf("%.2f",$$up);
                                       $total1=sprintf("%.2f",$qt1*$unitp1);

                                       if($k == 1)
                                       {
                                        echo '<tr><td>'.$k.'</td><td>'.$company.'</td><td>'.$$prod.'</td><td>'.$partdesc1.'</td><td>'.$qt1.'</td><td>'.$unitp1.'</td><td>'.$total1.'</td>';

                                       }
                                       else
                                       {
                                             echo '<tr><td>'.$k.'</td><td></td><td>'.$$prod.'</td><td>'.$partdesc1.'</td><td>'.$qt1.'</td><td>'.$unitp1.'</td><td>'.$total1.'</td>';
                                       }
                                       
                                       

                                        $ctotal=sprintf("%.2f",$ctotal+$total1);
                                       
                                       
                                       $array =  explode(',', $tax);
                                       $count=0;
                                       $grandtotal1=0;
                                       $subtotal1=0;
                                       $cgstshow=0;
                                       foreach ($array as $item) {
                                       global $count;
                                       $count=$count+1;
                                       }
                                       
                                       foreach ($array as $item) {
                                       
                                       
                                       $taxperarr =  explode('-', $item);


                                       
                                       foreach ($taxperarr as $perr) {
                                        $persep=explode('@', $perr);
                                        
                                          $taxpercentage=$persep[0];
                                        
                                          }

                                          

                                          foreach ($taxperarr as $perr) {
                                            
                                        $persep=explode('@', $perr);
                                        
                                          $showtax=$persep[1];

                                           
                                          if($l==$showtax)
                                          {

                                          
                                       
                                       $totalwtax1=sprintf("%.2f",($taxpercentage/100)*$total1);

                                       
                                       if($count>1 and $taxperarr[0]=="SGST" or $taxperarr[0]=="CGST")
                                       {
                                       
                                       
                                          echo '<td>'.$taxperarr[0].' '.$taxpercentage.'%'.'</td>';
                                          $cgstshow=1;
                                       
                                       }
                                       else
                                       {
                                          echo '<td colspan="2">'.$taxperarr[0].' '.$taxpercentage.'%'.'</td>';
                                                                              
                                       }
                                       
                                       
                                       $grandtotal1=$grandtotal1+$totalwtax1;
 
                                     
                                   }
                                     
                                       }
                                      
                                       
                                       
                                     }
                                       
                                       if($count>1 and $cgstshow==1)
                                       {
                                       
                                       echo '<td>'.$totalwtax1.'</td>';
                                       echo '<td>'.$totalwtax1.'</td>';
                                       
                                       
                                       }
                                       else
                                       {
                                       
                                       echo '<td colspan="2">'.$totalwtax1.'</td>';
                                       
                                       
                                       }
                                       $grandtotal=$grandtotal+$grandtotal1;
                                       
                                       
                                       $subtotal1=sprintf("%.2f",$grandtotal1+$total1);
                                       
                                       echo '<td align="center">'.$subtotal1.'</td></tr>';

                                          $subtotal=$subtotal+$subtotal1;
                                       
                                       }
                                       else
                                       {
                                          global $chk;
                                          $chk=$chk+1;
                                       }

                                       
                                          if ($chk>0) {
                                          break;
                                       }

                                       }



    $stax=0;$serviceamount=0;
    if(!empty($servicename) and !empty($servicecost) and !empty($servicetax))
    {
        $stax=($servicetax/100)*$servicecost;
        $serviceamount=$servicecost+$stax;
        echo '<tr><td></td><td></td><td>Service</td><td colspan="3">Name: '.$servicename.'</td><td>Cost: '.$servicecost.'</td><td colspan="2">Tax: '.$servicetax.'%</td><td colspan="2" align="center">'.$stax.'</td><td  align="center">'.$serviceamount.'</td></tr>';
    }

    $ftax=0;$freightamount=0;
    if(!empty($freightname) and !empty($freightcost) and !empty($freighttax))
    {
        $ftax=($freighttax/100)*$freightcost;
        $freightamount=$freightcost+$ftax;
        echo '<tr><td></td><td></td><td>Freight</td><td colspan="3">Name: '.$freightname.'</td><td>Cost: '.$freightcost.'</td><td colspan="2">Tax: '.$freighttax.'%</td><td colspan="2" align="center">'.$ftax.'</td><td  align="center">'.$freightamount.'</td></tr>';
    }

    /*if(!empty($deliverymode) and !empty($deliverycharges))
    {
        echo '<tr><td></td><td></td><td></td><td></td><td >Delivery</td><td>Delivery Mode:</td><td >'.$deliverymode.'</td><td colspan="2">Delivery Charges:</td><td colspan="2"></td><td  align="center">'.$deliverycharges.'</td></tr>';
    }
*/

    $grandtax=sprintf("%.2f",$grandtotal+$stax+$ftax);
    $grandsub=sprintf("%.2f",$subtotal+$serviceamount+$freightamount);

//$ctotal=sprintf("%.2f",$total1+$total2+$total3+$total4+$total5+$total6+$total7+$total8+$total9+$total10);



    echo '<tr style="font-weight:bold"><td></td><td></td><td></td><td></td><td></td><td>Sub Total</td><td>'.$ctotal.'</td><td colspan="2">Grand Total</td><td colspan="2" align="center">'.$grandtax.'</td><td  align="center">'.$grandsub.'</td></tr>';

    mysqli_query($dbc,"update `quotation` set `GrandTotal`='$grandsub' where  `QuotNo`='$quotno' and `RMail`='$id'  ");

    mysqli_query($dbc,"update `opf` set `CustomerGrandTotal`='$grandsub' where  `OPFNo`='$opfno' and `RMail`='$id' ");



    for($l=1;$l<=10;$l++)
     {
        if(!empty($l))
        {
            $product="product".$l;
            $prd[]=$$product;
        }
        
     }
     

     /*$requirements=implode(',',$prd);

     mysqli_query($dbc,"insert into `funnel` (`Representive`,`RMail`,`CampaignType`,`Company`,`Sector`,`Products`,`Probability`,`Revenue`,`Stage`,`Services`) values ('$rep','$rmail','Direct','$company','$sector','$requirements','50','$grandsub','Quotation','$service')");
*/





?>



                                                    </tr>
                                                </tbody>
                                                </table>
                                            </div>
                                            
                                        </div><br><br>



                                        <!-- customer detail end -->
                                        <!-- vendor detail start -->

                                        
<?php 
$currency=$opfcurrency; 
if($currency == "rupee")
{
    $cur="INR";
}
elseif($currency == "dollar")
{
    $cur="USD";
}
elseif ($currency == "euro") {
    $cur="EUR";
}
elseif ($currency == "pound") {
    $cur="GBP";
}
if($currency == "rupee")
{

?>
                                       <div class="row" style="font-size: 15px">
                                            
                                            
                                            <div class="col-md-12 pull-left" style="font-weight: bold;">Vendor/Seller Details</div>
                                            
                                            

                                        </div><br>

                                        

                                        <div class="row" style="font-size: 15px">
                                            
                                            
                                            <div class="col-md-12 table-responsive">
                                                <table class="table table-bordered table-striped mb-none" >
                                                    <thead>
                                                    <tr>
                                                        <th>Sl No.</th>
                                                        <th>Vendor Name</th>
                                                        <th>Product</th>
                                                        <th>Description</th>
                                                        
                                                        <th>Qty</th>
                                                        <th>Unit Price(<?php echo $cur; ?>)</th>
                                                        <th>Sub Total(<?php echo $cur; ?>)</th>
                                                        <th colspan="2" align="center">GST Rate</th>
                                                        <th colspan="2" align="center">GST Amount</th>
                                                        <th>Total Price(<?php echo $cur; ?>)</th>

                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    
                                                       <?php 
                                                        $vtotal=0;$total1=0;$igsttotal=0;$sgsttotal=0;$cgsttotal=0;$grandtotal=0;$subtotal=0;
                                                        $grandtotal1=0;

$w=60;$h=20;

$chk=0;
                                       
                                       for($k=1;$k<=60;$k++)
                                       {
                                          
                                             $m=$k+1;
                                         
                                          
                                          for($l=1;$l<=$k;$l++)
                                          {
                                             $pdsc="pdescription".$l;
                                             $qt="quantity".$l;
                                             $up="vendorprice".$l;
                                             $prod="product".$l;
                                             $vend="vendorname".$l;
  
                                          } 

                                          if(!empty($$prod))
                                             {
                                       
                                       
                                       $partdesc1=$$pdsc;
                                       $qt1=$$qt;
                                       $unitp1=sprintf("%.2f",$$up);
                                       $total1=sprintf("%.2f",$qt1*$unitp1);

                                       
                                       


                                       if($opfformname=="multiplevendor")
                                       {
                                         if($k==1)
                                       {
                                        echo '<tr><td>'.$k.'</td><td>'.$vendorname1.'</td><td>'.$$prod.'</td><td>'.$partdesc1.'</td><td>'.$qt1.'</td><td>'.$unitp1.'</td><td>'.$total1.'</td>';
                                       }
                                       else
                                       {
                                        echo '<tr><td>'.$k.'</td><td>'.$$vend.'</td><td>'.$$prod.'</td><td>'.$partdesc1.'</td><td>'.$qt1.'</td><td>'.$unitp1.'</td><td>'.$total1.'</td>';
                                       }

                                       }
                                       else
                                       {
                                        if($k==1)
                                       {
                                        echo '<tr><td>'.$k.'</td><td>'.$vendorname1.'</td><td>'.$$prod.'</td><td>'.$partdesc1.'</td><td>'.$qt1.'</td><td>'.$unitp1.'</td><td>'.$total1.'</td>';
                                       }
                                       else
                                       {
                                        echo '<tr><td>'.$k.'</td><td></td><td>'.$$prod.'</td><td>'.$partdesc1.'</td><td>'.$qt1.'</td><td>'.$unitp1.'</td><td>'.$total1.'</td>';
                                       }
                                       }


                                       

                                       $vtotal=sprintf("%.2f",$vtotal+$total1);
                                       
                                       
                                       $array =  explode(',', $tax);
                                       $count=0;
                                       $grandtotal1=0;
                                       $subtotal1=0;
                                       $cgstshow=0;
                                       foreach ($array as $item) {
                                       global $count;
                                       $count=$count+1;
                                       }
                                       
                                       foreach ($array as $item) {
                                       
                                       
                                       $taxperarr =  explode('-', $item);


                                       
                                       foreach ($taxperarr as $perr) {
                                        $persep=explode('@', $perr);
                                        
                                          $taxpercentage=$persep[0];
                                        
                                          }

                                          

                                          foreach ($taxperarr as $perr) {
                                            
                                        $persep=explode('@', $perr);
                                        
                                          $showtax=$persep[1];

                                           
                                          if($l==$showtax)
                                          {

                                          
                                       
                                       $totalwtax1=sprintf("%.2f",($taxpercentage/100)*$total1);

                                       
                                       if($count>1 and $taxperarr[0]=="SGST" or $taxperarr[0]=="CGST")
                                       {
                                       
                                       
                                          echo '<td>'.$taxperarr[0].' '.$taxpercentage.'%'.'</td>';
                                          $cgstshow=1;
                                       
                                       }
                                       else
                                       {
                                          echo '<td colspan="2">'.$taxperarr[0].' '.$taxpercentage.'%'.'</td>';
                                                                              
                                       }
                                       
                                       
                                       $grandtotal1=$grandtotal1+$totalwtax1;
 
                                     
                                   }
                                     
                                       }
                                      
                                       
                                       
                                     }
                                       
                                       if($count>1 and $cgstshow==1)
                                       {
                                       
                                       echo '<td>'.$totalwtax1.'</td>';
                                       echo '<td>'.$totalwtax1.'</td>';
                                       
                                       
                                       }
                                       else
                                       {
                                       
                                       echo '<td colspan="2">'.$totalwtax1.'</td>';
                                       
                                       
                                       }

                                       $grandtotal=$grandtotal+$grandtotal1;
                                       
                                       
                                       $subtotal1=sprintf("%.2f",$grandtotal1+$total1);
                                       
                                       echo '<td align="center">'.$subtotal1.'</td></tr>';

                                          $subtotal=$subtotal+$subtotal1;
                                       
                                       }
                                       else
                                       {
                                          global $chk;
                                          $chk=$chk+1;
                                       }

                                       
                                          if ($chk>0) {
                                          break;
                                       }

                                       }




    $stax=0;$serviceamount=0;
    if(!empty($servicename) and !empty($servicecost) and !empty($servicetax))
    {
        $stax=($servicetax/100)*$servicecost;
        $serviceamount=$servicecost+$stax;
        echo '<tr><td></td><td></td><td>Service</td><td colspan="3">Name: '.$servicename.'</td><td>Cost: '.$servicecost.'</td><td colspan="2">Tax: '.$servicetax.'%</td><td colspan="2" align="center">'.$stax.'</td><td  align="center">'.$serviceamount.'</td></tr>';
    }

    $ftax=0;$freightamount=0;
    if(!empty($freightname) and !empty($freightcost) and !empty($freighttax))
    {
        $ftax=($freighttax/100)*$freightcost;
        $freightamount=$freightcost+$ftax;
        echo '<tr><td></td><td></td><td>Freight</td><td colspan="3">Name: '.$freightname.'</td><td>Cost: '.$freightcost.'</td><td colspan="2">Tax: '.$freighttax.'%</td><td colspan="2" align="center">'.$ftax.'</td><td  align="center">'.$freightamount.'</td></tr>';
    }


    $vgrandtax=sprintf("%.2f",$grandtotal+$stax+$ftax);
    $vgrandsub=sprintf("%.2f",$subtotal+$serviceamount+$freightamount);

//$vtotal=sprintf("%.2f",$total1+$total2+$total3+$total4+$total5+$total6+$total7+$total8+$total9+$total10);



    echo '<tr style="font-weight:bold"><td></td><td></td><td></td><td></td><td></td><td>Sub Total</td><td>'.$vtotal.'</td><td colspan="2">Grand Total</td><td colspan="2" align="center">'.$vgrandtax.'</td><td  align="center">'.$vgrandsub.'</td></tr>';

    if(!empty($deliverymode) and !empty($deliverycharges))
    {
        echo '<tr><td></td><td></td><td></td><td></td><td></td><td >Delivery</td><td>Delivery Mode:</td><td colspan="2">'.$deliverymode.'</td><td colspan="2">Delivery Charges:</td><td  align="center">'.$deliverycharges.'</td></tr>';
    }

    $gp=0;
    $gp=sprintf("%.2f",$ctotal-$vtotal-$deliverycharges); 

    echo '<tr style="font-weight:bold;text-align:left"><td></td><td></td><td></td><td colspan="3"></td><td></td><td colspan="2"></td><td colspan="2" align="center">GP</td><td  align="center">'.$gp.'</td></tr>';


    mysqli_query($dbc,"update `quotation` set `SubTotal`='$subtotal',`GrandTotal`='$grandsub' where `Funnelid`='$fid' and `QuotNo`='$quotno' and `Company`='$company'  ");

    for($l=1;$l<=10;$l++)
     {
        if(!empty($l))
        {
            $product="product".$l;
            $prd[]=$$product;
        }
        
     }
     

     $requirements=implode(',',$prd);

     mysqli_query($dbc,"update `funnel` (`Representive`,`RMail`,`CampaignType`,`Company`,`Sector`,`Products`,`Probability`,`Revenue`,`Stage`,`Services`) values ('$rep','$rmail','Direct','$company','$sector','$requirements','50','$grandsub','Quotation','$service')");






?>



                                                    </tr>
                                                </tbody>
                                                </table>
                                                <br>
                                                
                                            </div>
                                            
                                        </div>


                                        <!-- vendor inr end -->

                                        

                                    <?php    }
elseif($currency == "dollar")
{   ?>

    <div class="row" style="font-size: 15px">
                                            
                                            
                                            <div class="col-md-12 pull-left" style="font-weight: bold;">International Vendor/Seller Details</div>
                                            
                                            

                                        </div><br>

                                        


                                        <?php

                                            if(!empty($whtpercentage) and $cur != "rupee")
                                            {



                                        ?>

                                        <!-- with wht start -->

    <div class="row" style="font-size: 15px">
                                            
                                            
                                            <div class="col-md-12 table-responsive">
                                                <table class="table table-bordered table-striped mb-none" >
                                                    <thead>
                                                    <tr>
                                                        <th>Sl No.</th>
                                                        <th>Vendor Name</th>
                                                        <th>Product</th>
                                                        <th>Description</th>
                                                        
                                                        <th>Qty</th>
                                                        <th>Unit Price(<?php echo $cur; ?>)</th>
                                                        <th>Sub Total(<?php echo $cur; ?>)</th>
                                                        <th>Conversion Rate</th>
                                                        <th><?php echo $cur; ?> to INR </th>
                                                        <th>WHT@ <?php echo $whtpercentage; ?> %</th>
                                                        <th>Total Price(INR)</th>

                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    
                                                       <?php 
                                                        $vtotal=0;$total1=0;
                                                        $totalinr1=0;$vtotalinr=0;
                                                        $igsttotal=0;$sgsttotal=0;$cgsttotal=0;$grandtotal=0;$subtotal=0;

                                                        $vgrandtotal=0;$grandtotal=0;
                                                        $subtotal=0;
$w=60;$h=20;

$chk=0;
                                       
                                       for($k=1;$k<=60;$k++)
                                       {
                                          
                                             $m=$k+1;
                                         
                                          
                                          for($l=1;$l<=$k;$l++)
                                          {
                                             $pdsc="pdescription".$l;
                                             $qt="quantity".$l;
                                             $up="vendorprice".$l;
                                             $prod="product".$l;
                                             $vend="vendorname".$l;
  
                                          } 

                                          if(!empty($$prod))
                                             {
                                       
                                       
                                       $partdesc1=$$pdsc;
                                       $qt1=$$qt;
                                       $unitp1=sprintf("%.2f",$$up);
                                       $total1=sprintf("%.2f",$qt1*$unitp1);

                                       

                                       if($opfformname=="multiplevendor")
                                       {
                                         if($k==1)
                                       {
                                        echo '<tr><td>'.$k.'</td><td>'.$vendorname1.'</td><td>'.$$prod.'</td><td>'.$partdesc1.'</td><td>'.$qt1.'</td><td>'.$unitp1.'</td><td>'.$total1.'</td><td>'.$conversionrate.'</td>';
                                       }
                                       else
                                       {
                                        echo '<tr><td>'.$k.'</td><td>'.$$vend.'</td><td>'.$$prod.'</td><td>'.$partdesc1.'</td><td>'.$qt1.'</td><td>'.$unitp1.'</td><td>'.$total1.'</td><td>'.$conversionrate.'</td>';
                                       }

                                       }
                                       else
                                       {
                                        if($k==1)
                                       {
                                        echo '<tr><td>'.$k.'</td><td>'.$vendorname1.'</td><td>'.$$prod.'</td><td>'.$partdesc1.'</td><td>'.$qt1.'</td><td>'.$unitp1.'</td><td>'.$total1.'</td><td>'.$conversionrate.'</td>';
                                       }
                                       else
                                       {
                                        echo '<tr><td>'.$k.'</td><td></td><td>'.$$prod.'</td><td>'.$partdesc1.'</td><td>'.$qt1.'</td><td>'.$unitp1.'</td><td>'.$total1.'</td><td>'.$conversionrate.'</td>';
                                       }
                                       }


                                       
                                       
                                       

                                       $vtotal=sprintf("%.2f",$vtotal+$total1);





                                       $totalinr1=sprintf("%.2f",$conversionrate*$total1);

                                    echo '<td >'.$totalinr1.'</td>';

                                    $vtotalinr=sprintf("%.2f",$vtotalinr+$totalinr1);

                                    $whtvalue1=sprintf("%.2f",($whtpercentage/100)*$totalinr1);

                                    echo '<td >'.$whtvalue1.'</td>';
                                    $grandtotal=sprintf("%.2f",$grandtotal+$whtvalue1);

                                    $subtotal1=sprintf("%.2f",$totalinr1+$whtvalue1);

                                    echo '<td align="center">'.$subtotal1.'</td></tr>';



                                          $subtotal=$subtotal+$subtotal1;
                                       
                                       }
                                       else
                                       {
                                          global $chk;
                                          $chk=$chk+1;
                                       }

                                       
                                          if ($chk>0) {
                                          break;
                                       }

                                       }



    $stax=0;$serviceamount=0;
    if(!empty($servicename) and !empty($servicecost) and !empty($servicetax))
    {
        $stax=($servicetax/100)*$servicecost;
        $serviceamount=$servicecost+$stax;
        echo '<tr><td></td><td></td><td></td><td>Service</td><td colspan="3">Name: '.$servicename.'</td><td>Cost: '.$servicecost.'</td><td >Tax: '.$servicetax.'%</td><td >Tax Value: '.$stax.'</td><td  align="center">'.$serviceamount.'</td></tr>';
    }

    $ftax=0;$freightamount=0;
    if(!empty($freightname) and !empty($freightcost) and !empty($freighttax))
    {
        $ftax=($freighttax/100)*$freightcost;
        $freightamount=$freightcost+$ftax;
        echo '<tr><td></td><td></td><td></td><td>Freight</td><td colspan="3">Name: '.$freightname.'</td><td>Cost: '.$freightcost.'</td><td>Tax: '.$freighttax.'%</td><td >Tax Value: '.$ftax.'</td><td  align="center">'.$freightamount.'</td></tr>';
    }

    


   




$vgrandtax=sprintf("%.2f",$grandtotal+$stax+$ftax);
    $vgrandsub=sprintf("%.2f",$subtotal+$serviceamount+$freightamount);


$vgrandtotal=sprintf("%.2f",$vgrandsub+$serviceamount+$freightamount);









    echo '<tr style="font-weight:bold;text-align:left"><td></td><td></td><td></td><td></td><td></td><td>Sub Total</td><td>'.$vtotalinr.'(INR)</td><td></td><td >Grand Total</td><td >'.$vgrandtax.'</td><td  align="center">'.$vgrandtotal.'</td></tr>';

    if(!empty($deliverymode) and !empty($deliverycharges))
    {
        echo '<tr><td></td><td></td><td></td><td></td><td></td><td >Delivery</td><td>Delivery Mode:</td><td>'.$deliverymode.'</td><td colspan="2">Delivery Charges:</td><td  align="center">'.$deliverycharges.'</td></tr>';
    }

    $gp=0;

    if($customercurrency != "INR")
    {
        $ctotalconverted=$ctotal*$conversionrate;
        $gp=sprintf("%.2f",$ctotalconverted-$vgrandtotal-$deliverycharges);

    }
    else
    {
        $gp=sprintf("%.2f",$ctotal-$vgrandtotal-$deliverycharges);
    }

    

    echo '<tr style="font-weight:bold;text-align:left"><td></td><td></td><td></td><td></td><td colspan="3"></td><td></td><td ></td><td >GP</td><td  align="center">'.$gp.' (INR)</td></tr>';

    mysqli_query($dbc,"update `quotation` set `SubTotal`='$subtotal',`GrandTotal`='$grandsub' where `Funnelid`='$fid' and `QuotNo`='$quotno' and `Company`='$company'  ");

    for($l=1;$l<=10;$l++)
     {
        if(!empty($l))
        {
            $product="product".$l;
            $prd[]=$$product;
        }
        
     }
     

     $requirements=implode(',',$prd);

     mysqli_query($dbc,"insert into `funnel` (`Representive`,`RMail`,`CampaignType`,`Company`,`Sector`,`Products`,`Probability`,`Revenue`,`Stage`,`Services`) values ('$rep','$rmail','Direct','$company','$sector','$requirements','50','$grandsub','Quotation','$service')");






?>



                                                    </tr>
                                                </tbody>
                                                </table>
                                                <br>
                                                
                                            </div>
                                            
                                        </div>
                                        <br><br>

                                        <!-- with wht end -->

                                        <?php
                                            }
                                            else
                                            {

                                        ?>
                                        <!-- without wht start -->
                                        <div class="row" style="font-size: 15px">
                                            
                                            
                                            <div class="col-md-12 table-responsive">
                                                <table class="table table-bordered table-striped mb-none" >
                                                    <thead>
                                                    <tr>
                                                        <th>Sl No.</th>
                                                        <th>Vendor Name</th>
                                                        <th>Product</th>
                                                        <th>Description</th>
                                                        
                                                        <th>Qty</th>
                                                        <th>Unit Price(<?php echo $cur; ?>)</th>
                                                        <th>Sub Total(<?php echo $cur; ?>)</th>

                                                        <th>Conversion Rate</th>
                                                        <th><?php echo $cur; ?> to INR</th>
                                                        
                                                        <th>Total Price(INR)</th>

                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    
                                                       <?php 
                                                       
                                                        $totalinr1=0;$vtotalinr=0;
                                                        $igsttotal=0;$sgsttotal=0;$cgsttotal=0;$grandtotal=0;$subtotal=0;

                                                        $vgrandtotal=0;$grandtotal=0;
                                                        $subtotal=0;
$w=60;$h=20;

$chk=0;
                                       
                                       for($k=1;$k<=60;$k++)
                                       {
                                          
                                             $m=$k+1;
                                         
                                          
                                          for($l=1;$l<=$k;$l++)
                                          {
                                             $pdsc="pdescription".$l;
                                             $qt="quantity".$l;
                                             $up="vendorprice".$l;
                                             $prod="product".$l;
                                             $vend="vendorname".$l;
  
                                          } 

                                          if(!empty($$prod))
                                             {
                                       
                                       
                                       $partdesc1=$$pdsc;
                                       $qt1=$$qt;
                                       $unitp1=sprintf("%.2f",$$up);
                                       $total1=sprintf("%.2f",$qt1*$unitp1);


                                       if($opfformname=="multiplevendor")
                                       {
                                            if($k==1)
                                       {
                                        echo '<tr><td>'.$k.'</td><td>'.$vendorname1.'</td><td>'.$$prod.'</td><td>'.$partdesc1.'</td><td>'.$qt1.'</td><td>'.$unitp1.'</td><td>'.$total1.'</td><td>'.$conversionrate.'</td>';
                                       }
                                       else
                                       {
                                        echo '<tr><td>'.$k.'</td><td>'.$$vend.'</td><td>'.$$prod.'</td><td>'.$partdesc1.'</td><td>'.$qt1.'</td><td>'.$unitp1.'</td><td>'.$total1.'</td><td>'.$conversionrate.'</td>';
                                       }

                                       }
                                       else
                                       {
                                        if($k==1)
                                       {
                                        echo '<tr><td>'.$k.'</td><td>'.$vendorname1.'</td><td>'.$$prod.'</td><td>'.$partdesc1.'</td><td>'.$qt1.'</td><td>'.$unitp1.'</td><td>'.$total1.'</td><td>'.$conversionrate.'</td>';
                                       }
                                       else
                                       {
                                        echo '<tr><td>'.$k.'</td><td></td><td>'.$$prod.'</td><td>'.$partdesc1.'</td><td>'.$qt1.'</td><td>'.$unitp1.'</td><td>'.$total1.'</td><td>'.$conversionrate.'</td>';
                                       }
                                       }

                                       




                                       
                                       
                                       

                                       $vtotal=sprintf("%.2f",$vtotal+$total1);





                                       $totalinr1=sprintf("%.2f",$conversionrate*$total1);

                                    echo '<td >'.$totalinr1.'</td>';

                                    $vtotalinr=sprintf("%.2f",$vtotalinr+$totalinr1);

                                    

                                    $subtotal1=sprintf("%.2f",$totalinr1);

                                    echo '<td align="center">'.$subtotal1.'</td></tr>';



                                          $subtotal=$subtotal+$subtotal1;
                                       
                                       }
                                       else
                                       {
                                          global $chk;
                                          $chk=$chk+1;
                                       }

                                       
                                          if ($chk>0) {
                                          break;
                                       }

                                       }



    $stax=0;$serviceamount=0;
    if(!empty($servicename) and !empty($servicecost) and !empty($servicetax))
    {
        $stax=($servicetax/100)*$servicecost;
        $serviceamount=$servicecost+$stax;
        echo '<tr><td></td><td></td><td>Service</td><td colspan="3">Name: '.$servicename.'</td><td>Cost: '.$servicecost.'</td><td >Tax: '.$servicetax.'%</td><td >Tax Value: '.$stax.'</td><td  align="center">'.$serviceamount.'</td></tr>';
    }

    $ftax=0;$freightamount=0;
    if(!empty($freightname) and !empty($freightcost) and !empty($freighttax))
    {
        $ftax=($freighttax/100)*$freightcost;
        $freightamount=$freightcost+$ftax;
        echo '<tr><td></td><td></td><td>Freight</td><td colspan="3">Name: '.$freightname.'</td><td>Cost: '.$freightcost.'</td><td>Tax: '.$freighttax.'%</td><td >Tax Value: '.$ftax.'</td><td  align="center">'.$freightamount.'</td></tr>';
    }

    


    




$vgrandtax=sprintf("%.2f",$stax+$ftax);
    $vgrandsub=sprintf("%.2f",$subtotal+$serviceamount+$freightamount);


$vgrandtotal=sprintf("%.2f",$vgrandsub+$serviceamount+$freightamount);



    echo '<tr style="font-weight:bold;text-align:left"><td></td><td></td><td></td><td></td><td></td><td>Sub Total</td><td>'.$vtotalinr.'(INR)</td><td ></td><td >Grand Total</td><td  align="center">'.$vgrandtotal.'</td></tr>';

    if(!empty($deliverymode) and !empty($deliverycharges))
    {
        echo '<tr><td></td><td></td><td></td><td></td><td></td><td >Delivery</td><td>Delivery Mode:</td><td>'.$deliverymode.'</td><td >Delivery Charges:</td><td  align="center">'.$deliverycharges.'</td></tr>';
    }

    $gp=0;

    //$gp=sprintf("%.2f",$ctotal-$vgrandtotal-$deliverycharges);

    if($customercurrency != "INR")
    {
        $ctotalconverted=$ctotal*$conversionrate;
        $gp=sprintf("%.2f",$ctotalconverted-$vgrandtotal-$deliverycharges);

    }
    else
    {
        $gp=sprintf("%.2f",$ctotal-$vgrandtotal-$deliverycharges);
    }


    echo '<tr style="font-weight:bold;text-align:left"><td></td><td></td><td></td><td colspan="3"></td><td></td><td ></td><td >GP</td><td  align="center">'.$gp.' (INR)</td></tr>';


    mysqli_query($dbc,"update `quotation` set `SubTotal`='$subtotal',`GrandTotal`='$grandsub' where `Funnelid`='$fid' and `QuotNo`='$quotno' and `Company`='$company'  ");

    for($l=1;$l<=10;$l++)
     {
        if(!empty($l))
        {
            $product="product".$l;
            $prd[]=$$product;
        }
        
     }
     

     $requirements=implode(',',$prd);

     mysqli_query($dbc,"insert into `funnel` (`Representive`,`RMail`,`CampaignType`,`Company`,`Sector`,`Products`,`Probability`,`Revenue`,`Stage`,`Services`) values ('$rep','$rmail','Direct','$company','$sector','$requirements','50','$grandsub','Quotation','$service')");






?>



                                                    </tr>
                                                </tbody>
                                                </table>
                                                <br>
                                                
                                            </div>
                                            
                                        </div>
                                        <br><br>
                                        <!-- without wht end -->
                                        <?php
                                            }
                                        ?>
                                        <!-- vendor detail end -->

                                        <!-- gross profit detail start -->

                                        
                                        <!-- gross profit detail end -->


<?php } 

 }/*multiple tax end*/
 elseif($quotformname=="licence")
 {

 }
 else
 {
    
$chk=0;
                                            for($k=1;$k<=60;$k++)
                                       {
                                          
                                             $m=$k+1;
                                         
                                          
                                          for($l=1;$l<=$k;$l++)
                                          {
                                             $pdsc="pdescription".$l;
                                             $qt="quantity".$l;
                                             $up="unitprice".$l;
                                             $prod="product".$l;

                                             
                                          } 

                                          if(!empty($$prod))
                                             {
                                       
                                       
                                       $partdesc1=$$pdsc;
                                       $qt1=$$qt;
                                       $unitp1=sprintf("%.2f",$$up);
                                       $total1=sprintf("%.2f",$qt1*$unitp1);

                                       if($k == 1)
                                       {
                                        echo '<tr><td>'.$k.'</td><td>'.$company.'</td><td>'.$$prod.'</td><td>'.$partdesc1.'</td><td>'.$qt1.'</td><td>'.$unitp1.'</td><td>'.$total1.'</td>';

                                       }
                                       else
                                       {
                                             echo '<tr><td>'.$k.'</td><td></td><td>'.$$prod.'</td><td>'.$partdesc1.'</td><td>'.$qt1.'</td><td>'.$unitp1.'</td><td>'.$total1.'</td>';
                                       }
                                       
                                       

                                       $ctotal=sprintf("%.2f",$ctotal+$total1);
                                       
                                       
                                       $array =  explode(',', $tax);
                                       $count=0;
                                       $grandtotal1=0;
                                       $subtotal1=0;
                                       foreach ($array as $item) {
                                       global $count;
                                       $count=$count+1;
                                       }
                                       
                                       foreach ($array as $item) {
                                       
                                       
                                       $taxperarr =  explode('-', $item);
                                       
                                       foreach ($taxperarr as $per) {
                                       $taxpercentage=$per;
                                       
                                       }
                                       
                                       $totalwtax1=sprintf("%.2f",($taxpercentage/100)*$total1);
                                       
                                       if($count>1)
                                       {
                                       
                                       echo '<td>'.$taxperarr[0].' '.$taxpercentage.'%'.'</td>';
                                       
                                       }
                                       else
                                       {
                                       echo '<td colspan="2">'.$taxperarr[0].' '.$taxpercentage.'%'.'</td>';
                                                                              
                                       }
                                       
                                       
                                       $grandtotal1=$grandtotal1+$totalwtax1;
                                       
                                       
                                       }
                                       
                                       if($count>1)
                                       {
                                       
                                       echo '<td>'.$totalwtax1.'</td>';
                                       echo '<td>'.$totalwtax1.'</td>';
                                       
                                       
                                       }
                                       else
                                       {
                                       
                                       echo '<td colspan="2">'.$totalwtax1.'</td>';
                                       
                                       
                                       }

                                       $grandtotal=$grandtotal+$grandtotal1;
                                       
                                       
                                       $subtotal1=sprintf("%.2f",$grandtotal1+$total1);
                                       
                                       echo '<td align="center">'.$subtotal1.'</td></tr>';

                                          $subtotal=$subtotal+$subtotal1;
                                       
                                       }
                                       else
                                       {
                                          global $chk;
                                          $chk=$chk+1;
                                       }

                                       
                                          if ($chk>0) {
                                          break;
                                       }

                                       }


    $stax=0;$serviceamount=0;
    if(!empty($servicename) and !empty($servicecost) and !empty($servicetax))
    {
        $stax=($servicetax/100)*$servicecost;
        $serviceamount=$servicecost+$stax;
        echo '<tr><td></td><td></td><td>Service</td><td colspan="3">Name: '.$servicename.'</td><td>Cost: '.$servicecost.'</td><td colspan="2">Tax: '.$servicetax.'%</td><td colspan="2" align="center">'.$stax.'</td><td  align="center">'.$serviceamount.'</td></tr>';
    }

    $ftax=0;$freightamount=0;
    if(!empty($freightname) and !empty($freightcost) and !empty($freighttax))
    {
        $ftax=($freighttax/100)*$freightcost;
        $freightamount=$freightcost+$ftax;
        echo '<tr><td></td><td></td><td>Freight</td><td colspan="3">Name: '.$freightname.'</td><td>Cost: '.$freightcost.'</td><td colspan="2">Tax: '.$freighttax.'%</td><td colspan="2" align="center">'.$ftax.'</td><td  align="center">'.$freightamount.'</td></tr>';
    }

    /*if(!empty($deliverymode) and !empty($deliverycharges))
    {
        echo '<tr><td></td><td></td><td></td><td></td><td >Delivery</td><td>Delivery Mode:</td><td >'.$deliverymode.'</td><td colspan="2">Delivery Charges:</td><td colspan="2"></td><td  align="center">'.$deliverycharges.'</td></tr>';
    }
*/

    $grandtax=sprintf("%.2f",$grandtotal+$stax+$ftax);
    $grandsub=sprintf("%.2f",$subtotal+$serviceamount+$freightamount);

//$ctotal=sprintf("%.2f",$total1+$total2+$total3+$total4+$total5+$total6+$total7+$total8+$total9+$total10);



    echo '<tr style="font-weight:bold"><td></td><td></td><td></td><td></td><td></td><td>Sub Total</td><td>'.$ctotal.'</td><td colspan="2">Grand Total</td><td colspan="2" align="center">'.$grandtax.'</td><td  align="center">'.$grandsub.'</td></tr>';

    mysqli_query($dbc,"update `quotation` set `GrandTotal`='$grandsub' where  `QuotNo`='$quotno' and `RMail`='$id'  ");

    mysqli_query($dbc,"update `opf` set `CustomerGrandTotal`='$grandsub' where  `OPFNo`='$opfno' and `RMail`='$id' ");



    for($l=1;$l<=10;$l++)
     {
        if(!empty($l))
        {
            $product="product".$l;
            $prd[]=$$product;
        }
        
     }
     

     /*$requirements=implode(',',$prd);

     mysqli_query($dbc,"insert into `funnel` (`Representive`,`RMail`,`CampaignType`,`Company`,`Sector`,`Products`,`Probability`,`Revenue`,`Stage`,`Services`) values ('$rep','$rmail','Direct','$company','$sector','$requirements','50','$grandsub','Quotation','$service')");
*/





?>



                                                    </tr>
                                                </tbody>
                                                </table>
                                            </div>
                                            
                                        </div><br><br>



                                        <!-- customer detail end -->
                                        <!-- vendor detail start -->

                                        
<?php 
$currency=$opfcurrency; 
if($currency == "rupee")
{
    $cur="INR";
}
elseif($currency == "dollar")
{
    $cur="USD";
}
elseif ($currency == "euro") {
    $cur="EUR";
}
elseif ($currency == "pound") {
    $cur="GBP";
}
if($currency == "rupee")
{

?>
                                       <div class="row" style="font-size: 15px">
                                            
                                            
                                            <div class="col-md-12 pull-left" style="font-weight: bold;">Vendor/Seller Details</div>
                                            
                                            

                                        </div><br>

                                        

                                        <div class="row" style="font-size: 15px">
                                            
                                            
                                            <div class="col-md-12 table-responsive">
                                                <table class="table table-bordered table-striped mb-none" >
                                                    <thead>
                                                    <tr>
                                                        <th>Sl No.</th>
                                                        <th>Vendor Name</th>
                                                        <th>Product</th>
                                                        <th>Description</th>
                                                        
                                                        <th>Qty</th>
                                                        <th>Unit Price(<?php echo $cur; ?>)</th>
                                                        <th>Sub Total(<?php echo $cur; ?>)</th>
                                                        <th colspan="2" align="center">GST Rate</th>
                                                        <th colspan="2" align="center">GST Amount</th>
                                                        <th>Total Price(<?php echo $cur; ?>)</th>

                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    
                                                       <?php 
                                                        $vtotal=0;$total1=0;$igsttotal=0;$sgsttotal=0;$cgsttotal=0;$grandtotal=0;$subtotal=0;
                                                        $grandtotal1=0;

$w=60;$h=20;

$chk=0;
                                       
                                       for($k=1;$k<=60;$k++)
                                       {
                                          
                                             $m=$k+1;
                                         
                                          
                                          for($l=1;$l<=$k;$l++)
                                          {
                                             $pdsc="pdescription".$l;
                                             $qt="quantity".$l;
                                             $up="vendorprice".$l;
                                             $prod="product".$l;
                                             $vend="vendorname".$l;
  
                                          } 

                                          if(!empty($$prod))
                                             {
                                       
                                       
                                       $partdesc1=$$pdsc;
                                       $qt1=$$qt;
                                       $unitp1=sprintf("%.2f",$$up);
                                       $total1=sprintf("%.2f",$qt1*$unitp1);

                                       

                                       if($opfformname=="multiplevendor")
                                       {
                                        if($k==1)
                                       {
                                        echo '<tr><td>'.$k.'</td><td>'.$vendorname1.'</td><td>'.$$prod.'</td><td>'.$partdesc1.'</td><td>'.$qt1.'</td><td>'.$unitp1.'</td><td>'.$total1.'</td>';
                                       }
                                       else
                                       {
                                        echo '<tr><td>'.$k.'</td><td>'.$$vend.'</td><td>'.$$prod.'</td><td>'.$partdesc1.'</td><td>'.$qt1.'</td><td>'.$unitp1.'</td><td>'.$total1.'</td>';
                                       }
                                       }
                                       else
                                       {
                                        if($k==1)
                                       {
                                        echo '<tr><td>'.$k.'</td><td>'.$vendorname1.'</td><td>'.$$prod.'</td><td>'.$partdesc1.'</td><td>'.$qt1.'</td><td>'.$unitp1.'</td><td>'.$total1.'</td>';
                                       }
                                       else
                                       {
                                        echo '<tr><td>'.$k.'</td><td></td><td>'.$$prod.'</td><td>'.$partdesc1.'</td><td>'.$qt1.'</td><td>'.$unitp1.'</td><td>'.$total1.'</td>';
                                       }
                                       }
                                       
                                       

                                       $vtotal=sprintf("%.2f",$vtotal+$total1);
                                       
                                       
                                       $array =  explode(',', $tax);
                                       $count=0;
                                       $grandtotal1=0;
                                       $subtotal1=0;
                                       foreach ($array as $item) {
                                       global $count;
                                       $count=$count+1;
                                       }
                                       
                                       foreach ($array as $item) {
                                       
                                       
                                       $taxperarr =  explode('-', $item);
                                       
                                       foreach ($taxperarr as $per) {
                                       $taxpercentage=$per;
                                       
                                       }
                                       
                                       $totalwtax1=sprintf("%.2f",($taxpercentage/100)*$total1);
                                       
                                       if($count>1)
                                       {
                                       
                                       echo '<td>'.$taxperarr[0].' '.$taxpercentage.'%'.'</td>';
                                       
                                       }
                                       else
                                       {
                                       echo '<td colspan="2">'.$taxperarr[0].' '.$taxpercentage.'%'.'</td>';
                                                                              
                                       }
                                       
                                       
                                       $grandtotal1=$grandtotal1+$totalwtax1;
                                       
                                       
                                       }
                                       
                                       if($count>1)
                                       {
                                       
                                       echo '<td>'.$totalwtax1.'</td>';
                                       echo '<td>'.$totalwtax1.'</td>';
                                       
                                       
                                       }
                                       else
                                       {
                                       
                                       echo '<td colspan="2">'.$totalwtax1.'</td>';
                                       
                                       
                                       }

                                       $grandtotal=$grandtotal+$grandtotal1;
                                       
                                       
                                       $subtotal1=sprintf("%.2f",$grandtotal1+$total1);
                                       
                                       echo '<td align="center">'.$subtotal1.'</td></tr>';

                                          $subtotal=$subtotal+$subtotal1;
                                       
                                       }
                                       else
                                       {
                                          global $chk;
                                          $chk=$chk+1;
                                       }

                                       
                                          if ($chk>0) {
                                          break;
                                       }

                                       }




    $stax=0;$serviceamount=0;
    if(!empty($servicename) and !empty($servicecost) and !empty($servicetax))
    {
        $stax=($servicetax/100)*$servicecost;
        $serviceamount=$servicecost+$stax;
        echo '<tr><td></td><td></td><td>Service</td><td colspan="3">Name: '.$servicename.'</td><td>Cost: '.$servicecost.'</td><td colspan="2">Tax: '.$servicetax.'%</td><td colspan="2" align="center">'.$stax.'</td><td  align="center">'.$serviceamount.'</td></tr>';
    }

    $ftax=0;$freightamount=0;
    if(!empty($freightname) and !empty($freightcost) and !empty($freighttax))
    {
        $ftax=($freighttax/100)*$freightcost;
        $freightamount=$freightcost+$ftax;
        echo '<tr><td></td><td></td><td>Freight</td><td colspan="3">Name: '.$freightname.'</td><td>Cost: '.$freightcost.'</td><td colspan="2">Tax: '.$freighttax.'%</td><td colspan="2" align="center">'.$ftax.'</td><td  align="center">'.$freightamount.'</td></tr>';
    }


    $vgrandtax=sprintf("%.2f",$grandtotal+$stax+$ftax);
    $vgrandsub=sprintf("%.2f",$subtotal+$serviceamount+$freightamount);

//$vtotal=sprintf("%.2f",$total1+$total2+$total3+$total4+$total5+$total6+$total7+$total8+$total9+$total10);



    echo '<tr style="font-weight:bold"><td></td><td></td><td></td><td></td><td></td><td>Sub Total</td><td>'.$vtotal.'</td><td colspan="2">Grand Total</td><td colspan="2" align="center">'.$vgrandtax.'</td><td  align="center">'.$vgrandsub.'</td></tr>';

    if(!empty($deliverymode) and !empty($deliverycharges))
    {
        echo '<tr><td></td><td></td><td></td><td></td><td></td><td >Delivery</td><td>Delivery Mode:</td><td colspan="2">'.$deliverymode.'</td><td colspan="2">Delivery Charges:</td><td  align="center">'.$deliverycharges.'</td></tr>';
    }

    $gp=0;
    $gp=sprintf("%.2f",$ctotal-$vtotal-$deliverycharges); 

    echo '<tr style="font-weight:bold;text-align:left"><td></td><td></td><td></td><td colspan="3"></td><td></td><td colspan="2"></td><td colspan="2" align="center">GP</td><td  align="center">'.$gp.'</td></tr>';


    mysqli_query($dbc,"update `quotation` set `SubTotal`='$subtotal',`GrandTotal`='$grandsub' where `Funnelid`='$fid' and `QuotNo`='$quotno' and `Company`='$company'  ");

    for($l=1;$l<=10;$l++)
     {
        if(!empty($l))
        {
            $product="product".$l;
            $prd[]=$$product;
        }
        
     }
     

     $requirements=implode(',',$prd);

     mysqli_query($dbc,"update `funnel` (`Representive`,`RMail`,`CampaignType`,`Company`,`Sector`,`Products`,`Probability`,`Revenue`,`Stage`,`Services`) values ('$rep','$rmail','Direct','$company','$sector','$requirements','50','$grandsub','Quotation','$service')");






?>



                                                    </tr>
                                                </tbody>
                                                </table>
                                                <br>
                                                
                                            </div>
                                            
                                        </div>


                                        <!-- vendor inr end -->

                                        

                                    <?php    }
elseif($currency == "dollar")
{   ?>

    <div class="row" style="font-size: 15px">
                                            
                                            
                                            <div class="col-md-12 pull-left" style="font-weight: bold;">International Vendor/Seller Details</div>
                                            
                                            

                                        </div><br>

                                        


                                        <?php

                                            if(!empty($whtpercentage) and $cur != "rupee")
                                            {



                                        ?>

                                        <!-- with wht start -->

    <div class="row" style="font-size: 15px">
                                            
                                            
                                            <div class="col-md-12 table-responsive">
                                                <table class="table table-bordered table-striped mb-none" >
                                                    <thead>
                                                    <tr>
                                                        <th>Sl No.</th>
                                                        <th>Vendor Name</th>
                                                        <th>Product</th>
                                                        <th>Description</th>
                                                        
                                                        <th>Qty</th>
                                                        <th>Unit Price(<?php echo $cur; ?>)</th>
                                                        <th>Sub Total(<?php echo $cur; ?>)</th>
                                                        <th>Conversion Rate</th>
                                                        <th><?php echo $cur; ?> to INR </th>
                                                        <th>WHT@ <?php echo $whtpercentage; ?> %</th>
                                                        <th>Total Price(INR)</th>

                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    
                                                       <?php 
                                                        $vtotal=0;$total1=0;
                                                        $totalinr1=0;$vtotalinr=0;
                                                        $igsttotal=0;$sgsttotal=0;$cgsttotal=0;$grandtotal=0;$subtotal=0;

                                                        $vgrandtotal=0;$grandtotal=0;
                                                        $subtotal=0;
$w=60;$h=20;

$chk=0;
                                       
                                       for($k=1;$k<=60;$k++)
                                       {
                                          
                                             $m=$k+1;
                                         
                                          
                                          for($l=1;$l<=$k;$l++)
                                          {
                                             $pdsc="pdescription".$l;
                                             $qt="quantity".$l;
                                             $up="vendorprice".$l;
                                             $prod="product".$l;
                                             $vend="vendorname".$l;
  
                                          } 

                                          if(!empty($$prod))
                                             {
                                       
                                       
                                       $partdesc1=$$pdsc;
                                       $qt1=$$qt;
                                       $unitp1=sprintf("%.2f",$$up);
                                       $total1=sprintf("%.2f",$qt1*$unitp1);

                                       if($opfformname=="multiplevendor")
                                       {
                                        if($k==1)
                                       {
                                        echo '<tr><td>'.$k.'</td><td>'.$vendorname1.'</td><td>'.$$prod.'</td><td>'.$partdesc1.'</td><td>'.$qt1.'</td><td>'.$unitp1.'</td><td>'.$total1.'</td><td>'.$conversionrate.'</td>';
                                       }
                                       else
                                       {
                                        echo '<tr><td>'.$k.'</td><td>'.$$vend.'</td><td>'.$$prod.'</td><td>'.$partdesc1.'</td><td>'.$qt1.'</td><td>'.$unitp1.'</td><td>'.$total1.'</td><td>'.$conversionrate.'</td>';
                                       }
                                       }
                                       else
                                       {
                                        if($k==1)
                                       {
                                        echo '<tr><td>'.$k.'</td><td>'.$vendorname1.'</td><td>'.$$prod.'</td><td>'.$partdesc1.'</td><td>'.$qt1.'</td><td>'.$unitp1.'</td><td>'.$total1.'</td><td>'.$conversionrate.'</td>';
                                       }
                                       else
                                       {
                                        echo '<tr><td>'.$k.'</td><td></td><td>'.$$prod.'</td><td>'.$partdesc1.'</td><td>'.$qt1.'</td><td>'.$unitp1.'</td><td>'.$total1.'</td><td>'.$conversionrate.'</td>';
                                       }
                                       }

                                       


                                       
                                       
                                       

                                       $vtotal=sprintf("%.2f",$vtotal+$total1);





                                       $totalinr1=sprintf("%.2f",$conversionrate*$total1);

                                    echo '<td >'.$totalinr1.'</td>';

                                    $vtotalinr=sprintf("%.2f",$vtotalinr+$totalinr1);

                                    $whtvalue1=sprintf("%.2f",($whtpercentage/100)*$totalinr1);

                                    echo '<td >'.$whtvalue1.'</td>';
                                    $grandtotal=sprintf("%.2f",$grandtotal+$whtvalue1);

                                    $subtotal1=sprintf("%.2f",$totalinr1+$whtvalue1);

                                    echo '<td align="center">'.$subtotal1.'</td></tr>';



                                          $subtotal=$subtotal+$subtotal1;
                                       
                                       }
                                       else
                                       {
                                          global $chk;
                                          $chk=$chk+1;
                                       }

                                       
                                          if ($chk>0) {
                                          break;
                                       }

                                       }



    $stax=0;$serviceamount=0;
    if(!empty($servicename) and !empty($servicecost) and !empty($servicetax))
    {
        $stax=($servicetax/100)*$servicecost;
        $serviceamount=$servicecost+$stax;
        echo '<tr><td></td><td></td><td></td><td>Service</td><td colspan="3">Name: '.$servicename.'</td><td>Cost: '.$servicecost.'</td><td >Tax: '.$servicetax.'%</td><td >Tax Value: '.$stax.'</td><td  align="center">'.$serviceamount.'</td></tr>';
    }

    $ftax=0;$freightamount=0;
    if(!empty($freightname) and !empty($freightcost) and !empty($freighttax))
    {
        $ftax=($freighttax/100)*$freightcost;
        $freightamount=$freightcost+$ftax;
        echo '<tr><td></td><td></td><td></td><td>Freight</td><td colspan="3">Name: '.$freightname.'</td><td>Cost: '.$freightcost.'</td><td>Tax: '.$freighttax.'%</td><td >Tax Value: '.$ftax.'</td><td  align="center">'.$freightamount.'</td></tr>';
    }

    


   




$vgrandtax=sprintf("%.2f",$grandtotal+$stax+$ftax);
    $vgrandsub=sprintf("%.2f",$subtotal+$serviceamount+$freightamount);


$vgrandtotal=sprintf("%.2f",$vgrandsub+$serviceamount+$freightamount);









    echo '<tr style="font-weight:bold;text-align:left"><td></td><td></td><td></td><td></td><td></td><td>Sub Total</td><td>'.$vtotalinr.'(INR)</td><td></td><td >Grand Total</td><td >'.$vgrandtax.'</td><td  align="center">'.$vgrandtotal.'</td></tr>';

    if(!empty($deliverymode) and !empty($deliverycharges))
    {
        echo '<tr><td></td><td></td><td></td><td></td><td></td><td >Delivery</td><td>Delivery Mode:</td><td>'.$deliverymode.'</td><td colspan="2">Delivery Charges:</td><td  align="center">'.$deliverycharges.'</td></tr>';
    }

    $gp=0;

    if($customercurrency != "INR")
    {
        $ctotalconverted=$ctotal*$conversionrate;
        $gp=sprintf("%.2f",$ctotalconverted-$vgrandtotal-$deliverycharges);

    }
    else
    {
        $gp=sprintf("%.2f",$ctotal-$vgrandtotal-$deliverycharges);
    }

    

    echo '<tr style="font-weight:bold;text-align:left"><td></td><td></td><td></td><td></td><td colspan="3"></td><td></td><td ></td><td >GP</td><td  align="center">'.$gp.' (INR)</td></tr>';

    mysqli_query($dbc,"update `quotation` set `SubTotal`='$subtotal',`GrandTotal`='$grandsub' where `Funnelid`='$fid' and `QuotNo`='$quotno' and `Company`='$company'  ");

    for($l=1;$l<=10;$l++)
     {
        if(!empty($l))
        {
            $product="product".$l;
            $prd[]=$$product;
        }
        
     }
     

     $requirements=implode(',',$prd);

     mysqli_query($dbc,"insert into `funnel` (`Representive`,`RMail`,`CampaignType`,`Company`,`Sector`,`Products`,`Probability`,`Revenue`,`Stage`,`Services`) values ('$rep','$rmail','Direct','$company','$sector','$requirements','50','$grandsub','Quotation','$service')");






?>



                                                    </tr>
                                                </tbody>
                                                </table>
                                                <br>
                                                
                                            </div>
                                            
                                        </div>
                                        <br><br>

                                        <!-- with wht end -->

                                        <?php
                                            }
                                            else
                                            {

                                        ?>
                                        <!-- without wht start -->
                                        <div class="row" style="font-size: 15px">
                                            
                                            
                                            <div class="col-md-12 table-responsive">
                                                <table class="table table-bordered table-striped mb-none" >
                                                    <thead>
                                                    <tr>
                                                        <th>Sl No.</th>
                                                        <th>Vendor Name</th>
                                                        <th>Product</th>
                                                        <th>Description</th>
                                                        
                                                        <th>Qty</th>
                                                        <th>Unit Price(<?php echo $cur; ?>)</th>
                                                        <th>Sub Total(<?php echo $cur; ?>)</th>

                                                        <th>Conversion Rate</th>
                                                        <th><?php echo $cur; ?> to INR</th>
                                                        
                                                        <th>Total Price(INR)</th>

                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    
                                                       <?php 
                                                       
                                                        $totalinr1=0;$vtotalinr=0;
                                                        $igsttotal=0;$sgsttotal=0;$cgsttotal=0;$grandtotal=0;$subtotal=0;

                                                        $vgrandtotal=0;$grandtotal=0;
                                                        $subtotal=0;
$w=60;$h=20;

$chk=0;
                                       
                                       for($k=1;$k<=60;$k++)
                                       {
                                          
                                             $m=$k+1;
                                         
                                          
                                          for($l=1;$l<=$k;$l++)
                                          {
                                             $pdsc="pdescription".$l;
                                             $qt="quantity".$l;
                                             $up="vendorprice".$l;
                                             $prod="product".$l;
                                             $vend="vendorname".$l;
  
                                          } 

                                          if(!empty($$prod))
                                             {
                                       
                                       
                                       $partdesc1=$$pdsc;
                                       $qt1=$$qt;
                                       $unitp1=sprintf("%.2f",$$up);
                                       $total1=sprintf("%.2f",$qt1*$unitp1);

                                       

                                       if($opfformname=="multiplevendor")
                                       {
                                        if($k==1)
                                       {
                                        echo '<tr><td>'.$k.'</td><td>'.$vendorname1.'</td><td>'.$$prod.'</td><td>'.$partdesc1.'</td><td>'.$qt1.'</td><td>'.$unitp1.'</td><td>'.$total1.'</td><td>'.$conversionrate.'</td>';
                                       }
                                       else
                                       {
                                        echo '<tr><td>'.$k.'</td><td>'.$$vend.'</td><td>'.$$prod.'</td><td>'.$partdesc1.'</td><td>'.$qt1.'</td><td>'.$unitp1.'</td><td>'.$total1.'</td><td>'.$conversionrate.'</td>';
                                       }
                                       }
                                       else
                                       {
                                        if($k==1)
                                       {
                                        echo '<tr><td>'.$k.'</td><td>'.$vendorname1.'</td><td>'.$$prod.'</td><td>'.$partdesc1.'</td><td>'.$qt1.'</td><td>'.$unitp1.'</td><td>'.$total1.'</td><td>'.$conversionrate.'</td>';
                                       }
                                       else
                                       {
                                        echo '<tr><td>'.$k.'</td><td></td><td>'.$$prod.'</td><td>'.$partdesc1.'</td><td>'.$qt1.'</td><td>'.$unitp1.'</td><td>'.$total1.'</td><td>'.$conversionrate.'</td>';
                                       }
                                       }


                                       
                                       
                                       

                                       $vtotal=sprintf("%.2f",$vtotal+$total1);





                                       $totalinr1=sprintf("%.2f",$conversionrate*$total1);

                                    echo '<td >'.$totalinr1.'</td>';

                                    $vtotalinr=sprintf("%.2f",$vtotalinr+$totalinr1);

                                    

                                    $subtotal1=sprintf("%.2f",$totalinr1);

                                    echo '<td align="center">'.$subtotal1.'</td></tr>';



                                          $subtotal=$subtotal+$subtotal1;
                                       
                                       }
                                       else
                                       {
                                          global $chk;
                                          $chk=$chk+1;
                                       }

                                       
                                          if ($chk>0) {
                                          break;
                                       }

                                       }



    $stax=0;$serviceamount=0;
    if(!empty($servicename) and !empty($servicecost) and !empty($servicetax))
    {
        $stax=($servicetax/100)*$servicecost;
        $serviceamount=$servicecost+$stax;
        echo '<tr><td></td><td></td><td>Service</td><td colspan="3">Name: '.$servicename.'</td><td>Cost: '.$servicecost.'</td><td >Tax: '.$servicetax.'%</td><td >Tax Value: '.$stax.'</td><td  align="center">'.$serviceamount.'</td></tr>';
    }

    $ftax=0;$freightamount=0;
    if(!empty($freightname) and !empty($freightcost) and !empty($freighttax))
    {
        $ftax=($freighttax/100)*$freightcost;
        $freightamount=$freightcost+$ftax;
        echo '<tr><td></td><td></td><td>Freight</td><td colspan="3">Name: '.$freightname.'</td><td>Cost: '.$freightcost.'</td><td>Tax: '.$freighttax.'%</td><td >Tax Value: '.$ftax.'</td><td  align="center">'.$freightamount.'</td></tr>';
    }

    


    




$vgrandtax=sprintf("%.2f",$stax+$ftax);
    $vgrandsub=sprintf("%.2f",$subtotal+$serviceamount+$freightamount);


$vgrandtotal=sprintf("%.2f",$vgrandsub+$serviceamount+$freightamount);



    echo '<tr style="font-weight:bold;text-align:left"><td></td><td></td><td></td><td></td><td></td><td>Sub Total</td><td>'.$vtotalinr.'(INR)</td><td ></td><td >Grand Total</td><td  align="center">'.$vgrandtotal.'</td></tr>';

    if(!empty($deliverymode) and !empty($deliverycharges))
    {
        echo '<tr><td></td><td></td><td></td><td></td><td></td><td >Delivery</td><td>Delivery Mode:</td><td>'.$deliverymode.'</td><td >Delivery Charges:</td><td  align="center">'.$deliverycharges.'</td></tr>';
    }

    $gp=0;

    //$gp=sprintf("%.2f",$ctotal-$vgrandtotal-$deliverycharges);

    if($customercurrency != "INR")
    {
        $ctotalconverted=$ctotal*$conversionrate;
        $gp=sprintf("%.2f",$ctotalconverted-$vgrandtotal-$deliverycharges);

    }
    else
    {
        $gp=sprintf("%.2f",$ctotal-$vgrandtotal-$deliverycharges);
    }


    echo '<tr style="font-weight:bold;text-align:left"><td></td><td></td><td></td><td colspan="3"></td><td></td><td ></td><td >GP</td><td  align="center">'.$gp.' (INR)</td></tr>';


    mysqli_query($dbc,"update `quotation` set `SubTotal`='$subtotal',`GrandTotal`='$grandsub' where `Funnelid`='$fid' and `QuotNo`='$quotno' and `Company`='$company'  ");

    for($l=1;$l<=10;$l++)
     {
        if(!empty($l))
        {
            $product="product".$l;
            $prd[]=$$product;
        }
        
     }
     

     $requirements=implode(',',$prd);

     mysqli_query($dbc,"insert into `funnel` (`Representive`,`RMail`,`CampaignType`,`Company`,`Sector`,`Products`,`Probability`,`Revenue`,`Stage`,`Services`) values ('$rep','$rmail','Direct','$company','$sector','$requirements','50','$grandsub','Quotation','$service')");


?>



                                                    </tr>
                                                </tbody>
                                                </table>
                                                <br>
                                                
                                            </div>
                                            
                                        </div>
                                        <br><br>
                                        <!-- without wht end -->
                                        <?php
                                            }
                                        ?>
                                        <!-- vendor detail end -->

                                        <!-- gross profit detail start -->

                                        
                                        <!-- gross profit detail end -->



<?php } 

    
 }

    

  ?>

                                    </tbody>
                              </table>
                           </div>
                               </div>  

                        <!-- multiple vendor end -->



                        <div class="row" style="font-size: 15px">
<div class="col-md-12 pull-left" ><b>End User Name: </b><?php echo $endusername; ?></div>
<div class="col-md-12 pull-left" ><b>End User EMail Id: </b><?php echo $enduseremailid; ?></div>
</div><br>
                        



                        
                        
                        <div class="row" style="font-size: 16px">
                           
                           
                           <div class="col-md-12" style="text-align: center;">
                              <?php echo '<div class="dropdown">
                                 ';
                                 for($i=1;$i<=100;$i++)
                                 {
                                     $qp="quotpath".$i;
                                     if(!empty($$qp))
                                     {
                                        echo '<a href="../../uploadfiles/quotation/'.$$qp.'" target="_blank" class="btn btn-warning">R'.$i.' </a> ';
                                     }
                                 }
                                 
                                         
                                    echo ' </div>';
                                 
                                     ?>
                              <?php
                              if(!empty($otherfile))
                              {
                                ?>
                                <a href="../../uploadfiles/quotation/<?php echo urldecode($otherfile); ?>" target="_blank" class="btn btn-warning">Other File</a>
                                <?php
                              }
                                ?>
                                <a href="opfupdate.php?o=<?php echo $opfno; ?>&otype=<?php echo $otype; ?>" target="_blank" class="btn btn-danger">Edit</a> <a href="TCPDF/crm/opf.php?o=<?php echo $opfno; ?>" target="_blank" class="btn btn-primary">View & Send PDF</a>
                           </div>
                           
                        </div>

                              <!-- end quotverify content -->
                           </div>
                        </div>
                        <!-- End Example Basic Form -->
                     </div>
                  </div>
               </div>
            </div>

      </div>
    </div>
    <!-- End Page -->


    <!-- Footer -->
    <?php include "includes/footer.php"; ?>
    <!-- Core  -->
    <script src="../../assets/global/vendor/babel-external-helpers/babel-external-helpers.js"></script>
    <script src="../../assets/global/vendor/jquery/jquery.js"></script>
    <script src="../../assets/global/vendor/popper-js/umd/popper.min.js"></script>
    <script src="../../assets/global/vendor/bootstrap/bootstrap.js"></script>
    <script src="../../assets/global/vendor/animsition/animsition.js"></script>
    <script src="../../assets/global/vendor/mousewheel/jquery.mousewheel.js"></script>
    <script src="../../assets/global/vendor/asscrollbar/jquery-asScrollbar.js"></script>
    <script src="../../assets/global/vendor/asscrollable/jquery-asScrollable.js"></script>
    <script src="../../assets/global/vendor/waves/waves.js"></script>
    
    <!-- Plugins -->
    <script src="../../assets/global/vendor/jquery-mmenu/jquery.mmenu.min.all.js"></script>
    <script src="../../assets/global/vendor/switchery/switchery.js"></script>
    <script src="../../assets/global/vendor/intro-js/intro.js"></script>
    <script src="../../assets/global/vendor/screenfull/screenfull.js"></script>
    <script src="../../assets/global/vendor/slidepanel/jquery-slidePanel.js"></script>
        <script src="../../assets/global/vendor/chartist/chartist.min.js"></script>
        <script src="../../assets/global/vendor/chartist-plugin-tooltip/chartist-plugin-tooltip.js"></script>
        <script src="../../assets/global/vendor/jvectormap/jquery-jvectormap.min.js"></script>
        <script src="../../assets/global/vendor/jvectormap/maps/jquery-jvectormap-world-mill-en.js"></script>
        <script src="../../assets/global/vendor/matchheight/jquery.matchHeight-min.js"></script>
        <script src="../../assets/global/vendor/peity/jquery.peity.min.js"></script>
    
    <!-- Scripts -->
    <script src="../../assets/global/js/Component.js"></script>
    <script src="../../assets/global/js/Plugin.js"></script>
    <script src="../../assets/global/js/Base.js"></script>
    <script src="../../assets/global/js/Config.js"></script>
    
    <script src="section/Menubar.js"></script>
    <script src="section/Sidebar.js"></script>
    <script src="section/PageAside.js"></script>
    <script src="section/GridMenu.js"></script>
    
    <!-- Config -->
    <script src="../../assets/global/js/config/colors.js"></script>
    <script src="../../assets/js/config/tour.js"></script>
    <script>Config.set('assets', '../assets');</script>
    
    <!-- Page -->
    <script src="../../assets/js/Site.js"></script>
    <script src="../../assets/global/js/Plugin/asscrollable.js"></script>
    <script src="../../assets/global/js/Plugin/slidepanel.js"></script>
    <script src="../../assets/global/js/Plugin/switchery.js"></script>
        <script src="../../assets/global/js/Plugin/matchheight.js"></script>
        <script src="../../assets/global/js/Plugin/jvectormap.js"></script>
        <script src="../../assets/global/js/Plugin/peity.js"></script>
    
        <script src="../../assets/examples/js/dashboard/v1.js"></script>

        


    
  <?php include "../../assets/twak/twak.php"; ?>
  </body>
</html>

<!-- 809 to 2905 multiplevendor -->